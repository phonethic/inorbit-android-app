package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.RequestTags;

public class GateToMakeSpecialOffer extends SherlockActivity implements OnClickListener{

	private Context mContext;
	private Button 	mBttnOfferForSpecialDates;
	private Button 	mBttnOfferForFavourteStore;
	private String msStoreName = "";
	private int msFavCount = -1;
	private ActionBar mActionBar;
	private String msStoreId;
	private String msMallId;
	private String msPlaceParent;
	private Button mBttnOfferForActiveMall;
	private RelativeLayout mlayoutActivMall;
	private TextView mTvMonthCount;
	private TextView mTvStoreFavCount;
	private TextView mTvActiveUsersCount;
	private SessionManager mSessionMnger;
	private FavActiveUserCountReceiver mReceiverFavActiveCount;
	private MonthlyCountReceiver mReceiverMonhtlyCount;
	private ProgressBar pBar;
	private boolean mbMerChantLoogedOut = false;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gate_to_make_special_offer);

		mContext = this;

		/** initialize the layout */
		initViews();

		/** Receive bundle values */
		Bundle mBundle = getIntent().getExtras();
		if(mBundle!=null) {
			msStoreName  	= mBundle.getString("STORE_NAME");
			msStoreId		= mBundle.getString("STORE_ID");
			msMallId 		= mBundle.getString("MALL_ID");
			msPlaceParent	= mBundle.getString("PLACE_PARENT");
		}

		/**
		 * If selected store is "Inorbit Mall Malad[Vashi|Pune|Cyberabad...]" than allow to make offer for the active mall
		 */
		if(msPlaceParent.equalsIgnoreCase("0")) {
			mBttnOfferForActiveMall.setVisibility(View.VISIBLE);
			mlayoutActivMall.setVisibility(View.VISIBLE);

			/**
			 * If selected store is not a mall manager store
			 * 
			 */
		} else {
			mBttnOfferForActiveMall.setVisibility(View.GONE);
			mlayoutActivMall.setVisibility(View.GONE);
		}
		
		mBttnOfferForSpecialDates.setText("Create special offer for customers who have birthdays & anniversaries this month");
		mBttnOfferForFavourteStore.setText("Create special offer for customers who have favourited "+msStoreName);
		mBttnOfferForActiveMall.setText("Create special offer for customers who are currently active/browsing the "+msStoreName);


		/**
		 * Netwrok calls
		 * 
		 */
		getFavActiveCounts();

		getMonthlyCounts();
	}

	private void initViews() {
		mBttnOfferForSpecialDates 	= (Button) findViewById(R.id.bttn_calendarOffer);
		mBttnOfferForFavourteStore 	= (Button) findViewById(R.id.bttn_favouriteStoreOffer);
		mBttnOfferForActiveMall		= (Button) findViewById(R.id.bttn_activeMallOffer);	
		mTvMonthCount				= (TextView) findViewById(R.id.text_monthly_userCount);	
		mTvStoreFavCount			= (TextView) findViewById(R.id.text_storeMallFav_userCount);	
		mTvActiveUsersCount			= (TextView) findViewById(R.id.text_active_userCount);	
		mlayoutActivMall			= (RelativeLayout) findViewById(R.id.layout_offer_activeMall);
		pBar						= (ProgressBar) findViewById(R.id.pBar);
		pBar.setVisibility(View.VISIBLE);

		mBttnOfferForFavourteStore.setOnClickListener(this);
		mBttnOfferForSpecialDates.setOnClickListener(this);
		mBttnOfferForActiveMall.setOnClickListener(this);



		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Special Offer");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();	

		mBttnOfferForSpecialDates.setTypeface(InorbitApp.getTypeFace());
		mBttnOfferForFavourteStore.setTypeface(InorbitApp.getTypeFace());
		mBttnOfferForActiveMall.setTypeface(InorbitApp.getTypeFace());
		mTvMonthCount.setTypeface(InorbitApp.getTypeFace());
		mTvStoreFavCount.setTypeface(InorbitApp.getTypeFace());
		mTvActiveUsersCount.setTypeface(InorbitApp.getTypeFace());

		mSessionMnger = new SessionManager(mContext);
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try {
			/**
			 * Create offer for the special event user.
			 * 
			 */
			if(v.getId() == mBttnOfferForSpecialDates.getId()) {
				//Toast.makeText(mContext, "Click 1", 0).show();
				EventTracker.logEvent(getResources().getString(R.string.mShake2Win_Calendar), false);
				Intent intent=new Intent(mContext,SpecialEventsDates.class);
				intent.putExtra("SureShop",true);
				intent.putExtra("STORE_ID", msStoreId);
				intent.putExtra("mall_id",msMallId);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				//finish();
			}

			/**
			 * Create offer for favourite users
			 * 
			 */
			if(v.getId() == mBttnOfferForFavourteStore.getId()) {
				//Toast.makeText(mContext, "Click 2 "+msFavCount, 0).show();
				if(msFavCount>0){

					//Intent intent = new Intent(mContext, CreateBroadCast.class);
					Intent intent = new Intent(mContext, CreateShakeAndWinOffer.class);


					if(msPlaceParent.equalsIgnoreCase("0")) {
						EventTracker.logEvent(getResources().getString(R.string.mShake2Win_SpecificMall), false);
						intent.putExtra("isMallWise",true);
					}else{
						EventTracker.logEvent(getResources().getString(R.string.mShake2Win_StoreFavourite), false);
						intent.putExtra("isFavouriteStore",true);
					}
					intent.putExtra("place_id",msStoreId); 
					intent.putExtra("mall_id",msMallId);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				} else {
					Toast.makeText(mContext, "No users have favorited this store", Toast.LENGTH_SHORT).show();
				}

				//finish();

			}



			/**
			 * 
			 *  Create offer for active users
			 * 
			 */

			if(v.getId() == mBttnOfferForActiveMall.getId()) {
				EventTracker.logEvent(getResources().getString(R.string.mShake2Win_ActiveMall), false);
				Intent intent = new Intent(mContext, CreateShakeAndWinOffer.class);
				intent.putExtra("isActiveMall",true);
				intent.putExtra("activeMallId",msStoreId);
				intent.putExtra("place_id",msMallId);
				startActivity(intent);
				//finish();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuItem extra	=	menu.add("View Old Offers");
		extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Special Offer")) {
			finishto();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}

		
		/**
		 * Check the old offers which has been created through shake and win
		 * 
		 */
		if(item.getTitle().toString().equalsIgnoreCase("View Old Offers")){

			//Toast.makeText(context, "clicked", 0).show();
			EventTracker.logEvent(getResources().getString(R.string.mShake2Win_SpecialPostView), false);
			Intent intent=new Intent(mContext,ViewSpecialPosts.class);
			intent.putExtra("SureShop",1);
			intent.putExtra("STORE_ID", msStoreId);
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			 finish();
		}

		return true;
	}


	/**
	 * 
	 * Network call to Get a count of the total users who have favourtied pariticual store or active in that mall
	 * 	
	 */
	private void getFavActiveCounts() {

		if(NetworkCheck.isNetConnected(mContext)) {
			pBar.setVisibility(View.VISIBLE);
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			headers.put("user_id", getUserId());
			headers.put("auth_id", getAuthId()); 
			InorbitLog.d("Merchnat user id "+getUserId());
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		
			if(msPlaceParent.equalsIgnoreCase("0")) {
				nameValuePairs.add(new BasicNameValuePair("mall_id", "1"));
			} else {
				nameValuePairs.add(new BasicNameValuePair("place_id", msStoreId));
			}

			MyClass myClass = new MyClass(mContext);
			myClass.getStoreRequest(RequestTags.Tag_GetFavActiveUserCount, nameValuePairs, headers);
		} else {
			pBar.setVisibility(View.GONE);
			Toast.makeText(mContext, "No Internet connection", Toast.LENGTH_SHORT).show();
		}
	}


	/**
	 * 
	 * Network request to get the count of the total users who have special dates for the current month
	 * 
	 */
	private void getMonthlyCounts() {

		if(NetworkCheck.isNetConnected(mContext)) {
			pBar.setVisibility(View.VISIBLE);
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			headers.put("user_id", getUserId());
			headers.put("auth_id", getAuthId()); 
			InorbitLog.d("Merchnat user id "+getUserId());
			MyClass myClass = new MyClass(mContext);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("place_id", msStoreId));
			myClass.getStoreRequest(RequestTags.Tag_GetMontlyOccasionCount, nameValuePairs, headers);	
		}else {
			pBar.setVisibility(View.GONE);
			Toast.makeText(mContext, "No Internet connection", Toast.LENGTH_SHORT).show();
		}	
	}

	String getUserId(){
		HashMap<String,String>user_details = mSessionMnger.getUserDetails();
		return user_details.get("user_id").toString();

	}

	String getAuthId(){
		HashMap<String,String>user_details = mSessionMnger.getUserDetails();
		return user_details.get("auth_id").toString();
	}

	
	
	/**
	 * 
	 * Broadcast Receiver for getFavActiveCounts()
	 * 
	 * @author Nitin
	 *
	 */
	private class FavActiveUserCountReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try {
				if(intent!=null) {
					pBar.setVisibility(View.GONE);
					String msSuccess = intent.getStringExtra("SUCCESS");
					if(msSuccess.equalsIgnoreCase("true")) {
						//Toast.makeText(mContext, "Done", Toast.LENGTH_SHORT).show();
						String msFavCountTemp = intent.getStringExtra("FAVOURITES");
						String msActive = intent.getStringExtra("ACTIVE");
						
						mTvStoreFavCount.setText(msFavCountTemp+" users have favourited");
						mTvActiveUsersCount.setText(msActive+" users are currently active");
						
						msFavCount = Integer.parseInt(msFavCountTemp);

					} else {
						String mCode = intent.getStringExtra("CODE");
						if(mCode!=null && !mCode.equalsIgnoreCase("") && mCode.equalsIgnoreCase("-116")) {
							logOutUser();
						}
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}

	}

	void logOutUser(){

		if(!mbMerChantLoogedOut){
			mbMerChantLoogedOut = true;
			mSessionMnger.logoutUser();
			Intent sIntent = new Intent(mContext, MerchantLogin.class);
			startActivity(sIntent);
			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}

	}


	/**
	 * 
	 * BroadcastReceiver for getMonthlyCounts()
	 * 
	 * @author Nitin
	 *
	 */
	private class MonthlyCountReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try {
				if(intent!=null) {
					pBar.setVisibility(View.GONE);
					String msSuccess = intent.getStringExtra("SUCCESS");
					if(msSuccess.equalsIgnoreCase("true")) {
						//Toast.makeText(mContext, "Done 1", Toast.LENGTH_SHORT).show();
						String msUsersCount = intent.getStringExtra("USER_COUNT");
						
						mTvMonthCount.setText(msUsersCount+" special events this month");
					} else {
						String mCode = intent.getStringExtra("CODE");
						if(mCode!=null && !mCode.equalsIgnoreCase("") && mCode.equalsIgnoreCase("-116")) {
							logOutUser();
						}
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishto();
	}

	void finishto(){
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			IntentFilter mFavfilter = new IntentFilter(RequestTags.Tag_GetFavActiveUserCount);
			IntentFilter mMonthlyCountfilter = new IntentFilter(RequestTags.Tag_GetMontlyOccasionCount);
			mContext.registerReceiver(mReceiverFavActiveCount = new FavActiveUserCountReceiver(), mFavfilter);
			mContext.registerReceiver(mReceiverMonhtlyCount = new MonthlyCountReceiver(), mMonthlyCountfilter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		try{
			if(mReceiverFavActiveCount!=null){
				mContext.unregisterReceiver(mReceiverFavActiveCount);
			}
			if(mReceiverMonhtlyCount!=null){
				mContext.unregisterReceiver(mReceiverMonhtlyCount);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

}

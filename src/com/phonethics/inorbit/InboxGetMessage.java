package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.adapters.GetMessageAdapter;
import com.phonethics.model.MessageModel;
import com.phonethics.model.RequestTags;

public class InboxGetMessage extends SherlockActivity {

	private Context mContext;
	private ActionBar mActionBar;
	SessionManager 	mSessionMnger;
	EditText mMessage;
	Button mSendBtn;
	LinearLayout mNewMessageLayout;
	String URL = "inbox_api/";
	ArrayList<String> allMessages;
	TextView msgTxtViews[];
	RelativeLayout mProgressBarNewMsg;
	MessageResponseReciver reciverObj;
	String mReader = "";
	String mSender = "";
	AllConversationMessages mConvoReceiver;
	UpdateStatus UpdateStatus;
	//ScrollView newMessagesScroll;

	ArrayList<String> to = new ArrayList<String>();
	ArrayList<String> message = new ArrayList<String>();
	ArrayList<String> id = new ArrayList<String>();
	ArrayList<String> time = new ArrayList<String>();
	ArrayList<String> from = new ArrayList<String>();
	ListView msg_list;
	
	ArrayList<MessageModel> message_models;
	
	int width = 0;
	int height = 0;
	String place_id="";
	String from_img_url;
	String to_img_url;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inbox_create_message);

		mContext = this;
		mActionBar	=	getSupportActionBar();
		mActionBar.setTitle("Messages");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();

		mSessionMnger = new SessionManager(mContext);
		mMessage = (EditText)findViewById(R.id.mMessage);
		mSendBtn = (Button)findViewById(R.id.mSendBtn);
		mNewMessageLayout = (LinearLayout)findViewById(R.id.messageLayout);
		allMessages = new ArrayList<String>();
		mProgressBarNewMsg = (RelativeLayout)findViewById(R.id.mProgressBarNewMsg);
		//newMessagesScroll = (ScrollView)findViewById(R.id.newMessagesScroll);
		msg_list = (ListView) findViewById(R.id.msg_list);
		
		// get screen display
		Display display = getWindowManager().getDefaultDisplay(); 
		width = display.getWidth();
		height = display.getHeight();
		
		String mSenderName = "";
		Bundle bundle = getIntent().getExtras();
		if(bundle!=null){
			mReader = bundle.getString("READER");
			mSender = bundle.getString("SENDER");
			mSenderName = bundle.getString("SENDER_NAME");
			place_id = bundle.getString("PLACE_ID");
			from_img_url= bundle.getString("FROM_IMG");
			to_img_url = bundle.getString("TO_IMG");
		}
		if (mSenderName.equals(""))
			mActionBar.setTitle(new DBUtil(this).getStoreNameFromPlaceId(mSender));
		else 
			mActionBar.setTitle(mSenderName);
		
		InorbitLog.d("INFO" + "READER " + mReader);
		InorbitLog.d("INFO" + "SENDER " + mSender);
		InorbitLog.d("INFO" + "PLACE_ID " + place_id);

		getConversation();
		mSendBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mMessage.getText().toString().trim().length()!=0){
					allMessages.add(mMessage.getText().toString().trim());
					postThisMessage();	
					//createMessageLayout();
					mMessage.setText("");
				}
				else{
					showtoast("Please enter text first");
				}

			}
		});
		
		mMessage.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.toString().trim().length() > 0) {
					mSendBtn.setBackgroundColor(Color.parseColor("#112D76"));
					mSendBtn.setTextColor(Color.parseColor("#ffffff"));;
				} else {
					mSendBtn.setBackgroundColor(Color.parseColor("#888888"));
					mSendBtn.setTextColor(Color.parseColor("#aaaaaa"));
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		setResult(RESULT_OK);
		finish();
		return true;
	}
	
	void createMessageLayout() {
		
		TextView msgTxtViews = new TextView(mContext);
		RelativeLayout.LayoutParams layoutParam = new RelativeLayout.LayoutParams(200,LayoutParams.WRAP_CONTENT );

		msgTxtViews.setText(mMessage.getText().toString());
		msgTxtViews.setBackgroundColor(Color.parseColor("#FFFFFF"));
		msgTxtViews.setTextColor(Color.parseColor("#000000"));
		msgTxtViews.setGravity(Gravity.LEFT);
		msgTxtViews.setTextSize(50);
		msgTxtViews.setPadding(20, 20, 20, 20);
		layoutParam.setMargins(0, 10, 0, 20);
		msgTxtViews.setLayoutParams(layoutParam);
		mNewMessageLayout.addView(msgTxtViews);

		//to add view for grey lines
		View view = new View(mContext);
		LayoutParams viewParam = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,10,0);
		view.setBackgroundResource(R.color.white_trans);
		view.setLayoutParams(viewParam);
		mNewMessageLayout.addView(view);

	}


	void showtoast(String msg) {
		// TODO Auto-generated method stub
		Toast.makeText(mContext, msg, 0).show();
	}

	void postThisMessage(){
		mProgressBarNewMsg.setVisibility(View.VISIBLE);

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		JSONObject json = new JSONObject();
		try {
			json.put("message", mMessage.getText().toString());
			JSONArray arr = new JSONArray();

			if(place_id.equalsIgnoreCase(mReader)){
				arr.put(mSender);
				json.put("to", arr);
				json.put("from",mReader);
			}
			else{
				arr.put(mReader);
				json.put("to", arr);
				json.put("from",mSender);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		InorbitLog.d("JSON " + json.toString());
		InorbitLog.d(mMessage.getText().toString());
		MyClass myclass = new MyClass(mContext);
		myclass.postRequest(RequestTags.TAG_CREATE_NEW_MSG, headers, json);
	}
	
	class MessageResponseReciver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {

				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					String message = intent.getStringExtra("MESSAGE");
					//mProgressBarNewMsg.setVisibility(View.GONE);
					if(success.equalsIgnoreCase("true")){
						//Toast.makeText(context, "True", Toast.LENGTH_SHORT).show();
						//showAlertDialog(message);
						/*if(((LinearLayout) mNewMessageLayout).getChildCount() > 0) 
						    ((LinearLayout) mNewMessageLayout).removeAllViews(); */
						getConversation();
						
					}else{
						showtoast(message);
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		//for posting new messages
		IntentFilter filter = new IntentFilter(RequestTags.TAG_CREATE_NEW_MSG);
		mContext.registerReceiver(reciverObj = new MessageResponseReciver(), filter);
		
		//for geting all messages in conversation
		IntentFilter mFilter = new IntentFilter(RequestTags.TAG_GET_CONVERSATION);
		mContext.registerReceiver(mConvoReceiver = new AllConversationMessages(), mFilter);
		
		//for updating status
		IntentFilter mUpdateFilter = new IntentFilter(RequestTags.TAG_UPDATE_STATUS);
		mContext.registerReceiver(UpdateStatus = new UpdateStatus(), mUpdateFilter);
	}

	protected void showAlertDialog(String message) {
		// TODO Auto-generated method stub

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(message)
		.setCancelable(false)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity
				finish();
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	//to get all conversation
	void getConversation() {
		// TODO Auto-generated method stub
		mProgressBarNewMsg.setVisibility(View.VISIBLE);

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("reader_id", mReader));
		nameValuePairs.add(new BasicNameValuePair("sender_id", mSender));
		MyClass myclass = new MyClass(mContext);
		myclass.getStoreRequest(RequestTags.TAG_GET_CONVERSATION, nameValuePairs, headers);
	}

	class AllConversationMessages extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {

				if(intent!=null){

					mProgressBarNewMsg.setVisibility(View.GONE);
					String success = intent.getStringExtra("SUCCESS");

					if(success.equalsIgnoreCase("true")){

						//Toast.makeText(context, "True", Toast.LENGTH_SHORT).show();
						to = intent.getStringArrayListExtra("CHAT_TO");
						message = intent.getStringArrayListExtra("CHAT_MSG");
						id = intent.getStringArrayListExtra("CHAT_ID");
						time = intent.getStringArrayListExtra("CHAT_TIME");
						from = intent.getStringArrayListExtra("CHAT_FROM");
						message_models = new ArrayList<MessageModel>();
						for (int i=0; i<message.size(); i++) {
							message_models.add(new MessageModel(from.get(i), to.get(i), from_img_url, to_img_url, message.get(i), time.get(i)));
						}
						
						createLayoutForConversation();
						callUpdateStatusApi();
						

					}else{
						String message = intent.getStringExtra("MESSAGE");
						showtoast(message);
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	public void createLayoutForConversation() {
		
		
		GetMessageAdapter adapter = new GetMessageAdapter(this, message_models, mReader, false);
		msg_list.setAdapter(adapter);
		
		
		//showtoast(message.size()+"");
		/*msgTxtViews = new TextView[message.size()];

		for(int i=0;i<msgTxtViews.length;i++){
			
			TextView msg_time = new TextView(mContext);
			msg_time.setText(time.get(i));
			InorbitLog.d(""+time.get(i));
			msg_time.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			msg_time.setBackgroundColor(Color.parseColor("#ffffff"));
			msg_time.setTextSize(10);
			msg_time.setTextColor(Color.parseColor("#000000"));
			msg_time.setGravity(Gravity.RIGHT);
			
			msgTxtViews[i] = new TextView(mContext);
			msgTxtViews[i].setText(message.get(i));
			msgTxtViews[i].setBackgroundColor(Color.parseColor("#FFFFFF"));
			msgTxtViews[i].setTextColor(Color.parseColor("#000000"));

			msgTxtViews[i].setTextSize(16);
			msgTxtViews[i].setPadding(20, 20, 20, 20);
			//layoutParam.setMargins(0, 10, 0, 20);

			//to decide the alignment
			if(!mReader.equalsIgnoreCase(from.get(i))){

				LinearLayout ll = new LinearLayout(this);
				ll.setGravity(Gravity.LEFT);
				ll.setBackgroundColor(Color.parseColor("#FFFFFF"));
				LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				llp.setMargins(10, 0, 60, 10);
				ll.setLayoutParams(llp);
				ll.setOrientation(LinearLayout.VERTICAL);
				
				msgTxtViews[i].setGravity(Gravity.LEFT);
				//layoutParam.gravity=Gravity.RIGHT;
				LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				//layoutParam.setMargins(10, 0, 60, 10);
				msgTxtViews[i].setLayoutParams(layoutParam);
				ll.addView(msgTxtViews[i]);
				ll.addView(msg_time);
				mNewMessageLayout.addView(ll);


				//to add view for grey lines
				View view = new View(mContext);
				LayoutParams viewParam = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,5,0);
				view.setBackgroundResource(R.color.white_trans);
				view.setLayoutParams(viewParam);
				mNewMessageLayout.addView(view);
			}
			else {
				LinearLayout ll = new LinearLayout(this);
				ll.setGravity(Gravity.RIGHT);
				LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				llp.setMargins(60, 0, 10, 10);
				ll.setLayoutParams(llp);
				//ll.setBackgroundColor(Color.parseColor("#ffffff"));
				ll.setOrientation(LinearLayout.VERTICAL);
				
				msgTxtViews[i].setGravity(Gravity.RIGHT);
				//layoutParam.gravity=Gravity.RIGHT;
				msgTxtViews[i].setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
				ll.addView(msgTxtViews[i]);
				ll.addView(msg_time);
				mNewMessageLayout.addView(ll);
				LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT );
				msgTxtViews[i].setGravity(Gravity.RIGHT);
				//layoutParam.gravity=Gravity.LEFT;
				layoutParam.setMargins(0, 10, 0, 10);
				msgTxtViews[i].setLayoutParams(layoutParam);
				mNewMessageLayout.addView(msgTxtViews[i]);


				//to add view for grey lines
				View view = new View(mContext);
				LayoutParams viewParam = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,5,0);
				view.setBackgroundResource(R.color.white_trans);
				view.setLayoutParams(viewParam);
				mNewMessageLayout.addView(view);
			}

		}*/
		//to scroll bottom
		/*newMessagesScroll.post(new Runnable() {
		     @Override
		     public void run() {
		         // This method works even better because there are no animations.
		    	 newMessagesScroll.scrollTo(0, newMessagesScroll.getBottom());
		     }
		});*/
		
		/*newMessagesScroll.post(new Runnable() { 
		    public void run() { 
		        newMessagesScroll.fullScroll(ScrollView.FOCUS_DOWN); 
		    } 
		});*/
		
		//msg_list.scrollTo(0, adapter.getCount()-1);
		
		msg_list.post(new Runnable(){
			  public void run() {
				  msg_list.setSelection(msg_list.getCount() - 1);
			  }
		});
	}


	public void callUpdateStatusApi() {
		// TODO Auto-generated method stub
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-HTTP-Method-Override", "PUT");
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("reader_id", mReader));
		nameValuePairs.add(new BasicNameValuePair("sender_id", mSender));
		
		MyClass myclass = new MyClass(mContext);
		myclass.postRequest(RequestTags.TAG_UPDATE_STATUS, nameValuePairs, headers, null);
	}
	
	class UpdateStatus extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			
			try {

				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					String message = intent.getStringExtra("MESSAGE");
					//mProgressBarNewMsg.setVisibility(View.GONE);
					if(success.equalsIgnoreCase("true")){
						//Toast.makeText(context, "True", Toast.LENGTH_SHORT).show();
						//showtoast(message);
					}else{
						showtoast(message);
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		
	}
	
	@Override
	protected void onStop() {
		try{
			mContext.unregisterReceiver(reciverObj);
			mContext.unregisterReceiver(mConvoReceiver);
			mContext.unregisterReceiver(UpdateStatus);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}

		super.onStop();
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		setResult(RESULT_OK);
		finish();
		super.onBackPressed();
	}
	
	
}

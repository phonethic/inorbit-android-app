package com.phonethics.inorbit;

import java.util.HashMap;

import org.json.JSONObject;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.CreatBroadCastNew.BroadCastOfferUpdate;
import com.phonethics.model.RequestTags;

import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Feedback extends SherlockActivity implements OnClickListener {




	private TextView text_name;
	private TextView text_email;
	private TextView text_message;
	private EditText edit_message;
	private EditText edit_email;
	private EditText edit_name;
	private Button btn_send;
	private Context mContext;
	private BroadcastReceiver mBroadcastFeedbackReceiver;
	private ProgressBar pBar;
	private ActionBar actionBar;
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);

		
		mContext = this;
		initViews();
		
	}


	private void initViews(){
		
	
	
		edit_message = (EditText) findViewById(R.id.edit_message);
		edit_email = (EditText) findViewById(R.id.edit_email);
		edit_name = (EditText) findViewById(R.id.edit_name);
		btn_send = (Button) findViewById(R.id.btn_send);
		pBar = (ProgressBar) findViewById(R.id.pBar);
		pBar.setVisibility(View.GONE);
		
	
		edit_message.setTypeface(InorbitApp.getTypeFace());
		edit_name.setTypeface(InorbitApp.getTypeFace());
		edit_email.setTypeface(InorbitApp.getTypeFace());
		btn_send.setTypeface(InorbitApp.getTypeFace());

		btn_send.setOnClickListener(this);
		actionBar		=	getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();
	}
	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==btn_send.getId()){
			if(validateData()){
				sendThisFeedback();
			}
		}

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		/*		slide_menu.toggle(true);*/
		finish();
		return true;
	}




	private boolean validateData(){
		if(edit_name.getText().toString().equalsIgnoreCase("")){
			Toast.makeText(mContext, "Please enter your name.", Toast.LENGTH_SHORT).show();
			return false;
		}  else if(edit_email.getText().toString().equalsIgnoreCase("")){
			Toast.makeText(mContext, "Please enter your email address", Toast.LENGTH_SHORT).show();
			return false;
		}  else if(!isValidEmail(edit_email.getText().toString())){
			Toast.makeText(mContext, "Please enter valid email address", Toast.LENGTH_SHORT).show();
			return false;
		} else if (edit_message.getText().toString().equalsIgnoreCase("")){
			Toast.makeText(mContext, "Please enter your message", Toast.LENGTH_SHORT).show();
			return false;
		} else {
			return true;
		}
	}
	
	public boolean isValidEmail(String target) {
		if (TextUtils.isEmpty(target)) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}
	
	
	private void sendThisFeedback(){
		
		try{
			
			pBar.setVisibility(View.VISIBLE);
			JSONObject jObj = new JSONObject();
			jObj.put("message", edit_message.getText().toString());
			jObj.put("email", edit_email.getText().toString());
			jObj.put("name", edit_name.getText().toString());

			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			
			MyClass myClass = new MyClass(mContext);
			myClass.postRequest(RequestTags.Tag_Feedback, headers, jObj);
			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	
	class BroadCastFeedback extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					pBar.setVisibility(View.GONE);
					String success = bundle.getString("SUCCESS");
					String msg = bundle.getString("MESSAGE");
					if(success.equalsIgnoreCase("true")){
						Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						
					} else {
						
					}
				}
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
		try{
			
			if(mBroadcastFeedbackReceiver!=null){
				mContext.unregisterReceiver(mBroadcastFeedbackReceiver);
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
  
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter filter = new IntentFilter(RequestTags.Tag_Feedback);
			mContext.registerReceiver(mBroadcastFeedbackReceiver = new BroadCastFeedback(), filter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
}


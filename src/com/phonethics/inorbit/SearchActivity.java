package com.phonethics.inorbit;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.AppEventsConstants;
import com.facebook.AppEventsLogger;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import com.phonethics.eventtracker.EventTracker;

import com.phonethics.model.StoreInfo;
import com.phonethics.networkcall.GetPlaceCategoryReceiver;
import com.phonethics.networkcall.GetPlaceCategoryService;
import com.phonethics.networkcall.SearchResultReceiver;
import com.phonethics.networkcall.SearchResultReceiver.SearchResult;
import com.phonethics.networkcall.SearchService;
/*import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;*/
import com.squareup.picasso.Picasso;

public class SearchActivity extends SherlockActivity implements SearchResult, OnClickListener, OnNavigationListener, OnCheckedChangeListener {

	Activity context;
	ActionBar actionbar;

	static String STORE_URL;
	static String API_HEADER;
	static String API_VALUE;
	static String STORE_CATEGORY;
	static String GET_SEARCH_URL;
	static String offer="";

	//search layout
	private EditText edtSearch;
	private ProgressBar searchListProg;
	private Button btnSearchLoadMore;
	private ListView listCategory;
	private PullToRefreshListView listSearchResult;
	private RelativeLayout searchListFilter;
	private RelativeLayout relListResult;
	private TextView txtSearchCounter;
	private TextView txtSearchCounterBack;
	private ImageView imgSearchButton;
	private ImageView imgSearchClose;


	private String TOTAL_SEARCH_PAGES="";
	private String TOTAL_SEARCH_RECORDS="";
	static String PHOTO_PARENT_URL;

	//Search
	private ArrayList<String> Search_ID=new ArrayList<String>();
	private ArrayList<String> Search_SOTRE_NAME=new ArrayList<String>();	
	private ArrayList<String> Search_BUILDING=new ArrayList<String>();
	private ArrayList<String> Search_STREET=new ArrayList<String>();
	private ArrayList<String> Search_LANDMARK=new ArrayList<String>();
	private ArrayList<String> Search_AREA=new ArrayList<String>();
	private ArrayList<String> Search_CITY=new ArrayList<String>();
	private ArrayList<String> Search_STATE=new ArrayList<String>();
	private ArrayList<String> Search_COUNTRY=new ArrayList<String>();
	private ArrayList<String> Search_MOBILE_NO=new ArrayList<String>();
	private ArrayList<String> Search_PHOTO=new ArrayList<String>();
	private ArrayList<String> Search_DISTANCE=new ArrayList<String>();
	private ArrayList<String> Search_DESCRIPTION=new ArrayList<String>();
	private ArrayList<String>category=new ArrayList<String>();
	private ArrayList<String>category_id=new ArrayList<String>();
	private ArrayList<String> Search_HAS_OFFER=new ArrayList<String>();
	private ArrayList<String> Search_OFFER=new ArrayList<String>();
	
	private ArrayList<StoreInfo> STORE_INFOS=new ArrayList<StoreInfo>();

	private int search_post_count=-1;
	private int search_page=1;

	static boolean isCheckOffers=false;
	

	private SearchResultReceiver searchReceiver;
	private GetPlaceCategoryReceiver mStoreCategoryReceiver;
	private NetworkCheck isnetConnected;
	//Session Manger
	private SessionManager session;
	boolean isSplitLogin=false;
	/*	SlidingMenu slide_menu ;*/
	private TextView mtvErrorMsg;
	private String business_id="1";
	private CategoryListAdapter categAdapter;
	private RelativeLayout categoryLayout;
	private CheckBox chkBox;
	private boolean isRefreshing=false;

	private ArrayList<String>dropdownItems=new ArrayList<String>();
	private String key="place_id";
	private String value;

	private int dropdown_position;
	private DBUtil dbutil;

	private String mallName = "";
	private String MALL_NAME;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		context=this;
		
		
		/**
		
		 * initialize the layout files, class variables.
		  
		 */
		
		dbutil = new DBUtil(context);
		isnetConnected=new NetworkCheck(context);
		chkBox=(CheckBox)findViewById(R.id.chkBox);
		chkBox.setOnCheckedChangeListener(this);
		edtSearch=(EditText)findViewById(R.id.edtSearch);
		btnSearchLoadMore=(Button)findViewById(R.id.btnSearchLoadMore);
		listSearchResult=(PullToRefreshListView)findViewById(R.id.listSearchResult);
		categoryLayout=(RelativeLayout)findViewById(R.id.categoryLayout);
		searchListFilter=(RelativeLayout)findViewById(R.id.searchListFilter);
		relListResult=(RelativeLayout)findViewById(R.id.relListResult);
		txtSearchCounter=(TextView)findViewById(R.id.txtSearchCounter);
		txtSearchCounterBack=(TextView)findViewById(R.id.txtSearchCounterBack);
		imgSearchButton=(ImageView)findViewById(R.id.imgSearchButton);
		imgSearchClose=(ImageView)findViewById(R.id.imgSearchClose);
		searchListProg=(ProgressBar)findViewById(R.id.searchListProg);
		
		
		//SearchReceiver
		searchReceiver=new SearchResultReceiver(new Handler());
		searchReceiver.setReceiver(this);
		
		
		//Category Receiver
		mStoreCategoryReceiver=new GetPlaceCategoryReceiver(new Handler());
		mStoreCategoryReceiver.setReceiver(this);
		
		
		STORE_URL= getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		/*STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.dummy_search);*/
		GET_SEARCH_URL=getResources().getString(R.string.searchapi);


		//Store Category
		STORE_CATEGORY=getResources().getString(R.string.place_category);

		//Photo url
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		//Api Key
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);
		key = getResources().getString(R.string.id_key);

		
		
		
		/**
		 * Initialize the action bar
		 * 
		 */
		actionbar=getSupportActionBar();
		actionbar.setTitle(getResources().getString(R.string.actionBarTitle));
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();

		//List Navigation

		getSupportActionBar().getThemedContext();
		dropdownItems = dbutil.getAllAreas();
		CustomSpinnerAdapter business_list=new CustomSpinnerAdapter(context, 0, 0, dropdownItems);
		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionbar.setListNavigationCallbacks(business_list, this);
		String activeAreaId = dbutil.getActiveMallId();
		String areaName = dbutil.getAreaNameByInorbitId(activeAreaId);
		int areaPos = dropdownItems.indexOf(areaName);
		Log.d("", "Inorbit active id "+activeAreaId+" Area : "+areaName+" ArrayPos "+areaPos);
		actionbar.setSelectedNavigationItem(areaPos);
		mtvErrorMsg = (TextView) findViewById(R.id.error_message);
		session=new SessionManager(getApplicationContext());
		isSplitLogin=session.isLoggedIn();
	
		txtSearchCounterBack.getBackground().setAlpha(200);

		//Search List Categories

		listCategory=(ListView)findViewById(R.id.listCategory);
		category.add("Offers");
		category_id.add("-1");
		searchListFilter.setVisibility(View.VISIBLE);
		relListResult.setVisibility(View.GONE);
		categoryLayout.setVisibility(View.GONE);

		
		
		categAdapter=new CategoryListAdapter(context, R.drawable.ic_launcher,  R.drawable.ic_launcher,category);
		listCategory.setAdapter(categAdapter);

		
		
		/**
		 * 
		 * load the categories.
		 */
		if(isnetConnected.isNetworkAvailable()){
			loadAllCategories();
		}else{
			MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);

		}
		
		//Refresh Listener
		listSearchResult.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				isRefreshing=true;
				try
				{
					if(isnetConnected.isNetworkAvailable()) {
						if(edtSearch.getText().toString().length()!=0 /*&& edtSearch.getText().toString().length() >=4 */) {
							search_page=1;
							search_post_count=-1;
							searchApi();

						} else {
							//Toast.makeText(context, "Search text should be atleast 4 characters", Toast.LENGTH_SHORT).show();
							//MyToast.showToast(context, "Search text should be atleast 4 characters", Toast.LENGTH_SHORT);
							LayoutInflater inflator=context.getLayoutInflater();
							View view =inflator.inflate(R.layout.customtoast, null);
							ImageView toastIcon=(ImageView)view.findViewById(R.id.toastIcon);
							toastIcon.setVisibility(View.GONE);
							TextView textView=(TextView)view.findViewById(R.id.customToastText);
							textView.setText("Type something to search");
							Toast toast=new Toast(context);
							toast.setDuration(Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.setView(view);
							//toast.setText("Search text should be atleast 4 characters");
							toast.show();
						}
					} else {
						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);

					}
				} catch(Exception ex) {
					ex.printStackTrace();
				}

			}
		});

		

		btnSearchLoadMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isnetConnected.isNetworkAvailable())
				{

					if(Search_ID.size()==Integer.parseInt(TOTAL_SEARCH_RECORDS))
					{
						Log.i("ID SIZE","ID SIZE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
						btnSearchLoadMore.setVisibility(View.GONE);
					}else if(search_page<Integer.parseInt(TOTAL_SEARCH_PAGES))
					{
						if(!searchListProg.isShown())
						{

							search_page++;
							Log.i("Page", "Page "+search_page);
							searchApi();
						}
						else
						{
							Toast.makeText(context, "Please wait while loading",Toast.LENGTH_SHORT).show();
						}

					}
					else
					{
						btnSearchLoadMore.setVisibility(View.GONE);
					}
				}

			}
		});

		//imgSearchButton click 
		imgSearchButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//Animating EditText
				Animation animation = new ScaleAnimation(0, 1, 1,1, Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 1);
				animation.setDuration(250);
				edtSearch.setVisibility(View.VISIBLE);
				//commented purposely

				edtSearch.startAnimation(animation);
				animation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub

						//Search Layout

						Animation animaSlidUp=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						searchListFilter.startAnimation(animaSlidUp);

					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						//Temporarily disabled 
						searchListFilter.setVisibility(View.VISIBLE);

						// TODO Auto-generated method stub
						

					}
				});
			}
		});

		//Add text change listener
		edtSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				categoryLayout.setVisibility(View.VISIBLE);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()==0){
					
					if(!searchListFilter.isShown()) {
						Animation animaSlidUp=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						searchListFilter.startAnimation(animaSlidUp);
						searchListFilter.setVisibility(View.VISIBLE);
						relListResult.setVisibility(View.GONE);

						clearSearchArray();

					}
					
					Animation animaSlidOut=AnimationUtils.loadAnimation(context, R.anim.fade_out);
					categoryLayout.startAnimation(animaSlidOut);
					categoryLayout.setVisibility(View.GONE);
				}
			}
		});
		//Searching In API.
		edtSearch.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					
					if(isnetConnected.isNetworkAvailable()){
						
						if(edtSearch.getText().toString().length()!=0 /*&& edtSearch.getText().toString().length()>=4*/){
							
							Animation animaSlidDown=AnimationUtils.loadAnimation(context, R.anim.fade_out);
							animaSlidDown.setFillAfter(true);

							//Temporarily disabled
							searchListFilter.startAnimation(animaSlidDown);
							categoryLayout.startAnimation(animaSlidDown);
							
							clearSearchArray();
							searchApi();
							
							categoryLayout.setVisibility(View.GONE);
							if(chkBox.isChecked()){
								chkBox.setChecked(false);
								offer="";
								isCheckOffers=false;
							}
							
							
							try{
								
								InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
								
							}catch(NullPointerException npe){
								npe.printStackTrace();
							}catch(Exception ex){
								ex.printStackTrace();
							}

							relListResult.setVisibility(View.VISIBLE);

							animaSlidDown.setAnimationListener(new AnimationListener() {

								@Override
								public void onAnimationStart(Animation animation) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onAnimationRepeat(Animation animation) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onAnimationEnd(Animation animation) {
									// TODO Auto-generated method stub
									//Temporarily disabled
									searchListFilter.setVisibility(View.GONE);
									relListResult.setVisibility(View.VISIBLE);			

								}
							});
						}else{
							//Toast.makeText(context, "Search text should atleast 4 characters", Toast.LENGTH_SHORT).show();
							View view =getLayoutInflater().inflate(R.layout.customtoast, null);
							ImageView toastIcon=(ImageView)view.findViewById(R.id.toastIcon);
							toastIcon.setVisibility(View.GONE);
							TextView textView=(TextView)view.findViewById(R.id.customToastText);
							textView.setText("Type something in the search box");
							Toast toast=new Toast(context);
							toast.setDuration(Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.setView(view);
							//toast.setText("Search text should be atleast 4 characters");
							toast.show();
						}
					}else{
						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
					}

					return true;
				}
				return false;
			}
		});

		imgSearchClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//Animating Edit text
				Animation animation = new ScaleAnimation(1, 0, 1,1, Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 1);
				animation.setDuration(250);
				final Animation animaSlidDown=AnimationUtils.loadAnimation(context, R.anim.fade_out);
				animaSlidDown.setFillAfter(true);

				final Animation animFadeOut=AnimationUtils.loadAnimation(context, R.anim.fade_out);
				AnimationUtils.loadAnimation(context, R.anim.fade_in);

				edtSearch.startAnimation(animation);
				animation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
						//Temporarily disabled
						searchListFilter.startAnimation(animaSlidDown);
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub
						imgSearchClose.setVisibility(View.GONE);
						edtSearch.setVisibility(View.GONE);
						searchListFilter.setVisibility(View.GONE);
						relListResult.setVisibility(View.GONE);
						imgSearchButton.setVisibility(View.VISIBLE);



						try{
							//Hiding keyboard
							InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
						}catch(NullPointerException npe){
							npe.printStackTrace();
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}

					}
				});
				animFadeOut.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub

					}
				});
				clearSearchArray();
				
			}
		});


	}//onCreate Ends Here


	class CustomSpinnerAdapter extends ArrayAdapter<String> implements SpinnerAdapter {

		ArrayList<String>name;
		Activity context;
		LayoutInflater layoutInflater;
		public CustomSpinnerAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> name) {
			super(context, resource, textViewResourceId, name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.name=name;
			layoutInflater=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}


		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return name.get(position);
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = layoutInflater.inflate(R.layout.list_menus, parent, false);
			}
			((TextView) convertView.findViewById(R.id.listText))
			.setText(getItem(position));
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
		
				convertView = layoutInflater.inflate(R.layout.list_dropdown_item, parent, false);
			}
			((TextView) convertView.findViewById(R.id.txtSpinner))
			.setText(getItem(position));
			return convertView;
		}


	}
	
	/***
	 * 
	 * Clear all the array list
	 * 
	 * 
	 */
	void clearSearchArray()
	{
		Search_ID.clear();
		Search_SOTRE_NAME.clear();
		Search_BUILDING.clear();
		Search_STREET.clear();
		Search_LANDMARK.clear();
		Search_AREA.clear();
		Search_CITY.clear();
		Search_MOBILE_NO.clear();
		Search_PHOTO.clear();
		Search_DISTANCE.clear();
		Search_DESCRIPTION.clear();
		Search_HAS_OFFER.clear();
		Search_OFFER.clear();
		search_page=1;
		search_post_count=-1;
		
		STORE_INFOS.clear();

		//	btnSearchLoadMore.setVisibility(View.GONE);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent=new Intent(context,HomeGrid.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();


	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		Intent intent=new Intent(context,HomeGrid.class);
		startActivity(intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}

	/**
	 * 
	 * 
	 * network call to search the stores based on the search value entered.
	 * 
	 * 
	 */
	void searchApi()
	{


		registerSearchTextEvent(edtSearch.getText().toString());
		Intent intent=new Intent(context, SearchService.class);
		intent.putExtra("searchapi",searchReceiver);
		
		intent.putExtra("URL", STORE_URL+GET_SEARCH_URL);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("search_post_count",search_post_count);
		intent.putExtra("search_page",search_page);
		intent.putExtra("search", edtSearch.getText().toString());
		intent.putExtra("key", key);
		intent.putExtra("value", value);
		//intent.putExtra("isShopLocal",IS_SHOPLOCAL);

		intent.putExtra("offer", offer);
		intent.putExtra("isCheckOffers", isCheckOffers);
		context.startService(intent);
		searchListProg.setVisibility(View.VISIBLE);
	}

	/**
	 * Load All Categories
	 * 
	 * 
	 */
	
	void loadAllCategories()
	{
		Intent intent=new Intent(context,GetPlaceCategoryService.class);
		intent.putExtra("category",mStoreCategoryReceiver);
		intent.putExtra("URL", STORE_URL+STORE_CATEGORY);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("IsAppend", false);
		intent.putExtra("STORE_ID", business_id);
		context.startService(intent);
		searchListProg.setVisibility(View.VISIBLE);
	}
	
	
	/**
	 * 
	 * Result receiver of the search network call information
	 * 
	 * 
	 */

	@Override
	public void onReciverSearchResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		try {
			String activeAreaId = dbutil.getActiveMallId();
			mallName = dbutil.getAreaNameByInorbitId(activeAreaId);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
		/**
		 * 
		 * Log the parameters on facebook
		 * 
		 * 
		 */
		AppEventsLogger logger = AppEventsLogger.newLogger(this);
		Bundle parameters = new Bundle();
		parameters.putString("Mall", mallName);
		parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "Store");
		parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, edtSearch.getText().toString());


		try{
			String SEARCH_STATUS=resultData.getString("SEARCH_STATUS");
			searchListProg.setVisibility(View.GONE);
			if(SEARCH_STATUS.equalsIgnoreCase("true")){

				mtvErrorMsg.setVisibility(View.GONE);
				parameters.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, "TRUE");

				final ArrayList<String> TempID=resultData.getStringArrayList("ID");
				final ArrayList<String> TempSOTRE_NAME=resultData.getStringArrayList("SOTRE_NAME");
				ArrayList<String> TempBUILDING=resultData.getStringArrayList("BUILDING");
				ArrayList<String> TempSTREET=resultData.getStringArrayList("STREET");
				ArrayList<String> TempLANDMARK=resultData.getStringArrayList("LANDMARK");
				final ArrayList<String> TempAREA=resultData.getStringArrayList("AREA");
				final ArrayList<String> TempCITY=resultData.getStringArrayList("CITY");
				final ArrayList<String> TempMOBILE_NO=resultData.getStringArrayList("MOBILE_NO");
				final ArrayList<String> TempPHOTO=resultData.getStringArrayList("PHOTO");
				ArrayList<String> TempDISTANCE=resultData.getStringArrayList("DISTANCE");
				ArrayList<String> TempDescription=resultData.getStringArrayList("DESCRIPTION");
				final ArrayList<String> category = resultData.getStringArrayList("CATEGORY");
				final ArrayList<String> place_parent = resultData.getStringArrayList("PLACE_PARENT");
				ArrayList<String> TempHasOffers=new ArrayList<String>();
				ArrayList<String> TempOffers=new ArrayList<String>();
				TempHasOffers.clear();
				TempOffers.clear();
				TempHasOffers=resultData.getStringArrayList("HAS_OFFERS");
				TempOffers=resultData.getStringArrayList("OFFERS");

				TOTAL_SEARCH_PAGES=resultData.getString("TOTAL_PAGES");
				TOTAL_SEARCH_RECORDS=resultData.getString("TOTAL_RECORDS");
				
				if(isRefreshing){
					clearSearchArray();
					isRefreshing=false;
				}

				for(int index=0;index<TempID.size();index++){
					
					Search_ID.add(TempID.get(index));
					Search_SOTRE_NAME.add(TempSOTRE_NAME.get(index));
					Search_BUILDING.add(TempBUILDING.get(index));
					Search_STREET.add(TempSTREET.get(index));
					Search_LANDMARK.add(TempLANDMARK.get(index));
					Search_AREA.add(TempAREA.get(index));
					Search_CITY.add(TempCITY.get(index));
					Search_MOBILE_NO.add(TempMOBILE_NO.get(index));
					Search_PHOTO.add(TempPHOTO.get(index));
					Search_DISTANCE.add(TempDISTANCE.get(index));
					/*TempDISTANCE.add("0");*/
					Search_DESCRIPTION.add(TempDescription.get(index));
					Search_HAS_OFFER.add(TempHasOffers.get(index));
					Search_OFFER.add(TempOffers.get(index));
					
					StoreInfo storeInfo = new StoreInfo();
					storeInfo.setId(TempID.get(index));
					storeInfo.setBuilding(TempBUILDING.get(index));
					storeInfo.setCity(TempCITY.get(index));
					storeInfo.setArea(TempAREA.get(index));
					storeInfo.setName(TempSOTRE_NAME.get(index));
					storeInfo.setStreet(TempSTREET.get(index)); 
					storeInfo.setImage_url(TempPHOTO.get(index));
					storeInfo.setCategory(category.get(index));
					storeInfo.setPlace_parent(place_parent.get(index));
					STORE_INFOS.add(storeInfo);
				}

				//Get the first visible position of listview
				final int old_pos = listSearchResult.getRefreshableView().getFirstVisiblePosition();
				listSearchResult.setVisibility(View.VISIBLE);
				SearchApiListAdapter adapter=new SearchApiListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, 
						Search_SOTRE_NAME, Search_PHOTO,Search_HAS_OFFER,Search_OFFER,Search_DESCRIPTION,Search_DISTANCE, STORE_INFOS);
				listSearchResult.setAdapter(adapter);
				txtSearchCounter.setText(Search_ID.size()+" Stores");
				if(Integer.parseInt(TOTAL_SEARCH_RECORDS)==Search_ID.size()){
					btnSearchLoadMore.setVisibility(View.GONE);
				}

				listSearchResult.onRefreshComplete();
				listSearchResult.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						listSearchResult.getRefreshableView().setSelection(old_pos);
					}
				});


				listSearchResult.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						if(!isRefreshing){
							Intent intent=new Intent(context, StoreDetails.class);
							intent.putExtra("STORE_ID", Search_ID.get(position-1));
							intent.putExtra("STORE", Search_SOTRE_NAME.get(position-1));
							intent.putExtra("PHOTO", Search_PHOTO.get(position-1));
							intent.putExtra("AREA", Search_AREA.get(position-1));
							intent.putExtra("CITY", Search_CITY.get(position-1));
							intent.putExtra("MOBILE_NO", Search_MOBILE_NO.get(position-1));
							intent.putExtra("MALL_NAME", MALL_NAME);
							intent.putExtra("STORE_INFO", STORE_INFOS.get(position-1));
							intent.putExtra("mall_id", STORE_INFOS.get(position-1).getPlace_parent());
							intent.putExtra("place_id", STORE_INFOS.get(position-1).getId());
							intent.putExtra("category", category.get(position-1));
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					}
				});
			}
			if(!SEARCH_STATUS.equalsIgnoreCase("true")){
				parameters.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, "FALSE");
				txtSearchCounter.setText("");
				String message="";
				message=resultData.getString("SEARCH_MESSAGE");
				mtvErrorMsg.setVisibility(View.VISIBLE);
				mtvErrorMsg.setTypeface(InorbitApp.getTypeFace());
				mtvErrorMsg.setText(message);
				//Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
				txtSearchCounter.setText("");
				btnSearchLoadMore.setVisibility(View.GONE);
				listSearchResult.onRefreshComplete();
			}

			
			
			
			logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED,parameters);

		}catch(Exception ex){
			ex.printStackTrace();
			listSearchResult.onRefreshComplete();
		}


	}

	//CategoryList Adapter
	
	/**
	 * 
	 * List adapter fot category list
	 * 
	 * @author Nitin
	 *
	 */

	static class CategoryListAdapter extends ArrayAdapter<String>{
		ArrayList<String> category_name;
		Activity context;
		LayoutInflater inflate;
		Typeface tf;
		Animation anim_fromRight,anim_fromLeft;
		String isFav="";

		public CategoryListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> category_name) {
			super(context, resource, textViewResourceId, category_name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.category_name=category_name;

			inflate=context.getLayoutInflater();

			//getting font from asset directory.
			tf=Typeface.createFromAsset(context.getAssets(), "fonts/DinDisplayProLight.otf");
			

			anim_fromRight=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim_fromLeft=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return category_name.size();
		}
		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return category_name.get(position);
		}
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			//inflate a view and build ui for list.
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.categorylistlayout,null);
				holder.txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);
				holder.chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
				holder.categoryLayout=(RelativeLayout)convertView.findViewById(R.id.categoryLayout);
				convertView.setTag(holder);

			}
			final ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.txtCategory.setText(category_name.get(position));
			hold.chkBox.setVisibility(View.GONE);
	

			return convertView;
		}

		static class ViewHolder
		{
			ImageView imgStoreLogo;
			TextView txtCategory;
			CheckBox chkBox;
			RelativeLayout categoryLayout;

		}

	}

	//Search Api Adapter
	
	/**
	 * 
	 * List adapter for Search results 
	 * 
	 * @author Nitin
	 *
	 */

	static class SearchApiListAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> store_name,distance,logo,has_offer,offers;
		ArrayList<String>Search_DESCRIPTION;
		ArrayList<StoreInfo> storeInfo;
		Activity context;
		LayoutInflater inflate;
		String photo;

		public SearchApiListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> store_name,ArrayList<String>logo,ArrayList<String>has_offers,ArrayList<String>offers,ArrayList<String>Search_DESCRIPTION,ArrayList<String>distance,
				ArrayList<StoreInfo> storeInfo) {
			super(context, resource, textViewResourceId, store_name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.store_name=store_name;
			this.distance=distance;
			this.logo=logo;
			this.Search_DESCRIPTION=Search_DESCRIPTION;
			
			this.offers=offers;
			this.has_offer=has_offers;
			inflate=context.getLayoutInflater();
			this.storeInfo = storeInfo;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return store_name.size();
		}
		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return store_name.get(position);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null){
				
				ViewHolder holder=new ViewHolder();
				
				convertView=inflate.inflate(R.layout.layout_search_api,null);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.imgStoreLogo.setScaleType(ScaleType.FIT_CENTER);
				holder.txtStore=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.txtDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				holder.textBookticket = (TextView) convertView.findViewById(R.id.textBookticket);
				
				holder.txtCounter = (TextView)convertView.findViewById(R.id.txtCounter);
				holder.txtCounter.setVisibility(View.GONE);
				

				holder.band=(TextView)convertView.findViewById(R.id.band);
				holder.band.setTextColor(Color.WHITE);
				
				convertView.setTag(holder);

			}
			
			ViewHolder hold=(ViewHolder)convertView.getTag();

			if(!logo.get(position).toString().equalsIgnoreCase("null")){
				photo=logo.get(position).replaceAll(" ", "%20");
				try {

					Picasso.with(context).load(PHOTO_PARENT_URL+photo)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(hold.imgStoreLogo);

				} catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){
					e.printStackTrace();
				}



			}
			else
			{
				hold.imgStoreLogo.setImageResource(R.drawable.ic_launcher);
			}
			hold.txtStore.setText(store_name.get(position));
			hold.txtSearchStoreDesc.setText(Search_DESCRIPTION.get(position));
			
			/*if (storeInfo.get(position).getCategory().equalsIgnoreCase("Dine")) {
				hold.textBookticket.setText("Dine");
			} else {
				Toast.makeText(context, storeInfo.get(position).getCategory(), Toast.LENGTH_SHORT).show();
			}*/
			
			hold.band.setVisibility(View.VISIBLE);
			if(has_offer.get(position).toString().length()!=0 && has_offer.get(position).equalsIgnoreCase("1")){
				
				hold.band.setText("Specail Offer : "+offers.get(position));	
			}else{
				hold.band.setVisibility(View.GONE);	
			}

			return convertView;
		}

		static class ViewHolder{
			ImageView imgStoreLogo;
			TextView txtStore;
			TextView txtSearchStoreDesc,txtCounter;
			TextView txtDistance;
			TextView band;
			TextView textBookticket;
		}


	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub

		value = dbutil.getMallIdByPos(""+(itemPosition+1));
		MALL_NAME = dropdownItems.get(itemPosition);
		dbutil.setActiveMall(value);
		
		createInhouseAnalyticsEvent("0", "8", dbutil.getActiveMallId());
		
		clearSearchArray();
		listSearchResult.setVisibility(View.GONE);
		if(categAdapter!=null){
			categAdapter.notifyDataSetChanged();
		}
		searchApi();
		Log.i("GET DROP DOWN ", "GET DROP DOWN ");
		return false;
	}
	
	private void createInhouseAnalyticsEvent(String flag, String activityId, String mallId) {
		Uri uri = new Uri.Builder().scheme("content").authority(context.getResources().getString(R.string.authority)).
				appendPath("/insert").build();
        ContentValues cv = new ContentValues(6);
		cv.put("UDM_ID", session.getUdmIDForCustomer());
		cv.put("DATE", new Date().toString());
		cv.put("FLAG", flag);
		cv.put("ACTIVITY_ID", activityId);
		cv.put("MALL_ID", mallId);
		cv.put("SYNC_STATUS", -1);
		context.getContentResolver().insert(uri, cv);
	}
	
	/**
	 * Result receiver for the categories network call
	 *  
	 */
	//@Override
	
	public void onReceiveStoreCategories(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		String category_Status=resultData.getString("category_status");
		ArrayList<String>CATEGORY_NAME=new ArrayList<String>();
		ArrayList<String>CATEGORY_ID=new ArrayList<String>();

		if(category_Status.equalsIgnoreCase("true")) {
			CATEGORY_NAME=resultData.getStringArrayList("CATEGORY_NAME");
			CATEGORY_ID=resultData.getStringArrayList("CATEGORY_ID");
			category.clear();
			category_id.clear();
			category.add("Offers");
			category_id.add("-1");
			for(int i=0;i<CATEGORY_NAME.size();i++)	{
				category.add(CATEGORY_NAME.get(i));
				category_id.add(CATEGORY_ID.get(i));
			}
			categAdapter=new CategoryListAdapter(context, R.drawable.ic_launcher,  R.drawable.ic_launcher,category);
			listCategory.setAdapter(categAdapter);
			categAdapter.notifyDataSetChanged();

			listCategory.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					if(position==0){
						Intent intent=new Intent(context,OffersNew.class);
						context.startActivityForResult(intent, 5);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);	
					}
					else if(!category_id.get(position).toString().equalsIgnoreCase("-1")){
						Intent intent=new Intent(context,StoreListing.class);
						if(category.get(position).toString().equalsIgnoreCase("shop")){
							Tables.setCurrentCategory(Tables.CATEGORY_SHOP);
						}else if(category.get(position).toString().contains("Dine")){
							Tables.setCurrentCategory(Tables.CATEGORY_DINE);
						}else if(category.get(position).toString().equalsIgnoreCase("entertainment")){
							Tables.setCurrentCategory(Tables.CATEGORY_ENTERTAINMENT);
						}else if(category.get(position).toString().equalsIgnoreCase("pamper zone")){
							Tables.setCurrentCategory(Tables.CATEGORY_PAMPERZONE);
						}
						intent.putExtra("callFromSearch", true);
						context.startActivityForResult(intent, 5);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
			});
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if(buttonView.isChecked())	{
			offer="1";
			isCheckOffers=true;
		}
		else{
			offer="";
			isCheckOffers=false;
		}
	}


	public void showToast(String text){
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==5){
			try{
				if(getResources().getBoolean(R.bool.fromSharePref)){
					SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
					dropdown_position=pref.getInt("dropdown_position",0);
					value = pref.getString("selected_location_id", "292");
					actionbar.setSelectedNavigationItem(dropdown_position);
				}else{
					String activeAreaId = dbutil.getActiveMallId();
					String areaName = dbutil.getAreaNameByInorbitId(activeAreaId);
					int areaPos = dropdownItems.indexOf(areaName);
					Log.d("", "Inorbit active id "+activeAreaId+" Area : "+areaName+" ArrayPos "+areaPos);
					actionbar.setSelectedNavigationItem(areaPos);
				}
				Log.d("", "Inorbit Reuslt code 5");

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try{
			EventTracker.endLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}
	
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub

		try{
			EventTracker.endFlurrySession(getApplicationContext());
			//imageLoaderList.clearCache();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onStop();
	}


	void registerSearchTextEvent(String text){
		try{
			Map<String, String> param = new HashMap<String, String>();
			param.put("Search_Text", text);
			param.put("Category_Name", "All");

			if(text!=null && !text.equalsIgnoreCase("")){
				EventTracker.logEvent(getResources().getString(R.string.search_StoreCategory), param);	
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void registerSearchCategoryEvent(String category){
		try{
			Map<String, String> param = new HashMap<String, String>();
			param.put("Category_Name", category);

			if(category!=null && !category.equalsIgnoreCase("")){
				EventTracker.logEvent(getResources().getString(R.string.search_StoreCategory), param);	
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}

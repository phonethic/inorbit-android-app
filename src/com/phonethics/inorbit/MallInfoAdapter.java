package com.phonethics.inorbit;

import java.util.ArrayList;


import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MallInfoAdapter extends ArrayAdapter<InorbitServiceInfo>{

	Activity 						context;
	ArrayList<InorbitServiceInfo> 	mallInfoArr;
	private static LayoutInflater 	inflator = null;
	//ImageLoader						imageLoader;

	public MallInfoAdapter(Activity context,ArrayList<InorbitServiceInfo> mallInfoArr){
		super(context,R.layout.grid_refrence_information);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.mallInfoArr = mallInfoArr;
		//imageLoader	= new ImageLoader(context);
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mallInfoArr.size();
	}


	@Override
	public InorbitServiceInfo getItem(int position) {
		// TODO Auto-generated method stub
		return mallInfoArr.get(position);
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){

			ViewHolder holder 		= new ViewHolder();
			inflator				= context.getLayoutInflater();
			convertView 			= inflator.inflate(R.layout.grid_refrence_information, null);
			

			holder.text_title		= (TextView) convertView.findViewById(R.id.text_title);
			holder.text_content		= (TextView) convertView.findViewById(R.id.text_info);
			holder.image_thumbnail	= (ImageView) convertView.findViewById(R.id.img_thumb);
			
			
			holder.text_title.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
			holder.text_content.setTypeface(InorbitApp.getTypeFace());
			holder.text_title.setTextSize(13);
			holder.text_content.setTextSize(12);
			

			convertView.setTag(holder);
		}

		ViewHolder hold = (ViewHolder) convertView.getTag();


		try{

			hold.text_title.setText(getItem(position).getTitle().toUpperCase());
			hold.text_content.setText(Html.fromHtml(getItem(position).getContent()));
			//			imageLoader.DisplayImage((getItem(position).getThumbnail()), hold.image_thumbnail);

			/*if(position==0){
				MallInfo.grid_inorbit.setNumColumns(1);
			} else {
				MallInfo.grid_inorbit.setNumColumns(2);
			}*/

			try {
				
				

				if(getItem(position).getThumbnail().equalsIgnoreCase("")){
					hold.image_thumbnail.setImageResource(R.drawable.ic_launcher);
				}else{
					Picasso.with(context).load(getItem(position).getThumbnail())
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(hold.image_thumbnail);
				}
				

			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			}
			catch(Exception e){
				e.printStackTrace();
			}





		}catch(Exception ex){
			ex.printStackTrace();
		}


		return convertView;
	}


	static class ViewHolder{
		public TextView 	text_title,text_content;
		public ImageView	image_thumbnail;
	}




}

package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.view.MenuItem;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phoenthics.adapter.MallOffersAdapter;
import com.phoenthics.adapter.OffersAdapter;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.adapters.ActionAdapter;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.StoreDetails.GalleryResultBroadcast;
import com.phonethics.inorbit.StoreDetails.LikePlace;
import com.phonethics.inorbit.StoreDetails.PageAdapter;
import com.phonethics.inorbit.StoreDetails.ParticularStoreDetailBroadcast;
import com.phonethics.inorbit.StoreDetails.SearchListAdapter;
import com.phonethics.inorbit.StoreDetails.StoresOffer;
import com.phonethics.inorbit.StoreDetails.UnLikePlace;
import com.phonethics.inorbit.StoreListing.SearachBroadcast;
import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;
import com.phonethics.model.StoreInfo;

import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class OffersStore extends SherlockActivity implements OnNavigationListener {

	private Activity 			mContext;
	private ActionBar 			mActionbar;
	private DBUtil				mDbutil;
	private SessionManager 		mSession;

	private EditText 			mEtSearchText;
	private ProgressBar 		mProg;
	private TextView 			mTvSearchCounter;
	private ImageView 			mIvSearch;
	private TextView 			mTvCounterBack;
	private ImageView 			mIvConnctionErr;
	private PullToRefreshListView mLvStores;
	private ArrayList<String>	mArrMallAreas;
	private ArrayList<StoreInfo> mArrStores;
	private  SessionManager 		session;  
	private ArrayList<PostDetail> arrOFFERS;

	private SearachBroadcast mSerachBroadCast;
	private LinearLayout	mLinearSearch;
	private InobitStoresDb  mInorbitDb;
	private boolean mbIsRefreshing  = false;
	private ArrayList<PostDetail> mArrOFFERS;
	private StoresOffer offersBroadCast;
	private Map<String, String> flurryEeventParams;
	private TextView mTvOfferCounter;
	private int preVisibleItem;
	boolean 			getLike		=	false;
	private StoreInfo storeInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offers_new);
		
		mContext 	= this;

		initViews();
		initActionBar();
		initObjects();
		//creatActionBarList();
		


		try {
			Bundle b=getIntent().getExtras();
			storeInfo = (StoreInfo) b.getParcelable("STORE_INFO");
			if(storeInfo!=null){
				InorbitLog.d("Store Details "+storeInfo.getTitle());
				InorbitLog.d("Store Details "+storeInfo.getCity());
				mActionbar.setTitle(storeInfo.getName());

				if(NetworkCheck.isNetConnected(mContext)) {
					getOffersOfStores();
				}
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}


		mLvStores.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				try{
					uploadBroadcastEvent( mArrOFFERS.get(position-1).getTitle(),mArrOFFERS.get(position-1).getName());
					//Intent intent=new Intent(mContext, OffersBroadCastDetails.class);
					Intent intent=new Intent(mContext, OfferViewNew.class);
					Conifg.fromPedingOffers = false;
					intent.putExtra("storeName", mArrOFFERS.get(position-1).getName());
					intent.putExtra("mallName", getActiveAreaName());
					intent.putExtra("title", mArrOFFERS.get(position-1).getTitle());
					intent.putExtra("body", mArrOFFERS.get(position-1).getDescription());
					intent.putExtra("POST_ID", mArrOFFERS.get(position-1).getId());
					intent.putExtra("PLACE_ID", mArrOFFERS.get(position-1).getPlace_id());
					intent.putExtra("fromCustomer", true);
					intent.putExtra("photo_source",mArrOFFERS.get(position-1).getImage_url1());
					mContext.startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
		});

		mLvStores.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				try{
					if(NetworkCheck.isNetConnected(mContext)){
						//getOffersOfMall();
						getOffersOfStores();
					}else{
						MyToast.showToast(mContext, getResources().getString(R.string.noConnection), 1);
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		});

		mLvStores.setOnScrollListener(new OnScrollListener() {



			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				InorbitLog.d("Scroll State "+mLvStores.getState()+" -- "+scrollState);
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				InorbitLog.d("Pre Visible = "+preVisibleItem+" Firstvisible Visible = "+firstVisibleItem);
				if(preVisibleItem!=firstVisibleItem){
					if((preVisibleItem<firstVisibleItem)){
						//InorbitLog.d("Scrolling down");
						mTvOfferCounter.setVisibility(View.INVISIBLE);
					}else{
						//InorbitLog.d("Scrolling Up");
						mTvOfferCounter.setVisibility(View.VISIBLE);
						mTvOfferCounter.bringToFront();

					}
				}

				preVisibleItem = firstVisibleItem;
				if(preVisibleItem==0 && firstVisibleItem==0){
					mTvOfferCounter.setVisibility(View.INVISIBLE);
				}
			}
		});

	}

	void initViews(){
		//mEtSearchText 		= (EditText) findViewById(R.id.searchText_offers);
		mProg				= (ProgressBar)findViewById(R.id.searchCategListProg_offers);
		mTvOfferCounter		= (TextView)findViewById(R.id.txt_total_offers);
		//mTvCounterBack		= (TextView)findViewById(R.id.txtCategSearchCounter_offers);
		//mIvSearch 			= (ImageView) findViewById(R.id.img_serach_offers);
		//mIvConnctionErr		= (ImageView) findViewById(R.id.img_connnection_error_offers);
		//mIvConnctionErr.setVisibility(View.GONE);
		mLvStores			= (PullToRefreshListView)findViewById(R.id.listCategSearchResult_offers);

		//mLinearSearch		= (LinearLayout) findViewById(R.id.searchBox_offers);
		//mTvOfferCounter.setVisibility(View.GONE);

	}

	void initActionBar(){
		mActionbar	= getSupportActionBar();
		mActionbar.setTitle(getString(R.string.actionBarTitle));
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.show();
	}

	void initObjects(){
		mDbutil 	= new DBUtil(mContext);
		mInorbitDb  = InobitStoresDb.getInstance(mContext);

		mSession	= new SessionManager(getApplicationContext());
		mArrMallAreas	= new ArrayList<String>();
		mArrStores		= new ArrayList<StoreInfo>();
		mArrOFFERS		= new ArrayList<PostDetail>();
		session			=	new SessionManager(mContext);

	}

	void creatActionBarList(){
		mArrMallAreas = mDbutil.getAllAreas();
		ActionAdapter mAdAction = new ActionAdapter(mContext, 0, 0, mArrMallAreas);
		mActionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		mActionbar.setListNavigationCallbacks(mAdAction, this);
		mActionbar.setSelectedNavigationItem(mArrMallAreas.indexOf(getActiveAreaName()));
	}


	String getActiveAreaId(){
		return mDbutil.getActiveMallId();
	}

	String getActiveAreaName(){
		return mDbutil.getAreaNameByInorbitId(getActiveAreaId());
	}

	String getMallName(){
		return mArrMallAreas.get((mActionbar.getSelectedNavigationIndex())+1);
	}

	String getCurrentMallId(){
		return mDbutil.getMallIdByPos(""+((mActionbar.getSelectedNavigationIndex())+1));
	}

	String getCurrentMallPlaceParent(){
		return mDbutil.getMallPlaceParentByMallID(getCurrentMallId());
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		String value = mDbutil.getMallIdByPos(""+(itemPosition+1));
		mDbutil.getMallCity(value);
		mDbutil.setActiveMall(value);
		mArrOFFERS.clear();
		//getOffersOfMall();

		/*if(NetworkCheck.isNetConnected(mContext)) {
			getOffersOfStores();
		}*/
		return false;
	}



	void getOffersOfStores(){
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		if(session.isLoggedInCustomer()) {
			//headers.put(USRE, API_VALUE);
			HashMap<String,String>user_details=session.getUserDetailsCustomer();
			String msUserID=user_details.get("user_id_CUSTOMER").toString();
			headers.put("user_id", msUserID);
		}else{

		}
		List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("place_id", storeInfo.getId()));

		MyClass myClass  = new MyClass(mContext);
		//myClass.getStoreRequest(RequestTags.TagViewBroadcast, nameValuePairs, headers);
		myClass.getStoreRequest(RequestTags.TagViewBroadcast_c, nameValuePairs, headers);
	}


	class StoresOffer extends BroadcastReceiver{

		

		@Override
		public void onReceive(final Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				mProg.setVisibility(View.GONE);
				mLvStores.onRefreshComplete();
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String success = bundle.getString("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						String total_pages = bundle.getString("TOTAL_PAGE");
						String total_record = bundle.getString("TOTAL_RECORD");
						bundle = intent.getBundleExtra("BundleData");
						if(bundle!=null){
							ArrayList<PostDetail> postArr = bundle.getParcelableArrayList("POST_DETAILS");
							mArrOFFERS = postArr;
							/*for(int i=0;i<arrOFFERS.size();i++){
								InorbitLog.d("Post "+arrOFFERS.get(i).getTitle());
							}*/
							
							mLvStores.setAdapter(new OffersAdapter((Activity) context, mArrOFFERS));

							/*PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, pages);
							mPager.setAdapter(adapter);
							mPager.setOffscreenPageLimit(pages.size());
							pagerTabs.setViewPager(mPager);
							pagerTabs.setTextColor(Color.BLACK);
							pagerTabs.setBackgroundColor(Color.TRANSPARENT);*/

						}
					}else{
						String message = bundle.getString("MESSAGE");
						//showToast(message);
						Toast.makeText(mContext, message,Toast.LENGTH_LONG).show();

						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){

							int code1 = bundle.getInt("CODE",0);
							String message1=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "View Post");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();

						}

					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			//IntentFilter offersFilter = new IntentFilter(RequestTags.TagViewBroadcast_mall);
			IntentFilter offersFilter = new IntentFilter(RequestTags.TagViewBroadcast_c);

			mContext.registerReceiver(offersBroadCast = new StoresOffer(), offersFilter);

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try{
			EventTracker.endLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}



	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		try{
			EventTracker.endFlurrySession(getApplicationContext());
			if(offersBroadCast!=null){
				mContext.unregisterReceiver(offersBroadCast);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		finishTo();
		return true;
	}

	void finishTo(){
		Intent intent = new Intent();
		setResult(5, intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		InorbitLog.d("Offers Back Press");
		finishTo();
	}

	void uploadBroadcastEvent(String title,String StoreName){
		try{
			//InorbitLog.d("Broadcast  MallName - "+MALL_NAME + " StroeName "+REC_SOTRE_NAME );
			boolean param  = false;
			flurryEeventParams = new HashMap<String, String>();
			if(getActiveAreaName()!=null && !getActiveAreaName().equals("")){
				flurryEeventParams.put("MallName", getActiveAreaName());
				param  = true;
			}else{
				param = false;
			}
			if(StoreName!=null && !StoreName.equals("")){
				flurryEeventParams.put("StoreName", StoreName +" - "+getActiveAreaName());
				param  = true;
			}else{
				param = false;
			}
			if(title!=null && !title.equals("")){
				if(param){
					param  = true;
					flurryEeventParams.put("BroadCastTitle", title +" - "+ StoreName +" - "+getActiveAreaName());
				}else{
					param  = false;
				}
			}
			if(param){
				EventTracker.logEvent(getResources().getString(R.string.offerDetail_View), flurryEeventParams);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}



}

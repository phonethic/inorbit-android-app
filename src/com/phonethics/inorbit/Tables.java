package com.phonethics.inorbit;

public class Tables {
	
	
	/**
	 * Fields for Table "Stores"
	 */
	public static final String TABLE_STORES 	= "Stores";
	public static final String ID 				= "id";
	public static final String STORE_ID 		= "store_id";
	public static final String STORE_NAME 		= "store_name";
	public static final String STORE_MALL_ID 	= "store_mall_id";
	public static final String STORE_PLACEPARENT = "store_place_parent";
	public static final String STORE_LOGO 		= "store_logo";
	public static final String STORE_DESC 		= "store_desc";
	public static final String STORE_LANDMARK 	= "store_landmark";
	public static final String STORE_AREA		= "store_area";
	public static final String STORE_CITY 		= "store_city";
	public static final String STORE_BUILDING 	= "store_building";
	public static final String STORE_STREET 	= "store_street";
	public static final String STORE_MOB_NO_1 	= "store_mob_no_1";
	public static final String STORE_MOB_NO_2 	= "store_mob_no_2";
	public static final String STORE_MOB_NO_3 	= "store_mob_no_3";
	public static final String STORE_LAND_NO_1 	= "store_land_no_1";
	public static final String STORE_LAND_NO_2 	= "store_land_no_2";
	public static final String STORE_LAND_NO_3 	= "store_land_no_3";
	public static final String STORE_TOLL_FREE_NO_1 = "toll_free_no_1";
	public static final String STORE_TOLL_FREE_NO_2 = "toll_free_no_2";
	public static final String STORE_WEBSITE 	= "store_website";
	public static final String STORE_FACEBOOK_URl 	= "store_facebook_url";
	public static final String STORE_TWITTER_URl 	= "store_twitter_url";
	public static final String STORE_WEBSITE_URl 	= "store_website_url";
	public static final String STORE_EMAIL 			= "store_email";
	public static final String STORE_TOTAL_LIKE 	= "store_total_like";
	public static final String STORE_TOTAL_RATING	= "store_total_rating";
	
	public static final String STORE_HAS_OFFER 		= "store_has_offer";
	public static final String STORE_OFFER_TITLE	= "store_offer_title";
	public static final String STORE_CATEGORY 		= "store_category";
	public static final String STORE_SUB_CATEGORY 	= "store_sub_category";
	
	
	public static final String CATEGORY_All 			= "5";
	public static final String CATEGORY_SHOP 			= "1";
	public static final String CATEGORY_DINE 			= "2";
	public static final String CATEGORY_ENTERTAINMENT 	= "3";
	public static final String CATEGORY_PAMPERZONE 		= "4";
	public static final String CATEGORY_OFFER 			= "6";
	
	
	public static String PRESENT_CATEGORY 				= CATEGORY_All;
	private  static String PRESENT_Category_For_Event	= "Shop";
	
	
	public static final String TABLE_Refersh 	= "Refresh_Data";
	public static final String KEY_Mall_Id 		= "mall_id";
	public static final String KEY_Cat_All_oldValue 			= "cat_all_oldValue";
	public static final String KEY_Cat_Shop_oldValue 			= "cat_shop_oldValue";
	public static final String KEY_Cat_Dine_oldValue 			= "cat_dine_oldValue";
	public static final String KEY_Cat_Entertainment_oldValue 	= "cat_entertainment_oldValue";
	public static final String KEY_Cat_Pamper_oldValue 			= "cat_pamper_oldValue";
	public static final String KEY_Cat_All_newValue 			= "cat_all_newValue";
	public static final String KEY_Cat_Shop_newValue 			= "cat_shop_newValue";
	public static final String KEY_Cat_Dine_newValue 			= "cat_dine_newValue";
	public static final String KEY_Cat_Entertainment_newValue 	= "cat_entertainment_newValue";
	public static final String KEY_Cat_Pamper_newValue 			= "cat_pamper_newValue";
	
	public static final String KEY_Cat_All_OfferCount_oldValue 			= "cat_all_OfferCount_oldValue";
	public static final String KEY_Cat_All_OfferCount_newValue 			= "cat_all_OfferCount_newValue";
	public static final String KEY_Cat_Shop_OfferCount_oldValue 		= "cat_shop_OfferCount_oldValue";
	public static final String KEY_Cat_Shop_OfferCount_newValue 		= "cat_shop_OfferCount_newValue";
	public static final String KEY_Cat_Dine_OfferCount_oldValue 		= "cat_dine_OfferCount_oldValue";
	public static final String KEY_Cat_Dine_OfferCount_newValue 		= "cat_dine_OfferCount_newValue";
	public static final String KEY_Cat_Entertainment_OfferCount_oldValue	= "cat_entertainment_OfferCount_oldValue";
	public static final String KEY_Cat_Entertainment_OfferCount_newValue	= "cat_entertainment_OfferCount_newValue";
	public static final String KEY_Cat_Pamper_OfferCount_oldValue 		= "cat_pamper_OfferCount_oldValue";
	public static final String KEY_Cat_Pamper_OfferCount_newValue 		= "cat_pamper_OfferCount_newValue";
	
	
	public static boolean mbIsMerchantLoggedInNow = false; 
	
	public static boolean mbNeedToCheckCount = true;
	
	
	public static String getCurrentCategory(){
		return PRESENT_CATEGORY;
	}
	
	public static void setCurrentCategory(String currentCategory){
		PRESENT_CATEGORY = currentCategory;
		if(currentCategory==CATEGORY_All) {
			PRESENT_Category_For_Event = "All";
		}else if(currentCategory==CATEGORY_SHOP){
			PRESENT_Category_For_Event = "Shop";
		}else if(currentCategory==CATEGORY_DINE){
			PRESENT_Category_For_Event = "Dine";
		}else if(currentCategory==CATEGORY_ENTERTAINMENT){
			PRESENT_Category_For_Event = "Entertainment";
		}else if(currentCategory==CATEGORY_PAMPERZONE){
			PRESENT_Category_For_Event = "Pameper Zone";
		}
	}
	
	public static String getCurrentCategoryForEvent(){
		return PRESENT_Category_For_Event;
	}

}

package com.phonethics.inorbit;

import android.os.Parcel;
import android.os.Parcelable;

public class SpecialPostModel implements Parcelable{

	private String place_id="";
	private String post_type="";
	private String date_type="";
	private String title="";
	private String description="";
	private String image_title1="";
	private String image_title2="";
	private String image_title3="";
	private String image_url1="";
	private String image_url2="";
	private String image_url3="";
	private String thumb_url1="";
	private String thumb_url2="";
	private String thumb_url3="";
	private String special_date="";
	private String offer_date_time="";
	private String created_at="";
	
	
	public String getPlace_id() {
		return place_id;
	}
	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}
	public String getPost_type() {
		return post_type;
	}
	public void setPost_type(String post_type) {
		this.post_type = post_type;
	}
	public String getDate_type() {
		return date_type;
	}
	public void setDate_type(String data_type) {
		this.date_type = data_type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage_title1() {
		return image_title1;
	}
	public void setImage_title1(String image_title1) {
		this.image_title1 = image_title1;
	}
	public String getImage_title2() {
		return image_title2;
	}
	public void setImage_title2(String image_title2) {
		this.image_title2 = image_title2;
	}
	public String getImage_title3() {
		return image_title3;
	}
	public void setImage_title3(String image_title3) {
		this.image_title3 = image_title3;
	}
	public String getImage_url1() {
		return image_url1;
	}
	public void setImage_url1(String image_url1) {
		this.image_url1 = image_url1;
	}
	public String getImage_url2() {
		return image_url2;
	}
	public void setImage_url2(String image_url2) {
		this.image_url2 = image_url2;
	}
	public String getImage_url3() {
		return image_url3;
	}
	public void setImage_url3(String image_url3) {
		this.image_url3 = image_url3;
	}
	public String getThumb_url1() {
		return thumb_url1;
	}
	public void setThumb_url1(String thumb_url1) {
		this.thumb_url1 = thumb_url1;
	}
	public String getThumb_url2() {
		return thumb_url2;
	}
	public void setThumb_url2(String thumb_url2) {
		this.thumb_url2 = thumb_url2;
	}
	
	
	public String getThumb_url3() {
		return thumb_url3;
	}
	public void setThumb_url3(String thumb_url3) {
		this.thumb_url3 = thumb_url3;
	}
	public String getSpecial_date() {
		return special_date;
	}
	public void setSpecial_date(String special_date) {
		this.special_date = special_date;
	}
	public String getOffer_date_time() {
		return offer_date_time;
	}
	public void setOffer_date_time(String offer_date_time) {
		this.offer_date_time = offer_date_time;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
	}
	
}

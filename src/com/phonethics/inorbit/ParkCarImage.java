package com.phonethics.inorbit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class ParkCarImage extends SherlockFragmentActivity {

	ActionBar actionbar;

	ImageView parkedCarImage;
	String ImageUri = "";
	
	Context context;
	Activity con;
	
	int deviceWidth;

	String IMG_PATH = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_park_car_image);

		con = this;
		context = this;
		actionbar=getSupportActionBar();
		actionbar.setTitle(getString(R.string.actionBarTitle));
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();

		deviceWidth = con.getWindowManager().getDefaultDisplay().getWidth();
		parkedCarImage = (ImageView) findViewById(R.id.parkedCarImage);



		try {

			Bundle b = getIntent().getExtras();
			ImageUri = b.getString("URI");
			IMG_PATH = b.getString("PATH");

//			File file = new File(ImageUri);
//
//			Bitmap bmp = decodeFile(file);
			
			Bitmap bmp = getNewBitmap(Uri.parse(ImageUri), 480);
			
			/** change by anirudh */
			parkedCarImage.setImageBitmap(rotateImage(bmp,IMG_PATH));

//			if(bmp!=null){
//				
//				Toast.makeText(context, "Valid bitmap",0).show();
//			}
//			else{
//				
//				Toast.makeText(context, "No bitmap",0).show();
//			}
			
//			parkedCarImage.setImageBitmap(bmp);


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


	}

	private Bitmap decodeFile(File f){

		try {


			//Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			o.inSampleSize = 8;
			BitmapFactory.decodeStream(new FileInputStream(f),null,o);

			//The new size we want to scale to
			final int REQUIRED_SIZE=70;

			//Find the correct scale value. It should be the power of 2.
			int scale=1;
			while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
				scale*=2;

			//Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {}
		return null;
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		this.finish();

		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

		return true;

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*finishTo();*/
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}
	
	
	
	//	Use this code to downsample images
	//  Alternative for the above code
	
	//Down scaling bitmap to get Thumbnail
		public  Bitmap getNewBitmap(Uri uri,int dimensions) throws FileNotFoundException, IOException{
			
			InputStream input = getApplicationContext().getContentResolver().openInputStream(uri);

			BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
			onlyBoundsOptions.inJustDecodeBounds = true;
			onlyBoundsOptions.inDither=true;//optional
			onlyBoundsOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional
			BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
			input.close();
			if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
				return null;

			int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

			double ratio = (originalSize > dimensions) ? (originalSize / dimensions) : 1.0;

			BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
			bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
			bitmapOptions.inDither=true;//optional
			bitmapOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional


			input = getApplicationContext().getContentResolver().openInputStream(uri);
			Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);

			input.close();
			return bitmap;
		}

		private static int getPowerOfTwoForSampleRatio(double ratio){
			int k = Integer.highestOneBit((int)Math.floor(ratio));
			if(k==0) return 2;
			else return k;
		}

	
		private Bitmap rotateImage(Bitmap bmp, String path) {
			// TODO Auto-generated method stub
			
			
			Matrix matrix=new Matrix();

			ExifInterface exif = null;
			try {
				exif = new ExifInterface(path);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String orientstring = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
			int orientation = orientstring != null ? Integer.parseInt(orientstring) : ExifInterface.ORIENTATION_NORMAL;
			int rotateangle = 0;
			if(orientation == ExifInterface.ORIENTATION_ROTATE_90) 
				rotateangle = 90;
			if(orientation == ExifInterface.ORIENTATION_ROTATE_180) 
				rotateangle = 180;
			if(orientation == ExifInterface.ORIENTATION_ROTATE_270) 
				rotateangle = 270;

			//imageView.setScaleType(ScaleType.CENTER_CROP);   //required
			matrix.setRotate(rotateangle);
//			imageView.setImageMatrix(matrix);



			Bitmap newBit = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

			//imageView.setImageBitmap(newBit);
			
			return newBit;

		}
	
}

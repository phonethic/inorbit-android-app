package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.model.AllMessagesModel;
import com.phonethics.model.RequestTags;
import com.squareup.picasso.Picasso;

public class InboxStoreList extends SherlockActivity {

	private Activity mAcontext;
	private Context mContext;
	private ActionBar mActionBar;
	private ListView mStoreList;
	RelativeLayout mProgressBar;
	Button mNewMessage;
	String place_id="";
	DBUtil dbUtil;
	AllMallMessages reciverObj;
	MessageListAdapter mAdapter;
	ArrayList<AllMessagesModel> mArrModel = new ArrayList<AllMessagesModel>();
	int REQUEST_CODE = 10;
	SessionManager mSessionManager;
	Handler handler;
	Runnable runnable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inbox_store_list);
		mSessionManager = new SessionManager(this);
		
		mAcontext = this;
		mContext = this;
		dbUtil = new DBUtil(mContext);
		mActionBar	=	getSupportActionBar();
		mActionBar.setTitle("Inbox");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();

		mProgressBar = (RelativeLayout)findViewById(R.id.mProgressBar);
		mNewMessage = (Button)findViewById(R.id.mNewMessage);
		mStoreList = (ListView)findViewById(R.id.mStoreList);
		place_id = dbUtil.getPlaceIdByPlaceParent("0");
		mActionBar.setTitle("Inorbit Mall " + dbUtil.getMallNameById(place_id));
		
		InorbitLog.d("Mall_ID " + place_id);
		getAllMessages();
		
		mNewMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(mContext, InboxListSelector.class);
				startActivity(intent);
			}
		});
		
		mStoreList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				//to set message count as 0
				mArrModel.get(position).setCount("0");
				
				Intent intent = new Intent(mContext, InboxGetMessage.class);
				if(place_id.equalsIgnoreCase(mArrModel.get(position).getFrom())) {
					intent.putExtra("READER",mArrModel.get(position).getFrom());
					intent.putExtra("SENDER",mArrModel.get(position).getTo());
					intent.putExtra("SENDER_NAME", mArrModel.get(position).getTo_store_name());
					intent.putExtra("PLACE_ID", place_id);
					intent.putExtra("FROM_IMG", (getResources().getString(R.string.photo_url))+mArrModel.get(position).getFrom_image_url());
					intent.putExtra("TO_IMG", (getResources().getString(R.string.photo_url))+mArrModel.get(position).getTo_image_url());
				} else {
					intent.putExtra("READER",mArrModel.get(position).getTo());
					intent.putExtra("SENDER",mArrModel.get(position).getFrom());
					intent.putExtra("SENDER_NAME", mArrModel.get(position).getFrom_store_name());
					intent.putExtra("PLACE_ID", place_id);
					intent.putExtra("FROM_IMG", (getResources().getString(R.string.photo_url))+mArrModel.get(position).getTo_image_url());
					intent.putExtra("TO_IMG", (getResources().getString(R.string.photo_url))+mArrModel.get(position).getFrom_image_url());
				}
				startActivityForResult(intent, REQUEST_CODE);
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		setResult(RESULT_OK);
		finish();
		return true;
	}

	/**
	 * Get the user id of the merchant
	 * 
	 * @return String : user id
	 */
	String getUserId(){
		HashMap<String,String>user_details = mSessionManager.getUserDetails();
		return user_details.get("user_id").toString();

	}

	/**
	 * Get the auth id of the merchant
	 * 
	 * @return String : Auth id
	 */
	String getAuthId(){
		HashMap<String,String>user_details = mSessionManager.getUserDetails();
		return user_details.get("auth_id").toString();
	}
	
	void getAllMessages() {
		// TODO Auto-generated method stub
		//mProgressBar.setVisibility(View.VISIBLE);

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		headers.put("auth_id", getAuthId());
		headers.put("user_id", getUserId());

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("reader_id", place_id));
		MyClass myclass = new MyClass(mContext);
		myclass.getStoreRequest(RequestTags.TAG_GET_MALL_CONVERSATIONS, nameValuePairs, headers);
	}
	
	class AllMallMessages extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {
				mProgressBar.setVisibility(View.GONE);
				if(intent!=null){
					
					mArrModel = intent.getParcelableArrayListExtra("ALL_MESSAGES");
					String success = intent.getStringExtra("SUCCESS");
					mProgressBar.setVisibility(View.GONE);
					if(success.equalsIgnoreCase("true")){
						//showtoast("true");
						mAdapter = new MessageListAdapter(mAcontext, R.drawable.ic_launcher, mArrModel);
						mStoreList.setAdapter(mAdapter);
					}else{
						//showtoast("false");
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		IntentFilter filter = new IntentFilter(RequestTags.TAG_GET_MALL_CONVERSATIONS);
		mContext.registerReceiver(reciverObj = new AllMallMessages(), filter);
		
		getTotalMessageCount();
	}

	void showtoast(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(mContext, ""+string, 0).show();
	}

	class MessageListAdapter extends ArrayAdapter<AllMessagesModel> {

		ArrayList<AllMessagesModel> modelArr;
		Activity mContext;
		LayoutInflater inflate;

		public MessageListAdapter(Activity mContext, int resource,ArrayList<AllMessagesModel> modelArr) {
			super(mContext, resource, modelArr);
			// TODO Auto-generated constructor stub

			this.mContext = mContext;
			this.modelArr = modelArr;
			inflate=mContext.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return modelArr.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null){
				final ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.all_messages_layout,null);
				holder.storeLogo = (ImageView) convertView.findViewById(R.id.imgsearchLogo);
				holder.storeTitle=(TextView)convertView.findViewById(R.id.mStoreName);
				holder.msgCount=(TextView)convertView.findViewById(R.id.mMsgCount);
				holder.msg_bubble = (RelativeLayout) convertView.findViewById(R.id.msg_background_img);
				convertView.setTag(holder);
			}
			
			ViewHolder hold=(ViewHolder)convertView.getTag();
			if(place_id.equalsIgnoreCase(mArrModel.get(position).getFrom())){
				hold.storeTitle.setText(modelArr.get(position).getTo_store_name());
			} else {
				hold.storeTitle.setText(modelArr.get(position).getFrom_store_name());
			}
			
			//showToast(modelArr.get(position).getFrom_image_url()+" Img Url");
			
			if (modelArr.get(position).getFrom_image_url()!= null && modelArr.get(position).getFrom_image_url().trim().length() > 0) {
				Picasso.with((Activity)mContext).load(getResources().getString(R.string.photo_url)+modelArr.get(position).getFrom_image_url())
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(hold.storeLogo);
			} else {
				hold.storeLogo.setImageResource(R.drawable.ic_launcher);
			}
			
			hold.msgCount.setText(modelArr.get(position).getCount());
			
			if ((Integer.parseInt(modelArr.get(position).getCount())) > 0)
				hold.msg_bubble.setBackgroundResource(R.drawable.msg_bubble_blue);
			else
				hold.msg_bubble.setBackgroundResource(R.drawable.msg_bubble_grey);
			
			return convertView;
		}

	}
	
	static class ViewHolder{

		TextView storeTitle;
		ImageView storeLogo;
		TextView msgCount;
		RelativeLayout msg_bubble;
	}
	
	@Override
	protected void onStop() {
		try{
			mContext.unregisterReceiver(reciverObj);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		handler.removeCallbacks(runnable);
		super.onStop();
	}

	@Override
	public void onBackPressed() {
		setResult(RESULT_OK);
		finish();
		super.onBackPressed();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==REQUEST_CODE){
			if(resultCode == RESULT_OK){
				//getTotalCount();
				//showToast("Call notify adapter");
//				for(int i=0;i<mArrModel.size();i++){
//					InorbitLog.d("COunt Updated " + mArrModel.get(i).getCount());
//				}
				mAdapter.notifyDataSetChanged();
			}
		}
	}
	
	void showToast(String msg){
		Toast.makeText(mContext, ""+msg, 0).show();
	}
	
	public void getTotalMessageCount() {
		handler = new Handler();
		runnable = new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				try {
					getAllMessages();
					//InorbitLog.d("Called Count Api");
					handler.postDelayed(runnable, 60000);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		};
		handler.postDelayed(runnable,60000);
	}
}

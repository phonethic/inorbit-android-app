package com.phonethics.inorbit;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.AppEventsConstants;
import com.facebook.AppEventsLogger;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.SplashScreen.UdmIDCustomer;
import com.phonethics.model.RequestTags;
//import com.urbanairship.push.PushManager;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager.BadTokenException;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginSigbUpCustomerNew extends SherlockActivity implements OnClickListener,OnTouchListener {

	private TextView mTvWhyLogin, mTvFirstTimeUser, mTvWhyLoginParent;
	private ImageView mIvPlaceHolder;
	private RelativeLayout mRlLoginOption,mRlLogin;
	private Button mBttnLoginFbOption,mBttnLoginInorbitOption,mBtnLoginCustomer,mBtnSignUpCustomer;
	private TextView mTvForgotPassword;	
	private EditText mEtCustomerMobileNum,mEtCustomerPassword;
	private ProgressBar mPbar;
	private Context mContext;
	private ActionBar mActionBar;
	private boolean mbIsInvalidAuth = false;
	private int		miComeBackTo = 0;
	private AlertDialog whyLoginDialog;
	private boolean mbIsFacebookOption;
	protected boolean pendingPublishReauthorization; 
	private String fbUserid;
	private String fbAccessToken;
	private String msUserId;
	private String msAuthId;
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private SessionManager mSessionManager;
	private CustomerLoginBroadcastReciver mbrReceiver;
	private TextView mtvWhySignUp;
	UdmIDCustomer mUdmIdCustomer;
	boolean dontUpdateProfile = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_sigb_up_customer_new);

		mContext = this;

		initView();
		checkKeyHash();
		getBundleValues();
		
		Bundle b = getIntent().getExtras();
		if(b!=null){
			dontUpdateProfile = b.getBoolean("dontUpdateProfile");
		}
	}


	/**
	 * 
	 * Initialize the layout, action bar and class variables
	 * 
	 */
	void initView() {
		mTvWhyLogin	= (TextView)findViewById(R.id.text_why_login);
		mTvFirstTimeUser	= (TextView)findViewById(R.id.text_firstTimeUser);
		mTvWhyLoginParent	= (TextView)findViewById(R.id.text_why_should_login_parent);
		mTvForgotPassword = (TextView)findViewById(R.id.forgotPasswordCustomer);
		mIvPlaceHolder	= (ImageView) findViewById(R.id.placeHolder);
		mIvPlaceHolder.setVisibility(View.INVISIBLE);

		mRlLoginOption	= (RelativeLayout) findViewById(R.id.rel_LoginOptionLayout);
		mRlLogin	= (RelativeLayout) findViewById(R.id.rel_login);

		mBttnLoginFbOption	= (Button) findViewById(R.id.bttn_signUpFb);
		mBttnLoginInorbitOption	= (Button) findViewById(R.id.bttn_signUpInorbit);

		mBttnLoginFbOption.setTypeface(InorbitApp.getTypeFace());
		mBttnLoginInorbitOption.setTypeface(InorbitApp.getTypeFace());

		mBtnLoginCustomer	= (Button)findViewById(R.id.btnLoginCustomer);
		mBtnSignUpCustomer	= (Button)findViewById(R.id.btnSingupCustomer);

		mTvForgotPassword	= (TextView)findViewById(R.id.forgotPasswordCustomer);
		mTvForgotPassword.setText("Forgot Password ?");
		mtvWhySignUp = (TextView)findViewById(R.id.text_why_login_main);
		mEtCustomerMobileNum	= (EditText)findViewById(R.id.edtCustomerMobileNo);
		mEtCustomerPassword	= (EditText)findViewById(R.id.edtCustomerPassword);
		mPbar	= (ProgressBar)findViewById(R.id.progLoginCustomer);
		mTvForgotPassword.setTypeface(InorbitApp.getTypeFace());
		mTvForgotPassword.setTypeface(InorbitApp.getTypeFace());
		mEtCustomerMobileNum.setTypeface(InorbitApp.getTypeFace());
		mTvFirstTimeUser.setTypeface(InorbitApp.getTypeFace());
		mEtCustomerMobileNum.setTextColor(Color.BLACK);
		mEtCustomerPassword.setTextColor(Color.BLACK);
		//mRlLoginOption=(RelativeLayout)findViewById(R.id.signUpLayout);
		mRlLoginOption.setVisibility(View.VISIBLE);
		mRlLogin.setVisibility(View.GONE);

		mSessionManager = new SessionManager(getApplicationContext()); 

		mActionBar =getSupportActionBar();
		mActionBar.setTitle(getResources().getString(R.string.customer)+" login");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();
		
		mTvWhyLogin.setOnClickListener(this);
		mTvWhyLoginParent.setOnClickListener(this);
		mBttnLoginFbOption.setOnClickListener(this);
		mBttnLoginInorbitOption.setOnClickListener(this);
		mBtnLoginCustomer.setOnClickListener(this);
		mtvWhySignUp.setOnClickListener(this);
		mBtnSignUpCustomer.setOnClickListener(this);
		mTvForgotPassword.setOnClickListener(this);

	}

	void checkKeyHash() {
		try {
			PackageInfo info = getPackageManager().getPackageInfo("com.phonethics.inorbit",PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}

	}

	void getBundleValues(){
		Bundle bundle = null;
		try{
			bundle = getIntent().getExtras();
			if(bundle!=null){
				mbIsInvalidAuth  = bundle.getBoolean("isInvalidAuth");
				miComeBackTo  = bundle.getInt("comeBackTo");
				if(mbIsInvalidAuth){
					mActionBar.setDisplayHomeAsUpEnabled(false);
					mActionBar.show();
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==mTvWhyLoginParent.getId()) {
			showWhyIloginDialog();
		}

		if(v.getId()==mTvWhyLogin.getId()) {
			showWhyIloginDialog();
		}

		if(v.getId()==mBttnLoginFbOption.getId()) {
			if (NetworkCheck.isNetConnected(mContext)) {
				//if(validate()) {
				EventTracker.logEvent(getResources().getString(R.string.cSignUp_FBOption), false);
				mbIsFacebookOption = true;
				login_facebook();
				//}
			} else {
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}
		if(v.getId()==mBttnLoginInorbitOption.getId()) {
			mRlLoginOption.setVisibility(View.GONE);
			mRlLogin.setVisibility(View.VISIBLE);
		}

		if(v.getId()==mBtnLoginCustomer.getId()){

			if (NetworkCheck.isNetConnected(mContext)) {
				if(validate()) {
					EventTracker.logEvent(getResources().getString(R.string.cSignup_LoginOption), false);
					loginCustomer();
				}
			} else {
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}

		if(v.getId()==mtvWhySignUp.getId()){
			showWhyIloginDialog();
		}

		if(v.getId()==mBtnSignUpCustomer.getId()){

			EventTracker.logEvent(getResources().getString(R.string.cSignup_SignUpOption), false);
			Intent intent=new Intent(this, InorbitCustomerSignUp.class);
			startActivity(intent);
			//finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(v.getId()==mTvForgotPassword.getId()){

			EventTracker.logEvent(getResources().getString(R.string.cSignup_ForgotPassword), false);
			Intent intent=new Intent(this, InorbitCustomerSignUp.class);
			intent.putExtra("ForgetPassword", 1);
			startActivity(intent);
			//finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
	}

	public void login_facebook(){
		try{
			Session.openActiveSession((Activity) mContext, true, new Session.StatusCallback() {

				@SuppressWarnings("deprecation")
				@Override
				public void call(final Session session, SessionState state, Exception exception) {
					if(session.isOpened()){

						mPbar.setVisibility(View.VISIBLE);
						showToast(getResources().getString(R.string.fbConnecting));
						List<String> permissions = session.getPermissions();
						if (!isSubsetOf(PERMISSIONS, permissions)) {  
							Log.d("INIF","INIF " + "INSIDE IF");
							pendingPublishReauthorization = true;

						}

						Request request = Request.newMeRequest(session,new Request.GraphUserCallback() {
							@Override
							public void onCompleted(GraphUser user, Response response) {
								// TODO Auto-generated method stub
								if(user!=null){
									mPbar.setVisibility(View.GONE);
									try{
										fbUserid = user.getId();
										fbAccessToken=session.getAccessToken();
										Log.d("User Id", "FbName : "+fbAccessToken);
										Log.d("User Id ", "FbId : "+fbUserid);
										mPbar.setVisibility(View.INVISIBLE);
										loginCustomer();
									}catch(Exception ex){
										ex.printStackTrace();
									}
								}else{
									mPbar.setVisibility(View.GONE);
									showToast("Something went wrong");
									EventTracker.logEvent(getResources().getString(R.string.cSignup_LoginFailedFB), true);
								}
							}
						});
						request.executeAsync(); 
					}

					if(session.isClosed()){
						try {
							if ((exception instanceof FacebookOperationCanceledException ||
									exception instanceof FacebookAuthorizationException)) {
								new AlertDialog.Builder(LoginSigbUpCustomerNew.this)
								.setTitle("Login Failed")
								.setMessage(exception.getMessage().toString())
								.setPositiveButton("OK", null)
								.show();
								EventTracker.logEvent(getResources().getString(R.string.cSignup_LoginFailedFB), true);
							}
						} catch (Exception e) {
							// TODO: handle exception
							EventTracker.logEvent(getResources().getString(R.string.cSignup_LoginFailedFB), true);
							e.printStackTrace();
						}		
						Log.d("EXCEPTION","EXCEPTION " + exception);
					}

				}
			});
		} catch(NullPointerException npx){
			EventTracker.logEvent(getResources().getString(R.string.cSignup_LoginFailedFB), true);
			npx.printStackTrace();
		} catch(BadTokenException bdx){
			EventTracker.logEvent(getResources().getString(R.string.cSignup_LoginFailedFB), true);
			bdx.printStackTrace();
		}
		catch(Exception ex){
			EventTracker.logEvent(getResources().getString(R.string.cSignup_LoginFailedFB), true);
			ex.printStackTrace();
		}

	}


	/**
	 * Network request to login, this request will return the user id and authentication code parameter. 
	 * 
	 */
	void loginCustomer(){
		if(NetworkCheck.isNetConnected(mContext)){
			String contact_no = "91"+mEtCustomerMobileNum.getText().toString();
			mPbar.setVisibility(View.VISIBLE);
			JSONObject json = new JSONObject();
			try {
				DBUtil dbUtil = new DBUtil(mContext);
				String msActiveMall = dbUtil.getActiveMallId();

				if(mbIsFacebookOption){
					json.put("facebook_user_id", fbUserid);
					json.put("facebook_access_token", fbAccessToken);
					json.put("active_mall", msActiveMall);

				} else {
					json.put("mobile", contact_no);
					json.put("password", mEtCustomerPassword.getText().toString());
					json.put("active_mall", msActiveMall);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			MyClass myClass = new MyClass(mContext);
			myClass.postRequest(RequestTags.TagLoginCustomer, headers, json);
		}else{
			Toast.makeText(mContext, "No internet connection.", Toast.LENGTH_SHORT).show();
		}
	}

	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 * Broadcast receiver for the login_customer()
	 * 
	 * @author Nitin
	 *
	 */
	class CustomerLoginBroadcastReciver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {

			try{
				InorbitLog.d("Result code receiver "+2);
				Bundle bundle = intent.getExtras();
				String success   = bundle.getString("SUCCESS");
				String message   = bundle.getString("MESSAGE");

				if(success.equalsIgnoreCase("true")){

					String id = bundle.getString("ID");
					String auth = bundle.getString("AUTH_ID");

					msUserId =	id;
					msAuthId =	auth;
					mPbar.setVisibility(View.INVISIBLE);

					if(!mbIsFacebookOption){

						mSessionManager.createLoginSessionCustomer("91"+mEtCustomerMobileNum.getText().toString(), mEtCustomerPassword.getText().toString(), msUserId, msAuthId);
						AppEventsLogger logger = AppEventsLogger.newLogger(context);
						Bundle parameters = new Bundle();
						parameters.putString("User Type", "Customer");
						parameters.putString("Login Type", "Mobile");
						logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION,parameters);
						EventTracker.logEvent(getResources().getString(R.string.cSignup_InorbitLoginSuccess), false);

					}else{

						mSessionManager.createLoginSessionCustomer("", "", msUserId, msAuthId);

						AppEventsLogger logger = AppEventsLogger.newLogger(context);

						Bundle parameters = new Bundle();
						parameters.putString("User Type", "Customer");
						parameters.putString("Login Type", "Facebook");

						logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION,parameters);
						EventTracker.logEvent(getResources().getString(R.string.cSignup_LoginSuccessFB), false);
					}

					if(mbIsInvalidAuth){

						if(miComeBackTo == Conifg.intForMe){
							Intent intent1 = new Intent(context, ForMe.class);
							startActivity(intent1);
							finish();
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

						}else if(miComeBackTo == Conifg.intMerchantFavouriteStore){
							Intent intent2 = new Intent(context, UserFavouritesStores.class);
							startActivity(intent2);
							finish();
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					}
					//tagCustomer(id);????UrbanAirShip

					//To get UDM_ID while customer login
					//call with the user_id
					callToGetUdmIdForCustomer(msUserId,"0");
					
				}else{

					mPbar.setVisibility(View.INVISIBLE);

					MyToast.showToast((Activity) context, message);
					EventTracker.logEvent(getResources().getString(R.string.cSignup_InorbitLoginFailed),false);

					boolean isVolleyError = bundle.getBoolean("volleyError");
					if(isVolleyError){

						int code1 = bundle.getInt("CODE");
						String message1=bundle.getString("MESSAGE");
						String errorMessage = bundle.getString("errorMessage");
						String apiUrl = bundle.getString("apiUrl");	

						if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
							EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "LoginCustomer");
						}
						Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	} 

	/**
	 * 
	 * Show the prompt why user should login ?
	 * 
	 */
	private void showWhyIloginDialog(){
		Builder builder = new AlertDialog.Builder(mContext);
		View view  = ((Activity) mContext).getLayoutInflater().inflate(R.layout.why_login_dialog, null);
		TextView txt_whylogin = (TextView) view.findViewById(R.id.text_why_login);
		Button bttn = (Button) view.findViewById(R.id.btn_ok);
		txt_whylogin.setTypeface(InorbitApp.getTypeFace());
		bttn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				whyLoginDialog.dismiss();
			}
		});
		builder.setView(view);
		whyLoginDialog = builder.create();
		whyLoginDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		whyLoginDialog.show();
	}

	void showToast(String text){
		Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onBackPressed() {
		if(mRlLogin.isShown()) {
			mRlLoginOption.setVisibility(View.VISIBLE);
			mRlLogin.setVisibility(View.GONE);
		}else{
			finishTo();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(mRlLogin.isShown()) {
			mRlLoginOption.setVisibility(View.VISIBLE);
			mRlLogin.setVisibility(View.GONE);
		}else{
			finishTo();
		}
		return true;
	}

	void finishTo(){
		Intent intent=new Intent();
		setResult(2,intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

		/*void tagCustomer(String customerId){
			try{
				
				Set<String> tags = new HashSet<String>();
				tags=PushManager.shared().getTags();
				InorbitLog.d("Customer Tag -- "+tags.toString());
				tags.add("customer_id_"+customerId);
				PushManager.shared().setTags(tags);
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}*/

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter filter = new IntentFilter(RequestTags.TagLoginCustomer);
			mbrReceiver = new CustomerLoginBroadcastReciver();
			mContext.registerReceiver(mbrReceiver, filter);

			//for udmid customer
			IntentFilter mCustomer	= new IntentFilter(RequestTags.TAG_GET_UDM_ID_CUSTOMER);
			mContext.registerReceiver(mUdmIdCustomer = new UdmIDCustomer(), mCustomer);
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		InputMethodManager imm = (InputMethodManager)getSystemService(
			      Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mEtCustomerPassword.getWindowToken(), 0);
		
		EventTracker.endLocalyticsSession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(getApplicationContext());
		try{
			if(mbrReceiver!=null){
				mContext.unregisterReceiver(mbrReceiver);
			}
			if(mUdmIdCustomer!=null){
				mContext.unregisterReceiver(mUdmIdCustomer);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onStop();
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		try {
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		} catch (Exception e) {

		}

	}

	/**
	 * Validate all the feilds entered by the user like phone number, password.
	 * 
	 * @return boolean True if all the values are proper | false if any of the value entered is incorrect.
	 */
	private boolean validate(){

		if (mEtCustomerMobileNum.getText().toString().length()==0) {
			Toast.makeText(mContext, "Please enter Mobile no.", Toast.LENGTH_SHORT).show();
			return false;
		} else if (mEtCustomerMobileNum.getText().toString().length()<10) {
			Toast.makeText(mContext, "Please enter correct Mobile no.", Toast.LENGTH_SHORT).show();
			return false;
		} else if (mEtCustomerPassword.getText().toString().length()==0) {
			Toast.makeText(mContext, "Please enter password", Toast.LENGTH_SHORT).show();
			return false;
		} else if (mEtCustomerPassword.getText().toString().length()<6) {
			Toast.makeText(mContext, "Password must be atleast 6 character's long", Toast.LENGTH_SHORT).show();
			return false;
		} else {
			return true;
		}
	}

	//method to get UDM_ID
	private void callToGetUdmIdForCustomer(String user_id, String isAdmin) {
		// TODO Auto-generated method stub
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		JSONObject jsonObj = new JSONObject();
		try {

			String regid = mSessionManager.getRegistrationId();
			jsonObj.put("device_id", regid);
			jsonObj.put("device_os", "Android");
			jsonObj.put("user_id", user_id);
			jsonObj.put("is_admin", isAdmin);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		InorbitLog.d("JSON " + jsonObj.toString());

		MyClass myclass = new MyClass(mContext);
		myclass.postRequest(RequestTags.TAG_GET_UDM_ID_CUSTOMER, headers, jsonObj);
	}

	class UdmIDCustomer extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						String udm_id_customer = intent.getStringExtra("UDMID");
						InorbitLog.d("ID "+ udm_id_customer);
						mSessionManager.setUdmIdForCustomer(udm_id_customer);

						//showToast(udm_id_customer);
						if (dontUpdateProfile) {
							finish();
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						} else {
							Intent intent1=new Intent(context,CustomerProfile.class);
							startActivityForResult(intent1, 2);
							finish();
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					}
					else{
						String message = intent.getStringExtra("MESSAGE");
						showToast(message);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}
}

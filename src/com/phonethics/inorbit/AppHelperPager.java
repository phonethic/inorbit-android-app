package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.phonethics.inorbit.HomeGrid.GridViewLayoutAdapter;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class AppHelperPager extends SherlockFragmentActivity implements OnClickListener {

	private ViewPager mViewPager;
	private Context	mContext;
	private static ArrayList<Integer> miarrDrawables;
	private static ArrayList<String> msHelperText;
	private RelativeLayout mrlParentLayout,mrlHelperLayout;
	private int miParentBack;
	private ActionBar mAcationBar;
	private String msActionTitle;
	private TextView mTvHelperMessage;
	private Button mbtnPrev;
	private Button mbtnNext;
	private boolean mbIsSkipped = false;
	private boolean mbIsTour = true;
	private ImageView mImvImg_front;
	private ImageView mImvImg_back;
	private Animation manimFadein;
	private Animation manimFadeOut;
	private Handler mHandler_anim;
	private Runnable mRunnable_anim;
	private int miImageCount = 0 ;
	private LinearLayout mllHelperLayout;
	private View[] viewIndicator;
	private boolean mbClickedNext = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_app_helper_pager);

		mContext = this;
		initViews();
		reciveBundleValues();
		setActionBar();
		//startAnim();
		animateLayouts();
		//initPager();
	}


	private void initViews(){
		mViewPager = (ViewPager)findViewById(R.id.viewCustomerPager);
		mTvHelperMessage = (TextView) findViewById(R.id.text_helperMessage);
		mbtnPrev = (Button) findViewById(R.id.button_prev);
		mbtnNext = (Button) findViewById(R.id.button_next);
		mImvImg_front	= (ImageView) findViewById(R.id.img_front);
		mImvImg_back	= (ImageView) findViewById(R.id.img_back);
		//mImvImg_front.setScaleType(ScaleType.FIT_XY);
		//mImvImg_flip_1.setScaleType(ScaleType.FIT_XY);
		miarrDrawables = new ArrayList<Integer>();
		msHelperText = new ArrayList<String>();
		mrlParentLayout = (RelativeLayout)findViewById(R.id.rel_help_parent);
		mrlHelperLayout = (RelativeLayout)findViewById(R.id.rel_parent_layout);
		mllHelperLayout = (LinearLayout)findViewById(R.id.linear_helper_indicator);
		manimFadein 	= AnimationUtils.loadAnimation(mContext, R.anim.fade_in_splash);
		manimFadeOut 	= AnimationUtils.loadAnimation(mContext, R.anim.fade_out_splash);
		mrlHelperLayout.setVisibility(View.INVISIBLE);
		mTvHelperMessage.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
		mTvHelperMessage.setText("Want to understand what is this about ?");
		mbtnPrev.setVisibility(View.VISIBLE);
		mbtnNext.setVisibility(View.VISIBLE);
		mbtnPrev.setText("Skip it for now");
		mbtnNext.setText("Next");
		mbtnPrev.setOnClickListener(this);
		mbtnNext.setOnClickListener(this);

	}

	private void reciveBundleValues(){
		Bundle mBundle = getIntent().getExtras();
		if(mBundle!=null) {
			miarrDrawables = mBundle.getIntegerArrayList("Helper_Images_Arr");
			msHelperText = mBundle.getStringArrayList("Helper_Text");
			miParentBack = mBundle.getInt("Parent_Back");
			msActionTitle = mBundle.getString("Title");
			viewIndicator = new View[miarrDrawables.size()];
			LayoutParams params=new LayoutParams(LayoutParams.MATCH_PARENT, 4,1f);
			for(int i=0;i<miarrDrawables.size();i++){

				viewIndicator[i] = new View(mContext);
				viewIndicator[i].setBackgroundColor(mContext.getResources().getColor(R.color.inorbit_yellow));
				viewIndicator[i].setLayoutParams(params);
				mllHelperLayout.addView(viewIndicator[i]);
			}
		}
	}

	private void setActionBar(){
		mAcationBar		=	getSupportActionBar();
		mAcationBar.setTitle(msActionTitle);
		mAcationBar.setDisplayHomeAsUpEnabled(true);
		mAcationBar.show();
	}
	private void animateLayouts(){
		final Animation manimSlideIn = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_from_top);
		Animation manimFadeIn = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
		mrlParentLayout.setBackgroundResource(miParentBack);
		mrlParentLayout.startAnimation(manimFadeIn);
		manimFadeIn.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				mrlHelperLayout.startAnimation(manimSlideIn);


			}
		});
		manimSlideIn.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				mrlHelperLayout.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				//initPager();
				startAnim();
			}
		});
	}

	private void initPager(){

		//miarrDrawables.add(R.drawable.menu_bg_orange);
		//miarrDrawables.add(R.drawable.menu_bg_orange_1);
		//miarrDrawables.add(R.drawable.menu_bg_orange_2);
		//mrlParentLayout.setBackgroundResource(miParentBack);
		//Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_from_top);
		//mrlHelperLayout.startAnimation(animation);

		//PageAdapter adapter = new PageAdapter(getSupportFragmentManager(), getFragments());
		//mViewPager.setAdapter(adapter);
		Animation manimFadeIn = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
		mViewPager.startAnimation(manimFadeIn);
	}

	class PageAdapter extends FragmentStatePagerAdapter{

		Activity context;
		ArrayList<String> pages;
		List<Fragment> fragments;

		public PageAdapter(FragmentManager fm,List<Fragment> fragments) {
			super(fm);
			this.fragments = fragments;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return fragments.size();
		}

	}


	private List<Fragment> getFragments(){
		List<Fragment> fList = new ArrayList<Fragment>();
		for(int i=0;i<miarrDrawables.size();i++){
			fList.add(PageFragment.newInstance(i));
		}
		return fList;
	}

	public  static class PageFragment extends Fragment{

		private View mView;
		private ImageView mivHelperImage;
		private int miPos;
		private static int miImageId;

		public PageFragment(){

		}


		public static final PageFragment newInstance(int pos){

			PageFragment pageFragment = new PageFragment();
			Bundle bdl=new Bundle(1);
			bdl.putInt("PagePosition", pos);
			pageFragment.setArguments(bdl);
			//miImageId = imageId;
			return pageFragment;

		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

			try{
				this.miPos = getArguments().getInt("PagePosition");
				mView	= inflater.inflate(R.layout.app_helper, null);
				mivHelperImage	= (ImageView) mView.findViewById(R.id.img_helper);
				mivHelperImage.setImageResource(miarrDrawables.get(this.miPos));

			}catch(Exception ex){
				ex.printStackTrace();
			}
			return mView;
		}
	}


	@Override
	public void onClick(View v) {
		try{
			if(v.getId()==mbtnPrev.getId()){/*
				mbClickedNext  = false;
				if(!mbIsSkipped && !mbIsTour){
					finish();
				} else if(mbIsTour){
					if(miImageCount==0){
						finish();
					}
					
					if(miImageCount<=0){
						miImageCount = 0;
						mbtnPrev.setText("Skip now");
						mbtnNext.setText("Next >");
						startAnim();
					} else {
						miImageCount--;
						if(miImageCount==(miarrDrawables.size()-1)){
							miImageCount--;
						}
						mbtnPrev.setText("< Prev");
						mbtnNext.setText("Next >");
						startAnim();
					}


				}
			*/
				
			
				if(miImageCount==0){
					finish();
				} else {
					miImageCount--;
					if(miImageCount==0){
						mbtnPrev.setText("Skip");
						mbtnNext.setText("Next >");
						startAnim();
					} else {
						mbtnPrev.setText("< Prev");
						mbtnNext.setText("Next >");
						startAnim();
					}
					
				}
				
				
			
			}

			if(v.getId()==mbtnNext.getId()){/*
				mbClickedNext  = true;
				if(!mbIsSkipped && !mbIsTour){
					mbIsTour = true;
					mbIsSkipped = false;
					//initPager();
					startAnim();
					//miImageCount++;

					mbtnNext.setVisibility(View.VISIBLE);
					mbtnPrev.setVisibility(View.VISIBLE);

					mbtnPrev.setText("< Prev");
					mbtnNext.setText("Next >");

				} else if(mbIsTour){
					if(miImageCount==0){

						mbtnNext.setVisibility(View.VISIBLE);
						mbtnPrev.setVisibility(View.VISIBLE);

						startAnim();
						//miImageCount++;

					}else if(miImageCount>=1 && miImageCount<miarrDrawables.size()){

						mbtnNext.setVisibility(View.VISIBLE);
						mbtnPrev.setVisibility(View.VISIBLE);

						mbtnPrev.setText("< Prev");
						mbtnNext.setText("Next >");

						startAnim();
						//miImageCount++;

						if(miImageCount==miarrDrawables.size()){

							mbtnNext.setVisibility(View.VISIBLE);
							mbtnPrev.setVisibility(View.VISIBLE);

							mbtnPrev.setText("< Prev");
							mbtnNext.setText("Finish");
						}

					}else if(miImageCount==miarrDrawables.size()){
						finish();
					}
				}
			*/
				
				mbClickedNext  = true;
				if(!mbIsSkipped && !mbIsTour){
					mbIsTour = true;
					mbIsSkipped = false;
					startAnim();
					mbtnNext.setVisibility(View.VISIBLE);
					mbtnPrev.setVisibility(View.VISIBLE);
					mbtnPrev.setText("< Prev");
					mbtnNext.setText("Next >");
					
					miImageCount++;
					if(miImageCount<(miarrDrawables.size()-1)){
						mbtnPrev.setText("< Prev");
						mbtnNext.setText("Finish");
					}
				}else if(mbIsTour){
					miImageCount++;
					if(miImageCount<miarrDrawables.size()){
						startAnim();
						mbtnNext.setVisibility(View.VISIBLE);
						mbtnPrev.setVisibility(View.VISIBLE);
						mbtnPrev.setText("< Prev");
						mbtnNext.setText("Next >");
						if(miImageCount==(miarrDrawables.size()-1)){
							mbtnPrev.setText("< Prev");
							mbtnNext.setText("Finish");
						}
					}else if(miImageCount==miarrDrawables.size()) {
						 finish();
						
					}else{
						
					}
					
					
				}
			
			
			
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	public void startAnim(){
		//mHandler_anim = new Handler();
		//mRunnable_anim = new Runnable() {

		//@Override
		//public void run() {
		// TODO Auto-generated method stub
		try{
			for(int i=0;i<miarrDrawables.size();i++){
				if(i<=miImageCount){
					viewIndicator[i].setBackgroundColor(mContext.getResources().getColor(R.color.inorbit_blue));
				}else{
					viewIndicator[i].setBackgroundColor(mContext.getResources().getColor(R.color.inorbit_yellow));
				}

			}
			mTvHelperMessage.setText(msHelperText.get(miImageCount));
			mImvImg_front.setImageResource(miarrDrawables.get(miImageCount));
			mImvImg_front.startAnimation(manimFadein);
			manimFadein.setFillAfter(true);
			manimFadein.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					Log.d("=====", "Inorbit Animation start" +miImageCount);//
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					Log.d("=====", "Inorbit Animation end" +miImageCount);

					if(miImageCount<miarrDrawables.size()){
						mImvImg_back.setImageResource(miarrDrawables.get(miImageCount));
					}
				
					/*if(mbClickedNext){
						miImageCount++;
						if(miImageCount==miarrDrawables.size()){
							//miImageCount=0;
							mbtnPrev.setText("< Prev");
							mbtnNext.setText("Finish");
						}	
					}*/
					
				}
			});

			//mHandler_anim.postDelayed(mRunnable_anim, 3000);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	//};
	//mHandler_anim.postDelayed(mRunnable_anim,1000);



}

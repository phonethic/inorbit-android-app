package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.StoreListing.SearachBroadcast;
import com.phonethics.model.RequestTags;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class InorbitMerchantSignUp extends SherlockFragmentActivity implements OnClickListener {

	private ActionBar mactionBar;
	private Context	mContext;	
	private ArrayList<String> msalMALLS = new ArrayList<String>();
	private Dialog mdAreaDialog;
	private EditText metMerchantName ;
	private EditText metShopName;
	private EditText metMerchantNumber;
	private TextView mtvSelectMall;
	private Button mbtnProceed;
	private TextView mtvTandC;
	private boolean mbIsMallSelected;
	private Dialog mdMallsDialog;
	private BroadcastReceiver receiver;
	public static ProgressBar pBar;
	private Dialog	mDialogTc;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inorbit_merchant_sign_up);
		mContext = this;
		
		initView();

		mtvTandC.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				//showToast("Clicked");
				showTCDialog();
			}
		});
	}
	
	private void showTCDialog(){
		
		mDialogTc		= new Dialog(mContext);
		mDialogTc.setContentView(R.layout.termsandcondition);
		mDialogTc.setTitle("Terms and conditions");
		mDialogTc.setCancelable(false);
		mDialogTc.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		WebView 	mWebView		= (WebView)mDialogTc.findViewById(R.id.tcWeb);
		Button 		mBtnTermDone 	= (Button) mDialogTc.findViewById(R.id.btnTermDone) ;
		Button 		mBtnTermCancel	= (Button) mDialogTc.findViewById(R.id.btnTermCancel);

		mBtnTermDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//mbAgree = true;
				mDialogTc.dismiss();
			}
		});
		mBtnTermCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//mbAgree = false;
				mDialogTc.dismiss();
			}
		});
		mWebView.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.tcURl));
		mDialogTc.show();
	}


	/**
	 * 
	 * Initialise the layout and class varibles
	 * 
	 */
	void initView(){

		mactionBar	= getSupportActionBar();
		mactionBar.setTitle(getResources().getString(R.string.actionBarTitle));
		mactionBar.show();
		mactionBar.setDisplayHomeAsUpEnabled(true);
		msalMALLS = new ArrayList<String>();
		//areaDialog = new Dialog(mContext);
		//areaDialog.setContentView(R.layout.dailog_interestedareas);

		metMerchantName 	= (EditText) findViewById(R.id.inorbit_merchant_name);
		metShopName 		= (EditText) findViewById(R.id.inorbit_shop_name);
		metMerchantNumber 	= (EditText) findViewById(R.id.inorbit_merchant_number);
		mtvSelectMall		= (TextView) findViewById(R.id.text_selectMall);
		mbtnProceed			= (Button) findViewById(R.id.inorbit_procceed_button);
		mtvTandC			= (TextView) findViewById(R.id.inorbit_tandc);
		pBar				= (ProgressBar) findViewById(R.id.pBar);
		pBar.setVisibility(View.GONE);

		metMerchantName.setTypeface(InorbitApp.getTypeFace());
		metShopName.setTypeface(InorbitApp.getTypeFace());
		metMerchantNumber.setTypeface(InorbitApp.getTypeFace());
		mtvSelectMall.setTypeface(InorbitApp.getTypeFace());
		mbtnProceed.setTypeface(InorbitApp.getTypeFace());
		mtvTandC.setTypeface(InorbitApp.getTypeFace());

		mbtnProceed.setOnClickListener(this);
		mtvSelectMall.setOnClickListener(this);

		receiver = new MerchantRegisterReciver();

	}





	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		this.finish();

		return true;
	}




	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
	}


	
	/**
	 * Network request to sign up this merchant.
	 * 
	 */
	void signUpMerchant(){
		
		MyClass myClass =  new MyClass(mContext);
		HashMap<String, String> headers =  new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		JSONObject json = new JSONObject();
		try{
			
			json.put("mobile", "91"+metMerchantNumber.getText().toString());
			String name = mtvSelectMall.getText().toString() +" - " +metShopName.getText().toString()+" - "+metMerchantName.getText().toString();
			json.put("name", name);
			myClass.postRequest(RequestTags.Tag_merchantregister, headers, json);
			pBar.setVisibility(View.VISIBLE);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	
	/**
	 * 
	 * Validate the information entered by the user
	 * 
	 * @return
	 */
	private boolean validateData(){
		if(metMerchantName.getText().toString().equalsIgnoreCase("")||(metMerchantName.getText().toString().length()==0)) {
			showToast("Please enter your name");
			return false;
		} else if(metShopName.getText().toString().equalsIgnoreCase("")||(metShopName.getText().toString().length()==0)) {
			showToast("Please enter your shop name");
			return false;
		} else if(mbIsMallSelected==false) {
			showToast("Please select your Inorbit mall location");
			return false;
		} else if(metMerchantNumber.getText().toString().equalsIgnoreCase("")||metMerchantNumber.getText().toString().length()==0){
			showToast("Please enter your mobile number");
			return false;
		} else if(metMerchantNumber.getText().toString().length()<10) {
			showToast("Mobile number must be 10 digit");
			return false;
		} else {
			return true;
		}
	}

	
	
	/**
	 * 
	 * Prompt the merchant to select the mall location
	 * 
	 */
	void showMallChooserDialog(){
		
		mdMallsDialog = new Dialog(mContext);
		mdMallsDialog.setContentView(R.layout.dailog_interestedareas);
		mdMallsDialog.setTitle("Select Mall");
		mdMallsDialog.setCancelable(true);
		mdMallsDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		ListView mlvArea = (ListView) mdMallsDialog.findViewById(R.id.interestedAreaDialogList);
		DBUtil mDbUtil = new DBUtil(mContext); 
		
		
		/**
		 * Get the mall list from database
		 * 
		 */
		msalMALLS = mDbUtil.getAllAreas();
		mlvArea.setAdapter(new AreaAdapter((Activity) mContext, msalMALLS));
		mlvArea.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				mbIsMallSelected = true;
				mtvSelectMall.setText(msalMALLS.get(arg2).toString());
				mdMallsDialog.dismiss();
			}
		});
		Button updateInterestedAreaBtn = (Button) mdMallsDialog.findViewById(R.id.updateInterestedAreaBtn);
		updateInterestedAreaBtn.setVisibility(View.GONE);
		mdMallsDialog.show();

	}

	void showToast(String message){
		Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==mbtnProceed.getId()){
			if(validateData()){
				signUpMerchant();
			}
		}
		if(v.getId()==mtvSelectMall.getId()){
			showMallChooserDialog();
		}
	}

	
	
	/**
	 * Broadcast receiver for signUpMerchant()
	 * 
	 * @author Nitin
	 *
	 */
	class MerchantRegisterReciver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String msSuccess = intent.getStringExtra("SUCCESS");
			pBar.setVisibility(View.GONE);
			if(msSuccess.equalsIgnoreCase("true")) {
				//Toast.makeText(context, "True - It's Working", Toast.LENGTH_SHORT).show();
				Intent intent1=new Intent(((Activity)context), SignupMerchant.class);
				intent1.putExtra("server_message", intent.getStringExtra("MESSAGE"));
				intent1.putExtra("mobileno", "91"+metMerchantNumber.getText().toString());
				((Activity)context).startActivity(intent1);
				((Activity) context).overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			} else {
				Toast.makeText(context, "Could not register now. Please try again after some time", Toast.LENGTH_SHORT).show();
			}
		}

	}


	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		// TODO Auto-generated method stub
		super.startActivityForResult(intent, requestCode);
	}

	
	
	
	public class AreaAdapter extends ArrayAdapter<String> {

		Activity 			context;
		LayoutInflater 		inflator = null;
		ArrayList<String> 	areaName;
		public AreaAdapter(Activity context, ArrayList<String> areaName) {
			super(context,R.layout.interestedarea);
			this.context = context;
			this.areaName = areaName;

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View rowView = convertView;
			final int pos = position;
			if(convertView == null){
				inflator = context.getLayoutInflater();
				rowView = inflator.inflate(R.layout.interestedarea, null);
				ViewHolder holder = new ViewHolder();
				holder.text = (TextView) rowView.findViewById(R.id.areaName);
				holder.areaCheck = (CheckBox) rowView.findViewById(R.id.areaCheckBox);
				holder.areaCheck.setVisibility(View.GONE);
				rowView.setTag(holder);
			}
			ViewHolder hold = (ViewHolder) rowView.getTag();
			hold.text.setText(areaName.get(position).toString());
			return rowView;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return areaName.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return areaName.get(position);
		}
	}

	static class ViewHolder{
		public TextView text;
		public CheckBox areaCheck;

	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			//EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter filter = new IntentFilter(RequestTags.Tag_merchantregister);
			mContext.registerReceiver(receiver, filter);
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		try{
			EventTracker.endFlurrySession(getApplicationContext());
			if(receiver!=null){
				mContext.unregisterReceiver(receiver);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

}

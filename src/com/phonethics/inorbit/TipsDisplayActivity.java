package com.phonethics.inorbit;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.RequestTags;
import com.phonethics.model.TipsModel;

public class TipsDisplayActivity extends SherlockFragmentActivity {
	Activity mContext;
	ListView tipsListView;
	private ActionBar mActionBar;
	ArrayList<TipsModel> tips;
	GetAllTips getAllTips;
	PageAdapter pageAdapter;
	private String place_id;
	private PagerSlidingTabStrip pagerTabs;
	private JazzyViewPager mPager;
	private ArrayList<String> pages;
	private ProgressBar pBar;
	private static final String sStrPage1 = "Approved";
	private static final String sStrPage2 = "Pending";
	/*private static final String sStrPage3 = "Rejected";*/
	private Dialog operationDialog;
	private UpdateTipStatusReceiver updateTipStatusReceiver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_post_new);
		
		mContext = this;
		initViews();
		
		tips = new ArrayList<TipsModel>();
		
		place_id = getIntent().getExtras().getString("place_id");
		
		getTips();
	}
	
	private void initViews() {
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Manage Tips");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();
		
		pBar = (ProgressBar) findViewById(R.id.pBar_view_post);
		pagerTabs = (PagerSlidingTabStrip) findViewById(R.id.pagerTabs_viewtabs);
		mPager=(JazzyViewPager)findViewById(R.id.viewPostPager);
		pages = new ArrayList<String>();
		pages.add(sStrPage1);
		pages.add(sStrPage2);
		/*pages.add(sStrPage3);*/
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		try {
			EventTracker.startFlurrySession(this);
		} catch (Exception e) {}
		
		IntentFilter getStoreMgrTips = new IntentFilter(RequestTags.TAG_TIP_GET_ALL);
		mContext.registerReceiver(getAllTips = new GetAllTips(), getStoreMgrTips);
		
		IntentFilter updateTipStatus = new IntentFilter(RequestTags.TAG_TIP_UPDATE_STATUS);
		mContext.registerReceiver(updateTipStatusReceiver = new UpdateTipStatusReceiver(), updateTipStatus);
	}
	
	@Override
	protected void onStop() {
		try {
			EventTracker.endFlurrySession(this);
		} catch (Exception e) {}
		
		if (getAllTips!=null) {
			mContext.unregisterReceiver(getAllTips);
		}
		
		if (updateTipStatusReceiver!=null) {
			mContext.unregisterReceiver(updateTipStatusReceiver);
		}
		super.onStop();
	}
	
	private void updateTipStatus(TipsModel tip, int changeToStatus) {
		if (new NetworkCheck(mContext).isNetworkAvailable()) {
			JSONObject json = new JSONObject();
			try {
				json.put("tip_id", tip.getTipId());
				json.put("status_code", changeToStatus+1);
				json.put("user_id", getUserId());
				json.put("auth_id", getAuthId());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			
			MyClass myClass = new MyClass(this);
			myClass.postRequest(RequestTags.TAG_TIP_UPDATE_STATUS, null, headers, json);
		} else {
			Toast.makeText(mContext, "No Internet Connection. Please try again after sometime.", Toast.LENGTH_SHORT).show();
		}
	}
	
	String getUserId() {
		SessionManager mSessionManager = new SessionManager(this);
		HashMap<String,String>user_details = mSessionManager.getUserDetails();
		return user_details.get("user_id").toString();

	}
	
	String getAuthId() {
		SessionManager mSessionManager = new SessionManager(this);
		HashMap<String,String>user_details = mSessionManager.getUserDetails();
		return user_details.get("auth_id").toString();
	}
	
	private void getTips() {
		if(new NetworkCheck(mContext).isNetworkAvailable()) {
			pBar.setVisibility(View.VISIBLE);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("place_id", place_id));
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			
			MyClass myClass = new MyClass(this);
			myClass.getStoreRequest(RequestTags.TAG_TIP_GET_ALL, nameValuePairs, headers);
		} else {
			Toast.makeText(mContext, "No Internet Connection. Please try again after some time.", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class UpdateTipStatusReceiver extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent!=null) {
				showToast(intent.getStringExtra("MESSAGE"));
			} else {
				showToast("Some error was generated");
			}
			getTips();
		}
	}
	
	private class GetAllTips extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent.getStringExtra("SUCCESS").equalsIgnoreCase("true")) {
					tips = intent.getExtras().getParcelableArrayList("data");
					if (tips != null && tips.size() == 0) {
						showToast("There are no tips posted for this store.");
					}
					if (pageAdapter == null) {
						pageAdapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, pages);
						mPager.setAdapter(pageAdapter);
						mPager.setCurrentItem(1);
					} else {
						//adapter.notifyDataSetChanged();
						int page = mPager.getCurrentItem();
						pageAdapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, pages);
						mPager.setAdapter(pageAdapter);
						mPager.setCurrentItem(page);
					}
					mPager.setOffscreenPageLimit(pages.size());
					pagerTabs.setViewPager(mPager);
					pagerTabs.setTextColor(Color.BLACK);
					pagerTabs.setBackgroundColor(Color.TRANSPARENT);
				} else {
					showToast(intent.getStringExtra("MESSAGE"));
					tips.clear();
					int position = mPager.getCurrentItem();
					pageAdapter = new PageAdapter(getSupportFragmentManager(), (Activity)context, pages);
					mPager.setAdapter(pageAdapter);
					mPager.setCurrentItem(position);
				}
				pageAdapter.notifyDataSetChanged();
				pBar.setVisibility(View.GONE);
			} else {
				showToast("Error was generated");
			}
		}
	}
	
	//Creating Pages with PageAdapter.
	public class PageAdapter extends FragmentStatePagerAdapter{
		Activity context;
		ArrayList<String> pages;
		
		public PageAdapter(FragmentManager fm,Activity context,ArrayList<String> pages) {
			super(fm);
			this.context=context;
			this.pages=pages;
		}
		
		@Override
		public String getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return new PageFragment(pages.get(position), context);
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return pages.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Object obj = super.instantiateItem(container, position);
			mPager.setObjectForPosition(obj, position);
			return obj;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if(object != null) {
				return ((Fragment)object).getView() == view;
			} else {
				return false;
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			
		}
	}
		
	@SuppressLint("ValidFragment")
	public class PageFragment extends Fragment {
		String title;
		Context context;
		View view;
		ArrayList<TipsModel> tipsToDisplay;
		
		public PageFragment(){
			
		}
		
		@SuppressLint("ValidFragment")
		public PageFragment(String title,Activity context){
			this.title=title;
			this.context=context;
		}
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setRetainInstance(true);
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			super.onCreateView(inflater, container, savedInstanceState);
			
			view = inflater.inflate(R.layout.list_view_post, null);
			
			tipsListView = (ListView) view.findViewById(R.id.listView_view_post);
			
			if(title.equalsIgnoreCase(sStrPage1)) {
				tipsToDisplay = getTipsToDisplay(1);
				tipsListView.setAdapter(new TipsAdapter(tipsToDisplay));
				
				tipsListView.setOnItemClickListener(new OnItemClickListener() {
					
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					}
				});
				
			} else if(title.equalsIgnoreCase(sStrPage2)) {
				tipsToDisplay = getTipsToDisplay(0);
				tipsListView.setAdapter(new TipsAdapter(tipsToDisplay));
				
				tipsListView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
						
					}
				});
				
				tipsListView.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int position, long arg3) {
						operationDialog  = new Dialog(mContext);
						operationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
						operationDialog.setContentView(R.layout.approve_item_dialog);
						operationDialog.setTitle(null);
						ListView listCateg = (ListView)operationDialog.findViewById(R.id.categDilogList);
						ArrayList<String> 	operationArr = new ArrayList<String>();
						operationArr.add("Approve");
						operationArr.add("Reject");
						listCateg.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, operationArr));
						listCateg.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1, int itemPos,
									long arg3) {
								// TODO Auto-generated method stub
								try{
									operationDialog.dismiss();
									showConfirmationDialog(tipsToDisplay.get(position), itemPos);
								} catch (Exception e) {
								}
							}
						});
						operationDialog.show();
						return true;
					}
				});
			}/* else if(title.equalsIgnoreCase(sStrPage3)) {
				tipsToDisplay = getTipsToDisplay(2);
				tipsListView.setAdapter(new TipsAdapter(tipsToDisplay));
				
				tipsListView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						
					}
				});*/
				
				/*listOfItems.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int position, long arg3) {
						
						if (!isMallManager) {
							operationDialog  = new Dialog(mContext);
							operationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
							operationDialog.setContentView(R.layout.approve_item_dialog);
							operationDialog.setTitle(null);
							ListView listCateg = (ListView)operationDialog.findViewById(R.id.categDilogList);
							ArrayList<String> 	operationArr = new ArrayList<String>();
							operationArr.add("Delete");
							listCateg.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, operationArr));
							listCateg.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> arg0, View arg1, int itemPos,
										long arg3) {
									// TODO Auto-generated method stub
									try{
										operationDialog.dismiss();
										if (itemPos == 0) {
											sendToDelete(restaurantsMenu.get(position));
										}
									} catch (Exception e) {
									}
								}
							});
							operationDialog.show();
						}
						return true;
					}
				});
			}*/
			return view;
		}
	}
	
	private void showConfirmationDialog(final TipsModel tip, final int changeToStatus) {
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setMessage("Are you sure you want to " + (changeToStatus == 0 ? "approve":"reject") + " this tip?")
				.setPositiveButton("No", new Dialog.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
					
				})
				.setNegativeButton("Yes", new Dialog.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						try {
							EventTracker.logEvent((changeToStatus == 0 ? "MManageTips_Approved":"MManageTips_Rejected"), true);
						} catch (Exception e) {}
						updateTipStatus(tip, changeToStatus);
					}
					
				});
		
		builder.create().show();
		//tipsToDisplay.get(position), itemPos);
	}
	
	private ArrayList<TipsModel> getTipsToDisplay(int status) {
		ArrayList<TipsModel> tipsToDisplay = new ArrayList<TipsModel>();
		if (tips != null) {
			for(TipsModel model:tips) {
				if (status == Integer.parseInt(model.getStatus())) {
					tipsToDisplay.add(model);
				}
			}
		}
		return tipsToDisplay;
	}
	
	private class TipsAdapter extends BaseAdapter {
		ArrayList<TipsModel> tipsToDisplay;
		
		public TipsAdapter(ArrayList<TipsModel> tipsToDisplay) {
			this.tipsToDisplay = tipsToDisplay;
		}
		
		@Override
		public int getCount() {
			return tipsToDisplay.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return tipsToDisplay.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = new ViewHolder();
			if (convertView == null) {
				convertView = mContext.getLayoutInflater().inflate(R.layout.tip_layout, null);
				holder.comment = (TextView) convertView.findViewById(R.id.tips);
				holder.userName = (TextView) convertView.findViewById(R.id.userName);
				holder.timeStamp = (TextView) convertView.findViewById(R.id.timestamp);
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			String tip = tipsToDisplay.get(position).getTip();
			String seperator = "\\\\n";
			String []tempText = tip.split(seperator);
			
			/*if (tip.contains(seperator)){*/
	        String b= "";
	        for (String c : tempText) {
	        	InorbitLog.d("%%%%%%% "+c);
	        	b +=c+"\n";
	        }
	        InorbitLog.d(b);
	        //b = b.substring(0, b.length()-2);
	        holder.comment.setText(b.trim());
			holder.userName.setText(tipsToDisplay.get(position).getName());
			holder.userName.setTypeface(null, Typeface.BOLD);
			holder.timeStamp.setTypeface(null, Typeface.BOLD);
			
			String msOldFormat="yyyy-MM-dd H:mm:ss";
			String msNewFormat = "dd MMM yyyy  h:mm a";
			String msNewDate = "";

			try{
				SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
				Date d = sdf.parse(tipsToDisplay.get(position).getTime());
				sdf.applyPattern(msNewFormat);
				msNewDate = sdf.format(d);
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
			holder.timeStamp.setText(msNewDate);
			return convertView;
		}
		
		private class ViewHolder {
			TextView comment, userName, timeStamp;
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}
	
	private void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
}

package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

import com.phoenthics.adapter.MallEventsAdapter;
import com.phonethics.adapters.ActionAdapter;
import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;

public class NewsActivity extends SherlockActivity implements  OnNavigationListener {

	//ImageLoader imageLoaderList;
	private Activity 	mContext;
	private ActionBar 	mactionbar;
	private ListView 	mLvMallEvents;
	private ArrayList<String> msArrMalls=new ArrayList<String>();
	private String 		msCurrentMallPlaceParent;
	private DBUtil 		mdbUtil;
	private ProgressBar mpbar;
	private MallOffersReceiver mallOfferReceiver;
	private TextView txtNoRecords;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);

		mContext = this;
		
		/**
		 * Initialize the view
		 * 
		 */
		initView();
		
		/**
		 * Initialize the action bar
		 * 
		 */
		initActionBar();


	}

	
	/**
	 * Initialize the view
	 * 
	 */
	
	private void initView(){
		mdbUtil = new DBUtil(mContext);
		mpbar	= (ProgressBar) findViewById(R.id.pBar);
		txtNoRecords = (TextView) findViewById(R.id.text_no_records);
		txtNoRecords.setTypeface(InorbitApp.getTypeFace());
		txtNoRecords.setVisibility(View.GONE);
		mLvMallEvents 	= (ListView)findViewById(R.id.allBroadcastList);

	}

	
	/**
	 * Initialize the action bar
	 * 
	 */
	private void initActionBar(){
		mactionbar = getSupportActionBar();
		mactionbar.setTitle(getResources().getString(R.string.actionBarTitle));
		mactionbar.setDisplayHomeAsUpEnabled(true);
		mactionbar.show();

		
		/**
		 * Get the malls list to load the action bar
		 * 
		 */
		msArrMalls 		= mdbUtil.getAllAreas();
		
		ActionAdapter business_list=new ActionAdapter(mContext, 0, 0, mdbUtil.getAllAreas());
		mactionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		mactionbar.setListNavigationCallbacks(business_list, this);
		
		String activeAreaId = mdbUtil.getActiveMallId();
		String areaName = mdbUtil.getAreaNameByInorbitId(activeAreaId);
		int areaPos = msArrMalls.indexOf(areaName);
		Log.d("", "Inorbit active id "+activeAreaId+" Area : "+areaName+" ArrayPos "+areaPos);
		mactionbar.setSelectedNavigationItem(areaPos);
	}


	/**
	 * Request the server to get the offers/post of the mall
	 * 
	 */
	void loadBroadCast(){
		if(NetworkCheck.isNetConnected(mContext)) {
			mpbar.setVisibility(View.VISIBLE);
			txtNoRecords.setVisibility(View.GONE);

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("place_id", msCurrentMallPlaceParent));


			HashMap<String, String> headers =  new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

			MyClass myClass = new MyClass(mContext);
			myClass.getStoreRequest(RequestTags.Tag_GetMallEvents, nameValuePairs, headers);

		} else {
			mpbar.setVisibility(View.GONE);
			showToast(getResources().getString(R.string.noInternetConnection));
		}



	}

	void showToast(String msg){
		Toast.makeText(mContext, msg,Toast.LENGTH_SHORT).show();
	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		/*Intent intent = new Intent();
		setResult(5, intent);*/
		finishTo();
		return true;
	}

	/**
	 * 
	 * Broadcast receiver of the malls offer api
	 * 
	 *
	 */
	class MallOffersReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try {
				mpbar.setVisibility(View.GONE);
				if(intent!=null) {
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")) {
						txtNoRecords.setVisibility(View.GONE);
						mLvMallEvents.setVisibility(View.VISIBLE);
						final ArrayList<PostDetail> postDetailsArr = intent.getParcelableArrayListExtra("POST_DETAILS");
						MallEventsAdapter mallEventsAdapter = new MallEventsAdapter((Activity) context, postDetailsArr);
						mLvMallEvents.setAdapter(mallEventsAdapter);

						mLvMallEvents.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1,
									int position, long arg3) {
								// TODO Auto-generated method stub
								try{

									//Intent intent=new Intent(NewsActivity.this, OffersBroadCastDetails.class);
									Intent intent=new Intent(NewsActivity.this, OfferViewNew.class);
									Conifg.fromPedingOffers = false;
									intent.putExtra("title", postDetailsArr.get(position).getTitle());
									intent.putExtra("body", postDetailsArr.get(position).getDescription());
									intent.putExtra("photo_source", postDetailsArr.get(position).getImage_url1());
									intent.putExtra("POST_ID", postDetailsArr.get(position).getId());
									intent.putExtra("PLACE_ID", msCurrentMallPlaceParent);
									intent.putExtra("storeName", "");
									intent.putExtra("fromCustomer", true);
									startActivity(intent);

								}catch(Exception ex){
									ex.printStackTrace();
								}
							}
						});
					} else {
						mLvMallEvents.setVisibility(View.GONE);
						//txtNoRecords.setVisibility(View.VISIBLE);
						//txtNoRecords.setText(intent.getStringExtra("MESSAGE"));
						ShowMessageDialog.show(mContext, intent.getStringExtra("MESSAGE"));
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}


		}

	}


	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		msCurrentMallPlaceParent 	= mdbUtil.getMallIdByPos(""+(itemPosition+1));
		msCurrentMallPlaceParent  = mdbUtil.getMallPlaceParentByMallID(msCurrentMallPlaceParent);	
		mdbUtil.setActiveMall(msCurrentMallPlaceParent);
		loadBroadCast();

		return false;
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}

	void finishTo(){
		Intent intent = new Intent();
		setResult(5, intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		try{
			//EventTracker.endFlurrySession(getApplicationContext());
			if(Conifg.useServices_c){

			}else{
				if(mallOfferReceiver!=null){
					mContext.unregisterReceiver(mallOfferReceiver);
				}

			}
		}catch(Exception ex){

		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			//EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter dateFilter 		= new IntentFilter(RequestTags.Tag_GetMallEvents);	
			mContext.registerReceiver(mallOfferReceiver 	= new MallOffersReceiver(), dateFilter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

}

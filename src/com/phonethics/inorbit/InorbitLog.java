package com.phonethics.inorbit;

import android.util.Log;

public class InorbitLog {

	public static String TAG = "Inorbit";
	public static boolean debug = false;


	public static void d(String msg) {
		if(debug){
			Log.d(TAG, TAG +" " +msg);
		}
	}

	public static void e(String msg) {
		if(debug){
			Log.e(TAG, TAG +" " +msg);
		}
	}


}

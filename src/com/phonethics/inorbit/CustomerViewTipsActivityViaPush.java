package com.phonethics.inorbit;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.RequestTags;
import com.phonethics.model.TipsModel;

public class CustomerViewTipsActivityViaPush extends SherlockActivity {
	Activity mContext;
	ArrayList<TipsModel> tips;
	String place_id;
	ActionBar mActionBar;
	GetAllTips getAllTips;
	ListView tipsListView;
	TipsAdapter adapter;
	AddTip addTip;
	ProgressBar progressBar;
	String storeName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tips_display_activity);
		mActionBar = getSupportActionBar();
		
		mContext = this;
		initViews();
		tips = new ArrayList<TipsModel>();
		adapter = new TipsAdapter(tips);
		tipsListView.setAdapter(adapter);
		
		place_id = getIntent().getExtras().getString("place_id");
		
		((TextView) findViewById(R.id.addComment)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SessionManager session = new SessionManager(mContext);
				if(session.isLoggedInCustomer()) {
					generateFlurryEvent(CustomerViewTipsActivityViaPush.this.getResources().getString(R.string.leave_a_tip));
					final View view = mContext.getLayoutInflater().inflate(R.layout.get_tip_layout, null);
					final EditText editText = ((EditText) view.findViewById(R.id.editText));
					
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
					if (tips.size()>0) {
						alertDialogBuilder.setTitle(tips.get(0).getStoreName());
					} else 
						alertDialogBuilder.setTitle(mContext.getResources().getString(R.string.actionBarTitle));
					alertDialogBuilder
					.setView(view)
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(false)
					.setPositiveButton("Post",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							/*InputMethodManager imm = (InputMethodManager)getSystemService(
								      Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
							//Intent intent=new Intent(mContext, LoginSigbUpCustomerNew.class);
							//intent.putExtra("dontUpdateProfile", true);
							//startActivity(intent);
							//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							if(new NetworkCheck(mContext).isNetworkAvailable()) {
								InorbitLog.d(">>>>>>>>>>> "+editText.getText().toString());
								String tip = editText.getText().toString().trim();
								if (tip != null && tip.length()>0) {
									sendTip(tip);
									dialog.dismiss();
								} else {
									showToast("Sorry, can't accept a blank tip.");
								}
							} else {
								Toast.makeText(mContext, "No Internet Connection. Please try again after some time.", Toast.LENGTH_SHORT).show();
							}*/
						}
					})
					.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							//registerAction(mContext.getResources().getString(R.string.favourite_AlertAction),"Cancel");
							InputMethodManager imm = (InputMethodManager)getSystemService(
								      Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
							dialog.dismiss();
						}
					});
					
					final AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
					alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
					{ 
						@Override
						public void onClick(View v)
						{
								InputMethodManager imm = (InputMethodManager)getSystemService(
										Context.INPUT_METHOD_SERVICE);
									imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
																		
														
														
															
								if(new NetworkCheck(mContext).isNetworkAvailable()) {
									
									String tip = editText.getText().toString().trim();
									if (tip != null && tip.length()>0) {
										sendTip(tip);
										alertDialog.dismiss();
									} else {
										showToast("Sorry, can't accept a blank tip.");
									}
								} else {
									Toast.makeText(mContext, "No Internet Connection. Please try again after some time.", Toast.LENGTH_SHORT).show();
								}
							}
				      });
				} else {
					String message = mContext.getResources().getString(R.string.login_to_add_tips);
					//showLoginDialog(context.getResources().getString(R.string.login_to_see_offers));
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
					alertDialogBuilder.setTitle(mContext.getResources().getString(R.string.actionBarTitle));
					alertDialogBuilder
					.setMessage(message)
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(true)
					.setPositiveButton("Login",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							dialog.dismiss();
							Intent intent=new Intent(mContext, LoginSigbUpCustomerNew.class);
							intent.putExtra("dontUpdateProfile", true);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

						}
					})
					.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							//registerAction(mContext.getResources().getString(R.string.favourite_AlertAction),"Cancel");
							generateFlurryEvent(CustomerViewTipsActivityViaPush.this.getResources().getString(R.string.leave_tip_cancel));
							dialog.dismiss();
						}
					});
					
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
				}
			}
		});
	}
	
	private void sendTip(String msg) {
		if (new NetworkCheck(mContext).isNetworkAvailable()) {
			progressBar.setVisibility(View.VISIBLE);
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			
			JSONObject json = new JSONObject();
			try {
				json.put("tip", msg);
				json.put("place_id", place_id);
				json.put("UDM_id", new SessionManager(mContext).getUdmIDForCustomer());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			generateFlurryEvent(this.getResources().getString(R.string.leave_tip_post));
			MyClass myClass = new MyClass(mContext);
			myClass.postRequest(RequestTags.TAG_TIP_ADD_TIP, headers, json);
		} else {
			Toast.makeText(mContext, "No Internet Connection. Please try again after some time.", Toast.LENGTH_SHORT).show();
		}
	}
	
	private void initViews() {
		mActionBar	=	getSupportActionBar();
		mActionBar.setTitle("Tips");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();
		
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		tipsListView = (ListView) findViewById(R.id.tipsListView);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		try {
			EventTracker.startFlurrySession(this);
		} catch (Exception e) {}
		
		try {
			EventTracker.logEvent("Push_Tip_Open", true);
		} catch (Exception e) {}
		
		getTips();
		IntentFilter getStoreMgrTips = new IntentFilter(RequestTags.TAG_TIP_GET_ALL);
		mContext.registerReceiver(getAllTips = new GetAllTips(), getStoreMgrTips);
		
		IntentFilter addTipFilter = new IntentFilter(RequestTags.TAG_TIP_ADD_TIP);
		mContext.registerReceiver(addTip = new AddTip(), addTipFilter);
	}
	
	@Override
	protected void onStop() {
		
		try {
			EventTracker.endFlurrySession(this);
		} catch (Exception e) {}
		
		if (getAllTips!=null) {
			mContext.unregisterReceiver(getAllTips);
		}
		
		if (addTip != null) {
			mContext.unregisterReceiver(addTip);
		}
		super.onStop();
	}
	
	private void getTips() {
		if(new NetworkCheck(mContext).isNetworkAvailable()) {
			progressBar.setVisibility(View.VISIBLE);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("place_id", place_id));
			
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			
			MyClass myClass = new MyClass(this);
			myClass.getStoreRequest(RequestTags.TAG_TIP_GET_ALL, nameValuePairs, headers);
		} else {
			Toast.makeText(mContext, "No Internet Connection. Please try again after some time.", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class GetAllTips extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent.getStringExtra("SUCCESS").equalsIgnoreCase("true")) {
					ArrayList<TipsModel> tempModel = intent.getExtras().getParcelableArrayList("data");
					tips.clear();
					for (TipsModel model:tempModel) {
						if (model.getStatus().equalsIgnoreCase("1"))
							tips.add(model);
					}
					if (tips.size()>0) {
						storeName = tips.get(0).getStoreName();
						mActionBar.setTitle(tips.get(0).getStoreName());
						generateFlurryEvent(CustomerViewTipsActivityViaPush.this.getResources().getString(R.string.tip_view));
					}
				} else {
					showToast(intent.getStringExtra("MESSAGE"));
				}
				adapter.notifyDataSetChanged();
			} else {
				showToast("Some Error was generated");
			}
			progressBar.setVisibility(View.GONE);
		}
	}
	
	private class AddTip extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				showToast(intent.getStringExtra("MESSAGE"));
			} else {
				showToast("Some error was generated");
			}
			progressBar.setVisibility(View.GONE);
		}
	}
	
	private class TipsAdapter extends BaseAdapter {
		ArrayList<TipsModel> tipsToDisplay;
		
		public TipsAdapter(ArrayList<TipsModel> tipsToDisplay) {
			this.tipsToDisplay = tipsToDisplay;
		}
		
		@Override
		public int getCount() {
			return tipsToDisplay.size();
		}
		
		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return tipsToDisplay.get(position);
		}
		
		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = new ViewHolder();
			if (convertView == null) {
				convertView = mContext.getLayoutInflater().inflate(R.layout.tip_layout, null);
				holder.comment = (TextView) convertView.findViewById(R.id.tips);
				holder.timeStamp = (TextView) convertView.findViewById(R.id.timestamp);
				holder.userName = (TextView) convertView.findViewById(R.id.userName);
				convertView.setTag(holder);
			}
			
			holder = (ViewHolder) convertView.getTag();
			
			holder.userName.setText(tipsToDisplay.get(position).getName());
			holder.timeStamp.setText(tipsToDisplay.get(position).getTime());
			
			String tip = tipsToDisplay.get(position).getTip();
			String seperator = "\\\\n";
			String []tempText = tip.split(seperator);
			
			/*if (tip.contains(seperator)){*/
	        String b= "";
	        for (String c : tempText) {
	        	InorbitLog.d("%%%%%%% "+c);
	        	b +=c+"\n";
	        }
	        
	        //b= b.substring(0, b.length()-2);
	        holder.comment.setText(b.trim());
	        InorbitLog.d(b);
			/*} else {
				holder.comment.setText(tip);
			}*/
	        
			String msOldFormat="yyyy-MM-dd H:mm:ss";
			String msNewFormat = "dd MMM yyyy";
			String msNewDate = "";
			
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
				Date d = sdf.parse(tipsToDisplay.get(position).getTime());
				sdf.applyPattern(msNewFormat);
				msNewDate = sdf.format(d);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			
			holder.timeStamp.setText(msNewDate);
			
			return convertView;
		}
		
		private class ViewHolder {
			TextView comment, userName, timeStamp;
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (isTaskRoot()) {
			Intent intent=new Intent(this, HomeGrid.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		} else {
			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		return true;
	}
	
	@Override
	public void onBackPressed() {
		if (isTaskRoot()) {
			Intent intent=new Intent(this, HomeGrid.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.slide_out_left, R.anim.slide_out_right);
		} else {
			finish();
			overridePendingTransition(R.anim.slide_out_left, R.anim.slide_out_right);
		}
		super.onBackPressed();
	}
	
	private void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
	
	private void generateFlurryEvent(String eventName) {
		try {
			DBUtil dbutil = new DBUtil(this);
			
			HashMap<String, String> map = new HashMap<String, String>();
			String activeAreaId = dbutil.getActiveMallId();
			
			dbutil.getMallPlaceParentByMallID(activeAreaId);
			String areaName = dbutil.getAreaNameByInorbitId(activeAreaId);
			
			if(areaName!=null && !areaName.equals("")) {
				map.put("MallName", areaName);
			}
			if (storeName != null && !storeName.equals("")) {
				map.put("StoreName", storeName);
			}
			EventTracker.logEvent(eventName, map);
		} catch (Exception e) {}
	}
}

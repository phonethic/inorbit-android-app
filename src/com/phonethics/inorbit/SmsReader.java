package com.phonethics.inorbit;

import java.util.List;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class SmsReader extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Bundle myBundle = intent.getExtras();
		SmsMessage [] messages = null;
		String strMessage = "";
		String strMessageBody="";

		if (myBundle != null)
		{
			Object [] pdus = (Object[]) myBundle.get("pdus");
			messages = new SmsMessage[pdus.length];

			for (int i = 0; i < messages.length; i++)
			{
				messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
				strMessage += "SMS From: " + messages[i].getOriginatingAddress();
				strMessage += " : ";
				strMessageBody += messages[i].getMessageBody();
				strMessage += "\n";
			}



			try{

				Log.i("Verification", "Message Verification Code is :  "+strMessageBody);
				String str = "";
				/*Thank You for registering with Shoplocal. Your Verification code is 217150*/
				String tempNum=strMessageBody.replaceAll("\\D+","");
				String tempBody=strMessageBody.replaceAll(tempNum,"");
				
				if(tempBody.contains("Welcome to the Inorbit Experience. Your verification code is")){
					str=strMessageBody.replaceAll("\\D+","");
				}
				Log.i("Verification", "Message Verification Code is replaced :  "+str);
				ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
				// get the info from the currently running task
				
				
				List< ActivityManager.RunningTaskInfo > taskInfo = am.getRunningTasks(1);
				String runningActivity=taskInfo.get(0).topActivity.getClassName();
				Log.d("topActivity", "CURRENT Activity ::"+ taskInfo.get(0).topActivity.getClassName());
			
				if(runningActivity.equalsIgnoreCase("com.phonethics.inorbit.SignUpCustomer"))
				{
					if(str.length()==6)
					{
						Intent intent_code=new Intent();
						intent_code.setAction("verif_codeCustomer_inorbit");
						intent_code.putExtra("verification_code", str.trim());
						context.sendBroadcast(intent_code);
						Log.i("Verification", "Message Verification Code is inside : "+str.trim());
					}
				}
				else if(runningActivity.equalsIgnoreCase("com.phonethics.inorbit.SignupMerchant"))
				{
					if(str.length()==6)
					{
						Intent intent_code=new Intent();
						intent_code.setAction("verif_code_inorbit");
						intent_code.putExtra("verification_code", str.trim());
						context.sendBroadcast(intent_code);
						Log.i("Verification", "Message Verification Code is inside : "+str.trim());
					}
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}



			/* if(strMessage.contains("Thank you for registering with Shoplocal.Your Verification code is"))
	            {
	            String code=strMessage.replaceAll("Thank you for registering with Shoplocal.Your Verification code is", "");
	            Intent intent_code=new Intent();
	            intent_code.setAction("verif_code");
	            intent_code.putExtra("verification_code", code.trim());
	            context.sendBroadcast(intent_code);
	            Log.i("Verification", "Message Verification Code is inside : "+code.trim());
	            }*/
			/*  Toast.makeText(context, strMessage.substring(index+1, strMessage.length()), Toast.LENGTH_LONG).show();*/

		}
	}}

package com.phonethics.inorbit;

import android.annotation.SuppressLint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;

public class ViewPagerTest extends SherlockFragmentActivity {

	private final Handler handler = new Handler();

	ActionBar actionbar;

	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private MyPagerAdapter adapter;

	private Drawable oldBackground = null;
	private int currentColor = 0xFF666666;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_pager_test);
		//Action Bar Sherlock
				actionbar=getSupportActionBar();
				actionbar.setTitle("PagerIndicator");
				actionbar.show();

				tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs2);
				pager = (ViewPager) findViewById(R.id.pager);
				adapter = new MyPagerAdapter(getSupportFragmentManager());

				pager.setAdapter(adapter);

				final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
						.getDisplayMetrics());
				pager.setPageMargin(pageMargin);

				pager.setOffscreenPageLimit(pager.getAdapter().getCount()-1);

				tabs.setViewPager(pager);

				tabs.setOnPageChangeListener(new OnPageChangeListener() {

					@Override
					public void onPageSelected(int position) {
						// TODO Auto-generated method stub
						Toast.makeText(getApplicationContext(), ""+position, Toast.LENGTH_SHORT).show();	
					}

					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onPageScrollStateChanged(int arg0) {
						// TODO Auto-generated method stub

					}
				});


		/*		changeColor(currentColor);*/
	}
	
	private void changeColor(int newColor) {

		tabs.setIndicatorColor(newColor);

		Drawable colorDrawable = new ColorDrawable(newColor);
		Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });

		if (oldBackground == null) {

			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
				ld.setCallback(drawableCallback);
			} else {
				actionbar.setBackgroundDrawable(ld);
			}

		} else {

			TransitionDrawable td = new TransitionDrawable(new Drawable[] { oldBackground, ld });

			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
				td.setCallback(drawableCallback);
			} else {
				actionbar.setBackgroundDrawable(td);
			}

			td.startTransition(200);

		}

		oldBackground = ld;

		currentColor = newColor;

		// http://stackoverflow.com/questions/11002691/actionbar-setbackgrounddrawable-nulling-background-from-thread-handler
		actionbar.setDisplayShowTitleEnabled(false);
		actionbar.setDisplayShowTitleEnabled(true);

	}



	@SuppressLint("NewApi")
	public void onColorClicked(View v) {

		int color = ((ColorDrawable) v.getBackground()).getColor();
		changeColor(color);

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("currentColor", currentColor);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		currentColor = savedInstanceState.getInt("currentColor");
		changeColor(currentColor);
	}

	private Drawable.Callback drawableCallback = new Drawable.Callback() {
		@Override
		public void invalidateDrawable(Drawable who) {
			actionbar.setBackgroundDrawable(who);
		}

		@Override
		public void scheduleDrawable(Drawable who, Runnable what, long when) {
			handler.postAtTime(what, when);
		}

		@Override
		public void unscheduleDrawable(Drawable who, Runnable what) {
			handler.removeCallbacks(what);
		}
	};

	public class MyPagerAdapter extends FragmentStatePagerAdapter {

		private final String[] TITLES = { "Location", "Register", "Business Hours", "Description" ,"Hours"};

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return TITLES[position];
		}

		@Override
		public int getCount() {
			return TITLES.length;
		}

		@Override
		public Fragment getItem(int position) {
			return new SuperAwesomeCardFragment(position);
		}

	}

}

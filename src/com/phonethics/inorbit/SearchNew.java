package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.ActionAdapter;
import com.phonethics.inorbit.StoreListing.SearachBroadcast;
import com.phonethics.model.RequestTags;
import com.phonethics.model.StoreInfo;

import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SearchNew extends SherlockFragmentActivity implements OnNavigationListener {

	private Activity 			mContext;
	private ActionBar 			mActionbar;
	private CheckBox mChkBox;
	private EditText mEtSearch;
	private Button mBtnSearchLoadMore;
	private PullToRefreshListView mListSearchResult;
	private RelativeLayout mCategoryLayout;
	private RelativeLayout mSearchListFilter;
	private RelativeLayout mRelListResult;
	private TextView mTxtSearchCounter;
	private TextView mTxtSearchCounterBack;
	private ImageView mImgSearchButton;
	private ImageView mImgSearchClose;
	private ProgressBar mSearchListProg;
	private DBUtil mDbutil;
	private ArrayList<String>	mArrMallAreas;
	private ArrayList<StoreInfo> mArrStores;
	private SessionManager mSession;





	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_new);

		mContext 	= this;

		initViews();
		initActionBar();
		initObjects();
		creatActionBarList();


	}

	void initViews(){

		mChkBox		= (CheckBox)findViewById(R.id.chkBox);
		//mChkBox.setOnCheckedChangeListener(this);
		mEtSearch	= (EditText)findViewById(R.id.edtSearch);
		mBtnSearchLoadMore = (Button)findViewById(R.id.btnSearchLoadMore);

		mListSearchResult = (PullToRefreshListView)findViewById(R.id.listSearchResult);
		mCategoryLayout	= (RelativeLayout)findViewById(R.id.categoryLayout);
		mSearchListFilter= (RelativeLayout)findViewById(R.id.searchListFilter);
		mRelListResult	= (RelativeLayout)findViewById(R.id.relListResult);
		mTxtSearchCounter= (TextView)findViewById(R.id.txtSearchCounter);
		mTxtSearchCounterBack	= (TextView)findViewById(R.id.txtSearchCounterBack);
		mImgSearchButton	= (ImageView)findViewById(R.id.imgSearchButton);
		mImgSearchClose	= (ImageView)findViewById(R.id.imgSearchClose);
		mSearchListProg	= (ProgressBar)findViewById(R.id.searchListProg);

	}

	void initObjects(){
		mDbutil 	= new DBUtil(mContext);	
		mSession	= new SessionManager(getApplicationContext());
		mArrMallAreas	= new ArrayList<String>();
		mArrStores		= new ArrayList<StoreInfo>();

	}
	void initActionBar(){
		mActionbar	= getSupportActionBar();
		mActionbar.setTitle(getString(R.string.actionBarTitle));
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.show();
	}

	void creatActionBarList(){
		mArrMallAreas = mDbutil.getAllAreas();
		ActionAdapter mAdAction = new ActionAdapter(mContext, 0, 0, mArrMallAreas);
		mActionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		mActionbar.setListNavigationCallbacks(mAdAction, this);
		mActionbar.setSelectedNavigationItem(mArrMallAreas.indexOf(getActiveAreaName()));
	}


	String getActiveAreaId(){
		return mDbutil.getActiveMallId();
	}

	String getActiveAreaName(){
		return mDbutil.getAreaNameByInorbitId(getActiveAreaId());
	}

	String getMallName(){
		return mArrMallAreas.get((mActionbar.getSelectedNavigationIndex())+1);
	}

	String getCurrentMallId(){
		return mDbutil.getMallIdByPos(""+((mActionbar.getSelectedNavigationIndex())+1));
	}

	String getCurrentMallPlaceParent(){
		return mDbutil.getMallPlaceParentByMallID(getCurrentMallId());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		Intent intent=new Intent(mContext,HomeGrid.class);
		startActivity(intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		String value = mDbutil.getMallIdByPos(""+(itemPosition+1));
		mDbutil.getMallCity(value);
		mDbutil.setActiveMall(value);
		mArrStores.clear();
		loadCategories();

		return false;
	}


	void searchStores(){
		try{

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void loadCategories(){
		
		try{
			HashMap<String, String> _mHeaders = new HashMap<String, String>();
			_mHeaders.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			MyClass myClass = new MyClass(mContext);
			myClass.getStoreRequest(RequestTags.Tag_LoadCategories, null, _mHeaders);
			mSearchListProg.setVisibility(View.VISIBLE);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	private class CategoriesRecevier extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			try{
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
		}
		
	}
	
}

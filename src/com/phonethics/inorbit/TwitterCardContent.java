package com.phonethics.inorbit;

import java.net.MalformedURLException;
import java.net.URL;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.phonethics.inorbit.R;

public class TwitterCardContent extends SherlockActivity{
	private WebView webview;
	ActionBar actionBar;
	ProgressBar aboutWebProg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Uri uri = getIntent().getData();
		URL url= null;
		try {
			url = new URL("http://inorbit.in/contest"); //new URL(uri.getScheme(), uri.getHost(), uri.getPath());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setContentView(R.layout.twittercard);
		webview = (WebView) findViewById(R.id.webviewdata);
		
		super.onCreate(savedInstanceState);
		
		actionBar=getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		aboutWebProg=(ProgressBar)findViewById(R.id.aboutWebProg);
		webview.setWebViewClient(new MyWebViewClient());
		
		/*
		webview.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
			    return true;
			}
		});
		*/
		//webview.setLongClickable(false);
		if (url != null)
			webview.loadUrl(url.toString());
	}
	
	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {


			Log.d("", "Inorbit url>> "+url);
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onLoadResource(WebView  view, String  url){

		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			aboutWebProg.setVisibility(View.GONE);
			super.onPageFinished(view, url);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			aboutWebProg.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			// TODO Auto-generated method stub
			aboutWebProg.setVisibility(View.GONE);
			super.onReceivedError(view, errorCode, description, failingUrl);
		}
	}    
}

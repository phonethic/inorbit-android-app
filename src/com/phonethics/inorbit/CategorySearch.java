package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Animation.AnimationListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;

import com.phonethics.model.RequestTags;
import com.phonethics.model.StoreInfo;
import com.squareup.picasso.Picasso;
public class CategorySearch extends SherlockActivity  implements  OnNavigationListener {


	static Activity context;
	Context	ctext;

	//Actionbar 
	ActionBar actionbar;

	static String API_HEADER;
	static String API_VALUE;

	//Network 
	NetworkCheck isnetConnected;
	static String PHOTO_PARENT_URL;

	//Session Manger
	static SessionManager session;

	//Listview
	PullToRefreshListView listCategSearchResult;
	TextView txtCategSearchCounter;
	TextView txtCategSearchCounterBack;
	ProgressBar searchCategListProg;
	Button btnCategSearchLoadMore;


	//Search

	ArrayList<StoreInfo> STORE_INFOS = new ArrayList<StoreInfo>();
	String TOTAL_SEARCH_PAGES="";
	String TOTAL_SEARCH_RECORDS="";

	int search_post_count=20;
	int search_page=1;


	static String offer="";
	String business_id="292";
	String category_id="";

	boolean isCheckOffers=false;
	boolean isCategory=true;

	int sposition=0;

	String locality="Lokhandwala";

	boolean isRefreshing=false;



	String latitued="";
	String longitude="";

	static boolean IS_SHOPLOCAL=false;

	ArrayList<String>dropdownItems=new ArrayList<String>();
	//String JSON_STRING;
	String key = "";
	String mallId;
	String place_id;

	int dropdown_position;
	static DBUtil	dbUtil;
	CategoreBroadcast broadcast;
	SearchApiListAdapter1 adapter1;
	EditText searchText;
	int textlength=0;
	ImageView imgSerach,imgConnection;

	LinearLayout searchBox;
	int scwidth;
	private String MALL_NAME;
	private String categoryName;
	boolean DATA_LOADING = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category_search);

		//Initializing context
		context=this;
		ctext = this;
		dbUtil = new DBUtil(context);
		business_id=getResources().getString(R.string.shoplocal_id);
		key = getResources().getString(R.string.id_key_shop);

		scwidth =  getWindowManager().getDefaultDisplay().getWidth();

		isnetConnected=new NetworkCheck(context);
		//imageLoaderList=new ImageLoader(context);


		//Action bar
		actionbar=getSupportActionBar();
		actionbar.setTitle(getString(R.string.actionBarTitle));
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();

		searchBox = (LinearLayout) findViewById(R.id.searchBox);

		Bundle bundle=getIntent().getExtras();
		if(bundle!=null)
		{
			category_id=bundle.getString("category_id");
			categoryName = bundle.getString("category_name");
			offer=bundle.getString("offer");
			isCategory=bundle.getBoolean("isCategory");
			if(!isCategory){
				isCheckOffers=true;
			}
		}

		//Photo url
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		//Api Key
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		//List Navigation
		Context acontext = getSupportActionBar().getThemedContext();



		dropdownItems.clear();
		dropdownItems = dbUtil.getAllAreas();

		//Create Adapter
		CustomSpinnerAdapter business_list=new CustomSpinnerAdapter(context, 0, 0, dropdownItems);


		//Setting Navigation Type.
		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionbar.setListNavigationCallbacks(business_list, this);
		String activeAreaId = dbUtil.getActiveMallId();
		place_id = dbUtil.getMallPlaceParentByMallID(activeAreaId);
		String areaName = dbUtil.getAreaNameByInorbitId(activeAreaId);
		int areaPos = dropdownItems.indexOf(areaName);
		actionbar.setSelectedNavigationItem(areaPos);


		//Session Manager
		session=new SessionManager(getApplicationContext());

		listCategSearchResult=(PullToRefreshListView)findViewById(R.id.listCategSearchResult);
		txtCategSearchCounter=(TextView)findViewById(R.id.txtCategSearchCounter);
		txtCategSearchCounterBack=(TextView)findViewById(R.id.txtCategSearchCounterBack);
		searchCategListProg=(ProgressBar)findViewById(R.id.searchCategListProg);
		btnCategSearchLoadMore=(Button)findViewById(R.id.btnCategSearchLoadMore);
		searchText = (EditText) findViewById(R.id.searchText);
		searchText.setVisibility(View.GONE);
		imgSerach = (ImageView) findViewById(R.id.img_serach);

		imgConnection = (ImageView) findViewById(R.id.img_connnection_error);
		imgConnection.setVisibility(View.GONE);
		btnCategSearchLoadMore.setVisibility(View.GONE);

		txtCategSearchCounter.setTypeface(InorbitApp.getTypeFace());
		txtCategSearchCounterBack.setTypeface(InorbitApp.getTypeFace());
		searchText.setTypeface(InorbitApp.getTypeFace());


		adapter1 = new SearchApiListAdapter1(context, STORE_INFOS);
		listCategSearchResult.setAdapter(adapter1);



		//OnRefresh Listener
		listCategSearchResult.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				isRefreshing=true;
				try
				{
					search_page=1;
					if(isnetConnected.isNetworkAvailable()){

						if(!searchCategListProg.isShown()){

							searchApi();
						}
						else{
							MyToast.showToast(context,"Please wait while loading",2);
							listCategSearchResult.onRefreshComplete();
						}
					}
					else{
						isRefreshing=false;
						InorbitLog.d("Refresh complete");
						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
						listCategSearchResult.onRefreshComplete();
						searchCategListProg.setVisibility(View.GONE);

					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

				if(!searchCategListProg.isShown()){
					listCategSearchResult.onRefreshComplete();
				}
			}
		});



		searchText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				textlength = searchText.getText().length();
				String text = searchText.getText().toString();
				if(Conifg.useServices_c){

				}else{
					final ArrayList<StoreInfo> STORE_INFOS_Temp = new ArrayList<StoreInfo>();
					for (int i = 0; i < STORE_INFOS.size(); i++){
						StoreInfo storeInfo  = STORE_INFOS.get(i);
						if(textlength <= storeInfo.getName().length()){

							if (text.equalsIgnoreCase((String) storeInfo.getName().subSequence(0, textlength))){
								STORE_INFOS_Temp.add(storeInfo); 
								InorbitLog.d(searchText.getText().toString());
							}else if(storeInfo.getDescription().contains(text)){
								STORE_INFOS_Temp.add(storeInfo); 
								InorbitLog.d(searchText.getText().toString());
							}
						}else if(text.contains(text)){
							STORE_INFOS_Temp.add(storeInfo); 
							InorbitLog.d(searchText.getText().toString());
						}
					}
					imgConnection.setVisibility(View.GONE);
					int storeSize = STORE_INFOS_Temp.size();
					if(storeSize==0){
						txtCategSearchCounter.setText("No Store");
					}else if(storeSize==1){
						txtCategSearchCounter.setText(storeSize+" Store");
					}else{
						txtCategSearchCounter.setText(storeSize+" Stores");
					}
					SearchApiListAdapter1 adapter_temp = new SearchApiListAdapter1(context, STORE_INFOS_Temp);
					listCategSearchResult.setAdapter(adapter_temp);


					listCategSearchResult.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View view,
								int position, long arg3) {
							// TODO Auto-generated method stub
							try{

								if(!isRefreshing)
								{

									Intent intent=new Intent(ctext, StoreDetails.class);
									intent.putExtra("STORE_ID", STORE_INFOS_Temp.get(position-1).getId());
									intent.putExtra("STORE", STORE_INFOS_Temp.get(position-1).getName());
									intent.putExtra("PHOTO", STORE_INFOS_Temp.get(position-1).getImage_url());
									intent.putExtra("AREA", STORE_INFOS_Temp.get(position-1).getArea());
									intent.putExtra("CITY", STORE_INFOS_Temp.get(position-1).getCity());
									intent.putExtra("MOBILE_NO", STORE_INFOS_Temp.get(position-1).getMob_no1());
									intent.putExtra("MALL_NAME", MALL_NAME);
									intent.putExtra("STORE_INFO", STORE_INFOS_Temp.get(position-1));
									startActivity(intent);
									overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

								}

							}catch(Exception ex){
								ex.printStackTrace();
							}
						}
					});

				}



			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		imgSerach.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(searchText.isShown()){

					//searchText.setVisibility(View.GONE);
					searchText.setText("");
					imgSerach.setBackgroundResource(R.drawable.action_search_holo_light);


					Animation anim = new ScaleAnimation(1f, 1f, 1, 0, 50, 0);
					anim.setDuration(500);
					searchText.startAnimation(anim);
					anim.setFillAfter(true);

					anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub

							searchText.setVisibility(View.GONE);
							InputMethodManager imm = (InputMethodManager)getSystemService( Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);

						}
					});

				}else{


					searchText.setVisibility(View.VISIBLE);
					imgSerach.setBackgroundResource(R.drawable.navigation_cancel_holo_light);
					Animation anim = new ScaleAnimation(1f, 1f, 0, 1, 100, 0);
					anim.setDuration(500);
					searchText.startAnimation(anim);
					anim.setFillAfter(true);

					anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							searchText.setOnFocusChangeListener(new OnFocusChangeListener() {

								@Override
								public void onFocusChange(View v, boolean hasFocus) {
									// TODO Auto-generated method stub
									searchText.post(new Runnable() {
										@Override
										public void run() {
											InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
											imm.showSoftInput(searchText, InputMethodManager.SHOW_IMPLICIT);
										}
									});
								}
							});searchText.requestFocus();
						}
					});

				}

			}
		});




	}//onCreate ends here

	class CustomSpinnerAdapter extends ArrayAdapter<String> implements SpinnerAdapter 
	{

		ArrayList<String>name;
		Activity context;
		LayoutInflater layoutInflater;
		public CustomSpinnerAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> name) {
			super(context, resource, textViewResourceId, name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.name=name;
			layoutInflater=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}


		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return name.get(position);
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = layoutInflater.inflate(R.layout.list_menus, parent, false);
			}


			TextView txtMenu = (TextView) convertView.findViewById(R.id.listText);
			txtMenu.setTypeface(InorbitApp.getTypeFace());
			txtMenu.setText(""+getItem(position));

			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = layoutInflater.inflate(R.layout.list_dropdown_item, parent, false);
			}

			TextView txtMenu = (TextView) convertView.findViewById(R.id.txtSpinner);
			txtMenu.setTypeface(InorbitApp.getTypeFace());
			txtMenu.setText(""+getItem(position));
			return convertView;
		}


	}



	void loadEntries(){
		try{



			if(isCategory){
				long lastDate = session.getLastDateOfApi();
				long todaysDate = getTodaysDate();
				long diff = getDaysDiff(lastDate, todaysDate);

				if(diff>=1){
					InorbitLog.d("Difference "+diff);
					if(isnetConnected.isNetworkAvailable()){
						imgConnection.setVisibility(View.GONE);
						dbUtil.deleteCategories();
						searchApi();
					}else{
						showNoInternetConnectionToast();
					}

				}else{

					dbUtil.open();
					TOTAL_SEARCH_RECORDS = dbUtil.getRowCountOfCategoryTable(category_id,place_id)+"";
					dbUtil.close();

					if(TOTAL_SEARCH_RECORDS.equalsIgnoreCase("0")){

						InorbitLog.d("Records for Cat id "+category_id+" = "+TOTAL_SEARCH_RECORDS);

						if(isnetConnected.isNetworkAvailable()){
							imgConnection.setVisibility(View.GONE);
							searchApi();
						}else{
							showNoInternetConnectionToast();
							listCategSearchResult.setVisibility(View.GONE);
						}

					}else{
						//InorbitLog.d("Records for Cat id "+category_id+" = "+TOTAL_SEARCH_RECORDS);
						imgConnection.setVisibility(View.GONE);

						dbUtil.open();
						STORE_INFOS = dbUtil.getCategoryStoreInfo(category_id,place_id);
						TOTAL_SEARCH_RECORDS = dbUtil.getTotalRecordsOfParticularCategory(category_id,place_id);
						TOTAL_SEARCH_PAGES = dbUtil.getTotalPagesOfParticularCategory(category_id,place_id);
						dbUtil.close();
						listCategSearchResult.setVisibility(View.VISIBLE);
						txtCategSearchCounter.setVisibility(View.VISIBLE);
						txtCategSearchCounter.setText(STORE_INFOS.size()+" of "+TOTAL_SEARCH_RECORDS);
						txtCategSearchCounter.setText(STORE_INFOS.size()+" Stores");
						adapter1 = new SearchApiListAdapter1(context, STORE_INFOS);
						listCategSearchResult.setAdapter(adapter1);
						listCategSearchResult.onRefreshComplete();
						listCategSearchResult.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0, View view,
									int position, long arg3) {
								// TODO Auto-generated method stub
								try{

									if(!isRefreshing)
									{

										Intent intent=new Intent(ctext, StoreDetails.class);
										intent.putExtra("STORE_ID", STORE_INFOS.get(position-1).getId());
										intent.putExtra("STORE", STORE_INFOS.get(position-1).getName());
										intent.putExtra("PHOTO", STORE_INFOS.get(position-1).getImage_url());
										intent.putExtra("AREA", STORE_INFOS.get(position-1).getArea());
										intent.putExtra("CITY", STORE_INFOS.get(position-1).getCity());
										intent.putExtra("MOBILE_NO", STORE_INFOS.get(position-1).getMob_no1());
										intent.putExtra("STORE_INFO", STORE_INFOS.get(position-1));
										startActivity(intent);
										overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);




									}
								}catch(Exception ex){
									ex.printStackTrace();
								}
							}
						});

						InorbitLog.d("TOTAL_SEARCH_RECORDS for Cat id "+category_id+" = "+TOTAL_SEARCH_RECORDS);


					}
				}
			}else{
				if(isnetConnected.isNetworkAvailable()){
					imgConnection.setVisibility(View.GONE);
					searchApi();
				}else{
					showNoInternetConnectionToast();
					listCategSearchResult.setVisibility(View.GONE);

				}
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	//call search api
	void searchApi()
	{


		txtCategSearchCounter.setText("Loading..");
		searchCategListProg.setVisibility(View.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);
		List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();

		if(isCategory)
		{

			nameValuePairs.add(new BasicNameValuePair(key, place_id));
			nameValuePairs.add(new BasicNameValuePair("category_id", category_id));
			nameValuePairs.add(new BasicNameValuePair("page", search_page+""));
			nameValuePairs.add(new BasicNameValuePair("count", search_post_count+""));
			
		}else if(isCheckOffers){

			nameValuePairs.add(new BasicNameValuePair(key, place_id));
			nameValuePairs.add(new BasicNameValuePair("search", ""));
			nameValuePairs.add(new BasicNameValuePair("offer", offer));
			nameValuePairs.add(new BasicNameValuePair("page", search_page+""));
			nameValuePairs.add(new BasicNameValuePair("count", search_post_count+""));
		}

		MyClass myClass = new MyClass(context);


		imgSerach.setVisibility(View.GONE);
		DATA_LOADING = true;
		long todayDate = getTodaysDate();
		session.storeTodayDate(todayDate);
		myClass.getStoreRequest(RequestTags.TagSearchStores, nameValuePairs, headers);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		Intent intent = new Intent();
		setResult(5, intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		Intent intent = new Intent();
		setResult(5, intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}






	void  showToast(String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}


	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub

		mallId = dbUtil.getMallIdByPos(""+(itemPosition+1));
		place_id = dbUtil.getMallPlaceParentByMallID(mallId);
		MALL_NAME = dropdownItems.get(itemPosition);
		if(getResources().getBoolean(R.bool.fromSharePref)){

		}else{
			dbUtil.setActiveMall(mallId);
			createInhouseAnalyticsEvent("0", "8", dbUtil.getActiveMallId());
		}
		registerEvent(getResources().getString(R.string.category_List));


		clearArray();
		loadEntries();

		return false;
	}





	void clearArray(){

		search_page=1;
		search_post_count=-1;
		STORE_INFOS.clear();

	}





	protected class CategoreBroadcast extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Bundle bundle = null;
			ArrayList<StoreInfo> storeInfos;
			try{
				bundle = intent.getExtras();
				if(bundle!=null){
					searchCategListProg.setVisibility(View.GONE);
					String  SEARCH_STATUS = bundle.getString("SUCCESS");
					if(SEARCH_STATUS.equalsIgnoreCase("true")){
						imgSerach.setVisibility(View.VISIBLE);
						DATA_LOADING = false;
						listCategSearchResult.setVisibility(View.VISIBLE);
						txtCategSearchCounter.setVisibility(View.VISIBLE);
						TOTAL_SEARCH_PAGES = bundle.getString("TOTAL_SEARCH_PAGES");
						TOTAL_SEARCH_RECORDS = bundle.getString("TOTAL_SEARCH_RECORDS");
						storeInfos = intent.getParcelableArrayListExtra("StoreInfo");
						if(isRefreshing)
						{
							clearArray();
							isRefreshing=false;
							dbUtil.deleteStoresOfCategory(category_id,place_id);
						}
						for(int i=0;i<storeInfos.size();i++){
							STORE_INFOS.add(storeInfos.get(i));
						}

						if(storeInfos!=null){
							final int old_pos = listCategSearchResult.getRefreshableView().getFirstVisiblePosition();

							adapter1.notifyDataSetChanged();

							txtCategSearchCounter.setText(STORE_INFOS.size()+" of "+TOTAL_SEARCH_RECORDS);
							txtCategSearchCounter.setText(STORE_INFOS.size()+" Stores");
							listCategSearchResult.onRefreshComplete();
							long entries_Affected=0;
							if(isCategory){
								for(int i=0;i<STORE_INFOS.size();i++){

									if(dbUtil.isStoreExists(STORE_INFOS.get(i).getId(),category_id)){

									}else{

										dbUtil.create_category_Table(STORE_INFOS, category_id, TOTAL_SEARCH_PAGES, TOTAL_SEARCH_RECORDS);
									}
								}
							}else{

							}



							InorbitLog.d("Entries Affted "+entries_Affected);


							listCategSearchResult.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> arg0, View view,
										int position, long arg3) {
									// TODO Auto-generated method stub
									try{

										if(!isRefreshing)
										{
											Intent intent=new Intent(ctext, StoreDetails.class);
											intent.putExtra("STORE_ID", STORE_INFOS.get(position-1).getId());
											intent.putExtra("STORE", STORE_INFOS.get(position-1).getName());
											intent.putExtra("PHOTO", STORE_INFOS.get(position-1).getImage_url());
											intent.putExtra("AREA", STORE_INFOS.get(position-1).getArea());
											intent.putExtra("CITY", STORE_INFOS.get(position-1).getCity());
											intent.putExtra("MOBILE_NO", STORE_INFOS.get(position-1).getMob_no1());
											intent.putExtra("STORE_INFO", STORE_INFOS.get(position-1));
											startActivity(intent);
											overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

										}
										//}
									}catch(Exception ex){
										ex.printStackTrace();
									}
								}
							});
						}
					}else{
						isRefreshing=false;
						searchCategListProg.setVisibility(View.GONE);




						STORE_INFOS.clear();
						adapter1 = new SearchApiListAdapter1(context, STORE_INFOS);
						listCategSearchResult.setAdapter(adapter1);

						if(STORE_INFOS.size()!=0){
							txtCategSearchCounter.setText(STORE_INFOS.size()+" Stores");
						}else{
							txtCategSearchCounter.setText("");
						}

						String message="";

						message=bundle.getString("MESSAGE");
						listCategSearchResult.onRefreshComplete();

						InorbitLog.d(message);
						Toast.makeText(context, message,Toast.LENGTH_SHORT).show();


						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){
							int code1 = bundle.getInt("CODE");
							message=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CategorySearch");
							}

							Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
						}
					}
				}
			}catch(Exception ex){
				searchCategListProg.setVisibility(View.GONE);
				ex.printStackTrace();
			}
		}

	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter filter = new IntentFilter(RequestTags.TagSearchStores);
			context.registerReceiver(broadcast = new CategoreBroadcast(), filter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	static class SearchApiListAdapter1 extends ArrayAdapter<StoreInfo>{

		ArrayList<StoreInfo> storeInfos;
		Context context;
		LayoutInflater inflate;

		public SearchApiListAdapter1(Context context, int resource) {
			super(context, resource);
			// TODO Auto-generated constructor stub
		}

		public SearchApiListAdapter1(Context context, ArrayList<StoreInfo> storeInfos) {
			super(context, R.layout.layout_search_api);
			// TODO Auto-generated constructor stub

			this.context 	= context;
			this.storeInfos = storeInfos;
			inflate=((Activity) context).getLayoutInflater();
		}




		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return storeInfos.size();
		}


		@Override
		public StoreInfo getItem(int position) {
			// TODO Auto-generated method stub
			return storeInfos.get(position);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.layout_search_api,null);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.imgStoreLogo.setScaleType(ScaleType.FIT_CENTER);
				holder.txtStore=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.txtDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				holder.textBookTicket = (TextView)convertView.findViewById(R.id.textBookticket);
				holder.txtSearchStoreTotalLike=(TextView)convertView.findViewById(R.id.txtSearchStoreTotalLike);
				holder.band=(TextView)convertView.findViewById(R.id.band);
				convertView.setTag(holder);

				holder.txtStore.setTypeface(InorbitApp.getTypeFaceTitle());
				holder.txtDistance.setTypeface(InorbitApp.getTypeFace());
				holder.txtSearchStoreDesc.setTypeface(InorbitApp.getTypeFace());
				holder.txtSearchStoreTotalLike.setTypeface(InorbitApp.getTypeFace());
				holder.band.setTypeface(InorbitApp.getTypeFace());
				holder.band.setTextColor(Color.BLACK);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			if(!getItem(position).getImage_url().toString().equalsIgnoreCase(""))
			{
				String photo=getItem(position).getImage_url().replaceAll(" ", "%20");

				try {

					Picasso.with(context).load(PHOTO_PARENT_URL+photo)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(hold.imgStoreLogo);

				}catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){
					e.printStackTrace();
				}


			}
			else
			{
				hold.imgStoreLogo.setImageResource(R.drawable.ic_launcher);
			}
			try{
				hold.txtStore.setText(getItem(position).getName());
				hold.txtSearchStoreDesc.setText(getItem(position).getDescription());

				hold.band.setVisibility(View.VISIBLE);
				if(getItem(position).getHas_offer().toString().length()!=0 && getItem(position).getHas_offer().equalsIgnoreCase("1")){

					hold.band.setText("Special Offer : "+getItem(position).getTitle());

				}
				else{
					hold.band.setVisibility(View.GONE);
				}

				if(getItem(position).getName().equalsIgnoreCase("Inox") 
						|| getItem(position).getName().equalsIgnoreCase("Cinemax") 
						|| getItem(position).getName().equalsIgnoreCase("Cinepolis")) {
					hold.textBookTicket.setText("Book Ticket");
					hold.textBookTicket.setVisibility(View.VISIBLE);
					hold.textBookTicket.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							try{

								HashMap<String, String> params = new HashMap<String, String>();
								String url="";
								String activeAreaId = dbUtil.getActiveMallId();
								String place_id = dbUtil.getMallPlaceParentByMallID(activeAreaId);
								String areaName = dbUtil.getAreaNameByInorbitId(activeAreaId);
								
								createInhouseAnalyticsEvent(getItem(position).getId(), "6", activeAreaId);
								
								if(areaName.equalsIgnoreCase("Malad")){
									params.put("Mall","Malad");
									url = context.getResources().getString(R.string.inox_malad);
									EventTracker.logEvent(context.getResources().getString(R.string.book_ticket), params);
								}else if(areaName.equalsIgnoreCase("Pune")){
									params.put("Mall","Pune");
									url = context.getResources().getString(R.string.inox_pune);
									EventTracker.logEvent(context.getResources().getString(R.string.book_ticket), params);
								}else if(areaName.equalsIgnoreCase("Cyberabad")){
									params.put("Mall","Cyberabad");
									url = context.getResources().getString(R.string.inox_cyberabad);
									EventTracker.logEvent(context.getResources().getString(R.string.book_ticket), params);
								} else if (areaName.equalsIgnoreCase("Vadodara")) {
									params.put("Mall", "Vadodara");
									url = context.getResources().getString(R.string.inox_vadodara);
									EventTracker.logEvent(context.getResources().getString(R.string.book_ticket), params);
								}


								if(!url.equalsIgnoreCase("")){
									Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
									context.startActivity(browserIntent);
								}
							}catch(Exception ex){
								ex.printStackTrace();
							}

						}
					});
				}else{
					hold.textBookTicket.setVisibility(View.GONE);
				}


				//set Total Like
				try
				{

					if(getItem(position).getTotal_like().equalsIgnoreCase("0"))
					{
						hold.txtSearchStoreTotalLike.setVisibility(View.GONE);
					}
					else
					{
						hold.txtSearchStoreTotalLike.setVisibility(View.VISIBLE);
					}
					hold.txtSearchStoreTotalLike.setText(getItem(position).getTotal_like());




				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}

			return convertView;
		}


		static class ViewHolder
		{
			ImageView imgStoreLogo;
			TextView txtStore;
			TextView txtSearchStoreDesc;
			TextView txtDistance;
			TextView band,txtSearchStoreTotalLike;
			TextView textBookTicket;
		}


	}




	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		try{
			EventTracker.endFlurrySession(getApplicationContext());
			if(broadcast!=null){
				context.unregisterReceiver(broadcast);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	public long getTodaysDate(){

		long todaysInMillSec = 0;
		Calendar cal = Calendar.getInstance();
		todaysInMillSec = cal.getTimeInMillis();
		InorbitLog.d("Time "+todaysInMillSec);
		return todaysInMillSec;


	}

	public long getDaysDiff(long lastDate, long todayDate){
		long diff = todayDate - lastDate;
		long days =  diff / (24 * 60 * 60 * 1000);
		return days;
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}

	void showNoInternetConnectionToast(){
		imgConnection.setVisibility(View.VISIBLE);
		txtCategSearchCounter.setText("No Internet Connection");
		MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
		listCategSearchResult.onRefreshComplete();
	}



	void registerEvent(String eventName){
		try{
			InorbitLog.d("Category Name "+categoryName);
			boolean params = false;
			Map<String, String> param = new HashMap<String, String>();

			if(MALL_NAME!=null && !MALL_NAME.equalsIgnoreCase("")){
				param.put("MallName",MALL_NAME);
				params = true;
			}else{
				params = false;
			}

			if(categoryName!=null && !categoryName.equalsIgnoreCase("")){
				if(params){
					param.put("Category", categoryName+"-"+MALL_NAME);
					params = true;
				}else{
					params = false;
				}

			}else{
				params = false;
			}

			if(params){
				EventTracker.logEvent(eventName, false);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private static void createInhouseAnalyticsEvent(String flag, String activityId, String mallId) {
		Uri uri = new Uri.Builder().scheme("content").authority(context.getResources().getString(R.string.authority)).
				appendPath("/insert").build();
        ContentValues cv = new ContentValues(6);
		cv.put("UDM_ID", session.getUdmIDForCustomer());
		cv.put("DATE", new Date().toString());
		cv.put("FLAG", flag);
		cv.put("ACTIVITY_ID", activityId);
		cv.put("MALL_ID", mallId);
		cv.put("SYNC_STATUS", -1);
		context.getContentResolver().insert(uri, cv);
	}

}

package com.phonethics.inorbit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils.TruncateAt;
import android.util.Base64;
import android.util.Config;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.BadTokenException;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.CustomerDetails;
import com.phonethics.model.RequestTags;
import com.phonethics.notification.LocalNotification;
import eu.janmuller.android.simplecropimage.CropImage;

public class CustomerProfile extends SherlockActivity{

	private ActionBar 			mActionBar;
	private ImageView 			mImvProfile;
	private ProfilePictureView profilePictureView;

	private EditText 			mEtCustomerName,mEtCustomerEmail,mEtCustomerCity,mEtCustomerState;
	private TextView 			mEtCustomerDob;
	private Button 				mBtnCreateProfile,mBtnLoginFb;

	private RadioGroup 			mRadioGroup;
	private RadioButton 		mRbGenMale,mRbFemale;
	private Activity 			mContext;
	private static final List<String> mListPERMISSIONS = Arrays.asList("email","user_birthday","user_location");
	private boolean 		mbPendingPublishReauthorization = false;
	private String 			msFbUserid	= "";
	private String 			msFbAccessToken	= "";
	private ProgressBar		mpBar;
	private Uri 			mImageCaptureUri;
	private File     		mFileTemp;
	private LinearLayout 	mllDateViewParent;
	//Activity request
	public static final int mREQUEST_CODE_GALLERY      = 0x1;
	public static final int mREQUEST_CODE_TAKE_PICTURE = 0x4;
	public static final int mREQUEST_CODE_CROP_IMAGE   = 0x3;

	public static final int mREQ_CODE_CLICK_IMAGE = 20; //click from camera
	public static final int mREQ_CODE_GALLERY_IMAGE = 21; // select from gallery
	private String 			msFilePath ="",mSGender="",msFileName="",msFileType="",INTERESTED_URL;
	private Dialog 			mddialog,mdRemoteDialog;
	private boolean 		mbIsImageBroadCast;
	private SessionManager 	mSession;

	private static final String msKEY_USER_ID_CUSTOMER="user_id_CUSTOMER";
	private static final String msKEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";
	private String msUser_id	= "";
	private String msAuth_id	= "";
	private boolean mbIsLoggedInCustomer, mbLoginThroughFB = false;
	private Calendar mcalDateTime = Calendar.getInstance();
	private String 	msProfilePicUrl, msUserIdFB, msAccessTokenFB;
	private String 	msNowAsString ;

	protected int 							miCompareIndexes;
	//Image loader
	private ImageLoader 					mIlmageLoader;
	private DisplayImageOptions 			mdiOptions;
	private ImageLoaderConfiguration 		mIlconfig;
	private File 							mfCacheDir;
	//private NetworkCheck 					network;
	private Bitmap 							mbitmapImage=null;
	private String 							msEncodedpath="";
	private Dialog 							mdInterestedAreaDialog;

	private ArrayList<String> 				msArrName;

	private ArrayList<String> 				msArrIds;

	
	private ArrayList<String> 				msArrAreas;
	private ArrayList<String> 				msArrAreaId;
	private ArrayList<String> 				msArrSelectedAreaName;
	private ArrayList<String> 				msArrSelectedAreaId;
	private ArrayList<String> 				msArrUpdateAreaId = new ArrayList<String>();
	private ArrayList<String> 				msArrUpdateAreaName = new ArrayList<String>();
	private ArrayList<String> 				msArrGetAllInterestedAreaIds = new ArrayList<String>();

	private DBUtil 							mdbUtil;
	private TextView[]  					mTvArrForDatePicker,mTVArrForDateTypePicker;
	private LinearLayout[] 					mTvArrChildLayout;
	private RelativeLayout[] 				mRlArraddMoreBtnLayout;
	private ImageView[] 					mIvArrDeleteImg;
	private boolean 						mbIsBelowDateClicked = false;
	private int 							miPositionOfDateText;
	private TextView 						mTvAddMore;
	private ArrayList<String> 				msArrDateToPass;
	private ArrayList<String> 				msArrIdToPass;
	private boolean 						mbIsError = false;
	private int 							miArrStatusAddDelete[];
	private byte[] 							mbArrProfilepic_byte;
	private String[] 						msArrIdToPasArray;
	private String[] 						msArrDateToPassArray;
	private ImageView 						mIvInfoText;
	private Dialog 							mddailog;
	private Dialog 							mdPrivacy_dailog;
	private Button 							mBtnDesclaimer;
	private TextView 						mTvDesclaimerText;
	private TextView 						mTvPrivacy;
	private WebView 						mWvPrivacyWeb;
	private Button 							mButtonOk;
	private Button 							mBtnProfChooseImage,mBAreaBtn,mBdoneBttn; 
	private int chkCount=0;
	private ListView 						mLvInterestedAreaDialogList;
	private DateCatogoryReceiver 			dateReceiver;
	private CustomerProfileReceiver 		cutomerProfReceiver;
	private UpdateCustomerProfileReceiver 	updateProfReceiver;
	private GetIntrestedMalls 				getIntrestedMallsReciver;
	private UpdateIntrestedMalls 			updateIntrestedMallsReciver;

	private boolean isAllApiResponse = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_customer_profile);

		
		/**
		 * 
		 * Initialize the layout, class variables
		 * 
		 * 
		 */
		Date alsoNow 	= Calendar.getInstance().getTime();
		msNowAsString 	= new SimpleDateFormat("yyyy-MM-dd").format(alsoNow);
		InorbitLog.d("NOWDATE" +  msNowAsString);
		mContext		= this;
		mSession 		= new SessionManager(mContext);
		mIlmageLoader	= ImageLoader.getInstance();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			mfCacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitCustomerCache");
		}else{
			mfCacheDir=mContext.getCacheDir();
		}

		if(mfCacheDir.exists()){
			mfCacheDir.delete();
		}

		if(!mfCacheDir.exists()){
			mfCacheDir.mkdirs();
		}else if(NetworkCheck.isNetConnected(mContext)){

		}

		mIlconfig	= new ImageLoaderConfiguration.Builder(mContext)
		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)
		.discCache(new UnlimitedDiscCache(mfCacheDir))
		.enableLogging()
		.build();

		mIlmageLoader.init(mIlconfig);
		mdiOptions = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();

		mIlmageLoader.clearDiscCache();
		mIlmageLoader.clearMemoryCache();

		HashMap<String,String>user_details	= mSession.getUserDetailsCustomer();
		msUser_id			= user_details.get(msKEY_USER_ID_CUSTOMER).toString();
		msAuth_id			= user_details.get(msKEY_AUTH_ID_CUSTOMER).toString();
		mbIsLoggedInCustomer = mSession.isLoggedInCustomer();

		mpBar 				= (ProgressBar)findViewById(R.id.pBar);
		mpBar.setVisibility(View.INVISIBLE);

		mActionBar			= getSupportActionBar();
		mActionBar.setTitle("My Profile");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();
		mRadioGroup			= (RadioGroup)findViewById(R.id.radioGroup);
		mRbGenMale			= (RadioButton)findViewById(R.id.genMale);
		mRbFemale			= (RadioButton)findViewById(R.id.genFemale);
		mBAreaBtn 			= (Button) findViewById(R.id.areaBtn);
		//Dialog
		mddailog				=	new Dialog(mContext);
		mdPrivacy_dailog		= 	new Dialog(mContext);
		mdPrivacy_dailog.setContentView(R.layout.privacy_policy);
		mdPrivacy_dailog.setTitle("Privacy Policy");
		mdPrivacy_dailog.setCancelable(true);
		mdPrivacy_dailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mWvPrivacyWeb			= (WebView)mdPrivacy_dailog.findViewById(R.id.privacyWeb);
		mButtonOk				= (Button)mdPrivacy_dailog.findViewById(R.id.buttonOk);
		mddailog.setContentView(R.layout.disclaimer_dialog);
		mddailog.setTitle("Why should I update profile?");
		mddailog.setCancelable(true);
		mddailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mBtnDesclaimer		= (Button)mddailog.findViewById(R.id.btnDesclaimer);
		mTvDesclaimerText	= (TextView)mddailog.findViewById(R.id.desclaimerText);
		mTvPrivacy			= (TextView)mddailog.findViewById(R.id.privacy);
		mTvPrivacy.setVisibility(View.GONE);
		mTvPrivacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));
		msArrAreas 				= new ArrayList<String>();
		msArrSelectedAreaId 	= new ArrayList<String>();
		msArrAreaId 			= new ArrayList<String>();
		msArrSelectedAreaName 	= new ArrayList<String>();
		mdbUtil 				= new DBUtil(mContext);
		mImvProfile			= (ImageView)findViewById(R.id.profileThumb);
		profilePictureView  = (ProfilePictureView) findViewById(R.id.img_profilePic);
		mBtnProfChooseImage	= (Button)findViewById(R.id.btnProfChooseImage);
		mEtCustomerName		= (EditText)findViewById(R.id.customerName);
		mEtCustomerDob		= (TextView)findViewById(R.id.customerDob);
		mEtCustomerEmail	= (EditText)findViewById(R.id.customerEmail);
		mEtCustomerCity		= (EditText)findViewById(R.id.customerCity);
		mEtCustomerState	= (EditText)findViewById(R.id.customerState);
		mBtnCreateProfile	= (Button)findViewById(R.id.btnCreateProfile);
		mBtnLoginFb			= (Button)findViewById(R.id.btnCreateProfileFb);
		mllDateViewParent 	= (LinearLayout) findViewById(R.id.dateViewParent);
		msArrDateToPass 	= new ArrayList<String>();
		msArrIdToPass 		= new ArrayList<String>();
		mFileTemp = new File(Environment.getExternalStorageDirectory(), "temp_photo.jpg");
		mImageCaptureUri = Uri.fromFile(mFileTemp);
		mTvAddMore 			= (TextView) findViewById(R.id.AddMore);
		mddialog			= new Dialog(mContext);
		mdRemoteDialog		= new Dialog(mContext);
		mdInterestedAreaDialog = new Dialog(mContext);
		mdInterestedAreaDialog.setContentView(R.layout.dailog_interestedareas);
		mdInterestedAreaDialog.setTitle("My Favourite Inorbit Mall");
		mdInterestedAreaDialog.setCancelable(true);
		mdInterestedAreaDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mLvInterestedAreaDialogList = (ListView) mdInterestedAreaDialog.findViewById(R.id.interestedAreaDialogList);
		mBdoneBttn 			= (Button) mdInterestedAreaDialog.findViewById(R.id.updateInterestedAreaBtn);



		if(NetworkCheck.isNetConnected(mContext)){
			
			getDateProfile();
			
		}else{
			Toast.makeText(mContext, "No Internet Connection", 0).show();
		}
		
		
		mWvPrivacyWeb.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.privacy_url));

		mTvPrivacy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mddailog.dismiss();
				mdPrivacy_dailog.show();
			}
		});

		mButtonOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mdPrivacy_dailog.dismiss();
			}
		});



		mBtnDesclaimer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mddailog.dismiss();
			}
		});

		mIvInfoText=(ImageView)findViewById(R.id.infoText);

		mIvInfoText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mddailog.show();
			}
		});

		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				if(checkedId==mRbGenMale.getId()){
					mSGender	= mRbGenMale.getText().toString();
				}
				else if(checkedId==mRbFemale.getId()){
					mSGender	= mRbFemale.getText().toString();
				}
			}
		});

		mBtnLoginFb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(isAllApiResponse){
					mbLoginThroughFB = true;
					try {
						PackageInfo info = getPackageManager().getPackageInfo("com.phonethics.inorbit",PackageManager.GET_SIGNATURES);
						for (Signature signature : info.signatures) {
							MessageDigest md = MessageDigest.getInstance("SHA");
							md.update(signature.toByteArray());
							Log.d("MY KEY HASH:", "MY KEY HASH:" +Base64.encodeToString(md.digest(), Base64.DEFAULT));
						}
					} catch (NameNotFoundException e) {

					} catch (NoSuchAlgorithmException e) {

					}
					if(NetworkCheck.isNetConnected(mContext)){
						regiserSimpleEvent(getResources().getString(R.string.myProfile_ConnectUsingFB));
						login_facebook();
					}
					else{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}else{
					Toast.makeText(mContext, "Fetching your profile, please Wait..",Toast.LENGTH_SHORT).show();
				}

			}
		});


		mBtnProfChooseImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isAllApiResponse){
					AlertDialog.Builder  alertDialog = new AlertDialog.Builder(mContext);
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setTitle("Inorbit");
					alertDialog.setMessage("Choose Image From :");
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setCancelable(true);
					alertDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							msEncodedpath="";
							mbLoginThroughFB = false;
							openGallery();
						}
					});
					alertDialog.setNegativeButton("Take Picture", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							msEncodedpath="";
							mbLoginThroughFB = false;
							takePicture();
						}
					});
					AlertDialog alert = alertDialog.create();
					alert.show();
				}else{
					Toast.makeText(mContext, "Fetching your profile, please Wait..",Toast.LENGTH_SHORT).show();
				}

			}
 

		});

		
		
		mBtnCreateProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(NetworkCheck.isNetConnected(mContext)){
					if(isAllApiResponse){
						
						msArrIdToPass.clear();
						msArrDateToPass.clear();
						
						for(int i=0;i<msArrIdToPasArray.length;i++){
							if(!(msArrIdToPasArray[i].equalsIgnoreCase("0")) && !(msArrDateToPassArray[i].equalsIgnoreCase("0"))){
								Log.d("PLACESTOFILL","PLACESTOFILL " + i);
								msArrIdToPass.add(msArrIdToPasArray[i]);
								msArrDateToPass.add(msArrDateToPassArray[i]);
							}
						}

						if(!mbIsLoggedInCustomer){
							
							showToast("Please login to update profile.");
							Intent intent=new Intent(mContext,LoginSigbUpCustomerNew.class);
							mContext.startActivityForResult(intent,2);
							overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);
							
						}else{
							
							if(mEtCustomerEmail.getText().length()>0 && !isValidEmail(mEtCustomerEmail.getText())){
								
								showToast("Email id is not valid.");
								
							}else{
								for(int i=0; i<mTvArrForDatePicker.length;i++){ 
									if (mTvArrForDatePicker[i].getVisibility() == View.VISIBLE){
										if(mTVArrForDateTypePicker[i].getText().toString().trim().length() == 0){
											if(mTvArrForDatePicker[i].getText().toString().length()!=0){
												mbIsError = true;
												break;
											}
										}else{
											mbIsError = false;
										}
									}
								}
								Log.d("ISERROR", "ISERROR" + mbIsError);
								if(mbIsError){
									
									showToast("Please fill date type");
								}else{
									
									for(int i=0;i<mTvArrForDatePicker.length;i++){
										if(mTvArrForDatePicker[i].getVisibility()==View.VISIBLE){
											if((mTvArrForDatePicker[i].getText().toString().length())>0){
												chkCount = 1;
												Log.d("COUNT","COUNT" + chkCount + " " + mTvArrForDatePicker[i].getText().toString());
												break;
											}
										}
									}

									if(chkCount!=1){

									}

									if(msArrUpdateAreaId.size() == 0){
										showToast("Please mark atleast one Inorbit mall as favourite.");
									}else if(chkCount!=1){
										AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
										alertDialog.setIcon(R.drawable.ic_launcher);
										alertDialog.setTitle("Inorbit");
										alertDialog.setMessage("Entering important dates allows merchants to send you special offers around those dates.");
										alertDialog.setIcon(R.drawable.ic_launcher);
										alertDialog.setCancelable(true);
										alertDialog.setPositiveButton("Skip", new DialogInterface.OnClickListener() {

											@Override
											public void onClick(DialogInterface dialog, int which) {

												addProfile();
												callInterestedAreaApi();
											}
										});
										alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

											@Override
											public void onClick(DialogInterface dialog, int which) {


											}
										});
										AlertDialog alert = alertDialog.create();
										alert.show();
									}else{
										addProfile();
										callInterestedAreaApi();
									}

								}

							}

						}
						regiserSimpleEvent(getResources().getString(R.string.myProfile_UpdateProfile));

					}else{
						Toast.makeText(mContext, "Fetching your profile, please wait..", 1).show();
					}
				}else{
					Toast.makeText(mContext, "No Network Connection", 0).show();
				}


			}
		});


		mEtCustomerDob.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mbIsBelowDateClicked = false;
				chooseDate();
			}
		});

		msArrAreas = mdbUtil.getAllAreas();
		msArrAreaId = mdbUtil.getAllMallIds();

		mLvInterestedAreaDialogList.setAdapter(new AreaAdapter(mContext, msArrAreas, msArrSelectedAreaName, msArrSelectedAreaId, msArrAreaId));

		mBAreaBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if(NetworkCheck.isNetConnected(mContext)){
					if(isAllApiResponse){
						mpBar.setVisibility(View.GONE);
						msArrUpdateAreaId.clear();
						msArrUpdateAreaName.clear();
						AreaAdapter areaAdapter = new AreaAdapter(mContext, msArrAreas, msArrSelectedAreaName, msArrGetAllInterestedAreaIds, msArrAreaId);
						mLvInterestedAreaDialogList.setAdapter(areaAdapter);
						mdInterestedAreaDialog.show();

					}else{
						Toast.makeText(mContext, "Fetching your profile, please Wait..",Toast.LENGTH_SHORT).show();
					}

				}else{

					showToast(getResources().getString(R.string.noInternetConnection));
				}



			}
		});


		mBdoneBttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Log.d("Size Unselected","Inorbit Size Unselected " + msArrUpdateAreaName.size());
				for(int i=0;i<msArrUpdateAreaId.size();i++){
					Log.d("Size Unselected","Inorbit selected area name " + msArrUpdateAreaName.get(i)+ " -- "+msArrUpdateAreaId.get(i));	
				}
				if(msArrUpdateAreaId.size() == 0){
					showToast("Favourite atleast one Inorbit Mall");
				}else{
					EventTracker.logEvent("MyProfile_FavouritedMalls", false);
					mdInterestedAreaDialog.dismiss();
					String placeParents = msArrUpdateAreaName.toString();
					String areas = placeParents.substring(1, placeParents.length() - 1).replace(", ", ", ");
					mBAreaBtn.setText(areas);
					
				}
			}
		});

		if(NetworkCheck.isNetConnected(mContext)){
			getInterestedAreas();
		}
		else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}


	}//on create Ends here

	//Validating Email id.
	public final  boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}   

	void showToast(String text)
	{
		Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		try{
			MenuItem extra=menu.add("Log Out");
			extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub


		if(item.getTitle().toString().equalsIgnoreCase("Log Out")){

			AlertDialog.Builder  alertDialog = new AlertDialog.Builder(mContext);
			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog.setTitle("Inorbit");
			alertDialog.setMessage("Are you sure you want to logout?");
			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog.setCancelable(true);
			alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					clearFbToken();

					mSession.logoutCustomer();
					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
				}


			});
			alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {


				}
			});

			AlertDialog alert = alertDialog.create();
			alert.show();
		}else {
			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}

		return true;
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub


		try
		{
			Session.openActiveSession(this, false, new Session.StatusCallback() {


				// callback when session changes state
				@Override
				public void call(Session session, SessionState state, Exception exception) {

					/*if(session.isOpened())
				{	*/
					session.closeAndClearTokenInformation();
					/*exception.printStackTrace();
					}*/
					Log.i("Clearing", "Token");
				}
			});
			//FlurryAgent.endTimedEvent("Gallery_Opened_Event");
		}catch(NullPointerException npx)
		{
			npx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);


	}


	public void login_facebook(){
		try{
			mpBar.setVisibility(View.VISIBLE);
			Session.openActiveSession(mContext, true, new Session.StatusCallback() {

				@Override
				public void call(final Session session, SessionState state, Exception exception) {
					// TODO Auto-generated method stub
					if(session.isOpened())
					{
						showToast("Please wait");
						List<String> permissions = session.getPermissions();

						if (!isSubsetOf(mListPERMISSIONS, permissions)) {
							mbPendingPublishReauthorization = true;
							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(mContext, mListPERMISSIONS);
							session.requestNewReadPermissions(newPermissionsRequest);
						}

						Request request = Request.newMeRequest(session,new Request.GraphUserCallback() {
							@Override
							public void onCompleted(GraphUser user, Response response) {
								// TODO Auto-generated method stub
								if(user!=null)
								{

									try{
										Log.d("", "Fb responcse == "+response.toString());
										Log.d("", "FbUser >> "+ user.toString());
										msFbUserid	= user.getId();
										msFbAccessToken	= session.getAccessToken();
										Log.i("User Name ", "FbName : "+user.getName());
										Log.i("User Id ", "FbId : "+user.getId());
										Log.i("User Id ", "FbLocation : "+user.getLocation().toString());

										Log.i("User Id ", "FbEmailId : "+user.getLink());
										Log.i("User Id ", "FbDob : "+user.getBirthday());

										Log.i("User Id ", "FbGender : "+user.asMap().get("gender").toString());
										Log.i("User Id ", "FbEmail : "+user.getProperty("email").toString());
										//Log.i("User Id ", "FbCity : "+user.getLocation().getCity().toString());
										//Log.i("User Id ", "FbState : "+user.getLocation().getState().toString());
										Log.i("User Access Token ", "FbAccess Token : "+session.getAccessToken());
										mpBar.setVisibility(View.INVISIBLE);

										String name = user.getName();
										String dob = user.getBirthday();
										mSGender = user.asMap().get("gender").toString();

										if(mSGender.equalsIgnoreCase("male")){
											mRbGenMale.setChecked(true);

										}
										else{
											mRbFemale.setChecked(true);

										}

										String email =user.getProperty("email").toString();
										String city = user.getLocation().getProperty("name").toString();
										profilePictureView.setProfileId(user.getId());
										profilePictureView.setVisibility(View.VISIBLE);
										String url="http://graph.facebook.com/"+user.getId()+"/picture?type=large";
										setText(name, dob, email, city, "","",url);

										msUserIdFB = user.getId();

										msAccessTokenFB = session.getAccessToken();

									}catch(Exception ex){
										ex.printStackTrace();
									}


								}

								else{
									mpBar.setVisibility(View.INVISIBLE);
									Log.i("ELSE", "ELSE");
								}
							}


						});
						request.executeAsync();

					}

					if(session.isClosed()){

						mpBar.setVisibility(View.GONE);

						try {

							if ((exception instanceof FacebookOperationCanceledException ||
									exception instanceof FacebookAuthorizationException)) {
								new AlertDialog.Builder(CustomerProfile.this)
								.setTitle("Login Failed")
								.setMessage(exception.getMessage().toString())
								.setPositiveButton("OK", null)
								.show();

							}

						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						Log.d("EXCEPTION","EXCEPTION " + exception);

					}

				}


			});}catch(NullPointerException npx)
			{
				npx.printStackTrace();
			}
		catch(BadTokenException bdx)
		{
			bdx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}




	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try{
			if(requestCode==2){
				
				HashMap<String,String>user_details=mSession.getUserDetailsCustomer();
				msUser_id=user_details.get(msKEY_USER_ID_CUSTOMER).toString();
				msAuth_id=user_details.get(msKEY_AUTH_ID_CUSTOMER).toString();
				
				mbIsLoggedInCustomer=mSession.isLoggedInCustomer();
				
				loadProfile();
			}
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
			
			
		}catch(IllegalStateException ilgx){
			ilgx.printStackTrace();
		}catch (NullPointerException e) {
			// TODO: handle exception
			e.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}

		if (resultCode != RESULT_OK) {
			return;
		}

		Bitmap bitmap;

		switch (requestCode) {

		case mREQUEST_CODE_GALLERY:

			try {

				InputStream inputStream = getContentResolver().openInputStream(data.getData());
				FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
				copyStream(inputStream, fileOutputStream);
				fileOutputStream.close();
				inputStream.close();

				startCropImage();

				msFileType="image/jpeg";
				msFileName="temp_photo.jpg";
				msFilePath="/sdcard/temp_photo.jpg";

			} catch (Exception e) {

				Log.e("", "Error while creating temp file", e);
			}

			break;
		case mREQUEST_CODE_TAKE_PICTURE:
			msFileType="image/jpeg";
			msFileName="temp_photo.jpg";
			msFilePath="/sdcard/temp_photo.jpg";
			startCropImage();
			break;
		case mREQUEST_CODE_CROP_IMAGE:

			msFileType="image/jpeg";
			msFileName="temp_photo.jpg";
			msFilePath="/sdcard/temp_photo.jpg";

			String path = data.getStringExtra(CropImage.IMAGE_PATH);
			if (path == null) {

				return;
			}

			bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
			setPhoto(bitmap);
			if(mddialog.isShowing()){
				mddialog.dismiss();
			}
			else if(mdRemoteDialog.isShowing()){
				mdRemoteDialog.dismiss();
			}
			mbIsImageBroadCast=true;
			break;

		case mREQ_CODE_CLICK_IMAGE:
			if(resultCode == RESULT_OK){
				msFileType="image/jpeg";
				msFileName="temp_photo.jpg";
				msFilePath="/sdcard/temp_photo.jpg";

				mbArrProfilepic_byte = data.getByteArrayExtra("byteArray");
				setPhoto(BitmapFactory.decodeByteArray(mbArrProfilepic_byte , 0, mbArrProfilepic_byte.length));
			}
			break;

		case mREQ_CODE_GALLERY_IMAGE:
			if(resultCode == RESULT_OK){
				msFileType="image/jpeg";
				msFileName="temp_photo.jpg";
				msFilePath="/sdcard/temp_photo.jpg";

				mbArrProfilepic_byte = data.getByteArrayExtra("byteArray");
				setPhoto(BitmapFactory.decodeByteArray(mbArrProfilepic_byte , 0, mbArrProfilepic_byte.length));
			}
			break;
		}

	}


	public void setText(String Name,String DOB, String emailId, String city, String state,String gender,String imgeUrl){

		mEtCustomerName.setText(Name);
		DateFormat  dt=new SimpleDateFormat("yyyy-mm-dd");


		DateFormat df = new SimpleDateFormat("mm/dd/yyyy"); 
		Date startDate;
		String newDateString = null ;
		try {
			startDate = df.parse(DOB);
			newDateString = dt.format(startDate);
			System.out.println(newDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}


		for(int i=0;i<msArrName.size();i++){


			if(i!=0 && mTVArrForDateTypePicker[i].getText().toString().equalsIgnoreCase(msArrName.get(0))){


			}
			else{
				mTvArrChildLayout[0].setVisibility(View.VISIBLE);
				mTvArrForDatePicker[0].setText(newDateString);
				mTVArrForDateTypePicker[0].setText(msArrName.get(0));

				msArrIdToPasArray[0] = "1";
				msArrDateToPassArray[0] = newDateString;
				break;

			}
		}




		mEtCustomerEmail.setText(emailId);
		mEtCustomerCity.setText(city);
		mEtCustomerState.setText(state);
		
		if(mbLoginThroughFB){
			mImvProfile.setVisibility(View.GONE);
			profilePictureView.setVisibility(View.VISIBLE);
		}else{
			profilePictureView.setVisibility(View.GONE);
			mImvProfile.setVisibility(View.VISIBLE);
		}

		mIlmageLoader.displayImage(imgeUrl, mImvProfile, new ImageLoadingListener() {

			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				// TODO Auto-generated method stub
				mpBar.setVisibility(View.VISIBLE);
			}

			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				// TODO Auto-generated method stub
				mpBar.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				// TODO Auto-generated method stub
				mpBar.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
				// TODO Auto-generated method stub
				mpBar.setVisibility(View.GONE);
			}
		});
		mBtnCreateProfile.setText("Update Profile");

		msProfilePicUrl = imgeUrl;
	}
	

	/**
	 * 
	 * Set the users information on layout
	 * 
	 * @param Name
	 * @param DOB
	 * @param emailId
	 * @param city
	 * @param state
	 * @param gender
	 * @param imgeUrl
	 */
	public void setProfile(String Name,String DOB, String emailId, String city, String state,String gender,String imgeUrl){

		try
		{
			Log.d("GENDER","GENDER" + gender);

			mEtCustomerName.setText(Name);

			mEtCustomerEmail.setText(emailId);
			mEtCustomerCity.setText(city);
			mEtCustomerState.setText(state);

			if(gender.equalsIgnoreCase("Male")){

				mRbGenMale.setChecked(true);
			}
			else{

				mRbFemale.setChecked(true);
			}


			mIlmageLoader.displayImage(getResources().getString(R.string.photo_url)+imgeUrl, mImvProfile, new ImageLoadingListener() {

				@Override
				public void onLoadingStarted(String arg0, View arg1) {
					// TODO Auto-generated method stub
					mpBar.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
					// TODO Auto-generated method stub
					mpBar.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
					// TODO Auto-generated method stub
					mpBar.setVisibility(View.GONE);
					mbitmapImage=((BitmapDrawable)mImvProfile.getDrawable()).getBitmap();
					msEncodedpath=encodeTobase64(mbitmapImage);
					msFileType="image/jpeg";
					msFileName="temp_photo.jpg";
				}

				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
					// TODO Auto-generated method stub
					mpBar.setVisibility(View.GONE);
				}
			});
			mBtnCreateProfile.setText("Update Profile");

			msProfilePicUrl = imgeUrl;
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	

	public static String encodeTobase64(Bitmap image)
	{
		Bitmap immagex=image;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] b = baos.toByteArray();
		String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

		Log.e("LOOK", imageEncoded);
		return imageEncoded;
	}
	

	public static Bitmap decodeBase64(String input) 
	{
		byte[] decodedByte = Base64.decode(input, 0);
		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	}


	private void openGallery() {

		Intent intent = new Intent(CustomerProfile.this, PhotoActivity.class);
		intent.putExtra("REQ_CODE_GALLERY_IMAGE", mREQ_CODE_GALLERY_IMAGE);
		startActivityForResult(intent, mREQ_CODE_GALLERY_IMAGE);
	}


	private void takePicture() {



		Intent intent = new Intent(CustomerProfile.this, PhotoActivity.class);
		intent.putExtra("REQ_CODE_CLICK_IMAGE", mREQ_CODE_CLICK_IMAGE);
		startActivityForResult(intent, mREQ_CODE_CLICK_IMAGE);
	}

	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}


	private void startCropImage() {

		Intent intent = new Intent(this, CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.SCALE, true);

		intent.putExtra(CropImage.ASPECT_X, 3);
		intent.putExtra(CropImage.ASPECT_Y, 3);

		startActivityForResult(intent, mREQUEST_CODE_CROP_IMAGE);
	}


	void setPhoto(Bitmap bitmap){
		mImvProfile.setImageBitmap(bitmap);
		mImvProfile.setScaleType(ScaleType.FIT_CENTER);
		mImvProfile.setVisibility(View.VISIBLE);
		profilePictureView.setVisibility(View.GONE);
	}

	
	/**
	 * 
	 * Network request to add/update the user's infomratin on the server
	 * 
	 */
	private void addProfile() {
		if(mbArrProfilepic_byte!=null){
			msEncodedpath=Base64.encodeToString(mbArrProfilepic_byte, Base64.DEFAULT);
			Log.i("Encode ", "Details : "+msEncodedpath);
		}else{
			msEncodedpath="undefined";
		}


		JSONObject json = new JSONObject();
		//Intent intent=new Intent(context, AddCustomerProfile.class);
		if(mbLoginThroughFB){
			mpBar.setVisibility(View.VISIBLE);
			try{
				json.put("name", mEtCustomerName.getText().toString());
				json.put("email", mEtCustomerEmail.getText().toString());
				json.put("gender", mSGender);
				json.put("city",  mEtCustomerCity.getText().toString());
				json.put("state", mEtCustomerState.getText().toString());
				json.put("image_url",msProfilePicUrl);
				json.put("facebook_user_id",msUserIdFB);
				json.put("facebook_access_token",msAccessTokenFB);
				json.put("active_mall", mSession.getActiveMallId());
				json.put("user_id", msUser_id	);
				json.put("auth_id", msAuth_id);
				Log.d("DATES SIZE","DATES SIZE" + msArrDateToPass.size());
				if(msArrDateToPass.size()!=0 && msArrIdToPass.size()!=0){
					JSONArray dates = new JSONArray();
					for(int i=0;i<msArrDateToPass.size();i++){
						JSONObject obj = new JSONObject();
						obj.put("date_type", msArrIdToPass.get(i));
						obj.put("date", msArrDateToPass.get(i));
						dates.put(obj);
					}
					json.put("dates", dates);
				}else{
					json.put("dates", "");
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}



		}else{
			mpBar.setVisibility(View.VISIBLE);

			try{
				json.put("name",  mEtCustomerName.getText().toString());
				json.put("email", mEtCustomerEmail.getText().toString());
				json.put("gender", mSGender);
				json.put("city", mEtCustomerCity.getText().toString());
				json.put("state", mEtCustomerState.getText().toString());
				json.put("active_mall", mSession.getActiveMallId());
				if(msFileName!=null && msEncodedpath!=null){
					json.put("filename",msFileName);
					json.put("filetype",msFileType);
					json.put("userfile",msEncodedpath);
				}

				json.put("user_id", msUser_id);
				json.put("auth_id", msAuth_id);
				if(msArrDateToPass.size()!=0 && msArrIdToPass.size()!=0){
					JSONArray dates = new JSONArray();
					for(int i=0;i<msArrDateToPass.size();i++){
						JSONObject obj = new JSONObject();
						obj.put("date_type", msArrIdToPass.get(i));
						obj.put("date", msArrDateToPass.get(i));
						dates.put(obj);
					}
					json.put("dates", dates);
				}else{
					json.put("dates", "");
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}

		}



		HashMap<String, String> headers =  new HashMap<String, String>();
		headers.put("X-HTTP-Method-Override", "PUT");
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));


		mpBar.setVisibility(View.VISIBLE);
		MyClass myClass = new MyClass(mContext);
		myClass.postRequest(RequestTags.Tag_UpdateCustomerProfile, headers, json);




	}

	/**
	 * 
	 * Network request to load the user's information
	 * 
	 * 
	 */
	private void loadProfile()
	{
		mpBar.setVisibility(View.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		headers.put("user_id", msUser_id);
		headers.put("auth_id", msAuth_id);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("user_id", msUser_id));
		
		MyClass myClass = new MyClass(mContext);
		myClass.getStoreRequest(RequestTags.Tag_CustomerProfile, nameValuePairs, headers);


	}


	//Convert image into byte array
	static 	byte[] imageTobyteArray(String path,int width,int height)
	{	byte[] b = null ;
	if(!path.equals("") || path.length()!=0)
	{


		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inDither=true;//optional
		options.inPreferredConfig=Bitmap.Config.RGB_565;//optional

		Bitmap bm = BitmapFactory.decodeFile(path,options);

		options.inSampleSize = calculateInSampleSize(options, width, height);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		bm=BitmapFactory.decodeFile(path,options);


		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object   
		b= baos.toByteArray();
	}
	return b;
	}

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}


	public void chooseDate(){
		/*	dateTime.add(Calendar.DAY_OF_MONTH, 1);*/
		new DatePickerDialog(mContext, d, mcalDateTime.get(Calendar.YEAR),mcalDateTime.get(Calendar.MONTH),mcalDateTime.get(Calendar.DAY_OF_MONTH)).show();
	}

	public void chooseDate(int pos){
		/*	dateTime.add(Calendar.DAY_OF_MONTH, 1);*/

		miPositionOfDateText  = pos;
		new DatePickerDialog(mContext, d, mcalDateTime.get(Calendar.YEAR),mcalDateTime.get(Calendar.MONTH),mcalDateTime.get(Calendar.DAY_OF_MONTH)).show();
	}



	DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			mcalDateTime.set(Calendar.YEAR,year);
			mcalDateTime.set(Calendar.MONTH, monthOfYear);
			mcalDateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			String ct = DateFormat.getDateInstance().format(new Date());
			String setDate = dt.format(mcalDateTime.getTime());
			if(setDate.compareTo(msNowAsString)>0){
				Toast.makeText(mContext, "Unable to set future date", 0).show();

				if(mbIsBelowDateClicked){
					for(int i=0; i<msArrName.size();i++){
						if(i==miPositionOfDateText)
							mTvArrForDatePicker[i].setText("");
					}

				}else{


				}

			}else{
				if(mbIsBelowDateClicked){
					for(int i=0; i<msArrName.size();i++){
						if(i==miPositionOfDateText){
							mTvArrForDatePicker[i].setText(dt.format(mcalDateTime.getTime()));
							msArrDateToPassArray[i] = dt.format(mcalDateTime.getTime());
						}
					}
				}else{

				}
			}
		}
	};


	/**
	 * 
	 * Load the special dates like Birthday,Anniversary, Spouse birthday...
	 * 
	 */
	private void getDateProfile() {
		// TODO Auto-generated method stub

		mpBar.setVisibility(View.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		MyClass myClass = new MyClass(mContext);
		myClass.makeGetRequest(RequestTags.Tag_SpecialDates, "-1", headers);
	}


	/**
	 * Create the dynamic layout to set the dates
	 * 
	 * @param size
	 */
	private void creatLayout(int size) {
		// TODO Auto-generated method stub

		for(int index=0; index< size; index++){
			
			mTvArrChildLayout[index].setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			lp2.setMargins(10, 10, 10, 10);
			mTvArrChildLayout[index].setLayoutParams(lp2);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
			lp.setMargins(0, 10, 5, 10);


			mTvArrForDatePicker[index].setLayoutParams(lp);
			mTvArrForDatePicker[index].setGravity(Gravity.CENTER);
			mTvArrForDatePicker[index].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.dropdownicon, 0);
			mTvArrForDatePicker[index].setBackgroundResource(R.drawable.shadow_effect);
			mTvArrForDatePicker[index].setHint("Select Date");
			mTvArrChildLayout[index].addView(mTvArrForDatePicker[index]);

			LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.9f);
			lp1.setMargins(0, 10, 5, 10);

			mTVArrForDateTypePicker[index].setLayoutParams(lp1);
			mTVArrForDateTypePicker[index].setGravity(Gravity.CENTER);
			mTVArrForDateTypePicker[index].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.dropdownicon, 0);
			mTVArrForDateTypePicker[index].setBackgroundResource(R.drawable.shadow_effect);
			mTVArrForDateTypePicker[index].setHint("select type of date");
			mTVArrForDateTypePicker[index].setTextSize(12);
			mTVArrForDateTypePicker[index].setEllipsize(TruncateAt.END);
			mTVArrForDateTypePicker[index].setSingleLine(true);
			mTvArrChildLayout[index].addView(mTVArrForDateTypePicker[index]);

			LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			lp3.setMargins(0, 10, 5, 10);

			mIvArrDeleteImg[index].setLayoutParams(lp3);
			mIvArrDeleteImg[index].setImageResource(R.drawable.deletebtn);
			mTvArrChildLayout[index].addView(mIvArrDeleteImg[index]);

			// FOR ADDMORE BUTTON LAYOUT AND ITS TEXT 
			RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			param.setMargins(20, 10, 0, 0);
			if(index!=size-1){

			}
			mllDateViewParent.addView(mTvArrChildLayout[index]);
		}
	}


	public void checkForAvailability(String string, int position, int textPos) {
		// TODO Auto-generated method stub

		boolean passIt = false;
		for(int chk = 0; chk< msArrName.size(); chk++){
			if(position == chk){

			}else{
				if(mTVArrForDateTypePicker[chk].getText().toString().equalsIgnoreCase(string)){
					passIt = false;
					showToast("you have already select the date type");
					mTVArrForDateTypePicker[position].setText("");
					break;
				}else{
					passIt = true;
					mTVArrForDateTypePicker[position].setText(string);
				}
			}

		}
		if(passIt){
			Log.d("IDARRAY","IDARRAY " + position);
			msArrIdToPasArray[position] = msArrIds.get(textPos);
		}

	}


	/**
	 * Array adapter to set the favourite the malls
	 * 
	 * 
	 * @author Nitin
	 *
	 */
	public class AreaAdapter extends ArrayAdapter<String> {

		Activity context;
		LayoutInflater inflator = null;
		ArrayList<String> areaName;
		ArrayList<String> selectedAreaName;
		ArrayList<String> selectedAreaIdArr;
		ArrayList<String> areaIdArr;

		public AreaAdapter(Activity context, ArrayList<String> areaName,ArrayList<String> selectedAreaName, ArrayList<String> selectedAreaIdArr, ArrayList<String> areaIdArr) {
			super(context,R.layout.interestedarea);
			this.context = context;
			//this.data = data;
			this.areaName = areaName;
			this.selectedAreaName = selectedAreaName;
			this.selectedAreaIdArr = selectedAreaIdArr;
			this.areaIdArr = areaIdArr;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			View rowView = convertView;

			final int pos = position;

			if(convertView == null){
				inflator = context.getLayoutInflater();
				rowView = inflator.inflate(R.layout.interestedarea, null);
				ViewHolder holder = new ViewHolder();

				holder.text = (TextView) rowView.findViewById(R.id.areaName);
				holder.areaCheck = (CheckBox) rowView.findViewById(R.id.areaCheckBox);

				holder.areaCheck.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub

						if(buttonView.isChecked()){
							registerMallSelectedEvent(getResources().getString(R.string.myProfile_FavouriteInorbitMall),areaName.get(pos));
							msArrUpdateAreaId.add(areaIdArr.get(pos));
							msArrUpdateAreaName.add(areaName.get(pos));
						}
						else{
							msArrUpdateAreaId.remove(areaIdArr.get(pos));
							msArrUpdateAreaName.remove(areaName.get(pos));
						}

					}
				});
				rowView.setTag(holder);
			}

			ViewHolder hold = (ViewHolder) rowView.getTag();
			hold.text.setText(areaName.get(position).toString());


			if(selectedAreaIdArr.contains(areaIdArr.get(position))){
				hold.areaCheck.setChecked(true);
			}else{
				hold.areaCheck.setChecked(false);
			}

			return rowView;
		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return areaName.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return areaName.get(position);
		}
	}



	static class ViewHolder{
		public TextView text;
		public CheckBox areaCheck;
	}


	/**
	 * 
	 * Network request to add the user's favourite malls
	 * 
	 */
	private void callInterestedAreaApi() {
		// TODO Auto-generated method stub

		mpBar.setVisibility(View.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));


		JSONObject json = new JSONObject();
		try{
			json.put("user_id", msUser_id);
			json.put("auth_id", msAuth_id);
			JSONArray areas = new JSONArray();	
			
			for(int i=0; i<msArrUpdateAreaId.size();i++){
				
				Log.d("AREA_SELECTED","AREA_SELECTED "  + msArrUpdateAreaId.get(i));
			}

			for(int i=0;i<msArrUpdateAreaId.size();i++){
				areas.put(msArrUpdateAreaId.get(i));
			}

			json.put("areas", areas);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		MyClass myClass = new MyClass(mContext);
		myClass.postRequest(RequestTags.Tag_UpdateIntrestedMalls, headers, json);



	}

	/**
	 * Network request to load all the areas of the user like Malad,Vashi...
	 * 
	 */
	private void getInterestedAreas() {
		// TODO Auto-generated method stub
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		headers.put("user_id", msUser_id);
		headers.put("auth_id", msAuth_id);

		MyClass myClass = new MyClass(mContext);
		myClass.makeGetRequest(RequestTags.Tag_GetIntrestedAreas, "-1", headers);
	}


	void setDateCatogories(ArrayList<String> id,ArrayList<String> categories){
		try{

			msArrIds = id;
			msArrName = categories;
			final int size = msArrName.size();
			
			mTvArrForDatePicker = new TextView[size];
			mTVArrForDateTypePicker = new TextView[size];
			mTvArrChildLayout = new LinearLayout[size];
			mRlArraddMoreBtnLayout = new RelativeLayout[size];
			mIvArrDeleteImg = new ImageView[size];
			miArrStatusAddDelete = new int[size];
			msArrIdToPasArray = new String[size];
			msArrDateToPassArray = new String[size];

			for(int indx=0; indx < msArrIdToPasArray.length; indx++){

				msArrIdToPasArray[indx] = "0";
				msArrDateToPassArray[indx] = "0";
			}


			// FOR INITIAL STATUS OF ADD AND DELETE 

			
			for(int k=0;k<miArrStatusAddDelete.length;k++){
				if(k==0){
					miArrStatusAddDelete[k] = 10;
				}else{
					miArrStatusAddDelete[k] = 5;
				}

			}


			for(int k=0;k<size;k++){

				mTvArrForDatePicker[k] = new TextView(mContext);
				mTVArrForDateTypePicker[k] = new TextView(mContext);
				mTvArrChildLayout[k]  = new LinearLayout(mContext);
				mRlArraddMoreBtnLayout[k] = new RelativeLayout(mContext);
				mIvArrDeleteImg[k] = new ImageView(mContext);
			}
			creatLayout(size);
			
			
			// TO SHOW ONLY ONE VIEW AT THE VERY FIRST TIME

			for(int chk=0;chk<size;chk++){
				if(chk!=0){
					mTvArrChildLayout[chk].setVisibility(View.GONE);
				}else{
					
				}
			}

			mTvAddMore.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					miCompareIndexes++;
					for(int m=0;m<miArrStatusAddDelete.length;m++){

						if(miArrStatusAddDelete[m] == 5){
							miArrStatusAddDelete[m] = 10;
							mTvArrChildLayout[m].setVisibility(View.VISIBLE);
							break;
						}
					}
					if(miCompareIndexes==msArrName.size()-1){
						mTvAddMore.setVisibility(View.INVISIBLE);
					}


				}
			});

			for(int j=0;j<size;j++){

				final int copy = j;

				mIvArrDeleteImg[j].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						if(mbIsError){
							mbIsError = false;
						}
						
						miCompareIndexes = miCompareIndexes-1;
						mTVArrForDateTypePicker[copy].setText("");
						mTvArrForDatePicker[copy].setText("");
						mTvArrChildLayout[copy].setVisibility(View.GONE);
						msArrIdToPasArray[copy] = "0";
						msArrDateToPassArray[copy] = "0";
						miArrStatusAddDelete[copy] = 5;
						mTvAddMore.setVisibility(View.VISIBLE);


					}
				});
			}

			// TO GET CLICK OF ALL DATEPICKERS 
			for( int m = 0 ; m< size ;m++){
				final int pos = m;
				mTvArrForDatePicker[m].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						mbIsBelowDateClicked = true;
						chooseDate(pos);
					}
				});
			}

			for(int l =0 ; l <size; l++){

				final int copy = l;
				mTVArrForDateTypePicker[l].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						final CharSequence[] cs = msArrName.toArray(new CharSequence[msArrName.size()]);

						AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
						builder.setTitle("Make your selection");
						builder.setIcon(R.drawable.ic_launcher);
						builder.setItems(cs, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,int which) {
								// TODO Auto-generated method stub

								checkForAvailability(cs[which].toString(), copy, which);
								//forDateTypePicker[copy].setText(cs[which]);

							}

						});
						AlertDialog alert = builder.create();
						alert.show();

					}
				});
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	
	/**
	 * 
	 * BroadcastReceiver for getDateProfile();
	 * @author Nitin
	 *
	 */
	class DateCatogoryReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				EventTracker.logEvent("MyProfile_AddEvent", false);
				mpBar.setVisibility(View.GONE);
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						
						ArrayList<String> id = intent.getStringArrayListExtra("ID");
						ArrayList<String> category = intent.getStringArrayListExtra("CATEGORY");
						
						setDateCatogories(id, category);
						loadProfile();
					}else{
						//isAllApiResponse = false;
						String message = intent.getStringExtra("MESSAGE");
						showToast(message);

						boolean isVolleyError = intent.getBooleanExtra("volleyError",false);
						if(isVolleyError){

							int code1 = intent.getIntExtra("CODE",0);
							String message1=intent.getStringExtra("MESSAGE");
							String errorMessage = intent.getStringExtra("errorMessage");
							String apiUrl = intent.getStringExtra("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CustomerProfile");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}
					}
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}

		}

	}

	
	/**
	 * BroadcastReceiver for loadProfile();
	 * 
	 * @author Nitin
	 *
	 */
	class CustomerProfileReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				if(intent!=null){
					mpBar.setVisibility(View.GONE);
					String success = intent.getStringExtra("SUCCESS");
					String mesaage = "";
					String code = "";
					if(success.equalsIgnoreCase("true")){
						isAllApiResponse = true;
						
						CustomerDetails customerDetails = intent.getParcelableExtra("CUSTOMER_DETAILS");
						
						if(customerDetails!=null){

							String	ID		=	customerDetails.getId();
							String	NAME	=	customerDetails.getName();
							String	MOBILE	=	customerDetails.getMobile();
							String	EMAIL	=	customerDetails.getEmail();
							String	GENDER	=	customerDetails.getGender();
							String	CITY	=	customerDetails.getCity();
							String	STATE	=	customerDetails.getState();
							String	IMAGE_URL	=	customerDetails.getImage_url();

							String FACEBOOK_USER_ID = customerDetails.getFacebook_user_id();

							Log.i("FUSERID","FUSERID "+FACEBOOK_USER_ID);

							// check whether facebook user_id is present or not

							if(FACEBOOK_USER_ID.equalsIgnoreCase("0")){
								mBtnLoginFb.setText("Connect with facebook");
							}
							else{
								mBtnLoginFb.setText("Sync profile from facebook");
							}



							ArrayList<String> passedDates = intent.getStringArrayListExtra("DATE");
							ArrayList<String> passedDateIds = intent.getStringArrayListExtra("DATE_CATEGORY_ID");
							
							setProfile(NAME, "", EMAIL, CITY, STATE, GENDER, IMAGE_URL);
							
							if(passedDates.size() == 1 && passedDates.get(0).equalsIgnoreCase("0000-00-00")){

								for(int i=0;i<msArrName.size();i++){
									
									if(i == 3){
										break;
									}else{
										
										mTvArrChildLayout[i].setVisibility(View.VISIBLE);
										mTvArrForDatePicker[i].setText("");
										mTVArrForDateTypePicker[i].setText(msArrName.get(i));
										msArrIdToPasArray[i] = msArrIds.get(i);
									}
								}

							}else{

								for(int i=0;i<passedDates.size();i++){
									
									mTvArrChildLayout[i].setVisibility(View.VISIBLE);
									mTvArrForDatePicker[i].setText(passedDates.get(i));
									chkCount = 1;
									
									for(int j=0;j< msArrName.size();j++){
										
										if(passedDateIds.get(i).equalsIgnoreCase(msArrIds.get(j))){
											
											mTVArrForDateTypePicker[i].setText(msArrName.get(j));
											msArrDateToPassArray[i] = passedDates.get(i);
											msArrIdToPasArray[i] = msArrIds.get(j);
											mIvArrDeleteImg[i].setVisibility(View.INVISIBLE);
											mTvArrForDatePicker[i].setOnClickListener(null);
											mTVArrForDateTypePicker[i].setOnClickListener(null);
											break;
										}
									}
								}

								for(int i=0;i<msArrDateToPassArray.length;i++){
									Log.d("IDTOPASS","IDTOPASSS " + msArrDateToPassArray[i] + " " + msArrIdToPasArray[i]);
								}

								
								if(msArrDateToPassArray.length==1 || msArrDateToPassArray.length==0){
								
									LocalNotification localNotification = new LocalNotification(context);
									localNotification.checkPref();
									localNotification.setProfilePref(false,localNotification.isProfilAlarmFired());
									localNotification.alarm();
								}
							}

						}
					}else{
						isAllApiResponse = false;
						String message = intent.getStringExtra("MESSAGE");
						code = intent.getStringExtra("CODE");

						boolean isVolleyError = intent.getBooleanExtra("volleyError",false);
						if(isVolleyError){



							int code1 = intent.getIntExtra("CODE",0);
							String message1=intent.getStringExtra("MESSAGE");
							String errorMessage = intent.getStringExtra("errorMessage");
							String apiUrl = intent.getStringExtra("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CustomerProfile");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}

						if(code.equalsIgnoreCase("-221")){
							Toast.makeText(context, R.string.invalid_auth, 1).show();
							logout();
						}
						if(code.equalsIgnoreCase("0")){
							showToast(message);
						}
						//showToast(message);
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	void logout(){
		mSession.logoutCustomer();
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}


	/**
	 * Broadcast receiver for 
	 * 
	 * @author Nitin
	 *
	 */
	class UpdateCustomerProfileReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				if(intent!=null){
					mpBar.setVisibility(View.GONE);
					String success = intent.getStringExtra("SUCCESS");
					String message = intent.getStringExtra("MESSAGE");
					if(success.equalsIgnoreCase("true")){
						try{
							AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
							alertDialog.setIcon(R.drawable.ic_launcher);
							alertDialog.setTitle("Inorbit");
							alertDialog.setMessage(message/*"Awesome! Your profile has been updated."*/);
							alertDialog.setIcon(R.drawable.ic_launcher);
							alertDialog.setCancelable(true);
							alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {

									finish();
								}
							});

							AlertDialog alert = alertDialog.create();
							alert.show();
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}else{
						showToast(message);

						boolean isVolleyError = intent.getBooleanExtra("volleyError",false);
						if(isVolleyError){


							int code1 = intent.getIntExtra("CODE",0);
							String message1=intent.getStringExtra("MESSAGE");
							String errorMessage = intent.getStringExtra("errorMessage");
							String apiUrl = intent.getStringExtra("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CustomerProfile");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	class GetIntrestedMalls extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");



					if(success.equalsIgnoreCase("true")){
						mdbUtil.open();
						msArrGetAllInterestedAreaIds = intent.getStringArrayListExtra("MALL_ID_ARR");

						for(int i=0;i<msArrGetAllInterestedAreaIds.size();i++){
							Log.d("IDS","IDS " + msArrGetAllInterestedAreaIds.get(i));
							Log.d("IDNAME","IDNAME " + mdbUtil.getAreaNameByInorbitId(msArrGetAllInterestedAreaIds.get(i)));

							msArrSelectedAreaName.add(mdbUtil.getAreaNameByInorbitId(msArrGetAllInterestedAreaIds.get(i)));			
							msArrUpdateAreaId.add(msArrGetAllInterestedAreaIds.get(i));
							msArrUpdateAreaName.add(mdbUtil.getAreaNameByInorbitId(msArrGetAllInterestedAreaIds.get(i)));
						}

						AreaAdapter areaAdapter = new AreaAdapter((Activity) context, msArrAreas, msArrSelectedAreaName, msArrGetAllInterestedAreaIds, msArrAreaId);
						mLvInterestedAreaDialogList.setAdapter(areaAdapter);

					}
					else{
						String message = intent.getStringExtra("MESSAGE");
						//showToast(message);

						if(message.equalsIgnoreCase("No records found.")){

							msArrSelectedAreaName.clear();
							msArrGetAllInterestedAreaIds.clear();
							//allAreaIdsDB.clear();

							AreaAdapter areaAdapter = new AreaAdapter((Activity) context, msArrAreas, msArrSelectedAreaName, msArrGetAllInterestedAreaIds, msArrAreaId);
							mLvInterestedAreaDialogList.setAdapter(areaAdapter);
						}

						boolean isVolleyError = intent.getBooleanExtra("volleyError",false);
						if(isVolleyError){



							int code1 = intent.getIntExtra("CODE",0);
							String message1=intent.getStringExtra("MESSAGE");
							String errorMessage = intent.getStringExtra("errorMessage");
							String apiUrl = intent.getStringExtra("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CustomerProfile");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}
					}

					mdbUtil.close();
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	/**
	 * 
	 * Broadcast receiver of callInterestedAreaApi();
	 * 
	 * @author Nitin
	 *
	 */
	class UpdateIntrestedMalls extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				if(intent!=null){
					mpBar.setVisibility(View.GONE);
					String success = intent.getStringExtra("SUCCESS");
					String message = intent.getStringExtra("MESSAGE");
					if(success.equalsIgnoreCase("true")){
						//showToast(message);
					}else{
						//showToast(message);
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		try{
			EventTracker.endFlurrySession(getApplicationContext());
			if(Conifg.useServices_c){

			}else{
				if(dateReceiver!=null){
					mContext.unregisterReceiver(dateReceiver);
				}
				if(cutomerProfReceiver!=null){
					mContext.unregisterReceiver(cutomerProfReceiver);
				}
				if(updateProfReceiver!=null){
					mContext.unregisterReceiver(updateProfReceiver);
				}
				if(getIntrestedMallsReciver!=null){
					mContext.unregisterReceiver(getIntrestedMallsReciver);
				}
				if(updateIntrestedMallsReciver!=null){
					mContext.unregisterReceiver(updateIntrestedMallsReciver);
				}
			}
		}catch(Exception ex){

		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			if(Conifg.useServices_c){

			}else{
				IntentFilter dateFilter 			= new IntentFilter(RequestTags.Tag_SpecialDates);
				IntentFilter customerProfileFilter 	= new IntentFilter(RequestTags.Tag_CustomerProfile);
				IntentFilter updateProfileFilter 	= new IntentFilter(RequestTags.Tag_UpdateCustomerProfile);
				IntentFilter getIntrestedMallsFilter	= new IntentFilter(RequestTags.Tag_GetIntrestedAreas);
				IntentFilter updateIntrestedMallsFilter	= new IntentFilter(RequestTags.Tag_UpdateIntrestedMalls);
				
				mContext.registerReceiver(dateReceiver 			= new DateCatogoryReceiver(), dateFilter);
				mContext.registerReceiver(cutomerProfReceiver 	= new CustomerProfileReceiver(), customerProfileFilter);
				mContext.registerReceiver(updateProfReceiver 	= new UpdateCustomerProfileReceiver(), updateProfileFilter);
				mContext.registerReceiver(getIntrestedMallsReciver 	= new GetIntrestedMalls(), getIntrestedMallsFilter);
				mContext.registerReceiver(updateIntrestedMallsReciver 	= new UpdateIntrestedMalls(), updateIntrestedMallsFilter);
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	protected void onPause() {
		try{
			EventTracker.endLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onPause();
	}

	@Override
	protected void onStart() {
		super.onStart();
		try{
			EventTracker.startFlurrySession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	void registerMallSelectedEvent(String eventName,String mallName){
		try{
			Map<String, String> params = new HashMap<String, String>();
			params.put("MallName", mallName);
			//params.put("UserId", USER_ID);
			EventTracker.logEvent(eventName, false);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void regiserSimpleEvent(String eventName){
		try{
			//params.put("UserId", USER_ID);
			EventTracker.logEvent(eventName, false);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	private void clearFbToken() {
		// TODO Auto-generated method stub


		try{
			Session.openActiveSession(this, false, new Session.StatusCallback() {
				@Override
				public void call(Session session, SessionState state, Exception exception) {
					session.closeAndClearTokenInformation();
					Log.i("Clearing", "Token");
				}
			});
			//FlurryAgent.endTimedEvent("Gallery_Opened_Event");
		}catch(NullPointerException npx){
			npx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}

}







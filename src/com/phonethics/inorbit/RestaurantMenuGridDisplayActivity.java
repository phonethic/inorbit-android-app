package com.phonethics.inorbit;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.plus.model.people.Person.Urls;
import com.phonethics.model.RequestTags;
import com.phonethics.model.RestaurantMenuModel;
import com.squareup.picasso.Cache;
import com.squareup.picasso.Picasso;
import android.support.v4.util.LruCache;

public class RestaurantMenuGridDisplayActivity extends SherlockFragmentActivity {
	private Activity mContext;
	private ActionBar actionBar;
	private LinearLayout editLayout, deleteLayout;
	private TextView editMenu, addMenu, reorderMenu, deleteMenu, cancelDelete;
	private GridView imageGrid;
	private ProgressBar progress;
	private NetworkCheck isnetConnected;
	private String place_id;
	private String mall_id;
	private String status;
	private AllMenuImages allMenuImages;
	private ArrayList<RestaurantMenuModel> mMenuModel  = null;
	private GridAdapter gridAdapter;
	private LruCache<String, Bitmap> mMemoryCache;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_menu_layout);
		
		initViews();
		getBundleValues();
		
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

	    // Use 1/8th of the available memory for this memory cache.
	    final int cacheSize = maxMemory / 8;

	    mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
	        @Override
	        protected int sizeOf(String key, Bitmap bitmap) {
	            // The cache size will be measured in kilobytes rather than
	            // number of items.
	            return getByteCount(bitmap) / 1024;
	        }
	    };
		
		getMenuFromServer();
		imageGrid.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				ArrayList<String> PHOTO_SOURCE = new ArrayList<String>();
				for (int i=0; i<mMenuModel.size(); i++) {
					if (mMenuModel.get(i).getImage_url() != null && mMenuModel.get(i).getImage_url().trim().length()>0 && !mMenuModel.get(i).getImage_url().trim().equalsIgnoreCase(""))
						PHOTO_SOURCE.add(mMenuModel.get(i).getImage_url());
				}
				//Intent intent=new Intent(mContext, MenuGalleryView.class);
				Intent intent=new Intent(mContext, StorePagerGallery.class);
				intent.putExtra("photo_source", PHOTO_SOURCE);
				intent.putExtra("position", position);
				startActivity(intent);
			}
		});
	}
	
	@SuppressLint("NewApi")
	public static int getByteCount(Bitmap bitmap) {
		if (android.os.Build.VERSION.SDK_INT>=android.os.Build.VERSION_CODES.HONEYCOMB) {
			return bitmap.getByteCount();
		} else {
			return bitmap.getRowBytes() * bitmap.getHeight();
		}
	}
	
	private void initViews() {
		/**
		 * Initialize the action bar
		 */
		mContext = this;
		actionBar =	getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		editLayout	= (LinearLayout) findViewById(R.id.editLayout);
		editMenu = (TextView)findViewById(R.id.editMenu);
		addMenu = (TextView) findViewById(R.id.addMenu);
		reorderMenu = (TextView) findViewById(R.id.reorderMenu);
		deleteLayout = (LinearLayout) findViewById(R.id.deleteLayout);
		deleteMenu = (TextView) findViewById(R.id.deleteMenu);
		cancelDelete = (TextView) findViewById(R.id.cancelDelete);
		isnetConnected		= new NetworkCheck(mContext);
		//Gridview
		imageGrid				= (GridView)findViewById(R.id.imageGrid);
		progress = (ProgressBar) findViewById(R.id.progress);
		
		addMenu.setVisibility(View.GONE);
		editLayout.setVisibility(View.GONE);
		deleteLayout.setVisibility(View.GONE);
	}
	
	private void getBundleValues() {
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			place_id = bundle.getString("place_id");
			mall_id = bundle.getString("mall_id");
			status = bundle.getString("status");
		}
	}
	
	protected void onResume() {
		super.onResume();
		
		IntentFilter filter = new IntentFilter(RequestTags.TAG_GET_MENU_FOR_STORE);
		mContext.registerReceiver(allMenuImages = new AllMenuImages(), filter);
	}
	
	@Override
	protected void onStop() {
		if (allMenuImages != null) {
			mContext.unregisterReceiver(allMenuImages);
		}
		super.onStop();
	}
	private void getMenuFromServer(){

		progress.setVisibility(View.VISIBLE);
		if (isnetConnected.isNetConnected(mContext)) {
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
	
			List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("place_id", place_id));
			nameValuePairs.add(new BasicNameValuePair("mall_id", mall_id));
			
			MyClass myClass = new MyClass(mContext);
			myClass.getStoreRequest(RequestTags.TAG_GET_MENU_FOR_STORE, nameValuePairs, headers);
		} else {
			showToast(getResources().getString(R.string.noInternetConnection));
			//Load Images from Cache using Picasso
			//getImagesForRestaurantId(place_id);
		}
	}
	
	private void getImagesForRestaurantId(String place_id) {
		DBUtil dbUtil = new DBUtil(mContext);
		ArrayList<String> keys = dbUtil.getMenu(place_id);
		mMenuModel = new ArrayList<RestaurantMenuModel>();
		for (int i=0; i<keys.size(); i++) {
			RestaurantMenuModel model = new RestaurantMenuModel();
			model.setImage_url(keys.get(i));
			mMenuModel.add(model);
		}
		if(mMenuModel!=null && mMenuModel.size()>0){
			showToast("No Internet");
			gridAdapter = new GridAdapter(mContext, R.drawable.ic_launcher, mMenuModel);
			imageGrid.setAdapter(gridAdapter);
		}
	}
	
	private void savePicture(String filename, Bitmap b, Context ctx){
	    try {
	        ObjectOutputStream oos;
	        FileOutputStream out;// = new FileOutputStream(filename);
	        out = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
	        oos = new ObjectOutputStream(out);
	        b.compress(Bitmap.CompressFormat.JPEG, 100, oos);

	        oos.close();
	        oos.notifyAll();
	        out.notifyAll();
	        out.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	class AllMenuImages extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			progress.setVisibility(View.GONE);
			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					ArrayList<RestaurantMenuModel> all_menus = new ArrayList<RestaurantMenuModel>();
					/*mMenuModel */
					all_menus = intent.getParcelableArrayListExtra("MENUS");
					if(success.equalsIgnoreCase("true")) {
						//showToast("true");
						mMenuModel = new ArrayList<RestaurantMenuModel>();
						for (int i=0; i<all_menus.size(); i++) {
							if (all_menus.get(i).getStatus().equalsIgnoreCase(status)) {
								mMenuModel.add(all_menus.get(i));
							}
						}
						if(mMenuModel!=null && mMenuModel.size()>0){
							gridAdapter = new GridAdapter(mContext, R.drawable.ic_launcher, mMenuModel);
							imageGrid.setAdapter(gridAdapter);
						} else if (mMenuModel != null && mMenuModel.size() == 0) {
							showToast(getResources().getString(R.string.no_menu_available_yet));
						}
						/*ArrayList<String> menuUrls = new ArrayList<String>();
						for (int i=0; i<mMenuModel.size(); i++) {
							String menuImage = mMenuModel.get(i).getImage_url();
							
							if (getBitmapFromMemCache(getResources().getString(R.string.photo_url)+menuImage) == null) {
								SaveToCache t = new SaveToCache();
								t.execute(menuImage);
								 menuUrls.add(menuImage);
							 }
						}*/
						
					} else{
						imageGrid.setAdapter(null);
						String message = getResources().getString(R.string.no_menu_available_yet);
						showToast(message);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	private class AddUrlsToDB extends AsyncTask<ArrayList<String>, Void, Void> {
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(ArrayList<String>... params) {
			if (params[0].size() >0) {
				addUrlToDb(place_id, params[0]);
			}
			return null;
		}
		
	}
	
	private class SaveToCache extends AsyncTask<String, Void, Void> {
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(String... params) {
			/*try { 
				//addBitmapToMemoryCache(getResources().getString(R.string.photo_url)+params[0],AddRestaurantMenu.decodeSampledBitmapFromResource(getResources(), data, 100, 100));
			} catch (NotFoundException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			return null;
		}
	}
	
	class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {
	    private final WeakReference<ImageView> imageViewReference;
	    private int data = 0;

	    public BitmapWorkerTask(ImageView imageView) {
	        // Use a WeakReference to ensure the ImageView can be garbage collected
	        imageViewReference = new WeakReference<ImageView>(imageView);
	    }

	    // Decode image in background.
	    @Override
	    protected Bitmap doInBackground(Integer... params) {
	        data = params[0];
	        return AddRestaurantMenu.decodeSampledBitmapFromResource(getResources(), data, 100, 100);
	    }

	    // Once complete, see if ImageView is still around and set bitmap.
	    @Override
	    protected void onPostExecute(Bitmap bitmap) {
	        if (imageViewReference != null && bitmap != null) {
	            final ImageView imageView = imageViewReference.get();
	            if (imageView != null) {
	                imageView.setImageBitmap(bitmap);
	            }
	        }
	    }
	}
	
	private void showToast(String msg) {
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}
	
	private class GridAdapter extends ArrayAdapter<RestaurantMenuModel> {

		ArrayList<RestaurantMenuModel> mModelMenu;
		Activity context;
		LayoutInflater inflate;
		String menuImage  = "";

		public GridAdapter(Activity context, int resource,ArrayList<RestaurantMenuModel> mModelMenu) {
			super(context, resource, mModelMenu);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.mModelMenu=mModelMenu;
			inflate=context.getLayoutInflater();
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mModelMenu.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			final int pos = position;
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.gridimagelayout,null);
				holder.photo=(ImageView)convertView.findViewById(R.id.imgGridImage);
				holder.toChkDelete=(CheckBox)convertView.findViewById(R.id.toChkDelete);
				holder.toChkDelete.setVisibility(View.GONE);
				
				convertView.setTag(holder);
			} else {
				Log.d("INIF", "INELSE");	
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();
			
			if(mMenuModel!=null && (mMenuModel.get(position).getThumb_url()!=null && mMenuModel.get(position).getThumb_url().trim().length()!=0) &&
					(mMenuModel.get(position).getImage_url()!=null && mMenuModel.get(position).getImage_url().trim().length()!=0)) {
				menuImage = mMenuModel.get(position).getThumb_url().replaceAll(" ", "%20");
				try {
					//if (isnetConnected.isNetworkAvailable()) {
						 Picasso.with(context).load(getResources().getString(R.string.photo_url)+menuImage)
						.placeholder(R.drawable.ic_launcher)
						.error(R.drawable.ic_launcher)
						.into(hold.photo);
					/*} else {
						showToast("No Internet");
						Picasso.with(context).load(getResources().getString(R.string.photo_url))
						.placeholder(new BitmapDrawable(menuImage))
						.error(R.drawable.ic_launcher)
						.into(hold.photo);
					}*/
				} catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			
			return convertView;
		}
	} 
	
	private void addUrlToDb(String place_id, ArrayList<String> urls){
		DBUtil dbUtil = new DBUtil(mContext);
		dbUtil.insertMenu(place_id, urls);
	}
	
	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
	    if (getBitmapFromMemCache(key) == null) {
	        mMemoryCache.put(key, bitmap);
	        //savePicture(key, bitmap, mContext);
	    }
	}

	public Bitmap getBitmapFromMemCache(String key) {
	    return mMemoryCache.get(key);
	}
	
	/*class SetCache implements Cache {
		SessionManager sessionManager = new SessionManager(mContext);
		
		@Override
		public void clear() {
			sessionManager.removeMenuId(place_id);
		}

		@Override
		public Bitmap get(String arg0) {
			if (sessionManager.containsMenu(place_id)) {
				
			}
			return null;
		}

		@Override
		public int maxSize() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void set(String arg0, Bitmap arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public int size() {
			// TODO Auto-generated method stub
			return 0;
		}
		
	}
	*/
	class ViewHolder
	{
		ImageView photo;
		CheckBox toChkDelete;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}
	
}

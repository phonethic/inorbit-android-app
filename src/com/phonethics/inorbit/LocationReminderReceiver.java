package com.phonethics.inorbit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

public class LocationReminderReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		final String key = LocationManager.KEY_PROXIMITY_ENTERING;
	    final Boolean entering = intent.getBooleanExtra(key, false);

	    if (entering) {
	        Toast.makeText(context, "LocationReminderReceiver entering", Toast.LENGTH_SHORT).show();
	        Log.i("LocationReminderReceiver", "Inorbit entering");
	    } else {
	        Toast.makeText(context, "LocationReminderReceiver exiting", Toast.LENGTH_SHORT).show();
	        Log.i("LocationReminderReceiver", "Inorbit exiting");
	    }
	}

}

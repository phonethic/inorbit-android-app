package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

/*import com.urbanairship.push.PushManager;*/

//import com.urbanairship.push.PushManager;

public class SessionManager {

	//Shared Preferences
	SharedPreferences pref;


	//Editor for Shared Preferences 
	Editor editor;

	//Shared Preference Mode
	int PRIVATE_MODE=0;

	//Shared Prefernce File Name
	private static final String PREF_NAME="SHOPLOCAL_SESSION";

	//All Shared Preferences Key
	private static final String IS_LOGIN="IS_LOGGED_IN";

	//User Name
	public static final String KEY_USER_NAME="mobileno";

	//Password
	public static final String KEY_PASSWORD="password";

	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	//Place Id
	public static final String KEY_PLACE_ID="place_id";

	//Customer
	//All Shared Preferences Key
	private static final String IS_LOGIN_CUSTOMER="IS_LOGGED_IN_CUSTOMER";

	//User Name
	public static final String KEY_USER_NAME_CUSTOMER="mobileno_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";

	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Place Id
	public static final String KEY_PLACE_ID_CUSTOMER="place_id_CUSTOMER";

	//Context 
	Context context;

	//
	public static final String KEY_SHOW_PLACE_CHOOSER_DIALOG= "show_place_chooser_dialog";
	public static final String KEY_MALL_MANAGER_LOGGED_IN= "mall_manager_logged_in";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";

	//udm id
	public static final String UDM_ID_CUSTOMER = "udm_id_customer";
	public static final String UDM_ID_MERCHANT = "udm_id_merchant";
	
	public static final String Last_Update_Time = "last_update_time";
	
	public static final String GCMID = "gcm_Id";
	public static final String GCMFlag = "gcm_Flag";
	public static final String GCMMsg = "gcm_Msg";
	
	//Constructor
	public SessionManager(Context context){
		this.context=context;
		pref=context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor=pref.edit();

	}





	/**
	 * Create Login Session for merchant - stores  details like mobile number, password, user id and auth id of the merchant.
	 */
	public void createLoginSession(String mobile_no,String password,String userid,String authid)
	{
		//Storing login value as true
		editor.putBoolean(IS_LOGIN, true);

		//Stroing mobile no in pref
		editor.putString(KEY_USER_NAME, mobile_no);

		//Storing password in pref
		editor.putString(KEY_PASSWORD, password);

		//Storing user id in pref
		editor.putString(KEY_USER_ID, userid);

		//Storing auth id in pref
		editor.putString(KEY_AUTH_ID, authid);

		//commit changes
		editor.commit();

		Log.i("Session", "Session Created");

	}

	public void addMenuId(String place_id) {
		editor.putBoolean(place_id, true);
		editor.commit();
	}

	public boolean containsMenu(String place_id) {
		boolean contains = pref.getBoolean(place_id, false);
		return contains;
	}
	
	public void removeMenuId(String place_id) {
		editor.putBoolean(place_id, false);
		editor.commit();
	}
	
	/**
	 * Create Login Session for merchant - stores  details like mobile number, password, user id and auth id of the customer.
	 */
	public void createLoginSessionCustomer(String mobile_no,String password,String userid,String authid)
	{
		//Storing login value as true
		editor.putBoolean(IS_LOGIN_CUSTOMER, true);

		//Stroing mobile no in pref
		editor.putString(KEY_USER_NAME_CUSTOMER, mobile_no);

		//Storing password in pref
		editor.putString(KEY_PASSWORD_CUSTOMER, password);

		//Storing user id in pref
		editor.putString(KEY_USER_ID_CUSTOMER, userid);

		//Storing auth id in pref
		editor.putString(KEY_AUTH_ID_CUSTOMER, authid);

		//commit changes
		editor.commit();

		Log.i("Session", "Session Created");

	}






	/**
	 * returns the deatils of the loggedin merchant
	 */
	public HashMap<String, String>getUserDetails()
	{

		HashMap<String, String>user=new HashMap<String, String>();

		//user name
		user.put(KEY_USER_NAME, pref.getString(KEY_USER_NAME, "not-found"));

		//password
		user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, "not-found"));

		//user id
		user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, "not-found"));

		//auth id
		user.put(KEY_AUTH_ID, pref.getString(KEY_AUTH_ID, "not-found"));

		//return user
		return user;

	}





	/**
	 * returns the deatils of the customer
	 */
	public HashMap<String, String>getUserDetailsCustomer()
	{

		HashMap<String, String>user=new HashMap<String, String>();

		//user name
		user.put(KEY_USER_NAME_CUSTOMER, pref.getString(KEY_USER_NAME_CUSTOMER, "not-found"));

		//password
		user.put(KEY_PASSWORD_CUSTOMER, pref.getString(KEY_PASSWORD_CUSTOMER, "not-found"));

		//user id
		user.put(KEY_USER_ID_CUSTOMER, pref.getString(KEY_USER_ID_CUSTOMER, "not-found"));

		//auth id
		user.put(KEY_AUTH_ID_CUSTOMER, pref.getString(KEY_AUTH_ID_CUSTOMER, "not-found"));

		//return user
		return user;

	}



	/*
	 * Check user logged in or not
	 */
	public void checkLogin()
	{
		//check Login Status
		if(!this.isLoggedIn())
		{
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(context, MerchantLogin.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			context.startActivity(i);
		}
	}

	/*
	 * Check user logged in or not
	 */
	public void checkLoginCustomer()
	{
		//check Login Status
		if(!this.isLoggedInCustomer())
		{
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(context, MerchantLogin.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			context.startActivity(i);
		}
	}

	/**
	 * Clear session details of merchant
	 * 
	 * */
	public void logoutUser(){
		// Clearing all data from Shared Preferences
		/*editor.clear();*/

		editor.remove(KEY_PASSWORD);
		editor.remove(KEY_USER_ID);
		editor.remove(KEY_AUTH_ID);
		editor.remove(IS_LOGIN);
		editor.commit();

		//unTagMerchant();


	}


	/*void unTagMerchant(){
		try
		{
			//Get All Tags of the Devices
			Set<String> tags = new HashSet<String>(); 
			tags=PushManager.shared().getTags();

			//Check if the Tag has merchant tag. Then untag it.
			if(tags.contains("merchant"))
			{
				tags.remove("merchant");

			}

			//Getting All Tags
			Iterator<String> iterator=tags.iterator();

			String temp="";
			ArrayList<String>tempIdList=new ArrayList<String>();

			//Traversing Tags
			while(iterator.hasNext())
			{
				temp=iterator.next();

				//InorbitLog.d("TAG "+ "Urban TAG PRINT "+temp);
				//If tag contains own_ then add them to another arraylist
				if(temp.startsWith("own_"))
				{
					tempIdList.add(temp);
				}
			}

			//Remove all tags contains own_id's.
			tags.removeAll(tempIdList);

			InorbitLog.d("TAG "+ "TAGS SESSION "+tags.toString());

			//Set Tags to Urban Airship
			PushManager.shared().setTags(tags);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	 }*/




	/**
	 * Clear session details of the customer
	 * */
	public void logoutCustomer(){
		String uId = pref.getString(KEY_USER_ID_CUSTOMER, "not-found");
		editor.remove(KEY_PASSWORD_CUSTOMER);
		editor.remove(KEY_USER_ID_CUSTOMER);
		editor.remove(KEY_AUTH_ID_CUSTOMER);
		editor.remove(IS_LOGIN_CUSTOMER);
		editor.remove(UDM_ID_CUSTOMER);
		editor.commit();

		//unTagCustomer(uId);

		Log.i("Session", "Inorbit Session Logout");
	}



	/*void unTagCustomer(String userid){
		try
		{
			//Get All Tags of the Devices
			Set<String> tags = new HashSet<String>(); 
			tags=PushManager.shared().getTags();
			InorbitLog.d("Customer Tag "+tags);

			//Check if the Tag has merchant tag. Then untag it.
			String uid = "customer_id_"+userid;
			InorbitLog.d("user id -- "+uid);
			if(tags.contains("customer_id_"+userid))
			{
				tags.remove("customer_id_"+userid);

			}


			//Set Tags to Urban Airship
			InorbitLog.d("Customer Tag "+tags);
			PushManager.shared().setTags(tags);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	 }*/




	/** check whether merchant is logged in or not */
	public boolean isLoggedIn(){
		Log.i("Session", "Session IS_LOGIN"+pref.getBoolean(IS_LOGIN, false));
		return pref.getBoolean(IS_LOGIN, false);
	}



	/** check whether custmer is logged in or not */
	public boolean isLoggedInCustomer(){
		Log.i("Session", "Session IS_LOGIN"+pref.getBoolean(IS_LOGIN_CUSTOMER, false));
		return pref.getBoolean(IS_LOGIN_CUSTOMER, false);
	}



	/** set whether dialog should be called or not to choose the defulat place of the mall */
	public void showCityChooserDialog(boolean isShow){
		editor.putBoolean(KEY_SHOW_PLACE_CHOOSER_DIALOG, isShow);
		editor.commit();

	}

	/** call this dialog when the app gets open for the first time */
	public boolean toShowDialog(){
		return pref.getBoolean(KEY_SHOW_PLACE_CHOOSER_DIALOG, true);
	}


	/** set if mall manager id logged in, true if mall manager is logged in, false if store manager is logged in */
	public void setMallManagerLoggedin(boolean isMallManager){
		editor.putBoolean(KEY_MALL_MANAGER_LOGGED_IN, isMallManager);
		editor.commit();

	}


	/**
	 *  check whether mall manager is logged or store manager is logged in
	 * returns true if manager is logged in
	 * false if store manager is logged in
	 *  */
	public boolean isMallManagerLoggedin(){
		return pref.getBoolean(KEY_MALL_MANAGER_LOGGED_IN, true);
	}



	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 *
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	public String getRegistrationId() {

		//final SharedPreferences prefs = getGCMPreferences(context);

		String registrationId = pref.getString(PROPERTY_REG_ID, "");

		if (registrationId.equalsIgnoreCase("")) {
			InorbitLog.d("Registration not found.");
			return "";
		}

		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.

		int registeredVersion = pref.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);

		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			InorbitLog.d("App version changed.");
			return "";
		}
		return registrationId;
	}


	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 *
	 * @param context application's context.
	 * @param regId registration ID
	 */
	void storeRegistrationId(String regId) {
		/*final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);*/
		//Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, getAppVersion(context));
		editor.commit();
	}


	public void storeTodayDate(long timeInMilli){
		editor.putLong("TodaysTime", timeInMilli);
		editor.commit();
	}


	public long getLastDateOfApi(){
		return pref.getLong("TodaysTime", 0);
	}


	public void setParentStoreApiCallDate(long todaysTimeInMilli){
		editor.putLong("ParentStoreApiCall", todaysTimeInMilli);
		editor.commit();
	}


	public long getParentStoreCallLastDate(){
		return pref.getLong("ParentStoreApiCall", 0);
	}


	public void setMallsApiCallDate(long todaysTimeInMilli){
		editor.putLong("MallApiCall", todaysTimeInMilli);
		editor.commit();
	}


	public long getMallsApiCallLastDate(){
		return pref.getLong("MallApiCall", 0);
	}


	public void setActiveMallId(String activeMallId){
		editor.putString("ActiveMallId", activeMallId);
		editor.commit();
	}


	public String getActiveMallId(){
		return pref.getString("ActiveMallId","");
	}

	public void setApplicationFor(int applicationFor){
		editor.putInt("ApplicationFor", applicationFor);
		editor.commit();
	}


	/**
	 * Application has merchant or not
	 * 
	 * @return 
	 */
	public int getApplicationFor(){
		return pref.getInt("ApplicationFor",0);
	}


	public void storeTodaysMerchantDate(long timeInMilli){
		editor.putLong("MerchantApiValidate", timeInMilli);
		editor.commit();
	}


	public long getLastDateOfMerchantApi(){
		return pref.getLong("MerchantApiValidate", 0);
	}


	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	//set UDM Id for customer
	void setUdmIdForCustomer(String udm_id){
		editor.putString(UDM_ID_CUSTOMER, udm_id);
		//commit changes
		editor.commit();
	}

	//set UDM Id for merchant
	void setUdmIdForMerchant(String udm_id){
		editor.putString(UDM_ID_MERCHANT, udm_id);
		//commit changes
		editor.commit();
	}

	public String getUdmIDForCustomer(){
		return pref.getString(UDM_ID_CUSTOMER, "");
	}

	public String getUdmIDForMerchant(){
		return pref.getString(UDM_ID_MERCHANT, "");
	}
	
	public void setLastVersionUpdateTime() {
		editor.putLong(Last_Update_Time, System.currentTimeMillis());
		editor.commit();
	}
	
	public long getLastVersionUpdateTime() {
		return pref.getLong(Last_Update_Time, 0);
	}
	
	public void setGCMID(String gcmId) {
		editor.putString(GCMID, gcmId);
		editor.commit();
	}
	
	public void setGCMFlag(String gcmFlag) {
		editor.putString(GCMFlag, gcmFlag);
		editor.commit();
	}
	
	public void setGCMMessage(String gcmMsg) {
		editor.putString(GCMMsg, gcmMsg);
		editor.commit();
	}
	
	public String getGCMID() {
		return pref.getString(GCMID, "");
	}
	
	public String getGCMFlag() {
		return pref.getString(GCMFlag, "");
	}
	
	public String getGCMMessage() {
		return pref.getString(GCMMsg, "");
	}
}
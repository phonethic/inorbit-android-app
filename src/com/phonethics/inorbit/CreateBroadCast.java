package com.phonethics.inorbit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.RequestTags;
//import com.phonethics.networkcall.TalkNowResultReceiver;
//import com.phonethics.networkcall.TalkNowResultReceiver.TalkNow;

//import com.phonethics.networkcall.TalkNowService;

import eu.janmuller.android.simplecropimage.CropImage;

public class CreateBroadCast extends SherlockActivity {

	private ActionBar mActionbar;
	static String API_HEADER;
	static String API_VALUE;

	static String LOGIN_PATH;
	static String LOGOUT_PATH;

	static String LOGIN_URL;

	Activity context;

	//CreateBroadcast
	EditText edtBroadTitle,edtBroadDesc,edtTags;
	Button btnReview;
	CheckBox chkIsOffer;

	TextView offerDate_start,offerTime_start;
	TextView offerDate_end,offerTime_end;

	//AddBroadCastReceiver mAddBroadCast;
	ArrayList<String> SELECTED_STORE_ID;

	boolean isSplitLogin=false,isMyBroadCast=false;

	TableRow tblSelectDate_start,tblselectTime_start;
	TableRow tblSelectDate_end,tblselectTime_end;
	//TableRow tblOfferDetails;

	String is_offered="";


	Calendar dateTime=Calendar.getInstance();
	Calendar dateTimeCurrent=Calendar.getInstance();
	long miliseconds;

	//Session Manger
	SessionManager session;
	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	byte[] gallery_byte;

	ImageView imgBroadCast;

	String MERCHANT_BUSINESS_ID="";

	ProgressBar prog;

	Dialog dialog,remoteDialog;
	ImageView imgThumbPreview;

	String FILE_PATH="/sdcard/temp_photo.jpg";
	String FILE_NAME="temp_photo.jpg";
	String FILE_TYPE="image/jpeg";

	/*FILE_TYPE="image/jpeg";
	FILE_NAME="temp_photo.jpg";*/
	/**************************FILE CROP***********************/
	public static final int REQUEST_CODE_GALLERY      = 6;
	public static final int REQUEST_CODE_TAKE_PICTURE = 7;
	public static final int REQUEST_CODE_CROP_IMAGE   = 0x3;
	final int PIC_CROP = 3;
	private File      mFileTemp;

	Uri mImageCaptureUri;

	Button btnImageChooser;
	Button btnChooseImage;
	Button btnTakeImage;
	Button btnRemoteImage;
	Button btnSubmitUrl;


	EditText url;

	boolean isRemote=false;
	String remoteurl="";

	boolean isImageBroadCast;
	boolean isReuse=false;
	String imageType="";
	String encodedpath="/sdcard/temp_photo.jpg";

	ScrollView makePostScroll;
	ScrollView viewPostScroll;

	Button editButton;
	Button postButton;

	TextView txtViewOfferDate_start;
	TextView txtViewOfferDate_end;
	TextView txtViewOfferDetail;

	ImageView reviewImage;

	ImageView deleteImage;

	String title;
	String description;
	String photourl;
	String tags;

	ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;
	String PHOTO_URL;

	Bitmap bitmapImage;

	TextView txtViewOfferTitle;

	Typeface tf;

	NetworkCheck network;
	File cacheDir;

	ImageView sepratorCheck;

	//TalkNowResultReceiver talkNowBroadCast;
	EditText edtImageTitle;

	boolean isSureShop = false;
	boolean isFavouriteStore = false;
	boolean isMallWise = false;
	boolean isActiveMall = false;

	String post_type;
	String calenderFullDate;
	String place_id;
	String date_type;
	String mall_id;

	RadioGroup radioGroup;
	RadioButton genMale;
	RadioButton genFemale;

	String gender = "";

	TextView ClickToShoeGender;
	TableRow genderView;

	int tap = 1;
	ImageView separaterLineGender;
	BroadCastOfferUpdate b_OfferUpdate;
	private DatePickerDialog dateDialog;
	private ImageView imgSeprator_1,imgSeprator_2,imgSeprator_3,imgSeprator_4;
	private TableRow tblOfferTill;
	private TableRow tblOfferFrom;
	private TextView text_isOffer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_broad_cast);

		context=this;
		network=new NetworkCheck(context);
		mActionbar=getSupportActionBar();
		mActionbar.setTitle(getResources().getString(R.string.createBroadCastHeader));
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.show();

		imageLoader=ImageLoader.getInstance();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitCache");
		}
		else{
			cacheDir=context.getCacheDir();
		}

		if(!cacheDir.exists()){
			cacheDir.mkdirs();
		}else if(network.isNetworkAvailable()){

		}

		config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();

		//tf=Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		tf = InorbitApp.getTypeFace();

		//Setting Current Date

		Date cdt=new Date();

		dateTimeCurrent.setTime(cdt);

		dialog=new Dialog(context);
		remoteDialog=new Dialog(context);

		PHOTO_URL=getResources().getString(R.string.photo_url);

		//ScrollViews
		makePostScroll=(ScrollView)findViewById(R.id.makePostScroll);
		viewPostScroll=(ScrollView)findViewById(R.id.viewPostScroll);

		//Button
		editButton=(Button)findViewById(R.id.editButton);
		postButton=(Button)findViewById(R.id.postButton);

		sepratorCheck=(ImageView)findViewById(R.id.sepratorCheck);
		imgSeprator_1=(ImageView)findViewById(R.id.img_separator_1);
		imgSeprator_2 = (ImageView)findViewById(R.id.img_separator_2);
		imgSeprator_3 =(ImageView)findViewById(R.id.img_separator_3);
		imgSeprator_4 =(ImageView)findViewById(R.id.img_separator_4);

		//TextView
		txtViewOfferDate_start=(TextView)findViewById(R.id.txtViewOfferDate_start);
		txtViewOfferDate_end=(TextView)findViewById(R.id.txtViewOfferDate_end);
		txtViewOfferDetail=(TextView)findViewById(R.id.txtViewOfferDetail);
		txtViewOfferTitle=(TextView)findViewById(R.id.txtViewOfferTitle);
		editButton.setText(getString(R.string.edit));

		txtViewOfferTitle.setTypeface(tf);
		txtViewOfferDate_start.setTypeface(tf);
		txtViewOfferDate_end.setTypeface(tf);

		reviewImage=(ImageView)findViewById(R.id.reviewImage);

		//Image Thumbnail
		imgThumbPreview=(ImageView)findViewById(R.id.imgThumbPreview);
		deleteImage=(ImageView)findViewById(R.id.deleteImage);

		//Dialogs
		remoteDialog.setContentView(R.layout.remoteurl);
		dialog.setContentView(R.layout.createimagebroadcastdialog);

		//Setting Titles to dialogs
		remoteDialog.setTitle("Enter url");
		dialog.setTitle("Image");

		//Setting animation to dialogs
		dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		remoteDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mFileTemp = new File(Environment.getExternalStorageDirectory(), "temp_photo.jpg");
		mImageCaptureUri = Uri.fromFile(mFileTemp);

		FILE_PATH="/sdcard/temp_photo.jpg";

		//Image Chooser button
		btnImageChooser=(Button)findViewById(R.id.btnImageChooser);

		//dialog buttons
		btnRemoteImage=(Button)dialog.findViewById(R.id.btnRemote);
		btnSubmitUrl=(Button)remoteDialog.findViewById(R.id.btnSubmitUrl);


		btnChooseImage=(Button)findViewById(R.id.btnChooseImage);
		btnTakeImage=(Button)findViewById(R.id.btnCapture);

		//Dialog url
		url=(EditText)remoteDialog.findViewById(R.id.edtRemoteUrl);

		//Caption
		edtImageTitle=(EditText)findViewById(R.id.edtImageTitle);
		edtImageTitle.setTypeface(tf);

		/*
		 * API KEY
		 */

		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);
		//talkNowBroadCast = new TalkNowResultReceiver(new Handler());
		//talkNowBroadCast.setReceiver(this);

		radioGroup = (RadioGroup) findViewById(R.id.gender);
		genMale = (RadioButton) findViewById(R.id.genMale);
		genFemale = (RadioButton) findViewById(R.id.genFemale);
		ClickToShoeGender = (TextView) findViewById(R.id.ClickToShoeGender);
		genderView = (TableRow) findViewById(R.id.genderView);
		separaterLineGender = (ImageView) findViewById(R.id.separaterLineGender);

		Bundle b=getIntent().getExtras();
		session=new SessionManager(getApplicationContext());



		//Creating broadcast
		edtBroadTitle=(EditText)findViewById(R.id.edtBroadTitle);
		edtBroadDesc=(EditText)findViewById(R.id.edtBroadDesc);
		edtTags=(EditText)findViewById(R.id.edtTags);
		chkIsOffer=(CheckBox)findViewById(R.id.chkIsOffer);
		btnReview=(Button)findViewById(R.id.btnReview);

		prog=(ProgressBar)findViewById(R.id.prog);

		imgBroadCast=(ImageView)findViewById(R.id.imgBroadCast);

		Date current_date=new Date();
		Date current_time=new Date();

		DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
		DateFormat  dt2=new SimpleDateFormat(" HH:mm:ss a");

		offerDate_start=(TextView)findViewById(R.id.offerDate_start);
		offerTime_start=(TextView)findViewById(R.id.offerTime_start);
		offerDate_end=(TextView)findViewById(R.id.offerDate_end);
		offerTime_end=(TextView)findViewById(R.id.offerTime_end);
		text_isOffer=(TextView)findViewById(R.id.text_isOffer);


		Calendar cal = Calendar.getInstance();

		String formattedDate = dt.format(cal.getTime());
		cal.add(Calendar.DATE, 7);
		offerDate_start.setText(dt.format(cal.getTime()));
		offerDate_end.setText(dt.format(cal.getTime()));
		dateTime.set(Calendar.HOUR_OF_DAY, 23);
		dateTime.set(Calendar.MINUTE,30);
		dateTime.set(Calendar.SECOND,00);
		
		DateFormat  dt3=new SimpleDateFormat(" HH:mm:ss a");
		miliseconds=dateTime.getTimeInMillis();
		offerTime_start.setText(dt3.format(dateTime.getTime()));
		offerTime_end.setText(dt3.format(dateTime.getTime()));

		tblSelectDate_start=(TableRow)findViewById(R.id.tblSelectDate_start);
		tblselectTime_start=(TableRow)findViewById(R.id.tblselectTime_start);
		tblSelectDate_end=(TableRow)findViewById(R.id.tblSelectDate_end);
		tblselectTime_end=(TableRow)findViewById(R.id.tblselectTime_end);
		tblOfferFrom=(TableRow)findViewById(R.id.tblOfferFrom);
		tblOfferTill=(TableRow)findViewById(R.id.tblOfferTill);
	

		tblSelectDate_start.setVisibility(View.GONE);
		tblselectTime_start.setVisibility(View.GONE);
		tblSelectDate_end.setVisibility(View.GONE);
		tblselectTime_end.setVisibility(View.GONE);
		imgSeprator_1.setVisibility(View.GONE);
		imgSeprator_2.setVisibility(View.GONE);
		imgSeprator_3.setVisibility(View.GONE);
		imgSeprator_4.setVisibility(View.GONE);
		tblOfferFrom.setVisibility(View.GONE);
		tblOfferTill.setVisibility(View.GONE);
	
		

		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				if(checkedId==genMale.getId())
				{
					gender=genMale.getText().toString().toLowerCase();
					//showToast(gender);
				}
				else if(checkedId==genFemale.getId())
				{
					gender=genFemale.getText().toString().toLowerCase();
					//showToast(gender);
				}
			}
		});

		ClickToShoeGender.setOnClickListener(new OnClickListener() {
			@Override  
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tap++;
				if(tap%2==0){
					genderView.setVisibility(View.VISIBLE);
					ClickToShoeGender.setText("Post to specific gender -");
				}else {
					genderView.setVisibility(View.GONE);
					ClickToShoeGender.setText("Post to specific gender +");
				}
			}
		});

		try{

			if(b!=null){
				isSplitLogin=session.isLoggedIn();
				SELECTED_STORE_ID=b.getStringArrayList("SELECTED_STORE_ID");
				title=b.getString("title");
				description=b.getString("description");
				photourl=b.getString("photourl");
				tags=b.getString("tags");
				Log.i("PASSED DATA", "PASSED DATA title "+title);
				Log.i("PASSED DATA", "PASSED DATA description "+description);
				Log.i("PASSED DATA", "PASSED DATA photourl "+photourl);
				Log.i("PASSED DATA", "PASSED DATA tags "+tags);
				if(title!=""){
					edtBroadTitle.setText(title);
				}
				if(description!=""){
					edtBroadDesc.setText(description);
				}
				if(photourl!=""){
					try{
						photourl=photourl.replaceAll(" ","%20");
						if(!photourl.equalsIgnoreCase("notfound") || !photourl.equalsIgnoreCase("") ){
							
							imgThumbPreview.setVisibility(View.VISIBLE);
							reviewImage.setVisibility(View.VISIBLE);
							deleteImage.setVisibility(View.VISIBLE);
							imageLoader.displayImage(PHOTO_URL+photourl, imgThumbPreview, new ImageLoadingListener() {

								@Override
								public void onLoadingStarted(String arg0, View arg1) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
									// TODO Auto-generated method stub
									//get Bitmap and encode it to Base64.
									bitmapImage=((BitmapDrawable)imgThumbPreview.getDrawable()).getBitmap();
									FILE_TYPE="image/jpeg";
									FILE_NAME="temp_photo.jpg";

									try{
										ByteArrayOutputStream stream = new ByteArrayOutputStream();
										bitmapImage.compress(Bitmap.CompressFormat.JPEG, 80, stream);
										gallery_byte = stream.toByteArray();
									}catch(Exception ex){
										ex.printStackTrace();
									}
								}

								@Override
								public void onLoadingCancelled(String arg0, View arg1) {
									// TODO Auto-generated method stub

								}
							});
							imageLoader.displayImage(PHOTO_URL+photourl, reviewImage, new ImageLoadingListener() {

								@Override
								public void onLoadingStarted(String arg0, View arg1) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
									// TODO Auto-generated method stub


								}

								@Override
								public void onLoadingCancelled(String arg0, View arg1) {
									// TODO Auto-generated method stub

								}
							});
							isImageBroadCast=true;
							isReuse=true;
							
							
							Log.i("PASSED DATA", "PASSED DATA encoded "+encodedpath);

						}
						else{
							reviewImage.setVisibility(View.GONE);
							imgThumbPreview.setVisibility(View.GONE);
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}if(tags!=""){
					edtTags.setText(tags);
				}
				isSureShop = b.getBoolean("isSureShop",false);
				isFavouriteStore = b.getBoolean("isFavouriteStore",false);
				isMallWise = b.getBoolean("isMallWise", false);
				isActiveMall = b.getBoolean("isActiveMall", false);

				if(genMale.isChecked()){

				}else{

				}

				if(isSureShop){

					ClickToShoeGender.setVisibility(View.VISIBLE);
					separaterLineGender.setVisibility(View.GONE);
					place_id = b.getString("place_id");
					date_type = b.getString("date_type");
					calenderFullDate = b.getString("special_date");
					post_type = "1";
					chkIsOffer.setVisibility(View.GONE);
					text_isOffer.setVisibility(View.GONE);
					is_offered="1";
					tblSelectDate_start.setVisibility(View.VISIBLE);
					tblselectTime_end.setVisibility(View.VISIBLE);
					tblSelectDate_end.setVisibility(View.VISIBLE);
					tblselectTime_start.setVisibility(View.VISIBLE);
					sepratorCheck.setVisibility(View.VISIBLE);
					imgSeprator_1.setVisibility(View.VISIBLE);
					imgSeprator_2.setVisibility(View.VISIBLE);
					imgSeprator_3.setVisibility(View.VISIBLE);
					imgSeprator_4.setVisibility(View.VISIBLE);
					tblOfferFrom.setVisibility(View.VISIBLE);
					tblOfferTill.setVisibility(View.VISIBLE);
					mall_id = b.getString("mall_id");


					Log.d("MALLID","ShakeWinFor " +  calenderFullDate +" -- "+date_type);  

				}else if(isFavouriteStore){

					ClickToShoeGender.setVisibility(View.VISIBLE);
					separaterLineGender.setVisibility(View.GONE);
					place_id = b.getString("place_id");
					post_type = "2";
					chkIsOffer.setVisibility(View.GONE);
					text_isOffer.setVisibility(View.GONE);
					is_offered="1";
					tblSelectDate_start.setVisibility(View.VISIBLE);
					tblselectTime_start.setVisibility(View.VISIBLE);
					tblSelectDate_end.setVisibility(View.VISIBLE);
					tblselectTime_end.setVisibility(View.VISIBLE);
					sepratorCheck.setVisibility(View.VISIBLE);
					imgSeprator_1.setVisibility(View.VISIBLE);
					imgSeprator_2.setVisibility(View.VISIBLE);
					imgSeprator_3.setVisibility(View.VISIBLE);
					imgSeprator_4.setVisibility(View.VISIBLE);
					tblOfferFrom.setVisibility(View.VISIBLE);
					tblOfferTill.setVisibility(View.VISIBLE);
					mall_id = b.getString("mall_id");
					Log.d("MALLID","MALLID " +  mall_id);

				}else if(isMallWise){

					ClickToShoeGender.setVisibility(View.VISIBLE);
					//separaterLineGender.setVisibility(View.VISIBLE);
					separaterLineGender.setVisibility(View.GONE);

					place_id = b.getString("place_id");
					mall_id = b.getString("mall_id");

					post_type = "3";

					chkIsOffer.setVisibility(View.GONE);
					text_isOffer.setVisibility(View.GONE);
					is_offered="1";
					tblSelectDate_start.setVisibility(View.VISIBLE);
					tblselectTime_start.setVisibility(View.VISIBLE);
					tblSelectDate_end.setVisibility(View.VISIBLE);
					tblselectTime_end.setVisibility(View.VISIBLE);
					sepratorCheck.setVisibility(View.VISIBLE);
					imgSeprator_1.setVisibility(View.VISIBLE);
					imgSeprator_2.setVisibility(View.VISIBLE);
					imgSeprator_3.setVisibility(View.VISIBLE);
					imgSeprator_4.setVisibility(View.VISIBLE);
					tblOfferFrom.setVisibility(View.VISIBLE);
					tblOfferTill.setVisibility(View.VISIBLE);
				}else if(isActiveMall){

					ClickToShoeGender.setVisibility(View.VISIBLE);
					separaterLineGender.setVisibility(View.GONE);

					place_id = b.getString("place_id");
					mall_id = b.getString("activeMallId");
					text_isOffer.setVisibility(View.GONE);

					post_type = "4";

					chkIsOffer.setVisibility(View.GONE);
					is_offered="1";
					tblSelectDate_start.setVisibility(View.VISIBLE);
					tblselectTime_start.setVisibility(View.VISIBLE);
					tblSelectDate_end.setVisibility(View.VISIBLE);
					tblselectTime_end.setVisibility(View.VISIBLE);
					sepratorCheck.setVisibility(View.VISIBLE);
					imgSeprator_1.setVisibility(View.VISIBLE);
					imgSeprator_2.setVisibility(View.VISIBLE);
					imgSeprator_3.setVisibility(View.VISIBLE);
					imgSeprator_4.setVisibility(View.VISIBLE);
					tblOfferFrom.setVisibility(View.VISIBLE);
					tblOfferTill.setVisibility(View.VISIBLE);
					//tblOfferDetails.setVisibility(View.VISIBLE);
				}


			}


		}catch(Exception ex)
		{
			ex.printStackTrace();

		}


		btnChooseImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				openGallery();
				isRemote=false;
			}
		});

		btnTakeImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				takePicture();
				isRemote=false;
			}
		});

		btnImageChooser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				dialog.show();
				btnChooseImage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						openGallery();
						isRemote=false;
					}
				});

				btnTakeImage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						takePicture();
						isRemote=false;
					}
				});

				btnRemoteImage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						isRemote=true;
						dialog.dismiss();
						remoteDialog.show();
					}
				});

				btnSubmitUrl.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						remoteurl=url.getText().toString();
						remoteDialog.dismiss();
					}
				});

			}
		});

		chkIsOffer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(chkIsOffer.isChecked()){
					is_offered="1";
					tblSelectDate_start.setVisibility(View.VISIBLE);
					tblselectTime_start.setVisibility(View.VISIBLE);
					tblSelectDate_end.setVisibility(View.VISIBLE);
					tblselectTime_end.setVisibility(View.VISIBLE);
					sepratorCheck.setVisibility(View.VISIBLE);
					imgSeprator_1.setVisibility(View.VISIBLE);
					imgSeprator_2.setVisibility(View.VISIBLE);
					imgSeprator_3.setVisibility(View.VISIBLE);
					imgSeprator_4.setVisibility(View.VISIBLE);
					tblOfferFrom.setVisibility(View.VISIBLE);
					tblOfferTill.setVisibility(View.VISIBLE);
					//tblOfferDetails.setVisibility(View.VISIBLE);
				}else{
					is_offered="";
					tblSelectDate_start.setVisibility(View.GONE);
					tblselectTime_start.setVisibility(View.GONE);
					tblSelectDate_end.setVisibility(View.GONE);
					tblselectTime_end.setVisibility(View.GONE);
					sepratorCheck.setVisibility(View.GONE);
					imgSeprator_1.setVisibility(View.GONE);
					imgSeprator_2.setVisibility(View.GONE);
					imgSeprator_3.setVisibility(View.GONE);
					imgSeprator_4.setVisibility(View.GONE);
					tblOfferFrom.setVisibility(View.GONE);
					tblOfferTill.setVisibility(View.GONE);
					//tblOfferDetails.setVisibility(View.GONE);
				}
			}
		});

		//Review post
		btnReview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				editButton.setText(getString(R.string.edit));

				if(edtBroadTitle.getText().toString().length()==0)
				{
					Toast.makeText(context, "Please enter title", Toast.LENGTH_SHORT).show();
				}
				else if(edtBroadDesc.getText().toString().length()==0)
				{
					Toast.makeText(context, "Please enter description", Toast.LENGTH_SHORT).show();
				}
				//If its offer
				else if(is_offered.equalsIgnoreCase("1"))
				{
					if(offerDate_start.getText().toString().length()==0){
						Toast.makeText(context, "Please select offer start date", Toast.LENGTH_SHORT).show();
					}
					else if(offerTime_start.getText().toString().length()==0){
						Toast.makeText(context, "Please select offer start time", Toast.LENGTH_SHORT).show();
					}else if(offerDate_end.getText().toString().length()==0){
						Toast.makeText(context, "Please select offer end date", Toast.LENGTH_SHORT).show();
					}else if(offerTime_end.getText().toString().length()==0){
						Toast.makeText(context, "Please select offer end time", Toast.LENGTH_SHORT).show();
					}else{
					
						makePostScroll.setVisibility(ViewGroup.GONE);
						viewPostScroll.setVisibility(ViewGroup.VISIBLE);

						txtViewOfferDate_start.setText("Offer Starts From "+offerDate_start.getText().toString()+offerTime_start.getText().toString());
						txtViewOfferDate_end.setText("Offer Ends on "+offerDate_end.getText().toString()+offerTime_end.getText().toString());
						txtViewOfferDetail.setText(edtBroadDesc.getText().toString());
						txtViewOfferTitle.setText(edtBroadTitle.getText().toString());
					}
				}
				//If its not offer
				else
				{
					Log.i("IS OFFER", "IS OFFER "+is_offered);
					makePostScroll.setVisibility(ViewGroup.GONE);
					viewPostScroll.setVisibility(ViewGroup.VISIBLE);

					txtViewOfferDate_start.setText("Offer Starts From "+offerDate_start.getText().toString()+offerTime_start.getText().toString());
					txtViewOfferDate_end.setText("Offer Ends on "+offerDate_end.getText().toString()+offerTime_end.getText().toString());
					txtViewOfferDetail.setText(edtBroadDesc.getText().toString());
					txtViewOfferTitle.setText(edtBroadTitle.getText().toString());
					makePostScroll.setVisibility(ViewGroup.GONE);
					viewPostScroll.setVisibility(ViewGroup.VISIBLE);

					/*postBroadCast(false);*/
				}

				if(viewPostScroll.isShown())
				{
					if(is_offered.equalsIgnoreCase("1")){
						txtViewOfferDate_start.setVisibility(View.VISIBLE);
						txtViewOfferDate_end.setVisibility(View.VISIBLE);
					}else{
						txtViewOfferDate_start.setVisibility(View.INVISIBLE);
						txtViewOfferDate_end.setVisibility(View.INVISIBLE);
					}
				}


			}
		});

		//Allow user to Edit Broadcast
		editButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!prog.isShown())
				{
					editButton.setText(getString(R.string.edit));
					makePostScroll.setVisibility(ViewGroup.VISIBLE);
					viewPostScroll.setVisibility(ViewGroup.GONE);
				}
			}
		});

		//Post Broadcast.
		postButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(edtBroadTitle.getText().toString().length()==0)
				{
					Toast.makeText(context, "Please enter broadcast title", Toast.LENGTH_SHORT).show();
				}
				else if(edtBroadDesc.getText().toString().length()==0)
				{
					Toast.makeText(context, "Please enter broadcast description", Toast.LENGTH_SHORT).show();
				}
				//If its offer
				else if(is_offered.equalsIgnoreCase("1"))
				{
					if(offerDate_start.getText().toString().length()==0){
						Toast.makeText(context, "Please select offer validity date", Toast.LENGTH_SHORT).show();
					}else if(offerDate_start.getText().toString().length()==0){
						Toast.makeText(context, "Please select offer validity time", Toast.LENGTH_SHORT).show();
					}else{
						Log.i("IS OFFER", "IS OFFER "+is_offered);
						if(!prog.isShown())
						{
							Log.i("IS OFFER", "IS OFFER "+is_offered);
							if(!prog.isShown()){
								if(isSureShop){
									Log.d("DATE","DATE " + calenderFullDate);
									Log.d("place_id","place_id " + place_id);
									Log.d("date_type","date_type " + date_type);
									callTalkNow(place_id, date_type, calenderFullDate,post_type,gender, mall_id);
								}
								else if(isFavouriteStore){
									Log.d("place_id","place_id " + place_id);
									Log.d("post_type","post_type " + post_type);
									callTalkNow(place_id, post_type,gender, mall_id, "");
								}
								else if(isMallWise){
									Log.d("place_id","place_id " + place_id);
									Log.d("mall_id","mall_id " + mall_id);
									Log.d("post_type","post_type " + post_type);
									callTalkNow(place_id,mall_id, post_type,gender);
								}
								else if(isActiveMall){
									Log.d("place_id","place_id " + place_id);
									Log.d("mall_id","mall_id " + mall_id);
									Log.d("post_type","post_type " + post_type);
									callTalkNow(place_id,mall_id, 4, gender);
								}
								else{		
									postBroadCast(true);			
								}
					
							}
							else{
								Toast.makeText(context, "Please wait posting is in progress", Toast.LENGTH_SHORT).show();
							}
						}else{
							Toast.makeText(context, "Please wait posting is in progress", Toast.LENGTH_SHORT).show();
						}
					}
				}
				//If its not offer
				else
				{
					Log.i("IS OFFER", "IS OFFER "+is_offered);

					if(!prog.isShown())
					{
						Bundle b = getIntent().getExtras();

						if(b!=null){
							if(isSureShop){

							}
							else if(isFavouriteStore){


							}else if(isMallWise){


							}
							else{
								postBroadCast(false);

							}

						}

					}else{
						Toast.makeText(context, "Please wait posting is in progress", Toast.LENGTH_SHORT).show();
					}

				}
			}
		});

		offerDate_start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chooseStartDate();
			}
		});
		offerTime_start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chooseStartTime();	
			}
		});
		
		offerDate_end.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chooseEndDate();
			}
		});
		offerTime_end.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chooseEndTime();	
			}
		});


		//delete image
		deleteImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*imgThumbPreview.setImageResource(R.drawable.placeholder);
				reviewImage.setImageResource(R.drawable.placeholder);*/
				imgThumbPreview.setVisibility(View.GONE);
				reviewImage.setVisibility(View.GONE);

				FILE_PATH="";
				FILE_NAME="";
				encodedpath="";
				isImageBroadCast=false;
				deleteImage.setVisibility(View.GONE);
			}
		});
	}
	
	private String getUserId(){
		HashMap<String,String>user_details=session.getUserDetails();
		return user_details.get(KEY_USER_ID).toString();
		
	}
	
	private String getAuthId(){
		HashMap<String,String>user_details=session.getUserDetails();
		return user_details.get(KEY_AUTH_ID).toString();
	}

	public static String encodeTobase64(Bitmap image)
	{
		Bitmap immagex=image;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] b = baos.toByteArray();
		String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

		Log.e("LOOK", imageEncoded);
		return imageEncoded;
	}
	public static Bitmap decodeBase64(String input) 
	{
		byte[] decodedByte = Base64.decode(input, 0);
		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	}

	void postBroadCast(boolean isOffer){
		JSONObject json = new JSONObject();
		//Array of Place Id
		JSONArray jsonArray=new JSONArray(SELECTED_STORE_ID);
		//place id array
		try{
			json.put("place_id", jsonArray);

			json.put("title", edtBroadTitle.getText().toString());//title

			json.put("description", edtBroadDesc.getText().toString());//body
			json.put("url", remoteurl);//url
			json.put("tags", edtTags.getText().toString());//tags
			json.put("type", "photo");//type
			json.put("state", "published");//state

			//json.put("status", "0");// unapprove status

			json.put("format", "html");//format
			if(is_offered.length()==0){
				json.put("is_offered", "0");
			}else{
				json.put("is_offered","1");
			}
			if(isOffer){
				json.put("offer_date_time", offerDate_end.getText().toString()+offerTime_end.getText().toString());//offer_date_time + offer_time
				json.put("offer_start_date_time", offerDate_start.getText().toString()+offerTime_start.getText().toString());//offer_date_time + offer_time
			}else{
				json.put("offer_date_time", "");
				json.put("offer_start_date_time", "");//offer_date_time + offer_time
			}

			json.put("user_id", getUserId()); // user_id
			json.put("auth_id", getAuthId()	); // auth_id

			//image object array
			if(isImageBroadCast){
				//File Array
				try{
					if(!FILE_PATH.equals("") && FILE_PATH.length()!=0){
						byte [] b=imageTobyteArray(FILE_PATH);
						if(b!=null)
							encodedpath=Base64.encodeToString(b, Base64.DEFAULT);
						Log.i("Encode ", "Details : "+encodedpath);
					}else{
						encodedpath="undefined";
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				
				JSONArray fileArray=new JSONArray();
				JSONObject fileobject = new JSONObject();

				fileobject.put("filename", FILE_NAME);//filename
				fileobject.put("filetype", FILE_TYPE);
				fileobject.put("userfile", encodedpath);
				fileArray.put(fileobject);

				json.put("file",fileArray);
			}

		}catch(JSONException jex){
			jex.printStackTrace();
		}catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		Log.i("JSON", json.toString());
		prog.setVisibility(View.VISIBLE);

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);

		MyClass myClass = new MyClass(context);
		myClass.postRequest(RequestTags.TagCreateBroadcast, headers, json);	
	}

	public void chooseStartDate(){

		dateTime.add(Calendar.DAY_OF_MONTH, 1);
		new DatePickerDialog(context, startDate, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
		DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
				dateTime.set(Calendar.YEAR,year);
				dateTime.set(Calendar.MONTH, monthOfYear);
				dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				updateStartDate();
			}
		};

	}

	public void chooseEndTime(){
		new TimePickerDialog(context, endTime, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), true).show();
	}
	

	public void chooseEndDate(){

		dateTime.add(Calendar.DAY_OF_MONTH, 1);
		new DatePickerDialog(context, endDate, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
		DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
				dateTime.set(Calendar.YEAR,year);
				dateTime.set(Calendar.MONTH, monthOfYear);
				dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				updateEndDate();
			}
		};

	}

	public void chooseStartTime(){
		new TimePickerDialog(context, startTime, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), true).show();
	}

	DatePickerDialog.OnDateSetListener startDate=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			dateTime.set(Calendar.YEAR,year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateStartDate();

		}
	};

	TimePickerDialog.OnTimeSetListener startTime=new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTime.set(Calendar.MINUTE,minute);
			updateStartTime();
		}
	};
	
	DatePickerDialog.OnDateSetListener endDate=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			dateTime.set(Calendar.YEAR,year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateEndDate();

		}
	};

	TimePickerDialog.OnTimeSetListener endTime=new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTime.set(Calendar.MINUTE,minute);
			updateEndTime();
		}
	};
	
	
	private void updateStartTime() {

		DateFormat  dt2=new SimpleDateFormat(" HH:mm:ss a");
		miliseconds=dateTime.getTimeInMillis();
		offerTime_start.setText(dt2.format(dateTime.getTime()));
	}
	private void updateStartDate() {

		Log.i("Current Date", "Current Date "+dateTimeCurrent.get(Calendar.YEAR)+" "+dateTimeCurrent.get(Calendar.MONTH)+" "+dateTimeCurrent.get(Calendar.DAY_OF_MONTH));
		Log.i("Current Date", "Current Date changed "+dateTime.get(Calendar.YEAR)+" "+dateTime.get(Calendar.MONTH)+" "+dateTime.get(Calendar.DAY_OF_MONTH));
		Date d1=dateTimeCurrent.getTime();
		Date d2=dateTime.getTime();

		if(dateDiff(d2, d1)<0){
			showToast("Slected date cannot be less than current date");
			
		}else{			
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dtReview=new SimpleDateFormat("dd-MMMM-yyyy");
			offerDate_start.setText(dt.format(dateTime.getTime()));

		}
	}
	private void updateEndTime() {

		DateFormat  dt2=new SimpleDateFormat(" HH:mm:ss a");
		miliseconds=dateTime.getTimeInMillis();
		offerTime_end.setText(dt2.format(dateTime.getTime()));
	}
	private void updateEndDate() {

		Log.i("Current Date", "Current Date "+dateTimeCurrent.get(Calendar.YEAR)+" "+dateTimeCurrent.get(Calendar.MONTH)+" "+dateTimeCurrent.get(Calendar.DAY_OF_MONTH));
		Log.i("Current Date", "Current Date changed "+dateTime.get(Calendar.YEAR)+" "+dateTime.get(Calendar.MONTH)+" "+dateTime.get(Calendar.DAY_OF_MONTH));
		Date d1=dateTimeCurrent.getTime();
		Date d2=dateTime.getTime();

		if(dateDiff(d2, d1)<0){
			showToast("Slected date cannot be less than current date");
			
		}else{			
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dtReview=new SimpleDateFormat("dd-MMMM-yyyy");
			offerDate_end.setText(dt.format(dateTime.getTime()));

		}
	}

	public long dateDiff(Date d1,Date d2){
		long datediff=0;
		datediff=(d1.getTime()-d2.getTime())/(24 * 60 * 60 * 1000);
		Log.i("daysBetween", "daysBetween "+datediff);
		return datediff;
	}

	//Date Differnece
	public static long daysBetween(Calendar startDate, Calendar endDate) {  
		Calendar date = (Calendar) startDate.clone();  
		long daysBetween = 0;  
		while (date.before(endDate)) {  
			date.add(Calendar.DAY_OF_MONTH, 1);  
			daysBetween++;  
		}  

		Log.i("daysBetween", "daysBetween "+daysBetween);
		return daysBetween;  
	} 


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishto();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			/*session.logoutUser();
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/
		}
		if(item.getTitle().toString().equalsIgnoreCase("Inorbit Malls"))
		{
			finishto();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishto();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();	
	}


	/*********************************************** Photo Section ********************************************************/

	//Convert image into byte array
	byte[] imageTobyteArray(String path)
	{	byte[] b = null ;
	if(!path.equals("") || path.length()!=0)
	{


		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inDither=true;//optional
		options.inPreferredConfig=Bitmap.Config.RGB_565;//optional

		Bitmap bm = BitmapFactory.decodeFile(path,options);

		options.inSampleSize = calculateInSampleSize(options, 320, 320);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		bm=BitmapFactory.decodeFile(path,options);


		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object   
		b= baos.toByteArray();
	}
	return b;
	}

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}


	private void takePicture() {
		try {  
			Intent i = new Intent(CreateBroadCast.this, PhotoActivity.class);
			i.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);
			startActivityForResult(i, REQUEST_CODE_TAKE_PICTURE);
		} catch (ActivityNotFoundException e) {

			Log.d("", "cannot take picture", e);
		}
	}

	private void openGallery() {	
		Intent i = new Intent(CreateBroadCast.this, PhotoActivity.class);
		i.putExtra("REQUEST_CODE_GALLERY", REQUEST_CODE_GALLERY);
		startActivityForResult(i, REQUEST_CODE_GALLERY);
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub

		if (resultCode != RESULT_OK) {
			return;
		}
		Bitmap bitmap;
		switch (requestCode) {
		case REQUEST_CODE_GALLERY:
			try {

				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";
				if(data != null) {
					gallery_byte=data.getByteArrayExtra("byteArray");
					setPhoto(BitmapFactory.decodeByteArray(gallery_byte , 0,gallery_byte.length));
					registerPhotoEvent("Gallery");
					/*bitmap = BitmapFactory.decodeFile(encodedpath);
					setPhoto(bitmap);*/
				}

			} catch (Exception e) {
				Log.e("", "Error while creating temp file", e);
			}
			break;
		case REQUEST_CODE_TAKE_PICTURE:
			
			FILE_TYPE="image/jpeg";
			FILE_NAME="temp_photo.jpg";
			FILE_PATH="/sdcard/temp_photo.jpg";

			if(data != null) {
				gallery_byte=data.getByteArrayExtra("byteArray");
				setPhoto(BitmapFactory.decodeByteArray(gallery_byte , 0,gallery_byte.length));
				/*bitmap = BitmapFactory.decodeFile(encodedpath);
				setPhoto(bitmap);*/
				registerPhotoEvent("Camera");
			}		
			break;
		case REQUEST_CODE_CROP_IMAGE:

			/*FILE_TYPE="image/jpeg";
			FILE_NAME="temp_photo.jpg";
			FILE_PATH="/sdcard/temp_photo.jpg";*/

			FILE_PATH="";
			FILE_NAME="";
			//	encodedpath="";

			//String path = data.getStringExtra(CropImage.IMAGE_PATH);
			String path = "";
			if (path == null) {

				return;
			}

			bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
			setPhoto(bitmap);
			if(dialog.isShowing())
			{
				dialog.dismiss();
			}
			else if(remoteDialog.isShowing())
			{
				remoteDialog.dismiss();
			}
			isImageBroadCast=true;
			/*mImageView.setImageBitmap(bitmap);*/
			break;
		}

		super.onActivityResult(requestCode, resultCode, data);

	}


	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

	/*private void startCropImage() {
		Intent intent = new Intent(this, CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.SCALE, true);

		intent.putExtra(CropImage.ASPECT_X, 3);
		intent.putExtra(CropImage.ASPECT_Y, 3);

		startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
	}*/


	//Scaling down exisitng bitmap
	public static Bitmap decodeSampledBitmap(byte [] bitmap,  int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		/* BitmapFactory.decodeResource(res, resId, options);*/
		BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length, options);
		/* return BitmapFactory.decodeResource(res, resId, options);*/
	}

	//converting bitmap to byet array
	public static byte[] convertBitmapToByteArray(Bitmap bitmap) {
		if (bitmap == null) {
			return null;
		} else {
			byte[] b = null;
			try {
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.JPEG, 0, byteArrayOutputStream);
				b = byteArrayOutputStream.toByteArray();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return b;
		}
	}


	//Cropping image
	void perFormCrop(Uri picUri){
		try {
			//call the standard crop action intent (the user device may not support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			//indicate image type and Uri
			cropIntent.setDataAndType(picUri, "image/*");
			//set crop properties
			cropIntent.putExtra("crop", "true");
			//indicate aspect of desired crop
			cropIntent.putExtra("aspectX", 1);
			cropIntent.putExtra("aspectY", 1);
			//indicate output X and Y
			cropIntent.putExtra("outputX", 256);
			cropIntent.putExtra("outputY", 256);
			//retrieve data on return
			cropIntent.putExtra("return-data", true);
			//start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, PIC_CROP);

		}
		catch(ActivityNotFoundException anfe){
			//display an error message
			String errorMessage = "Whoops - your device doesn't support the crop action!";
			Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.show();
		}
	}


	void setPhoto(Bitmap bitmap){
		try{
			FILE_TYPE="image/jpeg";
			FILE_NAME="temp_photo.jpg";
			FILE_PATH="/sdcard/temp_photo.jpg";

			imgThumbPreview.setImageBitmap(bitmap);
			imgThumbPreview.setScaleType(ScaleType.FIT_CENTER);
			reviewImage.setImageBitmap(bitmap);
			reviewImage.setScaleType(ScaleType.FIT_XY);
			deleteImage.setVisibility(View.VISIBLE);
			imgThumbPreview.setVisibility(View.VISIBLE);
			reviewImage.setVisibility(View.VISIBLE);

			editButton.setText(getString(R.string.edit));
			editButton.setTextColor(Color.WHITE);
			isImageBroadCast=true;

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Image path for selected image from gallery.
	void setImagePath(Uri uri){
		FILE_PATH=getRealPathFromURI(uri);
		Log.i("Image  : ", "Image Path Choosen : "+getRealPathFromURI(uri));

	}

	void setImagePath(String imagePath){
		FILE_PATH=imagePath;
		Log.i("Image  : ", "Image Path Captured : "+imagePath);
	}


	//Image name for selected image from gallery.
	void setImageName(String imageName){
		FILE_NAME=imageName;
		Log.i("Image  : ", "Image Name : "+imageName);
	}

	//Get absolute path of of Image File
	private String getRealPathFromURI(Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
		Cursor cursor = loader.loadInBackground();
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}


	/**
	 * Get the uri of the captured file
	 * @return A Uri which path is the path of an image file, stored on the dcim folder
	 */
	private  Uri getImageUri() {
		
		DateFormat  dt=new SimpleDateFormat("d MMM yyyy");
		DateFormat  dt2=new SimpleDateFormat("hh:mm a");
		Date date=new Date();
		/*String CAPTURE_TITLE=dt.format(date).toString()+" "+dt2.format(date).toString()+".jpeg";*/
		String CAPTURE_TITLE="capured"+".jpeg";
		/*			String CAPTURE_TITLE="hi.jpg";*/
		File file = new File(Environment.getExternalStorageDirectory() + "/.citypics", CAPTURE_TITLE);
		Uri imgUri = Uri.fromFile(file);
		Log.i("Captured", "File Path: "+file.getAbsolutePath()+" Name : "+file.getName());
		/*		txtImagePath.setText(file.getAbsolutePath());
					txtImageName.setText(CAPTURE_TITLE);*/
		FILE_PATH=file.getAbsolutePath();
		FILE_TYPE="image/jpeg";
		FILE_NAME=CAPTURE_TITLE;

		return imgUri;
	}





	void finishto(){
		if(edtBroadTitle.getText().toString().length()!=0 || edtBroadDesc.getText().toString().length()==0){
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Inorbit");
			alertDialogBuilder3
			.setMessage("Do you want to discard this offer?")
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
					//Intent intent=new Intent(context, MerchantsHomeGrid.class);
					//Intent intent=new Intent(context,MerchantHome.class);
					//startActivity(intent);
					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

				}
			})
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
				}
			})
			;

			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();
			
		}else if(!prog.isShown()){
		
			Intent intent=new Intent(context,MerchantHome.class);
			startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}else{
			MyToast.showToast(context, "Please wait while posting",3);
		}
	}

	// call using calender
	public void callTalkNow(String place_id, String date_type, String calenderFullDate, String post_type, String gender, String mall_id){

		//Intent intent=new Intent(context, TalkNowService.class);
		//intent.putExtra("talkNow",talkNowBroadCast);
		Intent intent = null;
		intent.putExtra("URL", getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api)+getResources().getString(R.string.special));
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", getUserId());
		intent.putExtra("auth_id", getAuthId());
		intent.putExtra("mall_id", mall_id);
		intent.putExtra("place_id", place_id);
		intent.putExtra("post_type", post_type);
		intent.putExtra("date_type", date_type);
		intent.putExtra("special_date", calenderFullDate);
		intent.putExtra("title", edtBroadTitle.getText().toString());
		intent.putExtra("description", edtBroadDesc.getText().toString());

		if(gender.trim().length()>0){
		 intent.putExtra("gender", gender);

		}

		

		//If its Image Broadcast
		if(isImageBroadCast){		
			//if local image
			if(isRemote==false){
				imageType="photo";
				if(FILE_PATH.equals("") && FILE_PATH.length()==0){
					Toast.makeText(context, "Please Select photo to upload", Toast.LENGTH_SHORT).show();
				}else{
					try{
						if(!FILE_PATH.equals("") && FILE_PATH.length()!=0){
							byte [] b=imageTobyteArray(FILE_PATH);
							if(b!=null)
								encodedpath=Base64.encodeToString(b, Base64.DEFAULT);
							Log.i("Encode ", "Details : "+encodedpath);
						}else{
							encodedpath="undefined";
						}
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}else{
				imageType="url";
				FILE_NAME="";
				FILE_TYPE="";
				encodedpath="";
			}

			intent.putExtra("type", "photo");
			intent.putExtra("isImageBroadCast", isImageBroadCast);
			intent.putExtra("filename", FILE_NAME);
			intent.putExtra("filetype", FILE_TYPE);
			intent.putExtra("userfile", encodedpath);
		}
		intent.putExtra("offer_date_time", offerDate_end.getText().toString());
		intent.putExtra("offer_time", offerTime_end.getText().toString());
		//intent.putExtra("image_title", edtImageTitle.getText().toString());
		intent.putExtra("image_title", "");
		context.startService(intent);
		prog.setVisibility(View.VISIBLE);


	}

	// call using favourite store
	public void callTalkNow(String place_id, String post_type, String gender, String mall_id, String isFavourite){

		//Intent intent=new Intent(context, TalkNowService.class);
		//intent.putExtra("talkNow",talkNowBroadCast);
		Intent intent = null;
		intent.putExtra("URL", getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api)+getResources().getString(R.string.special));
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", getUserId());
		intent.putExtra("auth_id", getAuthId());
		intent.putExtra("mall_id", mall_id);
		intent.putExtra("place_id", place_id);
		intent.putExtra("post_type", post_type);
		intent.putExtra("title", edtBroadTitle.getText().toString());
		intent.putExtra("description", edtBroadDesc.getText().toString());

		if(gender.trim().length()>0){
			intent.putExtra("gender", gender);
		}
		//If its Image Broadcast
		if(isImageBroadCast){		
			//if local image
			if(isRemote==false){
				imageType="photo";
				if(FILE_PATH.equals("") && FILE_PATH.length()==0){
					Toast.makeText(context, "Please Select photo to upload", Toast.LENGTH_SHORT).show();
				}
				else{
					try{
						if(!FILE_PATH.equals("") && FILE_PATH.length()!=0){
							byte [] b=imageTobyteArray(FILE_PATH);
							if(b!=null)
								encodedpath=Base64.encodeToString(b, Base64.DEFAULT);
							Log.i("Encode ", "Details : "+encodedpath);
						}else{
							encodedpath="undefined";
						}
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}
			//if remote image
			else{
				imageType="url";
				FILE_NAME="";
				FILE_TYPE="";
				encodedpath="";
			}

			intent.putExtra("type", "photo");
			intent.putExtra("isImageBroadCast", isImageBroadCast);
			intent.putExtra("filename", FILE_NAME);
			intent.putExtra("filetype", FILE_TYPE);
			intent.putExtra("userfile", encodedpath);
		}
		intent.putExtra("offer_date_time", offerDate_end.getText().toString());
		intent.putExtra("offer_time", offerTime_end.getText().toString());
		//intent.putExtra("image_title", edtImageTitle.getText().toString());
		intent.putExtra("image_title", "");

		context.startService(intent);
		prog.setVisibility(View.VISIBLE);


	}


	// for active mall same parameters as of favourite users

	public void callTalkNow(String place_id, String active_mall_id, int post_type, String gender){
		
		//Intent intent=new Intent(context, TalkNowService.class);
		//intent.putExtra("talkNow",talkNowBroadCast);
		Intent intent = null;
		intent.putExtra("URL", getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api)+getResources().getString(R.string.special));
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", getUserId());
		intent.putExtra("auth_id", getAuthId());
		intent.putExtra("place_id", place_id);
		intent.putExtra("post_type", post_type + "");
		intent.putExtra("title", edtBroadTitle.getText().toString());
		intent.putExtra("description", edtBroadDesc.getText().toString());
		intent.putExtra("mall_id", active_mall_id);
		intent.putExtra("isActiveMall", true);

		if(gender.trim().length()>0){

			intent.putExtra("gender", gender);
			//Toast.makeText(context, "" + gender, 0).show();
		}

		//If its Image Broadcast
		if(isImageBroadCast)
		{		
			//if local image
			if(isRemote==false)
			{
				imageType="photo";
				if(FILE_PATH.equals("") && FILE_PATH.length()==0)
				{
					Toast.makeText(context, "Please Select photo to upload", Toast.LENGTH_SHORT).show();
				}
				else
				{

					try
					{
						/*if(!isReuse)
							{*/
						if(!FILE_PATH.equals("") && FILE_PATH.length()!=0)
						{
							byte [] b=imageTobyteArray(FILE_PATH);
							if(b!=null)
								encodedpath=Base64.encodeToString(b, Base64.DEFAULT);
							Log.i("Encode ", "Details : "+encodedpath);
						}
						else
						{
							encodedpath="undefined";
						}
						/*}else
							{

							}*/
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			}
			//if remote image
			else
			{
				imageType="url";
				FILE_NAME="";
				FILE_TYPE="";
				encodedpath="";
			}

			intent.putExtra("type", "photo");
			intent.putExtra("isImageBroadCast", isImageBroadCast);
			intent.putExtra("filename", FILE_NAME);
			intent.putExtra("filetype", FILE_TYPE);
			intent.putExtra("userfile", encodedpath);
		}
		//intent.putExtra("url", remoteurl);
		intent.putExtra("offer_date_time", offerDate_end.getText().toString());
		intent.putExtra("offer_time", offerTime_end.getText().toString());
		//intent.putExtra("image_title", edtImageTitle.getText().toString());
		intent.putExtra("image_title", "");

		context.startService(intent);
		prog.setVisibility(View.VISIBLE);


	}

	public void callTalkNow(String place_id,String mall_id, String post_type, String gender){

		//Intent intent=new Intent(context, TalkNowService.class);
		//intent.putExtra("talkNow",talkNowBroadCast);
		Intent intent=null;
		intent.putExtra("URL", getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api)+getResources().getString(R.string.special));
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", getUserId());
		intent.putExtra("auth_id", getAuthId());

		/*ArrayList<String>tempStore=new ArrayList<String>();
		tempStore.add(SELECTED_STORE_ID);*/
		//intent.putStringArrayListExtra("store_id", SELECTED_STORE_ID);

		intent.putExtra("place_id", place_id);
		intent.putExtra("post_type", post_type);
		intent.putExtra("mall_id", mall_id);
		//		intent.putExtra("special_date", calenderFullDate);

		intent.putExtra("title", edtBroadTitle.getText().toString());
		intent.putExtra("description", edtBroadDesc.getText().toString());

		if(gender.trim().length()>0){

			Log.d("SENDINGGENDER","SENDINGGENDER " +  gender);

			intent.putExtra("gender", gender);
			//Toast.makeText(context, "" + gender, 0).show();
		}
		

		//If its Image Broadcast
		if(isImageBroadCast)
		{		
			//if local image
			if(isRemote==false)
			{
				imageType="photo";
				if(FILE_PATH.equals("") && FILE_PATH.length()==0)
				{
					Toast.makeText(context, "Please Select photo to upload", Toast.LENGTH_SHORT).show();
				}
				else
				{

					try
					{
						/*if(!isReuse)
							{*/
						if(!FILE_PATH.equals("") && FILE_PATH.length()!=0)
						{
							byte [] b=imageTobyteArray(FILE_PATH);
							if(b!=null)
								encodedpath=Base64.encodeToString(b, Base64.DEFAULT);
							Log.i("Encode ", "Details : "+encodedpath);
						}
						else
						{
							encodedpath="undefined";
						}
						/*}else
							{

							}*/
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			}
			//if remote image
			else
			{
				imageType="url";
				FILE_NAME="";
				FILE_TYPE="";
				encodedpath="";
			}

			intent.putExtra("type", "photo");
			intent.putExtra("isImageBroadCast", isImageBroadCast);
			intent.putExtra("filename", FILE_NAME);
			intent.putExtra("filetype", FILE_TYPE);
			intent.putExtra("userfile", encodedpath);
		}
		//intent.putExtra("url", remoteurl);
		intent.putExtra("offer_date_time", offerDate_end.getText().toString());
		intent.putExtra("offer_time", offerTime_end.getText().toString());
		//intent.putExtra("image_title", edtImageTitle.getText().toString());
		intent.putExtra("image_title", "");

		context.startService(intent);
		prog.setVisibility(View.VISIBLE);


	}

	/*@Override
	public void onTalkNow(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		prog.setVisibility(View.GONE);

		String status = resultData.getString("talknow_status");
		String msg = resultData.getString("talknow_msg");

		if(status.equalsIgnoreCase("true")){
			if(isSureShop){
				EventTracker.logEvent(getResources().getString(R.string.mShake2Win_CalendarPosted),false);
			}else if(isFavouriteStore){
				EventTracker.logEvent(getResources().getString(R.string.mShake2Win_StoreFavouritePosted),false);
			}else if(isMallWise){
				EventTracker.logEvent(getResources().getString(R.string.mShake2Win_SpecificMallPosted),false);
			}else if(isActiveMall){
				EventTracker.logEvent(getResources().getString(R.string.mShake2Win_ActiveMallPosted),false);
			}

			showToast(msg);
			//Intent intent=new Intent(context, MerchantsHomeGrid.class);
			Intent intent=new Intent(context,MerchantHome.class);
			startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);


		}else{

			showToast(msg);

		}

	}*/

	class BroadCastOfferUpdate extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String success = bundle.getString("SUCCESS");
					String code = "";
					String msg = bundle.getString("MESSAGE");
					if(success.equalsIgnoreCase("true")){
						EventTracker.logEvent(getResources().getString(R.string.newPost_Posted), false);
						Toast.makeText(context, msg, 0).show();
						//Intent intent1=new Intent(context, ViewPost.class);
						Intent intent1=new Intent(context, ViewPostNew.class);
						intent1.putExtra("STORE_ID",SELECTED_STORE_ID.get(0));
						startActivity(intent1);
						finish();
					}else if(success.equalsIgnoreCase("false")){
						EventTracker.logEvent(getResources().getString(R.string.newPost_PostFailed), false);
						code = bundle.getString("CODE");

						if(code.equalsIgnoreCase("-116")){
							logOutUser();
						}

						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){

							int code1 = bundle.getInt("CODE");
							String message=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CategorySearch");
							}

							Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
						}
					}

				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	void registerPhotoEvent(String source){
		Map<String, String> params = new HashMap<String, String>();
		params.put("Source", source);
		EventTracker.logEvent(getResources().getString(R.string.newPost_ImageAdded), params);
	}

	void logOutUser(){
		session.logoutUser();
		Intent sIntent = new Intent(context, MerchantLogin.class);
		startActivity(sIntent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
		try{
			if(b_OfferUpdate!=null){
				context.unregisterReceiver(b_OfferUpdate);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{

			EventTracker.startLocalyticsSession(getApplicationContext());

			IntentFilter filter = new IntentFilter(RequestTags.TagCreateBroadcast);
			context.registerReceiver(b_OfferUpdate = new BroadCastOfferUpdate(), filter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}



}


package com.phonethics.inorbit;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class RestaurantMenuActivity extends SherlockActivity {
	private ActionBar mActionbar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		setContentView(R.layout.restaurant_menu_layout);
		super.onCreate(savedInstanceState);
		
		initActionBar();
		
		WebView w = (WebView) findViewById(R.id.menu_restaurant);
		w.loadUrl("http://stage.phonethics.in/inorbitapp/menu/view/"+getIntent().getStringExtra("store_id"));
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if (item.getTitle().toString().equalsIgnoreCase("Menu")) {
			/*Intent intent = new Intent();
			setResult(5, intent);*/
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		return true;
	}
	
	void initActionBar(){
		mActionbar	= getSupportActionBar();
		mActionbar.setTitle(getString(R.string.actionBarTitle));
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.show();
	}

}

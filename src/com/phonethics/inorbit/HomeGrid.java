package com.phonethics.inorbit;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.Animation.AnimationListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.phoenthics.adapter.MallOffersAdapter;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.OffersNew.StoresOffer;
import com.phonethics.inorbit.syncadapter.AuthenticatorService;
import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;
import com.phonethics.notification.LocalNotification;
/*import com.urbanairship.push.PushManager;*/

public class HomeGrid extends SherlockFragmentActivity implements OnNavigationListener {

	private GridView grid_home;
	static ArrayList<String>gridItems;
	private ArrayList<String>cityNames;

	static ArrayList<Integer>gridItemIcons;
	static Activity context;
	private  ActionBar actionBar;
	private  String business_id="292";
	private  int sposition=0;
	private  int imageCount =0;

	static SessionManager session;
	static boolean gridLayout = true;
	static boolean mBDebug = true;


	//Animating Button
	static DecelerateInterpolator sDecelerator = new DecelerateInterpolator();
	static OvershootInterpolator sOvershooter = new OvershootInterpolator(10f);

	private  Vibrator vibrate;
	private  Runnable runnable_anim;
	AlertDialog city_chooser_dialog;
	static GridView gridHome;
	static Map<String, String> param = new HashMap<String, String>();
	static String ACTIVE_MALL_NAME = "";


	static String USER_ID="";
	static String AUTH_ID="";

	//User Name
	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";
	
	boolean isLoggedInCustomer;
	
	private  ArrayList<String>dropdownItems=new ArrayList<String>();
	private  String key="place_id";
	private String value;
	
	private  int dropdown_position;
	
	private ViewPager viewCustomerPager;
	
	private ArrayList<String> pages;

	private LinearLayout pagerdots;
	private TextView txtDots[];		

	private Typeface tf;
	//Api Urls
	static String API_HEADER;
	static String API_VALUE;

	static String STORE_URL;
	static String AREA_URL;
	static String ISO_URL;
	String countryCode;

	private static DBUtil dbutil;
	private NetworkCheck network;

	String GET_CATEGORIES;
	String selected_position_id = "";

	ArrayList<String> areaId;
	static String	fbUrl = "";
	TextView txtIndicator_1,txtIndicator_2;

	static String	 TAB = "Tab_";
	InputMethodManager imm;
	static GridViewLayoutAdapter gridAdapter;
	private String ACTIVE_MALL_ID;

	private TextView mTvOffer; 
	private Handler mHandlerOffer;
	private Runnable runnable_Tv;
	private int offerCount = 0;
	private ArrayList<PostDetail> mArrOFFERS;
	private StoresOffer offersBroadCast;
	private Handler handler_anim;

	// The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.phonethics.inorbit.syncadapter.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "Inorbit.in";
    // The account name
    public static final String ACCOUNT = "Inorbit";
    // Instance fields
    Account mAccount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_home_grid);
		/*setContentView(R.layout.homegridstatic);*/
		context=this;  
		
		network = new NetworkCheck(context);
		dbutil = new DBUtil(context);
		
		/*
		 * API KEY
		 */
		API_HEADER =getResources().getString(R.string.api_header);
		API_VALUE =getResources().getString(R.string.api_value);
		
		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		// Areas
		AREA_URL = getResources().getString(R.string.areas);
		TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		countryCode = tm.getSimCountryIso();
		
		//ISO_URL = "?iso-code=" + countryCode;

		GET_CATEGORIES = getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api)+getResources().getString(R.string.dateCat);

		imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

		session=new SessionManager(context);
		pages =new ArrayList<String>();
		areaId = new ArrayList<String>();
		cityNames = new ArrayList<String>();
		mTvOffer = (TextView) findViewById(R.id.text_offers);
		mTvOffer.setVisibility(View.GONE);
		pagerdots=(LinearLayout)findViewById(R.id.indicator);
		try
		{
			SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
			dropdown_position=pref.getInt("dropdown_position",0);
			Log.i("dropdown_position", "dropdown_position"+dropdown_position);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		viewCustomerPager=(ViewPager)findViewById(R.id.viewCustomerPager);
		txtIndicator_1 = (TextView) findViewById(R.id.text_indicator_1);
		txtIndicator_2 = (TextView) findViewById(R.id.text_indicator_2);
		ImageView img_flip_1 = (ImageView) findViewById(R.id.img_flip);
		ImageView img_flip_2 = (ImageView) findViewById(R.id.img_flip_1);
		mArrOFFERS = new ArrayList<PostDetail>();
		
		ArrayList<Integer> urls = new ArrayList<Integer>();
		urls.add(R.drawable.menu_bg_orange);
		urls.add(R.drawable.menu_bg_orange_1);
		urls.add(R.drawable.menu_bg_orange_2);
		
		img_flip_2.setBackgroundResource(R.drawable.menu_bg_orange);

		vibrate=(Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);

		actionBar=getSupportActionBar();

		
		actionBar.setTitle("Inorbit");

		if(dbutil.getAreaRowCount()==0){
			if(network.isNetworkAvailable()){

				//getAreas();
			}else{

			}
		} else {
			dropdownItems = dbutil.getAllAreas();
			areaId = dbutil.getAllMallIds(); 
			CustomSpinnerAdapter business_list=new CustomSpinnerAdapter(context, 0, 0, dropdownItems);

			//Setting Navigation Type.
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			actionBar.setListNavigationCallbacks(business_list, this);

			if(getResources().getBoolean(R.bool.fromSharePref)) {
				actionBar.setSelectedNavigationItem(dropdown_position);
			} else {
				String activeAreaId = dbutil.getActiveMallId();
				if(activeAreaId.equalsIgnoreCase("")){
					actionBar.setSelectedNavigationItem(0);
				}else{
					String areaName 	= dbutil.getAreaNameByInorbitId(activeAreaId);
					ACTIVE_MALL_NAME	= areaName;
					int areaPos 		= dropdownItems.indexOf(areaName);
					Log.d("", "Inorbit active id "+activeAreaId+" Area : "+areaName+" ArrayPos "+areaPos);
					actionBar.setSelectedNavigationItem(areaPos);
				}
			}
		}
		
		/**
		 * Array list to decide what number of pages we want in grid of customer dashboard.
		 */
		pages=new ArrayList<String>();
		pages.add("Page1");
		pages.add("Page2");

		PageAdapter adapter=new PageAdapter(getSupportFragmentManager(),getFragments());
		viewCustomerPager.setAdapter(adapter);

		tf = Typeface.createFromAsset(getAssets(),"fonts/AirstreamNF.ttf");
		//Setting Margins for Indicator
		LayoutParams params=new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		DisplayMetrics metrics=new DisplayMetrics();
		//Getting Display Densities
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		switch(metrics.densityDpi){
		case DisplayMetrics.DENSITY_LOW: {
			params.setMargins(10, -20, 0, 0);
			break;
		}
		case DisplayMetrics.DENSITY_MEDIUM:
		{

			params.setMargins(10, -40, 0, 0);
			break;
		}
		case DisplayMetrics.DENSITY_HIGH:
		{


			params.setMargins(10, -50, 0, 0);
			break;
		}
		case DisplayMetrics.DENSITY_XHIGH:
		{
			params.setMargins(10, -50, 0, 0);

			break;
		}
		}



		/**
		 * Set view pager indicator
		 * 
		 */
		txtIndicator_1.setTextColor(getResources().getColor(R.color.indicator_active));
		txtIndicator_2.setTextColor(getResources().getColor(R.color.indicator_inactive));
		txtIndicator_2.setTextSize(80);
		txtIndicator_1.setTextSize(80);
		txtIndicator_1.setText(".");
		txtIndicator_2.setText(".");
		txtIndicator_1.setTypeface(tf);
		txtIndicator_2.setTypeface(tf);


		viewCustomerPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				if(position==0){
					txtIndicator_1.setTextColor(getResources().getColor(R.color.indicator_active));
					txtIndicator_2.setTextColor(getResources().getColor(R.color.indicator_inactive));
				}else{
					txtIndicator_1.setTextColor(getResources().getColor(R.color.indicator_inactive));
					txtIndicator_2.setTextColor(getResources().getColor(R.color.indicator_active));
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});


		gridItems=new ArrayList<String>();
		gridItemIcons=new ArrayList<Integer>();



		/**
		 * 
		 * Titles of the Tabs on the customer dashboard.
		 * 
		 */
		gridItems.add("Directory");
		gridItems.add("Offers");
		gridItems.add("Search");

		gridItems.add("Shop");
		gridItems.add("Dine");
		gridItems.add("Entertainment");

		gridItems.add("Pamper Zone");
		gridItems.add("Mall Events");
		gridItems.add("Mall Info");

		gridItems.add("Park Assist");
		gridItems.add("Favourites");
		gridItems.add("Shake & Win");

		gridItems.add("Social Connect");


		if(session.isLoggedInCustomer()){
			gridItems.add("My Profile");
			HashMap<String,String>user_details=session.getUserDetailsCustomer();
			USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
			AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
			isLoggedInCustomer=session.isLoggedInCustomer();
		}else{
			gridItems.add("Sign In");
		}

		gridItems.add("App Info");
		
		gridItems.add("Share this app");
		gridItems.add("");
		gridItems.add("");


		
		/**
		 * 
		 * Icons on the customer dashboard.
		 * 
		 */
		gridItemIcons.add(R.drawable.all_stores_new);
		gridItemIcons.add(R.drawable.offers_new);
		gridItemIcons.add(R.drawable.search_new);

	
		gridItemIcons.add(R.drawable.shop_new);
		gridItemIcons.add(R.drawable.food_new);
		gridItemIcons.add(R.drawable.entertainment_new);

		gridItemIcons.add(R.drawable.services_new);
		gridItemIcons.add(R.drawable.news_event_new);
		gridItemIcons.add(R.drawable.mall_info_new);

		gridItemIcons.add(R.drawable.park_assist_new);
		gridItemIcons.add(R.drawable.favourites_new);
		gridItemIcons.add(R.drawable.shake_win_new);

		gridItemIcons.add(R.drawable.social_new);

		if(session.isLoggedInCustomer()){
			gridItemIcons.add(R.drawable.user_profile_new);
		}else{
			gridItemIcons.add(R.drawable.login_new);
		}

		gridItemIcons.add(R.drawable.about_us_new);
		
		gridItemIcons.add(R.drawable.ic_share);
		gridItemIcons.add(R.drawable.about_us_new);
		gridItemIcons.add(R.drawable.about_us_new);
		
		startImageAnimation(img_flip_2, img_flip_1, urls);
		if(session.toShowDialog()){
			cityNames = dbutil.getAllMallCitys();
			showPlaceChooserDialog();
		}

		getParentStoreInfo();
		
		// Create the dummy account
        mAccount = CreateSyncAccount(this);
        //sendSmsByManager();
	}//onCreate Ends
	
	public void sendSmsByManager() {
	    try {
	        // Get the default instance of the SmsManager
	        SmsManager smsManager = SmsManager.getDefault();
	        smsManager.sendTextMessage("+919029090720",
	                null, 
	                "New Message. What say!!!",
	                null,
	                null);
	        /*Toast.makeText(getApplicationContext(), "Your sms has successfully sent!",
	                Toast.LENGTH_LONG).show();*/
	    } catch (Exception ex) {
	        /*Toast.makeText(getApplicationContext(),"Your sms has failed...",
	                Toast.LENGTH_LONG).show();*/
	        ex.printStackTrace();
	    }
	}
	
	/**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
        }
        ContentResolver.setMasterSyncAutomatically(true);
        ContentResolver.setSyncAutomatically(newAccount, AUTHORITY, true);
        ContentResolver.addPeriodicSync(newAccount, AUTHORITY, Bundle.EMPTY, 10);
        return newAccount;
    }
	
	
	private List<Fragment> getFragments(){
		List<Fragment> fList = new ArrayList<Fragment>();

		for(int i=0;i<pages.size();i++){
			fList.add(PageFragment.newInstance(i));
		}
		return fList;
	}
	
	private void gcmNotificationClickedEvent() {
		JSONObject json = new JSONObject();
		try {
			json.put("device_id", session.getUdmIDForCustomer());
			json.put("device_os", "Android");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);

		MyClass myClass = new MyClass(context);
		myClass.postRequest(RequestTags.GCMNotificationClickedEvent, headers, json);
	}
	
	//Creating Pages with PageAdapter.
	class PageAdapter extends FragmentStatePagerAdapter{
		Activity context;
		ArrayList<String> pages;
		
		List<Fragment> fragments;
		
		public PageAdapter(FragmentManager fm,List<Fragment> fragments) {
			super(fm);
			// TODO Auto-generated constructor stub
			
			this.fragments = fragments;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return fragments.size();
		}
	}


	@SuppressLint("ValidFragment")
	public  static class PageFragment extends Fragment{
		String page="";
		
		View view;
		ImageView splashImage;
		ProgressBar prog;
		TextView txtPlaceText;
		Animation anim;
		int pos;
		
		public PageFragment(){
			
		}
		
		public static final PageFragment newInstance(int pos){
			PageFragment pageFragment = new PageFragment();
			Bundle bdl=new Bundle(1);
			bdl.putInt("PagePosition", pos);
			pageFragment.setArguments(bdl);
			return pageFragment;
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			// TODO Auto-generated method stub

			try
			{
				this.pos = getArguments().getInt("PagePosition");
				InorbitLog.d("Position Fragment "+this.pos);
				if(gridLayout){
					view			=inflater.inflate(R.layout.homegridstatic, null);
					gridHome		= (GridView) view.findViewById(R.id.grid_home);
					final RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.layout_grid);		
					gridHome.setAdapter( gridAdapter = new GridViewLayoutAdapter(context, this.pos));
					gridHome.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View view,
								int position, long arg3) {
							// TODO Auto-generated method stub
							/*if(pos==0){
								//startClass(position+1);
								startClassFromTag(view);
							}else{
								startClassFromTag(view);
							}*/
							
							startClassFromTag(view);
						}
					});

				}else{

				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
			return view;
		}

	}


	class CustomSpinnerAdapter extends ArrayAdapter<String> implements SpinnerAdapter{

		ArrayList<String>name;
		Activity context;
		LayoutInflater layoutInflater;
		public CustomSpinnerAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> name) {
			super(context, resource, textViewResourceId, name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.name=name;
			layoutInflater=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}


		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return name.get(position);
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = layoutInflater.inflate(R.layout.list_menus, parent, false);
			}
		
			TextView txtMenu = (TextView) convertView.findViewById(R.id.listText);
			txtMenu.setTypeface(InorbitApp.getTypeFace());
			txtMenu.setText(""+getItem(position));
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = layoutInflater.inflate(R.layout.list_dropdown_item, parent, false);
			}
			TextView txtMenu = (TextView) convertView.findViewById(R.id.txtSpinner);
			txtMenu.setTypeface(InorbitApp.getTypeFace());
			txtMenu.setText(""+getItem(position));
			return convertView;
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		switch (session.getApplicationFor()) {
		
		
		case 0:
			
			/**
			 * Customer only
			 * 
			 */
			break;
		case 1:
			
			/**
			 * merchant and customer
			 * 
			 */

			MenuItem extra=menu.add("Extra Settings").setIcon(R.drawable.switch_view);
			extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

			break;
		case 2:

			MenuItem extra1=menu.add("Extra Settings").setIcon(R.drawable.switch_view);
			extra1.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

			break;

		default:
			break;
		}
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getTitle().toString().equalsIgnoreCase("Extra Settings"))
		{
			
			
			SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
			Editor 	editor=prefs.edit();
			
			if(session.isLoggedIn()){
				
				/**
				 * 
				 * Switch the app to merchant dashboard, when the merchant is loggedin
				 * 
				 */
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();
				Intent intent=new Intent(context,MerchantHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}else{
				
				
				/**
				 * 
				 * Switch the app to merchant side, and prompt for merchant log in
				 * 
				 */
				Intent intent=new Intent(context,MerchantLogin.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		
		param.clear();
		ACTIVE_MALL_NAME = "";
		gridItems.clear();
		cityNames.clear();
		gridItemIcons.clear();
		USER_ID="";
		AUTH_ID="";	
		LocalNotification localNotification = new LocalNotification(context);
		localNotification.alarm();	
		if((city_chooser_dialog!=null)&&(city_chooser_dialog.isShowing())){
			city_chooser_dialog.dismiss();
		}
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();

	}


	static class ViewHolder{
		ImageView imgHomeGrid;
	}
	
	
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		
		business_id=itemPosition+"";

		if(getResources().getBoolean(R.bool.fromSharePref)){
			SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
		}else{
			String activeAreaPlaceId = dbutil.getMallIdByPos(""+(itemPosition+1)); // id == 0 == 1
			ACTIVE_MALL_ID = activeAreaPlaceId;
			dbutil.setActiveMall(activeAreaPlaceId);
			
			fbUrl = dbutil.getFbUrlByPos(""+(itemPosition+1));
			String activeAreaId = dbutil.getActiveMallId();
			String areaName 	= dbutil.getAreaNameByInorbitId(activeAreaId);
			ACTIVE_MALL_NAME	= areaName;
			Log.d("", "Inorbit ACTIVE_MALL_NAME "+ACTIVE_MALL_NAME);

			String activeMallId = session.getActiveMallId();
			if(activeMallId.equalsIgnoreCase(activeAreaPlaceId)){

			}else{
				session.setActiveMallId(activeAreaPlaceId);
				if(session.isLoggedInCustomer()){
					updateActiveMall();
					//setAreaTag(ACTIVE_MALL_NAME);
				}
			}
			//getOffersOfMall();

		}
		createInhouseAnalyticsEvent("0", "8", dbutil.getActiveMallId());
		
		return false;
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			/*			viewCustomerPager.getAdapter().notifyDataSetChanged();*/
			InorbitLog.d("Result code "+requestCode);
			
/*			if(session.isLoggedInCustomer()){
				gridItemIcons.set(13, R.drawable.user_profile_new);
			}else{
				gridItemIcons.set(13, R.drawable.login_new);
			}
			
			if(session.isLoggedInCustomer()){
				gridItems.set(13, "My Profile");
				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
				isLoggedInCustomer=session.isLoggedInCustomer();
			}else{
				gridItems.set(13, "Sign In");
			}
			PageAdapter adapter=new PageAdapter(getSupportFragmentManager(),getFragments());
			viewCustomerPager.setAdapter(adapter);
*/			
			
			if(session.isLoggedInCustomer()) {
				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
				isLoggedInCustomer=session.isLoggedInCustomer();
				gridItems.set(13, "My Profile");
				gridItemIcons.set(13, R.drawable.user_profile_new);
				gridAdapter.notifyDataSetChanged();

			}else{
				gridItems.set(13, "Sign In");
				gridItemIcons.set(13, R.drawable.login_new);
				gridAdapter.notifyDataSetChanged();
		
			}

/*			if(requestCode==2)
			{

				if(session.isLoggedInCustomer()) {
					HashMap<String,String>user_details=session.getUserDetailsCustomer();
					USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
					AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
					isLoggedInCustomer=session.isLoggedInCustomer();
					gridItems.set(13, "My Profile");
					gridItemIcons.set(13, R.drawable.user_profile_new);
					gridAdapter.notifyDataSetChanged();

				}else{
					gridItems.set(13, "Sign In");
					gridItemIcons.set(13, R.drawable.login_new);
					gridAdapter.notifyDataSetChanged();
			
				}
			}else */if(resultCode == 5){
				try{
					if(getResources().getBoolean(R.bool.fromSharePref)){
						SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
						dropdown_position=pref.getInt("dropdown_position",0);
						Log.i("dropdown_position", "dropdown_position"+dropdown_position);
						actionBar.setSelectedNavigationItem(dropdown_position);
					}else{
						String activeAreaId = dbutil.getActiveMallId();
						String areaName = dbutil.getAreaNameByInorbitId(activeAreaId);
						int areaPos = dropdownItems.indexOf(areaName);
						Log.d("", "Inorbit active id "+activeAreaId+" Area : "+areaName+" ArrayPos "+areaPos);
						actionBar.setSelectedNavigationItem(areaPos);
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}else if(requestCode==6){
				LayoutInflater inflater = context.getLayoutInflater();
				View view=inflater.inflate(R.layout.homegridstatic2, null);
				ImageView login_logOut=(ImageView)view.findViewById(R.id.logout_Grid);
				if(session.isLoggedInCustomer()){
					//login_logOut.setImageResource(R.drawable.logout);
				}
				else{
					//login_logOut.setImageResource(R.drawable.login);
				}
				
			/**
			 * comes back from App Info tab
			 * 	
			 */
			}else if(requestCode==8){
				
				/**
				 * Refresh the action bar to check whether merchant switch has been enable or not
				 * 
				 */
				invalidateActionBar();

			}



		}catch(NullPointerException ex)
		{
			ex.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	
	void invalidateActionBar(){
		this.supportInvalidateOptionsMenu();
		//Toast.makeText(context, "From App Info",0).show();
	}


	void showToast(String text){
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}


	public void showLoginDialog(String message){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle(getResources().getString(R.string.actionBarTitle));

		alertDialogBuilder
		.setMessage(message)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(true)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				Intent intent=new Intent(context,LoginSigbUpCustomerNew.class);
				context.startActivityForResult(intent,2);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog = alertDialogBuilder.create();
		TextView txtMessage = (TextView) alertDialog.findViewById(android.R.id.message);
		TextView title = (TextView) alertDialog.findViewById(android.R.id.title);
		title.setTypeface(InorbitApp.getTypeFace());
		txtMessage.setTypeface(InorbitApp.getTypeFace());
		alertDialog.show();
	}

	
	
	
	/**
	 * Background Animation for on customer dashboard.
	 * 
	 * @param img_1
	 * @param img_2
	 * @param urls
	 */
	void startImageAnimation(final ImageView img_1,
			final ImageView img_2,
			final ArrayList<Integer> urls){



		final Animation fade_in_anim=AnimationUtils.loadAnimation(context, R.anim.fade_in_splash);


		final Handler handler_anim = new Handler();
		runnable_anim = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try{


					img_2.setBackgroundResource(urls.get(imageCount));
					img_2.startAnimation(fade_in_anim);
					fade_in_anim.setFillAfter(true);
					fade_in_anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

							Log.d("=====", "Inorbit Animation start" +imageCount);
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							Log.d("=====", "Inorbit Animation end" +imageCount);

							img_1.setBackgroundResource(urls.get(imageCount));
							imageCount++;
							if(imageCount==urls.size()){
								imageCount=0;
							}
						}
					});

					handler_anim.postDelayed(runnable_anim, 2500);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};
		handler_anim.postDelayed(runnable_anim,1000);
	}

	
	
	
	/**
	 * Will pop up the dialog when user has open the app for the first time
	 * It will ask to select any one mall as defualt mall location.
	 * Once user select any mall location from this, than actionBar will keep that mall as active mall
	 * 
	 */
	protected void showPlaceChooserDialog(){


		Builder builder = new AlertDialog.Builder(context);
		//builder.setTitle("Select Your City");
		View view  = context.getLayoutInflater().inflate(R.layout.dialog_select_city, null);
		ListView lv = (ListView) view.findViewById(R.id.list_select_city);
		TextView txtView = (TextView) view.findViewById(R.id.text_title);
		txtView.setTypeface(InorbitApp.getTypeFace());
		
		if((dropdownItems!=null)&&(dropdownItems.size()!=0)&&(cityNames!=null)&&(cityNames.size()!=0)){
			CityChooserAdapter adap = new CityChooserAdapter(context, dropdownItems,cityNames);
			lv.setAdapter(adap);
			lv.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View v, int pos,
						long arg3) {
					// TODO Auto-generated method stub
					session.showCityChooserDialog(false);
					String activeAreaPlaceId = dbutil.getMallIdByPos(""+(pos+1)); // id == 0 == 1
					dbutil.setActiveMall(activeAreaPlaceId);
					fbUrl = dbutil.getFbUrlByPos(""+(pos+1));
					String areaName = dbutil.getAreaNameByInorbitId(activeAreaPlaceId);
					//showToast(areaName);
					int areaPos 		= dropdownItems.indexOf(areaName);
					InorbitLog.d("Inorbit active id "+activeAreaPlaceId+" Area : "+areaName+" ArrayPos "+areaPos);
					actionBar.setSelectedNavigationItem(areaPos);
					city_chooser_dialog.dismiss();
				}
			});
			builder.setView(view);
			city_chooser_dialog = builder.create();
			city_chooser_dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;


			Handler handler = null;
			handler = new Handler(); 
			handler.postDelayed(new Runnable(){ 
				public void run(){
					city_chooser_dialog.show();
				}
			}, 1000);
		}
		
	}



	/**
	 * 
	 * List adapter of the place chooser dialog
	 *
	 */
	class CityChooserAdapter extends ArrayAdapter<String>{

		Activity context;
		ArrayList<String> places;
		ArrayList<String> city;
		LayoutInflater inflate;

		public CityChooserAdapter(Activity context, ArrayList<String> places,ArrayList<String> city) {
			super(context, R.layout.select_city_layout);
			// TODO Auto-generated constructor stub
			this.context  	= context;
			this.places 	= places;
			this.city		= city;
			inflate			= context.getLayoutInflater();

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return places.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return places.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null){
				ViewHolder1 holder = new ViewHolder1();
				convertView=inflate.inflate(R.layout.select_city_layout,null);
				holder.textView=(TextView)convertView.findViewById(R.id.text_city);	
				holder.textView.setTypeface(InorbitApp.getTypeFace());
				convertView.setTag(holder);

			}
			ViewHolder1 hold = (ViewHolder1) convertView.getTag();
			

			try{
				hold.textView.setText(getItem(position));
				if(getItem(position).equalsIgnoreCase(city.get(position))){

				}else{
					hold.textView.setText(getItem(position)+", "+city.get(position));
				}
			}catch(IndexOutOfBoundsException iob){
				iob.printStackTrace();
			}

			return convertView;
		}





	} 

	static class ViewHolder1{
		TextView textView;
	} 


	/**
	 * 
	 * Grid adapter for customer dashboard.
	 *
	 */
	static class GridViewLayoutAdapter extends BaseAdapter{

		int page;
		Activity context;
		LayoutInflater inflater;

		public GridViewLayoutAdapter(Activity context, int page) {
			this.page = page;
			this.context = context;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			//if(page == 0)
				//return 9;
			//else
				//return 7;
			
			return 9;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View view = null;
			Holder holder;

			try{


				if(convertView == null ){
					holder = new Holder();
					view = inflater.inflate(R.layout.gridviewlayout, null);
					holder.image = (ImageView) view.findViewById(R.id.imageView1);
					holder.image.setVisibility(View.VISIBLE);
					holder.textview = (TextView) view.findViewById(R.id.textView1);
					holder.textview.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
					
					view.setTag(holder);
				}
				else{
					view = convertView;
					holder = (Holder) view.getTag();
				}

				try{
					if(page==0){
						
						int id = gridItemIcons.get(position);
						String text = gridItems.get(position);

						holder.tag = gridItems.get(position);
						InorbitLog.d("GridTag "+position+" -- "+gridItems.get(position));
						holder.image.setImageResource(id);
						holder.textview.setText(text.toUpperCase());
						
						

					}else{
						int id = gridItemIcons.get(position+9);
						String text = gridItems.get(position+9);

						holder.tag = gridItems.get(position+9);
						InorbitLog.d("GridTag "+(position+9)+" -- "+gridItems.get(position+9));
						holder.image.setImageResource(id);
						holder.textview.setText(text.toUpperCase());

						if(text.equalsIgnoreCase("")){
							holder.image.setVisibility(View.INVISIBLE);
							holder.textview.setVisibility(View.INVISIBLE);
						}else{
							holder.image.setVisibility(View.VISIBLE);
							holder.image.setImageResource(id);
							holder.textview.setText(text.toUpperCase());
							holder.textview.setVisibility(View.VISIBLE);
						}

						holder.textview.setTextColor(context.getResources().getColor(R.color.inorbit_blue));
					}



					InorbitLog.d("Title "+gridItems.get(position+9).toUpperCase());
					holder.textview.setTextColor(context.getResources().getColor(R.color.inorbit_blue));
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
			return view;
		}
	}

	static class Holder{

		ImageView image;
		TextView textview;
		String tag;
	}

	
	/**
	 * 
	 * Update active mall location of the customer. 
	 * This method gets if the customer is logged in and changes the mall location from action bar
	 *
	 */
	void updateActiveMall(){
		try{
			JSONObject jObj = new JSONObject();
			jObj.put("user_id", USER_ID);
			jObj.put("auth_id", AUTH_ID);
			jObj.put("active_mall", ACTIVE_MALL_ID);

			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("X-HTTP-Method-Override", "PUT");
			headers.put(API_HEADER, API_VALUE);

			MyClass myClass = new MyClass(context);
			myClass.postRequest(RequestTags.Tag_UpdateActiveMall, headers, jObj);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	
	
	/*void setAreaTag(String area)
	{
		//Log.i("URBAN", "URBAN SET TAG");
		boolean isMisMatch=false;

		//Adding Tags

		Set<String> tags = new HashSet<String>();
		tags=PushManager.shared().getTags();

		ArrayList<String>tempIdList=new ArrayList<String>();

		String temp="";
		String replace="";
		//Getting All Tags
		Iterator<String> iterator=tags.iterator();

		//Traversing Tags to find out if there are any tags existing with own_id

		while(iterator.hasNext()){
			temp=iterator.next();
			Log.i("TAG", "Urban TAG PRINT "+temp);
			//If tag contains own_
			if(temp.startsWith("area_"))
			{
				//Replace area_ with "" and compare it with ID
				replace=temp.replaceAll("area_", "");
				tempIdList.add(replace);
				Log.i("TAG", "Urban TAG ID "+replace);

				if(!area.equalsIgnoreCase(replace))
				{
					isMisMatch=true;
					break;
				}
			}
		}

		if(isMisMatch==false && tempIdList.size()==0)
		{
			isMisMatch=true;
		}


		if(isMisMatch==true)
		{

			//Clear area_ from Tag List
			for(int i=0;i<tempIdList.size();i++)
			{
				tags.remove("area_"+tempIdList.get(i));
			}


			tags.add("area_"+area);

			Log.i("TAG", "Urban TAG AFTER "+tags.toString());

			PushManager.shared().setTags(tags);
		}
	}*/
	
	
	/**
	 * 
	 * Calls the next activity, based on the tag/title of the option which user has clicked on customer dashboard
	 *
	 */
	static void startClassFromTag(View v){

		try{
			Holder holder = (Holder) v.getTag();
			//Toast.makeText(context, holder.tag, 0).show();
			String strTag = holder.tag;
			InorbitLog.d("Tag "+strTag);
			Intent intent=null;
			if(strTag.equalsIgnoreCase("Directory")){
				if(mBDebug){
					registerEvent(context.getResources().getString(R.string.tab_Directory));
					intent=new Intent(context,StoreListing.class);
					intent.putExtra("callFromSearch", false);
					Tables.setCurrentCategory(Tables.CATEGORY_All);
					context.startActivityForResult(intent, 5);
					//context.finish();  
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}else{
					
				}
			}else if(strTag.equalsIgnoreCase("Offers")){
				registerEvent(context.getResources().getString(R.string.tab_Offers));
				intent=new Intent(context,OffersNew.class);
				Tables.setCurrentCategory(Tables.CATEGORY_OFFER);
				context.startActivityForResult(intent, 5);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				
			}else if(strTag.equalsIgnoreCase("Search")){
				registerEvent(context.getResources().getString(R.string.tab_Search));
				intent=new Intent(context,SearchActivity.class);
				context.startActivity(intent);
				context.finish();
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}else if(strTag.equalsIgnoreCase("Shop")){ 

				if(mBDebug){
					registerEvent(context.getResources().getString(R.string.tab_Shop));
					intent=new Intent(context,StoreListing.class);
					intent.putExtra("callFromSearch", false);
					Tables.setCurrentCategory(Tables.CATEGORY_SHOP);
					context.startActivityForResult(intent, 5);
					//context.finish();
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}else{
					registerEvent(context.getResources().getString(R.string.tab_Shop));
					intent=new Intent(context,CategorySearch.class);
					intent.putExtra("category_id", "1");
					intent.putExtra("category_name", "Shop");
					intent.putExtra("offer", "");
					intent.putExtra("isCategory", true);
					context.startActivityForResult(intent, 5);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				

			}else if(strTag.equalsIgnoreCase("Dine")){

				if(mBDebug){
					registerEvent(context.getResources().getString(R.string.tab_Dine));
					intent=new Intent(context,StoreListing.class);
					intent.putExtra("callFromSearch", false);
					Tables.setCurrentCategory(Tables.CATEGORY_DINE);
					context.startActivityForResult(intent, 5);
					//context.finish();
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}else{
					registerEvent(context.getResources().getString(R.string.tab_Dine));
					intent=new Intent(context,CategorySearch.class);
					intent.putExtra("category_id", "2");
					intent.putExtra("category_name", "Dine");
					intent.putExtra("offer", "");
					intent.putExtra("isCategory", true);
					context.startActivityForResult(intent, 5);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
				
			}else if(strTag.equalsIgnoreCase("Entertainment")){

				if(mBDebug){
					registerEvent(context.getResources().getString(R.string.tab_Entertainment));
					intent=new Intent(context,StoreListing.class);
					intent.putExtra("callFromSearch", false);
					Tables.setCurrentCategory(Tables.CATEGORY_ENTERTAINMENT);
					context.startActivityForResult(intent, 5);
					//context.finish();
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}else{
					registerEvent(context.getResources().getString(R.string.tab_Entertainment));
					intent=new Intent(context,CategorySearch.class);
					intent.putExtra("category_id", "3");
					intent.putExtra("offer", "");
					intent.putExtra("category_name", "Entertainment");
					intent.putExtra("isCategory", true);
					context.startActivityForResult(intent, 5);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				
			

			}else if(strTag.equalsIgnoreCase("Pamper Zone")){

				if(mBDebug){
					registerEvent(context.getResources().getString(R.string.tab_PamperZone));
					intent=new Intent(context,StoreListing.class);
					intent.putExtra("callFromSearch", false);
					Tables.setCurrentCategory(Tables.CATEGORY_PAMPERZONE);
					context.startActivityForResult(intent, 5);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}else{
					registerEvent(context.getResources().getString(R.string.tab_PamperZone));
					intent=new Intent(context,CategorySearch.class);
					intent.putExtra("category_id", "4");
					intent.putExtra("offer", "");
					intent.putExtra("category_name", "Pamper Zone");
					intent.putExtra("isCategory", true);
					context.startActivityForResult(intent, 5);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				

			}else if(strTag.equalsIgnoreCase("Mall Info")){

				registerEvent(context.getResources().getString(R.string.tab_MallInfo));
				//FireEvents.TagScreen(TAB+"MallInfo");
				context.startActivityForResult((new Intent(context, MallInfo.class)),5);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


			}else if(strTag.equalsIgnoreCase("Mall Events")){

				registerEvent(context.getResources().getString(R.string.tab_MallEvents));
				//FireEvents.TagScreen(TAB+"NewsAndEvents");
				intent=new Intent(context,NewsActivity.class);
				context.startActivityForResult(intent,5);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}else if(strTag.equalsIgnoreCase("Park Assist")){

				context.startActivityForResult((new Intent(context, ParkAssist.class)),5);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}else if(strTag.equalsIgnoreCase("Favourites")){
				if(session.isLoggedInCustomer())
				{
					createInhouseAnalyticsEvent("0", "3", dbutil.getActiveMallId());
					registerEvent(context.getResources().getString(R.string.tab_Favourites));
					//FireEvents.TagScreen(TAB+"Favourites");
					//intent=new Intent(context,MerchantFavouriteStores.class);
					intent=new Intent(context,UserFavouritesStores.class);
					context.startActivityForResult(intent,5);
					
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
				else
				{


					String message = context.getResources().getString(R.string.login_to_see_favourites);
					//showLoginDialog(context.getResources().getString(R.string.login_to_see_offers));
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
					alertDialogBuilder.setTitle(context.getResources().getString(R.string.actionBarTitle));
					alertDialogBuilder
					.setMessage(message)
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(true)
					.setPositiveButton("Login",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							dialog.dismiss();
							registerAction(context.getResources().getString(R.string.favourite_AlertAction),"Login");
							Intent intent=new Intent(context,LoginSigbUpCustomerNew .class);
							context.startActivityForResult(intent,2);
							context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

						}
					})
					.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							registerAction(context.getResources().getString(R.string.favourite_AlertAction),"Cancel");
							dialog.dismiss();
						}
					});

					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
				}

			}else if(strTag.equalsIgnoreCase("Shake & Win")){
				
				if(session.isLoggedInCustomer()){
					createInhouseAnalyticsEvent("0", "1", dbutil.getActiveMallId());
					registerEvent(context.getResources().getString(R.string.tab_Shake2win));
					//FireEvents.TagScreen(TAB+"ShakeAndWin");
					intent = new Intent(context, ForMe.class);
					intent.putExtra("MALL_NAME", ACTIVE_MALL_NAME);
					context.startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}else{
					String message = context.getResources().getString(R.string.login_to_see_offers);
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
					alertDialogBuilder.setTitle(context.getResources().getString(R.string.actionBarTitle));
					alertDialogBuilder
					.setMessage(message)
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(true)
					.setPositiveButton("Login",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							dialog.dismiss();
							registerAction(context.getResources().getString(R.string.shake2Win_AlertAction),"Login");
							Intent intent=new Intent(context,LoginSigbUpCustomerNew.class);
							context.startActivityForResult(intent,2);
							context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

						}
					})
					.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							registerAction(context.getResources().getString(R.string.shake2Win_AlertAction),"Cancel");
							dialog.dismiss();
						}
					});
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
				}

			} else if(strTag.equalsIgnoreCase("Facebook")) {
				intent = new Intent(context, Twitter.class);
				intent.putExtra("isFacebook", true);
				intent.putExtra("url", fbUrl);
				context.startActivityForResult(intent,5);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			} else if(strTag.equalsIgnoreCase("Twitter")) {
				intent = new Intent(context, Twitter.class);
				intent.putExtra("isFacebook", false);
				intent.putExtra("url", "");
				context.startActivityForResult(intent,5);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			} else if(strTag.equalsIgnoreCase("Sign In") || strTag.equalsIgnoreCase("Sign out")) {
				registerEvent(context.getResources().getString(R.string.tab_SignIn));
				intent=new Intent(context,LoginSigbUpCustomerNew.class);
				context.startActivityForResult(intent,2);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			} else if(strTag.equalsIgnoreCase("App Info")) {

				registerEvent(context.getResources().getString(R.string.tab_AppInfo));
				intent = new Intent(context, AboutUs.class);
				context.startActivityForResult(intent, 8);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			} else if(strTag.equalsIgnoreCase("My Profile")) {
				if(session.isLoggedInCustomer()) {
					registerEvent(context.getResources().getString(R.string.tab_MyProfile));
					intent=new Intent(context,CustomerProfile.class);
					context.startActivityForResult(intent, 2);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				} else {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
					alertDialogBuilder.setTitle(context.getResources().getString(R.string.actionBarTitle));
					alertDialogBuilder
					.setMessage("Please login to update profile")
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(true)
					.setPositiveButton("Login",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							dialog.dismiss();
							//FireEvents.TagScreen(TAB+"Login");
							registerEvent(context.getResources().getString(R.string.tab_MyProfile));
							Intent intent=new Intent(context,LoginSigbUpCustomerNew.class);
							context.startActivityForResult(intent,2);
							context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

						}
					})
					.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.dismiss();
						}
					});
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();
				}
			} else if(strTag.equalsIgnoreCase("Social Connect")) {
				registerEvent(context.getResources().getString(R.string.tab_SocialConnect));
				intent=new Intent(context,SocialConnect.class);
				context.startActivityForResult(intent,5);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			} else if(strTag.equalsIgnoreCase("Share this app")) {
				createInhouseAnalyticsEvent("0", "7", dbutil.getActiveMallId());
				registerEvent(context.getResources().getString(R.string.tab_shareThisApp));
				ShareThisApp shareApp = new ShareThisApp(context);
				shareApp.showDialog(context.getResources().getString(R.string.share_app_text));
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static void createInhouseAnalyticsEvent(String flag, String activityId, String mallId) {
		Uri uri = new Uri.Builder().scheme("content").authority(context.getResources().getString(R.string.authority)).
				appendPath("/insert").build();
        ContentValues cv = new ContentValues(6);
		cv.put("UDM_ID", session.getUdmIDForCustomer());
		cv.put("DATE", new Date().toString());
		cv.put("FLAG", flag);
		cv.put("ACTIVITY_ID", activityId);
		cv.put("MALL_ID", mallId);
		cv.put("SYNC_STATUS", -1);
		context.getContentResolver().insert(uri, cv);
	}
	
	/**
	 * 
	 * Gets called when database don't have any info about place parent stores or if call for this api was done 2 days before from present day.
	 * There is no broadcast receiver for this api as received information gets stored in database directly.
	 *
	 */
	void getParentStoreInfo(){
		try{
			long count = dbutil.getRowCountOfPlarentTable();
			InorbitLog.d("Row Count "+count);

			if(count==0){

				ArrayList<String> parentIds = new ArrayList<String>();
				
				/**
				 * Get the mall ids from database
				 * 
				 */
				parentIds = dbutil.getPlaceParentIdsArr();

				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put(API_HEADER, API_VALUE);
				headers.put("user_id", USER_ID);

				String placeParents = parentIds.toString();
				String csv = placeParents.substring(1, placeParents.length() - 1).replace(", ", ",");
				InorbitLog.d("Place Parents "+csv);

				MyClass myClass = new MyClass(context);
				//for(int i =0;i<parentIds.size();i++){
				List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("place_id", csv));
				
				
				/**
				 * Network call to get the infomration of parent stores
				 * 
				 */
				myClass.getStoreRequest(RequestTags.TagGetParticularStoreDetails_d, nameValuePairs, headers);


				//}
				
				/**
				 * 
				 * Set present date and refres this after every 2 days
				 */
				session.setMallsApiCallDate(CustomMethods.getPresentTime());

			}else{
				//long lastDate = session.getMallsApiCallLastDate();
				long lastDate = session.getParentStoreCallLastDate();
				int  days = Integer.parseInt(getResources().getString(R.string.refresh_days));
				if(CustomMethods.getDaysDiff(lastDate)>=days){

					ArrayList<String> parentIds = new ArrayList<String>();
					parentIds = dbutil.getPlaceParentIdsArr();

					String placeParents = parentIds.toString();
					String csv = placeParents.substring(1, placeParents.length() - 1).replace(", ", ",");
					InorbitLog.d("Place Parents "+csv);
					HashMap<String, String> headers = new HashMap<String, String>();
					headers.put(API_HEADER, API_VALUE);
					headers.put("user_id", USER_ID);


					MyClass myClass = new MyClass(context);
					//for(int i =0;i<parentIds.size();i++){
					List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
					nameValuePairs.add(new BasicNameValuePair("place_id", csv));
					myClass.getStoreRequest(RequestTags.TagGetParticularStoreDetails_d, nameValuePairs, headers);		

					//}
					session.setParentStoreApiCallDate(CustomMethods.getPresentTime());
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	/**
	 * Used for analytics, to track the user actions
	 * 
	 * @param eventName
	 */
	static void registerEvent(String eventName){
		try{
			param = new HashMap<String, String>();
			param.put("MallName", ACTIVE_MALL_NAME);
			if(ACTIVE_MALL_NAME!=null && !ACTIVE_MALL_NAME.equalsIgnoreCase("") ){
				EventTracker.logEvent(eventName, true);
				EventTracker.logEvent(eventName, param);
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * Use to track the screens like Directory,Offers, Dine..
	 * 
	 * @param eventName
	 * @param action
	 */
	static void registerAction(String eventName,String action){
		try{
			boolean param1 = false;
			param = new HashMap<String, String>();
			if(ACTIVE_MALL_NAME!=null&&!ACTIVE_MALL_NAME.equalsIgnoreCase("")){
				param.put("MallName", ACTIVE_MALL_NAME);
				param1 = true;
			}
			if(action!=null&&!action.equalsIgnoreCase("")){
				param.put("Action", action);
				param1 = true;
			}
			
			if(param1){
				EventTracker.logEvent(eventName, param);
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
			
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EventTracker.endLocalyticsSession(getApplicationContext());
		//startOfferAnim(false, mArrOFFERS);

	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			//IntentFilter offersFilter = new IntentFilter(RequestTags.TagViewBroadcast_mall);
			//context.registerReceiver(offersBroadCast = new StoresOffer(), offersFilter);
			Bundle b = new Bundle();
			b.putString("KEy", "Value");
			/*b.putBoolean(
	                ContentResolver.SYNC_EXTRAS_MANUAL, true);*/
	        b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
			ContentResolver.requestSync(mAccount, AUTHORITY, b);
			
			if(getIntent().getExtras() != null && getIntent().getExtras().containsKey("KEY")) {
				try {
					EventTracker.logEvent(getResources().getString(R.string.gcm_notification_app_open), true);
				} catch (Exception e) {}
	            gcmNotificationClickedEvent();
	        }
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		//startOfferAnim(true, mArrOFFERS);
		

	}


	/* on start */
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		super.onStop();
		EventTracker.endFlurrySession(getApplicationContext());
		try{
			//startOfferAnim(false, mArrOFFERS);
			//if(offersBroadCast!=null){
			//	context.unregisterReceiver(offersBroadCast);
			//}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}


	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(mArrOFFERS!=null){
			//startOfferAnim(false, mArrOFFERS);
		}
		
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}



}


package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.ParticularStoreInfo;
import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;
import com.phonethics.model.TipsManagerResponseModel;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;

import com.actionbarsherlock.view.Menu;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class MerchantHome extends SherlockFragmentActivity{
	
	//GridView 				grid_home;
	private static ArrayList<String>		gridItems;
	private static ArrayList<Integer>		gridItemIcons;
	
	static Activity 				context;
	ActionBar 				actionBar;
	
	private static SessionManager 			session;
	
	SharedPreferences 		prefs;
	private static Editor 					editor;
	//ViewPager 				viewCustomerPager;

	private static DBUtil 					dbUtil;
	private static String 					USER_ID;
	private static String 					AUTH_ID;
	SessionManager	mSessionManager;

	public static final String 				KEY_USER_ID="user_id";
	//Auth Id
	public static final String 				KEY_AUTH_ID="auth_id";
	TextView				txt_merchantType;

	static String KEY_NEW_POST = "NEW OFFER";
	static String KEY_VIEW_POST = "VIEW OFFERS";
	static String KEY_ADD_STORE = "ADD STORE";
	static String KEY_EDIT_STORE = "EDIT STORE";
	static String KEY_MANAGE_GALLERY = "MANAGE GALLERY";
	static String KEY_LOGOUT = "LOGOUT";
	static String KEY_LOGIN = "LOGIN";
	static String KEY_SHAKE_WIN	= "SHAKE & WIN";
	static String KEY_PENDING_APPROVALS	= "OFFER APPROVALS";
	static String KEY_ADD_RESTAURANTMENU =	"ADD MENU";
	static String KEY_MANAGE_MENU = "MANAGE MENU";
	static String KEY_INBOX = "INBOX";
	static String KEY_MANAGE_TIPS = "MANAGE TIPS";

	private static ArrayList<GridAdapter> gridAdapterList = new ArrayList<MerchantHome.GridAdapter>();
	private ValidateMerchantBroadCast validateReceiver;
	TextView text_poweredby;
	private String msMallManagerArea = "";
	private static int miLabelCount = 0;
	private PostReceiver checkCountReceiverReceiver;
	
	static int mTotalMsg = 0;
	TotalCount mTotalCountReciver;
	static int unapprovedMenuCount = 0;
	static int tips_count = 0;

	UnapprovedMenuCountReceiver unapprovedMenuCountReceiver;
	
	//For request code
	public static final int REQUEST_CODE = 20;
	Runnable runnable;
	Handler handler;
	
	private static ViewPager viewPager;
	private static PageAdapter pageAdapter;
	TextView txtIndicator_1,txtIndicator_2;
	private static GridView gridHome = null;
	private static final int TIPS_REQUEST_CODE = 30; 
	StoreMgrTips storeMgrTips, mallMgrTips;

	private ArrayList<String> storeNames;
	private ArrayList<String> storeLogoUrls;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_merchant_home);

		mSessionManager = new SessionManager(this);
		
		/**
		 * 
		 * Initilize the layout and class variables
		 * 
		 */
		actionBar=getSupportActionBar();
		actionBar.setTitle("Inorbit");
		actionBar.show();
		
		context=this;
		dbUtil = new DBUtil(context);
		//grid_home = (GridView) findViewById(R.id.merchant_grid);
		
		viewPager = (ViewPager) findViewById(R.id.viewPager);
		
		txt_merchantType = (TextView) findViewById(R.id.text_merchantType);
		text_poweredby = (TextView) findViewById(R.id.text_poweredby);
		txt_merchantType.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD_ITALIC);
		
		prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
		editor=prefs.edit();

		session			= new SessionManager(context);
		gridItems		= new ArrayList<String>();
		gridItemIcons	= new ArrayList<Integer>();

		//if(ConfigFile.isManagerLoogedIn() || session.isMallManagerLoggedin()){

		/**
		 * Dashboard for the mall manager
		 */
		//loadAllStoreForThisMerchant();
		/*showToast(dbUtil.isManager()+" "+!dbUtil.isMultipuleMallsLoggedIn());
		showToast(dbUtil.getSubCategoryStores("Dine").size()+" ");
		showToast(dbUtil.mallStoresCount() +" ");*/
		
		loadAllStoreForThisMerchant();
		
		if(dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()){
			msMallManagerArea = dbUtil.getMallManagersArea();
			
			txt_merchantType.setText("Mall Manager Dashboard");
			gridItems.add(KEY_NEW_POST);
			gridItems.add(KEY_VIEW_POST);
			gridItems.add(KEY_ADD_STORE);
			gridItems.add(KEY_EDIT_STORE);
			gridItems.add(KEY_MANAGE_TIPS);
			gridItems.add(KEY_SHAKE_WIN);
			gridItems.add(KEY_PENDING_APPROVALS);
			gridItems.add(KEY_INBOX);			
			gridItems.add(KEY_MANAGE_MENU);
			//for Inbox item
			
			gridItems.add(KEY_ADD_RESTAURANTMENU);
			gridItems.add(KEY_MANAGE_GALLERY);
			
			gridItems.add(KEY_LOGOUT);
			
			gridItemIcons.add(R.drawable.new_post_new);
			gridItemIcons.add(R.drawable.view_post_new);
			gridItemIcons.add(R.drawable.add_store_new);
			gridItemIcons.add(R.drawable.edit_store_new);
			gridItemIcons.add(R.drawable.tips);
			gridItemIcons.add(R.drawable.shake_win_new);
			gridItemIcons.add(R.drawable.post_approvals);
			gridItemIcons.add(R.drawable.inbox_grid);
			gridItemIcons.add(R.drawable.managedmenu);
			//for Inbox item
			
			gridItemIcons.add(R.drawable.menu);
			gridItemIcons.add(R.drawable.manage_gallery_new);
			
			gridItemIcons.add(R.drawable.logout_new);
			
			actionBar.setTitle(msMallManagerArea);
			actionBar.show();
		} else {
			/**
			 * Dashboard for the store owner
			 */

			txt_merchantType.setText("Shop Owner Dashboard");
			
			gridItems.add(KEY_NEW_POST);
			gridItems.add(KEY_VIEW_POST);
			gridItems.add(KEY_EDIT_STORE);
			gridItems.add(KEY_MANAGE_GALLERY);
			long subCatStoresCount = dbUtil.getSubCategoryStoresCount("Dine");
			if (subCatStoresCount > 0) {
				gridItems.add(KEY_ADD_RESTAURANTMENU);
				gridItems.add(KEY_MANAGE_MENU);
			}
			//for Inbox item
			gridItems.add(KEY_INBOX);
			
			gridItems.add(KEY_MANAGE_TIPS);

			gridItems.add(KEY_LOGOUT);

			gridItemIcons.add(R.drawable.new_post_new);
			gridItemIcons.add(R.drawable.view_post_new);
			gridItemIcons.add(R.drawable.edit_store_new);
			gridItemIcons.add(R.drawable.manage_gallery_new);
			if (subCatStoresCount > 0) {
				gridItemIcons.add(R.drawable.menu);
				gridItemIcons.add(R.drawable.managedmenu);
			}
			//for Inbox icon
			gridItemIcons.add(R.drawable.inbox_grid);
			gridItemIcons.add(R.drawable.tips);

			gridItemIcons.add(R.drawable.logout_new);
		}
		
		//to show total count for Inbox
		getTotalCount();
		
		callRegularlyCountApi();
		
		/**
		 * 
		 * Get the details of the logged in merchant
		 * 
		 * 
		 */
		HashMap<String,String>user_details=session.getUserDetails();
		USER_ID=user_details.get(KEY_USER_ID).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID).toString();
		
		//if (gridItemIcons.size()<10) {
			/*adapter=new GridAdapter(context, R.drawable.ic_launcher,  R.drawable.ic_launcher, gridItems,gridItemIcons);
			grid_home.setAdapter(adapter);*/
		pageAdapter = new PageAdapter(getSupportFragmentManager());
		viewPager.setAdapter(pageAdapter);
		
		Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/AirstreamNF.ttf");
		txtIndicator_1 = (TextView) findViewById(R.id.text_indicator_1);
		txtIndicator_2 = (TextView) findViewById(R.id.text_indicator_2);
		txtIndicator_1.setTextColor(getResources().getColor(R.color.indicator_active));
		txtIndicator_2.setTextColor(getResources().getColor(R.color.indicator_inactive));
		txtIndicator_2.setTextSize(80);
		txtIndicator_1.setTextSize(80);
		txtIndicator_1.setText(".");
		txtIndicator_2.setText(".");
		txtIndicator_1.setTypeface(tf);
		txtIndicator_2.setTypeface(tf);
		if (gridItems.size()<=9) {
			txtIndicator_2.setVisibility(View.GONE);
		}
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				if(position==0){
					txtIndicator_1.setTextColor(getResources().getColor(R.color.indicator_active));
					txtIndicator_2.setTextColor(getResources().getColor(R.color.indicator_inactive));
				}else{
					txtIndicator_1.setTextColor(getResources().getColor(R.color.indicator_inactive));
					txtIndicator_2.setTextColor(getResources().getColor(R.color.indicator_active));
				}
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
		/*} else {
			pages=new ArrayList<String>();
			pages.add("Page1");
			pages.add("Page2");
			
			PageAdapter adapter=new PageAdapter(getSupportFragmentManager(),getFragments());
			viewCustomerPager.setAdapter(adapter);
		}*/
		
		/*grid_home.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long arg3) {

				//startClass(position+1);
				startClassFromTag(view);
			}
		});*/

		text_poweredby.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, Twitter.class);
				intent.putExtra("ActivityTitle", "Phonethics");
				intent.putExtra("url", "http://phonethics.in/");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.fade_out);
			}
		});

		/**
		 * 
		 * Check whether this merchant has been loggedin in any other device 
		 * 
		 */
		validateMerchant();



		/**
		 * 
		 * Get the unapproved post count to indicate the batch for mall manager  
		 * 
		 */
		if(Tables.mbNeedToCheckCount && !dbUtil.isMultipuleMallsLoggedIn()){
			getAllUnApprovedPost();	
		}
		
		if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) {
			getUnapprovedMenuCount();
		}
		
		if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) {
			getTipsForMallManager();
		} else {
			final ArrayList<String> store_ids = dbUtil.getAllStoreIDs();
			storeNames = dbUtil.getAllStoresNames();
			storeLogoUrls = dbUtil.getAllStorePhotoUrl();
			getTipsForStoreManager(store_ids);
		}
	}

	private static long  getTotalShops(){
		return dbUtil.getStoresRowCount();
	}

	private static ParticularStoreInfo getStoreInfo(){	
		return dbUtil.getStoreInfo();
	}
	
	void loadAllStoreForThisMerchant(){

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		headers.put("user_id", getUserId());
		headers.put("auth_id", getAuthId()); 

		InorbitLog.d("Merchnat user id "+getUserId());

		MyClass myClass = new MyClass(context);
		myClass.getStoreRequest(RequestTags.TagPlaceChooser, null, headers);

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuItem extra=menu.add("Extra Settings").setIcon(R.drawable.switch_view);
		extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	protected static void startClassFromTag(View v) {
		try{
			ViewHolder holder = (ViewHolder) v.getTag();
			String tag = holder.Tag;
			if(tag.equalsIgnoreCase(KEY_ADD_STORE)){
				try{
					EventTracker.logEvent(context.getResources().getString(R.string.tab_AddStore), true);
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					context.startActivity(intent);
					//finish();
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}catch(Exception ex){
					ex.printStackTrace();
				}
			}else if(tag.equalsIgnoreCase(KEY_VIEW_POST)){
				try{

					//registerTabEvent(context.getResources().getString(R.string.tab_ViewPost));
					
					EventTracker.logEvent(context.getResources().getString(R.string.tab_ViewPost), true);
					
					if (getTotalShops()==1) {
						ParticularStoreInfo particularStoreInfo = getStoreInfo();
						//Intent intent=new Intent(context, ViewPost.class);
						Intent intent=new Intent(context, ViewPostNew.class);
						intent.putExtra("storeName", particularStoreInfo.getName());
						intent.putExtra("STORE_ID", particularStoreInfo.getId());
						context.startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						//finish();
					} else {
						Intent intent=new Intent(context,PlaceChooser.class);
						intent.putExtra("choiceMode", 1);
						intent.putExtra("viewPost",1);
						intent.putExtra("AddStore",0);
						intent.putExtra("manageGallery",0);
						context.startActivity(intent);
						//finish();
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}else if(tag.equalsIgnoreCase(KEY_NEW_POST)){
				try{

					EventTracker.logEvent(context.getResources().getString(R.string.tab_NewPost), true);
					if(getTotalShops()==1) {
						ArrayList<String> storeIdArr = new ArrayList<String>();
						storeIdArr.add(getStoreInfo().getId());
						Intent intent=new Intent(context, CreatBroadCastNew.class);
						intent.putStringArrayListExtra("SELECTED_STORE_ID",storeIdArr);
						context.startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						//finish();
					} else {
						Intent intent=new Intent(context,PlaceChooser.class);
						intent.putExtra("choiceMode", 2);
						intent.putExtra("viewPost", 0);
						intent.putExtra("AddStore",0);
						intent.putExtra("manageGallery",0);
						context.startActivity(intent);
						//finish();
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

			}else if(tag.equalsIgnoreCase(KEY_EDIT_STORE)){
				try{

					EventTracker.logEvent(context.getResources().getString(R.string.tab_EditStore), true);
					if(getTotalShops()==1){
						//MyToast.showToast(context, "Total One Store");
						ParticularStoreInfo particularStoreInfo = getStoreInfo();
						Intent intent=new Intent(context, EditStore.class);
						intent.putExtra("isSplitLogin",true);
						intent.putExtra("USER_ID", USER_ID);
						intent.putExtra("AUTH_ID", AUTH_ID);
						intent.putExtra("STORE_NAME", particularStoreInfo.getName());
						intent.putExtra("STORE_LOGO", particularStoreInfo.getImage_url());
						intent.putExtra("STORE_ID", particularStoreInfo.getId());
						intent.putExtra("PLACE_PARENT", particularStoreInfo.getPlace_parent());
						context.startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						//finish();

					}else{
						Intent intent=new Intent(context,PlaceChooser.class);
						intent.putExtra("choiceMode", 1);
						intent.putExtra("viewPost",2);
						intent.putExtra("AddStore",0);
						intent.putExtra("manageGallery",0);
						context.startActivity(intent);
						//finish();
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}else if(tag.equalsIgnoreCase(KEY_LOGOUT)){
				try{

					EventTracker.logEvent(context.getResources().getString(R.string.tab_mLogOut), true);
					editor.clear();
					session.logoutUser();

					session.setMallManagerLoggedin(false);
					dbUtil.deletePlacesTable();
					
					mTotalMsg = 0;
					unapprovedMenuCount = 0;
					
					tips_count = 0;
					
					pageAdapter = null;
					gridAdapterList.clear();
					
					SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
					Editor editor=prefs.edit();
					editor.putBoolean("isPlaceRefreshRequired", true);
					editor.commit();
					
					Intent intent=new Intent(context,MerchantLogin.class);
					context.startActivity(intent);
					context.finish();

				}catch(Exception ex){
					ex.printStackTrace();
				}
			} else if(tag.equalsIgnoreCase(KEY_LOGIN)) {

				try{
					Intent intent=new Intent(context,MerchantLogin.class);
					context.startActivity(intent);
					context.finish();
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}else if(tag.equalsIgnoreCase(KEY_MANAGE_GALLERY)){
				try{
					EventTracker.logEvent(context.getResources().getString(R.string.tab_ManageGallery), true);
					if(getTotalShops()==1) {
						ParticularStoreInfo particularStoreInfo = getStoreInfo();
						Intent intent=new Intent(context,EditStore.class);
						intent.putExtra("manageGallery",1);
						intent.putExtra("STORE_NAME", particularStoreInfo.getName());
						intent.putExtra("STORE_LOGO", particularStoreInfo.getImage_url());
						intent.putExtra("STORE_ID", particularStoreInfo.getId());
						intent.putExtra("PLACE_PARENT", particularStoreInfo.getPlace_parent());
						context.startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						//finish();
					} else {
						Intent intent=new Intent(context,PlaceChooser.class);
						intent.putExtra("choiceMode", 1);
						intent.putExtra("viewPost",2);
						intent.putExtra("AddStore",0);
						intent.putExtra("manageGallery",1);
						context.startActivity(intent);
						//finish();
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}else if(tag.equalsIgnoreCase(KEY_SHAKE_WIN)){
				try{
					EventTracker.logEvent(context.getResources().getString(R.string.tab_mShakeAndWin), true);
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 1);
					intent.putExtra("report",0);
					intent.putExtra("viewPost",-1);
					intent.putExtra("AddStore",-1);
					intent.putExtra("manageGallery",-1);
					intent.putExtra("sure_shop",1);
					context.startActivity(intent);
					//finish();
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}catch(Exception ex){
					ex.printStackTrace();
				}
			}else if(tag.equalsIgnoreCase(KEY_PENDING_APPROVALS)){
				try{
					Intent intent=new Intent(context,PendingPost.class);
					context.startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}catch(Exception ex){
					ex.printStackTrace();
				}

			} else if (tag.equalsIgnoreCase(KEY_ADD_RESTAURANTMENU)) {

				//call ist of stores having dine category
				try {
					EventTracker.logEvent(context.getResources().getString(R.string.tab_AddMenu), true);
				} catch (Exception e) {
					
				}
				long subCatStoresCount = dbUtil.getSubCategoryStoresCount("Dine");
				if (subCatStoresCount == 0) {
					showToast("No Dine Stores available");
				} else if(subCatStoresCount == 1) {
					ArrayList<ParticularStoreInfo> allStores = dbUtil.getSubCategoryStores("Dine");
					Intent intent=new Intent(context, AddRestaurantMenu.class);
					intent.putExtra("STORE_NAME", allStores.get(0).getName());
					intent.putExtra("STORE_LOGO", allStores.get(0).getImage_url());
					intent.putExtra("STORE_ID", allStores.get(0).getId());
					intent.putExtra("PLACE_PARENT", allStores.get(0).getPlace_parent());
					context.startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					//finish();
				} else {
					Intent intent=new Intent(context,RestaurantSelectorActivity.class);
					ArrayList<ParticularStoreInfo> allStores = dbUtil.getSubCategoryStores("Dine");
					intent.putParcelableArrayListExtra("ALL_RESTAURANTS", allStores);
					intent.putExtra("toApprovedPage", false);
					context.startActivity(intent);
					//finish();
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			} else if (tag.equalsIgnoreCase(KEY_MANAGE_MENU)) {
				try {
					EventTracker.logEvent(context.getResources().getString(R.string.tab_mManageMenu), true);
				} catch (Exception e) {
					
				}
				if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) {
					Intent intent = new Intent(context, RestaurantMenuApprovalPage.class);
					intent.putExtra("PLACE_PARENT", dbUtil.getPlaceIdByPlaceParent("0"));
					context.startActivityForResult(intent, 10);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				} else {
					long subCatStoresCount = dbUtil.getSubCategoryStoresCount("Dine");
					if (subCatStoresCount == 0) {
						showToast("No Dine Stores available");
					} else if(subCatStoresCount == 1) {
						ArrayList<ParticularStoreInfo> allStores = dbUtil.getSubCategoryStores("Dine");
						Intent intent=new Intent(context, RestaurantMenuApprovalPage.class);
						intent.putExtra("STORE_NAME", allStores.get(0).getName());
						intent.putExtra("STORE_LOGO", allStores.get(0).getImage_url());
						intent.putExtra("STORE_ID", allStores.get(0).getId());
						intent.putExtra("PLACE_PARENT", allStores.get(0).getPlace_parent());
						context.startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						//finish();
					} else {
						Intent intent=new Intent(context,RestaurantSelectorActivity.class);
						ArrayList<ParticularStoreInfo> allStores = dbUtil.getSubCategoryStores("Dine");
						intent.putParcelableArrayListExtra("ALL_RESTAURANTS", allStores);
						intent.putExtra("toApprovedPage", true);
						context.startActivity(intent);
						//finish();
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
			} else if(tag.equalsIgnoreCase(KEY_INBOX)) {

				final ArrayList<String> STOREID=dbUtil.getAllStoreIDs();
				final ArrayList<String> STORE_NAME=dbUtil.getAllStoresNames();
				final ArrayList<String>PHOTO      =dbUtil.getAllStorePhotoUrl();
				final ArrayList<String> STORE_DESC=dbUtil.getAllStoreDesc();
				final ArrayList<String> MALL_ID=dbUtil.getAllShopsMallIds();
				final ArrayList<String> PLACE_STATUS=dbUtil.getAllStoresPlaceStatus();
				final ArrayList<String> PLACE_PARENT=dbUtil.getAllShopsPlaceParent();

				EventTracker.logEvent(context.getResources().getString(R.string.tab_mInbox), true);

				if(dbUtil.isManager()){
					Intent intent = new Intent(context, InboxStoreList.class);
					context.startActivityForResult(intent, REQUEST_CODE);
					//finish();
				} else{
					if(STOREID.size()>1){
						//showToast("Call Store Chooser");
						Intent intent = new Intent(context, MultipleStoreList.class);
						context.startActivityForResult(intent, REQUEST_CODE);
					}
					else{
						//showToast("Call Chat Window");
						String mall_id = dbUtil.getPlaceParentFromPlaceId(STOREID.get(0));
						
						Intent intent = new Intent(context, InboxGetMessageStoreManager.class);
						intent.putExtra("READER", STOREID.get(0));
						intent.putExtra("SENDER", mall_id);
						intent.putExtra("FROM_IMG_URL", context.getResources().getString(R.string.photo_url)+dbUtil.getStoreLogoFromPlaceId(STOREID.get(0)));
						intent.putExtra("TO_IMG_URL", "");
						context.startActivityForResult(intent, REQUEST_CODE);
					}
				}
			} else if (tag.equalsIgnoreCase(KEY_MANAGE_TIPS)) {
				if (dbUtil.isManager()) {
					Intent intent = new Intent(context, TipsStoreList.class);
					context.startActivityForResult(intent, TIPS_REQUEST_CODE);
				} else {
					if (dbUtil.getAllStoreIDs().size() > 1) {
						Intent intent = new Intent(context, TipsStoreList.class);
						context.startActivityForResult(intent, TIPS_REQUEST_CODE);
					} else {
						Intent intent = new Intent(context, TipsDisplayActivity.class);
						ArrayList<String> store_ids = dbUtil.getAllStoreIDs();
						intent.putExtra("place_id", store_ids.get(0));
						context.startActivityForResult(intent, TIPS_REQUEST_CODE);
					}
				}
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		try
		{
			editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.customer));
			editor.commit();
			Intent intent=new Intent(context,HomeGrid.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}

	/**
	 * 
	 * Check whether this merchant has been loggedin in any other device 
	 * 
	 */
	public void validateMerchant(){

		JSONObject jObj = new JSONObject();
		//{"register_from":"iPad 6.1.2","user_id":18,"auth_id":8868622}
		try {
			jObj.put("register_from", "");
			jObj.put("user_id", USER_ID);
			jObj.put("auth_id", AUTH_ID);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header),getResources().getString(R.string.api_value));
		headers.put("X-HTTP-Method-Override","PUT");

		MyClass myClass = new MyClass(getApplicationContext());
		//ValidateUser validateUser = new ValidateUser(getApplicationContext());
		//validateUser.validateMerchant(RequestTags.Tag_ValidateMerchant);

		long lastDate = session.getLastDateOfMerchantApi();
		int  days = Integer.parseInt(getResources().getString(R.string.refresh_days));

		if(CustomMethods.getDaysDiff(lastDate)>=days){
			EventTracker.logEvent(getResources().getString(R.string.Merchant_ValidateLogin), false);
			myClass.validateUser(RequestTags.Tag_ValidateMerchant, jObj,headers);
			session.setParentStoreApiCallDate(CustomMethods.getPresentTime());
		}

	}



	/**
	 * Network request to check any unapproved offers, if response is true than show the badge on "Offer Approvals" tab
	 * 
	 */
	public void getAllUnApprovedPost(){
		try{
			if(NetworkCheck.isNetConnected(context)){
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put(getResources().getString(R.string.api_header),getResources().getString(R.string.api_value));
				headers.put(KEY_USER_ID,USER_ID);
				headers.put(KEY_AUTH_ID,AUTH_ID);

				List<NameValuePair>  nameValue = new ArrayList<NameValuePair>();
				nameValue.add(new BasicNameValuePair("place_id", dbUtil.getPlaceId("0")));

				MyClass myClass = new MyClass(context);
				myClass.getStoreRequest(RequestTags.Tag_UnapprovedBroadcast, nameValue, headers);
				//pBar.setVisibility(View.VISIBLE);
				InorbitLog.d("Merchnat user id "+USER_ID);


			}else{
				//showToast("No Internet Connection");
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}




	/**
	 * 
	 * Broadcast receiver for getAllUnApprovedPost()
	 * 
	 * @author Nitin
	 *
	 */

	class PostReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				//pBar.setVisibility(View.GONE);
				Bundle bundle = intent.getExtras();
				if(bundle!=null) {
					
					String success = bundle.getString("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						bundle = intent.getBundleExtra("BundleData");
						if(bundle!=null){
							ArrayList<PostDetail> postArr = bundle.getParcelableArrayList("POST_DETAILS");
							miLabelCount = postArr.size();
							/*adapter=new GridAdapter(MerchantHome.this.context, R.drawable.ic_launcher,  R.drawable.ic_launcher, gridItems,gridItemIcons);
							grid_home.setAdapter(adapter);*/
							/*pageAdapter = new PageAdapter(getSupportFragmentManager());
							viewPager.setAdapter(pageAdapter);*/
						} else {
							miLabelCount = 0;
						}
					}else{
						miLabelCount = 0;
					}
				} else {
					miLabelCount =0;
				}
				pageAdapter.notifyDataSetChanged();
				gridAdapterList.get(0).notifyDataSetChanged();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}




	/**
	 * Broadcast receiver for validateMerchant()
	 * 
	 * @author Nitin
	 *
	 */

	class ValidateMerchantBroadCast extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")) {
						//Toast.makeText(context, "True", Toast.LENGTH_SHORT).show();
					} else {
						//Toast.makeText(context, "False", Toast.LENGTH_SHORT).show();
						loginBack();
					}
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Grid adapter for merchant dashboard
	 * 	 * @author Nitin
	 */
	private static class GridAdapter extends ArrayAdapter<String> {
		Activity 			context;
		ArrayList<String>	gridItems;
		ArrayList<Integer>	gridItemIcons;
		LayoutInflater 		inflator;
		int page;
		
		public GridAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> gridItems,ArrayList<Integer>gridItemIcons, int page) {
			super(context, resource, textViewResourceId, gridItems);
			this.context=context;
			this.gridItems=gridItems;
			this.gridItemIcons=gridItemIcons;
			inflator=context.getLayoutInflater();
			this.page = page;
		}
		
		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}
		
		@Override
		public boolean isEnabled(int position) {
			if (page == 1 && 9+position >= gridItems.size())
				return false;
			else 
				return true;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder holder;

			try{
				if(convertView==null) {
					holder=new ViewHolder();
					convertView=inflator.inflate(R.layout.gridviewlayout, null);
					//holder.imgHomeGrid=(ImageView)convertView.findViewById(R.id.imgHomeGrid);
					holder.imgHomeGrid = (ImageView) convertView.findViewById(R.id.imageView1);
					holder.textview = (TextView) convertView.findViewById(R.id.textView1);
					holder.textLabel = (TextView) convertView.findViewById(R.id.text_LabelCount);
					holder.textview.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
					if (position+((int)Math.pow(9,page)==1 ? 0:(int)Math.pow(9,page))<gridItems.size()) {
						holder.Tag = gridItems.get(position+((int)Math.pow(9,page)==1 ? 0:(int)Math.pow(9,page)));
						convertView.setTag(holder);
					} else {
						convertView.setFocusable(false);
						holder.imgHomeGrid.setFocusable(false);
						holder.textview.setFocusable(false);
						holder.imgHomeGrid.setVisibility(View.INVISIBLE);
						holder.textview.setVisibility(View.INVISIBLE);
					}
				} else {
					convertView = convertView;
					holder = (ViewHolder) convertView.getTag();
					if (position+((int)Math.pow(9,page)==1 ? 0:(int)Math.pow(9,page))>=gridItems.size()) {
						convertView.setFocusable(false);
					}
				}
				/*if (MerchantHome.gridItems.size()/(int)Math.pow(9, page+1) != 0 || 
						(MerchantHome.gridItems.size()/(int)Math.pow(9, page+1) == 0 && (MerchantHome.gridItems.size() - (int)Math.pow(9, page)) > 0)) {*/
					ViewHolder hold=(ViewHolder)convertView.getTag();
					if ((position+((int)Math.pow(9,page)==1 ? 0:(int)Math.pow(9,page)) < gridItems.size()) 
							&& !gridItems.get(position+((int)Math.pow(9,page)==1 ? 0:(int)Math.pow(9,page))).equalsIgnoreCase("")) {
						hold.imgHomeGrid.setImageResource(gridItemIcons.get(position+((int)Math.pow(9,page)==1 ? 0:(int)Math.pow(9,page))));
						holder.textview.setText(gridItems.get(position+((int)Math.pow(9,page)==1 ? 0:(int)Math.pow(9,page))));
						
						if(gridItems.get(position+((int)Math.pow(9,page)==1 ? 0:(int)Math.pow(9,page))).equalsIgnoreCase(KEY_PENDING_APPROVALS)){
							if (miLabelCount>0) {
								holder.textLabel.setVisibility(View.VISIBLE);
								holder.textLabel.setText(""+miLabelCount);
							} else {
								holder.textLabel.setVisibility(View.GONE);
							}
						}
						
						//for Inbox
						if(gridItems.get(position+((int)Math.pow(9,page)==1 ? 0:(int)Math.pow(9,page))).equalsIgnoreCase(KEY_INBOX)){
							if (mTotalMsg>0) {
								holder.textLabel.setVisibility(View.VISIBLE);
								holder.textLabel.setText(""+mTotalMsg);
							} else {
								holder.textLabel.setVisibility(View.GONE);
							}
						} 
						
						if(gridItems.get(position+((int)Math.pow(9,page)==1 ? 0:(int)Math.pow(9,page))).equalsIgnoreCase(KEY_MANAGE_MENU)){
							if (unapprovedMenuCount>0) {
								holder.textLabel.setVisibility(View.VISIBLE);
								holder.textLabel.setText(""+unapprovedMenuCount);
							} else {
								holder.textLabel.setVisibility(View.GONE);
							}
						}
						
						if (gridItems.get(position+((int)Math.pow(9, page)==1 ? 0:(int)Math.pow(9, page))).equalsIgnoreCase(KEY_MANAGE_TIPS)) {

							if (tips_count > 0) {
								holder.textLabel.setVisibility(View.VISIBLE);
								holder.textLabel.setText(tips_count+"");
							} else {
								holder.textLabel.setVisibility(View.GONE);
							}
						}
						
						if (miLabelCount + mTotalMsg + unapprovedMenuCount + tips_count == 0) {

							holder.textLabel.setVisibility(View.GONE);
						}
					} else {
						convertView.setFocusable(false);
						/*holder.imgHomeGrid.setFocusable(false);
						holder.textview.setFocusable(false);
						holder.imgHomeGrid.setVisibility(View.INVISIBLE);
						holder.textview.setVisibility(View.INVISIBLE);*/
					}
					
					//for post approvals
					
				/*} else {
					holder.imgHomeGrid.setVisibility(View.INVISIBLE);
					holder.textview.setVisibility(View.INVISIBLE);
				}*/
			}catch(Exception ex){
				ex.printStackTrace();
			}

			return convertView;
		}
		
		@Override
		public int getCount() {
		/*	if (MerchantHome.gridItems.size()/(int)Math.pow(9, page+1) == 0) {
				return MerchantHome.gridItems.size() - (int)Math.pow(9, page);
			} else {*/
				return 9;
			//}
		}
	}

	static class ViewHolder{
		ImageView imgHomeGrid;
		TextView  textview;
		String	  Tag;
		TextView textLabel;
	}


	/**
	 * Shows the dialog to login again.
	 * 
	 */
	void loginBack(){
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Inorbit");
		alertDialogBuilder3
		.setMessage("It seems that you were loggedin from other device too, Please Re-login to continue.")
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				
				dialog.dismiss();
				//Intent intent=new Intent(context, MerchantsHomeGrid.class);
				
				session.logoutUser();
				Intent intent=new Intent(context,MerchantLogin.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		});
		AlertDialog alertDialog3 = alertDialogBuilder3.create();
		alertDialog3.show();
	}
	
	@Override
	protected void onResume() {
		try{
			
			EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter filter = new IntentFilter(RequestTags.Tag_ValidateMerchant);
			IntentFilter filter_unapprove_post_count = new IntentFilter(RequestTags.Tag_UnapprovedBroadcast);
			context.registerReceiver(validateReceiver = new ValidateMerchantBroadCast(), filter);
			context.registerReceiver(checkCountReceiverReceiver = new PostReceiver(), filter_unapprove_post_count);

			IntentFilter filter_total_count = new IntentFilter(RequestTags.TAG_TOTAL_COUNT);
			context.registerReceiver(mTotalCountReciver = new TotalCount(), filter_total_count);
			
			IntentFilter get_unapproved_menu_count = new IntentFilter(RequestTags.TAG_UNAPPROVED_MENU_COUNT);
			context.registerReceiver(unapprovedMenuCountReceiver = new UnapprovedMenuCountReceiver(), get_unapproved_menu_count);
			
			IntentFilter getStoreMgrTips = new IntentFilter(RequestTags.TAG_TIP_STORE_MANAGER);
			context.registerReceiver(storeMgrTips = new StoreMgrTips(), getStoreMgrTips);
			
			IntentFilter getMallMgrTips = new IntentFilter(RequestTags.TAG_TIP_MALL_MANAGER);
			context.registerReceiver(mallMgrTips = new StoreMgrTips(), getMallMgrTips);

			
			callRegularlyCountApi();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onResume();
	}

	@Override
	protected void onPause() {	 	
		try{
			EventTracker.endLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		try
		{
			editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.customer));
			editor.commit();
			Intent intent=new Intent(context,HomeGrid.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/* on start */
	@Override
	protected void onStart() {
		super.onStart();
		try{
			EventTracker.startFlurrySession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		try{
			EventTracker.endFlurrySession(getApplicationContext());
			context.unregisterReceiver(validateReceiver);
			context.unregisterReceiver(checkCountReceiverReceiver);
			context.unregisterReceiver(mTotalCountReciver);
			context.unregisterReceiver(unapprovedMenuCountReceiver);
			
			if (storeMgrTips != null) {
				context.unregisterReceiver(storeMgrTips);
			}

			handler.removeCallbacks(runnable);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		super.onStop();
	}

	/**
	 * Get the user id of the merchant
	 * 
	 * @return String : user id
	 */
	String getUserId(){
		HashMap<String,String>user_details = mSessionManager.getUserDetails();
		return user_details.get("user_id").toString();

	}

	/**
	 * Get the auth id of the merchant
	 * 
	 * @return String : Auth id
	 */
	String getAuthId(){
		HashMap<String,String>user_details = mSessionManager.getUserDetails();
		return user_details.get("auth_id").toString();
	}

	private void getTotalCount() {

		JSONObject json = new JSONObject();
		if(dbUtil.isManager()){
			//mTotlaMsg = 1;
			//showToast("Mall Manager");
			String place_id = dbUtil.getPlaceIdByPlaceParent("0");
			try {

				JSONArray reader_id = new JSONArray();
				reader_id.put(place_id);
				json.put("reader_id", reader_id);
				json.put("user_id", getUserId());
				json.put("auth_id", getAuthId());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		else{
			//mTotlaMsg = 2;
			final ArrayList<String> STORE_ID=dbUtil.getAllStoreIDs();
			try {
				JSONArray reader_id = new JSONArray();
				for(int i=0;i<STORE_ID.size();i++){
					reader_id.put(STORE_ID.get(i));	
				}
				json.put("reader_id", reader_id);
				json.put("user_id", getUserId());
				json.put("auth_id", getAuthId());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		InorbitLog.d("JSON "+json.toString());
		callTotalCountApi(json);
	}

	void callTotalCountApi(JSONObject json) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		MyClass myclass = new MyClass(context);
		myclass.postRequest(RequestTags.TAG_TOTAL_COUNT, headers, json);
	}

	class TotalCount extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				if(intent!=null){
					if (intent.getStringExtra("SUCCESS").equalsIgnoreCase("true")) {
						final ArrayList<String> STORE_ID=dbUtil.getAllStoreIDs();
						if(STORE_ID.size()>1){
							mTotalMsg = Integer.parseInt(intent.getStringExtra("TOTAL_COUNT"));
							pageAdapter.notifyDataSetChanged();
							/*if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) 
								gridAdapterList.get(1).notifyDataSetChanged();
							else
								*/gridAdapterList.get(0).notifyDataSetChanged();
							/*gridAdapter.notifyDataSetChanged();*/
							/*gridAdapter.notifyDataSetChanged();
							gridHome.setAdapter(gridAdapter);*/
							//showToast("Total Msg " + mTotlaMsg);
						} else {
							mTotalMsg = Integer.parseInt(intent.getStringArrayListExtra("COUNT").get(0));
							pageAdapter.notifyDataSetChanged();
							/*if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn())
								gridAdapterList.get(1).notifyDataSetChanged();
							else 
								*/gridAdapterList.get(0).notifyDataSetChanged();
							/*gridAdapter.notifyDataSetChanged();
							gridHome.setAdapter(gridAdapter);*/
							/*gridAdapter = new GridAdapter(MerchantHome.this, 0, 0, gridItems, gridItemIcons, 1);
							gridHome.setAdapter(gridAdapter);*/
							//showToast("Total Msg " + mTotlaMsg);
						}
					} else {
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	private void getTipsForStoreManager(ArrayList<String> store_ids) {
		
		JSONArray jsonArray = new JSONArray();
		for (String storeId: store_ids)
			jsonArray.put(storeId);

		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("reader_id", jsonArray);
			jsonObj.put("user_id", getUserId());
			jsonObj.put("auth_id", getAuthId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		
		MyClass myClass = new MyClass(context);
		myClass.postRequest(RequestTags.TAG_TIP_STORE_MANAGER, headers, jsonObj);
	}
	
	private void getTipsForMallManager() {

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("place_id", dbUtil.getPlaceIdByPlaceParent("0")));
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		headers.put("auth_id", getAuthId());
		headers.put("user_id", getUserId());
		
		MyClass myClass = new MyClass(context);
		myClass.getStoreRequest(RequestTags.TAG_TIP_MALL_MANAGER, nameValuePairs, headers);
	}
	
	private class StoreMgrTips extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent.getStringExtra("SUCCESS").equalsIgnoreCase("true")) {
					ArrayList<TipsManagerResponseModel> storeList = intent.getParcelableArrayListExtra("data");
					if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) {
						tips_count = Integer.parseInt(intent.getStringExtra("TOTAL_COUNT"));
					} else if (dbUtil.getAllStoreIDs().size() > 1){
						tips_count = Integer.parseInt(intent.getStringExtra("TOTAL_COUNT"));
					} else {
						tips_count = storeList.get(0).getCount();
					}
					pageAdapter.notifyDataSetChanged();
					if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) 
						gridAdapterList.get(0).notifyDataSetChanged();
					else
						gridAdapterList.get(0).notifyDataSetChanged();
				} else {
					//showToast(intent.getStringExtra("MESSAGE"));
				}
			} else {
				showToast("Error was generated");
			}
		}
	}
	
	/*private class GetAllTips extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent.getStringExtra("SUCCESS").equalsIgnoreCase("true")) {
					final ArrayList<String> STORE_ID=dbUtil.getAllStoreIDs();
					if(STORE_ID.size()>1){
						tips_count = Integer.parseInt(intent.getStringExtra("TOTAL_COUNT"));
						pageAdapter.notifyDataSetChanged();
						if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) 
							gridAdapterList.get(1).notifyDataSetChanged();
						else
							gridAdapterList.get(0).notifyDataSetChanged();
					} else {
						tips_count = Integer.parseInt(intent.getStringArrayListExtra("COUNT").get(0));
						pageAdapter.notifyDataSetChanged();
						if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn())
							gridAdapterList.get(1).notifyDataSetChanged();
						else 
							gridAdapterList.get(0).notifyDataSetChanged();
					}
				} else {
					
				}
				pageAdapter.notifyDataSetChanged();
				pBar.setVisibility(View.GONE);
			} else {
				showToast("Error was generated");
			}
		}
	}*/
	
	void getUnapprovedMenuCount() {
		if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) {
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			
			List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("mall_id", dbUtil.getPlaceIdByPlaceParent("0")));
			MyClass myClass = new MyClass(context);
			myClass.getStoreRequest(RequestTags.TAG_UNAPPROVED_MENU_COUNT, nameValuePairs, headers);
		}
	}
	
	class UnapprovedMenuCountReceiver extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent.getStringExtra("SUCCESS").equalsIgnoreCase("true")) {
					unapprovedMenuCount = intent.getIntExtra("TOTAL_COUNT", 0);
					pageAdapter.notifyDataSetChanged();
					gridAdapterList.get(0).notifyDataSetChanged();
					/*gridAdapter.notifyDataSetChanged();*/
				} else {
					unapprovedMenuCount = 0;
					pageAdapter.notifyDataSetChanged();
					gridAdapterList.get(0).notifyDataSetChanged();
					/*gridAdapter.notifyDataSetChanged();*/
				}
				//gridAdapter = new GridAdapter(MerchantHome.this, 0, 0, gridItems, gridItemIcons, 0);
				/*gridAdapter.notifyDataSetChanged();
				gridHome.setAdapter(gridAdapter);*/
			} else {
				showToast("Some Error was generated");
			}
		}
	}
	
	private static void showToast(String msg){
		Toast.makeText(context, ""+msg, 0).show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) {
			getTipsForMallManager();
		} else {
			final ArrayList<String> store_ids = dbUtil.getAllStoreIDs();
			storeNames = dbUtil.getAllStoresNames();
			storeLogoUrls = dbUtil.getAllStorePhotoUrl();
			getTipsForStoreManager(store_ids);
		}
		
		if(Tables.mbNeedToCheckCount && !dbUtil.isMultipuleMallsLoggedIn()){
			getAllUnApprovedPost();
		}
		
		if (requestCode == 10)
			getUnapprovedMenuCount();
		
		if(requestCode==REQUEST_CODE) {
			if(resultCode == RESULT_OK) {
				getTotalCount();
				//showToast("Called api again");
			}
		}
	}


	private void callRegularlyCountApi() {
		//to call regularly total count api
		handler = new Handler();
		runnable = new Runnable() {

			@Override
			public void run() {
				try {
					getUnapprovedMenuCount();
					getTotalCount();
					//InorbitLog.d("Called Count Api");
					handler.postDelayed(runnable, 60000);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		};
		handler.postDelayed(runnable,60000);
	}
	
	
	class PlaceChooserBroadcastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(final Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{

				//mRelProgressLayout.setVisibility(ViewGroup.GONE);
				Bundle bundle = intent.getExtras();
				if(intent!=null){
					String SEARCH_STATUS = intent.getStringExtra("SUCCESS");

					if(SEARCH_STATUS.equalsIgnoreCase("true")){

						ArrayList<String> mArrSTORE_IDS = new ArrayList<String>();

						ArrayList<ParticularStoreInfo> particularStoreInfoArr = intent.getParcelableArrayListExtra("ParticularStoreInfoDetails");

						for(int i=0;i<particularStoreInfoArr.size();i++){
							mArrSTORE_IDS.add(particularStoreInfoArr.get(i).getId());
						}
						
						DBUtil mdbUtil = new DBUtil(context);

						boolean isManagerLoggedIn = mdbUtil.isManager();

						if(particularStoreInfoArr.size()!=0){
							if(isManagerLoggedIn){
								ConfigFile.setManagerLoogedIn(true);
								session.setMallManagerLoggedin(true);
							}else{

								//tagMerchant();
								ConfigFile.setManagerLoogedIn(false);
								session.setMallManagerLoggedin(false);
							}
						}else{
							ConfigFile.setManagerLoogedIn(false);
							session.setMallManagerLoggedin(false);
						}

						Handler handler = new Handler(); 
						handler.postDelayed(new Runnable() { 
							public void run() { 
								Intent intent=new Intent(context, MerchantHome.class);
								startActivity(intent);	
								finish();
							} 
						}, 500);

					}else{

						String msg=intent.getStringExtra("MESSAGE");
						Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){
							int code1 = intent.getIntExtra("CODE",0);
							String message1=intent.getStringExtra("MESSAGE");
							String errorMessage = intent.getStringExtra("errorMessage");
							String apiUrl = intent.getStringExtra("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "PlaceChooser");
							}
							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}
					}
				}else{

				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}
	
	/*class PageAdapter extends FragmentStatePagerAdapter{
		Activity context;
		ArrayList<String> pages;
		
		List<Fragment> fragments;
		
		public PageAdapter(FragmentManager fm,List<Fragment> fragments) {
			super(fm);
			// TODO Auto-generated constructor stub
			
			this.fragments = fragments;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return fragments.size();
		}

	}
	
	private List<Fragment> getFragments(){
		List<Fragment> fList = new ArrayList<Fragment>();

		for(int i=0;i<pages.size();i++){
			fList.add(PageFragment.newInstance(i));
		}
		return fList;
	}*/
	
	private class PageAdapter extends FragmentStatePagerAdapter{
		
		public PageAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return position+"";
		}
		
		@Override
		public Fragment getItem(int position) {
			return PageFragment.newInstance(position);
			/*PageFragment pf = new PageFragment();
			pf.newInstance(position);
			return pf;*/
		}
		
		@Override
		public int getCount() {
			return (int)Math.ceil(gridItems.size()/9.0);
		}
	}
	/*
	private List<Fragment> getFragments() {
		private List<Fragment> flist = new ArrayList<Fragment>();
		for (int i=0; i<)
	}
	*/
	/*private List<Fragment> getFragments(){
		List<Fragment> fList = new ArrayList<Fragment>();

		for(int i=0;i<pages.size();i++){
			fList.add(PageFragment.newInstance(i));
		}
		return fList;
	}*/
	
	public static class PageFragment extends Fragment {
		View view;
		ProgressBar prog;
		int pos;
		
		public PageFragment(){

		}
		
		public static final PageFragment newInstance(int pos){
			PageFragment pageFragment = new PageFragment();
			Bundle bdl=new Bundle(1);
			bdl.putInt("PagePosition", pos);
			pageFragment.setArguments(bdl);
			return pageFragment;
		}
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			
			try {
				this.pos = getArguments().getInt("PagePosition");
				InorbitLog.d("Position Fragment "+this.pos);
				view = inflater.inflate(R.layout.homegridstatic, null);
				gridHome = (GridView) view.findViewById(R.id.grid_home);
				final RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.layout_grid);
				if (gridAdapterList.size()-1 < pos) {
					GridAdapter gridAdapter = new GridAdapter(context, R.drawable.ic_launcher,  R.drawable.ic_launcher, gridItems,gridItemIcons, pos);
					gridHome.setAdapter(gridAdapter);/*GridViewLayoutAdapter(context, this.pos)*/
					gridHome.setOnItemClickListener(new OnItemClickListener() {
						
						@Override
						public void onItemClick(AdapterView<?> arg0, View view,
								int position, long arg3) {
							startClassFromTag(view);
						}
						
					});
					gridAdapterList.add(gridAdapter);
				} else {
					gridHome.setAdapter(gridAdapterList.get(pos));
					gridHome.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							startClassFromTag(view);
						}
					});
				}
				

			}catch(Exception ex){
				ex.printStackTrace();
			}
			return view;
		}
	}
	
	/*private static class GridViewLayoutAdapter extends BaseAdapter {
		int page;
		Activity context;
		LayoutInflater inflater;

		public GridViewLayoutAdapter(Activity context, int page) {
			this.page = page;
			this.context = context;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return 9;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View view = null;
			Holder holder;

			try {
				if(convertView == null) {
					holder = new Holder();
					view = inflater.inflate(R.layout.gridviewlayout, null);
					holder.image = (ImageView) view.findViewById(R.id.imageView1);
					holder.image.setVisibility(View.VISIBLE);
					holder.textview = (TextView) view.findViewById(R.id.textView1);
					holder.textview.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
					
					view.setTag(holder);
				} else {
					view = convertView;
					holder = (Holder) view.getTag();
				}
				
				try{
					if(page==0) {
						int id = gridItemIcons.get(position);
						String text = gridItems.get(position);

						holder.tag = gridItems.get(position);
						InorbitLog.d("GridTag "+position+" -- "+gridItems.get(position));
						holder.image.setImageResource(id);
						holder.textview.setText(text.toUpperCase());
						
						

					}else{
						int id = gridItemIcons.get(position+9);
						String text = gridItems.get(position+9);

						holder.tag = gridItems.get(position+9);
						InorbitLog.d("GridTag "+(position+9)+" -- "+gridItems.get(position+9));
						holder.image.setImageResource(id);
						holder.textview.setText(text.toUpperCase());

						if(text.equalsIgnoreCase("")){
							holder.image.setVisibility(View.INVISIBLE);
							holder.textview.setVisibility(View.INVISIBLE);
						}else{
							holder.image.setVisibility(View.VISIBLE);
							holder.image.setImageResource(id);
							holder.textview.setText(text.toUpperCase());
							holder.textview.setVisibility(View.VISIBLE);
						}

						holder.textview.setTextColor(context.getResources().getColor(R.color.inorbit_blue));
					}
					
					
					//InorbitLog.d("Title "+gridItems.get(position+9).toUpperCase());
					
					String text = gridItems.get(position*(page+1));
					
					holder.tag = gridItems.get(position*(page+1));
					InorbitLog.d("GridTag "+(position*(page+1))+" -- "+gridItems.get(position*(page+1)));
					holder.image.setImageResource(gridItemIcons.get(position*(page+1)));
					holder.textview.setText(text.toUpperCase());
					
					holder.image.setVisibility(View.VISIBLE);
					holder.image.setImageResource(gridItemIcons.get(position*(page+1)));
					holder.textview.setText(text.toUpperCase());
					holder.textview.setVisibility(View.VISIBLE);

					holder.textview.setTextColor(context.getResources().getColor(R.color.inorbit_blue));
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
			return view;
		}
	}

	static class Holder{

		ImageView image;
		TextView textview;
		String tag;
	}*/
}
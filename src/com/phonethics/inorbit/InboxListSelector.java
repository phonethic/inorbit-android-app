package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.model.ParticularStoreInfo;
import com.phonethics.model.RequestTags;
import com.phonethics.model.StoreListModel;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

public class InboxListSelector extends SherlockActivity {

	private Activity mActivity;
	private Context mContext;
	private ActionBar mActionBar;
	private ListView mStoreList;
	SessionManager 	mSessionMnger;
	RelativeLayout mProgressBar;
	PlaceChooserBroadcastReceiver 	mbPlaceChooserBroadcast;
	Button mNewMessage;
	Button mDoneBtn;
	ArrayList<String>	marrTempPlaceId	=	new ArrayList<String>();
	CreateBroadCastListAdapter mAdapter;
	boolean all_store_selected;
	com.actionbarsherlock.view.Menu action_bar_menu;
	/*final ArrayList<String> STOREID		= new ArrayList<String>();
	final ArrayList<String> STORE_NAME	= new ArrayList<String>();
	final ArrayList<String> PHOTO		= new ArrayList<String>();
	final ArrayList<String> STORE_DESC	= new ArrayList<String>();
	final ArrayList<String> PLACE_PARENT = new ArrayList<String>();
	final ArrayList<String> MALL_ID		= new ArrayList<String>();
	final ArrayList<String> PLACE_STATUS= new ArrayList<String>();*/
	EditText searchText;
	private ArrayList<ListModel> store_models = new ArrayList<InboxListSelector.ListModel>();
	
	public class ListModel {
		public ParticularStoreInfo store;
		public boolean checked;
		
		public ListModel(ParticularStoreInfo storeinfo, boolean checked) {
			this.store = storeinfo;
			this.checked = checked;
		}
		
		@Override
		public boolean equals(Object o) {
			if (this.store.getId().equals(((ParticularStoreInfo)o).getId()))
				return true;
			return false;
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inbox_list_selector);

		mContext = this;
		mActivity = this;
		mActionBar	=	getSupportActionBar();
		mActionBar.setTitle("Inbox");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();

		mSessionMnger = new SessionManager(mContext);
		mProgressBar = (RelativeLayout)findViewById(R.id.mProgressBar);
		mStoreList = (ListView)findViewById(R.id.mNewMsgStoreList);
		mDoneBtn = (Button)findViewById(R.id.mDoneBtn);
		searchText = (EditText)findViewById(R.id.searchText);
		
		loadAllStoreForUser();

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (mAdapter != null) {
                    mAdapter.getFilter().filter(searchText.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        
        
		mDoneBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				marrTempPlaceId.clear();
				for(int i=0;i<store_models.size();i++){
					if(store_models.get(i).checked) {
						marrTempPlaceId.add(store_models.get(i).store.getId());
					}
				}
				if(marrTempPlaceId.size()!=0){
					//Intent intent=new Intent(mContext, CreateBroadCast.class);
					Intent intent=new Intent(mContext, InboxCreateMessage.class);
					intent.putStringArrayListExtra("SELECTED_STORE_ID",marrTempPlaceId);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					InputMethodManager imm = 
						    (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
					//finish();
				}else{
					showToast("Please select atleast one store");
				}

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		getSupportMenuInflater().inflate(R.menu.select_all_stores, menu);
		this.action_bar_menu = menu;
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (!all_store_selected) {
			action_bar_menu.findItem(R.id.select_all_stores).setTitle("Select All");
		} else {
			action_bar_menu.findItem(R.id.select_all_stores).setTitle("Unselect All");
		}
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.select_all_stores) {
			if (!all_store_selected) {
				for (int i=0; i<store_models.size(); i++) {
					store_models.get(i).checked = true;
				}
				all_store_selected = true;
			} else {
				for (int i=0; i<store_models.size(); i++) {
					store_models.get(i).checked = false;
				}
				all_store_selected = false;
				action_bar_menu.findItem(R.id.select_all_stores).setTitle("Select All");
			}
			
			mAdapter.notifyDataSetChanged();
			this.supportInvalidateOptionsMenu();
			ActivityCompat.invalidateOptionsMenu(this);
			return true;
		}
		else{
			finish();
		}
		return false;
	}

	void loadAllStoreForUser(){

		mProgressBar.setVisibility(ViewGroup.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		headers.put("user_id", getUserId());
		headers.put("auth_id", getAuthId()); 
		InorbitLog.d("Merchnat user id "+getUserId());
		MyClass myClass = new MyClass(mContext);
		myClass.getStoreRequest(RequestTags.TagPlaceChooser, null, headers);

		//new ValidateUser(mContext).validateMerchant(RequestTags.Tag_ValidateMerchant);

	}

	String getUserId(){
		HashMap<String,String>user_details = mSessionMnger.getUserDetails();
		return user_details.get("user_id").toString();

	}

	String getAuthId(){
		HashMap<String,String>user_details = mSessionMnger.getUserDetails();
		return user_details.get("auth_id").toString();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			IntentFilter filter = new IntentFilter(RequestTags.TagPlaceChooser);

			mContext.registerReceiver(mbPlaceChooserBroadcast = new PlaceChooserBroadcastReceiver(), filter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		mContext.unregisterReceiver(mbPlaceChooserBroadcast);
		super.onStop();
	}

	class PlaceChooserBroadcastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(final Context context, Intent intent) {
			try{

				mProgressBar.setVisibility(ViewGroup.GONE);
				if(intent!=null){
					String SEARCH_STATUS = intent.getStringExtra("SUCCESS");
					if(SEARCH_STATUS.equalsIgnoreCase("true")){
						/*ArrayList<ParticularStoreInfo> particularStoreInfoArr = intent.getParcelableArrayListExtra("ParticularStoreInfoDetails");*/
						
						/*
						for(int i=1;i<particularStoreInfoArr.size();i++){

							STOREID.add(particularStoreInfoArr.get(i).getId());
							STORE_NAME.add(particularStoreInfoArr.get(i).getName());
							PHOTO.add(particularStoreInfoArr.get(i).getImage_url());
							STORE_DESC.add(particularStoreInfoArr.get(i).getDescription());
							MALL_ID.add(particularStoreInfoArr.get(i).getMall_id());
							PLACE_STATUS.add(particularStoreInfoArr.get(i).getPlace_status());
							PLACE_PARENT.add(particularStoreInfoArr.get(i).getPlace_parent());
						}

						final ArrayList<StoreListModel> list=new ArrayList<StoreListModel>();
						final ArrayList<StoreListModel> list_id=new ArrayList<StoreListModel>();
						final ArrayList<StoreListModel> list_id_desc=new ArrayList<StoreListModel>();

						for(int i=0;i<STOREID.size();i++)
						{
							Log.d("", "Inorbit store id >> "+STOREID.get(i));
							list.add( get(STORE_NAME.get(i), false) );
							list_id.add( getID(STORE_NAME.get(i),STOREID.get(i),false) );
							list_id_desc.add( getID(STORE_NAME.get(i),STOREID.get(i),STORE_DESC.get(i),false) );
							//logos.add(particularStoreInfoArr.get(i).getImage_url());
						}*/

						/*****------------New-----------*/
						
						ArrayList<ParticularStoreInfo> stores = intent.getParcelableArrayListExtra("ParticularStoreInfoDetails");
						for (int i=1; i<stores.size(); i++) {
							store_models.add(new ListModel(stores.get(i), false));
						}
						/*--------------End New-------------------***/
						mAdapter = new CreateBroadCastListAdapter(mActivity, 0, store_models);
						mStoreList.setAdapter(mAdapter);

					}else{
						showToast("No Stores Available");
					}
				}else{
					MyToast.showToast(InboxListSelector.this, "");
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	class CreateBroadCastListAdapter extends ArrayAdapter<ListModel> implements Filterable {
		/*ArrayList<StoreListModel> TITLE;
		ArrayList<StoreListModel> ID;
		ArrayList<StoreListModel>list_id_desc;*/
		ArrayList<ListModel> storesInfo;
		/*ArrayList<String>place_parent;
		ArrayList<String>distance,logo;*/
		Activity context;
		LayoutInflater inflate;

		public CreateBroadCastListAdapter(Activity context, int resource, ArrayList<ListModel> storesInfo) {
			super(context, resource, storesInfo);
			this.context=context;
			inflate=context.getLayoutInflater();
			this.storesInfo = storesInfo;
		}
		
		@Override
		public int getCount() {
			return storesInfo.size();
		}


		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		/*StoreListModel getselectedposition(int position) {
			return ((StoreListModel) getItem(position));
		}*/

		/*ArrayList<StoreListModel> getcheckedposition() {
			ArrayList<StoreListModel> checkedposition = new ArrayList<StoreListModel>();
			for (StoreListModel p : ID) {
				if (p.isSelected())
					checkedposition.add(p);
			}
			return checkedposition;
		}*/

		/*OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getselectedposition((Integer) buttonView.getTag()).selected = isChecked;
			}
		};*/



		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
 
			if (convertView==null) {
				final ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.inbox_store_selector,null);
				holder.txtTitle=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.placeCheckBox=(CheckBox)convertView.findViewById(R.id.placeCheckBox);
				holder.imgsearchLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				//holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				holder.placeCheckBox.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						ListModel  model = (ListModel) holder.placeCheckBox.getTag();
						model.checked = !model.checked;
						for (int i=0; i<storesInfo.size(); i++) {
							if (storesInfo.get(i).equals(model.store)) {
								storesInfo.get(i).checked = model.checked;
								for (int j=0; j<store_models.size(); j++)
									if (storesInfo.get(i).equals(store_models.get(j).store)) {
										store_models.get(j).checked = model.checked;
									}
								
								InorbitLog.d(storesInfo.get(i).checked+" "+i);
								break;
							}
							InorbitLog.d(storesInfo.get(i).checked+" "+i);
						}
					}
				});
				
				convertView.setTag(holder);
				holder.placeCheckBox.setTag(storesInfo.get(position));
				
				holder.txtTitle.setTextColor(Color.BLACK);
				holder.imgsearchLogo.setScaleType(ScaleType.CENTER_INSIDE);
			} else {
				((ViewHolder)convertView.getTag()).placeCheckBox.setTag(storesInfo.get(position));
			}
				
			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.txtTitle.setText(storesInfo.get(position).store.getName());
			hold.placeCheckBox.setChecked(storesInfo.get(position).checked);
			
			String photo_source = storesInfo.get(position).store.getImage_url().toString().replaceAll(" ", "%20");
			try {
				if(!photo_source.equalsIgnoreCase("")){
					Picasso.with(context).load(getResources().getString(R.string.photo_url)+photo_source)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(hold.imgsearchLogo);
				}
				else{
					hold.imgsearchLogo.setImageResource(R.drawable.ic_launcher);
				}
			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			} catch(Exception e){
				e.printStackTrace();
			}

			return convertView;
		}
		
		@Override
		public Filter getFilter() {
			// TODO Auto-generated method stub
			 return new Filter() {
		            @Override
		            protected FilterResults performFiltering(CharSequence charSequence) {
		                List<ListModel> filteredResult = getFilteredResults(charSequence.toString());

		                FilterResults results = new FilterResults();
		                results.values = filteredResult;
		                results.count = filteredResult.size();
		                
		                return results;
		            }
		            
		            @Override
		            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
		            	if (filterResults!= null && filterResults.count > 0) {
		            		storesInfo = (ArrayList<ListModel>) filterResults.values;
		            	} else {
		            		storesInfo = new ArrayList<InboxListSelector.ListModel>();
		            	}
		            	InorbitLog.d("null "+(filterResults==null));
		                mAdapter.notifyDataSetChanged();
		            }


		            private ArrayList<ListModel> getFilteredResults(String constraint){
		                if (constraint.length() == 0){
		                    return  store_models;
		                }
		                ArrayList<ListModel> listResult = new ArrayList<ListModel>();
		                for (ListModel obj : store_models) {
		                	InorbitLog.d(obj.store.getName()+" "+constraint);
		                    if (constraint.length()<=obj.store.getName().length() && obj.store.getName().toLowerCase().contains(constraint.toLowerCase())/*constraint.equalsIgnoreCase(obj.store.getName().subSequence(0, constraint.length()).toString())*/){
		                        listResult.add(obj);
		                    }
		                }
		                return listResult;
		            }
		        };
		}
	}

	
	
	static class ViewHolder{

		TextView txtTitle;
		CheckBox placeCheckBox;
		ImageView imgsearchLogo;
		//TextView txtSearchStoreDesc;
	}

	private StoreListModel get(String placename,boolean selected) {
		return new StoreListModel(placename,selected);
	}
	private StoreListModel getID(String placename,String id,boolean selected) {
		StoreListModel sm=new StoreListModel(placename, selected);
		sm.setId(id);
		return  sm;
	}

	private StoreListModel getID(String placename,String id,String placedesc,boolean selected) {
		StoreListModel sm=new StoreListModel(placename,placedesc,selected);
		sm.setId(id);
		return  sm;
	}

	void showToast(String msg){
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}

}

package com.phonethics.inorbit;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

public class ShowMessageDialog {

	
	public static void show(Activity context,String msMessage){
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.message_dialog);
		
		
		TextView mtvMessage = (TextView) dialog.findViewById(R.id.text_message);
		mtvMessage.setText(msMessage);
		mtvMessage.setTypeface(InorbitApp.getTypeFace());
		
		TextView mtvOk = (TextView) dialog.findViewById(R.id.text_ok);
		mtvOk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		
		dialog.show();
		
		
		
	}
	
}

package com.phonethics.inorbit;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.customviewpager.JazzyViewPager.TransitionEffect;

public class StorePagerGallery extends SherlockFragmentActivity {

	public static ImageLoader imageLoader;
	public static DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;

	ActionBar actionbar;
	Activity context;
	JazzyViewPager mPager;
	ArrayList<String> urls=new ArrayList<String>();
	int position=0;
	NetworkCheck network;
	public PageAdapter adapter;

	static String PHOTO_PARENT_URL;
	PageFragment pageFrag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_pager_gallery);
		 if (savedInstanceState != null) {
		        //Restore the fragment's instance
		        pageFrag = (PageFragment) getSupportFragmentManager().getFragment(
		                    savedInstanceState, "mContent");
		}
		context=this;
		
		//Photo url
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);
		
		actionbar=getSupportActionBar();
		actionbar.setTitle(getResources().getString(R.string.actionBarTitle));
/*		actionbar.setSubtitle("Store Gallery");*/
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();

		network=new NetworkCheck(context);
		
		Bundle b=getIntent().getExtras();

		imageLoader=ImageLoader.getInstance();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitLocalCache");
		}
		else
		{
			cacheDir=context.getCacheDir();
		}

		if(!cacheDir.exists())
		{
			cacheDir.mkdirs();
		}/*else if(network.isNetworkAvailable())
		{

			DeleteRecursive(cacheDir);
		}
		 */
		config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();
		mPager=(JazzyViewPager)findViewById(R.id.viewPagerStore);

		try
		{
		if(b!=null)
		{
			urls=b.getStringArrayList("photo_source");
			position=b.getInt("position");
			//urls.remove(0);

			for(int i=0;i<urls.size();i++){
				InorbitLog.d("Inrobit Url "+urls.get(i));
			}
			
			adapter=new PageAdapter(getSupportFragmentManager(), context, urls);
			mPager.setAdapter(adapter);
			mPager.setTransitionEffect(TransitionEffect.ZoomIn);
			mPager.setCurrentItem(position, true);
		}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		getSupportFragmentManager().putFragment(outState, "mContent", pageFrag);
		//outState.putInt("position", position);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
			finishTo();
		
		return true;
	}

	void finishTo()
	{
		this.finish();
	}


	//Creating Pages with PageAdapter.
	public class PageAdapter extends FragmentStatePagerAdapter
	{
		Activity context;
		ArrayList<String> pages;
		public PageAdapter(FragmentManager fm,Activity context,ArrayList<String> pages) {
			super(fm);
			this.context=context;
			this.pages=pages;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			String photo=pages.get(position).toString().replaceAll(" ", "%20");
			InorbitLog.d("Inorbit url "+photo);
			pageFrag =  new PageFragment(photo, context);
			return pageFrag;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return pages.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Object obj = super.instantiateItem(container, position);
			mPager.setObjectForPosition(obj, position);
			return obj;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if(object != null){
				return ((Fragment)object).getView() == view;
			}else{
				return false;
			}
		}

		/*@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub

			FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();

			super.destroyItem(container, position, object);
		}*/
	}


	@SuppressLint("ValidFragment")
	public static class PageFragment extends Fragment 
	{
		String url="";
		Activity context;
		View view;
		TouchImageView splashImage;
		ProgressBar prog;
		TextView txtPlaceText;
		Animation anim;
		
		public PageFragment() {

		}
		
		public PageFragment(String url,Activity context)
		{
			this.url=url;
			this.context=context;
			anim=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim.setDuration(1200);
		}
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			try
			{
			view=inflater.inflate(R.layout.storeimagepager, null);
			splashImage=(TouchImageView)view.findViewById(R.id.storeImageView);
			prog=(ProgressBar)view.findViewById(R.id.prog);

			/*			txtPlaceText.startAnimation(anim);*/

			imageLoader.displayImage(PHOTO_PARENT_URL+url, splashImage,options, new ImageLoadingListener() {

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					// TODO Auto-generated method stub
					prog.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view,
						FailReason failReason) {
					// TODO Auto-generated method stub
					//prog.setVisibility(View.INVISIBLE);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					// TODO Auto-generated method stub
					prog.setVisibility(View.INVISIBLE);
				}

				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					// TODO Auto-generated method stub
					//prog.setVisibility(View.INVISIBLE);
					
				}
			});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			return view;
		}
	}


}
package com.phonethics.inorbit.syncadapter;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;

import com.phonethics.inorbit.DBUtil;
import com.phonethics.inorbit.InorbitLog;

/*
 * Define an implementation of ContentProvider that stubs out
 * all methods
 */
public class StubProvider extends ContentProvider implements LoaderCallbacks<Cursor> {
	private DBUtil dbUtil;
	private SQLiteOpenHelper dbHelper;
	private SQLiteDatabase db;
	private static final String TABLE_NAME = "TABLE_ANALYTICS";
	private static final String UDM_ID = "ANALYTICS_UDM_ID";
	private static final String DATE = "ANALYTICS_DATE";
	private static final String FLAG = "ANALYTICS_FLAG";
	private static final String ACTIVITY_ID = "ANALYTICS_ACTIVITY_ID";
	private static final String MALL_ID = "ANALYTICS_MALL_ID";
	private static final String SYNC_STATUS = "ANALYTICS_SYNC_STATUS";
	
    /*
     * Always return true, indicating that the
     * provider loaded correctly.
     */
    @Override
    public boolean onCreate() {
    	InorbitLog.d("StubProvider OnCreate");
    	dbUtil = new DBUtil(getContext());
    	dbHelper = dbUtil.getDBHelper();
    	db = dbHelper.getWritableDatabase();
    	
        return true;
    }
    /*
     * Return an empty String for MIME type
     */
    @Override
	public synchronized String getType(Uri uri) {
		// TODO Auto-generated method stub
    	
		return new String();
	}
    /*
     * query() always returns no results
     *
     */
    @Override
    public synchronized Cursor query(
            Uri uri,
            String[] projection,
            String selection,
            String[] selectionArgs,
            String sortOrder) {
    
    	Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE SYNC_STATUS  = -1 ", null);
        return cursor;
    
    }
    /*
     * insert() always returns null (no URI)
     */
    @Override
    public synchronized Uri insert(Uri uri, ContentValues values) {
    	long yourmilliseconds = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");

        Date resultdate = new Date(yourmilliseconds);
    	
    	values.put("DATE", sdf.format(resultdate));
    	db.insert(TABLE_NAME, null, values);/*("INSERT INTO " + TABLE_NAME + " VALUES ("+values.get("_ID") + ", " + values.get(UDM_ID)+", "+values.get(DATE)+", "+values.get(FLAG)
    			+", "+values.get(ACTIVITY_ID)+", "+values.get(MALL_ID)+", "+values.get(SYNC_STATUS) +");");*/
        return uri;
    }
    /*
     * delete() always returns "no rows affected" (0)
     */
    @Override
    public synchronized int delete(Uri uri, String selection, String[] selectionArgs) {
    	InorbitLog.d("Delete");
    	int count = db.delete(TABLE_NAME, "SYNC_STATUS = ?", new String[]{"0"});
        return count;
    }
    /*
     * update() always returns "no rows affected" (0)
     */
    public synchronized int update(
            Uri uri,
            ContentValues values,
            String selection,
            String[] selectionArgs) {
    	
    	for (int i = 0; i < selectionArgs.length; i++) {
    		db.execSQL("UPDATE " + TABLE_NAME + " SET SYNC_STATUS = " + 0 +
    				" WHERE  SYNC_STATUS  = -1 AND _ID = " + selectionArgs[i]);
    	}
        return selectionArgs.length;
    }
    
    
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		// TODO Auto-generated method stub
		return null; //new CursorLoader(this, uri, projection, selection, selectionArgs, sortOrder);
	}
	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub
		
	}
}
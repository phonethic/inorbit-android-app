package com.phonethics.inorbit.syncadapter;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.inorbit.DBUtil;
import com.phonethics.inorbit.InorbitLog;
import com.phonethics.inorbit.MyClass;
import com.phonethics.inorbit.NetworkCheck;
import com.phonethics.inorbit.R;
import com.phonethics.model.RequestTags;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.content.AbstractThreadedSyncAdapter;
import android.content.BroadcastReceiver;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.telephony.SmsManager;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */
@SuppressLint("NewApi")
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    // Global variables
    // Define a variable to contain a content resolver instance
    ContentResolver mContentResolver;
    Context context;
    AnalyticsResponseReceiver activityTracking;
    /*long id = 0;*/
    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
        this.context = context;
        IntentFilter activityAnalytics = new IntentFilter(RequestTags.AnalyticsRequest);
		context.registerReceiver(activityTracking = new AnalyticsResponseReceiver(), activityAnalytics);
    }
    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
        this.context = context;
        IntentFilter activityAnalytics = new IntentFilter(RequestTags.AnalyticsRequest);
		context.registerReceiver(activityTracking = new AnalyticsResponseReceiver(), activityAnalytics);
    }
	@Override
	public void onPerformSync(Account account, Bundle extras, String authority,
			ContentProviderClient provider, SyncResult syncResult) {
		// TODO Auto-generated method stub
		//checkVersion();
		InorbitLog.d("In Sync "+ authority);
		Uri uri = new Uri.Builder().authority(authority).appendPath("/insert").build();
		try {
			ContentValues cv = new ContentValues(6);
			/*cv.put("_ID", id++);*/
			cv.put("UDM_ID", "123");
			cv.put("DATE", System.currentTimeMillis());
			cv.put("FLAG", "5");
			cv.put("ACTIVITY_ID", "345");
			cv.put("MALL_ID", "1");
			cv.put("SYNC_STATUS", -1);
			//provider.insert(uri, cv);
			
			Cursor cursor = provider.query(uri, new String[]{}, authority, new String[]{}, "");
			if (cursor != null && cursor.moveToFirst()) {
				JSONArray jsonArr = new JSONArray();
				while (!cursor.isAfterLast()) {
					InorbitLog.d(cursor.getInt(0) + " " + cursor.getString(1) + " " + cursor.getString(2)
							 + " " + cursor.getString(3) + " " + cursor.getString(4) + " " + cursor.getString(5) + " " + cursor.getString(6));
					JSONObject item = new JSONObject();
					try {
						item.put("id", cursor.getInt(0));
						item.put("UDM_id", cursor.getString(1));
						item.put("date", cursor.getString(2));
						item.put("flag", cursor.getString(3));
						item.put("AM_id", cursor.getString(4));
						item.put("activ_mall_id", cursor.getString(5));
						jsonArr.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
						cursor.close();
						return;
					}
					cursor.moveToNext();
				}
				
				JSONObject activityData = new JSONObject();
				try {
					activityData.put("activitys", jsonArr);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put(context.getResources().getString(R.string.api_header), context.getResources().getString(R.string.api_value));
				if(NetworkCheck.isNetConnected(context)) {
					MyClass myClass = new MyClass(context);
					myClass.postRequest(RequestTags.AnalyticsRequest, headers, activityData);
				}
			} else {
				provider.delete(uri, "", new String[]{});
			}
			//provider.update(uri, new ContentValues(), authority, new String[]{});
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Toast.makeText(context.getApplicationContext(), "In Sync", Toast.LENGTH_SHORT).show();
	}
	
	private void checkVersion(){

		InorbitLog.d("Refresh Version api call");
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(context.getResources().getString(R.string.api_header), context.getResources().getString(R.string.api_value));
		MyClass myClass = new MyClass(context);
		if(NetworkCheck.isNetConnected(context)) {
			myClass.getStoreRequest(RequestTags.Tag_GetDataVersion, null, headers);
		}
	}
	
	private class AnalyticsResponseReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				if(intent != null) {
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")) {
						DBUtil dbUtil = new DBUtil(context);
						String[] arr = intent.getStringArrayExtra("data");
						Cursor cursor = dbUtil.getCursorActivityWithStatus(arr, "0");
						if (cursor != null && cursor.moveToFirst()) {
							while (!cursor.isAfterLast()) {
								InorbitLog.d(cursor.getString(0) + " "+cursor.getString(6));
								cursor.moveToNext();
							}
						}
						cursor.close();
					} else {
						String message = intent.getStringExtra("MESSAGE");
						InorbitLog.d(message);
					}
				}
			} catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	 public void sendSmsByManager() {
	     try {
	         // Get the default instance of the SmsManager
	         SmsManager smsManager = SmsManager.getDefault();
	         smsManager.sendTextMessage("+919029090720",
	                 null, 
	                 "New Message. What say!!!",
	                 null,
	                 null);
	         /*Toast.makeText(getApplicationContext(), "Your sms has successfully sent!",
	                 Toast.LENGTH_LONG).show();*/
	     } catch (Exception ex) {
	         /*Toast.makeText(getApplicationContext(),"Your sms has failed...",
	                 Toast.LENGTH_LONG).show();*/
	         ex.printStackTrace();
	     }
	 }

	 private class AnalyticsDataModel {
		private int id, syncStatus;
		private String udm, date, flag, activityId, mallId;
		
		public String getUdm() {
			return udm;
		}

		public void setUdm(String udm) {
			this.udm = udm;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getFlag() {
			return flag;
		}

		public void setFlag(String flag) {
			this.flag = flag;
		}

		public String getActivityId() {
			return activityId;
		}

		public void setActivityId(String activityId) {
			this.activityId = activityId;
		}

		public String getMallId() {
			return mallId;
		}

		public void setMallId(String mallId) {
			this.mallId = mallId;
		}

		public int getId() {
			return id;
		}
		
		public void setId(int id) {
			this.id = id;
		}
		
		public int getSyncStatus() {
			return syncStatus;
		}
		
		public void setSyncStatus(int syncStatus) {
			this.syncStatus = syncStatus;
		}
	}
	 
}
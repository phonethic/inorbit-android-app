package com.phonethics.inorbit;

import java.io.File;

import android.content.Context;

public class Cache {

	private static final String PICASSO_CACHE = "picasso-cache";

	public static void clearCache(Context context) {
	    final File cache = new File(
	            context.getApplicationContext().getCacheDir(),
	            PICASSO_CACHE);
	    if (cache.exists()) {
	        deleteFolder(cache);
	    }
	}
	
	private static void deleteFolder(File fileOrDirectory) {
	    if (fileOrDirectory.isDirectory()) {
	        for (File child : fileOrDirectory.listFiles())
	            deleteFolder(child);
	    }
	    fileOrDirectory.delete();
	}
}

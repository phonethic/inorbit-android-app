package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;

public class FacebookOperations {


	Context mContext1;
	private static final String TAG = "FacebookMethods";
	private static ProgressDialog progressDialog;
	private static final List<String> mListPERMISSIONS = Arrays.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private static boolean pendingPublishReauthorization = false;
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");

	//private static final String objectName = "store";

	public FacebookOperations(Context mContext){
		this.mContext1 = mContext;
	}




	/*
	 * Method use to invite friends to use your app;
	 * 
	 */
	public static void sendAppReuest(final Context mContext){
		///Toast.makeText(mContext, "Inviting Friends 1", 0).show();
		String nameSpace = mContext.getResources().getString(R.string.fb_name_space);
		final Bundle params = new Bundle();
		params.putString("message", "Watch the offers in your area. Happy Shopping!!");
		///params.putString("", value)



		Session.openActiveSession((Activity) mContext, true, new Session.StatusCallback() {

			@Override
			public void call(Session session, SessionState state,
					Exception exception) {
				// TODO Auto-generated method stub
				if(session.isOpened()) {
					WebDialog requestDialog = ( new WebDialog.RequestsDialogBuilder(mContext, Session.getActiveSession(), params))
							.setOnCompleteListener(new OnCompleteListener() {

								@Override
								public void onComplete(Bundle values, FacebookException error) {
									// TODO Auto-generated method stub
									if (error!=null) {
										if (error instanceof FacebookOperationCanceledException) {
											Toast.makeText(mContext,"Request cancelled",Toast.LENGTH_SHORT).show();
										} else {
											Toast.makeText(mContext,"Network Error",Toast.LENGTH_SHORT).show();
										}
									} else {
										final String requestId = values.getString("request");
										Log.d("Inorbit Friend Request ", "Request "+values.toString());
										if (requestId != null) {
											Toast.makeText(mContext,"Request sent",Toast.LENGTH_SHORT).show();
										} else {
											Toast.makeText(mContext,"Request cancelled",Toast.LENGTH_SHORT).show();
										}
									}

								}
							})
							//params.putString("to",  "");
							.build();
					//Toast.makeText(mContext, "Inviting Friends 2", 0).show();
					requestDialog.show();
				}
			}
		});


	}

	/**
	 * 
	 * @param mContext
	 * @param fbObj
	 * @param objectName
	 * 
	 */
	public static void shareThisInfo(final Context mContext, final FbObject fbObj, final String fbObjectName){
		final String nameSpace = mContext.getResources().getString(R.string.fb_name_space);
		//Session session = Session.getActiveSession();
		
		
		Session.openActiveSession((Activity) mContext, true, new Session.StatusCallback() {

			@Override
			public void call(Session session, SessionState state,
					Exception exception) {
				// TODO Auto-generated method stub
				if (session != null || !session.isOpened()) {
					// Check for publish permissions    
					/*List<String> permissions = session.getPermissions();
					if (!isSubsetOf(mListPERMISSIONS, permissions)) {
						pendingPublishReauthorization = true;
						Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest((Activity)mContext, mListPERMISSIONS);
						session.requestNewPublishPermissions(newPermissionsRequest);
						return;
					}*/

					// Show a progress dialog because the batch request could take a while.
					//progressDialog = ProgressDialog.show(mContext, "", "Please Wait", true);
					try {
						// Create a batch request, firstly to post a new object and
						// secondly to publish the action with the new object's id.
						RequestBatch requestBatch = new RequestBatch();

						// Set up the JSON representing the book
						JSONObject store = new JSONObject();
						store.put("title", fbObj.getobjTitle());
						store.put("image",fbObj.getobjImage());
						store.put("url",fbObj.getobjUrl());
						store.put("description", fbObj.getobjDescription());

						Bundle objectParams = new Bundle();
						objectParams.putString("object", store.toString());
						// Set up the object request callback
						Request.Callback objectCallback = new Request.Callback() {

							@Override
							public void onCompleted(Response response) {
								// Log any response error
								Log.d("", "response 1 =="+response.toString());
								FacebookRequestError error = response.getError();
								if (error != null) {
									dismissProgressDialog();
									Log.i(TAG, error.getErrorMessage());
								}
							}
						};

						// Create the request for object creation
						Request objectRequest = new Request(Session.getActiveSession(),"me/objects/"+nameSpace+":"+fbObjectName,objectParams, HttpMethod.POST, objectCallback);

						// Set the batch name so you can refer to the result
						// in the follow-on publish action request
						objectRequest.setBatchEntryName("objectCreate");

						// Add the request to the batch
						requestBatch.add(objectRequest);

						// Request: Publish action request
						// --------------------------------------------
						Bundle actionParams = new Bundle();

						// Refer to the "id" in the result from the previous batch request
						actionParams.putString(fbObjectName, "{result=objectCreate:$.id}");

						// Turn on the explicit share flag
						actionParams.putString("fb:explicitly_shared", "true");

						// Set up the action request callback
						Request.Callback actionCallback = new Request.Callback() {

							@Override
							public void onCompleted(Response response) {
								dismissProgressDialog();
								Log.d("", "response =="+response.toString());

								FacebookRequestError error = response.getError();
								if (error != null) {
									Toast.makeText(mContext,error.getErrorMessage(),Toast.LENGTH_LONG).show();
								} else {
									String actionId = null;
									try {
										JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
										actionId = graphResponse.getString("id");
									} catch (JSONException e) {
										Log.i(TAG,"JSON error "+ e.getMessage());
									}
									Toast.makeText(mContext, actionId,Toast.LENGTH_LONG).show();
								}
							}
						};

						// Create the publish action request
						Request actionRequest = new Request(Session.getActiveSession(),"me/"+nameSpace+":share",actionParams, HttpMethod.POST,actionCallback);
						// Add the request to the batch
						requestBatch.add(actionRequest);
						// Execute the batch request
						requestBatch.executeAsync();
					} catch (JSONException e) {
						Log.i(TAG,"JSON error "+ e.getMessage());
						dismissProgressDialog();
					}
				}
			}
			
			
		});
		
	}

	/** 
	 * @param mContext
	 * @param fbObj
	 * @param objectName
	 */
	public static void likeOnFb(final Context mContext,final FbObject fbObj,final String fbObjectName){
		final String nameSpace = mContext.getResources().getString(R.string.fb_name_space);
		//Session session = Session.getActiveSession();
		
		
		
		Session.openActiveSession((Activity) mContext, true, new Session.StatusCallback() {

			@Override
			public void call(Session session, SessionState state,
					Exception exception) {
				// TODO Auto-generated method stub
				if (session != null || !session.isOpened()) {
					// Check for publish permissions    
					/*List<String> permissions = session.getPermissions();
					if (!isSubsetOf(mListPERMISSIONS, permissions)) {
						pendingPublishReauthorization = true;
						Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest((Activity)mContext, mListPERMISSIONS);
						session.requestNewPublishPermissions(newPermissionsRequest); 
						return;
					}*/

					// Show a progress dialog because the batch request could take a while.
					//progressDialog = ProgressDialog.show(mContext, "", "Please Wait", true);
					try {
						// Create a batch request, firstly to post a new object and
						// secondly to publish the action with the new object's id.
						RequestBatch requestBatch = new RequestBatch();

						// Set up the JSON representing the book
						JSONObject store = new JSONObject();
						store.put("title", fbObj.getobjTitle());
						store.put("image",fbObj.getobjImage());
						store.put("url",fbObj.getobjUrl());
						store.put("description", fbObj.getobjDescription());

						Bundle objectParams = new Bundle();
						objectParams.putString("object", store.toString());
						// Set up the object request callback
						Request.Callback objectCallback = new Request.Callback() {

							@Override
							public void onCompleted(Response response) {
								// Log any response error
								Log.d("", "response 1 =="+response.toString());
								FacebookRequestError error = response.getError();
								if (error != null) {
									dismissProgressDialog();
									Log.i(TAG, error.getErrorMessage());
								}
							}
						};

						// Create the request for object creation
						Request objectRequest = new Request(Session.getActiveSession(),"me/objects/"+nameSpace+":"+fbObjectName,objectParams, HttpMethod.POST, objectCallback);

						// Set the batch name so you can refer to the result
						// in the follow-on publish action request
						objectRequest.setBatchEntryName("objectCreate");

						// Add the request to the batch
						requestBatch.add(objectRequest);

						// Request: Publish action request
						// --------------------------------------------
						Bundle actionParams = new Bundle();

						// Refer to the "id" in the result from the previous batch request
						actionParams.putString("object", "{result=objectCreate:$.id}");

						// Turn on the explicit share flag
						actionParams.putString("fb:explicitly_shared", "true");

						// Set up the action request callback
						Request.Callback actionCallback = new Request.Callback() {

							@Override
							public void onCompleted(Response response) {
								dismissProgressDialog();
								Log.d("", "response =="+response.toString());

								FacebookRequestError error = response.getError();
								if (error != null) {
									Toast.makeText(mContext,error.getErrorMessage(),Toast.LENGTH_LONG).show();
								} else {
									String actionId = null;
									try {
										JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
										actionId = graphResponse.getString("id");
									} catch (JSONException e) {
										Log.i(TAG,"JSON error "+ e.getMessage());
									}
									Toast.makeText(mContext, actionId,Toast.LENGTH_LONG).show();
								}
							}
						};

						// Create the publish action request
						Request actionRequest = new Request(Session.getActiveSession(),"me/og.likes", actionParams, HttpMethod.POST,actionCallback);

						// Add the request to the batch
						requestBatch.add(actionRequest);

						// Execute the batch request
						requestBatch.executeAsync();
					} catch (JSONException e) {
						Log.i(TAG,"JSON error "+ e.getMessage());
						dismissProgressDialog();
					}
				}
			}
			
			
		});
		
	}





	private static void dismissProgressDialog() {
		// Dismiss the progress dialog
		if (progressDialog != null) { 
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	private static boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}


}

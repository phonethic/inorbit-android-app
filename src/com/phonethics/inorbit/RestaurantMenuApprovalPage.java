package com.phonethics.inorbit;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.RequestTags;
import com.phonethics.model.RestaurantMenuModel;
import com.phonethics.model.RestaurantsForMall;
import com.squareup.picasso.Picasso;

public class RestaurantMenuApprovalPage extends SherlockFragmentActivity {
	private Activity mContext;
	private ActionBar mActionBar;
	private PagerSlidingTabStrip pagerTabs;
	private JazzyViewPager mPager;
	private ArrayList<String> pages;
	private ProgressBar pBar;
	private static final String sStrPage1 = "Approved";
	private static final String sStrPage2 = "Pending";
	private static final String sStrPage3 = "Rejected";
	private ListView listOfItems;
	private ArrayList<RestaurantsForMall> restaurantsForMallManager;
	private ArrayList<RestaurantMenuModel> menuImages;
	private boolean isMallManager;
	private DBUtil dbUtil;
	private String storeId, storePlaceParent;
	private MallManagerRestaurants mallManagerRestaurants;
	private AllMenuImages allMenuImages;
	private DeleteMenuImage deleteMenuImage;
	private UpdateMenuStatus updateMenuStatus;
	private Dialog operationDialog;
	private PageAdapter adapter;
	
	@Override  
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_post_new);
		mContext = this;
		dbUtil = new DBUtil(mContext);
		isMallManager = dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn();
		
		initViews();
		getBundleValues();
		getAllImages();
	}
	
	@Override
	protected void onResume() {
		IntentFilter intentFilter = new IntentFilter(RequestTags.TAG_GET_MENU_FOR_MALL);
		mContext.registerReceiver(mallManagerRestaurants = new MallManagerRestaurants(), intentFilter);
		
		IntentFilter filter = new IntentFilter(RequestTags.TAG_GET_MENU_FOR_STORE);
		mContext.registerReceiver(allMenuImages = new AllMenuImages(), filter);
		
		IntentFilter deleteImgFilter = new IntentFilter(RequestTags.TAG_DELETE_MENU_IMAGES);
		mContext.registerReceiver(deleteMenuImage = new DeleteMenuImage(), deleteImgFilter);
		
		IntentFilter updateMenuStatusFilter = new IntentFilter(RequestTags.TAG_UPDATE_MENU_IMAGE_STATUS); 
		mContext.registerReceiver(updateMenuStatus = new UpdateMenuStatus(), updateMenuStatusFilter);
		super.onResume();
	}
	
	protected void onStop() {
		try {
			if(mallManagerRestaurants!=null){
				mContext.unregisterReceiver(mallManagerRestaurants);	
			}
			
			if(allMenuImages!=null){
				mContext.unregisterReceiver(allMenuImages);
			}
			
			if (deleteMenuImage!=null) {
				mContext.unregisterReceiver(deleteMenuImage);
			}
			
			if (updateMenuStatus!=null) {
				mContext.unregisterReceiver(updateMenuStatus);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		super.onStop();
	}
	
	private class MallManagerRestaurants extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent.getStringExtra("SUCCESS").equalsIgnoreCase("true")) {
					restaurantsForMallManager  = intent.getParcelableArrayListExtra("MENUS");
					if (adapter==null) {
						adapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, pages);
						mPager.setAdapter(adapter);
					} else {
						int page = mPager.getCurrentItem();
						adapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, pages);
						mPager.setAdapter(adapter);
						mPager.setCurrentItem(page);
					}
					mPager.setCurrentItem(1);
					mPager.setOffscreenPageLimit(pages.size());
					pagerTabs.setViewPager(mPager);
					pagerTabs.setTextColor(Color.BLACK);
					pagerTabs.setBackgroundColor(Color.TRANSPARENT);
				} else {
					showToast("No Menus Found");
					if (restaurantsForMallManager != null) {
						restaurantsForMallManager.clear();
						int position = mPager.getCurrentItem();
						adapter = new PageAdapter(getSupportFragmentManager(), (Activity)context, pages);
						mPager.setAdapter(adapter);
						mPager.setCurrentItem(position);
					}
				}
			} else {
				showToast("Some Error was generated");
			}
			pBar.setVisibility(View.GONE);
		}
	}
	
	private void showToast(String msg) {
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		setResult(RESULT_OK);
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}
	
	private class AllMenuImages extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			pBar.setVisibility(View.GONE);
			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						menuImages = intent.getParcelableArrayListExtra("MENUS");
						if (adapter == null) {
							adapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, pages);
							mPager.setAdapter(adapter);
						} else {
							//adapter.notifyDataSetChanged();
							int page = mPager.getCurrentItem();
							adapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, pages);
							mPager.setAdapter(adapter);
							mPager.setCurrentItem(page);
						}
						mPager.setOffscreenPageLimit(pages.size());
						pagerTabs.setViewPager(mPager);
						pagerTabs.setTextColor(Color.BLACK);
						pagerTabs.setBackgroundColor(Color.TRANSPARENT);
					} else {
						String message = intent.getStringExtra("MESSAGE");
						showToast("No menus Found");
						if (menuImages != null) {
							menuImages.clear();
							int position = mPager.getCurrentItem();
							adapter = new PageAdapter(getSupportFragmentManager(), (Activity)context, pages);
							mPager.setAdapter(adapter);
							mPager.setCurrentItem(position);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	void initViews() {
		mActionBar	=	getSupportActionBar();
		mActionBar.setTitle("Manage Menu");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();
		
		pBar = (ProgressBar) findViewById(R.id.pBar_view_post);
		pagerTabs = (PagerSlidingTabStrip) findViewById(R.id.pagerTabs_viewtabs);
		mPager=(JazzyViewPager)findViewById(R.id.viewPostPager);
		pages = new ArrayList<String>();
		pages.add(sStrPage1);
		pages.add(sStrPage2);
		pages.add(sStrPage3);
	}
	
	private void getBundleValues() {
		Bundle bundle = getIntent().getExtras();
		if(bundle != null) {
			storeId = getIntent().getStringExtra("STORE_ID");
			storePlaceParent = getIntent().getStringExtra("PLACE_PARENT");
		}
	}
	
	private void getAllImages() {
		if (isMallManager) {
			if(new NetworkCheck(mContext).isNetworkAvailable()) {
				MyClass myClass = new MyClass(mContext);
				
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("mall_id", dbUtil.getPlaceIdByPlaceParent("0")));
				
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
				myClass.getStoreRequest(RequestTags.TAG_GET_MENU_FOR_MALL, nameValuePairs, headers);
			} else {
				Toast.makeText(mContext, "No Internet Connection. Please try again after some time.", Toast.LENGTH_SHORT).show();
			}
		} else {
			if(new NetworkCheck(mContext).isNetworkAvailable()) {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
				
				List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("place_id", storeId));
				nameValuePairs.add(new BasicNameValuePair("mall_id", storePlaceParent));
				MyClass myClass = new MyClass(mContext);
				myClass.getStoreRequest(RequestTags.TAG_GET_MENU_FOR_STORE, nameValuePairs, headers);
			}else{
				Toast.makeText(mContext, "No Internet Connection. Please try again after some time.", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	//Creating Pages with PageAdapter.
	public class PageAdapter extends FragmentStatePagerAdapter{
		Activity context;
		ArrayList<String> pages;
		
		public PageAdapter(FragmentManager fm,Activity context,ArrayList<String> pages) {
			super(fm);
			this.context=context;
			this.pages=pages;
		}
		
		@Override
		public String getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return new PageFragment(pages.get(position), context);
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return pages.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Object obj = super.instantiateItem(container, position);
			mPager.setObjectForPosition(obj, position);
			return obj;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if(object != null) {
				return ((Fragment)object).getView() == view;
			} else {
				return false;
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			
		}
	}
	
	@SuppressLint("ValidFragment")
	public class PageFragment extends Fragment {
		String title;
		Context context;
		View view;
		ArrayList<RestaurantsForMall> restaurantsForMall = new ArrayList<RestaurantsForMall>();
		ArrayList<RestaurantMenuModel> restaurantsMenu = new ArrayList<RestaurantMenuModel>();
		
		public PageFragment(){
			
		}
		
		@SuppressLint("ValidFragment")
		public PageFragment(String title,Activity context){
			this.title=title;
			this.context=context;
		}
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setRetainInstance(true);
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			super.onCreateView(inflater, container, savedInstanceState);
			
			view = inflater.inflate(R.layout.list_view_post, null);
			
			listOfItems = (ListView) view.findViewById(R.id.listView_view_post);
			
			if(title.equalsIgnoreCase(sStrPage1)) {
				if (isMallManager) {
					restaurantsForMall = getApprovedRestaurantsArrayList(1);
					listOfItems.setAdapter(new MallManagerListAdapter((Activity) context, 0, restaurantsForMall));
				} else {
					restaurantsMenu = getRestaurantsMenu(1);
					listOfItems.setAdapter(new MenuListAdapter((Activity) context, 0, restaurantsMenu));
				}
				
				listOfItems.setOnItemClickListener(new OnItemClickListener() {
					
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
						if (isMallManager) {
							/*ArrayList<String> PHOTO_SOURCE = new ArrayList<String>();
							for (int i=0; i<restaurantsForMall.size(); i++) {
								PHOTO_SOURCE.add(restaurantsForMall.get(i).getFirstMenuImage());
							}
							Intent intent=new Intent(context,StorePagerGallery.class);
							intent.putExtra("photo_source", PHOTO_SOURCE);
							intent.putExtra("position", position);*/
							Intent intent = new Intent(context, RestaurantMenuGridDisplayActivity.class);
							intent.putExtra("place_id", restaurantsForMall.get(position).getPlaceId());
							intent.putExtra("mall_id", restaurantsForMall.get(position).getMallId());
							intent.putExtra("status", "1");
							startActivity(intent);
						} else {
							ArrayList<String> PHOTO_SOURCE = new ArrayList<String>();
							for (int i=0; i<restaurantsMenu.size(); i++) {
								PHOTO_SOURCE.add(restaurantsMenu.get(i).getImage_url());
							}
							Intent intent=new Intent(context,StorePagerGallery.class);
							intent.putExtra("photo_source", PHOTO_SOURCE);
							intent.putExtra("position", position);
							startActivity(intent);
						}
					}
				});
				
				listOfItems.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int position, long arg3) {
						
						if (!isMallManager) {
							operationDialog  = new Dialog(mContext);
							operationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
							operationDialog.setContentView(R.layout.approve_item_dialog);
							operationDialog.setTitle(null);
							ListView listCateg = (ListView)operationDialog.findViewById(R.id.categDilogList);
							ArrayList<String> 	operationArr = new ArrayList<String>();
							operationArr.add("Delete");
							
							listCateg.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, operationArr));
							listCateg.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
									try{
										operationDialog.dismiss();
										if (pos == 0) {
											try {
												EventTracker.logEvent(context.getResources().getString(R.string.manageMenu_Delete), true);
											} catch(Exception e) {}
											sendToDelete(restaurantsMenu.get(position));
										}
									} catch (Exception e) {
									}
								}
							});
							operationDialog.show();
						}
						return true;
					}
				});
			} else if(title.equalsIgnoreCase(sStrPage2)) {
				
				if (isMallManager) {
					restaurantsForMall = getApprovedRestaurantsArrayList(0);
					listOfItems.setAdapter(new MallManagerListAdapter((Activity) context, 0, restaurantsForMall));
				} else {
					restaurantsMenu = getRestaurantsMenu(0);
					listOfItems.setAdapter(new MenuListAdapter((Activity) context, 0, restaurantsMenu));
				}
				
				listOfItems.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
						if (isMallManager) {
							Intent intent = new Intent(context, RestaurantMenuGridDisplayActivity.class);
							intent.putExtra("place_id", restaurantsForMall.get(position).getPlaceId());
							intent.putExtra("mall_id", restaurantsForMall.get(position).getMallId());
							intent.putExtra("status", "0");
							startActivity(intent);
						} else {
							ArrayList<String> PHOTO_SOURCE = new ArrayList<String>();
							for (int i=0; i<restaurantsMenu.size(); i++) {
								PHOTO_SOURCE.add(restaurantsMenu.get(i).getImage_url());
							}
							Intent intent=new Intent(context,StorePagerGallery.class);
							intent.putExtra("photo_source", PHOTO_SOURCE);
							intent.putExtra("position", position);
							startActivity(intent);
						}
					}
				});
				listOfItems.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, final int position, long arg3) {
						if (isMallManager) {
							showApproveDialog(position, restaurantsForMall);
						} else {
							operationDialog  = new Dialog(mContext);
							operationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
							operationDialog.setContentView(R.layout.approve_item_dialog);
							operationDialog.setTitle(null);
							ListView listCateg = (ListView)operationDialog.findViewById(R.id.categDilogList);
							ArrayList<String> 	operationArr = new ArrayList<String>();
							operationArr.add("Delete");
							listCateg.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, operationArr));
							listCateg.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
										long arg3) {
									// TODO Auto-generated method stub
									try{
										operationDialog.dismiss();
										if (pos == 0) {
											try {
												EventTracker.logEvent(context.getResources().getString(R.string.manageMenu_Delete), true);
											} catch (Exception ew) {}
											sendToDelete(restaurantsMenu.get(position));
										}
									} catch (Exception e) {
									}
								}
							});
							operationDialog.show();
						}
						return true;
					}
				});
			} else if(title.equalsIgnoreCase(sStrPage3)) {
				
				if (isMallManager) {
					restaurantsForMall = getApprovedRestaurantsArrayList(2);
					listOfItems.setAdapter(new MallManagerListAdapter((Activity) context, 0, restaurantsForMall));
				} else {
					restaurantsMenu = getRestaurantsMenu(2);
					listOfItems.setAdapter(new MenuListAdapter((Activity) context, 0, restaurantsMenu));
				}
				
				listOfItems.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						if (isMallManager) {
							Intent intent = new Intent(context, RestaurantMenuGridDisplayActivity.class);
							intent.putExtra("place_id", restaurantsForMall.get(position).getPlaceId());
							intent.putExtra("mall_id", restaurantsForMall.get(position).getMallId());
							intent.putExtra("status", "2");
							startActivity(intent);
						} else {
							ArrayList<String> PHOTO_SOURCE = new ArrayList<String>();
							for (int i=0; i<restaurantsMenu.size(); i++) {
								PHOTO_SOURCE.add(restaurantsMenu.get(i).getImage_url());
							}
							Intent intent=new Intent(context,StorePagerGallery.class);
							intent.putExtra("photo_source", PHOTO_SOURCE);
							intent.putExtra("position", position);
							startActivity(intent);
						}
					}
				});
				
				listOfItems.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int position, long arg3) {
						
						if (!isMallManager) {
							operationDialog  = new Dialog(mContext);
							operationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
							operationDialog.setContentView(R.layout.approve_item_dialog);
							operationDialog.setTitle(null);
							ListView listCateg = (ListView)operationDialog.findViewById(R.id.categDilogList);
							ArrayList<String> 	operationArr = new ArrayList<String>();
							operationArr.add("Delete");
							listCateg.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, operationArr));
							listCateg.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> arg0, View arg1, int itemPos,
										long arg3) {
									// TODO Auto-generated method stub
									try{
										operationDialog.dismiss();
										if (itemPos == 0) {
											try {
												EventTracker.logEvent(context.getResources().getString(R.string.manageMenu_Delete), true);
											} catch (Exception e) {
												
											}
											sendToDelete(restaurantsMenu.get(position));
										}
									} catch (Exception e) {
									}
								}
							});
							operationDialog.show();
						}
						return true;
					}
				});
			}
			return view;
		}
	}
	
	private void sendToDelete(RestaurantMenuModel deleteMenu) {
		JSONObject jsonObj = new JSONObject();
		try {
			JSONArray imgArray = new JSONArray();
			imgArray.put(deleteMenu.getId());
			jsonObj.putOpt("image_id", imgArray);

			pBar.setVisibility(View.VISIBLE);
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			
			MyClass myClass = new MyClass(mContext);
			myClass.postRequest(RequestTags.TAG_DELETE_MENU_IMAGES, headers, jsonObj);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public ArrayList<RestaurantsForMall> getApprovedRestaurantsArrayList(int status){
		ArrayList<RestaurantsForMall> restaurantsForMall = new ArrayList<RestaurantsForMall>();
		for(int i=0;i<restaurantsForMallManager.size();i++) {
			if(restaurantsForMallManager.get(i).getStatus().equalsIgnoreCase(""+status)){
				restaurantsForMall.add(restaurantsForMallManager.get(i));
			}
		}
		return restaurantsForMall;
	}
	
	public void showApproveDialog(final int listPosition,final ArrayList<RestaurantsForMall> tempArr){
		operationDialog  = new Dialog(mContext);
		operationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		operationDialog.setContentView(R.layout.approve_item_dialog);
		operationDialog.setTitle(null);
		ListView listCateg = (ListView)operationDialog.findViewById(R.id.categDilogList);
		ArrayList<String> 	operationArr = new ArrayList<String>();
		operationArr.add("Approve");
		operationArr.add("Reject");
		listCateg.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, operationArr));
		listCateg.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				try{
					try {
						if (pos==0)
							EventTracker.logEvent(mContext.getResources().getString(R.string.manageMenu_Approved), true);
						else 
							EventTracker.logEvent(mContext.getResources().getString(R.string.manageMenu_Rejected), true);
					} catch (Exception e) {}
					operationDialog.dismiss();
					showConfirmationDialog(tempArr.get(listPosition).getPlaceId() ,pos==0 ? "1":"2");
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		});
		operationDialog.show();

	}
	
	protected void showConfirmationDialog(final String placeId, final String itemClicked) {
		AlertDialog.Builder builder = 
				new AlertDialog.Builder(this)
				.setMessage("Are you sure you want to " + (itemClicked.equalsIgnoreCase("1") ? "approve":"reject") + " this menu?")
				.setPositiveButton("No", new Dialog.OnClickListener() {
			
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
					
				})
				.setNegativeButton("Yes", new Dialog.OnClickListener() {
			
					@Override
					public void onClick(DialogInterface dialog, int which) {
						updateStatusForRestaurant(placeId , itemClicked);
					}
					
				});
		builder.create().show();
	}

	private ArrayList<RestaurantMenuModel> getRestaurantsMenu(int status) {
		ArrayList<RestaurantMenuModel> menuStatusImages = new ArrayList<RestaurantMenuModel>();
		for(int i=0;i<menuImages.size();i++) {
			if(menuImages.get(i).getStatus().equalsIgnoreCase(""+status)){
				menuStatusImages.add(menuImages.get(i));
			}
		}
		
		return menuStatusImages;
	} 
	
	private void updateStatusForRestaurant(String place_id, String newStatus) {
		if(new NetworkCheck(mContext).isNetworkAvailable()) {
			MyClass myClass = new MyClass(mContext);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("mall_id", dbUtil.getPlaceIdByPlaceParent("0")));
			nameValuePairs.add(new BasicNameValuePair("status", newStatus));
			nameValuePairs.add(new BasicNameValuePair("place_id", place_id));
			
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			
			myClass.putRequest(RequestTags.TAG_UPDATE_MENU_IMAGE_STATUS, nameValuePairs, headers, new JSONObject());
		} else {
			Toast.makeText(mContext, "No Internet Connection. Please try again after some time.", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class MallManagerListAdapter extends ArrayAdapter<RestaurantsForMall> {
		Activity context;
		ArrayList<RestaurantsForMall> restaurants;
		LayoutInflater inflater;
		
		public MallManagerListAdapter(Activity context, int resource,
				ArrayList<RestaurantsForMall> objects) {
			super(context, resource, objects);
			this.context = context;
			this.restaurants = objects;
			inflater = context.getLayoutInflater();
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			if (convertView == null) {
				ViewHolder holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.layout_place_chooser, null);
				(convertView.findViewById(R.id.band)).setVisibility(View.GONE);
				
				holder.storeName=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.storeLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.storeLogo.setScaleType(ScaleType.FIT_CENTER);
				holder.date=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc); 
				
				holder.storeName.setTypeface(InorbitApp.getTypeFaceTitle());
				holder.date.setTypeface(InorbitApp.getTypeFace());
				holder.date.bringToFront();
				
				holder.storeLogo.setScaleType(ScaleType.CENTER_INSIDE);
				holder.storeName.setTypeface(InorbitApp.getTypeFaceTitle());
				holder.date.setTypeface(InorbitApp.getTypeFaceTitle());
				holder.date.setTextSize(12);
				convertView.setTag(holder);
			}
			
			ViewHolder holder = (ViewHolder) convertView.getTag();
			holder.storeName.setText(restaurants.get(position).getStoreName());
			
			String msOldFormat="yyyy-MM-dd H:mm:ss";
			String msNewFormat = "dd MMM yyyy  h:mm a";
			String msNewDate = "";

			try{
				SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
				Date d = sdf.parse(restaurants.get(position).getMenuUploadDate());
				sdf.applyPattern(msNewFormat);
				msNewDate = sdf.format(d);
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
			holder.date.setText(msNewDate);
			
			String photo_source=restaurants.get(position).getStoreLogo().toString().replaceAll(" ", "%20");
			try {
				Picasso.with(context).load(getResources().getString(R.string.photo_url)+photo_source)
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(holder.storeLogo);
			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			} catch(Exception e){
				e.printStackTrace();
			}
			return convertView;
		}
		
		private class ViewHolder {
			private ImageView storeLogo;
			private TextView storeName, date;
		}
	}
	
	private class MenuListAdapter extends ArrayAdapter<RestaurantMenuModel> {
		Activity context;
		ArrayList<RestaurantMenuModel> restaurants;
		LayoutInflater inflater;
		
		public MenuListAdapter(Activity context, int resource, ArrayList<RestaurantMenuModel> objects) {
			super(context, resource, objects);
			this.context = context;
			this.restaurants = objects;
			inflater = context.getLayoutInflater();
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			if (convertView == null) {
				ViewHolder holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.layout_place_chooser, null);
				(convertView.findViewById(R.id.band)).setVisibility(View.GONE);
				
				holder.storeName=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.storeLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.storeLogo.setScaleType(ScaleType.FIT_CENTER);
				holder.date=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc); 
				
				holder.storeName.setTypeface(InorbitApp.getTypeFaceTitle());
				holder.date.setTypeface(InorbitApp.getTypeFace());
				holder.date.bringToFront();
				
				holder.storeLogo.setScaleType(ScaleType.CENTER_INSIDE);
				holder.storeName.setTypeface(InorbitApp.getTypeFaceTitle());
				holder.date.setTypeface(InorbitApp.getTypeFaceTitle());
				holder.date.setTextSize(12);
				convertView.setTag(holder);
			}
			
			ViewHolder holder = (ViewHolder) convertView.getTag();
			String status = restaurants.get(position).getStatus();
			if (status.equalsIgnoreCase("1")) {
				holder.storeName.setText("Approved");
				holder.storeName.setTextColor(getResources().getColor(R.color.inorbit_green));
			} else if (status.equalsIgnoreCase("0")) {
				holder.storeName.setText("Pending");
				holder.storeName.setTextColor(getResources().getColor(R.color.inorbit_blue));
			} else if (status.equalsIgnoreCase("2")) {
				holder.storeName.setText("Rejected");
				holder.storeName.setTextColor(getResources().getColor(R.color.inorbit_red));
			}
			
			String msOldFormat="yyyy-MM-dd H:mm:ss";
			String msNewFormat = "dd MMM yyyy  h:mm a";
			String msNewDate = "";

			try{
				SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
				Date d = sdf.parse(restaurants.get(position).getDate());
				sdf.applyPattern(msNewFormat);
				msNewDate = sdf.format(d);
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
			
			holder.date.setText(msNewDate);
			
			String photo_source=restaurants.get(position).getThumb_url().toString().replaceAll(" ", "%20");
			try {
				Picasso.with(context).load(getResources().getString(R.string.photo_url)+photo_source)
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(holder.storeLogo);
			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			} catch(Exception e){
				e.printStackTrace();
			}
			return convertView;
		}
		
		private class ViewHolder {
			private ImageView storeLogo;
			private TextView storeName, date;
		}
	}
	
	private class DeleteMenuImage extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("false")){
						String message = intent.getStringExtra("MESSAGE");
						showToast(message);
						pBar.setVisibility(View.GONE);
					}
					getAllImages();
				} else {
					showToast("Error while deleting");
					pBar.setVisibility(View.GONE);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private class UpdateMenuStatus extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("false")) {
						String message = intent.getStringExtra("MESSAGE");
						showToast(message);
						pBar.setVisibility(View.GONE);
					} else {
						showToast("Your request has been processed");
					}
					getAllImages();
				} else {
					showToast("Error while deleting");
					pBar.setVisibility(View.GONE);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

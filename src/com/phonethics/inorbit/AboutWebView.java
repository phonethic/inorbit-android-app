package com.phonethics.inorbit;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;


public class AboutWebView extends SherlockActivity {

	WebView aboutWeb;
	ActionBar actionBar;
	ProgressBar aboutWebProg;
	String url;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_web_view);


		actionBar=getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		aboutWeb=(WebView)findViewById(R.id.aboutWeb);

		aboutWebProg=(ProgressBar)findViewById(R.id.aboutWebProg);
		aboutWeb.setWebViewClient(new MyWebViewClient());

		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			url=b.getString("url");
			if(url.equalsIgnoreCase("PrivacyPolicy")){
				actionBar.setTitle("Privacy Policy");
				aboutWeb.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.privacy_url));
				Log.d("", "Inorbit PPurl>> "+getResources().getString(R.string.base_url)+getResources().getString(R.string.privacy_url));
			}
			if(url.equalsIgnoreCase("Terms")){
				actionBar.setTitle("Terms & Conditions");
				aboutWeb.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.tcURl));
				Log.d("", "Inorbit TCurl>> "+getResources().getString(R.string.base_url)+getResources().getString(R.string.tcURl));
			}
			
			if(url.equalsIgnoreCase("Faq")){
				actionBar.setTitle("FAQ");
				aboutWeb.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.faq_url));
				Log.d("", "Inorbit TCurl>> "+getResources().getString(R.string.base_url)+getResources().getString(R.string.faq_url));
			}
			
		}


	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {


			Log.d("", "Inorbit url>> "+url);
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onLoadResource(WebView  view, String  url){

		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			aboutWebProg.setVisibility(View.GONE);
			super.onPageFinished(view, url);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			aboutWebProg.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			// TODO Auto-generated method stub
			aboutWebProg.setVisibility(View.GONE);
			super.onReceivedError(view, errorCode, description, failingUrl);
		}
	}    

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishTo();
		return true;
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}

	void finishTo()
	{
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}

}

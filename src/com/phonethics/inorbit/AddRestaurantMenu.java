package com.phonethics.inorbit;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.RequestTags;
import com.phonethics.model.RestaurantMenuModel;
import com.squareup.picasso.Picasso;

public class AddRestaurantMenu extends SherlockFragmentActivity {
	private Activity context;
	private ActionBar actionBar;
	private TextView addMenu, editMenu, reorderMenu, deleteMenu, cancelDelete;
	private LinearLayout editLayout, deleteLayout;
	private GridView imageGrid;
	private ProgressBar progress;
	NetworkCheck isnetConnected;
	private static final int REQUEST_CODE_SELECT_PICTURE_GALLERY = 11;
	private static final int REQUEST_CODE_REORDER_GALLERY = 5;
	private String FILE_TYPE, FILE_NAME, FILE_PATH;
	private String user_id, auth_id, storeId, storePlaceParent, storeName, storeLogoUrl;
	private UploadMenu uploadMenu;
	//private Bitmap thumbnail;
	private AllMenuImages reciverObj;
	ArrayList<RestaurantMenuModel> mMenuModel  = null;
	GridAdapter gridAdapter;
	String PHOTO_URL = "";
	Activity aContext;
	boolean showEditMode = false;
	static ArrayList<String> IDSTODELETE = new ArrayList<String>();
	DeleteMenuImages reciverObjDelete;
	JSONObject json = new JSONObject();
	boolean rotateImage = false;
	int angularRotation = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_menu_layout);
		context=this;
		aContext=this;
		initViews();
		PHOTO_URL = getResources().getString(R.string.photo_url);
		receiveBundleValues();
		
		//to get all of the images for Menus
		getMenuFromServer();

		editMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mMenuModel == null || mMenuModel.size() == 0) {
					showToast(getResources().getString(R.string.no_menu_available_yet));
					return;
				}
				editLayout.setVisibility(View.GONE);
				deleteLayout.setVisibility(View.VISIBLE);
				showEditMode = true;
				//showToast(""+showEditMode);
				if(mMenuModel!=null){
					gridAdapter = new GridAdapter(aContext, R.drawable.ic_launcher, mMenuModel);
					imageGrid.setAdapter(gridAdapter);	
				}
				
			}
		});

		cancelDelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				IDSTODELETE.clear();
				editLayout.setVisibility(View.VISIBLE);
				deleteLayout.setVisibility(View.GONE);
				showEditMode = false;
				//showToast(""+showEditMode);
				if(mMenuModel!=null){
					gridAdapter = new GridAdapter(aContext, R.drawable.ic_launcher, mMenuModel);
					imageGrid.setAdapter(gridAdapter);	
				}
				
			}
		});
		
		deleteMenu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EventTracker.logEvent(context.getResources().getString(R.string.addMenu_ImageDelete), true);
				if(IDSTODELETE.size()>0){
					AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
					alertDialogBuilder3.setTitle("Inorbit");
					alertDialogBuilder3
					.setMessage(getResources().getString(R.string.deleteGalleryImage))
					.setCancelable(false)
					.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

//							for(int i=0;i<IDSTODELETE.size();i++){
//
//								Log.d("IDSTODELETE", "IDSTODELETE " + IDSTODELETE.get(i));
//
//								URLTOAPPEND = URLTOAPPEND + IDSTODELETE.get(i);  
//
//								if(i!=IDSTODELETE.size()-1){
//									URLTOAPPEND = URLTOAPPEND + ",";
//								}
//
//
//
//							}
//							Log.d("URLTOSEND","URLTOSEND "  +Separator + URLTOAPPEND);
//							url_ids = Separator + URLTOAPPEND;

							JSONObject jsonObj = new JSONObject();
							try {
								JSONArray imgArray = new JSONArray();
								for(int i=0;i<IDSTODELETE.size();i++){
									imgArray.put(IDSTODELETE.get(i));
								}
								jsonObj.putOpt("image_id", imgArray);
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							
							InorbitLog.d("JSON " + jsonObj.toString());
							
							if(isnetConnected.isNetworkAvailable()){
								callDeleteImage(jsonObj);	
							}else{
								showToast(getResources().getString(R.string.noInternetConnection));
							}

						}


					})
					.setNegativeButton("No",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});;

					AlertDialog alertDialog3 = alertDialogBuilder3.create();
					alertDialog3.show();



				}
				else{
					showToast(getResources().getString(R.string.deleteImageValidation));
				}
			}
		});
		
		imageGrid.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				ArrayList<String> PHOTO_SOURCE = new ArrayList<String>();
				for (int i=0; i<mMenuModel.size(); i++) {
					PHOTO_SOURCE.add(mMenuModel.get(i).getImage_url());
				}
				Intent intent=new Intent(context, StorePagerGallery.class);
				intent.putExtra("photo_source", PHOTO_SOURCE);
				intent.putExtra("position", position);
				startActivity(intent);
			}
		});
		
		reorderMenu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EventTracker.logEvent(context.getResources().getString(R.string.addMenu_ImageReorder), true);
				
				if (mMenuModel == null || mMenuModel.size() == 0) {
					showToast(getResources().getString(R.string.no_menu_available_yet));
					return;
				}
				
				Intent intent = new Intent(AddRestaurantMenu.this, ReorderActivity.class);
				intent.putExtra("all_stores", mMenuModel);
				startActivityForResult(intent, REQUEST_CODE_REORDER_GALLERY);
			}
		});
	}
	
	

	@Override
	protected void onResume() {
		super.onResume();

		IntentFilter intentFilter = new IntentFilter(RequestTags.TAG_UPLOAD_MENU);
		context.registerReceiver(uploadMenu = new UploadMenu(), intentFilter);
		
		IntentFilter filter = new IntentFilter(RequestTags.TAG_GET_MENU_FOR_STORE);
		context.registerReceiver(reciverObj = new AllMenuImages(), filter);
		

		IntentFilter filterDelete = new IntentFilter(RequestTags.TAG_DELETE_MENU_IMAGES);
		context.registerReceiver(reciverObjDelete = new DeleteMenuImages(), filterDelete);
	}
	
	private void initViews() {
		/**
		 * Initialize the action bar
		 */
		actionBar =	getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		editLayout	= (LinearLayout) findViewById(R.id.editLayout);
		editMenu = (TextView)findViewById(R.id.editMenu);
		addMenu = (TextView) findViewById(R.id.addMenu);
		reorderMenu = (TextView) findViewById(R.id.reorderMenu);
		deleteLayout = (LinearLayout) findViewById(R.id.deleteLayout);
		deleteMenu = (TextView) findViewById(R.id.deleteMenu);
		cancelDelete = (TextView) findViewById(R.id.cancelDelete);
		isnetConnected		= new NetworkCheck(context);
		//Gridview
		imageGrid				= (GridView)findViewById(R.id.imageGrid);
		progress = (ProgressBar) findViewById(R.id.progress);

		addMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				EventTracker.logEvent(context.getResources().getString(R.string.addmenu_ImageAdd), true);
				selectPictureForGallery();
			}
		});
	}
	
	//select image gallery
	private void selectPictureForGallery(){
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_SELECT_PICTURE_GALLERY);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == REQUEST_CODE_SELECT_PICTURE_GALLERY){
			InorbitLog.d("REQUEST_CODE_SELECT_PICTURE_GALLERY");
			if(resultCode == Activity.RESULT_OK && data != null){
				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";
				
				try {
					Uri imageUri = data.getData();
					angularRotation = getOrientation(data.getData());
					if (angularRotation != 0)
						rotateImage = true;
					else 
						rotateImage = false;
					Back t = new Back();
					t.execute(imageUri);
				}catch(Exception e){
					InorbitLog.e("Exception at Create broacast class");
					e.printStackTrace();
				}
			}
		} else if (requestCode == REQUEST_CODE_REORDER_GALLERY) {
			getMenuFromServer();
		}
	}
	
	public int getOrientation(Uri selectedImage) {
	    int orientation = 0;
	    final String[] projection = new String[]{MediaStore.Images.Media.ORIENTATION};      
	    final Cursor cursor = context.getContentResolver().query(selectedImage, projection, null, null, null);
	    if(cursor != null) {
	        final int orientationColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION);
	        if(cursor.moveToFirst()) {
	            orientation = cursor.isNull(orientationColumnIndex) ? 0 : cursor.getInt(orientationColumnIndex);
	        }
	        cursor.close();
	    }
	    return orientation;
	}
	
	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(
				getContentResolver().openInputStream(selectedImage), null, o);

		final int REQUIRED_SIZE = 5120;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}
		
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(
				getContentResolver().openInputStream(selectedImage), null, o2);
	}
	
	class Back extends AsyncTask<Uri, String, Bitmap> {
		
		@Override
		protected void onPreExecute() {
			progress.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}
		
		@Override
		protected Bitmap doInBackground(Uri... params) {
			/*try {
				thumbnail = decodeUri(params[0]);
				publishProgress("decoding successful");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			int bytes = thumbnail.getRowBytes() * thumbnail.getHeight();
			publishProgress("thumbnail obtained");
			
			ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
			thumbnail.copyPixelsToBuffer(buffer); //Move the byte data to the buffer
			publishProgress("Copy Pixels done");
			
			byte[] array = buffer.array();
			publishProgress("Array Obtained");*/
			try {
				String encodedpath="";
				
				/*byte [] b=imageTobyteArray(FILE_PATH,480,480);*/
				//progress.setVisibility(View.VISIBLE);
				//if(array!=null) {
					SessionManager session = new SessionManager(context);
					
					json.put("place_id", storeId);
					json.put("mall_id", storePlaceParent);
					if (session.isMallManagerLoggedin()) 
						json.put("status", 1+"");
					else
						json.put("status", 0+"");
					json.put("type", "Photo");
					json.put("filename", "menu.jpg");
					json.put("filetype", "image/jpeg");
					json.put("user_id", user_id);
					json.put("auth_id", auth_id);
					//CameraImageSave cameraSaveImage = new CameraImageSave();
					/*try{
						cameraSaveImage.cretaeFile();
						cameraSaveImage.saveBitmapToFile(thumbnail);
					}catch(Exception ex){
						ex.printStackTrace();
					}*/
					//publishProgress(thumbnail.getWidth()+" "+ thumbnail.getHeight());
					/*try{
						Bitmap bitmap1 = decodeUri(params[0]); //cameraSaveImage.getBitmapFromFile(thumbnail.getWidth(), thumbnail.getHeight());
						if(bitmap1!=null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, stream);
							//cameraSaveImage.deleteFromFile();
							array = stream.toByteArray();
							//cameraSaveImage = null;
						}
						publishProgress(json.toString());
						InorbitLog.d(json.toString());
					} catch (Exception e) { }*///decodeUri(params[0]);
					InputStream iStream = getContentResolver().openInputStream(params[0]);
					byte[] inputData = getBytes(iStream);
					try {
						json.put("userfile", Base64.encodeToString(inputData, Base64.DEFAULT));
						json.put("angular_rotation", angularRotation+"");
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				//}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			return null;
		}
		
		public byte[] getBytes(InputStream inputStream) throws IOException {
		      ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		      int bufferSize = 1024;
		      byte[] buffer = new byte[bufferSize];

		      int len = 0;
		      while ((len = inputStream.read(buffer)) != -1) {
		        byteBuffer.write(buffer, 0, len);
		      }
		      return byteBuffer.toByteArray();
		}
		
		@Override
		protected void onProgressUpdate(String... values) {
			showToast(" "+values[0]);
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			MyClass myClass = new MyClass(context);
			myClass.postRequest(RequestTags.TAG_UPLOAD_MENU, headers, json);
			super.onPostExecute(result);
		}
	}
	
/*	private void postGalleryData(byte[] image){

		try
		{
			if(isnetConnected.isNetworkAvailable()) {
				String encodedpath="";

				//byte [] b=imageTobyteArray(FILE_PATH,480,480);
				//progress.setVisibility(View.VISIBLE);
				if(image!=null) {	
					SessionManager session = new SessionManager(this);
					Toast.makeText(context, "Uploading image",Toast.LENGTH_SHORT).show();

					HashMap<String, String> headers = new HashMap<String, String>();
					headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

					JSONObject json = new JSONObject();
					json.put("place_id", storeId);
					json.put("mall_id", storePlaceParent);
					if (session.isMallManagerLoggedin()) 
						json.put("status", 1+"");
					else
						json.put("status", 0+"");
					json.put("type", "Photo");
					json.put("filename", "menu.jpg");
					json.put("filetype", "image/jpeg");
					json.put("user_id", user_id);
					json.put("auth_id", auth_id);
					CameraImageSave cameraSaveImage = new CameraImageSave();

					try{
						cameraSaveImage.cretaeFile();
						cameraSaveImage.saveBitmapToFile(thumbnail);
					}catch(Exception ex){
						ex.printStackTrace();
					}
					
					try{
						Bitmap bitmap1 = cameraSaveImage.getBitmapFromFile(thumbnail.getWidth(), thumbnail.getHeight());
						if(bitmap1!=null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, stream);
							cameraSaveImage.deleteFromFile();
							image = stream.toByteArray();
							cameraSaveImage = null;
						}
						Toast.makeText(context, json.toString(),Toast.LENGTH_SHORT).show();
						InorbitLog.d(json.toString());
					} catch (Exception e) { }
					json.put("userfile", Base64.encodeToString(image, Base64.DEFAULT));
					
					InorbitLog.d(json.toString());
					MyClass myClass = new MyClass(this);
					myClass.postRequest(RequestTags.TAG_UPLOAD_MENU, headers, json);
				}
				else
				{
					encodedpath="undefined";
					Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
				}
			}
			else
			{
				Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
*/
	void receiveBundleValues() {
		Bundle b=getIntent().getExtras();
		if (b!=null) {
			SessionManager session = new SessionManager(this);

			user_id = session.getUserDetails().get("user_id").toString();
			auth_id = session.getUserDetails().get("auth_id").toString();

			/**
			 * Selected store id
			 * 
			 */
			storeId	= b.getString("STORE_ID");
			/**
			 * Selected store name
			 * 
			 */
			storeName = b.getString("STORE_NAME");
			actionBar.setTitle(storeName);

			/**
			 * Selected store logo url
			 * 
			 */
			storeLogoUrl = b.getString("STORE_LOGO");

			/**
			 * Selected store place parent
			 * 
			 */
			storePlaceParent = b.getString("PLACE_PARENT");
			//showToast("id "+ storeId+" name "+storeName);
			//getMenuFromServer();
		}
	}
	
	private void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
	
	private class UploadMenu extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				progress.setVisibility(View.GONE);
				showToast(intent.getStringExtra("message"));

				getMenuFromServer();
			} else {
				showToast("Error");
			}

		}
	}
	
	private class GridAdapter extends ArrayAdapter<RestaurantMenuModel> {
		
		ArrayList<RestaurantMenuModel> mModelMenu;
		Activity context;
		LayoutInflater inflate;
		String menuImage  = "";
		
		public GridAdapter(Activity context, int resource,ArrayList<RestaurantMenuModel> mModelMenu) {
			super(context, resource, mModelMenu);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.mModelMenu=mModelMenu;
			inflate=context.getLayoutInflater();
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mModelMenu.size();
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			final int pos = position;
			if(convertView==null)
			{
				//				Log.d("INIF", "INIF");
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.gridimagelayout,null);

				holder.photo=(ImageView)convertView.findViewById(R.id.imgGridImage);
				//holder.txtImageTitle=(TextView)convertView.findViewById(R.id.txtImageTitle);
				holder.toChkDelete=(CheckBox)convertView.findViewById(R.id.toChkDelete);
				
				if(showEditMode){

					holder.toChkDelete.setVisibility(View.VISIBLE);
				}

				holder.toChkDelete.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub

						if(buttonView.isChecked()) {
							IDSTODELETE.add(mModelMenu.get(pos).getId());
						} else {
							IDSTODELETE.remove(mModelMenu.get(pos).getId());
						}
					}
				});

				convertView.setTag(holder);
			}
			else{
				Log.d("INIF", "INELSE");	
			}
			
			ViewHolder hold=(ViewHolder)convertView.getTag();
			
			if(mMenuModel!=null && (mMenuModel.get(position).getThumb_url()!=null || mMenuModel.get(position).getThumb_url().length()!=0 
					|| mMenuModel.get(position).getImage_url().length()!=0)) {
				menuImage = mMenuModel.get(position).getThumb_url().replaceAll(" ", "%20");
				
				try {
					Picasso.with(context).load(PHOTO_URL+menuImage)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(hold.photo);
				} catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){
					e.printStackTrace();
				}

			}
			return convertView;
		}

	} 

	class ViewHolder {
		ImageView photo;
		CheckBox toChkDelete;
	}
	
	private void getMenuFromServer(){
		
		progress.setVisibility(View.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		
		List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("place_id", storeId));
		nameValuePairs.add(new BasicNameValuePair("mall_id", storePlaceParent));
		headers.put("user_id", user_id);
		headers.put("auth_id", auth_id);
		
		MyClass myClass = new MyClass(context);
		myClass.getStoreRequest(RequestTags.TAG_GET_MENU_FOR_STORE, nameValuePairs, headers);	
	}

/*	private void uploadImageToServer() {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("place_id", storeId));
		nameValuePairs.add(new BasicNameValuePair("user_id", user_id));
		headers.put("user_id", user_id);
		headers.put("auth_id", auth_id);
	}*/

	class AllMenuImages extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			progress.setVisibility(View.GONE);
			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					mMenuModel = intent.getParcelableArrayListExtra("MENUS");
					
					if(success.equalsIgnoreCase("true")){
						//showToast("true");
						if(mMenuModel!=null){
							gridAdapter = new GridAdapter(aContext, R.drawable.ic_launcher, mMenuModel);
							imageGrid.setAdapter(gridAdapter);	
						}
						
					}
					else{
						imageGrid.setAdapter(null);
						String message = intent.getStringExtra("MESSAGE");/*getResources().getString(R.string.no_menu_available_yet);*/
						showToast(message);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	void callDeleteImage(JSONObject jsonObj) {
		// TODO Auto-generated method stub
		
		progress.setVisibility(View.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		MyClass myClass = new MyClass(context);
		myClass.postRequest(RequestTags.TAG_DELETE_MENU_IMAGES, headers, jsonObj);
	}
	
	class DeleteMenuImages extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			
			try {
				IDSTODELETE.clear();
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					
					if(success.equalsIgnoreCase("false")){
						String message = intent.getStringExtra("MESSAGE");
						showToast(message);
					} else {
						showToast("Your menu has been deleted successfully");
					}
					getMenuFromServer();
				}
				else{
					showToast("Error while deleting");
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		
	}


	@Override
	protected void onStop() {
		try{
			if(reciverObj!=null){
				context.unregisterReceiver(reciverObj);	
			}
			if(uploadMenu!=null){
				context.unregisterReceiver(uploadMenu);
			}
			if (reciverObjDelete!=null) {
				context.unregisterReceiver(reciverObjDelete);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onStop();
	}
	
	class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {
	    private final WeakReference<ImageView> imageViewReference;
	    private int data = 0;

	    public BitmapWorkerTask(ImageView imageView) {
	        // Use a WeakReference to ensure the ImageView can be garbage collected
	        imageViewReference = new WeakReference<ImageView>(imageView);
	    }

	    // Decode image in background.
	    @Override
	    protected Bitmap doInBackground(Integer... params) {
	        data = params[0];
	        return decodeSampledBitmapFromResource(getResources(), data, 100, 100);
	    }

	    // Once complete, see if ImageView is still around and set bitmap.
	    @Override
	    protected void onPostExecute(Bitmap bitmap) {
	        if (imageViewReference != null && bitmap != null) {
	            final ImageView imageView = imageViewReference.get();
	            if (imageView != null) {
	                imageView.setImageBitmap(bitmap);
	            }
	        }
	    }
	}
	
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	
	public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        final int halfHeight = height / 2;
        final int halfWidth = width / 2;

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
            inSampleSize *= 2;
        }
    }

    return inSampleSize;
}
}

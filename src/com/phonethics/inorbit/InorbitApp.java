package com.phonethics.inorbit;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
/*import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.UAirship;
import com.urbanairship.push.CustomPushNotificationBuilder;
import com.urbanairship.push.PushManager;*/
//import com.urbanairship.AirshipConfigOptions;
//import com.urbanairship.UAirship;
//import com.urbanairship.push.CustomPushNotificationBuilder;
//import com.urbanairship.push.PushManager;

public class InorbitApp extends Application{

	Context context;

	/**
	 * Log or request TAG
	 */
	public static final String TAG = "InorbitVolley";

	/** Urban airship keys */
	String urbanAirshiptAppKey;
	String urbanAirshipAppSecret;
	String urbanAirshipGCMProjectId;



	/**
	 * Global request queue for Volley
	 */
	
	private RequestQueue mRequestQueue;

	private static Typeface tf;
	private static Typeface tf_title;


	/**
	 * A singleton instance of the application class for easy access in other places
	 */
	private static InorbitApp sInstance;


	@Override
	public void onCreate() {
		super.onCreate();

		context 	= this;
		sInstance 	= this;
		tf				= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
		tf_title		= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");

		/*AirshipConfigOptions options = AirshipConfigOptions.loadDefaultOptions(this);
		urbanAirshipGCMProjectId = getResources().getString(R.string.urbanAirshipGCMProjectId);

		options.inProduction = false;

		//options.inProduction = true;

		if(options.inProduction==false)
		{
			options.developmentAppKey 		= getResources().getString(R.string.urbanAirshipdevelopmentAppKey);
			options.developmentAppSecret 	= getResources().getString(R.string.urbanAirshipdevelopmentAppSecret);
		}
		else
		{
			options.productionAppKey		= getResources().getString(R.string.urbanAirshipProductionAppKey);
			options.productionAppSecret 	= getResources().getString(R.string.urbanAirshipProductionAppSecret);

		}

		options.gcmSender = urbanAirshipGCMProjectId;
		options.transport = "gcm";


		UAirship.takeOff(this, options);
		//        Logger.logLevel = Log.VERBOSE;

		//use CustomPushNotificationBuilder to specify a custom layout
		CustomPushNotificationBuilder nb = new CustomPushNotificationBuilder();

		nb.statusBarIconDrawableId = R.drawable.ic_launcher;//custom status bar icon

		nb.layout = R.layout.notification;
		nb.layoutIconDrawableId = R.drawable.ic_launcher;//custom layout icon
		nb.layoutIconId = R.id.icon;
		nb.layoutSubjectId = R.id.subject;
		nb.layoutMessageId = R.id.message;
		
		PushManager.enablePush();
		PushManager.shared().setNotificationBuilder(nb);
		PushManager.shared().setIntentReceiver(IntentReceiver.class);*/
	}


	/**
	 * @return InorbitApp singleton instance
	 */
	public static  InorbitApp getInstance() {
		return sInstance;
	}


	/**
	 * @return The Volley Request queue, the queue will be created if it is null
	 */
	public RequestQueue getRequestQueue() {
		// lazy initialize the request queue, the queue instance will be
		// created when it is accessed for the first time
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
			InorbitLog.d("New Request Queue");
		}

		return mRequestQueue;
	}


	/**
	 * Adds the specified request to the global queue, if tag is specified
	 * then it is used else Default TAG is used.
	 * 
	 * @param req
	 * @param tag
	 */
	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(tag);

		VolleyLog.d("Adding request to queue: %s", req.getUrl());
		InorbitLog.d(req.getUrl());

		getRequestQueue().add(req);
	}

	/**
	 * Adds the specified request to the global queue using the Default TAG.
	 * 
	 * @param req
	 * @param tag
	 */
	public <T> void addToRequestQueue(Request<T> req) {
		// set the default tag if tag is empty
		req.setTag(TAG);
		VolleyLog.d("Adding request to queue: %s", req.getUrl());
		InorbitLog.d(req.getUrl());
		getRequestQueue().add(req);
	}

	/**
	 * Cancels all pending requests by the specified TAG, it is important
	 * to specify a TAG so that the pending/ongoing requests can be cancelled.
	 * 
	 * @param tag
	 */
	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
			InorbitLog.d("Cancel Request");
		}
	}

	public static Typeface getTypeFace(){
		return tf;
	}

	public static Typeface getTypeFaceTitle(){
		return tf_title;
	}

	/** Urban airship */


	



}

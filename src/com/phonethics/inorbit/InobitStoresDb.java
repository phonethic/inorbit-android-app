package com.phonethics.inorbit;

import java.util.ArrayList;

import com.phonethics.model.StoreInfo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;


public class InobitStoresDb extends SQLiteOpenHelper {


	private  				Context 		mContext;
	private static final 	String 			mSDataBaseName 		 = "InorbitStores.db";
	private static final 	int  			mIDatabaseVersion    = 1;
	private static InobitStoresDb			mInstance = null;

	private static			SQLiteDatabase 	mSqlDataBase;
	private static final 	String 			mSTableStores		 = "STORES";


	public InobitStoresDb(Context context, String name, CursorFactory factory,
			int version) {
		super(context, mSDataBaseName, null, mIDatabaseVersion);
		// TODO Auto-generated constructor stub
	}

	public static InobitStoresDb getInstance(Context context) {
		if(mInstance == null && context != null) {
			mInstance = new InobitStoresDb(context);
			mSqlDataBase = mInstance.getWritableDatabase();
		}
		
		if(mSqlDataBase!=null && !mSqlDataBase.isOpen()){
			mSqlDataBase = mInstance.getWritableDatabase();
		}
		return mInstance;
	}

	public InobitStoresDb(Context context){
		super(context, mSDataBaseName, null, mIDatabaseVersion);
	}


	public  InobitStoresDb open() throws SQLException {
		mSqlDataBase = getWritableDatabase();
		return this;
	}

	public void closeDb() {
		if(mSqlDataBase != null) {
			if(mSqlDataBase.isOpen()){
				mSqlDataBase.close();
				mSqlDataBase = null;
			}
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

		/** 
		 * Create table Stores.
		 * 
		 */
		db.execSQL("CREATE TABLE IF NOT EXISTS "+ Tables.TABLE_STORES +"("+
				Tables.ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
				Tables.STORE_ID + " TEXT, "+
				Tables.STORE_NAME + " TEXT, "+
				Tables.STORE_MALL_ID + " TEXT, "+
				Tables.STORE_PLACEPARENT + " TEXT, "+
				Tables.STORE_LOGO + " TEXT, "+
				Tables.STORE_DESC + " TEXT, "+
				Tables.STORE_CITY + " TEXT, "+
				Tables.STORE_BUILDING + " TEXT, "+
				Tables.STORE_STREET + " TEXT, "+
				Tables.STORE_AREA + " TEXT, "+
				Tables.STORE_LANDMARK + " TEXT, "+
				Tables.STORE_MOB_NO_1 + " TEXT, "+
				Tables.STORE_MOB_NO_2 + " TEXT, "+
				Tables.STORE_MOB_NO_3 + " TEXT, "+
				Tables.STORE_LAND_NO_1 + " TEXT, "+
				Tables.STORE_LAND_NO_2 + " TEXT, "+
				Tables.STORE_LAND_NO_3 + " TEXT, "+
				Tables.STORE_WEBSITE + " TEXT, "+
				Tables.STORE_FACEBOOK_URl + " TEXT, "+
				Tables.STORE_WEBSITE_URl + " TEXT, "+
				Tables.STORE_EMAIL + " TEXT, "+
				Tables.STORE_TOTAL_LIKE + " TEXT, "+
				Tables.STORE_HAS_OFFER + " TEXT, "+
				Tables.STORE_OFFER_TITLE + " TEXT, "+
				Tables.STORE_CATEGORY + " TEXT);");



	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS "+Tables.TABLE_STORES);
		onCreate(db);
	}



	public long createStoreTable(ArrayList<StoreInfo> mArrStoreDetails,String mSCategory){
		long entiresAffected=0;
		open();
		for(int i=0;i<mArrStoreDetails.size();i++){

			ContentValues cv = new ContentValues();

			cv.put(Tables.STORE_ID, mArrStoreDetails.get(i).getId());
			cv.put(Tables.STORE_NAME, mArrStoreDetails.get(i).getName());
			cv.put(Tables.STORE_MALL_ID, mArrStoreDetails.get(i).getMall_id());
			cv.put(Tables.STORE_PLACEPARENT, mArrStoreDetails.get(i).getPlace_parent());
			cv.put(Tables.STORE_LOGO , mArrStoreDetails.get(i).getImage_url());
			cv.put(Tables.STORE_DESC, mArrStoreDetails.get(i).getDescription());
			cv.put(Tables.STORE_CITY, mArrStoreDetails.get(i).getCity());
			cv.put(Tables.STORE_BUILDING, mArrStoreDetails.get(i).getBuilding());
			cv.put(Tables.STORE_STREET, mArrStoreDetails.get(i).getStreet());
			cv.put(Tables.STORE_LANDMARK, mArrStoreDetails.get(i).getLandmark());
			cv.put(Tables.STORE_AREA, mArrStoreDetails.get(i).getArea());
			cv.put(Tables.STORE_MOB_NO_1, mArrStoreDetails.get(i).getMob_no1());
			cv.put(Tables.STORE_WEBSITE, mArrStoreDetails.get(i).getWebsite());
			cv.put(Tables.STORE_EMAIL, mArrStoreDetails.get(i).getEmail());
			cv.put(Tables.STORE_TOTAL_LIKE, mArrStoreDetails.get(i).getTotal_like());
			cv.put(Tables.STORE_HAS_OFFER, mArrStoreDetails.get(i).getHas_offer());
			cv.put(Tables.STORE_OFFER_TITLE, mArrStoreDetails.get(i).getTitle());
			cv.put(Tables.STORE_CATEGORY, mSCategory);

			entiresAffected = mSqlDataBase.insert(Tables.TABLE_STORES, null, cv);


		}

		close();
		InorbitLog.d("Entries affected "+entiresAffected);
		return entiresAffected;


	}

	public Integer getTotalStores(String msCategory,String msPlaceParent){
		long total_records = 0;
		try{

			if(mSqlDataBase!=null && !mSqlDataBase.isOpen()){
				open();	
			}
			
			String query="SELECT COUNT("+Tables.STORE_ID+") AS ROW_COUNT FROM "+Tables.TABLE_STORES + 
					" WHERE "+Tables.STORE_CATEGORY +"='"+msCategory+"' AND "+Tables.STORE_PLACEPARENT+"='"+msPlaceParent+"'";
			InorbitLog.d("Query "+query);
			Cursor  cursor = mSqlDataBase.rawQuery(query, null);
			cursor.moveToFirst();
			total_records=cursor.getLong(cursor.getColumnIndex("ROW_COUNT"));
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			if (mSqlDataBase != null && mSqlDataBase.isOpen()) {
				mSqlDataBase.close();
		    }
			
		}
		return Integer.parseInt(total_records+"");

	}

	
	public ArrayList<StoreInfo> getStores(String msStoreCategory,String msPlaceParent){
		ArrayList<StoreInfo> mArrStores = new ArrayList<StoreInfo>();
		try{
			if(mSqlDataBase!=null && !mSqlDataBase.isOpen()){
				open();	
			}
			String query = "SELECT * FROM "+Tables.TABLE_STORES+
					" WHERE "+Tables.STORE_CATEGORY+"='"+msStoreCategory+"' AND "+Tables.STORE_PLACEPARENT+"='"+msPlaceParent+"'";
			InorbitLog.d("Query "+query);
			Cursor  cursor = mSqlDataBase.rawQuery(query, null);
			InorbitLog.d("Cursor size "+cursor.getCount());
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				StoreInfo store = new StoreInfo();
		
				store.setArea(cursor.getString(cursor.getColumnIndex(Tables.STORE_AREA)));
				store.setCity(cursor.getString(cursor.getColumnIndex(Tables.STORE_CITY)));
				store.setDescription(cursor.getString(cursor.getColumnIndex(Tables.STORE_DESC)));
				store.setEmail(cursor.getString(cursor.getColumnIndex(Tables.STORE_EMAIL)));
				store.setHas_offer(cursor.getString(cursor.getColumnIndex(Tables.STORE_HAS_OFFER)));
				store.setId(cursor.getString(cursor.getColumnIndex(Tables.STORE_ID)));
				store.setImage_url(cursor.getString(cursor.getColumnIndex(Tables.STORE_LOGO)));
				store.setLandmark(cursor.getString(cursor.getColumnIndex(Tables.STORE_LANDMARK)));
				store.setMall_id(cursor.getString(cursor.getColumnIndex(Tables.STORE_MALL_ID)));
				store.setMob_no1(cursor.getString(cursor.getColumnIndex(Tables.STORE_MOB_NO_1)));
				store.setName(cursor.getString(cursor.getColumnIndex(Tables.STORE_NAME)));
				store.setPlace_parent(cursor.getString(cursor.getColumnIndex(Tables.STORE_PLACEPARENT)));
				store.setStreet(cursor.getString(cursor.getColumnIndex(Tables.STORE_STREET)));
				store.setTitle(cursor.getString(cursor.getColumnIndex(Tables.STORE_OFFER_TITLE)));
				store.setTotal_like(cursor.getString(cursor.getColumnIndex(Tables.STORE_TOTAL_LIKE)));
				store.setWebsite(cursor.getString(cursor.getColumnIndex(Tables.STORE_WEBSITE)));
				
				
				mArrStores.add(store);
				
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if (mSqlDataBase != null && mSqlDataBase.isOpen()) {
				mSqlDataBase.close();
		    }
			
		}
		return mArrStores;
		
	}
	
	void deleteStrores(String msStoreCategory, String msPlaceParent){
		try{

			if(mSqlDataBase!=null && !mSqlDataBase.isOpen()){
				open();	
			}
			String msWhere = Tables.STORE_CATEGORY+"='"+msStoreCategory+"' AND "+Tables.STORE_PLACEPARENT+"='"+msPlaceParent+"'";	
			int entriesAffected = mSqlDataBase.delete(Tables.TABLE_STORES,msWhere,null);
			InorbitLog.d("Deleted Entries "+entriesAffected);
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			if (mSqlDataBase != null && mSqlDataBase.isOpen()) {
				mSqlDataBase.close();
		    }
		}

	}
}

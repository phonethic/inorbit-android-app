package com.phonethics.inorbit;

public class FbObject {
	
	private String objTitle = "";
	private String objImage = "";
	private String objUrl	= "";
	private String objDescription = "";
	
	
	
	public String getobjTitle() {
		return objTitle;
	}
	public void setobjTitle(String objTitle) {
		this.objTitle = objTitle;
	}
	public String getobjImage() {
		return objImage;
	}
	public void setobjImage(String objImage) {
		this.objImage = objImage;
	}
	public String getobjUrl() {
		return objUrl;
	}
	public void setobjUrl(String objUrl) {
		this.objUrl = objUrl;
	}
	public String getobjDescription() {
		return objDescription;
	}
	public void setobjDescription(String objDescription) {
		this.objDescription = objDescription;
	}
	
	
	

}

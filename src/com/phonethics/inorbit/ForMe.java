package com.phonethics.inorbit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;

import com.phonethics.inorbit.CategorySearch.CategoreBroadcast;
import com.phonethics.model.RequestTags;
import com.phonethics.model.SpecialPost;
import com.squareup.picasso.Picasso;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class ForMe extends SherlockActivity implements SensorEventListener {


	private SensorManager sensorManager;
	private boolean color = false;
	private View view;
	private long lastUpdate;
	private static final float SHAKE_THRESHOLD_GRAVITY = 2F;
	static Context		context;
	NetworkCheck isnetConnected;
	SpecialPostBroadcast broadcast;
	String URL;
	String Special_Url;
	ProgressBar pBar;
	ListView specialPostList;
	Activity actContext;
	AlertDialog alertDialog;
	ImageView imgView;   
	TextView	text_shake;
	static String PHOTO_PARENT_URL;
	ActionBar actionBar;
	static String USER_ID="";
	static String AUTH_ID="";
	static String API_HEADER;
	static String API_VALUE;
	static SessionManager session;
	int imageCount =0;
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";
	boolean isLoggedInCustomer;
	AnimationDrawable frameAnimation;
	TextView txt1;
	TextView txt2;
	TextView txt3,txt5;
	int scwidth = 0;
	Runnable runnable_anim;
	TextView[] txtarray = new TextView[3];
	int count=1;
	String txtarr[];
	ArrayList<SpecialPost> specialPostModelArr = new ArrayList<SpecialPost>();
	private String MALL_NAME;
	boolean shakedOnce = false;
	Button bttn_win_surprises;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_for_me);


		
		/**
		 * 
		 * Initialize the layout and class variables
		 * 
		 * 
		 */
		actContext	=	this;
		context		= this;
		isnetConnected	=	new NetworkCheck(context);
		session = new SessionManager(context);
		imgView = (ImageView)findViewById(R.id.img_mobile_shake);   
		text_shake = (TextView) findViewById(R.id.text_mobile_shake);
		bttn_win_surprises = (Button) findViewById(R.id.bttn_win_surprises);
		imgView.setVisibility(ImageView.VISIBLE);
		imgView.setBackgroundResource(R.drawable.frame_animation);
		specialPostList = (ListView) findViewById(R.id.specialPostList);
		
		
		/**
		 * Initalize the action bar
		 * 
		 */
		actionBar=getSupportActionBar();
		actionBar.setTitle("Shake & Win");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

	
		/**
		 * Not in use now
		 * 
		 */
		txtarr = new String[6];
		txt1 = (TextView) findViewById(R.id.txt1);
		txt2 = (TextView) findViewById(R.id.txt2);
		txt3 = (TextView) findViewById(R.id.txt3);
		txt5 = (TextView) findViewById(R.id.txt5);

		txt1.setTypeface(InorbitApp.getTypeFace());
		txt2.setTypeface(InorbitApp.getTypeFace());
		txt3.setTypeface(InorbitApp.getTypeFace());
		txt5.setTypeface(InorbitApp.getTypeFace());
		text_shake.setTypeface(InorbitApp.getTypeFace());


		Bundle bundle = getIntent().getExtras();
		if(bundle!=null){
			MALL_NAME = bundle.getString("MALL_NAME");
		}
		
		
		/**
		 * Not in use now
		 * 
		 */
		ArrayList<String> data = new ArrayList<String>();
		data.add("Have you completed your profile ?");
		data.add("Have you entered your birthday ?");
		data.add("Have you entered your anniversary ?");
		data.add("Have you selected your favourite Inorbit Mall?");



		//data.add("Text 4");

		/**
		 * 
		 * Shake animation
		 * 
		 */
		pBar =(ProgressBar)findViewById(R.id.pBar);
		pBar.setVisibility(View.GONE);
		frameAnimation = (AnimationDrawable) imgView.getBackground();
		frameAnimation.start();


		/**
		 * API KEY
		 */

		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		
		/**
		 * Get the user information from session manager
		 * 
		 */
		HashMap<String,String>user_details=session.getUserDetailsCustomer();
		USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();

	
		/**
		 * 
		 * Check whether customer is logged in or not
		 * 
		 */
		isLoggedInCustomer=session.isLoggedInCustomer();

		// URLS

		URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api);
		Special_Url = getResources().getString(R.string.getspecialpost);
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);


		

		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		lastUpdate = System.currentTimeMillis();
		
		animate();
		startImageAnimation(txt2, txt1, data);
		
		txt5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(session.isLoggedInCustomer()){
					//FireEvents.TagScreen(TAB+"MyProfile");
					registerEvent(getResources().getString(R.string.shake2Win_CompleteProfile));
					Intent intent=new Intent(context,CustomerProfile.class);
					context.startActivity(intent);
					((Activity) context).overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
			}
		});

		bttn_win_surprises.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				registerEvent(getResources().getString(R.string.shake2Win_howToWin));
				showDialog();
			}
		});
		
	
	
	}




	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if(specialPostModelArr.size()==0){
			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				getAccelerometer(event);
			}
		}
	}



	private void getAccelerometer(SensorEvent event) {
		float[] values = event.values;
		// Movement
		float x = values[0];
		float y = values[1];
		float z = values[2];

		float gForce = (x * x + y * y + z * z)/ (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
		long actualTime = System.currentTimeMillis();

		// gForce will be close to 1 when there is no movement.

		if (gForce >= SHAKE_THRESHOLD_GRAVITY) //
		{
			if (actualTime - lastUpdate < 1000) {
				Log.d("", "Inorbit true");
				return;
			}
			lastUpdate = actualTime;
			//Toast.makeText(this, "Device was shuffed", Toast.LENGTH_SHORT).show();
			if(isnetConnected.isNetworkAvailable()){

				if(isLoggedInCustomer){
					/*if(PLACE_ID.size()==0 && !shakedOnce){
						callGetSpecialPost();
					}*/
					callGetSpecialPost();
					registerEventWithPara(getResources().getString(R.string.shake2Win_Shake),"");

				}else{

					MyToast.showToast(actContext, "Login to see offers for you");
				}
			}else{
				MyToast.showToast(actContext, getResources().getString(R.string.noInternetConnection), 1);
			}
			
		}
	}


	@Override
	protected void onResume() {
		super.onResume();
		try{
			// register this class as a listener for the orientation and
			// accelerometer sensors
			EventTracker.startLocalyticsSession(getApplicationContext());
			sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_NORMAL);
			IntentFilter filter = new IntentFilter(RequestTags.TagGetSpecialPost);

			context.registerReceiver(broadcast = new SpecialPostBroadcast(), filter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		// unregister listener

		try{
			EventTracker.endLocalyticsSession(getApplicationContext());
			sensorManager.unregisterListener(this);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onPause();
	}


	private void animate() {
	

		if(imgView.isShown()){
			imgView.setVisibility(View.GONE);
			text_shake.setVisibility(View.GONE);
			txt1.setVisibility(View.GONE);
			txt2.setVisibility(View.GONE);
			txt3.setVisibility(View.GONE);
			txt5.setVisibility(View.GONE);
			specialPostList.setVisibility(View.VISIBLE);
			specialPostList.bringToFront();
			bttn_win_surprises.setVisibility(View.GONE);
		}else{
			imgView.setVisibility(View.VISIBLE);
			text_shake.setVisibility(View.VISIBLE);
			txt1.setVisibility(View.GONE);
			txt2.setVisibility(View.GONE);
			txt3.setVisibility(View.GONE);
			txt5.setVisibility(View.GONE);
			specialPostList.setVisibility(View.GONE);
			bttn_win_surprises.setVisibility(View.VISIBLE);
			
		}
	}
 

	private class SpecialPostBroadcast extends BroadcastReceiver{

		@Override
		public void onReceive(final Context context, Intent intenet) {
			// TODO Auto-generated method stub
			try{
				animate();
				pBar.setVisibility(View.GONE);
				if(intenet!=null){

					String STATUS = intenet.getStringExtra("SUCCESS");
					if(STATUS.equalsIgnoreCase("true")){
						if(intenet!=null){
							specialPostList.setVisibility(View.VISIBLE);
							specialPostModelArr = intenet.getParcelableArrayListExtra("SpecialPostDetails");
							
							SpecialPostAdapterNew adapterNew = new SpecialPostAdapterNew((Activity) context,specialPostModelArr);
							specialPostList.setAdapter(adapterNew);


							specialPostList.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3) {
									// TODO Auto-generated method stub
									createInhouseAnalyticsEvent(specialPostModelArr.get(position).getId(), "2", session.getActiveMallId());
									registerEvent(getResources().getString(R.string.shake2Win_ViewOffer));
									Intent intent=new Intent(context, SpecialPostDetailView.class);
									intent.putExtra("PLACE_ID", specialPostModelArr.get(position).getPlace_id());
									//intent.putExtra("storeName", storeName);
									//intent.putExtra("POST_ID", ID.get(position));
									intent.putExtra("title", specialPostModelArr.get(position).getTitle());
									intent.putExtra("description", specialPostModelArr.get(position).getDescription());
									intent.putExtra("thumbUrl", specialPostModelArr.get(position).getThumb_url1());
									intent.putExtra("imageUrl", specialPostModelArr.get(position).getImage_url1());
									intent.putExtra("MALL_ID", specialPostModelArr.get(position).getMall_id());
									intent.putExtra("CODE", specialPostModelArr.get(position).getId()+"-"+USER_ID);
									Log.d("SPECIALDATE","SPECIAL " + specialPostModelArr.get(position).getOffer_date_time());
									intent.putExtra("offerValidTill", specialPostModelArr.get(position).getOffer_date_time());
									
									startActivity(intent);
								}
							});
						}
					}else{

						String message =  intenet.getStringExtra("MESSAGE");
						String code = intenet.getStringExtra("CODE");

						//showToast(message);
						ShowMessageDialog.show(actContext, message);

						if(message.contains("Invalid")){
							MyToast.showToast((Activity) context, "Please Login Again");
							Intent intent=new Intent(context,LoginSigbUpCustomerNew.class);
							intent.putExtra("isInvalidAuth", true);
							intent.putExtra("comeBackTo", Conifg.intForMe);
							startActivity(intent);
							onBackPressed();
						}else{
							animate();
							//showToast(message);
						}

						boolean isVolleyError = intenet.getBooleanExtra("volleyError",false);
						if(isVolleyError){

							int code1 = intenet.getIntExtra("CODE",0);
							String message1=intenet.getStringExtra("MESSAGE");
							String errorMessage = intenet.getStringExtra("errorMessage");
							String apiUrl = intenet.getStringExtra("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "ShakeAndWin");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}


					}
				}else{
					showToast("Null");
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}
	
	private static void createInhouseAnalyticsEvent(String flag, String activityId, String mallId) {
		Uri uri = new Uri.Builder().scheme("content").authority(context.getResources().getString(R.string.authority)).
				appendPath("/insert").build();
        ContentValues cv = new ContentValues(6);
		cv.put("UDM_ID", session.getUdmIDForCustomer());
		cv.put("DATE", new Date().toString());
		cv.put("FLAG", flag);
		cv.put("ACTIVITY_ID", activityId);
		cv.put("MALL_ID", mallId);
		cv.put("SYNC_STATUS", -1);
		context.getContentResolver().insert(uri, cv);
	}

	/**
	 * Network request to get the offer list
	 * 
	 */
	public void callGetSpecialPost() {
		// TODO Auto-generated method stub

		pBar.setVisibility(View.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);
		headers.put("user_id", USER_ID);
		headers.put("auth_id", AUTH_ID);

		int post_count = -1;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("page", 1+""));
		nameValuePairs.add(new BasicNameValuePair("count", post_count+""));


		MyClass myClass = new MyClass(actContext);
		myClass.makeGetRequest(RequestTags.TagGetSpecialPost, "-1", headers);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	void showToast(String message){
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

	}

	static class ViewHolder{
		TextView txtTitle,txtDate,txtMonth, broadCastOffersDescription;
		ImageView imgBroadcastLogo;
		View viewOverLay;

	}

	static class SpecialPostAdapterNew extends ArrayAdapter<SpecialPost>{


		Activity 		context;
		LayoutInflater 	inflate;
		ArrayList<SpecialPost> specialModelArr;
		ArrayList<String> createdAtList;

		public SpecialPostAdapterNew(Activity context,ArrayList<SpecialPost> specialPostModelArr){
			super(context, R.layout.special_post_layout_new);

			this.context = context;
			this.specialModelArr = specialPostModelArr;
			this.createdAtList = createdAtList;
			inflate = context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return specialModelArr.size();
		}

		@Override
		public SpecialPost getItem(int position) {
			// TODO Auto-generated method stub
			return specialModelArr.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.special_post_layout_new,null);

				holder.txtTitle=(TextView)convertView.findViewById(R.id.broadCastOffersText);
				holder.imgBroadcastLogo=(ImageView)convertView.findViewById(R.id.imgBroadcastLogo);
				holder.txtDate=(TextView)convertView.findViewById(R.id.broadCastOffersDate);
				holder.txtMonth=(TextView)convertView.findViewById(R.id.broadCastOffersMonth);
				holder.viewOverLay=(View)convertView.findViewById(R.id.viewOverLay);
				holder.broadCastOffersDescription=(TextView)convertView.findViewById(R.id.broadCastOffersDescription);

				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.txtTitle.setText(getItem(position).getTitle());
			hold.broadCastOffersDescription.setText(getItem(position).getDescription());

			try
			{
				InorbitLog.d("Date Offer "+getItem(position).getOffer_date_time());
				InorbitLog.d("Created Offer "+getItem(position).getCreated_at());
				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");


				Log.d("LOGDATE","LOGDATE " + "HI -- " +getItem(position).getCreated_at());
				//				Log.d("LOGDATE","LOGDATE " + "HI 1 -- " +getItem(position).getCreated_at1());
				Date date_con = (Date) dt.parse(getItem(position).getCreated_at());
				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				hold.txtDate.setText(day+"");
				hold.txtMonth.setText(dt2.format(date_con)+"");



			}catch(Exception ex){
				ex.printStackTrace();
			}

			try{
				if(getItem(position).getThumb_url1().toString().equalsIgnoreCase("")){
					hold.txtDate.setTextColor(Color.argb(255, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.INVISIBLE);
					Log.d("", "Inorbit url >"+PHOTO_PARENT_URL);
				}else{	
					Log.d("", "Inorbit url >>"+PHOTO_PARENT_URL+getItem(position).getThumb_url1().toString().replaceAll(" ", "%20"));
					hold.txtDate.setTextColor(Color.argb(200, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.VISIBLE);
					try {

						Picasso.with(context).load(PHOTO_PARENT_URL+getItem(position).getThumb_url1().toString().replaceAll(" ", "%20"))
						.placeholder(R.drawable.ic_launcher)
						.error(R.drawable.ic_launcher)
						.into(hold.imgBroadcastLogo);

					}catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
			return convertView;
		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		try{
			EventTracker.endFlurrySession(getApplicationContext());

			if(broadcast!=null){
				context.unregisterReceiver(broadcast);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onStop();

	}


	void startImageAnimation(final TextView img_1,
			final TextView img_2,
			final ArrayList<String> urls){



		final Animation fade_in_anim=AnimationUtils.loadAnimation(context, R.anim.fade_in_splash);
		final Animation fade_out_anim=AnimationUtils.loadAnimation(context, R.anim.fade_out_splash);


		final Handler handler_anim = new Handler();
		runnable_anim = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try{
					img_2.setText(urls.get(imageCount));

					img_2.startAnimation(fade_in_anim);
					fade_in_anim.setFillAfter(true);
					fade_in_anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

							Log.d("=====", "Inorbit Animation start" +imageCount);
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							imageCount++;
							if(imageCount==urls.size()){
								imageCount=0;
							}
						}
					});

					handler_anim.postDelayed(runnable_anim, 2500);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};
		handler_anim.postDelayed(runnable_anim,1000);
	}



	/**
	 * 
	 * Dilaog to show the information on how to win shake and win prize
	 * 
	 */
	void showDialog(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Inorbit In");
		View view = getLayoutInflater().inflate(R.layout.win_surprises_layout, null);
		TextView textView  = (TextView) view.findViewById(R.id.text_message);
		TextView textTodo  = (TextView) view.findViewById(R.id.text_to_do);
		textView.setText("To win the Special Offers, you need to..");
		textTodo.setText(getResources().getString(R.string.toWinOffer));
		textView.setTextColor(getResources().getColor(R.color.inorbit_blue));
		textView.setTextSize(20);
		textTodo.setTextSize(18);
		textView.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
		textTodo.setTypeface(InorbitApp.getTypeFace());
		Button bttn = (Button) view.findViewById(R.id.bttn_do_it_now);
		
		bttn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.dismiss();
				registerEvent(getResources().getString(R.string.shake2Win_CompleteProfile));
				Intent intent=new Intent(context,CustomerProfile.class);
				context.startActivity(intent);
				((Activity) context).overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		alertDialogBuilder.setView(view);
		alertDialogBuilder.setCancelable(true);
		alertDialog = alertDialogBuilder.create();
		alertDialog.show();
		
		
	}


	/* on start */
	@Override
	protected void onStart() {
		super.onStart();
		try{
			EventTracker.startFlurrySession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void registerEventWithPara(String eventName,String storeName){
		try{
			Map<String, String> params = new HashMap<String, String>();
			boolean param = false;
			
			if(MALL_NAME==null && MALL_NAME.equalsIgnoreCase("")){
				params.put("MallName", MALL_NAME);
				param = true;
			}else{
				param = false;
			}
			if(USER_ID==null && USER_ID.equalsIgnoreCase("")){
				if(param){
					params.put("UserId", USER_ID);
				}else{
					param = false;
				}
				
				
			}
			if(param){
				EventTracker.logEvent(eventName, params);
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void registerEvent(String eventName){
		try{

			EventTracker.logEvent(eventName, false);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}

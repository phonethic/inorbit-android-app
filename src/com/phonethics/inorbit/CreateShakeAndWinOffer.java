package com.phonethics.inorbit;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.PublicKey;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.CreatBroadCastNew.Back;
import com.phonethics.inorbit.CreatBroadCastNew.BroadCastOfferUpdate;
import com.phonethics.model.RequestTags;

import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class CreateShakeAndWinOffer extends SherlockActivity implements OnClickListener,OnCheckedChangeListener {

	private EditText metOfferTitle;
	private EditText metOfferDesc;
	private Button mbtnChooseImage;
	private Button mbtnCapture;
	private TextView mtvStartDateTime;
	private TextView mtvEndDateTime;
	private EditText metTags;
	private  final int REQUEST_CODE_CUSTOMCAMERA = 8;
	private  final int REQUEST_CODE_POSTGALLERY = 9;
	private  final int REQUEST_CODE_POSTCROPGALLERY = 10;
	private  final int REQUEST_CODE_TAKE_PICTURE = 7;
	private byte[] mByteArr = null ;
	private String FILE_PATH="";
	private String FILE_NAME="";
	private String FILE_TYPE="";
	private Context mContext;
	private ImageView mivThumbPreview;
	private ImageView mivDeleteImg;
	private boolean mbIsImagePost = false;
	private Calendar dateTime;
	private Calendar dateTimeCurrent;
	private long mlMiliseconds;
	private String msStartDate,msEndDate,msStartTime,msEndTime;	
	private long miliseconds;
	private TextView mtvStartPromt;
	private TextView mtvStartDate;
	private TextView mtvStartTime;
	private TextView mtvEndDatePromt;
	private TextView mtvEndDate;
	private TextView mtvEndTime;
	private TextView mtvIsOfferPrompt;
	private RadioGroup mrg_offer_for;
	private RadioButton rdbtn_offer;
	private RadioButton mrbAll;
	private RadioButton mrbMale;
	private RadioButton mrbFemale;
	private RelativeLayout mrlDate;
	private Button mBtnEdit;
	private Button mBtnReview;
	private Button mBtnPost;
	private LinearLayout mllOffer;
	private LinearLayout mllImageOption;
	private ImageView mivmgSeprate1;
	private ImageView mivmgSeprate2;
	private ImageView mivmgSeprate3;
	private ImageView mivmgSeprate4;
	private ImageView mivmgSeprate5;
	private ImageView mivImgCal;
	private ArrayList<String> msarrSelectedStore;
	private SessionManager mSessionManager;
	String msEncodedpath="/sdcard/temp_photo.jpg";
	private BroadCastOfferUpdate mbroadcastOfferUpdate;
	private ProgressBar mPbar;
	private ActionBar mActionbar;
	private boolean mbIsSureShop;
	private boolean mbIsFavouriteStore;
	private boolean mbIsMallWise;
	private boolean mbIsActiveMall;
	private String msPostType;
	private String msSpecialDate;
	private String msDateType;
	private String msMallId;
	private String msPlaceId;
	private int miAddWithImage = 0;
	private boolean rotateImage = false;
	private int angularRotation = 0;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_shake_and_win_offer);
		mContext =this;
		initViews();
		getBundleValues();
	}


	/**
	 * Initialize the layout and class variables
	 * 
	 */
	void initViews(){

		metOfferTitle = (EditText) findViewById(R.id.edtOfferTitle);
		metOfferDesc = (EditText) findViewById(R.id.edtOfferDesc);
		metTags = (EditText) findViewById(R.id.edtTags);
		mbtnChooseImage = (Button) findViewById(R.id.btnChooseImage);
		mbtnCapture = (Button) findViewById(R.id.btnCapture);
		mBtnEdit = (Button) findViewById(R.id.btn_edit);
		mBtnReview = (Button) findViewById(R.id.btn_Review);
		mBtnPost = (Button) findViewById(R.id.btn_post);
		mtvStartPromt = (TextView) findViewById(R.id.text_start_from);
		mtvStartDate = (TextView) findViewById(R.id.text_start_date);
		mtvStartTime = (TextView) findViewById(R.id.text_start_time);
		mtvEndDatePromt = (TextView) findViewById(R.id.text_ends_on);
		mtvEndDate = (TextView) findViewById(R.id.text_end_date);
		mtvEndTime = (TextView) findViewById(R.id.text_end_time);
		mtvIsOfferPrompt = (TextView) findViewById(R.id.text_offer_for);
		mrg_offer_for = (RadioGroup)findViewById(R.id.radigrp_offer_for);
		mrbAll = (RadioButton) findViewById(R.id.rdbtn_all);
		mrbMale = (RadioButton) findViewById(R.id.rdbtn_male);
		mrbFemale = (RadioButton) findViewById(R.id.rdbtn_female);
		mllOffer = (LinearLayout) findViewById(R.id.linear_is_offer);
		mllImageOption = (LinearLayout) findViewById(R.id.linear_image_option);

		mtvStartPromt.setText("Select offer start date");
		mtvEndDatePromt.setText("Select offer end date");
		mtvStartPromt.setVisibility(View.VISIBLE);
		mtvEndDatePromt.setVisibility(View.VISIBLE);
		mtvStartDate.setVisibility(View.GONE);
		mtvStartTime.setVisibility(View.GONE);
		mtvEndDate.setVisibility(View.GONE);
		mtvEndTime.setVisibility(View.GONE);


		mivThumbPreview = (ImageView) findViewById(R.id.imgThumbPreview);
		mivDeleteImg = (ImageView) findViewById(R.id.deleteImage);
		mivmgSeprate1 = (ImageView) findViewById(R.id.img_seprate_1);
		mivmgSeprate2 = (ImageView) findViewById(R.id.img_seprate_2);
		mivmgSeprate3 = (ImageView) findViewById(R.id.img_seprate_3);
		mivmgSeprate4 = (ImageView) findViewById(R.id.img_seprate_4);
		mivmgSeprate5 = (ImageView) findViewById(R.id.img_seprate_5);
		mivImgCal = (ImageView) findViewById(R.id.img_ic_date);
		mrlDate = (RelativeLayout) findViewById(R.id.rel_date);
		mPbar	= (ProgressBar) findViewById(R.id.pBar);
		mPbar.setVisibility(View.GONE);

		metOfferTitle.setTypeface(InorbitApp.getTypeFace());
		metOfferDesc.setTypeface(InorbitApp.getTypeFace());
		metTags.setTypeface(InorbitApp.getTypeFace());
		mbtnChooseImage.setTypeface(InorbitApp.getTypeFace());
		mbtnCapture.setTypeface(InorbitApp.getTypeFace());
		mtvStartPromt.setTypeface(InorbitApp.getTypeFace());
		mtvEndDatePromt.setTypeface(InorbitApp.getTypeFace());
		mtvIsOfferPrompt.setTypeface(InorbitApp.getTypeFace());
		mrbAll.setTypeface(InorbitApp.getTypeFace());
		mrbMale.setTypeface(InorbitApp.getTypeFace());
		mrbFemale.setTypeface(InorbitApp.getTypeFace());

		mbtnChooseImage.setOnClickListener(this);
		mBtnEdit.setOnClickListener(this);
		mBtnReview.setOnClickListener(this);
		mBtnPost.setOnClickListener(this);
		mbtnCapture.setOnClickListener(this);
		mivDeleteImg.setOnClickListener(this);
		mtvStartPromt.setOnClickListener(this);
		mtvEndDatePromt.setOnClickListener(this);
		mtvStartDate.setOnClickListener(this);
		mtvStartTime.setOnClickListener(this);
		mtvEndDate.setOnClickListener(this);
		mtvEndTime.setOnClickListener(this);
		mrg_offer_for.setOnCheckedChangeListener(this);

		dateTime=Calendar.getInstance();
		dateTimeCurrent=Calendar.getInstance();

		mSessionManager =new SessionManager(getApplicationContext());

		mActionbar=getSupportActionBar();
		mActionbar.setTitle(getResources().getString(R.string.createBroadCastHeader));
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.show();

		//miAddWithImage = getRandomNumber();
		//msEncodedpath = "/sdcard/snw"+miAddWithImage+".jpg";
	}

	
	/**
	 * Receive the bundle values
	 * 
	 */
	void getBundleValues(){
		try{
			Bundle mBundle  = getIntent().getExtras();
			if(mBundle!=null){
				mbIsSureShop = mBundle.getBoolean("isSureShop",false);
				mbIsFavouriteStore = mBundle.getBoolean("isFavouriteStore",false);
				mbIsMallWise = mBundle.getBoolean("isMallWise", false);
				mbIsActiveMall = mBundle.getBoolean("isActiveMall", false);
			}
			/**
			 * Offer for special date users
			 * 
			 */
			if(mbIsSureShop){
				
				msPostType = "1";
				
				msSpecialDate = mBundle.getString("special_date");
				msDateType = mBundle.getString("date_type");
				msMallId = mBundle.getString("mall_id");
				msPlaceId = mBundle.getString("place_id");
				
				/**
				 * Offer for favourite users
				 * 
				 */
			}else if(mbIsFavouriteStore){
				
				msPostType = "2";
				
				msPlaceId = mBundle.getString("place_id");
				msMallId = mBundle.getString("mall_id");
				
				/**
				 * Offer for whole favourtie mall
				 * 
				 */
			}else if(mbIsMallWise){
				
				msPostType = "3";
				msPlaceId = mBundle.getString("place_id");
				msMallId = mBundle.getString("mall_id");
				
				/**
				 * Offer for active mall
				 * 
				 */
			}else if(mbIsActiveMall){
				
				msPostType = "4";
				
				msPlaceId = mBundle.getString("place_id");
				msMallId = mBundle.getString("activeMallId");
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishto();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}

	@Override
	public void onClick(View v) {

		if(v.getId()==mbtnChooseImage.getId()){
			openGallery();
		}
		if (v.getId()==mbtnCapture.getId()) {
			takePictureFromCustomCamera();
		}
		if(v.getId()==mivDeleteImg.getId()){
			deleteThisImage();
		}
		if(v.getId()==mtvStartPromt.getId()){
			//selectStartDate();
			chooseStartDate();
		}
		if(v.getId()==mtvStartDate.getId()){
			//selectStartDate();
			chooseStartDate();
		}
		if(v.getId()==mtvStartTime.getId()){
			//selectStartDate();
			chooseStartTime();
		}
		if(v.getId()==mtvEndDatePromt.getId()){
			//selectEndDate();
			chooseEndDate();
		}
		if(v.getId()==mtvEndDate.getId()){
			//selectEndDate();
			chooseEndDate();
		}
		if(v.getId()==mtvEndTime.getId()){
			//selectEndDate();
			chooseEndTime();
		}
		if(v.getId()==mBtnEdit.getId()){
			createOfferView();
		}
		
		if(v.getId()==mBtnPost.getId()){

			if(NetworkCheck.isNetConnected(mContext)){
				postThisOffer();	
			}else{
				showToast("No Internet Connection");
			}

		}
		
		
		if(v.getId()==mBtnReview.getId()){
			if(metOfferTitle.getText().toString().length()==0){
				showToast("Please enter the offer title.");
			} else if(metOfferDesc.getText().toString().length()==0){
				showToast("Please enter offer description.");
			} else {
				reviewOfferView();
			}

		}

	}

	
	/**
	 * Set the layout parameters to create the offer
	 * 
	 */
	void createOfferView(){
		if (mPbar.getVisibility() == View.VISIBLE) {
			return;
		}
		mBtnReview.setVisibility(View.VISIBLE);
		mBtnEdit.setVisibility(View.GONE);
		mBtnPost.setVisibility(View.GONE);
		mllOffer.setVisibility(View.VISIBLE);
		mllImageOption.setVisibility(View.VISIBLE);
		mivmgSeprate3.setVisibility(View.VISIBLE);
		mivmgSeprate4.setVisibility(View.VISIBLE);

		if(mbIsImagePost){
			mivThumbPreview.setVisibility(View.VISIBLE);
			mivDeleteImg.setVisibility(View.VISIBLE);

		}else{
			mivThumbPreview.setVisibility(View.GONE);
			mivDeleteImg.setVisibility(View.GONE);
		}
		metTags.setVisibility(View.VISIBLE);
		metOfferTitle.setFocusableInTouchMode(true);
		//metOfferTitle.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);
		metOfferDesc.setFocusableInTouchMode(true);
		//metOfferDesc.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);

		mtvEndDate.setClickable(true);
		mtvEndTime.setClickable(true);
		mtvEndDatePromt.setClickable(false);
		mtvStartDate.setClickable(true);
		mtvStartTime.setClickable(true);
		mtvStartPromt.setClickable(false);

		mtvEndDate.setBackgroundColor(getResources().getColor(R.color.inorbit_list_background));
		mtvEndTime.setBackgroundColor(getResources().getColor(R.color.inorbit_list_background));
		mtvStartDate.setBackgroundColor(getResources().getColor(R.color.inorbit_list_background));
		mtvStartTime.setBackgroundColor(getResources().getColor(R.color.inorbit_list_background));

	}

	

	/**
	 * Set the layout parameters to view the offer
	 * 
	 */
	void reviewOfferView(){
		mBtnReview.setVisibility(View.GONE);
		mBtnEdit.setVisibility(View.VISIBLE);
		mBtnPost.setVisibility(View.VISIBLE);
		mllOffer.setVisibility(View.GONE);
		mllImageOption.setVisibility(View.GONE);
		mivDeleteImg.setVisibility(View.GONE);
		mivmgSeprate3.setVisibility(View.GONE);
		mivmgSeprate4.setVisibility(View.GONE);
		if(mbIsImagePost){
			mivThumbPreview.setVisibility(View.VISIBLE);
		}else{
			mivThumbPreview.setVisibility(View.GONE);
		}
		metTags.setVisibility(View.GONE);
		metOfferTitle.setFocusable(false);
		metOfferDesc.setFocusable(false);

		mivImgCal.setVisibility(View.GONE);
		mtvEndDate.setClickable(false);
		mtvEndTime.setClickable(false);
		mtvEndDatePromt.setClickable(false);
		mtvStartDate.setClickable(false);
		mtvStartTime.setClickable(false);
		mtvStartPromt.setClickable(false);

		mtvEndDate.setBackgroundColor(Color.WHITE);
		mtvEndTime.setBackgroundColor(Color.WHITE);
		mtvStartDate.setBackgroundColor(Color.WHITE);
		mtvStartTime.setBackgroundColor(Color.WHITE);

	}


	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		if(checkedId == mrbAll.getId()){
			//mrlDate.setVisibility(View.VISIBLE);
		}else if(checkedId == mrbMale.getId()){
			//mrlDate.setVisibility(View.GONE);
		}else if(checkedId == mrbFemale.getId()){
			//mrlDate.setVisibility(View.GONE);
		}
	}

	
	/**
	 * Open the gallery of the device to select the picture for the offer
	 * 
	 */
	private void openGallery() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_POSTGALLERY);
	}

	/**
	 * OPen the custom camera to click the picture for the offer
	 * 
	 */
	private void takePictureFromCustomCamera() {
		try {
			/*Intent intent = new Intent(mContext,CameraView.class);
			intent.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);
			startActivityForResult(intent, REQUEST_CODE_CUSTOMCAMERA);*/
			
			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			if (intent.resolveActivity(getPackageManager()) != null) {
				startActivityForResult(intent,REQUEST_CODE_CUSTOMCAMERA);
			} else {
				showToast("No Supporting Camera Application Found");
			}
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
			InorbitLog.d("cannot take picture");
		}
	}

	/**
	 * Delete/ change the selected image 
	 * 
	 */
	private void deleteThisImage(){
		mivThumbPreview.setVisibility(View.GONE);
		//mReviewImage.setVisibility(View.GONE);
		mByteArr=null;
		FILE_PATH="";
		FILE_NAME="";
		msEncodedpath = "";
		mbIsImagePost =false;
		mivDeleteImg.setVisibility(View.GONE);
	}


	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		try{
			switch (requestCode) {

			/**
			 * 
			 * Activity return here from custom camera.
			 * Request code gets called when user select to take picture for the offer
			 * 
			 */
			
			case REQUEST_CODE_CUSTOMCAMERA:
				if(resultCode == Activity.RESULT_OK){
					/*try{
						Bitmap bitmap = null;
						CameraImageSave cameraImageSave = new CameraImageSave();
						bitmap = cameraImageSave.getBitmapFromFile(480, 480);
						if(bitmap != null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							mByteArr = stream.toByteArray();
							cameraImageSave.deleteFromFile();
							cameraImageSave = null;
							bitmap = null;
						}
						setPhoto(BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length));
					}
					catch(Exception e){
						e.printStackTrace();
					}*/
					
					Uri dataUri = data.getData();
					if (dataUri == null) {
						Cursor cursor = getContentResolver().query(Media.EXTERNAL_CONTENT_URI,
								new String[]{Media.DATA, Media.DATE_ADDED, MediaStore.Images.ImageColumns.ORIENTATION},
								Media.DATE_ADDED, null, Media.DATE_ADDED+" ASC");
						if(cursor != null && cursor.moveToFirst())
						{
						    do {
						        dataUri = Uri.parse(cursor.getString(cursor.getColumnIndex(Media.DATA)));
						    } while(cursor.moveToNext());
						    cursor.close();
						}
					}
					
					angularRotation = getOrientation(dataUri);
					if (angularRotation != 0)
						rotateImage = true;
					else 
						rotateImage = false;
					Back t = new Back();
					t.execute(dataUri);
				}
				break;

				/**
				 * 
				 * Activity return here from gallery.
				 * Request code gets called when user click on "Select from Gallery" option for the offer image
				 * 
				 */	
			case REQUEST_CODE_POSTGALLERY:
				if(resultCode == Activity.RESULT_OK) {
					try {
						Uri imageUri = data.getData();
						angularRotation = getOrientation(data.getData());
						if (angularRotation != 0)
							rotateImage = true;
						else 
							rotateImage = false;
						Back t = new Back();
						showToast(imageUri.toString());
						t.execute(imageUri);
					}catch(Exception e){
						InorbitLog.e("Exception at Create broacast class");
						e.printStackTrace();
					}
					/*Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();
					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);
					c.close();*/
					
					/**
					 * Crop that image to 1:1
					 * 
					 */
					/*cropCapturedImage(imageUri);*/
					
					
				}

				break;


				/**
				 * 
				 * Activity return here when merchant complete the cropping of the image selected frm the gallery.
				 * 
				 */	
			case REQUEST_CODE_POSTCROPGALLERY:
				if (resultCode == Activity.RESULT_OK) {
					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
					FILE_PATH="/sdcard/temp_photo.jpg";

					//Create an instance of bundle and get the returned data
					Bundle extras = data.getExtras();
					//get the cropped bitmap from extras
					Bitmap thePic = extras.getParcelable("data");

					CameraImageSave cameraSaveImage = new CameraImageSave();

					try {
						cameraSaveImage.cretaeFile();
						cameraSaveImage.saveBitmapToFile(thePic);
					} catch(Exception ex) {
						ex.printStackTrace();
					}
					
					try {
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480, 480);
						if(bitmap!=null) {
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
							cameraSaveImage.deleteFromFile();
							mByteArr = stream.toByteArray();
							bitmap = null;
							cameraSaveImage = null;
						}
						rotateImage = false;
						setPhoto(BitmapFactory.decodeByteArray(mByteArr , 0,mByteArr.length), "");
					}
					catch(Exception e){
						InorbitLog.e("Exception at Create broacast class");
						e.printStackTrace();
					}
				}
				break;
			}
			super.onActivityResult(requestCode, resultCode, data);
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}
	
	public int getOrientation(Uri selectedImage) {
	    int orientation = 0;
	    final String[] projection = new String[]{MediaStore.Images.Media.ORIENTATION};      
	    final Cursor cursor = mContext.getContentResolver().query(selectedImage, projection, null, null, null);
	    if(cursor != null) {
	        final int orientationColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION);
	        if(cursor.moveToFirst()) {
	            orientation = cursor.isNull(orientationColumnIndex) ? 0 : cursor.getInt(orientationColumnIndex);
	        }
	        cursor.close();
	    }
	    return orientation;
	}
	
	class Back extends AsyncTask<Uri, String, Bitmap> {
		//byte[] mByteArr = null;
		String picturePath = "";
		Uri uriForImage = null;
		byte[] localImgArr = null;
		@Override
		protected void onPreExecute() {
			mPbar.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}
		
		@Override
		protected Bitmap doInBackground(Uri... params) {
			try {
				String encodedpath="";
				
				SessionManager session = new SessionManager(mContext);
				uriForImage = params[0];
				String[] results = getRealPathFromURI(uriForImage);
				InputStream iStream = new FileInputStream(results[0]);//getContentResolver().openInputStream(uriForImage);
				mByteArr = getBytes(iStream);
				if (mByteArr.length < 2000000) {
					/*String[] filePathColumn={MediaStore.Images.Media.DATA};
					Cursor c = getContentResolver().query(uriForImage, filePathColumn, null, null, null);
					c.moveToFirst();
					int columnIndex=c.getColumnIndex(filePathColumn[0]);*/
					picturePath = results[0];
					//c.getString(columnIndex);
					//c.close();
					FILE_TYPE = results[1];
					publishProgress("Mime Type "+results[1]);
					BitmapFactory.Options opt =  new BitmapFactory.Options();
					
					opt.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(picturePath, opt);
					publishProgress("Width "+ opt.outWidth +" height "+opt.outHeight);
					
					opt.inSampleSize = calculateInSampleSize(opt, 480, 480);
					/*if (mByteArr.length > getResources().getInteger(R.integer.thumbnail_size_limit))
				        opt.inSampleSize = 8;*/
				    publishProgress("In Sample Size "+opt.inSampleSize);
				    opt.inJustDecodeBounds = false;
				    
			        Bitmap thePic = BitmapFactory.decodeFile(picturePath, opt);
			        return thePic;
			        /*CameraImageSave cameraSaveImage = new CameraImageSave();
			    	
					try {
						cameraSaveImage.cretaeFile();
						cameraSaveImage.saveBitmapToFile(thePic);
					} catch(Exception ex) {
						ex.printStackTrace();
					}

					try{
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480, 480);
						cameraSaveImage.deleteFromFile();
						return bitmap;
					}
					catch(Exception e){
						InorbitLog.e("Exception at Create broacast class");
						e.printStackTrace();
					}*/
				} else {
					publishProgress("Image is too big");
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			return null;
		}
		
		public int calculateInSampleSize (BitmapFactory.Options options, int reqWidth, int reqHeight) {
		    // Raw height and width of image
		    final int height = options.outHeight;
		    final int width = options.outWidth;
		    int inSampleSize = 1;
		    
		    if (height > reqHeight || width > reqWidth) {
		    	
		        final int halfHeight = height / 2;
		        final int halfWidth = width / 2;
		        
		        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
		        // height and width larger than the requested height and width.
		        while ((halfHeight / inSampleSize) > reqHeight
		                && (halfWidth / inSampleSize) > reqWidth) {
		            inSampleSize *= 2;
		        }
		    }
		    
		    return inSampleSize;
		}
		
		private String[] getRealPathFromURI(Uri contentURI) {
		    String[] result = new String[2];
		    Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
		    if (cursor == null) { // Source is Dropbox or other similar local file path
		        result[0] = contentURI.getPath();
		        result[1] = "image/jpeg";
		    } else {
		        cursor.moveToFirst(); 
		        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
		        result[0] = cursor.getString(idx);
		        result[1] = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.MIME_TYPE));
		        cursor.close();
		    }
		    return result;
		}
		
		public byte[] getBytes(InputStream inputStream) throws IOException {
		      ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		      int bufferSize = 1024;
		      byte[] buffer = new byte[bufferSize];

		      int len = 0;
		      while ((len = inputStream.read(buffer)) != -1) {
		        byteBuffer.write(buffer, 0, len);
		      }
		      return byteBuffer.toByteArray();
		}
		@Override
		protected void onProgressUpdate(String... values) {
			showToast(""+values[0]);
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			mPbar.setVisibility(View.GONE);
			if (result != null) {
				/*Cursor cursor = mContext.getContentResolver().query(uriForImage,
                        new String[] { MediaStore.MediaColumns.MIME_TYPE },
                        null, null, null);
				if (cursor != null && cursor.moveToNext()) {
				        FILE_TYPE = cursor.getString(0);
				}
				cursor.close();*/
				setPhoto(result, picturePath);
			}
		}
	}
	
	
	
	/**
	 * Crop the selected picture to 1:1
	 * 
	 * @param picUri
	 */
	public void cropCapturedImage(Uri picUri){
		//call the standard crop action intent 
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		//indicate image type and Uri of image
		cropIntent.setDataAndType(picUri, "image/*");
		//set crop properties
		cropIntent.putExtra("crop", "true");
		//indicate aspect of desired crop
		cropIntent.putExtra("aspectX", 1);
		cropIntent.putExtra("aspectY", 1);
		//indicate output X and Y
		cropIntent.putExtra("outputX", 256);
		cropIntent.putExtra("outputY", 256);
		//retrieve data on return
		cropIntent.putExtra("return-data", true);
		//start the activity - we handle returning in onActivityResult
		startActivityForResult(cropIntent, REQUEST_CODE_POSTCROPGALLERY);
	}

	void setPhoto(Bitmap bitmap, String picturePath){
		try{
			if (FILE_TYPE == null || FILE_TYPE.equalsIgnoreCase("")) 
				FILE_TYPE="image/jpeg";
			
			FILE_NAME="temp_photo.jpg";
			FILE_PATH="/sdcard/temp_photo.jpg";
			
			if (rotateImage)
				bitmap = rotateImage(bitmap, picturePath);
			
			mivThumbPreview.setImageBitmap(bitmap);
			mivThumbPreview.setScaleType(ScaleType.FIT_XY);
			//mReviewImage.setImageBitmap(bitmap);
			//mReviewImage.setScaleType(ScaleType.FIT_XY);
			mivDeleteImg.setVisibility(View.VISIBLE);
			mivThumbPreview.setVisibility(View.VISIBLE);
			//mReviewImage.setVisibility(View.VISIBLE);

			//mEditButton.setText(getString(R.string.edit));
			//mEditButton.setTextColor(Color.WHITE);
			//mEditButton.setGravity(Gravity.CENTER);

			mbIsImagePost=true;
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private Bitmap rotateImage(Bitmap bmp, String path) {
        // TODO Auto-generated method stub

        Matrix matrix=new Matrix();

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String orientstring = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientstring != null ? Integer.parseInt(orientstring) : ExifInterface.ORIENTATION_NORMAL;
        int rotateangle = 0;
        if(orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            rotateangle = 90;
        } if(orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            rotateangle = 180;
        }if(orientation == ExifInterface.ORIENTATION_ROTATE_270) {
        	//mByteArr = rotateByteArray(mByteArr);
            rotateangle = 270;
        }
        
        //imageView.setScaleType(ScaleType.CENTER_CROP);   //required
        matrix.setRotate(rotateangle);
        //imageView.setImageMatrix(matrix);



        Bitmap newBit = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        
        //imageView.setImageBitmap(newBit);
        rotateImage = false;
        return newBit;

    }
	
	
	/**
	 * Date picker for the offer start date 
	 * 
	 */
	public void chooseStartDate(){

		dateTime.add(Calendar.DAY_OF_MONTH, 1);
		new DatePickerDialog(mContext, startDate, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
		DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
				dateTime.set(Calendar.YEAR,year);
				dateTime.set(Calendar.MONTH, monthOfYear);
				dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				updateStartDate();
			}
		};

	}
	/**
	 * Time picker for the offer end time
	 * 
	 */
	public void chooseEndTime(){
		new TimePickerDialog(mContext, endTime, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), false).show();
	}

	/**
	 * Date picker for offe end date
	 * 
	 */
	public void chooseEndDate(){

		dateTime.add(Calendar.DAY_OF_MONTH, 1);
		new DatePickerDialog(mContext, endDate, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
		DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
				dateTime.set(Calendar.YEAR,year);
				dateTime.set(Calendar.MONTH, monthOfYear);
				dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				updateEndDate();
			}
		};

	}
	/**
	 * Time picker for offer start time
	 * 
	 */
	public void chooseStartTime(){
		new TimePickerDialog(mContext, startTime, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), false).show();
	}

	DatePickerDialog.OnDateSetListener startDate=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			dateTime.set(Calendar.YEAR,year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateStartDate();

		}
	};

	TimePickerDialog.OnTimeSetListener startTime=new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTime.set(Calendar.MINUTE,minute);
			updateStartTime();
		}
	};

	DatePickerDialog.OnDateSetListener endDate=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			dateTime.set(Calendar.YEAR,year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateEndDate();

		}
	};

	TimePickerDialog.OnTimeSetListener endTime=new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTime.set(Calendar.MINUTE,minute);
			updateEndTime();
		}
	};

	private void updateStartTime() {

		DateFormat  dt2=new SimpleDateFormat(" hh:mm a");
		miliseconds=dateTime.getTimeInMillis();
		//offerTime_start.setText(dt2.format(dateTime.getTime()));
		mtvStartTime.setText(dt2.format(dateTime.getTime()));
	}

	private void updateStartDate() {

		Log.i("Current Date", "Current Date "+dateTimeCurrent.get(Calendar.YEAR)+" "+dateTimeCurrent.get(Calendar.MONTH)+" "+dateTimeCurrent.get(Calendar.DAY_OF_MONTH));
		Log.i("Current Date", "Current Date changed "+dateTime.get(Calendar.YEAR)+" "+dateTime.get(Calendar.MONTH)+" "+dateTime.get(Calendar.DAY_OF_MONTH));
		Date d1=dateTimeCurrent.getTime();
		Date d2=dateTime.getTime();

		if(dateDiff(d2, d1)<0){
			//showToast("Slected date cannot be less than current date");

		}else{			
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dtReview=new SimpleDateFormat("dd-MMM-yyyy");
			//offerDate_start.setText(dt.format(dateTime.getTime()));
			mtvStartDate.setText(dtReview.format(dateTime.getTime()));
			mtvStartDate.setVisibility(View.VISIBLE);
			mtvStartTime.setVisibility(View.VISIBLE);
			mtvStartPromt.setText("From :");

		}
	}

	private void updateEndTime() {

		DateFormat  dt2=new SimpleDateFormat(" hh:mm a");
		miliseconds=dateTime.getTimeInMillis();
		//offerTime_end.setText(dt2.format(dateTime.getTime()));
		mtvEndTime.setText(dt2.format(dateTime.getTime()));


	}

	private void updateEndDate() {

		Log.i("Current Date", "Current Date "+dateTimeCurrent.get(Calendar.YEAR)+" "+dateTimeCurrent.get(Calendar.MONTH)+" "+dateTimeCurrent.get(Calendar.DAY_OF_MONTH));
		Log.i("Current Date", "Current Date changed "+dateTime.get(Calendar.YEAR)+" "+dateTime.get(Calendar.MONTH)+" "+dateTime.get(Calendar.DAY_OF_MONTH));
		Date d1=dateTimeCurrent.getTime();
		Date d2=dateTime.getTime();

		if(dateDiff(d2, d1)<0){
			//showToast("Slected date cannot be less than current date");

		}else{			
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dtReview=new SimpleDateFormat("dd-MMM-yyyy");
			mtvEndDate.setText(dtReview.format(dateTime.getTime()));
			mtvEndDate.setVisibility(View.VISIBLE);
			mtvEndTime.setVisibility(View.VISIBLE);
			mtvEndDatePromt.setText("Till :");

		}
	}
	/**
	 * Check the difference between two dates
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public long dateDiff(Date d1,Date d2){
		long datediff=0;
		datediff=(d1.getTime()-d2.getTime())/(24 * 60 * 60 * 1000);
		Log.i("daysBetween", "daysBetween "+datediff);
		return datediff;
	}


	/**
	 * 
	 * Network request to send the offer details on server
	 * 
	 */
	void postThisOffer(){
		if (mPbar.getVisibility() == View.VISIBLE) {
			return;
		}
		mPbar.setVisibility(View.VISIBLE);
		JSONObject json = new JSONObject();
		try{
			json.put("place_id", msPlaceId);
			json.put("title", metOfferTitle.getText().toString());
			json.put("description", metOfferDesc.getText().toString());
			json.put("post_type", msPostType);
			json.put("offer_date_time",  changeDateFormat(mtvEndDate.getText().toString())+changeTimeFormat(mtvEndTime.getText().toString()));
			json.put("offer_start_date_time", changeDateFormat(mtvStartDate.getText().toString())+changeTimeFormat(mtvStartTime.getText().toString()));
			json.put("user_id", getUserId());
			json.put("auth_id", getAuthId());

			if(mrg_offer_for.getCheckedRadioButtonId() == mrbMale.getId()){
				json.put("gender", "male");
			}else if(mrg_offer_for.getCheckedRadioButtonId() == mrbFemale.getId()){
				json.put("gender", "female");
			}else{

			}
			if(mbIsImagePost){
				/*//File Array
				try{
					if(!FILE_PATH.equals("") && FILE_PATH.length()!=0){
						byte [] b=imageTobyteArray(FILE_PATH);
						if(b!=null)
							msEncodedpath=Base64.encodeToString(b, Base64.DEFAULT);
						//Log.i("Encode ", "Details : "+msEncodedpath);
					}else{
						msEncodedpath="undefined";
					}
					Log.i("Encode ", "FILE_PATH : "+FILE_PATH);
					Log.i("Encode ", "Details : "+msEncodedpath);
				}catch(Exception ex){
					Log.i("Encode ", "Encode Exception: "+ex.toString());
					ex.printStackTrace();
				}*/
				JSONArray fileArray=new JSONArray();
				JSONObject fileobject = new JSONObject();

				/**
				 * If offer has image attached with it than set the parameters for that
				 * 
				 */
				
				if(mbIsImagePost==true){
					if(mByteArr!=null){
						String user_file=Base64.encodeToString(mByteArr, Base64.DEFAULT);
						fileobject.put("filename", FILE_NAME);
						fileobject.put("filetype", FILE_TYPE);
						fileobject.put("userfile", user_file);
						fileobject.put("angular_rotation", angularRotation+"");
						fileobject.put("title", "");
						fileArray.put(fileobject);
						json.put("file",fileArray);
					}
				}

			}

			if(mbIsSureShop){
				json.put("date_type", msDateType);
				if(msDateType!=null){
					if(msDateType.equalsIgnoreCase("8")) {
						json.put("special_month", msSpecialDate);
					}else{
						json.put("special_date", msSpecialDate);
					}
				}
			}else if(mbIsMallWise){
				json.put("mall_id", msMallId);
			}else if(mbIsFavouriteStore){

			}else if(mbIsActiveMall){
				json.put("active_mall", msMallId);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
		InorbitLog.d("Json Offer string-"+json.toString());
		InorbitLog.d("ImageOffer-"+mbIsImagePost);
		MyClass myClass = new MyClass(mContext);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		myClass.postRequest(RequestTags.Tag_SpecialOffer, headers, json);
	}

	void showToast(String mStr){
		Toast.makeText(mContext, mStr, 0).show();
	}


	private String getUserId(){
		HashMap<String,String>user_details=mSessionManager.getUserDetails();
		return user_details.get("user_id").toString();

	}

	private String getAuthId(){
		HashMap<String,String>user_details=mSessionManager.getUserDetails();
		return user_details.get("auth_id").toString();
	}

	/**
	 * Change the date format 
	 * 
	 * @param msDateToConvert
	 * @return
	 */
	private String changeDateFormat(String msDateToConvert){

		String msOldFormat="dd-MMM-yyyy";
		String msNewFormat = "yyyy-MM-dd";
		String msNewDate = "";

		try{
			SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
			Date d = sdf.parse(msDateToConvert);
			sdf.applyPattern(msNewFormat);
			msNewDate = sdf.format(d);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return msNewDate;
	}

	
	/**
	 * Chane the time format
	 * 
	 * @param msTimeToConvert
	 * @param mbSubmitPattern
	 * @return
	 */
	private String changeTimeFormat(String msTimeToConvert){

		String msOldFormat=" hh:mm a";
		String msNewFormat = " HH:mm";
		String msNewTime = "";

		try{
			SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
			Date d = sdf.parse(msTimeToConvert);
			sdf.applyPattern(msNewFormat);
			msNewTime = sdf.format(d);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return msNewTime;
	}

	byte[] imageTobyteArray(String path){	

		byte[] b = null ;
		if(!path.equals("") || path.length()!=0){
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			options.inDither=true;//optional
			options.inPreferredConfig=Bitmap.Config.RGB_565;//optional
			Bitmap bm = BitmapFactory.decodeFile(path,options);
			options.inSampleSize = calculateInSampleSize(options, 320, 320);
			options.inJustDecodeBounds = false;
			bm=BitmapFactory.decodeFile(path,options);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();  
			bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object   
			b= baos.toByteArray();
		}
		return b;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		if (height > reqHeight || width > reqWidth) {
			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	/**
	 * Broadcast receiver of postThisOffer() when new offer is being created
	 * 
	 * 
	 * @author Nitin
	 *
	 */
	class BroadCastOfferUpdate extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String success = bundle.getString("SUCCESS");
					String code = "";
					String msg = bundle.getString("MESSAGE");
					mPbar.setVisibility(View.GONE);
					if(success.equalsIgnoreCase("true")){
						EventTracker.logEvent(getResources().getString(R.string.mShake2Win_SpecialPostView), false);
						Intent intent1=new Intent(mContext,ViewSpecialPosts.class);
						intent1.putExtra("SureShop",1);
						intent1.putExtra("STORE_ID", msPlaceId);
						startActivity(intent1);
						finish();
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						//Toast.makeText(context, "True", 0).show();
						Toast.makeText(context, msg,Toast.LENGTH_SHORT).show();
					}else if(success.equalsIgnoreCase("false")){
						String message=bundle.getString("MESSAGE");
						Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
						EventTracker.logEvent(getResources().getString(R.string.newPost_PostFailed), false);
						code = bundle.getString("CODE");
						if(code.equalsIgnoreCase("-116")){
							//logOutUser();
						}

						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){
							int code1 = bundle.getInt("CODE");
							String message1=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CategorySearch");
							}
							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}
					}
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
		try{
			if(mbroadcastOfferUpdate!=null){
				mContext.unregisterReceiver(mbroadcastOfferUpdate);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter filter = new IntentFilter(RequestTags.Tag_SpecialOffer);
			mContext.registerReceiver(mbroadcastOfferUpdate = new BroadCastOfferUpdate(), filter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	void finishto(){
		if(metOfferTitle.getText().toString().length()!=0 || metOfferDesc.getText().toString().length()==0){
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle("Inorbit");
			alertDialogBuilder3
			.setMessage("Do you want to discard this offer?")
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.dismiss();
					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
				}
			})
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();
		}else if(!mPbar.isShown()){
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}else{
			//MyToast.showToast(mContext, "Please wait while posting",3);
		}
	}


	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishto();
	}

	

}

package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.HomeGrid.Holder;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SocialConnect extends SherlockActivity{

	ListView 					listView;
	ActionBar 					actionBar;
	ProgressBar					pBar;
	Context						context;
	ArrayList<String> 			arr_titles;
	ArrayList<Integer> 			arr_Icon;
	final String				strActionBarTitle = "Social Connect";
	DBUtil						dbUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_social_connect);

		
		/**
		 * Initialize the layout,class variables and action bar
		 * 
		 */
		context 		= this;
		actionBar		= getSupportActionBar();
		listView		= (ListView) findViewById(R.id.list_social_connect);
		arr_titles		= new ArrayList<String>();
		arr_Icon		= new ArrayList<Integer>();
		dbUtil			= new DBUtil(context);

		
		
		actionBar.setTitle(strActionBarTitle);
		actionBar.setDisplayHomeAsUpEnabled(true);

		
		/**
		 * Social channels and their icons
		 * 
		 */
		arr_titles.add("Facebook");
		arr_titles.add("Twitter");
		arr_titles.add("YouTube");
		arr_titles.add("Pinterest");
		arr_titles.add("Instagram");

		arr_Icon.add(R.drawable.facebook_new);
		arr_Icon.add(R.drawable.twitter_new);
		arr_Icon.add(R.drawable.youtube_new);
		arr_Icon.add(R.drawable.pintrest);
		arr_Icon.add(R.drawable.instagram);


		listView.setAdapter(new ListViewAdapter(context));

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = null;
				try{
					if(pos==0){
						intent = new Intent(context, Twitter.class);
						intent.putExtra("isFacebook", true);
						//intent.putExtra("url", fbUrl);
						context.startActivity(intent);
						((Activity) context).overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}else if(pos==1){
						
						registerEvent(getResources().getString(R.string.socialConnect)+arr_titles.get(pos));
						intent = new Intent(context, Twitter.class);
						intent.putExtra("isFacebook", false);
						intent.putExtra("isTwitter", true);
						
						intent.putExtra("url", getResources().getString(R.string.twitter_url));
						intent.putExtra("ActivityTitle", "Twitter");
						context.startActivity(intent);
						((Activity) context).overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					}else if(pos==2){
						
						registerEvent(getResources().getString(R.string.socialConnect)+arr_titles.get(pos));
						intent = new Intent(context, Twitter.class);
						intent.putExtra("isYouTube", true);
						intent.putExtra("isTwitter", false);
						intent.putExtra("ActivityTitle", "YouTube");
						intent.putExtra("url", getResources().getString(R.string.youtube_url));
						context.startActivity(intent);
						((Activity) context).overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}else if(pos==3){
						
						registerEvent(getResources().getString(R.string.socialConnect)+arr_titles.get(pos));
						intent = new Intent(context, Twitter.class);
						intent.putExtra("isYouTube", false);
						intent.putExtra("isTwitter", false);
						intent.putExtra("ActivityTitle", "Pinterest");
						intent.putExtra("url", getResources().getString(R.string.pintrest_url));
						context.startActivity(intent);
						((Activity) context).overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}else if(pos==4){
						
						registerEvent(getResources().getString(R.string.socialConnect)+arr_titles.get(pos));
						intent = new Intent(context, Twitter.class);
						intent.putExtra("isYouTube", false);
						intent.putExtra("isTwitter", false);
						intent.putExtra("ActivityTitle", "Instagram");
						intent.putExtra("url", getResources().getString(R.string.instagram_url));
						context.startActivity(intent);
						((Activity) context).overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		});
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishTo();
		return true;
	}



	class ListViewAdapter extends BaseAdapter{


		Context context;
		LayoutInflater inflater;

		public ListViewAdapter(Context context) {
			this.context = context;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return arr_titles.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View view = null;
			Holder holder;

			try{


				if(convertView == null ){
					holder = new Holder();
					view = inflater.inflate(R.layout.list_social_connect, null);
					holder.img_icon = (ImageView) view.findViewById(R.id.img_icon);
					holder.text_Title = (TextView) view.findViewById(R.id.text_title);
					holder.text_Title.setTypeface(InorbitApp.getTypeFace());
					view.setTag(holder);
				}
				else{
					view = convertView;
					holder = (Holder) view.getTag();
				}

				try{
					holder.text_Title.setText(arr_titles.get(position));
					holder.img_icon.setImageResource(arr_Icon.get(position));

				}catch(Exception ex){
					ex.printStackTrace();
				}

				//view.setTag(gridItems.get(position).toString());

			}catch(Exception ex){
				ex.printStackTrace();
			}
			return view;
		}
	}


	class Holder{
		ImageView 	img_icon;
		TextView 	text_Title;
	}


	void finishTo(){
		Intent intent = new Intent();
		setResult(5, intent);
		this.finish();	
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}


	@Override
	protected void onResume() {
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onResume();
	}


	@Override
	protected void onPause() {
		
		try{
			EventTracker.endLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onPause();
	}


	/* on start */
	@Override
	protected void onStart() {
		super.onStart();
		
		try{
			EventTracker.startFlurrySession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
	}

	void registerEvent(String eventName){
		try{
			EventTracker.logEvent(eventName, false);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}

	




}

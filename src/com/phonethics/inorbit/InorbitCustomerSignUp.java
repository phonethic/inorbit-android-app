package com.phonethics.inorbit;

import java.util.HashMap;

import org.json.JSONObject;
import org.w3c.dom.Text;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.CategorySearch.CategoreBroadcast;
import com.phonethics.model.RequestTags;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class InorbitCustomerSignUp extends SherlockActivity implements OnClickListener{

	private Context	 	mContext;
	private EditText 	mEtName,mEtNumber;
	private TextView 	mTvTandC;
	private Button	 	mBtnProceed;
	private Dialog		mDialogTc;
	private boolean		mbAgree;
	private DBUtil		mDbUtil;
	private RegisterReceiver broadcast;
	private ActionBar 	mActionBar;
	private ProgressBar 	pBar;
	private SessionManager	session;
	private RadioGroup 			mRadioGroup;
	private RadioButton 		mRbGenMale,mRbFemale;
	private String mSGender;
	private EditText mEtCity;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inorbit_customer_sign_up);

		mContext = this;
		initViews();
		mBtnProceed.setOnClickListener(this);
		mTvTandC.setOnClickListener(this);		

	}

	void initViews(){
		mEtName 		= (EditText) findViewById(R.id.edit_customer_name);
		mEtNumber 		= (EditText) findViewById(R.id.edit_customer_number);
		mEtCity 		= (EditText) findViewById(R.id.edit_customer_city);
		mTvTandC 		= (TextView) findViewById(R.id.text_tandc);
		mBtnProceed 	= (Button) findViewById(R.id.bttn_proceed);
		pBar			= (ProgressBar) findViewById(R.id.pBar_register);
		mRadioGroup			= (RadioGroup)findViewById(R.id.radioGroup);
		mRbGenMale			= (RadioButton)findViewById(R.id.genMale);
		mRbFemale			= (RadioButton)findViewById(R.id.genFemale);
		pBar.setVisibility(View.GONE);
		mbAgree			= false;
		mDbUtil			= new DBUtil(mContext);
		mActionBar		=getSupportActionBar();
		mActionBar.setTitle(getResources().getString(R.string.customer)+" Registration");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();
		
		session			= new SessionManager(mContext);
		
		mEtName.setTypeface(InorbitApp.getTypeFace());
		mEtNumber.setTypeface(InorbitApp.getTypeFace());
		mTvTandC.setTypeface(InorbitApp.getTypeFace());
		mBtnProceed.setTypeface(InorbitApp.getTypeFace());
		
		if(mRadioGroup.getCheckedRadioButtonId()==mRbGenMale.getId()) {
			mSGender	= mRbGenMale.getText().toString();
		} else {
			mSGender	= mRbFemale.getText().toString();
		}
		
		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				if(checkedId==mRbGenMale.getId()){
					mSGender	= mRbGenMale.getText().toString();
				}
				else if(checkedId==mRbFemale.getId()){
					mSGender	= mRbFemale.getText().toString();
				}
			}
		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==mBtnProceed.getId()){
			if(validateData()==true){
				EventTracker.logEvent(getResources().getString(R.string.cSignup_SignUpProceed),false);
				showTCDialog();
			}			
		}
		if(v.getId()==mTvTandC.getId()){
			showTCDialog();
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishTo();
		return true;
	}

	void finishTo(){
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	
		
	}
	
	
	private void showTCDialog(){
		mDialogTc		= new Dialog(mContext);
		mDialogTc.setContentView(R.layout.termsandcondition);
		mDialogTc.setTitle("Terms and conditions");
		mDialogTc.setCancelable(false);
		mDialogTc.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		WebView 	mWebView		= (WebView)mDialogTc.findViewById(R.id.tcWeb);
		Button 		mBtnTermDone 	= (Button) mDialogTc.findViewById(R.id.btnTermDone) ;
		Button 		mBtnTermCancel	= (Button) mDialogTc.findViewById(R.id.btnTermCancel);

		mBtnTermDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mbAgree = true;
				mDialogTc.dismiss();
				if(validateData()==true){
					registerThisUser();
				} else {
					
				}			
				//registerThisUser();
			}
		});
		mBtnTermCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mbAgree = false;
				mDialogTc.dismiss();
			}
		});
		mWebView.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.tcURl));
		mDialogTc.show();
	}
	
	
	private boolean validateData(){
		boolean mbProperData = false;
		if(mEtNumber.getText().toString().equalsIgnoreCase("")){
			Toast.makeText(mContext, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
			mbProperData = false;
			mbAgree = false;
		} else if (mEtNumber.getText().toString().length()<10){
			Toast.makeText(mContext, "Mobile number must be of 10 digit.", Toast.LENGTH_SHORT).show();
			mbProperData = false;
			mbAgree = false;
		} else if(mEtCity.getText().toString().length()==0) {
			Toast.makeText(mContext, "Please enter your city", Toast.LENGTH_SHORT).show();
		}/*else if (mbAgree == false) {
			mbProperData = false; 	
		}*/ else {
			mbProperData = true; 
		}
		return mbProperData;
	}
	


	private void registerThisUser(){
		//Toast.makeText(mContext, "Register this user", Toast.LENGTH_SHORT).show();
		
		MyClass myClass = new MyClass(mContext);
		try{
			JSONObject json = new JSONObject();
			json.put("mobile", "91"+mEtNumber.getText().toString());
			//json.put("mobile", mEtNumber.getText().toString());
			json.put("name", mEtName.getText().toString());
			json.put("active_mall", mDbUtil.getActiveMallId());
			json.put("gender", mSGender);
			json.put("city", mEtCity.getText().toString());
			json.put("register_from", getUserInfo());
			
			
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			
			if(NetworkCheck.isNetConnected(mContext)){
				pBar.setVisibility(View.VISIBLE);
				myClass.postRequest(RequestTags.Tag_RegisterUser, headers, json);
			}else{
				pBar.setVisibility(View.GONE);
				Toast.makeText(mContext, "No Internet Connection.!", Toast.LENGTH_SHORT).show();
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
	
	private String getUserInfo() {
		try{
			PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
			 return "android v"+pinfo.versionName+"("+pinfo.versionCode+") " +android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT;
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "android";

	}
	
	class RegisterReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				if(intent!=null){
					pBar.setVisibility(View.GONE);
					String success =  intent.getStringExtra("SUCCESS");
					String message =  intent.getStringExtra("MESSAGE");
					if(success.equalsIgnoreCase("true")){
						//Toast.makeText(mContext, "Registeration Done", Toast.LENGTH_SHORT).show();
						EventTracker.logEvent(getResources().getString(R.string.cSignup_VerifyCodeDone),false);
						Intent intent1=new Intent(mContext, SignupCustomer.class);
						intent1.putExtra("server_message", message);
						intent1.putExtra("mobileno", "+91"+mEtNumber.getText().toString());
						startActivityForResult(intent1, 2);
						((Activity) mContext).overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}else{
						Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
						EventTracker.logEvent(getResources().getString(R.string.cSignup_VerifyCodeInvalid),false);
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.i("RESULT ", "RESULT requestCode "+requestCode+" ResultCode "+resultCode);
		if(requestCode==10){

		}
		if(resultCode==2){
			if(session.isLoggedInCustomer()){
				finish();
				overridePendingTransition(0,R.anim.shrink_fade_out_center);
			}
		}
	}
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EventTracker.endLocalyticsSession(getApplicationContext());

	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter filter = new IntentFilter(RequestTags.Tag_RegisterUser);
			broadcast = new RegisterReceiver();
			mContext.registerReceiver(broadcast, filter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		finishTo();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();

	}


	/* on start */
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		super.onStop();
		EventTracker.endFlurrySession(getApplicationContext());
		if(broadcast!=null){
			mContext.unregisterReceiver(broadcast);
			//Toast.makeText(mContext, "onStop", Toast.LENGTH_SHORT).show();
		}
	}

	
}

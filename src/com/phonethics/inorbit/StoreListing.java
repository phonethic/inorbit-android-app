package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.actionbarsherlock.view.Window;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.phoenthics.adapter.StoreAdapter;
import com.phonethics.adapters.ActionAdapter;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.RequestTags;
import com.phonethics.model.StoreInfo;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.gesture.GestureLibrary;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class StoreListing extends SherlockActivity implements OnNavigationListener,OnClickListener{

	private Activity 			mContext;
	private ActionBar 			mActionbar;
	private DBUtil				mDbutil;
	private SessionManager 		mSession;
	private EditText 			mEtSearchText;
	private ProgressBar 		mProg;
	private TextView 			mTvSearchCounter;
	private PullToRefreshListView mLvStores;
	private ArrayList<String>	mArrMallAreas;
	private ArrayList<StoreInfo> mArrStores;
	private ProgressBar			pBar;
	private SearachBroadcast mSerachBroadCast;
	private boolean mbIsRefreshing  = true;
	private GestureLibrary gestureLib;
	private int preVisibleItem = 0;
	private ListView mLv;
	private int miPreviousStoreCount = 0;
	private boolean mbSearchClicked= false;
	private boolean mbCallFromSearch = false;
	private boolean mbStoreRefresh = false;
	private boolean mbOfferRefresh = false;
	private DataVersionReceiver mdataVersionReciver;
	private int miLocalRecors;
	private TextView mtvErrorMessage;
	private AverageRating mAvgRating;
	ArrayList<String> mAvgStoreRating = new ArrayList<String>();
	ArrayList<String> mPlaceId = new ArrayList<String>();
	ArrayList<String> mTotalRate = new ArrayList<String>();
	StoreAdapter _mAdapterStore ;
	String selectedStoreId="";
	int selectedPosition;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_listing);
		setSupportProgressBarIndeterminateVisibility(false); 

		mContext 	= this;

		/**
		 * initialize layout 
		 * 
		 */
		initViews();

		/**
		 * initialize actionbar 
		 * 
		 */

		initActionBar();


		/**
		 * initialize class objects 
		 * 
		 */
		initObjects();

		/**
		 * create list of action bar 
		 * 
		 */
		creatActionBarList();



		/**
		 * 
		 * chechk whether this activity has been called from customer dashboard or from search panel
		 * 
		 * 
		 */
		Bundle mBundle = getIntent().getExtras();
		if(mBundle!=null){
			mbCallFromSearch = mBundle.getBoolean("callFromSearch",false);
		}


		mLvStores.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				//if(position!=1){

				Intent intent=new Intent(mContext, StoreDetails.class);
				intent.putExtra("STORE_INFO", mArrStores.get(position-1));
				intent.putExtra("MALL_NAME", getActiveAreaName());
				intent.putExtra("place_id", mArrStores.get(position-1).getId());
				intent.putExtra("mall_id", mArrStores.get(position-1).getMall_id());
				intent.putExtra("category", mArrStores.get(position-1).getCategory());
				selectedStoreId = mArrStores.get(position-1).getId();
				selectedPosition = position-1;
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				setSupportProgressBarIndeterminateVisibility(false);


				//}
			}
		});

		mLvStores.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				InorbitLog.d("Scroll State "+mLvStores.getState()+" -- "+scrollState);
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				
				/**
				 * logic to show and hide the top ornage bar [below the action bar]
				 * invisible on scolling up
				 * visible on scrolling down
				 * 
				 */


				InorbitLog.d("Pre Visible = "+preVisibleItem+" Firstvisible Visible = "+firstVisibleItem);
				if(preVisibleItem!=firstVisibleItem){
					if((preVisibleItem<firstVisibleItem)){
						//InorbitLog.d("Scrolling down");
						mTvSearchCounter.setVisibility(View.INVISIBLE);
					}else{
						//InorbitLog.d("Scrolling Up");
						mTvSearchCounter.setVisibility(View.VISIBLE);
						mTvSearchCounter.bringToFront();
					}
				}

				preVisibleItem = firstVisibleItem;
				if(preVisibleItem==0 && firstVisibleItem==0){
					mTvSearchCounter.setVisibility(View.INVISIBLE);
				}
			}
		});
		
		//mIvSearch.setOnClickListener(this);
		mEtSearchText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					int miTextlength = mEtSearchText.getText().length();
					String text = mEtSearchText.getText().toString().toLowerCase();
					if(Conifg.useServices_c){

					}else{
						final ArrayList<StoreInfo> STORE_INFOS_Temp = new ArrayList<StoreInfo>();
						for (int i = 0; i < mArrStores.size(); i++){
							StoreInfo storeInfo  = mArrStores.get(i);
							if(miTextlength <= storeInfo.getName().length()){
								if (/*text.equalsIgnoreCase((String) storeInfo.getName().subSequence(0, miTextlength))*/
										storeInfo.getName().toLowerCase().contains(text)){
									STORE_INFOS_Temp.add(storeInfo); 
									InorbitLog.d(mEtSearchText.getText().toString());
								}else if(storeInfo.getDescription().contains(text)){
									STORE_INFOS_Temp.add(storeInfo); 
									InorbitLog.d(mEtSearchText.getText().toString());
								}
							}else if(storeInfo.getDescription().contains(text)){
								STORE_INFOS_Temp.add(storeInfo); 
								InorbitLog.d(mEtSearchText.getText().toString());
							}
						}

						setTotalStores(STORE_INFOS_Temp.size());


						_mAdapterStore = new StoreAdapter(mContext, STORE_INFOS_Temp, true);
						mLvStores.setAdapter(_mAdapterStore);
						mLvStores.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1, int position,
									long arg3) {
								// TODO Auto-generated method stub
								try{
									Intent intent=new Intent(mContext, StoreDetails.class);
									intent.putExtra("STORE_INFO", STORE_INFOS_Temp.get(position-1));
									intent.putExtra("MALL_NAME", getActiveAreaName());
									intent.putExtra("place_id", STORE_INFOS_Temp.get(position-1).getId());
									intent.putExtra("mall_id", STORE_INFOS_Temp.get(position-1).getMall_id());
									intent.putExtra("category", STORE_INFOS_Temp.get(position-1).getCategory());
									selectedStoreId = STORE_INFOS_Temp.get(position-1).getId();
									selectedPosition = position-1;
									startActivityForResult(intent, 2);
									overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);	
								}catch(Exception ex){
									ex.printStackTrace();
								}
							}
						});

					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, 
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}


		});



		mLvStores.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				mbIsRefreshing = true;
				try{
					if(NetworkCheck.isNetConnected(mContext)){
						if (mEtSearchText.isShown()) {
							animateEditText();
							invalidateActionBar();
						}
						loadStores();
//						callAverageRatingApi("");
					}else{
						mbIsRefreshing = false;
						MyToast.showToast(mContext, getResources().getString(R.string.noInternetConnection),1);
						mLvStores.onRefreshComplete();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		});

		//to call avg rating api
		//callAverageRatingApi();
	}


	/**
	 * initialize layout 
	 * 
	 */
	void initViews(){
		mEtSearchText 		= (EditText) findViewById(R.id.searchText);
		mEtSearchText.setVisibility(View.GONE);
		mProg				= (ProgressBar)findViewById(R.id.pBar_storeListing);
		mTvSearchCounter	= (TextView)findViewById(R.id.txtCounter);
		mLvStores			= (PullToRefreshListView)findViewById(R.id.list_stores);
		mtvErrorMessage = (TextView)findViewById(R.id.text_errorMessage);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		SubMenu subMenu1 = menu.addSubMenu("Search");
		if(mbSearchClicked){
			MenuItem subMenu1Item = subMenu1.getItem();
			subMenu1Item.setIcon(R.drawable.ic_navigationcancel_new);
			subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}else{
			MenuItem subMenu1Item = subMenu1.getItem();
			subMenu1Item.setIcon(R.drawable.ic_action_search_new);
			subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}

		return true;
	}

	/**
	 * initialize actionbar 
	 * 
	 */
	
	void initActionBar() {
		mActionbar	= getSupportActionBar();
		mActionbar.setTitle(getString(R.string.actionBarTitle));
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.show();
	}
	
	/**
	 * initialize class objects 
	 * 
	 */
	
	void initObjects(){
		mDbutil 	= new DBUtil(mContext);
		//mInorbitDb  = InobitStoresDb.getInstance(mContext);

		mSession	= new SessionManager(getApplicationContext());

		/**
		 * 
		 * Array list of mall locations [used for action bar]
		 * 
		 */
		mArrMallAreas	= new ArrayList<String>();

		/**
		 * Array list of stores
		 * 
		 */
		mArrStores = new ArrayList<StoreInfo>();
		_mAdapterStore = new StoreAdapter(mContext, mArrStores, true);
		mLvStores.setAdapter(_mAdapterStore);
	}
	
	
	/**
	 * create action bar list
	 * 
	 */
	void creatActionBarList(){
		/**
		 * Get malls to initialize the action bar
		 * 
		 */
		mArrMallAreas = mDbutil.getAllAreas();


		ActionAdapter mAdAction = new ActionAdapter(mContext, 0, 0, mArrMallAreas);
		mActionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		mActionbar.setListNavigationCallbacks(mAdAction, this);
		mActionbar.setSelectedNavigationItem(mArrMallAreas.indexOf(getActiveAreaName()));
	}


	/**
	 *
	 * @return mall id of the active mall [selected on the action bar] 
	 */
	String getActiveAreaId(){
		return mDbutil.getActiveMallId();
	}

	/**
	 * 
	 * @return Mall location [Malad - Vashi] based on the selected mall id
	 */

	String getActiveAreaName(){
		return mDbutil.getAreaNameByInorbitId(getActiveAreaId());
	}


	/** 
	 * @return Mall name [Malad - Vashi] based on the selected mall id
	 */
	String getMallName(){
		return mArrMallAreas.get((mActionbar.getSelectedNavigationIndex())+1);
	}


	/** 
	 * @return Mall id of selected mall on action bar
	 */
	String getCurrentMallId(){
		return mDbutil.getMallIdByPos(""+((mActionbar.getSelectedNavigationIndex())+1));
	}


	/** 
	 * @return Place parent of the selected mall
	 */
	String getCurrentMallPlaceParent(){
		return mDbutil.getMallPlaceParentByMallID(getCurrentMallId());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase(getString(R.string.actionBarTitle))){
			//Intent intent=new Intent(mContext,HomeGrid.class);
			//startActivity(intent);
			Intent intent = new Intent();
			setResult(5, intent);
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}

		if(item.getTitle().toString().equalsIgnoreCase("Search")){
			if(mbSearchClicked){
				mbSearchClicked = false;
			}else{
				mbSearchClicked = true;
			}
			animateEditText();
			invalidateActionBar();

		}
		return true;
	}



	void invalidateActionBar(){
		this.supportInvalidateOptionsMenu();
	}

	private void createInhouseAnalyticsEvent(String flag, String activityId, String mallId) {
		Uri uri = new Uri.Builder().scheme("content").authority(mContext.getResources().getString(R.string.authority)).
				appendPath("/insert").build();
        ContentValues cv = new ContentValues(6);
		cv.put("UDM_ID", mSession.getUdmIDForCustomer());
		cv.put("DATE", new Date().toString());
		cv.put("FLAG", flag);
		cv.put("ACTIVITY_ID", activityId);
		cv.put("MALL_ID", mallId);
		cv.put("SYNC_STATUS", -1);
		mContext.getContentResolver().insert(uri, cv);
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub

		try{
			String value = mDbutil.getMallIdByPos(""+(itemPosition+1));
			mDbutil.getMallCity(value);
			mDbutil.setActiveMall(value);
			mArrStores.clear();
			
			createInhouseAnalyticsEvent("0", "8", mDbutil.getActiveMallId());
			
			/**
			 * Get the entries from database if cached else check for network and get the entries from server
			 */
			
			if (Tables.getCurrentCategory()==Tables.CATEGORY_OFFER) {
				if(NetworkCheck.isNetConnected(mContext)){
					loadStores();
				}else{
					mLvStores.setVisibility(View.GONE);
					mtvErrorMessage.setText(getResources().getString(R.string.noInternetConnection));
					mtvErrorMessage.setTypeface(InorbitApp.getTypeFace());
					mProg.setVisibility(View.GONE);
				}
			} else {
				miLocalRecors = mDbutil.getTotalStores(Tables.getCurrentCategory(),getCurrentMallPlaceParent());
				InorbitLog.d("Total Records "+miLocalRecors);
				
				if (miLocalRecors==0) { /** if data is not there in databse */
					if(NetworkCheck.isNetConnected(mContext)) { /** if network is available than request for data */
						loadStores();
					} else {
						/**
						 * No local records and no Internet connection
						 */
						mLvStores.setVisibility(View.GONE);
						mtvErrorMessage.setVisibility(View.VISIBLE);
						mtvErrorMessage.setText(getResources().getString(R.string.noInternetConnection));
						mtvErrorMessage.setTypeface(InorbitApp.getTypeFace());
						mProg.setVisibility(View.GONE);
					}
				} else { /** Load the store list from local database*/
					mtvErrorMessage.setVisibility(View.GONE);
					mLvStores.setVisibility(View.VISIBLE);
					mArrStores.clear();
					mArrStores = mDbutil.getStores(Tables.getCurrentCategory(),getCurrentMallPlaceParent());
					//callAverageRatingApi();

					InorbitLog.d("Database Store Size "+mArrStores.size());
					_mAdapterStore = new StoreAdapter(mContext, mArrStores, true);
					mLvStores.setAdapter(_mAdapterStore);
					_mAdapterStore.notifyDataSetChanged();
					goneNoConnectionView(true);
					setTotalStores(mArrStores.size());
					mProg.setVisibility(View.GONE);
					
					int miCount = mDbutil.getCountOfRefreshRable();
					if(miCount==0) {
						checkVersion();
					} else {
						if(mDbutil.compareOldAndNewValuesOfStores(getCurrentMallId())) {
							mbStoreRefresh = true;
							InorbitLog.d("Refresh Data Require");
							loadStores();
						} else if (mDbutil.compareOldAndNewValuesOfOffers(getCurrentMallId())) {
							mbOfferRefresh = true;
							InorbitLog.d("Refresh Data Require");
							loadStores();
						}else {
							checkVersion();
						}
					}

				}
			}
			registerEvent(getResources().getString(R.string.category_List));
		}catch(Exception ex){
			ex.printStackTrace();
		}




		return false;
	}


	/**
	 *Get data version of the server, if it is not equal to the data version on the local database than refresh the whole data 
	 *for present selected category[directory/entertainment/pamper zone]
	 * 
	 */
	private void checkVersion(){

		InorbitLog.d("Refresh Version api call");
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		MyClass myClass = new MyClass(mContext);
		if(NetworkCheck.isNetConnected(mContext)) {
			myClass.getStoreRequest(RequestTags.Tag_GetDataVersion, null, headers);
		}
	}



	/**
	 * 
	 * Network call to get the information from the server 
	 * 
	 */
	private void loadStores() {
		try{
			if (NetworkCheck.isNetConnected(mContext)){

				HashMap<String, String> _mHeaders = new HashMap<String, String>();
				_mHeaders.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));


				List<NameValuePair> _mListNameValuePairs = new ArrayList<NameValuePair>();

				/**
				 * if user has selected Directory option on customer dashboard
				 * 
				 */
				if(Tables.getCurrentCategory() == Tables.CATEGORY_All){
					_mListNameValuePairs.add(new BasicNameValuePair(getResources().getString(R.string.id_key_shop), getCurrentMallPlaceParent()));
					_mListNameValuePairs.add(new BasicNameValuePair("page", "1"));
					_mListNameValuePairs.add(new BasicNameValuePair("count", "-1"));
				}

				/**
				 * if user has selected Offers option on customer dashboard
				 * 
				 */

				else if(Tables.getCurrentCategory() == Tables.CATEGORY_OFFER){
					_mListNameValuePairs.add(new BasicNameValuePair(getResources().getString(R.string.id_key_shop), 
							getCurrentMallPlaceParent()));
					_mListNameValuePairs.add(new BasicNameValuePair("search", ""));
					_mListNameValuePairs.add(new BasicNameValuePair("offer", "1"));
					_mListNameValuePairs.add(new BasicNameValuePair("page", "1"));
					_mListNameValuePairs.add(new BasicNameValuePair("count", "-1"));
				}


				/**
				 * if user has selected (Entertainment / shopping / dine / pamper zone) option on customer dashboard.
				 * 
				 */


				else{
					_mListNameValuePairs.add(new BasicNameValuePair(getResources().getString(R.string.id_key_shop), 
							getCurrentMallPlaceParent()));
					_mListNameValuePairs.add(new BasicNameValuePair("category_id", Tables.getCurrentCategory()));
					_mListNameValuePairs.add(new BasicNameValuePair("count", "-1"));
					_mListNameValuePairs.add(new BasicNameValuePair("page", "1"));
				}


				MyClass myClass = new MyClass(mContext);
				myClass.getStoreRequest(RequestTags.TagSearchStores, _mListNameValuePairs, _mHeaders);



				if(mArrStores.size()==0){
					mProg.setVisibility(View.VISIBLE);
				} else {
					setSupportProgressBarIndeterminateVisibility(true);
				}
				
				goneNoConnectionView(true);

			} else {
				goneNoConnectionView(false);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private void goneNoConnectionView(boolean isGone){
		if(isGone){
			//mIvConnctionErr.setVisibility(View.GONE);
			//mTvSearchCounter.setText("");
			//mTvCounterBack.setText("");
		} else {
			//mIvConnctionErr.setVisibility(View.VISIBLE);
			mProg.setVisibility(View.GONE);
			//mTvCounterBack.setText("No Internet Connection");
			showToast(getResources().getString(R.string.noInternetConnection));
			//mLvStores.setVisibility(View.GONE);
		}
	}

	private void finishThisActivity(){
		if(mbCallFromSearch){

		}else{
			//Intent intent=new Intent(mContext,HomeGrid.class);
			//startActivity(intent);
		}
		Intent intent = new Intent();
		setResult(5, intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	void showToast(String msg){
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private void animateEditText(){
		if(mEtSearchText.isShown()){

			//searchText.setVisibility(View.GONE);
			mEtSearchText.setText("");
			//mIvSearch.setBackgroundResource(R.drawable.action_search_holo_light);
			Animation anim = new ScaleAnimation(1f, 1f, 1, 0, 50, 0);
			anim.setDuration(500);
			mEtSearchText.startAnimation(anim);
			anim.setFillAfter(true);


			anim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					mEtSearchText.setVisibility(View.GONE);
					//mLinearSearch.setVisibility(View.GONE);
					InputMethodManager imm = (InputMethodManager)getSystemService( Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(mEtSearchText.getWindowToken(), 0);
					mbSearchClicked = true;
				}
			});

		}else{


			mEtSearchText.setVisibility(View.VISIBLE);
			mEtSearchText.bringToFront();
			//mLinearSearch.setVisibility(View.VISIBLE);
			//mIvSearch.setBackgroundResource(R.drawable.navigation_cancel_holo_light);


			Animation anim = new ScaleAnimation(1f, 1f, 0, 1, 100, 0);
			anim.setDuration(500);
			mEtSearchText.startAnimation(anim);
			anim.setFillAfter(true);

			anim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					mEtSearchText.setOnFocusChangeListener(new OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							mEtSearchText.post(new Runnable() {
								@Override
								public void run() {
									InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
									imm.showSoftInput(mEtSearchText, InputMethodManager.SHOW_IMPLICIT);
								}
							});
						}
					});
					mEtSearchText.requestFocus();
					mbSearchClicked = false;
				}
			});
		}
	}


	/**
	 * Broadcast receiver for the network call which is getting called through loadStores();
	 */
	protected class SearachBroadcast extends BroadcastReceiver{

		@Override
		public void onReceive(final Context context, Intent intent) {
			// TODO Auto-generated method stub
			try {
				mLvStores.onRefreshComplete();
				
				mtvErrorMessage.setVisibility(View.GONE);
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String  SEARCH_STATUS = bundle.getString("SUCCESS");
					mArrStores.clear();

					if(SEARCH_STATUS.equalsIgnoreCase("true")){


						if(mbStoreRefresh) {
							/*mbBackRefresh = false;*/
							mDbutil.updateStoresOldValue(getCurrentMallId());


						} /*else if (mbOfferRefresh) {
							mDbutil.updateOffersOldValue(getCurrentMallId());
						}*/



						mArrStores = intent.getParcelableArrayListExtra("StoreInfo");
						
						mLvStores.setVisibility(View.VISIBLE);
						mLvStores = (PullToRefreshListView)findViewById(R.id.list_stores);

						_mAdapterStore = new StoreAdapter(mContext, mArrStores, true);
						mLvStores.setAdapter(_mAdapterStore);
						//_mAdapterStore.notifyDataSetChanged();
						
						setTotalStores(mArrStores.size());

						callAverageRatingApi();

					}else{
						mLvStores.setVisibility(View.GONE);
						String msMessage = bundle.getString("MESSAGE");
						String msCode = bundle.getString("CODE");
						if(msCode.equalsIgnoreCase("-155")){
							Toast.makeText(context, msMessage, Toast.LENGTH_LONG).show();
						}
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}




	/**
	 * Broadcast receiver for the network call which is getting called through checkVersion();
	 */
	protected class DataVersionReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context arg0, Intent intent) {
			// TODO Auto-generated method stub
			try{
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")) {
						/*if(StoreVersionCount.isNewDataRequire(getCurrentMallId())){
							loadStores();
						}*/

						/*if(mDbutil.isRefershRequire(getCurrentMallId())) {
							loadStores();
						}*/
						if(mDbutil.compareOldAndNewValuesOfStores(getCurrentMallId())) {
							mbStoreRefresh = true;
							InorbitLog.d("Refresh Data Require store");
							loadStores();
						} /*else if (mDbutil.compareOldAndNewValuesOfOffers(getCurrentMallId())) {
							mbOfferRefresh = true;
							InorbitLog.d("Refresh Data Require Offers");
							loadStores();
						}*/

					}					
				}
			} catch(Exception ex){
				ex.printStackTrace();
			}

		}

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//imageLoaderList.clearCache();

		try{
			EventTracker.endFlurrySession(getApplicationContext());
			if(mSerachBroadCast!=null){
				mContext.unregisterReceiver(mSerachBroadCast);
			}
			if(mdataVersionReciver!=null){
				mContext.unregisterReceiver(mdataVersionReciver);

			}
			if(mAvgRating!=null){
				mContext.unregisterReceiver(mAvgRating);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}


	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{

			EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter filter = new IntentFilter(RequestTags.TagSearchStores);
			mContext.registerReceiver(mSerachBroadCast = new SearachBroadcast(), filter);



			IntentFilter dataVersionFilter = new IntentFilter(RequestTags.Tag_GetDataVersion);
			mContext.registerReceiver(mdataVersionReciver = new DataVersionReceiver(), dataVersionFilter);

			//for average rating api
			IntentFilter mAvgRatingfilter = new IntentFilter(RequestTags.TAG_AVG_STORE_RATING);
			mContext.registerReceiver(mAvgRating = new AverageRating(),mAvgRatingfilter);

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}


	/**
	 * Set the total stores count for the header.
	 * 
	 * @param _miTotalStores total count of the stores
	 */
	private void setTotalStores(int _miTotalStores){


		if(Tables.getCurrentCategory() == Tables.CATEGORY_All){
			this.mTvSearchCounter.setText("Directory - "+_miTotalStores+" Stores");


		} else if (Tables.getCurrentCategory() == Tables.CATEGORY_DINE) {
			this.mTvSearchCounter.setText("Dine - "+_miTotalStores+" Stores");

		} else if (Tables.getCurrentCategory() == Tables.CATEGORY_ENTERTAINMENT) {
			this.mTvSearchCounter.setText("Entertainment - "+_miTotalStores+" Stores");


		} else if (Tables.getCurrentCategory() == Tables.CATEGORY_PAMPERZONE) {
			this.mTvSearchCounter.setText("Pamper Zone - "+_miTotalStores+" Stores");


		} else if (Tables.getCurrentCategory() == Tables.CATEGORY_SHOP) {
			this.mTvSearchCounter.setText("Shop - "+_miTotalStores+" Stores");
		}


		this.mTvSearchCounter.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);

	}


	/**
	 * Write store information in local database
	 * 
	 * @param mArrStoreInfos Store info array
	 */
	private void writeToDatabase(ArrayList<StoreInfo> mArrStoreInfos){
		if(mbIsRefreshing || mbStoreRefresh || mbOfferRefresh){
			mbStoreRefresh = false;
			mbOfferRefresh = false;

			/**
			 * Delete old records and upadate new records
			 * 
			 */
			mDbutil.deleteStrores(Tables.getCurrentCategory(), getCurrentMallPlaceParent());

			/**
			 * add information of the stores
			 * 
			 */
			mDbutil.createStoreTable(mArrStoreInfos, Tables.getCurrentCategory());
			mbIsRefreshing = false;
			//mDbutil.createStoreTable(mArrStores, Tables.getCurrentCategory());
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		finishThisActivity();  
	}

	/**
	 * Used for analytics, to track the user actions
	 * 
	 * @param eventName
	 */
	void registerEvent(String eventName){
		try{
			InorbitLog.d("Category Name "+Tables.getCurrentCategoryForEvent());
			boolean params = false;
			Map<String, String> param = new HashMap<String, String>();

			if(getActiveAreaName()!=null && !getActiveAreaName().equalsIgnoreCase("")){
				param.put("MallName",getActiveAreaName());
				params = true;
			}else{
				params = false;
			}

			if(Tables.getCurrentCategoryForEvent()!=null && !Tables.getCurrentCategoryForEvent().equalsIgnoreCase("")){
				if(params){
					param.put("Category", Tables.getCurrentCategoryForEvent()+"-"+getActiveAreaName());
					params = true;
				}else{
					params = false;
				}

			}else{
				params = false;
			}

			if(params){
				EventTracker.logEvent(eventName, false);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	private void callAverageRatingApi() {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		JSONObject jsonObj = new JSONObject();
		try {
			JSONArray places = new JSONArray();
			InorbitLog.d("Places "+mArrStores.size());
			
			for(int i=0;i<mArrStores.size();i++){
				//places.add(mArrStores.get(i).getId());
				InorbitLog.d("Places put"+mArrStores.get(i).getId());
				places.put(mArrStores.get(i).getId());
			}
			
			jsonObj.put("place_id", places);
			InorbitLog.d("Json =="+jsonObj.toString());
			MyClass myclass = new MyClass(mContext);
			myclass.postRequest(RequestTags.TAG_AVG_STORE_RATING, headers, jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	class AverageRating extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")) {
						mAvgStoreRating = intent.getStringArrayListExtra("AVG_RATING");
						mPlaceId = intent.getStringArrayListExtra("PLACE_ID");
						mTotalRate = intent.getStringArrayListExtra("TOTAL_RATE");
						
						for(int i=0 ; i<mPlaceId.size(); i++){
							InorbitLog.d("Places get" + mPlaceId.get(i));
							for (int j=0; j<mArrStores.size(); j++) {
								if (mArrStores.get(j).getId().equalsIgnoreCase(mPlaceId.get(i))) {
									mArrStores.get(j).setTotal_rating(mAvgStoreRating.get(i));
								}
							}
						}
						
						if(Tables.getCurrentCategory()==Tables.CATEGORY_OFFER){

						}else{

							/**
							 * Load the store information on database
							 * 
							 */
							writeToDatabase(mArrStores);
							/*StoreAdapter _mAdapterStore = new StoreAdapter(mContext, mArrStores);
							mLvStores.setAdapter(_mAdapterStore);*/
							_mAdapterStore.notifyDataSetChanged();
							//showToast("Write to Database Done");
							setSupportProgressBarIndeterminateVisibility(false);
							mProg.setVisibility(View.GONE);
						}
					}
					else{
						String message = intent.getStringExtra("MESSAGE");
						showToast(message);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    // Check which request we're responding to
	    if (requestCode == 2) {
	    	callAverageRatingApi();
	    }
	}
}

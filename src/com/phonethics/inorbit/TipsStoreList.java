package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.model.RequestTags;
import com.phonethics.model.StoreInfo;
import com.phonethics.model.TipsManagerResponseModel;
import com.squareup.picasso.Picasso;

public class TipsStoreList extends SherlockActivity {
	private Activity mContext;
	private ListView mStoresListView;
	private ActionBar mActionBar;
	private DBUtil dbUtil;
	private RelativeLayout mStorePBar;
	private StoreMgrTips storeMgrTips;
	private StoreMgrTips mallMgrTips;
	private ArrayList<TipsManagerResponseModel> storeList;
	private StoreListAdapter adapter;
	private ArrayList<String> storeNames;
	private ArrayList<String> storeLogoUrls;
	private static final int TIPS_REQUEST_CODE = 10;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multiple_store_list);
		
		mContext = this;
		mStoresListView = (ListView)findViewById(R.id.mStoreList);
		
		mActionBar	=	getSupportActionBar();
		mActionBar.setTitle("Store List");
		
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();
		dbUtil = new DBUtil(mContext);
		mStorePBar=(RelativeLayout)findViewById(R.id.mProgressBar);
		
		storeList = new ArrayList<TipsManagerResponseModel>();
		adapter = new StoreListAdapter();
		mStoresListView.setAdapter(adapter);
		
		if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) {
			getTipsForMallManager();
		} else {
			final ArrayList<String> store_ids = dbUtil.getAllStoreIDs();
			storeNames = dbUtil.getAllStoresNames();
			storeLogoUrls = dbUtil.getAllStorePhotoUrl();
			getTipsForStoreManager(store_ids);
		}
		
		mStoresListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent(TipsStoreList.this, TipsDisplayActivity.class);
				intent.putExtra("place_id", storeList.get(position).getStoreId());
				startActivityForResult(intent, TIPS_REQUEST_CODE);
			}
		});
	}
	
	private class StoreListAdapter extends BaseAdapter {
		LayoutInflater inflater;
		
		public StoreListAdapter() {
			inflater = mContext.getLayoutInflater();
		}
		
		@Override
		public int getCount() {
			return storeList.size();
		}

		@Override
		public Object getItem(int position) {
			return storeList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView==null) {
				final ViewHolder holder=new ViewHolder();
				convertView=inflater.inflate(R.layout.all_messages_layout,null);
				holder.storeName = (TextView)convertView.findViewById(R.id.mStoreName);
				holder.storeLogo = (ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.mCount = (TextView)convertView.findViewById(R.id.mMsgCount);
				holder.msg_bubble = (RelativeLayout) convertView.findViewById(R.id.msg_background_img);
				
				holder.storeName.setTextColor(Color.BLACK);
				holder.storeLogo.setScaleType(ScaleType.CENTER_INSIDE);
				convertView.setTag(holder);
			}
				
			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.storeName.setText(storeList.get(position).getStoreName());
			
			if (storeList.get(position).getCount() > 0)
				hold.msg_bubble.setBackgroundResource(R.drawable.msg_bubble_blue);
			else
				hold.msg_bubble.setBackgroundResource(R.drawable.msg_bubble_grey);
			
			hold.mCount.setText(storeList.get(position).getCount()+"");
			
			String photo_source = storeList.get(position).getStoreLogo().toString().replaceAll(" ", "%20");
			try {
				if(!photo_source.equalsIgnoreCase("")){
					Picasso.with(mContext).load(getResources().getString(R.string.photo_url)+photo_source)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(hold.storeLogo);
				}
				else{
					hold.storeLogo.setImageResource(R.drawable.ic_launcher);
				}
			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			} catch(Exception e){
				e.printStackTrace();
			}
			
			return convertView;
		}
		
		private class ViewHolder {
			TextView storeName;
			ImageView storeLogo;
			TextView mCount;
			RelativeLayout msg_bubble;
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		IntentFilter getStoreMgrTips = new IntentFilter(RequestTags.TAG_TIP_STORE_MANAGER);
		mContext.registerReceiver(storeMgrTips = new StoreMgrTips(), getStoreMgrTips);
		
		IntentFilter getMallMgrTips = new IntentFilter(RequestTags.TAG_TIP_MALL_MANAGER);
		mContext.registerReceiver(mallMgrTips = new StoreMgrTips(), getMallMgrTips);
	}
	
	@Override
	protected void onStop() {
		if (storeMgrTips != null) {
			mContext.unregisterReceiver(storeMgrTips);
		}
		
		if (mallMgrTips != null) {
			mContext.unregisterReceiver(mallMgrTips);
		}
		
		super.onStop();
	}
	
	private void getTipsForStoreManager(ArrayList<String> store_ids) {
		mStorePBar.setVisibility(View.VISIBLE);
		
		JSONArray jsonArray = new JSONArray();
		for (String storeId: store_ids)
			jsonArray.put(storeId);

		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("reader_id", jsonArray);
			jsonObj.put("user_id", getUserId());
			jsonObj.put("auth_id", getAuthId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		
		MyClass myClass = new MyClass(mContext);
		myClass.postRequest(RequestTags.TAG_TIP_STORE_MANAGER, headers, jsonObj);
	}
	
	private void getTipsForMallManager() {
		mStorePBar.setVisibility(View.VISIBLE);
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("place_id", dbUtil.getPlaceIdByPlaceParent("0")));
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		headers.put("user_id", getUserId());
		headers.put("auth_id", getAuthId());
		
		MyClass myClass = new MyClass(mContext);
		myClass.getStoreRequest(RequestTags.TAG_TIP_MALL_MANAGER, nameValuePairs, headers);
	}
	
	String getUserId() {
		SessionManager mSessionManager = new SessionManager(this);
		HashMap<String,String>user_details = mSessionManager.getUserDetails();
		return user_details.get("user_id").toString();
	}
	
	String getAuthId() {
		SessionManager mSessionManager = new SessionManager(this);
		HashMap<String,String>user_details = mSessionManager.getUserDetails();
		return user_details.get("auth_id").toString();
	}
	
	private class StoreMgrTips extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent.getStringExtra("SUCCESS").equalsIgnoreCase("true")) {
					storeList = intent.getParcelableArrayListExtra("data");
					if (!dbUtil.isManager() || dbUtil.isMultipuleMallsLoggedIn()) {
						for (int i=0; i<storeList.size(); i++) {
							TipsManagerResponseModel tip = storeList.get(i);
							tip.setStoreName(storeNames.get(i));
							tip.setStoreLogo(storeLogoUrls.get(i));
						}
					}
				} else {
					showToast(intent.getStringExtra("MESSAGE"));
				}
				adapter.notifyDataSetChanged();
			} else {
				showToast("Error was generated");
			}
			mStorePBar.setVisibility(View.GONE);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (dbUtil.isManager() && !dbUtil.isMultipuleMallsLoggedIn()) {
			getTipsForMallManager();
		} else {
			final ArrayList<String> store_ids = dbUtil.getAllStoreIDs();
			storeNames = dbUtil.getAllStoresNames();
			storeLogoUrls = dbUtil.getAllStorePhotoUrl();
			getTipsForStoreManager(store_ids);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}
	
	private void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
}

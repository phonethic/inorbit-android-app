package com.phonethics.inorbit;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.Model;
import com.phonethics.model.ParticularStoreInfo;
import com.phonethics.model.RequestTags;
import com.phonethics.networkcall.DeleteGalleryImagesResultReceiver;
import com.phonethics.networkcall.DeleteGalleryImagesResultReceiver.DeleteImageReceiver;
import com.phonethics.networkcall.DeleteGalleryImagesService;
import com.phonethics.networkcall.LoadGalleryReceiver;
import com.phonethics.networkcall.LoadGalleryReceiver.LoadGallery;
import com.phonethics.networkcall.LoadStoreGallery;
import com.phonethics.networkcall.LocationIntentService;
import com.phonethics.networkcall.LocationResultReceiver;
import com.phonethics.networkcall.LocationResultReceiver.Receiver;
import com.phonethics.networkcall.PostPhotoReceiver;
import com.phonethics.networkcall.PostPhotoReceiver.photoreceiver;
import com.phonethics.notification.LocalNotification;
import com.squareup.picasso.Picasso;


public class EditStore extends SherlockFragmentActivity implements LoadGallery,photoreceiver,DeleteImageReceiver {
	
	ActionBar actionBar;
	static Activity context;
	//static ImageLoader imageLoader;
	NetworkCheck isnetConnected;
	//Session Manger
	SessionManager session;
	static String USER_ID="";
	static String AUTH_ID="";
	//User Id
	public static final String KEY_USER_ID="user_id";
	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";
	String business_id="";
	boolean isSplitLogin=false;
	private byte[] mByteArrLogo = null ;
	private byte[] mByteArrGall = null ;
	static String STORE_URL;
	static String STORE_GALLERY;
	static String ADD_BUSINESS_STORE_PATH;
	static String API_HEADER;
	static String API_VALUE;
	static String STORE_ID="";
	static String STORE_NAME;
	static String STORE_PHOTO;
	LoadGalleryReceiver mReceiver;
	PostPhotoReceiver mPostPhoto;
	GridView imageGrid;
	ProgressBar progUpload;
	private static final int SELECT_PHOTO = 2;
	private static final int THUMBNAIL_SIZE =450;
	String FILE_PATH="";
	String FILE_NAME="";
	String FILE_TYPE="";
	String FILE_PAGER_PATH="";
	String FILE_PAGER_NAME="";
	String FILE_PAGER_TYPE="";
	String encodedpath="";

	ArrayList<String>PHOTO_SOURCE=new ArrayList<String>();
	static JazzyViewPager mPager;
	ArrayList<String>pages=new ArrayList<String>();
	//location
	ArrayList<String> STORE_CATEGORY_ID=new ArrayList<String>();
	String PLACE_PARENT;
	String acttionBarTITLE="";
	//Store Pager
	static String store_name;
	static String store_parent;
	static String store_category;
	static String store_floor;
	static String store_unit;
	static String store_landmark = "Inorbit Mall";
	static String store_area;
	static String store_city;
	static String store_pincode;
	static String store_desc;
	static String store_landline1,store_landline2,store_landline3,store_mobileno1,store_mobileno2,store_mobileno3,store_faxno,store_tollfree,store_time_from,store_time_to;
	static String store_emailId,store_web,store_facebook,store_twitter;

	String PHOTO_URL="";

	static Calendar time = Calendar.getInstance(); 
	static int hour;
	static int min;

	/**************************FILE CROP***********************/

	private final int REQUEST_CODE_TAKE_PICTURE_LOGO = 7;
	private final int REQUEST_CODE_SELECT_PICTURE_LOGO = 8;
	private final int REQUEST_POST_GALL_CROP_LOGO = 9;
	private final int REQUEST_CODE_TAKE_PICTURE_GALLERY = 10;
	private final int REQUEST_CODE_SELECT_PICTURE_GALLERY = 11;
	private final int REQUEST_CODE_GALL_CROP_GALLERY = 12;

	private static File      mFileTemp;
	Uri mImageCaptureUri;
	static Uri mImagePagerCaptureUri;
	Dialog categoryDialog;
	Dialog mallsDialog;

	ArrayList<String> tempCatId=new ArrayList<String>();
	ArrayList<String> tempCatName=new ArrayList<String>();

	ArrayList<String> tempCatAdapterId=new ArrayList<String>();
	ArrayList<String> tempCatAdapterName=new ArrayList<String>();


	//STORE ID OF PARENT STORE;
	static String OWNER_ID="0";
	String categ_list="";
	static ProgressBar progStoreUpdate;
	boolean isNew; //to check whether to create new store or update Existing;

	//TABS
	Tab tab1;
	Tab tab2;
	int manageGallery=0;

	int cnt=0;

	//malad 
	static String latitued="19.1885679899768";
	static String longitude="72.8349024609436";

	int mappos=-1;
	private PagerSlidingTabStrip pagerTabs;

	Dialog chooseLogoDialog;
	GridView shoplocalGrid;

	String shopLocalIconUrl;
	boolean isShopLocalIcon=false;

	TextView chooseExisting;
	TextView takePic;

	Button shopIconDone;
	Button shopIconCancel;

	ArrayList<String>SHOPLOCAL_SOURCE;
	String SHOPLOCAL_ICON_URL;

	RelativeLayout galleryImageChooser;

	ImageView addGalleryImage;
	Button btnChooseGalleryImage;
	DBUtil	dbutil;
	String	SELECTED_MALL_ID="",SELECTED_PLACE_PARENT="";
	TextView SELECTED_MALL_LOCATION;
	EditText edtMallCity,edtMallPincode;
	static String place_status="1";

	//GalleryReciver broadcast_gallery;
	StoreInforReceiver broadcast_StoreInfo;
	StoreAddUpdate broadcast_addStore;
	LoadCategories broadcast_catogryInfo;
	private String STORE_PLACE_PARENT;

	Button done;
	Button edit;
	Button cancelEditing;

	LinearLayout editLay;
	LinearLayout editDoneLay;

	boolean showEditMode = false;

	GridAdapter adapterImg;
	static ArrayList<String> IDSTODELETE = new ArrayList<String>();

	String URLTOAPPEND = "";
	String Separator = "&gallery_id=";
	String url_ids = "";

	ArrayList<String>SOURCE=new ArrayList<String>();
	ArrayList<String>TITLE=new ArrayList<String>();
	static ArrayList<String>ID = new ArrayList<String>();
	ArrayList<String>STORE_ID_=new ArrayList<String>();
	ArrayList<String>IMAGE_DATE=new ArrayList<String>();
	ArrayList<String> THUMB = new ArrayList<String>();

	DeleteGalleryImagesResultReceiver deleteImages;
	String DELETE_IMG_URL;
	String Place_Id;
	String STORE_OPEN_TIME="";
	String STORE_CLOSE_TIME="";
	Button addImages;
	UploadPhotoGallery uploadPhotoManageGallery;
	boolean rotateImage = false;
	int angularRotation = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_edit_store);
		// Show the Up button in the action bar.
		context=this;

		initViews();

		reciveBundleValues();




		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		imageGrid.setLayoutAnimation(controller);


		edit.setOnClickListener(new android.view.View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				edit.setVisibility(View.GONE);
				editLay.setVisibility(View.VISIBLE);
				showEditMode = true;
				adapterImg=new GridAdapter(context, SOURCE,ID,TITLE, THUMB);
				imageGrid.setAdapter(adapterImg);
			}
		});


		done.setOnClickListener(new android.view.View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(IDSTODELETE.size()>0){


					AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
					alertDialogBuilder3.setTitle("Inorbit");
					alertDialogBuilder3
					.setMessage(getResources().getString(R.string.deleteGalleryImage))
					.setCancelable(false)
					.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							for(int i=0;i<IDSTODELETE.size();i++){

								Log.d("IDSTODELETE", "IDSTODELETE " + IDSTODELETE.get(i));

								URLTOAPPEND = URLTOAPPEND + IDSTODELETE.get(i);  

								if(i!=IDSTODELETE.size()-1){
									URLTOAPPEND = URLTOAPPEND + ",";
								}



							}
							Log.d("URLTOSEND","URLTOSEND "  +Separator + URLTOAPPEND);
							url_ids = Separator + URLTOAPPEND;

							if(isnetConnected.isNetworkAvailable()){
								callDeleteImage(url_ids);	
							}else{
								showToast(getResources().getString(R.string.noInternetConnection));
							}

						}


					})
					.setNegativeButton("No",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});;

					AlertDialog alertDialog3 = alertDialogBuilder3.create();
					alertDialog3.show();



				}
				else{
					showToast(getResources().getString(R.string.deleteImageValidation));
				}

			}
		});
		
		cancelEditing.setOnClickListener(new android.view.View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				IDSTODELETE.clear();
				URLTOAPPEND = "";


				editLay.setVisibility(View.VISIBLE);
				edit.setVisibility(View.VISIBLE);
				editLay.setVisibility(View.GONE);
				showEditMode = false;
				adapterImg=new GridAdapter(context, SOURCE,ID,TITLE, THUMB);
				imageGrid.setAdapter(adapterImg);

			}
		});


		addImages.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try {

					if(!isNew)
					{
						
						if(!progUpload.isShown())
						{
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
							alertDialogBuilder.setTitle("Inorbit");
							alertDialogBuilder.setMessage("Photo");
							//null should be your on click listener
							alertDialogBuilder.setPositiveButton("Take Picture", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									takePictureForGallery();
								}
							});
							alertDialogBuilder.setNegativeButton("Choose Existing", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {

									selectPictureForGallery();
								}
							});
							alertDialogBuilder.show();
						}
					}else
					{
						showToast("Please create store first.");
						mPager.setCurrentItem(0);
					}

				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}


			}
		});

	}

	/**
	 * 
	 * Initialize the view and class varaibles
	 * 
	 */
	void initViews(){


		/**
		 * Initialize the action bar
		 */
		actionBar		=	getSupportActionBar();
		acttionBarTITLE = 	"Edit Store";
		actionBar.setTitle(acttionBarTITLE);
		actionBar.setDisplayHomeAsUpEnabled(true);



		/**
		 * Initialize class varibles and layout
		 * 
		 */
		mFileTemp 			= new File(Environment.getExternalStorageDirectory(), "temp_photo.jpg");
		mImageCaptureUri 	= Uri.fromFile(mFileTemp);
		dbutil 				= new  DBUtil(context);
		mImagePagerCaptureUri	= Uri.fromFile(mFileTemp);
		FILE_PATH			= "/sdcard/temp_photo.jpg";
		PHOTO_URL			= getResources().getString(R.string.photo_url);
		editDoneLay 		= (LinearLayout) findViewById(R.id.editDoneLay);
		editLay 			= (LinearLayout) findViewById(R.id.editLay);
		done 				= (Button) findViewById(R.id.done);
		edit 				= (Button) findViewById(R.id.Edit);
		cancelEditing 		= (Button) findViewById(R.id.Cancel);
		addImages 			= (Button) findViewById(R.id.addImages);
		isnetConnected		= new NetworkCheck(context);
		//Porgressbar
		progUpload				= (ProgressBar)findViewById(R.id.progUpload);
		progStoreUpdate			= (ProgressBar)findViewById(R.id.progStoreUpdate);
		//Gridview
		imageGrid				= (GridView)findViewById(R.id.imageGrid);

		//Dilogs
		categoryDialog			= new Dialog(context);
		mallsDialog				= new Dialog(context);

		categoryDialog.setContentView(R.layout.categorylistdialog);
		mallsDialog.setContentView(R.layout.categorylistdialog);



		/**
		 * Initialize the dialog to choose the logo of the store
		 * 
		 */
		chooseLogoDialog	= new Dialog(context);
		chooseLogoDialog.setContentView(R.layout.logopickerdialog);
		chooseLogoDialog.setCancelable(true);
		chooseLogoDialog.setTitle("Choose Logo");
		chooseLogoDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;


		galleryImageChooser		= (RelativeLayout)findViewById(R.id.galleryImageChooser);

		addGalleryImage			= (ImageView)findViewById(R.id.addGalleryImage);
		btnChooseGalleryImage	= (Button)findViewById(R.id.btnChooseGalleryImage);


		/**
		 * Initialize the urls
		 * 
		 */
		//Store API
		STORE_URL				= getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		//Store Gallery
		STORE_GALLERY			= getResources().getString(R.string.store_gallery);
		//DeleteGalleryImage
		DELETE_IMG_URL 			= getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi)+getResources().getString(R.string.store_gallery_delete);
		Place_Id 				= "?" + getResources().getString(R.string.place_id);
		ADD_BUSINESS_STORE_PATH	= getResources().getString(R.string.add_store);
		
		
		
		
		/**
		 * Initilize the api headers
		 * 
		 */

		API_HEADER				= getResources().getString(R.string.api_header);
		API_VALUE				= getResources().getString(R.string.api_value);


		mReceiver				= new LoadGalleryReceiver(new Handler());
		mReceiver.setReceiver(this);
		mPostPhoto				= new PostPhotoReceiver(new Handler());
		mPostPhoto.setReceiver(this);
		deleteImages 			= new DeleteGalleryImagesResultReceiver(new Handler());
		deleteImages.setReceiver(this);


	






		/**
		 * Initialize the view pager and its title bar
		 * 
		 */
		pagerTabs 				= (PagerSlidingTabStrip) findViewById(R.id.pagerTabs);
		//ViewPager
		mPager					= (JazzyViewPager)findViewById(R.id.viewPagerManageStore);
		pages.add("Drop pin");
		pages.add("Details");
		pages.add("Contact");
		pages.add("Social");
		pages.add("Description");
		pages.add("Operations");
		pages.add("Gallery");


		PageAdapter adapter		= new PageAdapter(getSupportFragmentManager(), context, pages);
		mPager.setAdapter(adapter);
		mPager.setOffscreenPageLimit(pages.size());
		pagerTabs.setViewPager(mPager);
		pagerTabs.setTextColor(Color.BLACK);
		pagerTabs.setBackgroundColor(Color.TRANSPARENT);


		/**
		 * Get the merchant details from session 
		 * 
		 */
		session				= new SessionManager(getApplicationContext());
		HashMap<String,String>user_details	= session.getUserDetails();
		USER_ID		= user_details.get(KEY_USER_ID).toString();
		AUTH_ID		= user_details.get(KEY_AUTH_ID).toString();

	}

	void reciveBundleValues(){

		Bundle b=getIntent().getExtras();

		if (b!=null) {
			isSplitLogin	= session.isLoggedIn();
			/**
			 * isNew = true when merchant has clicked on AddStore tab
			 * isNew = false when merchant has clicked on EditStore / Manage Gallery tabs
			 * 
			 */
			
			isNew			= b.getBoolean("isNew");
			
			/**
			 * Selected store id
			 * 
			 */
			STORE_ID		= b.getString("STORE_ID");

			
			/**
			 * Selected store name
			 * 
			 */
			STORE_NAME		= b.getString("STORE_NAME");

			
			/**
			 * Selected store logo url
			 * 
			 */
			STORE_PHOTO		= b.getString("STORE_LOGO");

			/**
			 * Selected store place parent
			 * 
			 */
			STORE_PLACE_PARENT = b.getString("PLACE_PARENT");

			
			/**
			 * Manage gallery = 1 when merchant has clicked on the Manage Gallery tab
			 * 
			 */
			manageGallery= b.getInt("manageGallery");

			/**
			 * If Edit Store is true
			 */
			if(!isNew) {

				acttionBarTITLE = "Edit "+STORE_NAME;
				actionBar.setTitle(acttionBarTITLE);

				if((!STORE_ID.equals("") || STORE_ID.length()!=0) && isNew==false) {

					if(manageGallery!=1) {	

						/** Network request to load the details of the selected store */
						getPerticularStoreDetails();

					}else{

						acttionBarTITLE = STORE_NAME;
						actionBar.setTitle(acttionBarTITLE);
					}
				}
				
				/**
				 * For adding a new store
				 * 
				 */
			}else{
				acttionBarTITLE = "Add Store";

				actionBar.setTitle(acttionBarTITLE);

				if(isnetConnected.isNetworkAvailable()){

					/**
					 * Network request to load categories from server
					 * 
					 */
					loadAllCategories();

				}else{

				}


			}
		}


		if(manageGallery!=1) {

			mPager.setVisibility(View.VISIBLE);
			imageGrid.setVisibility(View.GONE);
			galleryImageChooser.setVisibility(View.GONE);
			addImages.setVisibility(View.GONE);
		}


		if(manageGallery==1) {

			addImages.setVisibility(View.VISIBLE);
			imageGrid.setVisibility(View.VISIBLE);
			mPager.setVisibility(View.GONE);
			pagerTabs.setVisibility(View.GONE);

			if(!isNew){
				/** Network request to load the store gallery  */
				loadGallery();
			}

		}
	}


	/**
	 * View pager adapter
	 * 
	 * @author Nitin
	 *
	 */
	//Creating Pages with PageAdapter.
	public class PageAdapter extends FragmentStatePagerAdapter
	{
		Activity context;
		ArrayList<String> pages;
		public PageAdapter(FragmentManager fm,Activity context,ArrayList<String> pages) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.pages=pages;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return new PageFragment(pages.get(position), context);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return pages.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Object obj = super.instantiateItem(container, position);
			mPager.setObjectForPosition(obj, position);
			return obj;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if(object != null){
				return ((Fragment)object).getView() == view;
			}else{
				return false;
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub


		}
	}

	
	/**
	 * Pager fragment of view pager
	 * 
	 * @author Nitin
	 *
	 */
	@SuppressLint("ValidFragment")
	public class PageFragment extends Fragment implements Receiver{

		String title="";
		Activity context;
		View view;
		ProgressBar progManageLocation;
		Animation anim;
		Button btnMangeStore_ImageChooser;
		TextView shopDropPin;
		GoogleMap googleMap;
		MarkerOptions markerOptions;
		LatLng latLng;
		Location location;
		String provider, current_address;
		double current_latitude, current_longitude;
		LatLng current_latlng;
		RelativeLayout mapLayout;
		TextView txtDropMessage;
		TextView shopDropMessage2;
		ImageView mapImage;

		//Location Page
		Button btnMangeStoreSave;
		TextView txtManageParent;
		TextView txtManageCategory;
		EditText edtManageStoreName;
		EditText edtManageFloor;
		EditText edtManageUnit;

		TextView edtManageArea;
		EditText edtManageCity;
		EditText edtManagePinCode;
		EditText edtManageDesc;


		//Description
		EditText edtManageDesc2;
		Button btnMangeStoreDescSave;
		Button btnMangeStoreDescSkip;



		//Contact Page 
		TextView txtMangeContactStoreName;
		EditText edtManageLandline1;
		EditText edtManageLandline2;
		EditText edtManageLandline3;
		EditText edtManageMobile1;
		EditText edtManageMobile2;
		EditText edtManageMobile3;
		EditText edtManageFax;
		EditText edtManageTollFree;
		EditText edtManageEmailId;
		EditText edtManageWebSite;
		EditText edtManageFacebook;
		EditText edtManageTwitter;
		Button btnManageContactSave;
		Button btnManageSocialSave;

		//Store Operation Page
		ImageView imgManageStore_ThumbPreview;
		TextView txtMangeStoreTimeFrom;
		TextView txtMangeStoreTimeTo;

		Button btnManageTime;

		//Gallery Page
		GridView imageGridMerchant;
		ImageView addGalleryPagerImage;
		Button btnChoosePagerGalleryImage ;
		Button pagerAddBtn;

		//Location Service
		public LocationResultReceiver mLocation;

		Typeface tf;

		public PageFragment()
		{

		}
		public PageFragment(String title,Activity context)
		{
			this.title=title;
			this.context=context;
			anim=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim.setDuration(1200);
		}
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setRetainInstance(true);

			tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");
			//Location Service
			mLocation=new LocationResultReceiver(new Handler());
			mLocation.setReceiver(this);

		}

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			super.onCreateView(inflater, container, savedInstanceState);
			// TODO Auto-generated method stub

			
			/**
			 * Drop pin fragment
			 * 
			 */
			if(title.equalsIgnoreCase("drop pin")){

				view=inflater.inflate(R.layout.merchant1, null);
				shopDropPin=(TextView)view.findViewById(R.id.shopDropPin);
				txtDropMessage=(TextView)view.findViewById(R.id.txtDropMessage);
				shopDropMessage2=(TextView)view.findViewById(R.id.shopDropMessage2);
				mapImage=(ImageView)view.findViewById(R.id.mapImage);
				Button skipButton=(Button)view.findViewById(R.id.skipButton);

				shopDropPin.setTypeface(tf);
				txtDropMessage.setTypeface(tf);
				skipButton.setTypeface(tf);
				shopDropMessage2.setTypeface(tf);


				skipButton.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						mPager.setCurrentItem(mPager.getCurrentItem()+1);
					}
				});

				//Drop pin
				mapImage.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						AlertDialog.Builder alert=new AlertDialog.Builder(context);
						alert.setTitle("Are you at the store?");
						alert.setPositiveButton("I am at the Store", new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub

								locationService();
								dialog.dismiss();

								txtDropMessage.setVisibility(View.INVISIBLE);
								shopDropMessage2.setVisibility(View.INVISIBLE);

							}
						});
						alert.setNegativeButton("Not now", new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();

							}
						});
						alert.show();
					}

				});

			}



			/**
			 * Store Details fragment
			 * 
			 */
			if(title.equalsIgnoreCase("Details")){


				view=inflater.inflate(R.layout.managestorelocation, null);
				progManageLocation=(ProgressBar)view.findViewById(R.id.progManageLocation);
				//Location Page
				txtManageParent=(TextView)view.findViewById(R.id.txtManageCategory);
				txtManageCategory=(TextView)view.findViewById(R.id.txtManageCategory);
				btnMangeStoreSave=(Button)view.findViewById(R.id.btnMangeStoreSave);
				edtManageStoreName=(EditText)view.findViewById(R.id.edtManageStoreName);
				edtManageFloor=(EditText)view.findViewById(R.id.edtManageFloor);
				edtManageUnit=(EditText)view.findViewById(R.id.edtManageUnit);
				edtManageArea=(TextView)view.findViewById(R.id.txtManageArea);

				SELECTED_MALL_LOCATION = edtManageArea;
				edtManageCity=(EditText)view.findViewById(R.id.edtManageCity);
				edtMallCity = edtManageCity;
				edtManagePinCode=(EditText)view.findViewById(R.id.edtManagePinCode);

				edtMallPincode = edtManagePinCode;
				edtManageDesc=(EditText)view.findViewById(R.id.edtManageDesc);

				txtManageParent.setTypeface(tf);
				txtManageCategory.setTypeface(tf);
				btnMangeStoreSave.setTypeface(tf);
				edtManageStoreName.setTypeface(tf);
				edtManageFloor.setTypeface(tf);
				edtManageUnit.setTypeface(tf);
				edtManageArea.setTypeface(tf);
				edtManageCity.setTypeface(tf);
				edtManagePinCode.setTypeface(tf);
				edtManageDesc.setTypeface(tf);

				if(isNew){
					
					/**
					 * If merchant has clicked on Add Store, than check which mall manager is logged in and set the
					 * location of that mall manager accordingly
					 * 
					 */
					String placeparent = dbutil.getPlaceIdByPlaceParent("0");
					setMallDropDown(placeparent);
				}




				btnMangeStoreSave.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						/**
						 * Fetch the store information - validate it and save it on server
						 * 
						 */
						fetchStoreInformation();
						
						if(validateStoreData())
						{
							if(isnetConnected.isNetworkAvailable()){
								/**
								 * Network request to save the store information on the server
								 * 
								 */
								updateStoreData();
							}else{
								showToast(getResources().getString(R.string.noInternetConnection));
							}
						}
					}
				});

			}
			
			
			/**
			 * Store Contact details fragment
			 * 
			 */
			if(title.equalsIgnoreCase("contact"))
			{
				view=inflater.inflate(R.layout.managestorecontact, null);


				
				/**
				 * Initialize the contact layout
				 * 
				 */
				//contact page
				txtMangeContactStoreName=(TextView)view.findViewById(R.id.txtMangeContactStoreName);
				edtManageLandline1=(EditText)view.findViewById(R.id.edtManageLandline1);
				edtManageLandline2=(EditText)view.findViewById(R.id.edtManageLandline2);
				edtManageLandline3=(EditText)view.findViewById(R.id.edtManageLandline3);
				edtManageMobile1=(EditText)view.findViewById(R.id.edtManageMobile1);
				edtManageMobile2=(EditText)view.findViewById(R.id.edtManageMobile2);
				edtManageMobile3=(EditText)view.findViewById(R.id.edtManageMobile3);
				edtManageFax=(EditText)view.findViewById(R.id.edtManageFax);
				edtManageTollFree=(EditText)view.findViewById(R.id.edtManageTollFree);
				btnManageContactSave=(Button)view.findViewById(R.id.btnManageContactSave);


				txtMangeContactStoreName.setTypeface(tf);
				edtManageLandline1.setTypeface(tf);
				edtManageLandline2.setTypeface(tf);
				edtManageLandline3.setTypeface(tf);
				edtManageMobile1.setTypeface(tf);
				edtManageMobile2.setTypeface(tf);
				edtManageMobile3.setTypeface(tf);
				edtManageFax.setTypeface(tf);
				edtManageTollFree.setTypeface(tf);
				btnManageContactSave.setTypeface(tf);


				btnManageContactSave.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						/**
						 * Fetch all the contact details from the layout and validate them
						 * 
						 */
						fetchContactData();

						if(isValidContactData()) {
							if(isnetConnected.isNetworkAvailable()) {
								/**
								 * Network request to save the store information on the server
								 * 
								 */
								updateStoreData();
							} else {
								showToast(getResources().getString(R.string.noInternetConnection));
							}
						}
					}
				});


			}


			
			/**
			 * 
			 * Social details page
			 * 
			 */
			if(title.equalsIgnoreCase("Social"))
			{
				view=inflater.inflate(R.layout.managestoreweb, null);
				edtManageEmailId=(EditText)view.findViewById(R.id.editManageEmailId);
				edtManageWebSite=(EditText)view.findViewById(R.id.edtManageWebSite);
				edtManageFacebook=(EditText)view.findViewById(R.id.edtManagefacebook);
				edtManageTwitter=(EditText)view.findViewById(R.id.edtManageTwitter);
				btnManageSocialSave=(Button)view.findViewById(R.id.btnManageSocialsave);

				edtManageEmailId.setTypeface(tf);
				edtManageWebSite.setTypeface(tf);
				edtManageTwitter.setTypeface(tf);
				edtManageFacebook.setTypeface(tf);

				btnManageSocialSave.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						fetchEmailData();
						if(isValidEmailId()) {
							if(isnetConnected.isNetworkAvailable()){
								/**
								 * Network request to save the store information on the server
								 * 
								 */
								updateStoreData();
							}else{
								showToast(getResources().getString(R.string.noInternetConnection));
							}
						}
					}
				});

			}


			/**
			 * 
			 * Description fragment
			 * 
			 */
			if(title.equalsIgnoreCase("Description")){


				view=inflater.inflate(R.layout.merchantdescription, null);
				edtManageDesc2=(EditText)view.findViewById(R.id.edtManageDesc2);
				btnMangeStoreDescSave=(Button)view.findViewById(R.id.btnMangeStoreDescSave);
				btnMangeStoreDescSkip=(Button)view.findViewById(R.id.btnMangeStoreDescSkip);

				edtManageDesc2.setTypeface(tf);
				btnMangeStoreDescSave.setTypeface(tf);
				btnMangeStoreDescSkip.setTypeface(tf);

				btnMangeStoreDescSkip.setOnClickListener(new android.view.View.OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							mPager.setCurrentItem(mPager.getCurrentItem()+1);
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}

					}
				});
				btnMangeStoreDescSave.setOnClickListener(new android.view.View.OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try{

							/**
							 * Fetch all the store and contact details from the layout and validate them
							 * 
							 */
							
							fetchStoreInformation();
							
							fetchContactData();

							if(isValidContactData()) {
								
								if(isnetConnected.isNetworkAvailable()) {
								
									/**
									 * Network request to save the store information on the server
									 * 
									 */
									updateStoreData();
								
								} else {
									showToast(getResources().getString(R.string.noInternetConnection));
								}

								/**
								 * Fire local notification
								 * 
								 */
								setAlarm();
							}


						}catch(Exception ex)
						{
							ex.printStackTrace();
						}

					}
				});

			}
			
			
			
			/**
			 * Timing and logo fragment
			 * 
			 */
			if(title.equalsIgnoreCase("operations")) {
				
				view=inflater.inflate(R.layout.managestoretimings, null);

				
				/**
				 * Initialize the layout
				 * 
				 */
				txtMangeStoreTimeFrom=(TextView)view.findViewById(R.id.txtMangeStoreTimeFrom);
				txtMangeStoreTimeTo=(TextView)view.findViewById(R.id.txtMangeStoreTimeTo);
				imgManageStore_ThumbPreview=(ImageView)view.findViewById(R.id.imgManageStore_ThumbPreview);
				btnMangeStore_ImageChooser=(Button)view.findViewById(R.id.btnMangeStore_ImageChooser);
				btnManageTime=(Button)view.findViewById(R.id.btnManageTime);

				try {
					//Formating time int 24 clock format.

					String now = new SimpleDateFormat("hh:mm aa").format(new java.util.Date().getTime());
					System.out.println("time in 12 hour format : " + now);
					SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
					SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
					String time24;
					time24 = outFormat.format(inFormat.parse(now));

					System.out.println("time in 24 hour format : " + time24);

					txtMangeStoreTimeFrom.setText(time24);

					txtMangeStoreTimeTo.setText(time24);

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}



				btnMangeStore_ImageChooser.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						chooseExisting=(TextView)chooseLogoDialog.findViewById(R.id.chooseExisting);
						takePic=(TextView)chooseLogoDialog.findViewById(R.id.takePic);

						chooseLogoDialog.show();

						chooseExisting.setOnClickListener(new android.view.View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								/**
								 * Select picture from gallery
								 * 
								 */
								openGalleryLogo();
								chooseLogoDialog.dismiss();
								isShopLocalIcon=false;
							}
						});
						takePic.setOnClickListener(new android.view.View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								//takePicture();
								
								/**
								 * Click a new picture
								 * 
								 */
								takeLogoPicture();
								chooseLogoDialog.dismiss();
								isShopLocalIcon=false;
							}
						});

					}
				});


				txtMangeStoreTimeFrom.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						new TimeFragment(context, "From").show(getFragmentManager(), "Time");
					}
				});


				txtMangeStoreTimeTo.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						new TimeFragment(context, "To").show(getFragmentManager(), "Time2");;
					}
				});

				btnManageTime.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						fetchStoreInformation();
						fetchContactData();
						fetchStoreTimingData();

						STORE_OPEN_TIME = txtMangeStoreTimeFrom.getText().toString();
						STORE_CLOSE_TIME = txtMangeStoreTimeTo.getText().toString();

						if(!isNew)
						{
							if(validateStoreData() && isValidContactData())
							{
								if(isnetConnected.isNetworkAvailable()){
									updateStoreData();

								}else{
									showToast(getResources().getString(R.string.noInternetConnection));
								}

							}
						}
						else
						{
							if( validateStoreData() && isValidContactData())
							{
								if(isnetConnected.isNetworkAvailable()){
									updateStoreData();
								}else{
									showToast(getResources().getString(R.string.noInternetConnection));
								}
							}
						}
					}
				});
			}

			
			
			/**
			 * Gallery fragment
			 * 
			 */
			if(title.equalsIgnoreCase("Gallery"))
			{

				ArrayList<String>SOURCE=new ArrayList<String>();

				view=inflater.inflate(R.layout.merchantgallery, null);
				imageGridMerchant=(GridView)view.findViewById(R.id.imageGridMerchant);
				addGalleryPagerImage=(ImageView)view.findViewById(R.id.addGalleryPagerImage);

				btnChoosePagerGalleryImage=(Button)view.findViewById(R.id.btnChoosePagerGalleryImage);
				pagerAddBtn = (Button)view.findViewById(R.id.pagerAddBtn);

				pagerAddBtn.setOnClickListener(new android.view.View.OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						try {

							if(!isNew)
							{
								if(!progUpload.isShown())
								{
									AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
									alertDialogBuilder.setTitle("Inorbit");
									alertDialogBuilder.setMessage("Photo");
									//null should be your on click listener
									alertDialogBuilder.setPositiveButton("Take Picture", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											//takePictureForGrid();
											/**
											 * Select picture from gallery
											 * 
											 */
											takePictureForGallery();
										}
									});
									alertDialogBuilder.setNegativeButton("Choose Existing", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											//openGalleryForGrid();
											
											/**
											 * Click a new picture
											 * 
											 */
											selectPictureForGallery();
										}
									});
									alertDialogBuilder.show();
								}
							} else {
								showToast("Please create store first.");
								mPager.setCurrentItem(0);
							}

						} catch (Exception e) {
							// TODO: handle exception

							e.printStackTrace();
						}
					}

				});

				btnChoosePagerGalleryImage.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(!isNew)
						{
							if(!progUpload.isShown())
							{
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
								alertDialogBuilder.setTitle(getResources().getString(R.string.actionBarTitle));
								alertDialogBuilder.setMessage("Photo");
								//null should be your on click listener
								alertDialogBuilder.setPositiveButton("Take Picture", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
									
										/**
										 * Select picture from gallery
										 * 
										 */
										takePictureForGrid();
									}
								});
								alertDialogBuilder.setNegativeButton("Choose Existing", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										/**
										 * Click a new picture
										 * 
										 */
										openGalleryForGrid();
									}
								});
								alertDialogBuilder.show();
							}
						}else
						{
							showToast("Please create store first.");
							mPager.setCurrentItem(0);
						}
					}
				});
				
				
				if(!isNew){
					// opens in edit

					loadGallery();


				}else{

					PHOTO_SOURCE=SOURCE;
				}

				Button btnMangeStoreGalleryave=(Button)view.findViewById(R.id.btnMangeStoreGalleryave);
				Button btnMangeStoreGallerySkip=(Button)view.findViewById(R.id.btnMangeStoreGallerySkip);

				btnMangeStoreGalleryave.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(STORE_ID!=null && STORE_ID.length()!=0)
							{
								/*Intent intent=new Intent(context,ShowPlaceMessage.class);
								intent.putExtra("STOREID", STORE_ID);*/
								Intent intent=new Intent(context,MerchantHome.class);
								startActivity(intent);
								finish();
								overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
							}
							else
							{
								showToast("Please create a store");
							}
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}
				});
				btnMangeStoreGallerySkip.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(STORE_ID!=null && STORE_ID.length()!=0)
							{
								//Intent intent=new Intent(context,ShowPlaceMessage.class);
								Intent intent=new Intent(context,MerchantHome.class);
								startActivity(intent);
								finish();
								overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
							}
							else
							{
								showToast("Please create a store");
							}
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}
				});


			}
			return view;
		}


		//Creating Time dialoge
		public class TimeFragment extends DialogFragment implements OnTimeSetListener
		{
			Context mTContext;
			String timeset;
			public TimeFragment(Context context,String timeset) {
				mTContext = context;
				this.timeset=timeset;
			}
			@Override
			public Dialog onCreateDialog(Bundle savedInstanceState) {
				// TODO Auto-generated method stub
				if(savedInstanceState!=null)
				{
					mTContext=getActivity().getApplicationContext();
				}
				hour=time.get(Calendar.HOUR_OF_DAY);
				min=time.get(Calendar.MONDAY);
				return new TimePickerDialog(mTContext, this, hour, min, false);
			}
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				time.set(Calendar.HOUR_OF_DAY,hourOfDay);
				time.set(Calendar.MINUTE,minute);
				updateTime(timeset);
			}


		}

		//Updating time on TextViews
		void updateTime(String timeset )
		{
			if(timeset.equalsIgnoreCase("From"))
			{
				DateFormat  dt=new SimpleDateFormat("hh:mm");


				try {
					//Formating time int 24 clock format.
					hour=time.get(Calendar.HOUR_OF_DAY);
					min=time.get(Calendar.MONDAY);

					String now = new SimpleDateFormat("hh:mm aa").format(time.getTime());
					System.out.println("time in 12 hour format : " + now);
					SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
					SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
					String time24;
					time24 = outFormat.format(inFormat.parse(now));

					System.out.println("time in 24 hour format : " + time24);
					txtMangeStoreTimeFrom.setText(time24);

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}




			}
			if(timeset.equalsIgnoreCase("To"))
			{
				DateFormat  dt=new SimpleDateFormat("hh:mm");


				try {
					//Formating time int 24 clock format.
					hour=time.get(Calendar.HOUR_OF_DAY);
					min=time.get(Calendar.MONDAY);


					String now = new SimpleDateFormat("hh:mm aa").format(time.getTime());
					System.out.println("time in 12 hour format : " + now);
					SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
					SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
					String time24;
					time24 = outFormat.format(inFormat.parse(now));

					System.out.println("time in 24 hour format : " + time24);
					txtMangeStoreTimeTo.setText(time24);

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}


		private void openGalleryLogo() {


			Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
			photoPickerIntent.setType("image/*");
			getActivity().startActivityForResult(photoPickerIntent, REQUEST_CODE_SELECT_PICTURE_LOGO);


		}

		private void takeLogoPicture() {

			try {
				Intent intent = new Intent(context,CameraView.class);
				getActivity().startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE_LOGO);
			} catch (ActivityNotFoundException e) {
				e.printStackTrace();
				InorbitLog.d("cannot take picture");
			}




		}  

		
		/**
		 * Fetch the store information
		 * 
		 */
		void fetchStoreInformation(){


			

			EditText tempManageStoreName=(EditText)mPager.findViewById(R.id.edtManageStoreName);
			EditText tempManageFloor=(EditText)mPager.findViewById(R.id.edtManageFloor);
			EditText tempManageUnit=(EditText)mPager.findViewById(R.id.edtManageUnit);
			TextView tempManageArea=(TextView)mPager.findViewById(R.id.txtManageArea);
			EditText tempManageCity=(EditText)mPager.findViewById(R.id.edtManageCity);
			EditText tempManagePinCode=(EditText)mPager.findViewById(R.id.edtManagePinCode);
			EditText tempManageDesc=(EditText)mPager.findViewById(R.id.edtManageDesc2);


			store_name=tempManageStoreName.getText().toString();
			store_floor=tempManageFloor.getText().toString();
			store_unit=tempManageUnit.getText().toString();
			store_area=tempManageArea.getText().toString();
			store_city=tempManageCity.getText().toString();
			store_pincode=tempManagePinCode.getText().toString();
			store_desc=tempManageDesc.getText().toString();
		}

		
		/**
		 * Fetch store's contact information
		 * 
		 */
		void fetchContactData(){

			View page=(View)mPager.getChildAt(2);

			//Contact Page 
			TextView tempMangeContactStoreName=(TextView)mPager.findViewById(R.id.txtMangeContactStoreName);
			EditText tempManageLandline1=(EditText)mPager.findViewById(R.id.edtManageLandline1);
			EditText tempManageLandline2=(EditText)mPager.findViewById(R.id.edtManageLandline2);
			EditText tempManageLandline3=(EditText)mPager.findViewById(R.id.edtManageLandline3);
			EditText tempManageMobile1=(EditText)mPager.findViewById(R.id.edtManageMobile1);
			EditText tempManageMobile2=(EditText)mPager.findViewById(R.id.edtManageMobile2);
			EditText tempManageMobile3=(EditText)mPager.findViewById(R.id.edtManageMobile3);
			EditText tempManageFax=(EditText)mPager.findViewById(R.id.edtManageFax);
			EditText tempManageTollFree=(EditText)mPager.findViewById(R.id.edtManageTollFree);
			EditText tempManageEmailId=(EditText)mPager.findViewById(R.id.editManageEmailId);
			EditText tempManageWebsite=(EditText)mPager.findViewById(R.id.edtManageWebSite);
			EditText tempManageFacebook=(EditText)mPager.findViewById(R.id.edtManagefacebook);
			EditText tempManageTwitter=(EditText)mPager.findViewById(R.id.edtManageTwitter);


			store_landline1=tempManageLandline1.getText().toString();
			store_landline2=tempManageLandline2.getText().toString();
			store_landline3=tempManageLandline3.getText().toString();
			store_mobileno1="91"+tempManageMobile1.getText().toString();
			store_mobileno2="91"+tempManageMobile2.getText().toString();
			store_mobileno3="91"+tempManageMobile3.getText().toString();
			store_faxno=tempManageFax.getText().toString();
			store_tollfree=tempManageTollFree.getText().toString();
			store_emailId=tempManageEmailId.getText().toString();
			store_web=tempManageWebsite.getText().toString();
			store_facebook=tempManageFacebook.getText().toString();
			store_twitter=tempManageTwitter.getText().toString();


		}

		boolean validateStoreData(){

			if(store_name==null || store_name.length()==0)
			{
				Toast.makeText(context, "Store name cannot be blank",Toast.LENGTH_SHORT).show();
				return false;
			}
			else if(store_area==null || store_area.length()==0 )
			{
				Toast.makeText(context, "Area cannot be blank",Toast.LENGTH_SHORT).show();
				return false;
			}
			else if(store_city==null || store_city.length()==0 )
			{
				Toast.makeText(context, "City cannot be blank",Toast.LENGTH_SHORT).show();
				return false;
			}
			else if(store_pincode==null || store_pincode.length()==0 )
			{
				Toast.makeText(context, "Pincode cannot be blank",Toast.LENGTH_SHORT).show();
				return false;
			}
			else if(store_pincode.length()<6)
			{
				Toast.makeText(context, "Pincode must be 6 digit.",Toast.LENGTH_SHORT).show();
				return false;
			}
			else if(tempCatId.size()==0)
			{
				Toast.makeText(context, "Please select atleast one category",Toast.LENGTH_SHORT).show();
				return false;
			}
			else
			{
				return true;
			}

		}

		void showToast(String text)
		{
			Toast.makeText(context, text,Toast.LENGTH_SHORT).show();
		}
		
		void fetchEmailData() {
			EditText email=(EditText)mPager.findViewById(R.id.editManageEmailId);
			store_emailId = email.getText().toString();
		}
		
		
		/**
		 * Check if all the contact details entered by merchant is proper or not
		 * 
		 * @return
		 */
		boolean isValidContactData()
		{

			if(store_mobileno1.length()==2 && store_mobileno2.length()==2 && store_mobileno3.length()==2 && store_landline1.length()==0 && store_landline2.length()==0 && store_landline3.length()==0  ){
				Toast.makeText(context, "Please provide atleast one contact number",Toast.LENGTH_SHORT).show();
				return false;
			}
			if(store_mobileno1.length()>2 && store_mobileno1.length()!=12)
			{
				Toast.makeText(context, "Mobile  no must be 10 digit.",Toast.LENGTH_SHORT).show();
				return false;
			}
			else if(store_mobileno2.length()>2 && store_mobileno2.length()!=12)
			{
				if(store_mobileno1.length()==12){
					return true;
				}else{
					Toast.makeText(context, "Mobile  no must be 10 digit.",Toast.LENGTH_SHORT).show();	
					return false;
				}


			}
			else if(store_mobileno3.length()>2 && store_mobileno3.length()!=12)
			{
				if(store_mobileno1.length()==12){
					return true;
				}else if(store_mobileno2.length()==12){
					return true;
				}else{
					Toast.makeText(context, "Mobile  no must be 10 digit.",Toast.LENGTH_SHORT).show();	
					return false;
				}




			}
			else if(store_landline1.length()>0 && store_landline1.length()!=11)
			{
				Toast.makeText(context, "Landline no must be 11 digit.",Toast.LENGTH_SHORT).show();
				return false;
			}
			else if(store_landline2.length()>0 && store_landline2.length()!=11)
			{
				Toast.makeText(context, "Landline no must be 11 digit.",Toast.LENGTH_SHORT).show();
				return false;
			}
			else if(store_faxno.length()>0 && store_faxno.length()!=11)
			{
				Toast.makeText(context, "Mobile no must be 11 digit.",Toast.LENGTH_SHORT).show();
				return false;
			}
			else if(store_tollfree.length()>0 && store_tollfree.length()!=11)
			{
				Toast.makeText(context, "Toll free no must be 11 digit.",Toast.LENGTH_SHORT).show();
				return false;
			}
			else
			{
				return true;
			}
		}

		public boolean isValidEmailId() {
			if(store_emailId.length()<=0 || !isValidEmail(store_emailId)) {
				Toast.makeText(context, "Please enter valid email-id.",Toast.LENGTH_SHORT).show();
				return false;
			} else {
				 return true;
			}
		}

		//Validating Email id.
		public final  boolean isValidEmail(CharSequence target) {
			if (target == null) {
				return false;
			} else {
				return ((String)target).matches(new String("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
						+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"));
			}
		}   


		/**
		 * Fetch store timings data
		 * 
		 */
		void fetchStoreTimingData() {
			
			TextView txtTempTimeFrom=(TextView)mPager.findViewById(R.id.txtMangeStoreTimeFrom);
			TextView txtTempTimeTo=(TextView)mPager.findViewById(R.id.txtMangeStoreTimeTo);

			store_time_from=txtTempTimeFrom.getText().toString();
			store_time_to=txtTempTimeTo.getText().toString();

			if(store_time_from==null || store_time_from.length()==0)
			{
				store_time_from="00:00:00";

			}else if(store_time_to==null || store_time_to.length()==0)
			{
				store_time_to="00:00:00";
			}

		}


		/**
		 * Network request to save the store information the server
		 * 
		 */
		void updateStoreData(){

			try
			{

				OWNER_ID = SELECTED_PLACE_PARENT;
				String encodedPagerpath="";
				if(mByteArrLogo!=null){
					encodedPagerpath=Base64.encodeToString(mByteArrLogo, Base64.DEFAULT);
					if(encodedPagerpath==null || encodedPagerpath.equals("")){
						encodedPagerpath="";
					}

				}

				InorbitLog.d("Path Encoded  = "+encodedPagerpath);
				InorbitLog.d("Path FILE_PAGER_NAME = "+FILE_PAGER_NAME);
				InorbitLog.d("Path FILE_PAGER_TYPE = "+FILE_PAGER_TYPE);

				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put(API_HEADER, API_VALUE);
				
				tempCatId = new ArrayList<String>(new LinkedHashSet<String>(tempCatId));
				
				if (tempCatId.contains("5")) {
					if (!tempCatId.contains("2"))
						tempCatId.add("2");
				}
				
				JSONArray jsonArray=new JSONArray(tempCatId);
				JSONObject json = new JSONObject();
				json.put("name", store_name);
				json.put("place_parent", OWNER_ID);
				json.put("mall_id", SELECTED_MALL_ID);
				json.put("category", jsonArray);
				json.put("description", store_desc);
				json.put("building", store_floor);
				json.put("street", store_unit);

				json.put("landmark", store_landmark);
				json.put("area", store_area);
				json.put("pincode", store_pincode);
				json.put("city", store_city);

				json.put("user_id", USER_ID);	
				json.put("auth_id", AUTH_ID);


				if(isNew)		
				{	
					json.put("place_id", "");

				}
				else
				{
					json.put("place_id", STORE_ID);
					headers.put("X-HTTP-Method-Override", "PUT");			
				}

				json.put("latitude", latitued);
				json.put("longitude", longitude	);
				//json.put("landline", store_landline1);
				if(store_landline1!=null){
					json.put("landline", store_landline1);
				}
				else {
					store_landline1="";
					json.put("landline2", store_landline1);
				}

				if(store_landline2!=null){
					json.put("landline2", store_landline2);
				}
				else {
					store_landline2="";
					json.put("landline2", store_landline2);
				}
				if(store_landline3!=null){
					json.put("landline3", store_landline3);
				}
				else {
					store_landline3="";
					json.put("landline3", store_landline3);
				}

				if(store_mobileno1!=null){
					if(store_mobileno1.length()==2){
						store_mobileno1="";
						json.put("mobile", store_mobileno1);
					}else{
						json.put("mobile", store_mobileno1);
					}

				}
				else {
					store_mobileno1="";
					json.put("mobile", store_mobileno1);
				}

				if(store_mobileno2!=null){
					if(store_mobileno2.length()==2){
						store_mobileno2="";
						json.put("mobile2", store_mobileno2);
					}else{
						json.put("mobile2", store_mobileno2);
					}

				}
				else {
					store_mobileno2="";
					json.put("mobile2", store_mobileno2);
				}
				if(store_mobileno3!=null){
					if(store_mobileno3.length()==2){
						store_mobileno3="";
						json.put("mobile3", store_mobileno3);
					}else{
						json.put("mobile3", store_mobileno3);
					}

				}
				else {
					store_mobileno3="";
					json.put("mobile3", store_mobileno3);
				}

				json.put("fax", store_faxno);
				json.put("tollfree",store_tollfree);
				json.put("email", store_emailId);
				json.put("open_time",STORE_OPEN_TIME);
				json.put("close_time",STORE_CLOSE_TIME);
				json.put("website",store_web);
				json.put(getResources().getString(R.string.id_key),SELECTED_MALL_ID);
				json.put("website",store_web);

				if(!encodedPagerpath.equals("") && isShopLocalIcon==false){
					json.put("filename",FILE_PAGER_NAME);
					json.put("filetype",FILE_PAGER_TYPE);
					json.put("userfile",encodedPagerpath);
					json.put("angular_rotation", angularRotation+"");
				}

				if(isShopLocalIcon==true){
					json.put("gallery_image",SHOPLOCAL_ICON_URL);
				}

				InorbitLog.d("OPen time close time "+STORE_OPEN_TIME+" to "+STORE_CLOSE_TIME);
				MyClass myClass = new MyClass(context);
				if(Conifg.useServices_m){
					//context.startService(intent);
				}else{
					InorbitLog.d("Using Volley");
					myClass.postRequest(RequestTags.Tag_AddStore, headers, json);
				}


				progStoreUpdate.setVisibility(View.VISIBLE);

			}catch(JSONException jEx){
				jEx.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}

		void locationService()
		{
			try
			{
				if(isnetConnected.isNetworkAvailable())
				{
					//calling location service.
					Intent intent=new Intent(context, LocationIntentService.class);
					intent.putExtra("receiverTag", mLocation);
					context.startService(intent);
					/*ShopLocalMain.shopProgress.setVisibility(View.VISIBLE);*/
					progStoreUpdate.setVisibility(View.VISIBLE);
				}else
				{
					progStoreUpdate.setVisibility(View.GONE);
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}


		/*
		 * (non-Javadoc)
		 * @see com.phonethics.networkcall.LocationResultReceiver.Receiver#onReceiveResult(int, android.os.Bundle)
		 */
		@Override
		public void onReceiveResult(int resultCode, Bundle resultData) {
			// TODO Auto-generated method stub

			progStoreUpdate.setVisibility(View.GONE);
			String status=resultData.getString("status");
			if(status.equalsIgnoreCase("set"))
			{
				try
				{
					registerEvent(getResources().getString(R.string.store_DropPin));
					latitued=resultData.getString("Lat");
					longitude=resultData.getString("Longi");

					location=new Location("locations");
					if(latitued!=null || latitued.length()>0 && longitude!=null || longitude.length()>0)
					{
						location.setLatitude(Double.parseDouble(latitued));
						location.setLongitude(Double.parseDouble(longitude));
						setImage(latitued,longitude);

					}
					Log.i("Location ", "LOCATION : "+latitued+" , "+longitude);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			else{

				try{

					if(latitued==null || latitued.length()==0 && longitude==null || longitude.length()==0){
						showToast("Please check your location service");
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

			}
		}

	} //end of Fragment class



	void setImage(String latitude,String longitude)
	{
		try
		{
			DisplayMetrics displaymetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
			int height = displaymetrics.heightPixels;
			int width = displaymetrics.widthPixels;


			Log.i("DEVICE ", "DEVICE WIDTH "+width);
			ImageView staticMap=(ImageView)mPager.findViewById(R.id.mapImage);
			Button btnSkip=(Button)mPager.findViewById(R.id.skipButton);
			TextView shopDropMessage2=(TextView)mPager.findViewById(R.id.shopDropMessage2);
			TextView txtDropMessage=(TextView)mPager.findViewById(R.id.txtDropMessage);

			txtDropMessage.setVisibility(View.INVISIBLE);
			shopDropMessage2.setVisibility(View.VISIBLE);
			shopDropMessage2.setText("Location Found.Swipe Right to continue.");
			btnSkip.setText("Proceed");



			String url="http://maps.googleapis.com/maps/api/staticmap?center="+latitude+","+longitude+"&zoom=18&size="+300+"x"+150+"&maptype=roadmap&markers=color:blue|label:S|"+latitude+","+longitude+"&sensor=true&key=AIzaSyCvFovNWeEIPL4355q8L0Chd1urF8n8c0g";
			staticMap.setScaleType(ScaleType.FIT_XY);
			staticMap.setAdjustViewBounds(true);

			try {

				Picasso.with(context).load(url)
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(staticMap);

			}catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			}
			catch(Exception e){
				e.printStackTrace();
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void getPerticularStoreDetails(){
		
		progUpload.setVisibility(View.VISIBLE);
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);
		
		List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("place_id", STORE_ID));
		nameValuePairs.add(new BasicNameValuePair("user_id", USER_ID));
		headers.put("user_id", USER_ID);
		headers.put("auth_id", AUTH_ID);
		
		MyClass myClass = new MyClass(context);
		myClass.getStoreRequest(RequestTags.TagGetParticularStoreDetails_m, nameValuePairs, headers);	

	}

	/*
	 * Load All Categories
	 */
	void loadAllCategories(){


		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		//nameValuePairs.add(new BasicNameValuePair("place_id", STORE_PLACE_PARENT));

		MyClass myClass = new MyClass(context);
		InorbitLog.d("Using Volley");
		myClass.getStoreRequest(RequestTags.Tag_CategoryInfo, nameValuePairs, headers);
	}


	void loadGallery()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			//Toast.makeText(context, "Loading Gallery", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(context, LoadStoreGallery.class);
			intent.putExtra("loadgall",mReceiver);
			intent.putExtra("URL", STORE_URL+STORE_GALLERY);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("store_id", STORE_ID);
			context.startService(intent);
			progUpload.setVisibility(View.VISIBLE);
		}
		else
		{
			progUpload.setVisibility(View.GONE);
			Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
		}
	}

	void loadStoreGallery()
	{
		if(isnetConnected.isNetworkAvailable()){


			progUpload.setVisibility(View.VISIBLE);

			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(API_HEADER, API_VALUE);


			MyClass myClass = new MyClass(context);

			InorbitLog.d("Using Volley");
			myClass.makeGetRequest(RequestTags.Tag_LoadStoreGallery, "-1", headers);


		}
		else
		{
			progUpload.setVisibility(View.GONE);
			Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
		}
	}

	void postGalleryData(){

		try
		{
			if(isnetConnected.isNetworkAvailable())
			{
				String encodedpath="";

				/*byte [] b=imageTobyteArray(FILE_PATH,480,480);*/

				if(mByteArrGall!=null) {
					//Toast.makeText(context, "Uploading image",Toast.LENGTH_SHORT).show();
					//encodedpath=Base64.encodeToString(gallery_byte, Base64.DEFAULT);
					Log.i("Encode ", "Details : "+encodedpath);
					/*Intent intent=new Intent(context, PostStorePhoto.class);
					intent.putExtra("postPhoto",mPostPhoto);
					intent.putExtra("URL", STORE_URL+STORE_GALLERY);
					intent.putExtra("api_header",API_HEADER);
					intent.putExtra("api_header_value", API_VALUE);
					intent.putExtra("user_id", USER_ID);
					intent.putExtra("auth_id", AUTH_ID);
					intent.putExtra("store_id", STORE_ID);
					intent.putExtra("filename", FILE_NAME);
					intent.putExtra("filetype", FILE_TYPE);
					String user_file=Base64.encodeToString(mByteArrGall, Base64.DEFAULT);
					
					Toast.makeText(context,STORE_URL+STORE_GALLERY +" "+STORE_ID+" "+FILE_TYPE+" "+AUTH_ID+
							" "+USER_ID+" "+ API_VALUE+" "+API_HEADER, 0).show();
					intent.putExtra("userfile", user_file);
					context.startService(intent);*/
					/*
					[httpClient setDefaultHeader:@"X-API-KEY" value:API_KEY];
					[httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
					[httpClient setDefaultHeader:@"Accept" value:@"application/json"];
					NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
					[postparams setObject:[NSString stringWithFormat:@"%d",self.storeId] forKey:@"place_id"];
					[postparams setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
					[postparams setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
					NSData *dataObj = UIImageJPEGRepresentation(pickerimage, 0.75);
					NSString *image = [IN_APP_DELEGATE base64forData:dataObj];
					[postparams setObject:image forKey:@"userfile"];
					[postparams setObject:@"gallery.jpg" forKey:@"filename"];
					[postparams setObject:@"image/jpeg" forKey:@"filetype"];*/
					HashMap<String, String> headers = new HashMap<String, String>();
					headers.put(API_HEADER, API_VALUE);
					
					JSONObject json = new JSONObject();
					json.put("place_id", STORE_ID);
					json.put("filename", FILE_NAME);
					json.put("filetype", FILE_TYPE);
					String user_file=Base64.encodeToString(mByteArrGall, Base64.DEFAULT);
					json.put("userfile", user_file);
					json.put("angular_rotation", angularRotation+"");
					json.put("user_id", USER_ID);
					json.put("auth_id", AUTH_ID	);
					
					MyClass myClass = new MyClass(context);
					myClass.postRequest(RequestTags.TAG_UPLOAD_PHOTO_GALLERY, headers, json); //(RequestTags.TAG_UPLOAD_PHOTO_GALLERY,  , json)
					progUpload.setVisibility(View.VISIBLE);
				} else {
					encodedpath="undefined";
					Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
				}
			}
			else
			{
				Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}



	class ShopLocalGridAdapter extends BaseAdapter
	{
		ArrayList<String>photo;
		Activity context;
		LayoutInflater inflate;
		String image;

		public ShopLocalGridAdapter(Activity context,ArrayList<String>photo)
		{
			this.context=context;
			this.photo=photo;
			inflate=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return photo.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return photo.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.shoplocalgridicon,null);
				holder.shopLocalIcon=(ImageView)convertView.findViewById(R.id.shopLocalIcon);
				convertView.setTag(holder);
			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			if(photo.get(position)!=null || photo.get(position).length()!=0)
			{
				image=photo.get(position).replaceAll(" ", "%20");
				//imageLoader.DisplayImage(PHOTO_URL+image,hold.shopLocalIcon);

				try {

					Picasso.with(context).load(PHOTO_URL+image)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(hold.shopLocalIcon);

				} catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){
					e.printStackTrace();
				}



			}


			return convertView;
		}

		class ViewHolder
		{
			ImageView shopLocalIcon;

		}

	}//Adapter ends here


	void setShopLocalIcon(String url)
	{
		try
		{

			ImageView imageIcon=(ImageView)mPager.findViewById(R.id.imgManageStore_ThumbPreview);
			//			imageLoader.DisplayImage(PHOTO_URL+url, imageIcon);
			Picasso.with(context).load(PHOTO_URL+url)
			.placeholder(R.drawable.ic_launcher)
			.error(R.drawable.ic_launcher)
			.into(imageIcon);

		}catch(IllegalArgumentException illegalArg){
			illegalArg.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}


	}

	@Override
	public void onReceiveGalleryResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			GridView imageGridMerchant;
			String status=resultData.getString("SEARCH_STATUS");
			String message=resultData.getString("MESSAGE");

			ID.clear();
			SOURCE.clear();
			TITLE.clear();

			progUpload.setVisibility(View.GONE);

			if(status.equalsIgnoreCase("true"))
			{




				ID=resultData.getStringArrayList("ID");
				STORE_ID_=resultData.getStringArrayList("STORE_ID");
				SOURCE=resultData.getStringArrayList("SOURCE");
				TITLE=resultData.getStringArrayList("TITLE");
				IMAGE_DATE=resultData.getStringArrayList("IMAGE_DATE");
				THUMB = resultData.getStringArrayList("THUMB");
				GridAdapter adapter=new GridAdapter(context, SOURCE,ID,TITLE, THUMB);
				imageGrid.setAdapter(adapter);
				
				try
				{
					imageGridMerchant=(GridView)mPager.findViewById(R.id.imageGridMerchant);
					imageGridMerchant.setAdapter(adapter);
					imageGridMerchant.setOnItemClickListener(new GridItemClickListener());
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}


				PHOTO_SOURCE=SOURCE;

				imageGrid.setOnItemClickListener(new GridItemClickListener());

				for(int i=0;i<ID.size();i++)
				{

					Log.i("======SOURCE", "SOURCE "+SOURCE.get(i));
				}


			}else{

				imageGrid.setAdapter(null);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void onReceivePhoto(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		String status=resultData.getString("broadcast_image_status");
		String message=resultData.getString("broadcast_message");
		progUpload.setVisibility(View.GONE);
		if(status.equalsIgnoreCase("true"))
		{
			registerEvent(getResources().getString(R.string.storeGallery_ImageAdd));
			Toast.makeText(context, "Photo uploaded successfully", Toast.LENGTH_SHORT).show();
			loadGallery();
		}
		else
		{
			Toast.makeText(context, message, Toast.LENGTH_SHORT).show();


		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		finishTo();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);


	}


	/*
	 * (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockFragmentActivity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getTitle().toString().equalsIgnoreCase(acttionBarTITLE))
		{
			finishTo();


		}

		if(item.getTitle().toString().equalsIgnoreCase("Extra Settings"))
		{

		}
		else if(item.getTitle().toString().equalsIgnoreCase("Delete store"))
		{
			try
			{
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Inorbit");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.deleteStoreMessage))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Delete",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						DeleteStoreAsync delete=new DeleteStoreAsync();
						delete.execute("");

					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

		}
		else if(item.getTitle().toString().equalsIgnoreCase("Show store")){

			try{


				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Inorbit");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.showStore))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Show",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						place_status="1";

						PlaceStatusAsync place=new PlaceStatusAsync();
						place.execute("");
					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();


			}catch(Exception ex)
			{
				ex.printStackTrace();
			}


		}
		else if(item.getTitle().toString().equalsIgnoreCase("Hide store"))
		{
			try
			{
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Inorbit");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.hideStoreMessage))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Hide",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						place_status="0";

						PlaceStatusAsync place=new PlaceStatusAsync();
						place.execute("");
					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();


			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			Intent intent=new Intent(context,MerchantHome.class);
			intent.putExtra("isSplitLogin",true);
			intent.putExtra("USER_ID", USER_ID);
			intent.putExtra("AUTH_ID", AUTH_ID);
			//startActivity(intent);
			//finish();
			//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}

		return true;
	}

	void finishTo()
	{
		if(manageGallery!=1)
		{
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Edit Store");
			alertDialogBuilder3
			.setMessage("Do you want to discard?")
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
					try
					{
						hour=0;
						min=0;
						store_landline1="";
						store_mobileno1="";
						store_landline2="";
						store_mobileno2="";
						store_landline3="";
						store_mobileno3="";
						store_faxno="";
						store_tollfree="";
						store_time_from="";
						store_time_to="";
						store_emailId="";
						store_web="";
						mByteArrLogo=null;
						mByteArrGall=null;
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

				}
			})
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		else
		{

			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}

	}

	/**************************************** Adapter *********************************************************/

	static class PlaceDropDown extends ArrayAdapter<String>
	{


		Activity context;
		LayoutInflater inflate;
		ArrayList<String>dropDownItem;
		ArrayList<String>store_id;

		public PlaceDropDown(Activity context, int resource,
				int textViewResourceId, ArrayList<String>dropDownItem,ArrayList<String>store_id) {
			super(context, resource, textViewResourceId, dropDownItem);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.dropDownItem=dropDownItem;
			this.store_id=store_id;
			inflate=context.getLayoutInflater();
		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return dropDownItem.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return dropDownItem.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.parentstorelist, null);
				holder.txtParentItem=(TextView)convertView.findViewById(R.id.txtParentItem);
				convertView.setTag(holder);
			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.txtParentItem.setText(dropDownItem.get(position));
			return convertView;
		}

		static class ViewHolder
		{
			TextView txtParentItem;
		}

	}

	class CategoryDropDown extends ArrayAdapter<Model> 
	{

		LayoutInflater inflator;
		ArrayList<Model>list;
		ArrayList<Model>list_id;
		Activity context;

		public CategoryDropDown(Activity context, int resource,
				int textViewResourceId, ArrayList<Model>list,ArrayList<Model>list_id) {
			super(context, resource, textViewResourceId, list);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.list=list;
			this.list_id=list_id;
			inflator=context.getLayoutInflater();

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}



		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub
			View view=null;
			if(convertView==null)
			{
				final ViewHolder viewHolder=new ViewHolder();
				inflator=context.getLayoutInflater();
				convertView=inflator.inflate(R.layout.categorylistlayout, null);
				viewHolder.chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
				viewHolder.txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);

				viewHolder.chkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub
						Model element=(Model)viewHolder.chkBox.getTag();
						element.setSelected(buttonView.isChecked());
						if(buttonView.isChecked())
						{

							tempCatName.add(list.get(position).getName());
							tempCatId.add(list_id.get(position).getId());
						}
						else
						{

							tempCatName.remove(list.get(position).getName());
							tempCatId.remove(list_id.get(position).getId());
							for (int i=0; i<tempCatId.size();) {
								if (tempCatId.get(i).equalsIgnoreCase(list_id.get(position).getId())) {
									tempCatId.remove(i);
								} else {
									i++;
								}
							}
						}
					}
				});

				convertView.setTag(viewHolder);
				viewHolder.chkBox.setTag(list.get(position));
			}
			else
			{
				view=convertView;
				((ViewHolder)convertView.getTag()).chkBox.setTag(list.get(position));
			}
			ViewHolder holder=(ViewHolder)convertView.getTag();
			holder.chkBox.setChecked(list.get(position).isSelected());
			holder.txtCategory.setText(list.get(position).getName());


			return convertView;
		}
	}

	class CategoryDropDownNew extends ArrayAdapter<String> 
	{

		LayoutInflater inflator;
		ArrayList<String>cat_name;
		ArrayList<String>cat_id;
		Activity context;

		public CategoryDropDownNew(Activity context, ArrayList<String>cat_name,ArrayList<String>cat_id) {
			super(context, R.layout.categorylistlayout, cat_name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.cat_name=cat_name;
			this.cat_id=cat_id;
			inflator=context.getLayoutInflater();
			InorbitLog.d("onAdapter");
			for(int i=0;i<cat_name.size();i++){
				InorbitLog.d(cat_name.get(i)+" --- " +cat_id.get(i));
			}

		}




		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return cat_name.size();
		}



		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub
			View view=null;
			if(convertView==null)
			{
				final ViewHolder viewHolder=new ViewHolder();
				inflator=context.getLayoutInflater();
				convertView=inflator.inflate(R.layout.categorylistlayout, null);
				viewHolder.chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
				viewHolder.txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);

				viewHolder.chkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub

						if(isChecked){
							if(tempCatId.contains(cat_id)){
								
							}else{
								tempCatId.add(cat_id.get(position));
								tempCatName.add(cat_name.get(position));
								/*if (position == 1) {
									tempCatId.add(cat_id.get(4));
									tempCatName.add(cat_name.get(4));
									notifyDataSetChanged();
								}*/
							}
						}else{
							tempCatId.remove(cat_id.get(position));
							tempCatName.remove(cat_name.get(position));
							/*showToast(tempCatId.contains(cat_id.get(position))+"");*/
							
							//Iterate to remove duplicates
							for (int i=0; i<tempCatId.size(); ) {
								if (tempCatId.get(i).equalsIgnoreCase(cat_id.get(position))) {
									tempCatId.remove(i);
								} else {
									i++;
								}
							}
							
							/*if (position == 1) {
								if (tempCatId.contains(cat_id.get(4))) {
									showToast(cat_id.get(4));
									tempCatId.remove(cat_id.get(4));
								}
								if (tempCatName.contains(cat_name.get(4))) {
									showToast(cat_name.get(4));
									tempCatName.remove(cat_name.get(4));
								}
								notifyDataSetChanged();
							}*/
						}
						/*String p = "";
						for(String s: tempCatId) {
							p+=s+" ";
						}
						showToast("TempCatId: "+ p);
						
						p = "";
						for(String s: cat_id) {
							p+=s+" ";
						}
						showToast("CatId: "+ p);*/
						
					}
				});

				convertView.setTag(viewHolder);
			}

			ViewHolder holder=(ViewHolder)convertView.getTag();
			holder.txtCategory.setText(cat_name.get(position));
			InorbitLog.d(cat_name.get(position));

			if(STORE_CATEGORY_ID.contains(cat_id.get(position))){
				holder.chkBox.setChecked(true);
			}else{
				holder.chkBox.setChecked(false);
			}

			return convertView;
		}
	}

	class MallsAdapter extends ArrayAdapter<String>{
		LayoutInflater inflator;
		ArrayList<String> mallsArr;
		ArrayList<String> mallPArentId;
		ArrayList<Boolean> checkedMall;
		Activity context;

		public MallsAdapter(Activity context, ArrayList<String> mallsArr ,ArrayList<String> mallPArentId){
			super(context, R.layout.categorylistlayout,mallsArr);

			this.context=context;
			this.mallsArr=mallsArr;
			this.mallPArentId=mallPArentId;
			checkedMall = new ArrayList<Boolean>();
			for(int i=0;i<mallsArr.size();i++){
				checkedMall.add(false);
			}


		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mallsArr.size();
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			View view=convertView;
			if(convertView==null)
			{
				ViewHolder viewHolder=new ViewHolder();
				inflator=context.getLayoutInflater();
				convertView=inflator.inflate(R.layout.categorylistlayout, null);
				viewHolder.chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
				viewHolder.txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);

				viewHolder.chkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub

						if(buttonView.isChecked())
						{
							SELECTED_PLACE_PARENT = mallPArentId.get(position);
							//SELECTED_MALL_LOCATION.setText(mallsArr.get(position));
							SELECTED_MALL_ID = dbutil.getMallIdByPlaceParent(SELECTED_PLACE_PARENT);
							SELECTED_MALL_LOCATION.setText(dbutil.getMallNameById(SELECTED_MALL_ID));
							String pincode = dbutil.getMallPinCode(SELECTED_MALL_ID);
							String city = dbutil.getMallCity(SELECTED_MALL_ID);

							edtMallCity.setText(city);
							edtMallPincode.setText(pincode);
							mallsDialog.dismiss();

							//checkedMall.set(position, true);
							setCheck(position);

							Log.d("", "Inorbit SELECTED_MALL_ID >> "+SELECTED_MALL_ID);
							Log.d("", "Inorbit SELECTED_MALL_LOCATION >> "+mallsArr.get(position));
							Log.d("", "Inorbit SELECTED_PLACE_PARENT >> "+SELECTED_PLACE_PARENT);
							Log.d("", "Inorbit SELECTED_PinCode>> "+pincode);
							Log.d("", "Inorbit SELECTED_city >> "+city);

						}
						else
						{


						}
					}
				});

				convertView.setTag(viewHolder);

			}

			ViewHolder hold = (ViewHolder) convertView.getTag();
			hold.txtCategory.setText(mallsArr.get(position));
			if(checkedMall.get(position)){
				hold.chkBox.setChecked(true);
			}else{
				hold.chkBox.setChecked(false);
			}
			Log.d("", "Inorbit mall "+mallsArr.get(position));
			return convertView;
		}


		void setCheck(int pos){
			try{
				for(int i =0;i<checkedMall.size();i++){
					if(i==pos){
						checkedMall.set(i, true);
					}else{
						checkedMall.set(i, false);
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}



	}

	static class ViewHolder{

		TextView txtCategory;
		CheckBox chkBox;
	}

	class GridAdapter extends BaseAdapter
	{
		ArrayList<String>photo,title,id, THUMB;
		Activity context;
		LayoutInflater inflate;
		String image;

		public GridAdapter(Activity context,ArrayList<String>photo,ArrayList<String>id,ArrayList<String>title, ArrayList<String> THUMB)
		{
			this.context=context;
			this.photo=photo;
			this.title=title;
			this.id=id;
			this.THUMB = THUMB;
			inflate=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return photo.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return photo.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub

			final int pos = position;

			if(convertView==null)
			{

				Log.d("INIF", "INIF");
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.gridimagelayout,null);

				holder.photo=(ImageView)convertView.findViewById(R.id.imgGridImage);
				holder.txtImageTitle=(TextView)convertView.findViewById(R.id.txtImageTitle);
				holder.toChkDelete=(CheckBox)convertView.findViewById(R.id.toChkDelete);

				if(showEditMode){

					holder.toChkDelete.setVisibility(View.VISIBLE);
				}

				holder.toChkDelete.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub

						if(buttonView.isChecked()){

							IDSTODELETE.add(ID.get(pos));
						}
						else{
							IDSTODELETE.remove(ID.get(pos));
						}
					}
				});

				convertView.setTag(holder);
			} else {
				Log.d("INIF", "INELSE");	
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();
			if(THUMB.get(position)!=null && THUMB.get(position).length()!=0 && photo.get(position)!=null && photo.get(position).length()!=0)
			{
				image=THUMB.get(position).replaceAll(" ", "%20");
				//imageLoader.DisplayImage(PHOTO_URL+image,hold.photo);


				try {
					Picasso.with(context).load(PHOTO_URL+image)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(hold.photo);

				} catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){
					e.printStackTrace();
				}



			}
			if(position==0){

			}
			hold.txtImageTitle.setVisibility(View.GONE);

			return convertView;
		}

		class ViewHolder
		{
			ImageView photo;
			TextView txtImageTitle;
			CheckBox toChkDelete;
		}

	}

	//Open gallery to choose photo to upload.
	public  void choosePict(Context context){

		try
		{
			Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
			photoPickerIntent.setType("image/*");
			startActivityForResult(photoPickerIntent, SELECT_PHOTO);
		}
		catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub

		Bitmap bitmap;

		Log.i("RESULT CODE","REQUEST CODE "+requestCode+" Result Code "+resultCode);
		
		if(requestCode == REQUEST_CODE_SELECT_PICTURE_GALLERY) {
			InorbitLog.d("REQUEST_CODE_SELECT_PICTURE_GALLERY");
			if(resultCode == Activity.RESULT_OK) {
				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";

				HashMap<String, String> map = new HashMap<String, String>();
				map.put("Source", "Gallery");
				EventTracker.logEvent(getResources().getString(R.string.mStore_ImageAdd), map);
				
				/*try{
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};
					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();
					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);
					c.close();
					cropCapturedImage(imageUri,REQUEST_CODE_GALL_CROP_GALLERY);
				}catch(Exception e){
					InorbitLog.e("Exception at Create broacast class");
					e.printStackTrace();
				}*/
				
				try {
					Uri imageUri = data.getData();
					angularRotation = getOrientation(data.getData());
					if (angularRotation != 0)
						rotateImage = true;
					else 
						rotateImage = false;
					Back t = new Back();
					t.execute(imageUri);
				}catch(Exception e){
					InorbitLog.e("Exception at Create broacast class");
					e.printStackTrace();
				}
			}
		}

		if(requestCode == REQUEST_CODE_GALL_CROP_GALLERY){

			InorbitLog.d("REQUEST_CODE_GALL_CROP_GALLERY");
			if(resultCode == Activity.RESULT_OK){
				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";
				

				//Create an instance of bundle and get the returned data
				Bundle extras = data.getExtras();
				//get the cropped bitmap from extras
				Bitmap thePic = extras.getParcelable("data");
				
				CameraImageSave cameraSaveImage = new CameraImageSave();

				try{
					cameraSaveImage.cretaeFile();
					cameraSaveImage.saveBitmapToFile(thePic);
				}catch(Exception ex){
					ex.printStackTrace();
				}

				try{
					Bitmap bitmap1 = cameraSaveImage.getBitmapFromFile(480, 480);
					if(bitmap1!=null){
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap1.compress(Bitmap.CompressFormat.JPEG, 80, stream);
						cameraSaveImage.deleteFromFile();
						mByteArrGall = stream.toByteArray();
						bitmap = null;
						cameraSaveImage = null;
					}
					setPhoto(BitmapFactory.decodeByteArray(mByteArrGall , 0,mByteArrGall.length),true,"");///Correct
				}
				catch(Exception e){
					InorbitLog.e("Exception at Create broacast class");
					e.printStackTrace();
				}
			}
		}



		if (requestCode == REQUEST_CODE_SELECT_PICTURE_LOGO) {
			InorbitLog.d("REQUEST_CODE_POSTGALLERY");
			if(resultCode == Activity.RESULT_OK) {
				Uri imageUri=data.getData();
				String[] filePathColumn={MediaStore.Images.Media.DATA};
				Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
				c.moveToFirst();
				int columnIndex=c.getColumnIndex(filePathColumn[0]);
				String picturePath=c.getString(columnIndex);
				c.close();
				cropCapturedImage(imageUri,REQUEST_POST_GALL_CROP_LOGO);
			}
		}


		if(requestCode == REQUEST_CODE_TAKE_PICTURE_GALLERY) {
			InorbitLog.d("REQUEST_CODE_TAKE_PICTURE_GALLERY");
			if(resultCode == Activity.RESULT_OK) {

				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";

				try{
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("Source", "Camera");
					EventTracker.logEvent(getResources().getString(R.string.mStore_ImageAdd), map);
					
					/*Bitmap bitmap1 = null;
					CameraImageSave cameraImageSave = new CameraImageSave();
					bitmap1 = cameraImageSave.getBitmapFromFile(480, 480);
					if(bitmap1 != null){
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap1.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						mByteArrGall = stream.toByteArray();
						cameraImageSave.deleteFromFile();
						cameraImageSave = null;
						bitmap = null;
					}
					setPhoto(BitmapFactory.decodeByteArray(mByteArrGall , 0, mByteArrGall.length),true);*/
					Uri dataUri = data.getData();
					if (dataUri == null) {
						Cursor cursor = getContentResolver().query(Media.EXTERNAL_CONTENT_URI,
								new String[]{Media.DATA, Media.DATE_ADDED, MediaStore.Images.ImageColumns.ORIENTATION},
								Media.DATE_ADDED, null, Media.DATE_ADDED+" ASC");
						if(cursor != null && cursor.moveToFirst())
						{
						    do {
						        dataUri = Uri.parse(cursor.getString(cursor.getColumnIndex(Media.DATA)));
						    } while(cursor.moveToNext());
						    cursor.close();
						}
					}
					
					angularRotation = getOrientation(dataUri);
					if (angularRotation != 0)
						rotateImage = true;
					else 
						rotateImage = false;
					Back t = new Back();
					t.execute(dataUri);
				
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}

		if(requestCode == REQUEST_CODE_TAKE_PICTURE_LOGO){
			InorbitLog.d("REQUEST_CODE_TAKE_PICTURE_LOGO");
			if(resultCode == Activity.RESULT_OK){
				try{
					Bitmap bitmap1 = null;
					CameraImageSave cameraImageSave = new CameraImageSave();
					bitmap1 = cameraImageSave.getBitmapFromFile(180, 180);
					if(bitmap1 != null){
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap1.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						mByteArrLogo = stream.toByteArray();
						cameraImageSave.deleteFromFile();
						cameraImageSave = null;
						bitmap = null;
					}
					setPhoto(BitmapFactory.decodeByteArray(mByteArrLogo , 0, mByteArrLogo.length),false, "");
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}

		if(requestCode == REQUEST_POST_GALL_CROP_LOGO){
			InorbitLog.d("REQUEST_POST_GALL_CROP_LOGO");
			
			if(resultCode == Activity.RESULT_OK){
				try {
					
					Bundle extras = data.getExtras();
					//get the cropped bitmap from extras
					bitmap = extras.getParcelable("data");
					
					CameraImageSave cameraImageSave = new CameraImageSave();
					
					try{
						cameraImageSave.cretaeFile();
						cameraImageSave.saveBitmapToFile(bitmap);
					}catch(Exception ex){
						ex.printStackTrace();
					}
					
					Bitmap bitmap1 = null;
					bitmap1 = cameraImageSave.getBitmapFromFile(180, 180);
					if(bitmap1 != null){
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap1.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						mByteArrLogo = stream.toByteArray();
						//cameraImageSave.c
						cameraImageSave.deleteFromFile();
						cameraImageSave = null;
						bitmap = null;
					}
					setPhoto(BitmapFactory.decodeByteArray(mByteArrLogo , 0, mByteArrLogo.length),false, ""); //Correct
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		
		InorbitLog.d("REQUEST_CODE_CUSTOMCAMERA--1");
		super.onActivityResult(requestCode, resultCode, data);
		InorbitLog.d("requestCode "+requestCode);
	}
	
	public int getOrientation(Uri selectedImage) {
	    int orientation = 0;
	    final String[] projection = new String[]{MediaStore.Images.Media.ORIENTATION};      
	    final Cursor cursor = context.getContentResolver().query(selectedImage, projection, null, null, null);
	    if(cursor != null) {
	        final int orientationColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION);
	        if(cursor.moveToFirst()) {
	            orientation = cursor.isNull(orientationColumnIndex) ? 0 : cursor.getInt(orientationColumnIndex);
	        }
	        cursor.close();
	    }
	    return orientation;
	}
	
	class Back extends AsyncTask<Uri, String, Bitmap> {
		//byte[] mByteArr = null;
		String picturePath = "";
		Uri uriForImage = null;
		byte[] localImgArr = null;
		@Override
		protected void onPreExecute() {
			progUpload.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}
		
		@Override
		protected Bitmap doInBackground(Uri...params) {
			try {
				String encodedpath="";
				
				SessionManager session = new SessionManager(context);
				uriForImage = params[0];
				String[] results = getRealPathFromURI(uriForImage);
				InputStream iStream = new FileInputStream(results[0]);//getContentResolver().openInputStream(uriForImage);
				mByteArrGall = getBytes(iStream);
				if (mByteArrGall.length < 2000000) {
					/*String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c = getContentResolver().query(uriForImage, filePathColumn, null, null, null);
					c.moveToFirst();
					
					int columnIndex=c.getColumnIndex(filePathColumn[0]);*/
					picturePath = results[0];
					FILE_TYPE = results[1];
					//c.getString(columnIndex);
					//c.close();
					InorbitLog.d(">>>>Picture Path: >>>"+picturePath);
					BitmapFactory.Options opt =  new BitmapFactory.Options();
				    
					opt.inJustDecodeBounds = true;
					
			        /*if (mByteArrGall.length > getResources().getInteger(R.integer.thumbnail_size_limit));
			        	opt.inSampleSize = 8;*/
					BitmapFactory.decodeFile(picturePath, opt);
					
					opt.inSampleSize = calculateInSampleSize(opt, 480, 480);
					
					opt.inJustDecodeBounds = false;
				    
			        Bitmap thePic = BitmapFactory.decodeFile(picturePath, opt);
			        return thePic;
			        /*CameraImageSave cameraSaveImage = new CameraImageSave();
			    	
					try{
						cameraSaveImage.cretaeFile();
						cameraSaveImage.saveBitmapToFile(thePic);
					}catch(Exception ex){
						ex.printStackTrace();
					}

					try{
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480, 480);
						cameraSaveImage.deleteFromFile();
						return bitmap;
					}
					catch(Exception e){
						InorbitLog.e("Exception at Create broacast class");
						e.printStackTrace();
					}*/
				} else {
					publishProgress("Image is too big");
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			return null;
		}
		
		public int calculateInSampleSize (BitmapFactory.Options options, int reqWidth, int reqHeight) {
		    // Raw height and width of image
		    final int height = options.outHeight;
		    final int width = options.outWidth;
		    int inSampleSize = 1;
		    
		    if (height > reqHeight || width > reqWidth) {
		    	
		        final int halfHeight = height / 2;
		        final int halfWidth = width / 2;
		        
		        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
		        // height and width larger than the requested height and width.
		        while ((halfHeight / inSampleSize) > reqHeight
		                && (halfWidth / inSampleSize) > reqWidth) {
		            inSampleSize *= 2;
		        }
		    }
		    
		    return inSampleSize;
		}
		
		public byte[] getBytes(InputStream inputStream) throws IOException {
		      ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		      int bufferSize = 1024;
		      byte[] buffer = new byte[bufferSize];

		      int len = 0;
		      while ((len = inputStream.read(buffer)) != -1) {
		        byteBuffer.write(buffer, 0, len);
		      }
		      return byteBuffer.toByteArray();
		}
		
		private String[] getRealPathFromURI(Uri contentURI) {
		    String[] result = new String[2];
		    Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
		    if (cursor == null) { // Source is Dropbox or other similar local file path
		        result[0] = contentURI.getPath();
		        result[1] = "image/jpeg";
		    } else {
		        cursor.moveToFirst();
		        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
		        result[0] = cursor.getString(idx);
		        result[1] = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.MIME_TYPE));
		        InorbitLog.d("Cursor not null "+" Path "+result);
		        cursor.close();
		    }
		    return result;
		}
		
		@Override
		protected void onProgressUpdate(String... values) {
			showToast(""+values[0]);
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			progUpload.setVisibility(View.GONE);
			if (result != null) {
				setPhoto(result, true, picturePath);
			}
		}
	}
	
	void registerPhotoEvent(String photoFrom){
		if(!photoFrom.equalsIgnoreCase("")){
			Map<String, String> params = new HashMap<String, String>();
			params.put("PictureFrom", photoFrom);
			EventTracker.logEvent(getResources().getString(R.string.store_LogoAdd), params);
		}
	}

	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

	public void cropCapturedImage(Uri picUri, int ReqCode){
		//call the standard crop action intent 
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		//indicate image type and Uri of image
		cropIntent.setDataAndType(picUri, "image/*");
		//set crop properties
		cropIntent.putExtra("crop", "true");
		//indicate aspect of desired crop
		cropIntent.putExtra("aspectX", 1);
		cropIntent.putExtra("aspectY", 1);
		//indicate output X and Y
		cropIntent.putExtra("outputX", 256);
		cropIntent.putExtra("outputY", 256);
		//retrieve data on return
		cropIntent.putExtra("return-data", true);
		//start the activity - we handle returning in onActivityResult
		startActivityForResult(cropIntent, ReqCode);
	}


	void setPhoto(Bitmap bitmap,boolean isLogoImage, String picturePath){
		
		if (FILE_TYPE == null || FILE_TYPE.equalsIgnoreCase(""))
			FILE_TYPE = "image/jpeg";
		
		if(isLogoImage){
			
			try {
				if (rotateImage)
					bitmap = rotateImage(bitmap, picturePath);
				
				addGalleryImage.setImageBitmap(bitmap);
				ImageView addGalleryPagerImage=(ImageView)mPager.findViewById(R.id.addGalleryPagerImage);
				addGalleryPagerImage.setImageBitmap(bitmap);
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
			if(isnetConnected.isNetworkAvailable()){
				postGalleryData();
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
			
			
		}else{
			View pager=(View)mPager.getChildAt(2);
			ImageView imgThumb=(ImageView)mPager.findViewById(R.id.imgManageStore_ThumbPreview);
			imgThumb.setImageBitmap(bitmap);
		}
	}
	
	private Bitmap rotateImage(Bitmap bmp, String path) {
        // TODO Auto-generated method stub

        Matrix matrix=new Matrix();

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String orientstring = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientstring != null ? Integer.parseInt(orientstring) : ExifInterface.ORIENTATION_NORMAL;
        int rotateangle = 0;
        if(orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            rotateangle = 90;
        } if(orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            rotateangle = 180;
        }if(orientation == ExifInterface.ORIENTATION_ROTATE_270) {
        	//mByteArr = rotateByteArray(mByteArr);
            rotateangle = 270;
        }
        
        //imageView.setScaleType(ScaleType.CENTER_CROP);   //required
        matrix.setRotate(rotateangle);
        //imageView.setImageMatrix(matrix);



        Bitmap newBit = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        
        //imageView.setImageBitmap(newBit);
        rotateImage = false;
        return newBit;

    }

	public class GridItemClickListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
			// TODO Auto-generated method stub

			if(!isNew){
				
			} else {
				showToast("Please create store first.");
				mPager.setCurrentItem(0);
			}
			Intent intent=new Intent(context,StorePagerGallery.class);
			intent.putExtra("photo_source", PHOTO_SOURCE);
			intent.putExtra("position", position);
			startActivity(intent);

		}
	}


	class StoreInforReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String success="";
			String code = "";
			try{
				progStoreUpdate.setVisibility(View.GONE);
				progUpload.setVisibility(View.GONE);
				Bundle bundle = intent.getExtras();
				success = intent.getStringExtra("SUCCESS");
				if(success.equalsIgnoreCase("true")){

					loadAllCategories();
					STORE_CATEGORY_ID = intent.getStringArrayListExtra("Categories");
					for(int i=0;i<STORE_CATEGORY_ID.size();i++){
						InorbitLog.d(" Cat >> "+STORE_CATEGORY_ID.get(i));
					}
					//bundle = intent.getBundleExtra("BundleData");
					if(bundle!=null){
						//STORE_CATEGORY_ID = bundle.getStringArrayList("Categories");
						ArrayList<String> Time1_OPEN	=new ArrayList<String>();
						ArrayList<String> Time1_CLOSE	=new ArrayList<String>();
						ArrayList<String> PLACE_STATUS	=new ArrayList<String>();
						ArrayList<String> DAY			=new ArrayList<String>();
						ArrayList<ParticularStoreInfo> storeInfoArr = intent.getParcelableArrayListExtra("ParticularStoreInfoDetails");
						ParticularStoreInfo storeInfo = storeInfoArr.get(0);
						PLACE_PARENT = storeInfo.getPlace_parent();
						SELECTED_PLACE_PARENT = PLACE_PARENT;
						SELECTED_MALL_ID =storeInfo.getMall_id();
						latitued=storeInfo.getLatitude();
						longitude=storeInfo.getLongitude();

						STORE_OPEN_TIME = storeInfo.getOpen_time();
						STORE_CLOSE_TIME = storeInfo.getClose_time();

						setImage(latitued, longitude);

						setDataToPagerNew(storeInfo);

						setMallDropDown(PLACE_PARENT);

						setOperationData(Time1_OPEN,Time1_CLOSE,DAY,PLACE_STATUS,storeInfo.getOpen_time(),storeInfo.getClose_time());
						
						place_status=storeInfo.getPlace_status();

						invalidateActionBar();
					}
				}else{

					String message = intent.getStringExtra("MESSAGE");
					Toast.makeText(context, "Seems you were login from other device too. Kindly Re-login to continue", Toast.LENGTH_LONG).show();

					code = intent.getStringExtra("CODE");
					if(code.equalsIgnoreCase("-116")){
						logOutUser();
					}
				}

			}catch(Exception ex){
				ex.printStackTrace();
				Toast.makeText(context, "catch "+success, Toast.LENGTH_SHORT).show();
			}
		}

	}

	void showToast(String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}


	void logOutUser(){
		session.logoutUser();
		Intent sIntent = new Intent(context, MerchantLogin.class);
		startActivity(sIntent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}


	void setMallDropDown(ArrayList<String> areas, ArrayList<String> placeParentId){
		try{

			if(areas==null || placeParentId==null){
				View page=(View)mPager.getChildAt(1);
				final TextView ownerDropDown=(TextView)page.findViewById(R.id.txtManageArea);
				final View view_seprator=(View)page.findViewById(R.id.view_seprator);
				view_seprator.setVisibility(View.GONE);
				ownerDropDown.setVisibility(View.GONE);
			}else{
				final ListView listCateg=(ListView)mallsDialog.findViewById(R.id.categDilogList);

				ArrayList<String> mallsArr 	= areas;
				ArrayList<String> mallPArentId 	= placeParentId;


				SELECTED_PLACE_PARENT = mallPArentId.get(0);
				//SELECTED_MALL_LOCATION.setText(mallsArr.get(position));
				SELECTED_MALL_ID = dbutil.getMallIdByPlaceParent(SELECTED_PLACE_PARENT);
				SELECTED_MALL_LOCATION.setText(dbutil.getMallNameById(SELECTED_MALL_ID));
				String pincode = dbutil.getMallPinCode(SELECTED_MALL_ID);
				String city = dbutil.getMallCity(SELECTED_MALL_ID);

				edtMallCity.setText(city);
				edtMallPincode.setText(pincode);

				listCateg.setAdapter(new MallsAdapter(context,mallsArr, mallPArentId));
				mallsDialog.setTitle("Select Preferred Mall");

				Button btnDone=(Button)mallsDialog.findViewById(R.id.btnDoneSelection);
				btnDone.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						mallsDialog.dismiss();
					}
				});
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void setMallDropDown(String placeParentId){
		SELECTED_PLACE_PARENT = placeParentId;
		//SELECTED_MALL_LOCATION.setText(mallsArr.get(position));
		SELECTED_MALL_ID = dbutil.getMallIdByPlaceParent(SELECTED_PLACE_PARENT);
		SELECTED_MALL_LOCATION.setText(dbutil.getMallNameById(SELECTED_MALL_ID));
		String pincode = dbutil.getMallPinCode(SELECTED_MALL_ID);
		String city = dbutil.getMallCity(SELECTED_MALL_ID);

		edtMallCity.setText(city);
		edtMallPincode.setText(pincode);
	}

	void setNewCategoryDropDown(final ArrayList<String>dropDownItem,final ArrayList<String>CATEG_ID)
	{
		try
		{
			View page=(View)mPager.getChildAt(0);
			final TextView categDropDown=(TextView)mPager.findViewById(R.id.txtManageCategory);

			String categ = "";

			final ListView listCateg=(ListView)categoryDialog.findViewById(R.id.categDilogList);

			ArrayList<Model> list=new ArrayList<Model>();
			ArrayList<Model> list_id=new ArrayList<Model>();

			for(int i=0;i<dropDownItem.size();i++){

				list_id.add(getID(dropDownItem.get(i),CATEG_ID.get(i),false));
				list.add(get(dropDownItem.get(i),false));


			}

			CategoryDropDown adapter=new CategoryDropDown(context, R.drawable.ic_launcher,  R.drawable.ic_launcher, list, list_id);
			listCateg.setAdapter(adapter);

			categoryDialog.setTitle("Select Category");

			categDropDown.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					categoryDialog.show();		
				}
			});

			Button btnDone=(Button)categoryDialog.findViewById(R.id.btnDoneSelection);
			btnDone.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub


					//Copying selected items to String
					categ_list=tempCatName.toString();
					//Setting selected categories to Textview

					categ_list=categ_list.substring(1, categ_list.length()-1).trim();
					if(categ_list.equalsIgnoreCase("")){
						categ_list = "Select Category *";
					}
					categDropDown.setText(categ_list);

					categoryDialog.dismiss();

				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}



	private Model get(String s,boolean selected) {
		return new Model(s,selected);
	}

	private Model getID(String name,String id,boolean selected) {
		Model m=new Model(name,selected);
		m.setId(id);
		return m;
	}

	void setOperationData(ArrayList<String> Time1_OPEN,ArrayList<String> Time1_Close,ArrayList<String> DAY,ArrayList<String> PLACE_STATUS,String open_time,String close_time){

		try{
			//time page.
			TextView txtMangeStoreTimeFrom=(TextView)mPager.findViewById(R.id.txtMangeStoreTimeFrom);
			TextView txtMangeStoreTimeTo=(TextView)mPager.findViewById(R.id.txtMangeStoreTimeTo);
			Log.i("DAY","open_time : "+open_time+" close_time : "+close_time);
			txtMangeStoreTimeFrom.setText(open_time);
			txtMangeStoreTimeTo.setText(close_time);
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}
	
	String getTime(String time){

		//Splitting string
		String [] splitstring;
		String time_status="";

		//Splitting string based on comma
		splitstring=time.split( ",\\s*");

		ArrayList<String>output=new ArrayList<String>();


		//Adding string to arraylist
		for(String string:splitstring){
			output.add(string);
		}


		for(int i=0;i<output.size();i++)
		{
			if(!output.get(i).equalsIgnoreCase("closed"))
			{
				time_status=output.get(i);
			}
			Log.i("Closed","Closed seprated "+output.get(i));
		}

		Log.i("Closed","Opened at "+time_status);

		return time_status;

	}

	/************************************************* Image Capturing Section ***********************************************/

	private void openGalleryForGrid() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		//startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
	}

	//add image gallery
	private void takePictureForGallery(){

		try {
			/*Intent intent = new Intent(EditStore.this,CameraView.class);
			startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE_GALLERY);*/
			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			if (intent.resolveActivity(getPackageManager()) != null) {
				startActivityForResult(intent,REQUEST_CODE_TAKE_PICTURE_GALLERY);
			} else {
				showToast("No Supporting Camera Application Found");
			}
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
			InorbitLog.d("cannot take picture");
		}


	}

	//select image gallery
	private void selectPictureForGallery(){

		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_SELECT_PICTURE_GALLERY);
	}


	private void takePictureForGrid() {

		/*Intent intent = new Intent(EditStore.this, PhotoActivity.class);
		intent.putExtra("REQUEST_CODE_TAKE_PAGER_PICTURE",REQUEST_CODE_TAKE_PAGER_PICTURE);
		startActivityForResult(intent, REQUEST_CODE_TAKE_PAGER_PICTURE);*/
	}


	void setDataToPagerNew(ParticularStoreInfo storeInfo){

		try{

			//location page
			EditText edtManageStoreName=(EditText)mPager.findViewById(R.id.edtManageStoreName);
			EditText edtManageDesc=(EditText)mPager.findViewById(R.id.edtManageDesc2);
			EditText edtManageFloor=(EditText)mPager.findViewById(R.id.edtManageFloor);
			EditText edtManageUnit=(EditText)mPager.findViewById(R.id.edtManageUnit);


			EditText edtManageCity=(EditText)mPager.findViewById(R.id.edtManageCity);
			EditText edtManagePinCode=(EditText)mPager.findViewById(R.id.edtManagePinCode);

			TextView edtManageArea=(TextView)mPager.findViewById(R.id.txtManageArea);


			edtManageStoreName.setText(storeInfo.getName());
			edtManageDesc.setText(storeInfo.getDescription());
			edtManageFloor.setText(storeInfo.getBuilding());
			edtManageUnit.setText(storeInfo.getStreet());
			edtManageArea.setText(storeInfo.getArea());
			edtManageCity.setText(storeInfo.getCity());
			edtManagePinCode.setText(storeInfo.getPincode());

			//contact page.
			TextView txtMangeContactStoreName=(TextView)mPager.findViewById(R.id.txtMangeContactStoreName);
			EditText edtManageLandline1=(EditText)mPager.findViewById(R.id.edtManageLandline1);
			EditText edtManageMobile1=(EditText)mPager.findViewById(R.id.edtManageMobile1);
			EditText edtManageLandline2=(EditText)mPager.findViewById(R.id.edtManageLandline2);
			EditText edtManageLandline3=(EditText)mPager.findViewById(R.id.edtManageLandline3);
			EditText edtManageMobile2=(EditText)mPager.findViewById(R.id.edtManageMobile2);
			EditText edtManageMobile3=(EditText)mPager.findViewById(R.id.edtManageMobile3);
			EditText edtManageFax=(EditText)mPager.findViewById(R.id.edtManageFax);
			EditText edtManageTollFree=(EditText)mPager.findViewById(R.id.edtManageTollFree);
			EditText edtManageEmailId=(EditText)mPager.findViewById(R.id.editManageEmailId);
			EditText edtManageWebSite=(EditText)mPager.findViewById(R.id.edtManageWebSite);

			txtMangeContactStoreName.setText(store_name);

			edtManageLandline1.setText(storeInfo.getTel_no1());
			edtManageLandline2.setText(storeInfo.getTel_no2());
			edtManageLandline3.setText(storeInfo.getTel_no3());

			String mobno1 = storeInfo.getMob_no1();
			if(storeInfo.getMob_no1()!=null && storeInfo.getMob_no1().length()>0)
			{
				mobno1=storeInfo.getMob_no1().substring(2);
			}
			edtManageMobile1.setText(mobno1);



			String mobno2 = storeInfo.getMob_no2();
			if(storeInfo.getMob_no2()!=null && storeInfo.getMob_no2().length()>0)
			{
				mobno2=storeInfo.getMob_no2().substring(2);
			}
			edtManageMobile2.setText(mobno2);

			String mobno3 = storeInfo.getMob_no3();
			if(storeInfo.getMob_no3()!=null && storeInfo.getMob_no3().length()>0)
			{
				mobno3=storeInfo.getMob_no3().substring(2);
			}
			edtManageMobile3.setText(mobno3);


			edtManageFax.setText(storeInfo.getFax_no1());
			edtManageTollFree.setText(storeInfo.getToll_free_no1());
			edtManageEmailId.setText(storeInfo.getEmail());
			edtManageWebSite.setText(storeInfo.getWebsite());


			ImageView imgManageStore_ThumbPreview=(ImageView)mPager.findViewById(R.id.imgManageStore_ThumbPreview);


			imgManageStore_ThumbPreview.setScaleType(ScaleType.FIT_XY);
			String photo=storeInfo.getImage_url().replaceAll(" ", "%20");


			try {

				Picasso.with(context).load(PHOTO_URL+photo)
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(imgManageStore_ThumbPreview);

			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			}
			catch(Exception e){
				e.printStackTrace();
			}




		}catch(IndexOutOfBoundsException ex)
		{
			ex.printStackTrace();	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub


		if (!isNew && manageGallery!=1) {
			SubMenu subMenu1 = menu.addSubMenu("Extra Settings");
			if(place_status.equalsIgnoreCase("0"))
			{
				subMenu1.add("Show store");
			}
			if (place_status.equalsIgnoreCase("1"))
			{
				subMenu1.add("Hide store");
			}

			MenuItem subMenu1Item = subMenu1.getItem();
			subMenu1Item.setIcon(R.drawable.delete);
			subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}




		return true;
	}



	public void hideStore(String url){
		Log.d("", "Inorbit hide url "+url.toString());
		Log.d("", "Inorbit store id "+STORE_ID.toString());
		Log.d("", "Inorbit user id "+USER_ID);
		Log.d("", "Inorbit user auth "+AUTH_ID);

		//url = "http://stage.phonethics.in/inorbitapp/api-1/place_api/place";
		JSONObject json = new JSONObject();
		try {
			json.put("place_id", STORE_ID);
			json.put("place_status", "0");
			json.put("user_id",USER_ID );
			json.put("auth_id", AUTH_ID);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		progStoreUpdate.setVisibility(View.VISIBLE);
		RequestQueue queue = Volley.newRequestQueue(context);
		VolleyResponse.Listener<JSONObject> listner = new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					Toast.makeText(context, "Success", 0).show();
					Log.d("", "Inorbit hide response "+response.toString());
					progStoreUpdate.setVisibility(View.GONE);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		};


		VolleyResponse.ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				try{
					progStoreUpdate.setVisibility(View.GONE);
					Toast.makeText(context, "Error", 0).show();
					Log.d("", "Inorbit hide error "+error.toString());
					Log.d("", "Inorbit hide error message "+error.getMessage());
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		};

		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, json, listner, errorListener);
		request.setShouldCache(false);
		queue.add(request);
	}


	public void deleteStore(String url){

		url = url+"?place_id="+STORE_ID;
		RequestQueue queue = Volley.newRequestQueue(context);

		VolleyResponse.Listener<JSONObject> listner = new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					Toast.makeText(context, "Success", 0).show();
					Log.d("", "Inorbit delete response "+response.toString());
					progStoreUpdate.setVisibility(View.GONE);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		};


		VolleyResponse.ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				try{
					progStoreUpdate.setVisibility(View.GONE);
					Toast.makeText(context, "Error", 0).show();
					Log.d("", "Inorbit delete error "+error.toString());
					Log.d("", "Inorbit delete error message "+error.getMessage());
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		};

		JsonObjectRequest request = new JsonObjectRequest(Method.GET,url, null, listner, errorListener);
		request.setShouldCache(false);
		queue.add(request);
	}



	private class PlaceStatusAsync extends AsyncTask<String, Void, String>{

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
			progUpload.setVisibility(View.GONE);
		}


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progUpload.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String status = "";
			try {

				BufferedReader bufferedReader = null;


				HttpParams httpParams = new BasicHttpParams();

				int timeoutConnection = 30000;
				HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

				//Creating HttpClient.
				HttpClient httpClient=new DefaultHttpClient(httpParams);

				//Setting URL to post data.
				/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/
				HttpPost httpPost=new HttpPost(STORE_URL+ADD_BUSINESS_STORE_PATH);

				Log.i("", "Inorbit Service Response URL "+STORE_URL+ADD_BUSINESS_STORE_PATH);


				httpPost.addHeader("X-HTTP-Method-Override", "PUT");
				httpPost.addHeader(API_HEADER, API_VALUE);

				JSONObject json = new JSONObject();

				json.put("user_id", USER_ID);
				json.put("auth_id", AUTH_ID);
				json.put("place_id", STORE_ID);
				json.put("place_status", place_status);

				StringEntity se = new StringEntity( json.toString());  

				se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				httpPost.setEntity(se);


				// Execute HTTP Post Request
				HttpResponse response = httpClient.execute(httpPost);
				/* Log.i("Response ", " : "+response.toString());*/
				bufferedReader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response : ", "Service Response Store "+stringBuffer.toString());
				status=stringBuffer.toString();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(ConnectTimeoutException c)
			{
				c.printStackTrace();
			}
			catch(SocketTimeoutException st)
			{
				st.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}


			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progUpload.setVisibility(View.GONE);

			String store_status;
			String store_msg = "";
			String store_data="";
			store_status=getStatus(result);
			if(store_status.equalsIgnoreCase("true"))
			{
				store_msg=getMessage(result);
				store_data=getData(result);

				invalidateActionBar();
				SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
				Editor editor=prefs.edit();
				editor.putBoolean("isPlaceRefreshRequired", true);
				editor.commit();

			}
			else
			{
				store_msg=getMessage(result);
				showToast(store_msg);
			}

		}

		String getStatus(String status)
		{
			String userstatus="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				userstatus=jsonobject.getString("success");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return userstatus;
		}

		String getMessage(String status)
		{
			String message="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				message=jsonobject.getString("message");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return message;
		}

		String getData(String status)
		{
			String data="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				JSONObject getDataObject=jsonobject.getJSONObject("data");
				data=getDataObject.getString("place_id");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return data;
		}


	}

	private class DeleteStoreAsync extends AsyncTask<String, Void, String>{

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		String URL="";



		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
			progUpload.setVisibility(View.GONE);
		}



		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progUpload.setVisibility(View.VISIBLE);
		}



		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String status = "";

			try
			{
				BufferedReader bufferedReader = null;


				HttpParams httpParams = new BasicHttpParams();

				int timeoutConnection = 30000;
				HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

				//Creating HttpClient.
				HttpClient httpClient=new DefaultHttpClient(httpParams);

				//Setting URL to post data.
				/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/

				//Adding Place ID to Delete
				nameValuePairs.add(new BasicNameValuePair("place_id",STORE_ID));


				URL=STORE_URL+ADD_BUSINESS_STORE_PATH;

				//Creating URL
				String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
				URL+="?"+paramString;


				HttpDelete httpDelete=new HttpDelete(URL);

				httpDelete.addHeader(API_HEADER, API_VALUE);
				httpDelete.addHeader("user_id", USER_ID);
				httpDelete.addHeader("auth_id", AUTH_ID);

				Log.i("", "Inorbit Service Response URL Delete "+URL);
				Log.i("", "Inorbit Service Response URL Delete "+USER_ID);
				Log.i("", "Inorbit Service Response URL AUTH_ID "+AUTH_ID);

				HttpResponse response=httpClient.execute(httpDelete);

				bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer=new StringBuffer("");
				String line="";
				String LineSeparator=System.getProperty("line.separator");

				while((line=bufferedReader.readLine())!=null)
				{
					stringBuffer.append(line+LineSeparator);
				}
				bufferedReader.close();
				Log.i("Response : ", "Service Response Store Details Delete: "+stringBuffer.toString());
				status=stringBuffer.toString();

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			return  status;
		}



		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			progUpload.setVisibility(View.GONE);

			String store_status;
			String store_msg = "";
			String store_data="";
			store_status=getStatus(result);
			if(store_status.equalsIgnoreCase("true"))
			{
				store_msg=getMessage(result);
				store_data=getData(result);
				showToast(store_msg);
				finishOnDelete();

				SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
				Editor editor=prefs.edit();
				editor.putBoolean("isPlaceRefreshRequired", true);
				editor.commit();

			}
			else
			{
				store_msg=getMessage(result);
				showToast(store_msg);
			}
		}



		String getStatus(String status)
		{
			String userstatus="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				userstatus=jsonobject.getString("success");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return userstatus;
		}

		//TODO Discuss with nitin about this
		String getMessage(String status)
		{
			String message="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				message=jsonobject.getString("message");

			} catch (JSONException e) {
				// TODO Discuss with nitin about this
				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return message;
		}

		String getData(String status)
		{
			String data="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				JSONObject getDataObject=jsonobject.getJSONObject("data");
				data=getDataObject.getString("place_id");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return data;
		}


	}


	void invalidateActionBar()
	{
		this.supportInvalidateOptionsMenu();
	}


	void finishOnDelete()
	{
		try
		{
			hour=0;
			min=0;
			store_landline1="";
			store_landline2="";
			store_landline3="";


			store_mobileno1="";
			store_mobileno2="";
			store_mobileno3="";

			store_faxno="";
			store_tollfree="";
			store_time_from="";
			store_time_to="";
			store_emailId="";
			store_web="";
			latitued="";
			longitude="";
			place_status="";
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		Intent intent=new Intent(context,MerchantHome.class);
		intent.putExtra("isSplitLogin",true);
		intent.putExtra("USER_ID", USER_ID);
		intent.putExtra("AUTH_ID", AUTH_ID);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

	void setCategoryDropDownVolley(ArrayList<String> ALL_CATEGORY_NAME,ArrayList<String> ALL_CATEGORY_ID){
		try{

			View page	=	(View)mPager.getChildAt(1);
			final TextView categDropDown	=	(TextView)mPager.findViewById(R.id.txtManageCategory);

			String categ = "";

			InorbitLog.d("onDropDown");
			for(int i=0;i<ALL_CATEGORY_NAME.size();i++){
				InorbitLog.d(ALL_CATEGORY_NAME.get(i)+" +++ " +ALL_CATEGORY_ID.get(i));
			}

			final ListView listCateg=(ListView)categoryDialog.findViewById(R.id.categDilogList);
			listCateg.setAdapter(new CategoryDropDownNew(context,ALL_CATEGORY_NAME, ALL_CATEGORY_ID));

			categoryDialog.setTitle("Select Category");
			if(isNew){
				
			}else{
				InorbitLog.d("Inside Else");
				ArrayList<String> temName = new ArrayList<String>();
				for(int i=0;i<ALL_CATEGORY_ID.size();i++){
					if(STORE_CATEGORY_ID.contains(ALL_CATEGORY_ID.get(i))) {
						tempCatId.add(ALL_CATEGORY_ID.get(i));
						temName.add(ALL_CATEGORY_NAME.get(i));
						InorbitLog.d("++ >> "+ ALL_CATEGORY_NAME.get(i));
					}else{

					}
				}

				categ_list=temName.toString();
				InorbitLog.d("++ >> "+ temName.toString());
				categ_list=categ_list.substring(1, categ_list.length()-1).trim();
				if(categ_list.equalsIgnoreCase("")){
					categ_list = "Select Category *";
				}
				InorbitLog.d("++ >> "+ categ_list);
				categDropDown.setText(categ_list);

			}

			categDropDown.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					categoryDialog.show();		
				}
			});

			Button btnDone=(Button)categoryDialog.findViewById(R.id.btnDoneSelection);
			btnDone.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub


					//Removing duplicates
					categ_list=tempCatName.toString();
					categ_list=categ_list.substring(1, categ_list.length()-1).trim();
					if(categ_list.equalsIgnoreCase("")){
						categ_list = "Select Category *";
					}
					categDropDown.setText(categ_list);
					categoryDialog.dismiss();
					STORE_CATEGORY_ID=tempCatId;


				}
			});

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	class StoreAddUpdate extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{

				progUpload.setVisibility(View.GONE);
				progStoreUpdate.setVisibility(View.GONE);
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String success = bundle.getString("SUCCESS");
					String msg = bundle.getString("MESSAGE");
					String code="";
					if(success.equalsIgnoreCase("true")){

						if(mPager.getCurrentItem()!=mPager.getAdapter().getCount())
						{
							mPager.setCurrentItem(mPager.getCurrentItem()+1,true);
						}


						if(!isNew)
						{
							registerEvent(getResources().getString(R.string.store_Modified));
							showToast("Store Updated successfully");

						}
						else
						{
							registerEvent(getResources().getString(R.string.store_Added));
							showToast("Store created successfully");
							STORE_ID=bundle.getString("PLACE_ID");
							Log.i("STORE_ID", "STORE_ID CREATED" +STORE_ID);
							//updateStore_timeData();
							isNew=false;
						}

						SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();
					}

					if(success.equalsIgnoreCase("false")){
						showToast(msg);
						progUpload.setVisibility(View.GONE);

						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){


							int code1 = bundle.getInt("CODE");
							String message1=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CategorySearch");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}

						code = intent.getStringExtra("CODE");
						if(code.equalsIgnoreCase("-116")){
							logOutUser();
						}


					}
				}


			}catch(Exception ex){
				ex.printStackTrace();
			}

		}

	}

	class LoadCategories extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				progStoreUpdate.setVisibility(View.GONE);
				String success = intent.getStringExtra("STATUS");
				String code ="";
				if(success.equalsIgnoreCase("true")){
					progUpload.setVisibility(View.GONE);


					ArrayList<String>ALL_CATEGORY_NAME=new ArrayList<String>();
					ArrayList<String>ALL_CATEGORY_ID=new ArrayList<String>();
					ALL_CATEGORY_NAME=intent.getStringArrayListExtra("CATEGORY_LABEL");
					ALL_CATEGORY_ID=intent.getStringArrayListExtra("CATEGORY_ID");


					InorbitLog.d("onRecive");
					for(int i=0;i<ALL_CATEGORY_NAME.size();i++){
						InorbitLog.d(ALL_CATEGORY_NAME.get(i)+" == " +ALL_CATEGORY_ID.get(i));
					}
					setCategoryDropDownVolley(ALL_CATEGORY_NAME, ALL_CATEGORY_ID);


				}else if(success.equalsIgnoreCase("false")){
					String category_msg=intent.getStringExtra("MESSAGE");
					progUpload.setVisibility(View.GONE);
					//showToast(category_msg);


					boolean isVolleyError = intent.getBooleanExtra("volleyError",false);
					if(isVolleyError){


						int code1 = intent.getIntExtra("CODE",0);
						String message1=intent.getStringExtra("MESSAGE");
						String errorMessage = intent.getStringExtra("errorMessage");
						String apiUrl = intent.getStringExtra("apiUrl");	
						if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
							EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CategorySearch");
						}

						Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
					}


					code = intent.getStringExtra("CODE");
					if(code.equalsIgnoreCase("-116")){
						logOutUser();
					}
				}



			}catch(Exception ex){
				ex.printStackTrace();
			}

		}

	}

	void setAlarm(){
		boolean isDiscriptionMissing = false;
		boolean isMobileMissing = false;
		if(store_desc.equalsIgnoreCase("")){
			isDiscriptionMissing = true;
		}else{
			isDiscriptionMissing  = false;
		}

		if(store_mobileno1.length()==2 || store_mobileno2.length()==2 || store_mobileno3.length()==2 || store_landline1.length()==0 || store_landline2.length()==0 || store_landline3.length()==0  ){
			isMobileMissing = true;
		}

		if(isDiscriptionMissing || isMobileMissing ){
			//Toast.makeText(context, "Alarm fired", Toast.LENGTH_SHORT).show();
			LocalNotification localNotification = new LocalNotification(context);
			localNotification.checkPref();
			localNotification.setPlacePref(STORE_ID, true, localNotification.isPlaceAlarmFired());
			localNotification.alarm();
		}

	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			if(Conifg.useServices_m){

			}else{
				//context.unregisterReceiver(broadcast_gallery);
				context.unregisterReceiver(broadcast_StoreInfo);
				context.unregisterReceiver(broadcast_addStore);
				context.unregisterReceiver(broadcast_catogryInfo);
				context.unregisterReceiver(uploadPhotoManageGallery);
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			if(Conifg.useServices_m){

			}else{
				//IntentFilter galleryFilter 			= new IntentFilter(RequestTags.Tag_LoadStoreGallery);
				IntentFilter particularStoreInfo 	= new IntentFilter(RequestTags.TagGetParticularStoreDetails_m);
				IntentFilter addStoreFilter 		= new IntentFilter(RequestTags.Tag_AddStore);
				IntentFilter catgoryFilter 			= new IntentFilter(RequestTags.Tag_CategoryInfo);
				IntentFilter uploadPhotoGallery 			= new IntentFilter(RequestTags.TAG_UPLOAD_PHOTO_GALLERY);
				
				//context.registerReceiver(broadcast_gallery 		= new GalleryReciver(), galleryFilter);
				context.registerReceiver(broadcast_StoreInfo 	= new StoreInforReceiver(), particularStoreInfo);
				context.registerReceiver(broadcast_addStore 	= new StoreAddUpdate(), addStoreFilter);
				context.registerReceiver(broadcast_catogryInfo 	= new LoadCategories(),catgoryFilter);
				context.registerReceiver(uploadPhotoManageGallery = new UploadPhotoGallery(), uploadPhotoGallery);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}



	@Override
	protected void onPause() {
		try{
			EventTracker.endLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onPause();
	}


	/* on start */
	@Override
	protected void onStart() {
		super.onStart();

		try{
			EventTracker.startFlurrySession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void registerEvent(String event){
		try{
			EventTracker.logEvent(event, false);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void registerImageLogo(String event){
		try{
			Map<String, String> params = new HashMap<String, String>();
			params.put("Source", event);
			EventTracker.logEvent(event, params);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private class UploadPhotoGallery extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			boolean status=intent.getBooleanExtra("broadcast_image_status", false);
			String message=intent.getStringExtra("broadcast_message");
			progUpload.setVisibility(View.GONE);
			if (status) {
				registerEvent(getResources().getString(R.string.storeGallery_ImageAdd));
				Toast.makeText(context, "Photo uploaded successfully", Toast.LENGTH_SHORT).show();
				loadGallery();
			}
			else
			{
				Toast.makeText(context, intent.getStringExtra("MESSAGE"), Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void callDeleteImage(String UrlToAppend) {
		// TODO Auto-generated method stub

		progUpload.setVisibility(View.VISIBLE);
		Intent intent = new Intent(context,DeleteGalleryImagesService.class);
		intent.putExtra("deleteImages", deleteImages);
		intent.putExtra("URL", DELETE_IMG_URL + Place_Id + "=" + STORE_ID + UrlToAppend);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);

		Log.d("WHOLEURL","WHOLEURL " + DELETE_IMG_URL + Place_Id + "=" + STORE_ID + UrlToAppend);

		context.startService(intent);
	}

	@Override
	public void onDeleteImage(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		try
		{
			progUpload.setVisibility(View.INVISIBLE);

			String status = resultData.getString("status");

			String message = resultData.getString("message");

			if(status.equalsIgnoreCase("true")){
				IDSTODELETE.clear();
				URLTOAPPEND = "";
				registerEvent(getResources().getString(R.string.storeGallery_ImageDelete));
				loadGallery();

			}
			else{
				showToast(message);
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

}

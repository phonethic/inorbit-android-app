package com.phonethics.inorbit;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.phoenthics.adapter.SharedListViewAdapter;
import com.phoenthics.settings.ConfigFile;

import com.phonethics.eventtracker.EventTracker;

import com.phonethics.model.RequestTags;


public class AboutUs extends SherlockActivity implements OnClickListener {

	TextView aboutAppLable;
	TextView versionLable;
	//TextView prefTutorial;
	TextView contactLable;
	//TextView pref_number;share
	TextView pref_ContactMail;
	TextView infoLable;
	TextView pref_PrivacyPolicy;
	TextView text_store_manager;
	CheckBox		checkManager;

	TextView pref_Terms;
	//TextView pref_Faq;
	TextView versionNo;
	TextView txtPoweredBy,txtFaq,txtRateApp,txtClearCahe,txtAppVersion,textAppVersion;

	Typeface tf;

	ActionBar actionBar;
	Activity context;
	SessionManager session;
	
	TableLayout shareLayout;
	TableLayout inviteFriendsLayout;
	
	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	
	//	String shareText = "Hea, I have found a cool app Inorbit -�http://inorbitapp.in/download -�Why don't you try and experience it yourself.";

	//String shareText = "Hea, I have found a cool app Inorbit -�http://inorbitapp.in/download -�Why don't you try and experience it yourself.";
	//String shareText = "Hi, I have found a cool app Inorbit - http://stage.phonethics.in/inorbitapp/download - Why don't you try and experience it yourself.";
	String shareText = "Get best deals, book movie tickets, win big prizes & get info on your favorite brand. Download the official app of Inorbit Malls http://inorbitapp.in/download";
	Dialog dialogShare;

	ListView listViewShare;

	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;
	private UiLifecycleHelper uiHelper;
	double total_length;

	File file1, file2, file3, file4, file5;
	private TextView txtInviteFriends;
	private Context mContext;
	private TextView text_feedback;

	//	ImageLoader imgLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);
		context=this;
		mContext = this;
		session = new SessionManager(context);
		actionBar=getSupportActionBar();
		actionBar.setTitle("App Info");
		actionBar.setDisplayHomeAsUpEnabled(true);
		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");
		//	imgLoader = new ImageLoader(context);

		aboutAppLable=(TextView)findViewById(R.id.aboutAppLable);
		text_feedback=(TextView)findViewById(R.id.text_feedback);
		versionLable=(TextView)findViewById(R.id.versionLable);
		txtPoweredBy=(TextView)findViewById(R.id.text_powered_by);
		txtPoweredBy.setTextColor(getResources().getColor(R.color.inorbit_blue));
		//prefTutorial=(TextView)findViewById(R.id.prefTutorial);
		contactLable=(TextView)findViewById(R.id.contactLable);
		//pref_number=(TextView)findViewById(R.id.pref_number);
		pref_ContactMail=(TextView)findViewById(R.id.pref_ContactMail);
		infoLable=(TextView)findViewById(R.id.infoLable);
		pref_PrivacyPolicy=(TextView)findViewById(R.id.pref_PrivacyPolicy);
		pref_Terms=(TextView)findViewById(R.id.pref_Terms);
		text_store_manager =(TextView)findViewById(R.id.text_store_manager);
		//pref_Faq=(TextView)findViewById(R.id.pref_Faq);
		//pref_Faq.setVisibility(View.GONE);
		versionNo=(TextView)findViewById(R.id.versionNo);

		shareLayout = (TableLayout) findViewById(R.id.shareLayout);
		inviteFriendsLayout = (TableLayout) findViewById(R.id.inviteFriends);
		inviteFriendsLayout.setVisibility(View.GONE);
		txtFaq = (TextView)findViewById(R.id.text_share_this_app);
		txtRateApp = (TextView)findViewById(R.id.text_rate_this_app);
		txtInviteFriends = (TextView)findViewById(R.id.text_invite_friends);
		txtAppVersion = (TextView)findViewById(R.id.text_app_version);
		textAppVersion= (TextView)findViewById(R.id.text_appVersion);
		txtClearCahe = (TextView) findViewById(R.id.text_clear_cache);
		checkManager = (CheckBox) findViewById(R.id.check_shop_manager);

		aboutAppLable.setTypeface(tf,Typeface.BOLD_ITALIC);
		versionLable.setTypeface(tf);
		//prefTutorial.setTypeface(tf);
		contactLable.setTypeface(tf,Typeface.BOLD_ITALIC);
		//pref_number.setTypeface(tf);
		pref_ContactMail.setTypeface(tf);
		pref_PrivacyPolicy.setTypeface(tf);
		infoLable.setTypeface(tf,Typeface.BOLD_ITALIC);
		pref_Terms.setTypeface(tf);
		//pref_Faq.setTypeface(tf);
		versionNo.setTypeface(tf);
		text_store_manager.setTypeface(tf);
		txtFaq.setTypeface(tf,Typeface.BOLD);
		textAppVersion.setTypeface(tf);
		txtAppVersion.setTypeface(tf);
		txtRateApp.setTypeface(tf);
		txtClearCahe.setTypeface(tf);
		text_feedback.setTypeface(tf);
		
		txtFaq.setText("FAQ");
		

		PackageInfo pinfo = null;
		try {
			pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		versionNo.setText(pinfo.versionName+"("+pinfo.versionCode+")");

		pref_ContactMail.setOnClickListener(this);
		pref_PrivacyPolicy.setOnClickListener(this);
		pref_Terms.setOnClickListener(this);
		text_store_manager.setOnClickListener(this);
		txtPoweredBy.setOnClickListener(this);
		txtFaq.setOnClickListener(this);
		textAppVersion.setOnClickListener(this);
		txtRateApp.setOnClickListener(this);
		txtClearCahe.setOnClickListener(this);
		txtInviteFriends.setOnClickListener(this);
		text_feedback.setOnClickListener(this);
		//pref_Faq.setOnClickListener(this);

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			file1=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitCache");
			file2=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitCustomerCache");
			file3=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitLocalCache");
			file4=new File(android.os.Environment.getExternalStorageDirectory(),"/.Inorbit");
			double total_length_file1 = getFolderSize(file1);
			double total_length_file2 = getFolderSize(file2);
			double total_length_file3 = getFolderSize(file3);
			double total_length_file4 = getFolderSize(file4);
			
			total_length = total_length_file1 + total_length_file2 + total_length_file3 + total_length_file4;
			total_length = total_length/(1024*1024);
			DecimalFormat df = new DecimalFormat("#.##");
			Log.d("GINGERBREAD","GINGERBREAD " + total_length);
			
		}else{
			
			file1=context.getCacheDir();
			file2=context.getCacheDir();
			file3=context.getCacheDir();
			file4=context.getCacheDir();
			//file5=context.getCacheDir();
		}

		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);


		if(session.getApplicationFor()== ConfigFile.ForCustomer){
			checkManager.setChecked(false);
		}else{
			checkManager.setChecked(true);
		}

		checkManager.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					//ConfigFile.setApplicationFor(ConfigFile.ForMerChant);
					session.setApplicationFor(ConfigFile.ForMerChant);
				}else{
					//ConfigFile.setApplicationFor(ConfigFile.ForCustomer);
					session.setApplicationFor(ConfigFile.ForCustomer);
				}
			}
		});


		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");


		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);


		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);


		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
					//inviteFriendsLayout.setVisibility(View.VISIBLE);
				}else{
					//inviteFriendsLayout.setVisibility(View.GONE);
				}
			

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}

			}

			if(!isFacebookPresent){
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}


		}

		/** Custom share dialog */
		dialogShare = new Dialog(AboutUs.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));


		/*txtFaq.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//showDialogTab();
			}
		});*/
		
		txtFaq.setOnClickListener(this);


		txtRateApp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EventTracker.logEvent(getResources().getString(R.string.appInfo_RateApp), true);
				final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
				try {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
				} catch (android.content.ActivityNotFoundException anfe) {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
				}
			}
		});


		txtClearCahe.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				DeleteCache();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishTo();
		return true;
	}

	
	private boolean checkIfFbPresent() {

		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");
		
		final PackageManager pm = getPackageManager();
		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);
		for (ResolveInfo rInfo : list) {
			Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
			String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

			if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
				isFacebookPresent = true;
			}
		}
		
		return isFacebookPresent;
	}
	

	void finishTo()
	{


		Intent intent = new Intent();
		setResult(8, intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		try {
			//pBarFb.setVisibility(View.GONE);
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try
		{

			if(v.getId()==pref_ContactMail.getId())
			{
				registerEvent(getResources().getString(R.string.appInfo_contactus));
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",getResources().getString(R.string.contact_email), null));
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
				startActivity(Intent.createChooser(emailIntent, "Send email..."));
			}
			if(v.getId()==pref_PrivacyPolicy.getId())
			{
				
				Intent intent=new Intent(context,AboutWebView.class);
				intent.putExtra("url", "PrivacyPolicy");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);



			}
			
			if(v.getId()==txtFaq.getId()) {
				EventTracker.logEvent(getResources().getString(R.string.appInfo_FAQ), true);
				Intent intent=new Intent(context,AboutWebView.class);
				intent.putExtra("url", "Faq");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			
			if(v.getId()==pref_Terms.getId())
			{
				registerEvent(getResources().getString(R.string.appInfo_TandC));
				Intent intent=new Intent(context,AboutWebView.class);
				intent.putExtra("url", "Terms");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
			if(v.getId()==text_store_manager.getId()){
				if(checkManager.isChecked()){
					checkManager.setChecked(false);
					//ConfigFile.setApplicationFor(ConfigFile.ForCustomer);
					session.setApplicationFor(ConfigFile.ForCustomer);

				}else{
					registerEvent(getResources().getString(R.string.appInfo_storeManager));
					checkManager.setChecked(true);
					//ConfigFile.setApplicationFor(ConfigFile.ForMerChant);
					session.setApplicationFor(ConfigFile.ForMerChant);
				}
			}

			if(v.getId()==checkManager.getId()){
				/*if(checkManager.isChecked()){
					checkManager.setChecked(false);
					ConfigFile.setApplicationFor(ConfigFile.ForCustomer);
					session.setApplicationFor(ConfigFile.ForCustomer);



				}else{
					checkManager.setChecked(true);
					ConfigFile.setApplicationFor(ConfigFile.ForMerChant);
					session.setApplicationFor(ConfigFile.ForMerChant);

				}*/
			}
			if(v.getId()==txtPoweredBy.getId()){
				registerEvent(getResources().getString(R.string.appInfo_poweredBy_c));
				Intent intent = new Intent(context, Twitter.class);
				intent.putExtra("ActivityTitle", "Phonethics");
				intent.putExtra("url", "http://phonethics.in/");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.fade_out);
			}
			
			
			if(v.getId()==txtInviteFriends.getId()){
				Toast.makeText(context, "Inviting Friends", 0).show();
				FacebookOperations.sendAppReuest(mContext);
			}
			
			if(v.getId()==text_feedback.getId()){
				EventTracker.logEvent(getResources().getString(R.string.appInfo_feedback), true);
				
				Intent intent = new Intent(mContext, Feedback.class);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.fade_out);
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	void showDialogTab(){

		dialogShare.show();

		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {


				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

					FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(AboutUs.this)
					.setLink("http://inorbitapp.in/download")
					.setDescription(shareText)
					.setName("Inorbit")
					.build();
					uiHelper.trackPendingDialogCall(shareDialog.present());
					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}

				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivity(i);
				}

				dismissDialog();

			}
		});

	}

	void dismissDialog(){
		dialogShare.dismiss();
	}

	void DeleteCache()
	{

		DecimalFormat df = new DecimalFormat("#.##");



		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Inorbit");
		alertDialogBuilder3
		.setMessage("Delete existing cache" + "(File size " + df.format(total_length) + "MB)" + "?")
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Clear",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				total_length = 0;

				//clear.setText("Clear (File size " + total_length + "MB)");

				//			imgLoader.clearFileCache();

				if(file1.exists())
				{
					DeleteRecursive(file1);
				}

				if(file2.exists())
				{
					DeleteRecursive(file2);
				}

				if(file3.exists())
				{
					DeleteRecursive(file3);
				}

				if(file4.exists())
				{
					DeleteRecursive(file4);
				}
				//
				//				if(file5.exists())
				//				{
				//					DeleteRecursive(file5);
				//				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}


	public static double getFolderSize(File folderPath) {

		double totalSize = 0;

		if (folderPath == null) {
			return 0;
		}

		if (!folderPath.isDirectory()) {
			return 0;
		}

		File[] files = folderPath.listFiles();
		if(files != null){
			for (File file : files) {
				if (file.isFile()) {
					totalSize += file.length();
				} else if (file.isDirectory()) {
					totalSize += file.length();
					totalSize += getFolderSize(file);
				}
			}
		}
		return totalSize;
	}

	void DeleteRecursive(File file) {
		/*if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
			{

				Log.i("Delete:", "File "+child.getAbsolutePath());
				DeleteRecursive(child);
			}

		fileOrDirectory.delete();*/

		if(file.isDirectory()){

			//directory is empty, then delete it
			if(file.list().length==0){

				file.delete();
				System.out.println("Directory is deleted : " 
						+ file.getAbsolutePath());

			}else{

				//list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					//construct the file structure
					File fileDelete = new File(file, temp);

					//recursive delete
					DeleteRecursive(fileDelete);
				}

				//check the directory again, if empty then delete it
				if(file.list().length==0){
					file.delete();
					System.out.println("Directory is deleted : " 
							+ file.getAbsolutePath());
				}
			}

		}else{
			//if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}

	}



	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		try{
			EventTracker.endFlurrySession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onStop();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		try{
			EventTracker.endLocalyticsSession(getApplicationContext());

		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onPause();


	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			//FireEvents.upload();
			EventTracker.startLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	/* on start */
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}



	void registerEvent(String eventName){
		try{
			EventTracker.logEvent(eventName, false);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void registerEventWithParam(String shareApp){
		Map<String, String> param = new HashMap<String, String>();
		if(shareApp!=null && !shareApp.equalsIgnoreCase("")){
			param.put("ShareWith", shareApp);
			EventTracker.logEvent(getResources().getString(R.string.appInfo_sharethisapp), param);
		}

		try{

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}



}
package com.phonethics.inorbit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.webkit.JsPromptResult;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.model.RequestTags;

public class MyClass  {

	HashMap<String, String> headers = new HashMap<String, String>();
	private ResultReceiver resultReceiver = null;
	private static String GALLERY_URL = "", MALLS_URL="",URL="";
	Context context;
	ParseData parseData;
	String TAG;
	int TimeOut = 30000;
	public String ID="";
	final String serverErrorMsg = "Something went wrong. Please try after some time!";
	final String connectionTimeoutMsg = "Network Time Out.!";
	boolean chache = false;
	
	public MyClass(Context context){
		this.context= context; 
		parseData = new ParseData(context);

	}

	public MyClass(Context context, String Tag){
		this.context= context; 
		parseData = new ParseData(context);
		this.TAG = Tag;
	}


	public void makeGetRequest(final String Tag,String ID,final HashMap<String, String> headers){

		if(ID.equalsIgnoreCase("-1")){

		}else{		
			this.ID = ID;
		}


		URL = getUrlFromTag(Tag);
		InorbitLog.d(Tag + " URL "+URL.toString());

		VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				
				// TODO Auto-generated method stub
				parseData = new ParseData(context);
				ConfigFile.isVolleyError = false;
				
				
				InorbitLog.d(Tag + " Response "+response.toString());
				
				if(Tag.equalsIgnoreCase(RequestTags.TagGetGallery)){
					parseData.parseGalleryResponse(response,Tag);
				}else if(Tag.equalsIgnoreCase(RequestTags.TagGetMalls)){

					try{
						String success = response.getString("success");
						if(success.equalsIgnoreCase("true")){
							JSONObject jObj = response.getJSONObject("data");
							parseData.parseMallInfo(jObj.getJSONArray("areas"),Tag);
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}else if(Tag.equalsIgnoreCase(RequestTags.TagGetStoreGallery)){
					try{
						parseData.parseStoreGalleryResponse(response,Tag);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.TagGetSpecialPost)){
					parseData.parseSpecialPostResponse(response, Tag);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_LoadStoreGallery)){
					parseData.parseInorbitGalleryResponse(response, Tag);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_SpecialDates)){
					parseData.parseSpecialDateResponse(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_CustomerProfile)){
					parseData.parseCustomerProfile(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_GetIntrestedAreas)){
					parseData.parseIntresrtedMallsResponse(Tag, response);
				}
			}
		};




		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				try{
					InorbitLog.d("Error "+Tag+" "+error.toString());


					ConfigFile.isVolleyError = true;
					Intent intent = new Intent();
					intent.putExtra("SUCCESS", "false");
					intent.putExtra("MESSAGE", getErrorMessage(error));
					intent.putExtra("CODE", getErrorCode(error));
					intent.putExtra("volleyError", true);
					intent.putExtra("apiUrl", getUrlFromTag(Tag));
					intent.putExtra("errorMessage", error.getMessage());
					intent.setAction(Tag);
					context.sendBroadcast(intent);


					//registerEroor();

				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};





		JsonObjectRequest req = new JsonObjectRequest(Method.GET,URL, null, listener, errorListener){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {

				// TODO Auto-generated method stub
				return headers;
			}

		};
		req.setRetryPolicy(new DefaultRetryPolicy(TimeOut, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		req.setShouldCache(chache);
		InorbitApp.getInstance().addToRequestQueue(req, Tag);

	}


	public void getStoreRequest(final String Tag,List<NameValuePair> nameValuePairs,final HashMap<String, String> headers_1){

		URL  = getUrlFromTag(Tag);
		
		if(nameValuePairs!=null){
			String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
			URL	+= paramString;
		}
		InorbitLog.d(Tag + " Url >> "+URL );

		
		VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>(){

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				ConfigFile.isVolleyError = false;
				//parseData = new ParseData(context);
				InorbitLog.d(Tag + " Resposne "+response.toString());
				if(Tag.equalsIgnoreCase(RequestTags.TagSearchStores)){
					try{
						parseData.parseStoreResponse(response, Tag);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_c)){
					try{
						parseData.parseParticularStoreDetails(response, Tag);
					}catch(Exception ex){
						ex.printStackTrace();
					} 
				}else if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_d)){
					try{
						//parseData.parseParticularStoreDetails(response, Tag);
						parseData.parseAndStoreiInDb(Tag, response);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.TagPlaceChooser)){
					try{
						parseData.parsePlaceChooserResponse(Tag, response);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.TagViewBroadcast)){
					try{
						parseData.parseBroadcastResponse(Tag, response);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.TagViewBroadcast_c)){
					try{
						parseData.parseBroadcastResponseForUser(Tag, response);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TagViewBroadcast_mall)){
					try{
						parseData.parseBroadcastResponseOfMall(Tag, response);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_m)){
					try{
						parseData.parseParticularStoreDetails(response, Tag);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_CategoryInfo)){
					try{
						parseData.parsecategoreyResponse(Tag, response);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_CustomerProfile)){
					parseData.parseCustomerProfile(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_DeletePost)){
					try{
						//parseData.parsecategoreyResponse(Tag, response);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UnapprovedBroadcast)){
					parseData.pasrseUnApprovedResposne(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.TagGetSpecialPost)){
					parseData.parseSpecialPostResponse(response, Tag);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_GetMallEvents)){
					//Log.d(Tag, Tag + " -- "+response.toString());
					parseData.parseMallEventsResponse(Tag, response);

				} else if (Tag.equalsIgnoreCase(RequestTags.Tag_GetDataVersion)) {
					parseData.parseDataVersionResponse(Tag, response);
				}else if (Tag.equalsIgnoreCase(RequestTags.Tag_GetFavActiveUserCount)) {
					parseData.parseFavCountResponse(Tag,response);
				} else if(Tag.equalsIgnoreCase(RequestTags.Tag_GetMontlyOccasionCount)) {
					parseData.parseMonthlySpecialCountResponse(Tag,response);
				} else if(Tag.equalsIgnoreCase(RequestTags.Tag_UserFavouriteStores)){
					try{
						parseData.parseStoreResponse(response, Tag);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				} else if(Tag.equalsIgnoreCase(RequestTags.Tag_GetOfferDetail)){
					parseData.parseOfferDetailResponse(response,Tag);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TAG_GET_MALL_CONVERSATIONS)){
					parseData.parseAllMessagesForMall(response,Tag);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TAG_GET_CONVERSATION)){
					parseData.getAllConversationMessages(response,Tag);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TAG_USER_SPECIFIC_STORE_RATE)){
					parseData.getUserSpecificRating(response,Tag);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_GET_MENU_FOR_STORE)) {
					parseData.getMenuForStore(response, Tag);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_GET_MENU_FOR_MALL)) {
					parseData.getMenuForMall(response, Tag);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_UNAPPROVED_MENU_COUNT)) {
					parseData.getUnapprovedMenuCount(response, Tag);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_TIP_MALL_MANAGER)) {
					parseData.getMallMgrTip(Tag, response);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_TIP_GET_ALL)) {
					parseData.getAllTips(Tag, response);
				} else if (Tag.equalsIgnoreCase(RequestTags.AppVersionCheck)) {
					parseData.apiVersionCheck(Tag, response);
				} else if (Tag.equalsIgnoreCase(RequestTags.Back_Up_Push)) {
					parseData.backUpPush(Tag, response);
				}
			}

		};

		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				InorbitLog.d("Error "+Tag+" "+error.toString());
				ConfigFile.isVolleyError = true;
				try{


					Intent intent = new Intent();
					intent.putExtra("SUCCESS", "false");
					intent.putExtra("MESSAGE",  getErrorMessage(error));
					intent.putExtra("volleyError", true);
					intent.putExtra("CODE", getErrorCode(error));
					intent.putExtra("apiUrl", getUrlFromTag(Tag));
					intent.putExtra("errorMessage", error.getMessage());
					intent.setAction(Tag);
					context.sendBroadcast(intent);


				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};

		JsonObjectRequest req = new JsonObjectRequest(Method.GET,URL, null, listener, errorListener){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {

				// TODO Auto-generated method stub
				return headers_1;
			}

		};

		req.setShouldCache(chache);
		req.setRetryPolicy(new DefaultRetryPolicy(TimeOut, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

		InorbitApp.getInstance().addToRequestQueue(req, Tag);

	}

	void delete(final String Tag,List<NameValuePair> nameValuePairs,final HashMap<String, String> headers_1){
		URL  = getUrlFromTag(Tag);
		if(nameValuePairs!=null){
			String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
			URL	+= paramString;
		}
		InorbitLog.d(Tag + " Url >> "+URL );

		VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>(){

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				InorbitLog.d(Tag + " Resposne "+response.toString());
				if(Tag.equalsIgnoreCase(RequestTags.Tag_DeletePost)){
					try{
						parseData.parsePostDeleteResponse(Tag,response);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UnLikeThisOffer)){
					parseData.parseOfferLikeResponse(Tag, response);
				}
			}

		};

		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				InorbitLog.d("Error "+Tag+" "+error.toString());
				if(Tag.equalsIgnoreCase(RequestTags.TagSearchStores)){
					try{


					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}

		};

		JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE,URL, null, listener, errorListener){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {

				// TODO Auto-generated method stub
				return headers_1;
			}

		};
		req.setRetryPolicy(new DefaultRetryPolicy( 
				TimeOut, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

		InorbitApp.getInstance().addToRequestQueue(req, Tag);

	}

	void delete(final String Tag,List<NameValuePair> nameValuePairs,final HashMap<String, String> headers_1,JSONObject jObj){
		URL  = getUrlFromTag(Tag);
		if(nameValuePairs!=null){
			String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
			URL	+= paramString;
		}
		InorbitLog.d(Tag + " Url >> "+URL );

		VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>(){

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				InorbitLog.d(Tag + " Resposne "+response.toString());
				if(Tag.equalsIgnoreCase(RequestTags.Tag_DeletePost)){
					try{
						parseData.parsePostDeleteResponse(Tag,response);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UnLikePlace)){
					parseData.parseLikePlace(Tag,response);
				}
			}

		};

		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				InorbitLog.d("Error "+Tag+" "+error.toString());
				if(Tag.equalsIgnoreCase(RequestTags.TagSearchStores)){
					try{


					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}

		};

		JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE,URL, jObj, listener, errorListener){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {

				// TODO Auto-generated method stub
				return headers_1;
			}

		};
		req.setRetryPolicy(new DefaultRetryPolicy( 
				TimeOut, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

		InorbitApp.getInstance().addToRequestQueue(req, Tag);

	}
	
	public void postRequest(final String Tag, final HashMap<String, String> headers, JSONObject jsonObject){

		URL  = getUrlFromTag(Tag);
		InorbitLog.d(Tag+" ==XX== "+URL);
		//InorbitLog.d(Tag+" JSon "+jsonObject.toString());
		VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>(){
			
			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				ConfigFile.isVolleyError = false;
				InorbitLog.d(Tag+" "+response.toString());
				if(Tag.equalsIgnoreCase(RequestTags.TagLoginMerchant)){
					parseData.parseLogin(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.TagLoginCustomer)){
					parseData.parseLogin(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.TagCreateBroadcast)){
					parseData.parseNewBroadcastResponse(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_AddStore)){
					parseData.parseAddStoreResponse(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateCustomerProfile)){
					parseData.parseUpdateCustomerProfileResponse(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateIntrestedMalls)){
					parseData.parseMallsAddResponse(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateActiveMall)){
					
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_ApproveBroadcast)){
					parseData.parseApprovePost(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_LikePlace)){
					parseData.parseLikePlace(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_RegisterUser)){
					parseData.parseRegisterUser(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_merchantregister)){
					parseData.parseRegisterUser(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_SpecialOffer)){
					//parseData.parseLikePlace(Tag,response);
					parseData.parseNewBroadcastResponse(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_LikeThisOffer)){
					parseData.parseOfferLikeResponse(Tag,response);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.Tag_ShareThisOffer)){
					parseData.parseOfferLikeResponse(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateOffer)){
					parseData.parseNewBroadcastResponse(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_Feedback)){
					parseData.parseFeedbackResponse(Tag, response);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TAG_CREATE_NEW_MSG)){
					parseData.parseCreateMsg(Tag, response);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TAG_TOTAL_COUNT)){
					parseData.parseTotalCount(Tag, response);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TAG_GET_UDM_ID_CUSTOMER)){
					parseData.parseUdmId(Tag, response);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TAG_GET_UDM_ID_MERCHANT)){
					parseData.parseUdmId(Tag, response);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TAG_AVG_STORE_RATING)) {
					parseData.parseAvgRating(Tag, response);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_UPLOAD_MENU)) {
					parseData.parseUploadMenuResponse(Tag, response);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TAG_DELETE_MENU_IMAGES)){
					parseData.deleteMenuImages(Tag,response);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_CHANGE_MENU_ORDER)) {
					parseData.updateMenuOrder(response, Tag);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_TIP_STORE_MANAGER)) {
					parseData.postStoreMgrTips(Tag, response);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_TIP_ADD_TIP)) {
					parseData.parseAddTip(Tag, response);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_UPLOAD_PHOTO_GALLERY)) {
					parseData.uploadGalleryStore(Tag, response);
				} else if (Tag.equalsIgnoreCase(RequestTags.GCMNotificationClickedEvent)) {
					parseData.gcmNotificationReceiver(Tag, response);
				} else if (Tag.equalsIgnoreCase(RequestTags.AnalyticsRequest)) {
					parseData.activityTrackingResponse(Tag, response);
				}
			}
		};


		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				InorbitLog.d("Error "+Tag+" "+error.toString());
				//if(Tag.equalsIgnoreCase(RequestTags.TagSearchStores)){
				try{

					Intent intent = new Intent();
					intent.putExtra("SUCCESS", "false");
					intent.putExtra("MESSAGE",  getErrorMessage(error));
					intent.putExtra("CODE", getErrorCode(error));
					intent.putExtra("volleyError", true);
					intent.putExtra("apiUrl", getUrlFromTag(Tag));
					intent.putExtra("errorMessage", error.getMessage());
					intent.setAction(Tag);
					context.sendBroadcast(intent);

				}catch(Exception ex){
					ex.printStackTrace();
				}
				//}
			}

		};

		JsonObjectRequest req = new JsonObjectRequest(Method.POST,URL, jsonObject, listener, errorListener){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}

		};
		req.setRetryPolicy(new DefaultRetryPolicy(
				TimeOut, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		req.setShouldCache(chache);
		InorbitApp.getInstance().addToRequestQueue(req, Tag);
	}


	public void postRequest(final String Tag,List<NameValuePair> nameValuePairs, final HashMap<String, String> headers, JSONObject jsonObject){

		URL  = getUrlFromTag(Tag);
		if(nameValuePairs!=null){
			String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
			URL	+= paramString;
		}
		
		if(jsonObject==null){
			jsonObject = null;
		}
		InorbitLog.d(Tag+" ==XX== "+URL);
		//InorbitLog.d(Tag+" "+jsonObject.toString());
		VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>(){

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				ConfigFile.isVolleyError = false;
				InorbitLog.d(Tag+" "+response.toString());
				if(Tag.equalsIgnoreCase(RequestTags.TagLoginMerchant)){
					parseData.parseLogin(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.TagLoginCustomer)){
					parseData.parseLogin(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.TagCreateBroadcast)){
					parseData.parseNewBroadcastResponse(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_AddStore)){
					parseData.parseAddStoreResponse(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateCustomerProfile)){
					parseData.parseUpdateCustomerProfileResponse(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateIntrestedMalls)){
					parseData.parseMallsAddResponse(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateActiveMall)){

				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_ApproveBroadcast)){
					parseData.parseApprovePost(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_LikePlace)){
					parseData.parseLikePlace(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UnLikePlace)){
					parseData.parseLikePlace(Tag,response);
				}else if(Tag.equalsIgnoreCase(RequestTags.Tag_SpecialOffer)){
					//parseData.parseLikePlace(Tag,response);
					parseData.parseNewBroadcastResponse(Tag, response);
				}else if(Tag.equalsIgnoreCase(RequestTags.TAG_UPDATE_STATUS)){
					//parseData.parseLikePlace(Tag,response);
					parseData.parseUpdateStatusResponse(Tag, response);
				}
				else if(Tag.equalsIgnoreCase(RequestTags.TAG_RATE_STORE)){
					//parseData.parseLikePlace(Tag,response);
					parseData.parseRatingResponse(Tag, response);
				} else if (Tag.equalsIgnoreCase(RequestTags.TAG_TIP_UPDATE_STATUS)) {
					parseData.parseTipStatusChangeResponse(Tag, response);
				}
			}

		};


		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				InorbitLog.d("Error "+Tag+" "+error.toString());
				//if(Tag.equalsIgnoreCase(RequestTags.TagSearchStores)){
				try{

					Intent intent = new Intent();
					intent.putExtra("SUCCESS", "false");
					intent.putExtra("MESSAGE",  getErrorMessage(error));
					intent.putExtra("CODE", getErrorCode(error));
					intent.putExtra("volleyError", true);
					intent.putExtra("apiUrl", getUrlFromTag(Tag));
					intent.putExtra("errorMessage", error.getMessage());
					intent.setAction(Tag);
					context.sendBroadcast(intent);

				}catch(Exception ex){
					ex.printStackTrace();
				}
				//}
			}

		};

		JsonObjectRequest req = new JsonObjectRequest(Method.POST,URL, jsonObject, listener, errorListener){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}

		};
		req.setRetryPolicy(new DefaultRetryPolicy(
				TimeOut, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		req.setShouldCache(chache);
		InorbitApp.getInstance().addToRequestQueue(req, Tag);
	}

	void validateUser(final String Tag,JSONObject jSonObject,final HashMap<String, String> headers){
		URL  = getUrlFromTag(Tag);
		InorbitLog.d(Tag+" "+jSonObject.toString()+" -- URL-- "+URL);
		VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>(){

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				InorbitLog.d(Tag+" "+response.toString());
				if(Tag.equalsIgnoreCase(RequestTags.Tag_ValidateMerchant)){
					parseData.parseValidateUser(Tag,response);
				}

			}

		};

		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				InorbitLog.d("Error "+Tag+" "+error.toString());

				try{
					Intent intent = new Intent();
					intent.putExtra("SUCCESS", "false");
					intent.putExtra("MESSAGE",  getErrorMessage(error));
					intent.putExtra("CODE", getErrorCode(error));
					intent.putExtra("volleyError", true);
					intent.putExtra("errorMessage", error.getMessage());
					intent.putExtra("apiUrl", getUrlFromTag(Tag));
					intent.setAction(Tag);
					context.sendBroadcast(intent);

				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};

		JsonObjectRequest req = new JsonObjectRequest(Method.POST,URL, jSonObject, listener, errorListener){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}

		};
		req.setRetryPolicy(new DefaultRetryPolicy(
				TimeOut, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

		InorbitApp.getInstance().addToRequestQueue(req, Tag);

	}


	void putRequest(final String Tag, final HashMap<String, String> headers, JSONObject jsonObject){
		URL  = getUrlFromTag(Tag);
		InorbitLog.d(Tag+" ==XX== "+URL);
		InorbitLog.d(Tag+" "+jsonObject.toString());
		VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>(){

			@Override
			public void onResponse(JSONObject response) {
				if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateOffer)){
					parseData.parseNewBroadcastResponse(Tag, response);
				}
			}
		};
		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				InorbitLog.d("Error "+Tag+" "+error.toString());
				//if(Tag.equalsIgnoreCase(RequestTags.TagSearchStores)){
				try{

					Intent intent = new Intent();
					intent.putExtra("SUCCESS", "false");
					intent.putExtra("MESSAGE",  getErrorMessage(error));
					intent.putExtra("CODE", getErrorCode(error));
					intent.putExtra("volleyError", true);
					intent.putExtra("apiUrl", getUrlFromTag(Tag));
					intent.putExtra("errorMessage", error.getMessage());
					intent.setAction(Tag);
					context.sendBroadcast(intent);

				}catch(Exception ex){
					ex.printStackTrace();
				}
				//}
			}

		};
		JsonObjectRequest req = new JsonObjectRequest(Method.PUT,URL, jsonObject, listener, errorListener){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}

		};
		req.setRetryPolicy(new DefaultRetryPolicy(
				TimeOut, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		req.setShouldCache(chache);
		InorbitApp.getInstance().addToRequestQueue(req, Tag);
	}

	
	void putRequest(final String Tag,List<NameValuePair> nameValuePairs, final HashMap<String, String> headers, JSONObject jsonObject){
		URL  = getUrlFromTag(Tag);
		InorbitLog.d(Tag+" ==XX== "+URL);
		
		if(nameValuePairs!=null){
			String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
			URL	+= paramString;
		}
		
		InorbitLog.d(Tag+" "+jsonObject.toString());
		VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>(){

			@Override
			public void onResponse(JSONObject response) {
				if (Tag.equalsIgnoreCase(RequestTags.TAG_UPDATE_MENU_IMAGE_STATUS)) {
					parseData.getMenuUpdateStatusResult(response, Tag);
				}
			}
		};
		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				InorbitLog.d("Error "+Tag+" "+error.toString());
				//if(Tag.equalsIgnoreCase(RequestTags.TagSearchStores)){
				try{

					Intent intent = new Intent();
					intent.putExtra("SUCCESS", "false");
					intent.putExtra("MESSAGE",  getErrorMessage(error));
					intent.putExtra("CODE", getErrorCode(error));
					intent.putExtra("volleyError", true);
					intent.putExtra("apiUrl", getUrlFromTag(Tag));
					intent.putExtra("errorMessage", error.getMessage());
					intent.setAction(Tag);
					context.sendBroadcast(intent);

				}catch(Exception ex){
					ex.printStackTrace();
				}
				//}
			}

		};
		JsonObjectRequest req = new JsonObjectRequest(Method.PUT,URL, jsonObject, listener, errorListener){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{
				
				// TODO Auto-generated method stub
				return headers;
			}

		};
		req.setRetryPolicy(new DefaultRetryPolicy(
				TimeOut, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		req.setShouldCache(chache);
		InorbitApp.getInstance().addToRequestQueue(req, Tag);
	}
	
	private String getUrlFromTag(String Tag){
		String URL = "";


		if(Tag.equalsIgnoreCase(RequestTags.TagGetGallery)){
			URL		=	context.getResources().getString(R.string.server_url)		+
					context.getResources().getString(R.string.storeapi)			+
					context.getResources().getString(R.string.store_gallery)	+
					ID;

		}else if(Tag.equalsIgnoreCase(RequestTags.TagGetMalls)){
			
			URL 	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.areas);

		}else if(Tag.equalsIgnoreCase(RequestTags.TagSearchStores)){

			URL = 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.searchapi);

		}else if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_c)){

			URL = 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.allStores)+"?";

		}else if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_d)){

			URL = 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.allStores)+"?";

		}else if(Tag.equalsIgnoreCase(RequestTags.TagGetStoreGallery)){

			URL = context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.store_gallery)+ID;

		}else if(Tag.equalsIgnoreCase(RequestTags.TagGetSpecialPost)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.getspecialpost);

		}else if(Tag.equalsIgnoreCase(RequestTags.TagLoginMerchant)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.merchant_api)+
					context.getResources().getString(R.string.login);

		}else if(Tag.equalsIgnoreCase(RequestTags.TagPlaceChooser)){
			URL		=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.allStores);

		}else if(Tag.equalsIgnoreCase(RequestTags.TagLoginCustomer)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.login);

		}else if(Tag.equalsIgnoreCase(RequestTags.TagCreateBroadcast)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+
					context.getResources().getString(R.string.add_broadcast);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateOffer)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+
					context.getResources().getString(R.string.update_broadcast);

		}else if(Tag.equalsIgnoreCase(RequestTags.TagViewBroadcast)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+
					context.getResources().getString(R.string.broadcasts);

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TagViewBroadcast_c)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+
					context.getResources().getString(R.string.broadcasts);

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TagViewBroadcast_mall)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+"/"+
					context.getResources().getString(R.string.searchapi);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_LoadStoreGallery)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.shoplocal_gellery);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_AddStore)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.add_store);

		}else if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_m)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.allStores)+"?";

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_CategoryInfo)){

			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getString(R.string.place_category);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_SpecialDates)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.dateCat);
		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_CustomerProfile)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.register)+"?";

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateCustomerProfile)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.register);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateActiveMall)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.register);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_GetIntrestedAreas)){
			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.interested_areas);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UpdateIntrestedMalls)){
			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.interested_areas);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_DeletePost)){
			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+
					context.getResources().getString(R.string.add_broadcast)+"?";

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_ValidateMerchant)){
			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.merchant_api)+
					context.getResources().getString(R.string.validate_login);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UnapprovedBroadcast)){
			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+
					context.getResources().getString(R.string.broadcasts_unapproved)+"?";

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_ApproveBroadcast)){
			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+
					context.getResources().getString(R.string.add_broadcast);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_LikePlace)){
			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.like);
		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UnLikePlace)){
			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.like)+"?";
		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_RegisterUser)){
			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.register);
		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_merchantregister)){
			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.merchant_api)+
					context.getResources().getString(R.string.merchant_register);
		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_GetMallEvents)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+
					context.getResources().getString(R.string.broadcasts);

		} else if(Tag.equalsIgnoreCase(RequestTags.Tag_GetDataVersion)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.data_version);

		} else if(Tag.equalsIgnoreCase(RequestTags.Tag_GetFavActiveUserCount)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.fav_activeCount);

		} else if(Tag.equalsIgnoreCase(RequestTags.Tag_GetMontlyOccasionCount)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.merchant_api)+
					context.getResources().getString(R.string.count_special_date);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UserFavouriteStores)){
			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.favourite_places)+"?";
		} else if(Tag.equalsIgnoreCase(RequestTags.Tag_SpecialOffer)) {
			URL	= context.getResources().getString(R.string.server_url)
					+context.getResources().getString(R.string.broadcast_api)+
					context.getResources().getString(R.string.special);
		} else if(Tag.equalsIgnoreCase(RequestTags.Tag_GetOfferDetail)){

			URL	= context.getResources().getString(R.string.server_url)
					+context.getResources().getString(R.string.broadcast_api)+
					context.getResources().getString(R.string.broadcasts);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_LikeThisOffer)){

			URL	= context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+"/"+
					context.getResources().getString(R.string.like);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_UnLikeThisOffer)){

			URL	= context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+"/"+
					context.getResources().getString(R.string.like)+"?";

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_ShareThisOffer)){

			URL	= context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.broadcast_api)+"/"
					+context.getResources().getString(R.string.share);

		}else if(Tag.equalsIgnoreCase(RequestTags.Tag_LoadCategories)){

			URL	= context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.place_category);

		} else if(Tag.equalsIgnoreCase(RequestTags.Tag_Feedback)){

			URL	= context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.feedback_api)+
					context.getResources().getString(R.string.feedback_user);

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TAG_CREATE_NEW_MSG)){

			URL	= context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.inbox_api)+
					context.getResources().getString(R.string.sent_msg);

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TAG_GET_MALL_CONVERSATIONS)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.inbox_api)+
					context.getResources().getString(R.string.store_count_msg)+"?";

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TAG_GET_CONVERSATION)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.inbox_api)+
					context.getResources().getString(R.string.read_msg)+"?";

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TAG_UPDATE_STATUS)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.inbox_api)+
					context.getResources().getString(R.string.update_msg_status)+"?";

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TAG_TOTAL_COUNT)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.inbox_api)+
					context.getResources().getString(R.string.count_msg);

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TAG_RATE_STORE)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.rating_store);

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TAG_GET_UDM_ID_CUSTOMER)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.device_registration);

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TAG_GET_UDM_ID_MERCHANT)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.user_api)+
					context.getResources().getString(R.string.device_registration);

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TAG_AVG_STORE_RATING)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.count_rate);

		}
		else if(Tag.equalsIgnoreCase(RequestTags.TAG_USER_SPECIFIC_STORE_RATE)){

			URL	= 	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.storeapi)+
					context.getResources().getString(R.string.cheak_israted)+"?";

		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_UPLOAD_MENU)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.menu_upload);
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_GET_MENU_FOR_STORE)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.get_menu_for_store) + "?";
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_GET_MENU_FOR_MALL)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.get_menustores_for_mall) + "?";
		}
		else if (Tag.equalsIgnoreCase(RequestTags.TAG_DELETE_MENU_IMAGES)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.remove_image);
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_UPDATE_MENU_IMAGE_STATUS)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.update_menu_status) + "?";
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_CHANGE_MENU_ORDER)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.change_menu_order);
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_UNAPPROVED_MENU_COUNT)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.get_unapproved_menu_count) + "?";
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_TIP_STORE_MANAGER)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.store_mgr_tip);
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_TIP_MALL_MANAGER)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.mall_mgr_tip) + "?";
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_TIP_GET_ALL)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.tips) + "?";
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_TIP_UPDATE_STATUS)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.update_tip_status);
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_TIP_ADD_TIP)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.add_tip);
		} else if (Tag.equalsIgnoreCase(RequestTags.TAG_UPLOAD_PHOTO_GALLERY)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.storeapi) +
					context.getResources().getString(R.string.post_gallery);
		} else if (Tag.equalsIgnoreCase(RequestTags.GCMNotificationClickedEvent)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.user_api) +
					context.getResources().getString(R.string.push_open);
		} else if (Tag.equalsIgnoreCase(RequestTags.AppVersionCheck)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.user_api) +
					context.getResources().getString(R.string.app_version_check) + "?";
		} else if (Tag.equalsIgnoreCase(RequestTags.Back_Up_Push)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.user_api) +
					context.getResources().getString(R.string.backup_push);
		} else if (Tag.equalsIgnoreCase(RequestTags.AnalyticsRequest)) {
			URL = context.getResources().getString(R.string.server_url) +
					context.getResources().getString(R.string.user_api) +
					context.getResources().getString(R.string.activity_tracking);
		} 

		return URL;

	}
	
	
	
	private String getErrorMessage(VolleyError error){

		if(error instanceof NetworkError){
			return context.getResources().getString(R.string.noNetwork);
		}else if(error instanceof AuthFailureError){
			return context.getResources().getString(R.string.authFail);
		}else if(error instanceof ServerError){
			return context.getResources().getString(R.string.serverError);
		}else if(error instanceof NoConnectionError){
			return context.getResources().getString(R.string.noConnection);
		}else if(error instanceof TimeoutError){
			return context.getResources().getString(R.string.connectionTimeOut);
		}else{
			return "Something went wrong!";
		}
	}

	public int getErrorCode(VolleyError error){

		if(error instanceof NetworkError){
			return ConfigFile.NetworkError;
		}else if(error instanceof AuthFailureError){
			return ConfigFile.AuthFailure;
		}else if(error instanceof ServerError ){
			return ConfigFile.ServerError;
		}else if(error instanceof NoConnectionError){
			return ConfigFile.NointernetConnection;
		}else if(error instanceof TimeoutError){
			return ConfigFile.TimeOutError;
		}else if(error instanceof ParseError){

			return ConfigFile.ParseError;
		}else{ 
			return -1;
		}
	}




}








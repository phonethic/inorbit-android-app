package com.phonethics.inorbit;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.eventtracker.EventTracker;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class GateToTalk extends SherlockActivity {

	Button TalkCalender;
	Button TalkFavouriteUser;
	Button TalkMallWise;
	Button TalkCustomerActiveMall;

	ActionBar actionbar;
	Context context;
	
	
	String STORE_ID;
	String mall_id;
	
	SessionManager session;
	String active_mall_id = "";
	String active_mall_place_parent = "";
	
	DBUtil dbUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gate_to_talk);

		context=this;
		
		dbUtil = new DBUtil(context);
		//network=new NetworkCheck(context);

		actionbar=getSupportActionBar();
		actionbar.setTitle("Shake & Win");
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();
		
//		session = new SessionManager(context);
//		
//		active_mall_id = session.getActiveMallId();
		
		try {
		

			active_mall_place_parent = dbUtil.getPlaceId("0");
			active_mall_id = dbUtil.getMallIdByPlaceParent(active_mall_place_parent);
			
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
		}
		
		
//		Toast.makeText(context, " " + active_mall_id, 0).show();

		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			STORE_ID=b.getString("STORE_ID");
			mall_id = b.getString("MALL_ID");
			
			Log.d("SOTREID","inorbit MALLID "  + mall_id);
			
			
		}else{
			Log.d("SOTREID","inorbit bundle null");
		}

		TalkFavouriteUser = (Button) findViewById(R.id.TalkFavouriteUser);
		TalkCalender = (Button) findViewById(R.id.TalkCalender);
		TalkMallWise = (Button) findViewById(R.id.TalkMallWise);
		TalkCustomerActiveMall = (Button) findViewById(R.id.TalkCustomerActiveMall);
		
		//TalkFavouriteUser.setText("Generate the Special Offer for the customer who have favourited any store or Inorbit Mall Malad");

		TalkCalender.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				EventTracker.logEvent(getResources().getString(R.string.mShake2Win_Calendar), false);
				 Intent intent=new Intent(context,SpecialEventsDates.class);
				 intent.putExtra("SureShop",true);
//				 intent.putExtra("STORE_NAME", STORE_NAME.get(position));
//				 intent.putExtra("STORE_LOGO", PHOTO.get(position));
				 intent.putExtra("STORE_ID", STORE_ID);
				 intent.putExtra("mall_id",mall_id);
				 startActivity(intent);
				 overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				 finish();

			}
		});

		TalkFavouriteUser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				EventTracker.logEvent(getResources().getString(R.string.mShake2Win_StoreFavourite), false);
				Intent intent = new Intent(context, CreateBroadCast.class);
				intent.putExtra("isFavouriteStore",true);
				intent.putExtra("place_id",STORE_ID);
				intent.putExtra("mall_id",mall_id);
				context.startActivity(intent);
				finish();
			}
		});
		
		TalkMallWise.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				EventTracker.logEvent(getResources().getString(R.string.mShake2Win_SpecificMall), false);
				Intent intent = new Intent(context, CreateBroadCast.class);
				intent.putExtra("isMallWise",true);
				intent.putExtra("mall_id",mall_id);
				intent.putExtra("place_id",STORE_ID);
				context.startActivity(intent);
				finish();
			}
		});
		
		
		TalkCustomerActiveMall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EventTracker.logEvent(getResources().getString(R.string.mShake2Win_ActiveMall), false);
				Intent intent = new Intent(context, CreateBroadCast.class);
				intent.putExtra("isActiveMall",true);
				intent.putExtra("activeMallId",active_mall_id);
				intent.putExtra("place_id",STORE_ID);
				context.startActivity(intent);
				finish();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("View Post");
		extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		if(item.getTitle().toString().equalsIgnoreCase("Shake & Win"))
		{
			finishto();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		else if(item.getTitle().toString().equalsIgnoreCase("View Post")){
			
			 //Toast.makeText(context, "clicked", 0).show();
			EventTracker.logEvent(getResources().getString(R.string.mShake2Win_SpecialPostView), false);
			 Intent intent=new Intent(context,ViewSpecialPosts.class);
			 intent.putExtra("SureShop",1);
//			 intent.putExtra("STORE_NAME", STORE_NAME.get(position));
//			 intent.putExtra("STORE_LOGO", PHOTO.get(position));
			 intent.putExtra("STORE_ID", STORE_ID);
			 startActivity(intent);
			 overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
//			 finish();
		}
		return true;
	}

	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		finishto();
	}

	
	void finishto()
	{
		//Intent intent=new Intent(context, MerchantsHomeGrid.class);
		Intent intent=new Intent(context,MerchantHome.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

		this.finish();
	}
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
	}

	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		EventTracker.startLocalyticsSession(getApplicationContext());
		super.onResume();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}
	
	

}

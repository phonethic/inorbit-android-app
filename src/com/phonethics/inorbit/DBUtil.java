package com.phonethics.inorbit;

import java.util.ArrayList;

import com.phonethics.model.ParticularStoreInfo;
import com.phonethics.model.StoreInfo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBUtil {
	private final 			Context 		ourContext;
	private static final 	String 			DATABASE_NAME = "Inorbit.db";
	private static final 	int  			DATABASE_VERSOIN=15;
	private 				DbHelper 		dbHelper;
	private 				SQLiteDatabase 	sqlDataBase;

	/*
	 * Table Name
	 */
	private static final 	String 			MERCHANT_TABLE	=	"MERCHANT";
	private static final 	String 			CATEGORY_TABLE	=	"CATEGORY";
	private static final 	String 			TABLE_BUSINESS			 ="BUSINESS";
	private static final 	String 			TABLE_BUSINESS_STORE	 ="BUSINESS_STORE";
	private static final    String 			TABLE_TEXT_SEARCH		 ="TEXT_SEARCH";
	private static final    String 			TABLE_GEOTEXT_SEARCH	 ="GEOTEXT_SEARCH";
	private static final    String 			TABLE_GEOTEXT_FAV_SEARCH  ="GEOTEXT_FAV_SEARCH";
	private static final 	String 			TABLE_ANALYTICS			  = "TABLE_ANALYTICS";
	


	/*
	 * STORE FIELD'S
	 */
	public static final String		KEY_STORE_ID								="id";
	public static final String		KEY_STORE_BUSINESS_ID						="business_id";
	public static final String		KEY_STORE_NAME								="store_name";
	public static final String		KEY_STORE_BUILDING							="building";
	public static final String		KEY_STORE_STREET 						    ="street";
	public static final String		KEY_STORE_LANDMARK							="landmark";
	public static final String		KEY_STORE_AREA								="area";
	public static final String		KEY_STORE_PINCODE							="pincode";
	public static final String		KEY_STORE_CITY								="city";
	public static final String		KEY_STORE_STATE								="state";
	public static final String		KEY_STORE_COUNTRY							="country";
	public static final String		KEY_STORE_GEOLOCATION						="geolocation";
	public static final String		KEY_STORE_TEL_NO1							="tel_no1";
	public static final String		KEY_STORE_TEL_NO2							="tel_no2";
	public static final String		KEY_STORE_MOB_NO1							="mob_no1";
	public static final String		KEY_STORE_MOB_NO2							="mob_no2";
	public static final String		KEY_STORE_FAXNO1							="fax_no1";
	public static final String		KEY_STORE_FAXNO2							="fax_no2";
	public static final String		KEY_STORE_TOLL_FREE_NO1						="toll_free_no1";
	public static final String		KEY_STORE_TOLL_FREE_NO2						="toll_free_no2";
	public static final String		KEY_STORE_PHOTO								="photo";

	/*
	 * GEO-LOCATION SEARCH FIELD'S
	 * 
	 */
	public static final String		KEY_GEO_SEARCH_STORE_NAME					="store_name";

	/*
	 * TEXT-GEO-LOCATION SEARCH FIELD'S
	 * 
	 */
	public static final String		KEY_SEARCH_AUTO_ID					="_id";
	public static final String		KEY_SEARCH_ID						="id";
	public static final String		KEY_SEARCH_PLACE_PARENT			="place_parent";
	public static final String		KEY_SEARCH_STORE_NAME				="store_name";
	public static final String		KEY_SEARCH_DESCRIPTION				="description";
	public static final String		KEY_SEARCH_BUILDING				="building";
	public static final String		KEY_SEARCH_STREET					="street";
	public static final String		KEY_SEARCH_LANDMARK				="landmakr";
	public static final String		KEY_SEARCH_AREA					="area";
	public static final String		KEY_SEARCH_CITY					="city";
	public static final String		KEY_SEARCH_MOB_NO1					="mob_no1";
	public static final String		KEY_SEARCH_PHOTO					="photo";
	public static final String		KEY_SEARCH_DISTANCE				="distance";
	public static final String 		KEY_SEARCH_TOTAL_PAGES				="total_page";
	public static final String 		KEY_SEARCH_TOTAL_RECORD			="total_record";
	public static final String 		KEY_SEARCH_CURRENT_PAGE			="current_page";
	public static final String 		KEY_TOTAL_LIKE						="total_like";
	public static final String 		KEY_SEARCH_FAVOURITED				="favourite";
	public static final String 		KEY_SEARCH_HAS_OFFER				="has_offer";
	public static final String 		KEY_SEARCH_OFFER_TITLE				="title";



	/*
	 * TEXT-CATGORY SEARCH FIELD'S
	 * 
	 */
	public static final String		KEY_CAT_TEXT_SEARCH_AUTO_ID					="_id";
	public static final String		KEY_CAT_TEXT_STORE_ID						="id";
	public static final String		KEY_CAT_TEXT_STORE_PLACE_PARENT				="place_parent";

	public static final String		KEY_CAT_TEXT_STORE_STORE_NAME				="name";
	public static final String		KEY_CAT_TEXT_STORE_DESCRIPTION				="description";
	public static final String		KEY_CAT_TEXT_STORE_BUILDING					="building";

	public static final String		KEY_CAT_TEXT_STORE_STREET					="street";
	public static final String		KEY_CAT_TEXT_STORE_LANDMARK					="landmark";
	public static final String		KEY_CAT_TEXT_STORE_AREA						="area";

	public static final String		KEY_CAT_TEXT_STORE_CITY						="city";
	public static final String		KEY_CAT_TEXT_STORE_MOB_NO1					="mob_no1";
	public static final String		KEY_CAT_TEXT_STORE_PHOTO					="image_url";

	public static final String 		KEY_CAT_TEXT_TOTAL_LIKE						="total_like";
	public static final String 		KEY_CAT_TEXT_STORE_HAS_OFFER				="has_offer";

	public static final String 		KEY_CAT_TEXT_STORE_OFFER_TITLE				="title";
	public static final String 		KEY_CAT_TEXT_CATEGORY_ID					="category_id";
	public static final String 		KEY_CAT_TEXT_STORE_TOTAL_PAGES				="total_page";

	public static final String 		KEY_CAT_TEXT_STORE_TOTAL_RECORD				="total_record";



	/*
	 * FAV-TEXT-GEO-LOCATION SEARCH FIELD'S
	 * 
	 */
	public static final String		KEY_FAV_AUTO_ID					="_id";
	public static final String		KEY_FAV_SEARCH_ID				="id";
	public static final String 		KEY_FAV_USER_ID					="user_id";
	public static final String		KEY_FAV_PLACE_PARENT			="place_parent";
	public static final String		KEY_FAV_STORE_NAME				="store_name";
	public static final String		KEY_FAV_DESCRIPTION				="description";
	public static final String		KEY_FAV_BUILDING				="building";
	public static final String		KEY_FAV_STREET					="street";
	public static final String		KEY_FAV_MALL_ID					="mall_id";
	public static final String		KEY_FAV_LANDMARK				="landmakr";
	public static final String		KEY_FAV_AREA					="area";
	public static final String		KEY_FAV_CITY					="city";
	public static final String		KEY_FAV_MOB_NO1					="mob_no1";
	public static final String 		KEY_FAV_MOB_NO2					= 	"mob_no2";
	public static final String 		KEY_FAV_MOB_NO3					= 	"mob_no3";
	public static final String 		KEY_FAV_LAND_NO1				= 	"landLine_no1";
	public static final String 		KEY_FAV_LAND_NO2				= 	"landLine_no2";
	public static final String 		KEY_FAV_LAND_NO3				= 	"landLine_no3";
	public static final String 		KEY_FAV_TOLL_FREE_NO1			=	"toll_free_no1";
	public static final String 		KEY_FAV_TOLL_FREE_NO2			=	"toll_free_no2";
	public static final String		KEY_FAV_PHOTO					="photo";
	public static final String		KEY_FAV_DISTANCE				="distance";
	public static final String 		KEY_FAV_TOTAL_PAGES			="total_page";
	public static final String 		KEY_FAV_TOTAL_RECORD			="total_record";
	public static final String 		KEY_FAV_CURRENT_PAGE			="current_page";
	public static final String 		KEY_FAV_TOTAL_LIKE				="total_like";
	public static final String 		KEY_FAV_FAVOURITED				="favourite";
	public static final String 		KEY_FAV_SEARCH_HAS_OFFER		="has_offer";
	public static final String 		KEY_FAV_SEARCH_OFFER_TITLE		="title";
	public static final String 		KEY_FAV_TOTAL_RATING			="total_rating";
	public static final String 		KEY_FAV_CATEGORY				= "category";





	public static final String	rowId 				= "ROW_ID";
	public static final String	id					= "ID";
	public static final String	businessId 			= "BUSINESS_ID";
	public static final String	store_name			= "STORE_NAME";
	public static final String 	building			= "BUILDING";
	public static final String	street				= "STREET";
	public static final String 	landmark			= "LAND_MARK";
	public static final String	area				= "AREA";
	public static final String 	pincode				= "PIN_CODE";
	public static final String	city				= "CITY";
	public static final String	state				= "STATE";
	public static final String	country				= "COUNTRY";
	public static final String	latitude			= "LATITUDE";
	public static final String	longitude			= "LONGITUDE";
	public static final String	tel_no1				= "TEL_NO1";
	public static final String	tel_no2				= "TEL_NO2";
	public static final String	mob_no1				= "MOB_NO1";
	public static final String	mob_no2				= "MOB_NO2";
	public static final String	fax_no1				= "FAX_NO1";
	public static final String	fax_no2				= "FAX_NO2";
	public static final String	toll_free_no1		= "TOLL_FREE_NO1";
	public static final String	toll_free_no2		= "TOLL_FREE_NO2";
	public static final String	photo				= "PHOTO";
	public static final String	fav					= "FAVOURITE";



	// fields for TABLE_AREAS
	public static final String key_area_auto_id	 	="_id";
	public static final String mall_id	 			= "ID";
	public static final String area_name	 		= "AREA_NAME";
	public static final String area_latitude	 	= "AREA_LATITUDE";
	public static final String area_longitude	 	= "AREA_LONGITUDE";	
	public static final String area_pincode	 		= "AREA_PINCODE";
	public static final String area_city	 		= "AREA_CITY";
	public static final String area_country	 		= "AREA_COUNTRY";
	public static final String area_country_code 	= "AREA_COUNTRY_CODE";
	public static final String area_iso_code	 	= "AREA_ISO_CODE";
	public static final String place_parent	 		= "AREA_PLACEID";
	public static final String area_locality	 	= "AREA_LOCALITY";
	public static final String area_state	 		= "AREA_STATE";
	public static final String fb_url	 			= "FACEBOOK_URL";
	private static final String active_area 		= "ACTIVE_AREA";
	private static final String TABLE_AREAS 		= "AREA_DESCRIPTION";


	public static final String KEY_PARENT_STORE_AUTO_ID 		="auto_id";
	public static final String KEY_PARENT_STORE_ID 				="id";
	public static final String KEY_PARENT_STORE_NAME			= "name";
	public static final String KEY_PARENT_STORE_PLACE_PARENT 	= "place_parent";
	public static final String KEY_PARENT_STORE_DESCRIPTION 	= "description";

	public static final String KEY_PARENT_STORE_BUILDING 		= "building";
	public static final String KEY_PARENT_STORE_STREET 			= "street";
	public static final String KEY_PARENT_STORE_LANDMARK 		= "landmark";

	public static final String KEY_PARENT_STORE_MALL_ID 		= "mall_id";
	public static final String KEY_PARENT_STORE_AREA 			= "area";
	public static final String KEY_PARENT_STORE_PINCODE 		= "pincode";

	public static final String KEY_PARENT_STORE_CITY 			= "city";
	public static final String KEY_PARENT_STORE_STATE 			= "state";
	public static final String KEY_PARENT_STORE_COUNTRY 		= "country";

	public static final String KEY_PARENT_STORE_LAT 			= "latitude";
	public static final String KEY_PARENT_STORE_LONG 			= "longitude";
	public static final String KEY_PARENT_STORE_TEL_NO_1 		= "tel_no_1";

	public static final String KEY_PARENT_STORE_TEL_NO_2		= "tel_no_2";
	public static final String KEY_PARENT_STORE_MOB_NO_1 		= "mob_no_1";
	public static final String KEY_PARENT_STORE_MOB_NO_2		= "mob_no_2";

	public static final String KEY_PARENT_STORE_FAX_NO_1 		= "fax_no_1";
	public static final String KEY_PARENT_STORE_FAX_NO_2 		= "fax_no_2";
	public static final String KEY_PARENT_STORE_TOLL_FREE_NO_1 	= "tol_no_1";

	public static final String KEY_PARENT_STORE_TOLL_FREE_NO_2 	= "tol_no_2";
	public static final String KEY_PARENT_STORE_IMAGE_URL 		= "image_url";
	public static final String KEY_PARENT_STORE_EMAIL 			= "email";

	public static final String KEY_PARENT_STORE_WEBSITE 		= "website";
	public static final String KEY_PARENT_STORE_FACEBOOK_URL 	= "facebook_url";
	public static final String KEY_PARENT_STORE_TWITTER_URL 	= "twitter_url";



	//Fields For Table PLACES
	private static final    String 			TABLE_PLACE_CHOOSER	 		="MY_PLACES";
	private static final    String 			TABLE_PARENT_STORE	 		="PARENT_STORE";

	public static final String KEY_PLACE_AUTO_ID="_id";
	public static final String KEY_SHOP_ID="STORE_ID";
	public static final String KEY_SHOP_PARENT="place_parent";
	public static final String KEY_SHOP_NAME="shop_name";

	public static final String KEY_SHOP_DESCRIPTION="shop_description";
	public static final String KEY_SHOP_BUILDING="building";

	public static final String KEY_SHOP_STREET="shop_street";
	public static final String KEY_SHOP_LANDMARK="shop_landmark";

	//public static final String KEY_SHOP_AREA="place_area";
	public static final String KEY_SHOP_MALL_ID="mall_id";
	public static final String KEY_SHOP_PINCODE="shop_pincode";
	public static final String KEY_SHOP_AREA="shop_area";
	public static final String KEY_SHOP_CITY="shop_city";
	public static final String KEY_SHOP_STATE="shop_state";

	public static final String KEY_SHOP_COUNTRY="shop_country";
	public static final String KEY_SHOP_LATITUDE="shop_latitude";
	public static final String KEY_SHOP_LONGITUDE="shop_longitude";



	public static final String KEY_SHOP_TELLNO1="SHOP_tellno1";
	public static final String KEY_SHOP_TELLNO2="place_tellno2";
	//public static final String KEY_PLACE_TELLNO3="place_tellno3";


	public static final String KEY_SHOP_MOBNO1="place_mobno1";
	public static final String KEY_SHOP_MOBNO2="place_mobno2";
	//public static final String KEY_PLACE_MOBNO3="place_mobno3";


	public static final String KEY_SHOP_image_url="place_image_url";
	public static final String KEY_SHOP_email="place_email";
	public static final String KEY_SHOP_website="place_website";

	public static final String KEY_SHOP_facebook="place_facebook";
	public static final String KEY_SHOP_twitter="place_twitter";
	public static final String KEY_SHOP_place_status="place_status";
	public static final String KEY_SHOP_place_published="place_published";
	public static final String KEY_SHOP_SUB_CATEGORY = "sub_category";


	/*public static final String KEY_Mall_Id = "mall_id";
	public static final String KEY_Cat_All = "cat_all";
	public static final String KEY_Cat_Shop = "cat_shop";
	public static final String KEY_Cat_Dine = "cat_dine";
	public static final String KEY_Cat_Entertainment = "cat_entertainment";
	public static final String KEY_Cat_Pamper = "cat_pamper";*/

	public static final String MENU_TABLE = "MENU";
	public static final String MENU_ID = "ID";
	public static final String MENU_PLACE_ID = "PLACE_ID";
	public static final String MENU_IMAGE_URL_KEYS = "MENU_URL_KEYS";

	//COLUMNS FOR ANALYTICS
	public static final String ANALYTICS_ID = "_ID";
	public static final String ANALYTICS_UDM_ID = "UDM_ID";
	public static final String ANALYTICS_DATE = "DATE";
	public static final String ANALYTICS_FLAG = "FLAG";
	public static final String ANALYTICS_ACTIVITY_ID 	= "ACTIVITY_ID";
	public static final String ANALYTICS_MALL_ID = "MALL_ID";
	public static final String ANALYTICS_SYNC_STATUS = "SYNC_STATUS";
	
	public DBUtil(Context c){
		ourContext=c;
	}


	private static class  DbHelper extends SQLiteOpenHelper{

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSOIN);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL("CREATE TABLE "+
					MERCHANT_TABLE +" ("+
					rowId		+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					id 			+" TEXT, "+
					businessId 	+" TEXT, "+
					store_name 	+" TEXT, "+
					building 	+" TEXT, "+
					street 		+" TEXT, "+
					landmark 	+" TEXT, "+
					area 		+" TEXT, "+
					pincode 	+" TEXT, "+
					city 		+" TEXT, "+
					state 		+" TEXT, "+
					country 	+" TEXT, "+
					latitude	+" TEXT, "+
					longitude	+" TEXT, "+
					tel_no1		+" TEXT, "+
					tel_no2		+" TEXT, "+
					mob_no1		+" TEXT, "+
					mob_no2		+" TEXT, "+
					fax_no1		+" TEXT, "+
					fax_no2		+" TEXT, "+
					toll_free_no1	+" TEXT, "+
					toll_free_no2	+" TEXT, "+
					photo			+" TEXT, "+
					fav			+ " TEXT);");	


			/*
			 * Creating FAv Geo+Text Search
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_GEOTEXT_FAV_SEARCH +" ("+
					KEY_FAV_AUTO_ID  +" INTEGER PRIMARY KEY AUTOINCREMENT , "+
					KEY_FAV_SEARCH_ID		+" INTEGER NOT NULL UNIQUE, "+
					KEY_FAV_USER_ID		+" TEXT," +
					KEY_FAV_PLACE_PARENT +" INTEGER,  "+
					KEY_FAV_STORE_NAME 	+" TEXT, "+
					KEY_FAV_DESCRIPTION 	+" TEXT, "+
					KEY_FAV_BUILDING 	+" TEXT, "+
					KEY_FAV_STREET 		+" TEXT, "+
					KEY_FAV_MALL_ID	+" TEXT, "+
					KEY_FAV_LANDMARK 		+" TEXT, "+
					KEY_FAV_AREA 	+" TEXT, "+
					KEY_FAV_CITY 	+" TEXT, "+
					KEY_FAV_MOB_NO1 	+" TEXT, "+
					KEY_FAV_MOB_NO2 	+" TEXT, "+
					KEY_FAV_MOB_NO3 	+" TEXT, "+
					KEY_FAV_LAND_NO1 	+" TEXT, "+
					KEY_FAV_LAND_NO2 	+" TEXT, "+
					KEY_FAV_LAND_NO3 	+" TEXT, "+
					KEY_FAV_TOLL_FREE_NO1	+	" TEXT, " +
					KEY_FAV_TOLL_FREE_NO2	+ 	" TEXT, " +
					KEY_FAV_PHOTO 	+" TEXT, "+
					KEY_FAV_DISTANCE 	+" TEXT, "+
					KEY_FAV_FAVOURITED 	+" TEXT, "+
					KEY_FAV_SEARCH_HAS_OFFER 	+" TEXT, "+
					KEY_FAV_SEARCH_OFFER_TITLE 	+" TEXT, "+
					KEY_FAV_CATEGORY 	+" TEXT, "+
					KEY_FAV_CURRENT_PAGE +" INTEGER, "+
					KEY_FAV_TOTAL_PAGES +" INTEGER, "+
					KEY_FAV_TOTAL_LIKE +" INTEGER, "+
					KEY_FAV_TOTAL_RATING+" INTEGER, "+
					KEY_FAV_TOTAL_RECORD + " INTEGER);");	

			/*
			 * Creating Geo+Text Search
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_GEOTEXT_SEARCH +" ("+
					KEY_SEARCH_AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_SEARCH_ID		+" INTEGER NOT NULL UNIQUE, "+
					KEY_SEARCH_PLACE_PARENT +" INTEGER,  "+
					KEY_SEARCH_STORE_NAME 	+" TEXT, "+
					KEY_SEARCH_DESCRIPTION 	+" TEXT, "+
					KEY_SEARCH_BUILDING 	+" TEXT, "+
					KEY_SEARCH_STREET 		+" TEXT, "+
					KEY_SEARCH_LANDMARK 		+" TEXT, "+
					KEY_SEARCH_AREA 	+" TEXT, "+
					KEY_SEARCH_CITY 	+" TEXT, "+
					KEY_SEARCH_MOB_NO1 	+" TEXT, "+
					KEY_SEARCH_PHOTO 	+" TEXT, "+
					KEY_SEARCH_DISTANCE 	+" TEXT, "+
					KEY_SEARCH_FAVOURITED 	+" TEXT, "+
					KEY_SEARCH_HAS_OFFER 	+" TEXT, "+
					KEY_SEARCH_OFFER_TITLE 	+" TEXT, "+
					KEY_SEARCH_CURRENT_PAGE +" INTEGER, "+
					KEY_SEARCH_TOTAL_PAGES +" INTEGER, "+
					KEY_TOTAL_LIKE +" INTEGER, "+
					KEY_SEARCH_TOTAL_RECORD + " INTEGER);");	



			/*
			 * Create table AREA
			 */

			db.execSQL("CREATE TABLE IF NOT EXISTS " +
					TABLE_AREAS +" ("+
					key_area_auto_id + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
					mall_id + " TEXT, " +
					area_name + " TEXT, " +
					area_latitude + " TEXT, " +
					area_longitude + " TEXT, " +
					area_pincode + " TEXT, " +
					area_city + " TEXT, " +
					area_country + " TEXT, " +
					area_country_code + " TEXT, " +
					area_iso_code + " TEXT, " +
					area_locality + " TEXT, " +
					area_state + " TEXT, " +
					place_parent + " TEXT, "+
					fb_url + " TEXT, "+
					active_area+" TEXT);");



			/*
			 * Creating TABLE_PLACE_CHOOSER 
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_PLACE_CHOOSER +" ("+
					KEY_PLACE_AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_SHOP_ID		+" INTEGER NOT NULL UNIQUE, "+
					KEY_SHOP_PARENT +" INTEGER,  "+
					KEY_SHOP_NAME 	+" TEXT, "+
					KEY_SHOP_DESCRIPTION 	+" TEXT, "+
					KEY_SHOP_BUILDING 	+" TEXT, "+
					KEY_SHOP_STREET 		+" TEXT, "+
					KEY_SHOP_LANDMARK 		+" TEXT, "+
					KEY_SHOP_MALL_ID 	+" TEXT, "+
					KEY_SHOP_AREA 	+" TEXT, "+
					KEY_SHOP_PINCODE 	+" TEXT, "+
					KEY_SHOP_CITY 	+" TEXT, "+
					KEY_SHOP_STATE 	+" TEXT, "+
					KEY_SHOP_COUNTRY 	+" TEXT, "+
					KEY_SHOP_LATITUDE 	+" TEXT, "+
					KEY_SHOP_LONGITUDE 	+" TEXT, "+
					KEY_SHOP_TELLNO1 	+" TEXT, "+
					KEY_SHOP_TELLNO2 +" TEXT, "+
					//KEY_PLACE_TELLNO3 +" TEXT, "+

					KEY_SHOP_MOBNO1 	+" TEXT, "+
					KEY_SHOP_MOBNO2 +" TEXT, "+
					//KEY_SHOP_MOBNO3 +" TEXT, "+

					KEY_SHOP_image_url 	+" TEXT, "+
					KEY_SHOP_email +" TEXT, "+
					KEY_SHOP_website +" TEXT, "+

					KEY_SHOP_facebook 	+" TEXT, "+
					KEY_SHOP_twitter +" TEXT, "+
					KEY_SHOP_place_status +" TEXT, "+
					KEY_SHOP_place_published 	+" TEXT, " +
					KEY_SHOP_SUB_CATEGORY	+" TEXT);");	


			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					CATEGORY_TABLE +" ("+

					KEY_CAT_TEXT_SEARCH_AUTO_ID 		+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_CAT_TEXT_STORE_ID				+" TEXT, "+
					KEY_CAT_TEXT_STORE_PLACE_PARENT 	+" TEXT,  "+

					KEY_CAT_TEXT_STORE_STORE_NAME 		+" TEXT, "+
					KEY_CAT_TEXT_STORE_DESCRIPTION 		+" TEXT, "+
					KEY_CAT_TEXT_STORE_BUILDING 		+" TEXT, "+

					KEY_CAT_TEXT_STORE_STREET 			+" TEXT, "+
					KEY_CAT_TEXT_STORE_LANDMARK 		+" TEXT, "+
					KEY_CAT_TEXT_STORE_AREA 			+" TEXT, "+

					KEY_CAT_TEXT_STORE_CITY 			+" TEXT, "+
					KEY_CAT_TEXT_STORE_MOB_NO1 			+" TEXT, "+
					KEY_CAT_TEXT_STORE_PHOTO 			+" TEXT, "+


					KEY_CAT_TEXT_STORE_TOTAL_PAGES 		+" TEXT, "+
					KEY_CAT_TEXT_STORE_TOTAL_RECORD 	+" TEXT, "+



					KEY_CAT_TEXT_TOTAL_LIKE 			+" TEXT, "+
					KEY_CAT_TEXT_STORE_HAS_OFFER 		+" TEXT, "+

					KEY_CAT_TEXT_STORE_OFFER_TITLE 		+" TEXT, "+
					KEY_CAT_TEXT_CATEGORY_ID 			+" TEXT);");	


			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_PARENT_STORE +" ("+

					KEY_PARENT_STORE_AUTO_ID			+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_PARENT_STORE_ID					+" TEXT, "+
					KEY_PARENT_STORE_NAME					+" TEXT, "+
					KEY_PARENT_STORE_PLACE_PARENT			+" TEXT, "+
					KEY_PARENT_STORE_DESCRIPTION			+" TEXT, "+

					KEY_PARENT_STORE_BUILDING				+" TEXT, "+
					KEY_PARENT_STORE_STREET					+" TEXT, "+
					KEY_PARENT_STORE_LANDMARK				+" TEXT, "+

					KEY_PARENT_STORE_MALL_ID				+" TEXT, "+
					KEY_PARENT_STORE_AREA					+" TEXT, "+
					KEY_PARENT_STORE_PINCODE				+" TEXT, "+

					KEY_PARENT_STORE_CITY					+" TEXT, "+
					KEY_PARENT_STORE_STATE					+" TEXT, "+
					KEY_PARENT_STORE_COUNTRY				+" TEXT, "+

					KEY_PARENT_STORE_LAT					+" TEXT, "+
					KEY_PARENT_STORE_LONG					+" TEXT, "+
					KEY_PARENT_STORE_TEL_NO_1				+" TEXT, "+


					KEY_PARENT_STORE_TEL_NO_2				+" TEXT, "+
					KEY_PARENT_STORE_MOB_NO_1				+" TEXT, "+
					KEY_PARENT_STORE_MOB_NO_2				+" TEXT, "+

					KEY_PARENT_STORE_FAX_NO_1				+" TEXT, "+
					KEY_PARENT_STORE_FAX_NO_2				+" TEXT, "+
					KEY_PARENT_STORE_TOLL_FREE_NO_1			+" TEXT, "+

					KEY_PARENT_STORE_TOLL_FREE_NO_2			+" TEXT, "+
					KEY_PARENT_STORE_IMAGE_URL				+" TEXT, "+
					KEY_PARENT_STORE_EMAIL					+" TEXT, "+


					KEY_PARENT_STORE_WEBSITE				+" TEXT, "+
					KEY_PARENT_STORE_FACEBOOK_URL			+" TEXT, "+
					KEY_PARENT_STORE_TWITTER_URL 			+" TEXT);");	

			/** 
			 * Create table Stores.
			 * 
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+ Tables.TABLE_STORES +"("+
					Tables.ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					Tables.STORE_ID + " TEXT, "+
					Tables.STORE_NAME + " TEXT, "+
					Tables.STORE_MALL_ID + " TEXT, "+
					Tables.STORE_PLACEPARENT + " TEXT, "+
					Tables.STORE_LOGO + " TEXT, "+
					Tables.STORE_DESC + " TEXT, "+
					Tables.STORE_CITY + " TEXT, "+
					Tables.STORE_BUILDING + " TEXT, "+
					Tables.STORE_STREET + " TEXT, "+
					Tables.STORE_AREA + " TEXT, "+
					Tables.STORE_LANDMARK + " TEXT, "+
					Tables.STORE_MOB_NO_1 + " TEXT, "+
					Tables.STORE_MOB_NO_2 + " TEXT, "+
					Tables.STORE_MOB_NO_3 + " TEXT, "+
					Tables.STORE_LAND_NO_1 + " TEXT, "+
					Tables.STORE_LAND_NO_2 + " TEXT, "+
					Tables.STORE_LAND_NO_3 + " TEXT, "+
					Tables.STORE_TOLL_FREE_NO_1	+	" TEXT, " +
					Tables.STORE_TOLL_FREE_NO_2 + 	" TEXT, " +
					Tables.STORE_WEBSITE + " TEXT, "+
					Tables.STORE_FACEBOOK_URl + " TEXT, "+
					Tables.STORE_WEBSITE_URl + " TEXT, "+
					Tables.STORE_EMAIL + " TEXT, "+
					Tables.STORE_TOTAL_LIKE + " TEXT, "+
					Tables.STORE_TOTAL_RATING + " TEXT," +
					Tables.STORE_HAS_OFFER + " TEXT, "+
					Tables.STORE_OFFER_TITLE + " TEXT, "+
					Tables.STORE_CATEGORY + " TEXT, " +
					Tables.STORE_SUB_CATEGORY+ " TEXT);");


			/** 
			 * Create table refresh.
			 * 
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+ Tables.TABLE_Refersh +"("+
					Tables.ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					Tables.KEY_Mall_Id + " TEXT, "+

					Tables.KEY_Cat_All_oldValue + " TEXT, "+
					Tables.KEY_Cat_All_newValue + " TEXT, "+

					Tables.KEY_Cat_Shop_oldValue + " TEXT, "+
					Tables.KEY_Cat_Shop_newValue + " TEXT, "+

					Tables.KEY_Cat_Dine_oldValue + " TEXT, "+
					Tables.KEY_Cat_Dine_newValue + " TEXT, "+

					Tables.KEY_Cat_Entertainment_oldValue + " TEXT, "+
					Tables.KEY_Cat_Entertainment_newValue + " TEXT, "+

					Tables.KEY_Cat_Pamper_oldValue + " TEXT, "+
					Tables.KEY_Cat_Pamper_newValue + " TEXT, "+

					Tables.KEY_Cat_All_OfferCount_oldValue + " TEXT, "+
					Tables.KEY_Cat_All_OfferCount_newValue + " TEXT, "+

					Tables.KEY_Cat_Shop_OfferCount_oldValue + " TEXT, "+
					Tables.KEY_Cat_Shop_OfferCount_newValue + " TEXT, "+

					Tables.KEY_Cat_Dine_OfferCount_oldValue + " TEXT, "+
					Tables.KEY_Cat_Dine_OfferCount_newValue + " TEXT, "+

					Tables.KEY_Cat_Entertainment_OfferCount_oldValue + " TEXT, "+
					Tables.KEY_Cat_Entertainment_OfferCount_newValue + " TEXT, "+

					Tables.KEY_Cat_Pamper_OfferCount_oldValue + " TEXT, "+
					Tables.KEY_Cat_Pamper_OfferCount_newValue + " TEXT);");
			
			db.execSQL("CREATE TABLE IF NOT EXISTS "+MENU_TABLE+ "("+
					MENU_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					MENU_PLACE_ID + " PLACE_ID, "+
					MENU_IMAGE_URL_KEYS + " MENU_URL_KEYS);");
			
			db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_ANALYTICS + " ( " +
					ANALYTICS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					ANALYTICS_UDM_ID + " TEXT, " +
					ANALYTICS_DATE + " TEXT, " +
					ANALYTICS_FLAG + " TEXT, " +
					ANALYTICS_ACTIVITY_ID + " TEXT, " +
					ANALYTICS_MALL_ID + " TEXT, " +
					ANALYTICS_SYNC_STATUS + " INTEGER DEFAULT 0);");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			Log.d("", "Inorbit Darabase Updated");
			db.execSQL("DROP TABLE IF EXISTS "+MERCHANT_TABLE);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_BUSINESS);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_BUSINESS_STORE);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_TEXT_SEARCH);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_GEOTEXT_SEARCH);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_GEOTEXT_FAV_SEARCH);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_AREAS);
			db.execSQL("DROP TABLE IF EXISTS "+CATEGORY_TABLE);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_PARENT_STORE);	
			db.execSQL("DROP TABLE IF EXISTS "+Tables.TABLE_STORES);
			db.execSQL("DROP TABLE IF EXISTS "+Tables.TABLE_Refersh);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_PLACE_CHOOSER);
			//db.execSQL("DROP TABLE IF EXISTS "+Tables.TA);
			onCreate(db);

		}
	}

	public  DBUtil open() throws SQLException{
		dbHelper=new DbHelper(ourContext);
		sqlDataBase=dbHelper.getWritableDatabase();
		return this;

	}
	
	public DbHelper getDBHelper() {
		if (dbHelper == null) {
			dbHelper = new DbHelper(ourContext); 
		}
		return dbHelper;
	}
	
	public void close(){
		dbHelper.close();
	}


	/*
	 * Create Store
	 *
	 */
	public long create_store(int store_id,int store_bid,String store_name,String buildingName,String street,
			String landmark,String area,int pincode,String city,String state,String country,String geolocation,
			String telno1,String telno2,String mobno1,String mobno2,String faxno1,String faxno2,String toll_freeno1,String toll_freeno2,String photo)
	{
		Log.i("INSIDE ", "DATA BASE STORE");

		ContentValues cv=new ContentValues();
		cv.put(KEY_STORE_ID, store_id);
		cv.put(KEY_STORE_BUSINESS_ID, store_bid);
		cv.put(KEY_STORE_NAME, store_name);
		cv.put(KEY_STORE_BUILDING, buildingName);
		cv.put(KEY_STORE_STREET, street);
		cv.put(KEY_STORE_LANDMARK, landmark);
		cv.put(KEY_STORE_AREA, area);
		cv.put(KEY_STORE_PINCODE, pincode);
		cv.put(KEY_STORE_CITY, city);
		cv.put(KEY_STORE_STATE, state);
		cv.put(KEY_STORE_COUNTRY, country);
		cv.put(KEY_STORE_GEOLOCATION, geolocation);
		cv.put(KEY_STORE_TEL_NO1, telno1);
		cv.put(KEY_STORE_TEL_NO2, telno2);
		cv.put(KEY_STORE_TEL_NO1, telno1);
		cv.put(KEY_STORE_MOB_NO1, mobno1);
		cv.put(KEY_STORE_MOB_NO2, mobno2);
		cv.put(KEY_STORE_FAXNO1, faxno1);
		cv.put(KEY_STORE_FAXNO2, faxno2);
		cv.put(KEY_STORE_TOLL_FREE_NO1, toll_freeno1);
		cv.put(KEY_STORE_TOLL_FREE_NO2, toll_freeno2);
		cv.put(KEY_STORE_PHOTO, photo);
		return sqlDataBase.insert(TABLE_BUSINESS_STORE, null, cv);

	}



	public long create_category_Table(StoreInfo storeInfo,String CATEGORY_ID,String TOTAL_PAGES,String TOTAL_RECORD){

		ContentValues cv=new ContentValues();
		try{	

			cv.put(KEY_CAT_TEXT_STORE_ID, storeInfo.getId());
			cv.put(KEY_CAT_TEXT_STORE_PLACE_PARENT, storeInfo.getPlace_parent());

			cv.put(KEY_CAT_TEXT_STORE_STORE_NAME, storeInfo.getName());
			cv.put(KEY_CAT_TEXT_STORE_DESCRIPTION, storeInfo.getDescription());
			cv.put(KEY_CAT_TEXT_STORE_BUILDING, storeInfo.getBuilding());

			cv.put(KEY_CAT_TEXT_STORE_STREET, storeInfo.getStreet());
			cv.put(KEY_CAT_TEXT_STORE_LANDMARK, storeInfo.getLandmark());
			cv.put(KEY_CAT_TEXT_STORE_AREA, storeInfo.getArea());

			cv.put(KEY_CAT_TEXT_STORE_CITY, storeInfo.getCity());
			cv.put(KEY_CAT_TEXT_STORE_MOB_NO1, storeInfo.getMob_no1());
			cv.put(KEY_CAT_TEXT_STORE_PHOTO, storeInfo.getImage_url());


			cv.put(KEY_CAT_TEXT_TOTAL_LIKE, storeInfo.getTotal_like());
			cv.put(KEY_CAT_TEXT_STORE_HAS_OFFER, storeInfo.getHas_offer());

			cv.put(KEY_CAT_TEXT_STORE_OFFER_TITLE, storeInfo.getTitle());
			cv.put(KEY_CAT_TEXT_CATEGORY_ID, CATEGORY_ID);
			cv.put(KEY_CAT_TEXT_STORE_TOTAL_PAGES, TOTAL_PAGES);

			cv.put(KEY_CAT_TEXT_STORE_TOTAL_RECORD, TOTAL_RECORD);




		}catch(Exception ex){
			ex.printStackTrace();
		}
		InorbitLog.d("Structure "+cv.toString());
		return sqlDataBase.insert(CATEGORY_TABLE, null, cv);


	}

	public void create_category_Table(ArrayList<StoreInfo> storeInfo,String CATEGORY_ID,String TOTAL_PAGES,String TOTAL_RECORD){

		open();
		sqlDataBase.beginTransaction();

		try{	
			for(int i=0;i<storeInfo.size();i++){
				ContentValues cv=new ContentValues();

				InorbitLog.d("Database "+storeInfo.get(i).getName());
				cv.put(KEY_CAT_TEXT_STORE_ID, storeInfo.get(i).getId());
				cv.put(KEY_CAT_TEXT_STORE_PLACE_PARENT, storeInfo.get(i).getPlace_parent());

				cv.put(KEY_CAT_TEXT_STORE_STORE_NAME, storeInfo.get(i).getName());
				cv.put(KEY_CAT_TEXT_STORE_DESCRIPTION, storeInfo.get(i).getDescription());
				cv.put(KEY_CAT_TEXT_STORE_BUILDING, storeInfo.get(i).getBuilding());

				cv.put(KEY_CAT_TEXT_STORE_STREET, storeInfo.get(i).getStreet());
				cv.put(KEY_CAT_TEXT_STORE_LANDMARK, storeInfo.get(i).getLandmark());
				cv.put(KEY_CAT_TEXT_STORE_AREA, storeInfo.get(i).getArea());

				cv.put(KEY_CAT_TEXT_STORE_CITY, storeInfo.get(i).getCity());
				cv.put(KEY_CAT_TEXT_STORE_MOB_NO1, storeInfo.get(i).getMob_no1());
				cv.put(KEY_CAT_TEXT_STORE_PHOTO, storeInfo.get(i).getImage_url());


				cv.put(KEY_CAT_TEXT_TOTAL_LIKE, storeInfo.get(i).getTotal_like());
				cv.put(KEY_CAT_TEXT_STORE_HAS_OFFER, storeInfo.get(i).getHas_offer());

				cv.put(KEY_CAT_TEXT_STORE_OFFER_TITLE, storeInfo.get(i).getTitle());
				cv.put(KEY_CAT_TEXT_CATEGORY_ID, CATEGORY_ID);
				cv.put(KEY_CAT_TEXT_STORE_TOTAL_PAGES, TOTAL_PAGES);

				cv.put(KEY_CAT_TEXT_STORE_TOTAL_RECORD, TOTAL_RECORD);

				sqlDataBase.insert(CATEGORY_TABLE, null, cv);


			}
			sqlDataBase.setTransactionSuccessful();

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			sqlDataBase.endTransaction();
		}
		close();
		//InorbitLog.d("Structure "+cv.toString());
		//return sqlDataBase.insert(CATEGORY_TABLE, null, cv);


	}




	public long create_parent_store_Table(ParticularStoreInfo storeInfo){

		ContentValues cv=new ContentValues();
		try{	

			cv.put(KEY_PARENT_STORE_ID, storeInfo.getId());
			cv.put(KEY_PARENT_STORE_NAME, storeInfo.getName());

			cv.put(KEY_PARENT_STORE_PLACE_PARENT, storeInfo.getPlace_parent());
			cv.put(KEY_PARENT_STORE_DESCRIPTION, storeInfo.getDescription());
			cv.put(KEY_PARENT_STORE_AREA, storeInfo.getArea());

			cv.put(KEY_PARENT_STORE_BUILDING, storeInfo.getBuilding());
			cv.put(KEY_PARENT_STORE_CITY, storeInfo.getCity());
			cv.put(KEY_PARENT_STORE_COUNTRY, storeInfo.getCountry());

			cv.put(KEY_PARENT_STORE_EMAIL, storeInfo.getEmail());
			cv.put(KEY_PARENT_STORE_FACEBOOK_URL, storeInfo.getFacebook_url());
			cv.put(KEY_PARENT_STORE_FAX_NO_1, storeInfo.getFax_no1());

			cv.put(KEY_PARENT_STORE_FAX_NO_2, storeInfo.getFax_no2());
			cv.put(KEY_PARENT_STORE_IMAGE_URL, storeInfo.getImage_url());
			cv.put(KEY_PARENT_STORE_LANDMARK, storeInfo.getLandmark());

			cv.put(KEY_PARENT_STORE_LAT, storeInfo.getLatitude());
			cv.put(KEY_PARENT_STORE_LONG, storeInfo.getLongitude());
			cv.put(KEY_PARENT_STORE_MALL_ID, storeInfo.getMall_id());

			cv.put(KEY_PARENT_STORE_MOB_NO_1, storeInfo.getMob_no1());
			cv.put(KEY_PARENT_STORE_MOB_NO_2, storeInfo.getMob_no2());
			cv.put(KEY_PARENT_STORE_PINCODE, storeInfo.getPincode());

			cv.put(KEY_PARENT_STORE_STATE, storeInfo.getState());
			cv.put(KEY_PARENT_STORE_STREET, storeInfo.getStreet());
			cv.put(KEY_PARENT_STORE_TEL_NO_1, storeInfo.getTel_no1());

			cv.put(KEY_PARENT_STORE_TEL_NO_2, storeInfo.getTel_no2());
			cv.put(KEY_PARENT_STORE_TOLL_FREE_NO_1, storeInfo.getToll_free_no1());
			cv.put(KEY_PARENT_STORE_TOLL_FREE_NO_2, storeInfo.getToll_free_no2());

			cv.put(KEY_PARENT_STORE_TWITTER_URL, storeInfo.getTwitter_url());
			cv.put(KEY_PARENT_STORE_WEBSITE, storeInfo.getWebsite());


			InorbitLog.d("Database Store "+storeInfo.getName());


		}catch(Exception ex){
			ex.printStackTrace();
		}

		return sqlDataBase.insert(TABLE_PARENT_STORE, null, cv);
	}


	/**
	 * 
	 *  Delete all the parent store data
	 */
	void deleteParentStore(){
		try{
			open();
			sqlDataBase.delete(TABLE_PARENT_STORE,null,null);
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			close();
		}

	}


	public long create_geotext(
			int geo_txt_id,
			int place_parent,
			String search_storename,
			String description,
			String building,
			String street,
			String landmark,
			String area,
			String city,
			String mobno1,
			String photo,
			String distance,
			int total_pages,
			int total_record,
			int current_page,
			String fav,
			String hasoffer,
			String offertite,
			int total_like)
	{
		Log.i("INSIDE ", "DATA BASE STORE BUSINESS" +search_storename+" "+area);

		ContentValues cv=new ContentValues();
		cv.put(KEY_SEARCH_ID, geo_txt_id);
		cv.put(KEY_SEARCH_PLACE_PARENT, place_parent);
		cv.put(KEY_SEARCH_STORE_NAME, search_storename);
		cv.put(KEY_SEARCH_DESCRIPTION, description);
		cv.put(KEY_SEARCH_BUILDING, building);
		cv.put(KEY_SEARCH_STREET, street);
		cv.put(KEY_SEARCH_LANDMARK, landmark);
		cv.put(KEY_SEARCH_AREA, area);
		cv.put(KEY_SEARCH_CITY, city);
		cv.put(KEY_SEARCH_MOB_NO1, mobno1);
		cv.put(KEY_SEARCH_PHOTO, photo);
		cv.put(KEY_SEARCH_DISTANCE, distance);
		cv.put(KEY_SEARCH_TOTAL_PAGES, total_pages);
		cv.put(KEY_SEARCH_TOTAL_RECORD, total_record);
		cv.put(KEY_SEARCH_FAVOURITED, fav);
		cv.put(KEY_SEARCH_HAS_OFFER, hasoffer);
		cv.put(KEY_SEARCH_OFFER_TITLE, offertite);
		cv.put(KEY_SEARCH_CURRENT_PAGE, current_page);
		cv.put(KEY_TOTAL_LIKE, total_like);

		return sqlDataBase.insert(TABLE_GEOTEXT_SEARCH, null, cv);

	}


	//***************************** Favourite Geo text search ****************************//
	public long create_geotextFav(int geo_txt_id,int place_parent,String search_storename,String description,
			String building,String street,String landmark,String area,String city,
			String mobno1, String mobno2, String mobno3, String landNo1, String landNo2, String landNo3, String tollFreeNo1, String tollFreeNo2,
			String photo,String distance,int total_pages,int total_record,int current_page,String fav,String hasoffer,String offertite,int total_like)
	{
		Log.i("INSIDE ", "DATA BASE STORE BUSINESS "+geo_txt_id);

		ContentValues cv=new ContentValues();
		cv.put(KEY_FAV_SEARCH_ID, geo_txt_id);
		cv.put(KEY_FAV_PLACE_PARENT, place_parent);
		cv.put(KEY_FAV_STORE_NAME, search_storename);
		cv.put(KEY_FAV_DESCRIPTION, description);
		cv.put(KEY_FAV_BUILDING, building);
		cv.put(KEY_FAV_STREET, street);
		cv.put(KEY_FAV_LANDMARK, landmark);
		cv.put(KEY_FAV_AREA, area);
		cv.put(KEY_FAV_CITY, city);
		cv.put(KEY_FAV_MOB_NO1, mobno1);
		cv.put(KEY_FAV_MOB_NO2, mobno2);
		cv.put(KEY_FAV_MOB_NO3, mobno3);
		cv.put(KEY_FAV_LAND_NO1, landNo1);
		cv.put(KEY_FAV_LAND_NO2, landNo2);
		cv.put(KEY_FAV_LAND_NO3, landNo3);
		cv.put(KEY_FAV_TOLL_FREE_NO1, tollFreeNo1);
		cv.put(KEY_FAV_TOLL_FREE_NO2, tollFreeNo2);
		cv.put(KEY_FAV_PHOTO, photo);
		cv.put(KEY_FAV_DISTANCE, distance);
		cv.put(KEY_FAV_TOTAL_PAGES, total_pages);
		cv.put(KEY_FAV_TOTAL_RECORD, total_record);
		cv.put(KEY_FAV_FAVOURITED, fav);
		cv.put(KEY_FAV_SEARCH_HAS_OFFER, hasoffer);
		cv.put(KEY_FAV_SEARCH_OFFER_TITLE, offertite);
		cv.put(KEY_FAV_CURRENT_PAGE, current_page);
		cv.put(KEY_FAV_TOTAL_LIKE, total_like);

		Log.i("Current Page", "Current Page "+current_page);

		return sqlDataBase.insert(TABLE_GEOTEXT_FAV_SEARCH, null, cv);

	}

	/**
	 * Add the users favourited stores
	 * 
	 * @param marrStoreInfo ArrayList of StoreInfo;
	 * @return long : number of rows affected
	 */
	public long updateUsersFavStores(ArrayList<StoreInfo> marrStoreInfo, String user_id){
		long mlRowsAffected=0;
		ContentValues cv=new ContentValues();
		open();
		try {
			sqlDataBase.delete(TABLE_GEOTEXT_FAV_SEARCH, null, null);
			
			for(int i=0;i<marrStoreInfo.size();i++) {
				InorbitLog.d("StoreName "+marrStoreInfo.get(i).getName());
				cv.put(KEY_FAV_SEARCH_ID, marrStoreInfo.get(i).getId().toString());
				cv.put(KEY_FAV_USER_ID, user_id);
				cv.put(KEY_FAV_PLACE_PARENT, marrStoreInfo.get(i).getPlace_parent().toString());
				cv.put(KEY_FAV_STORE_NAME, marrStoreInfo.get(i).getName().toString());
				cv.put(KEY_FAV_DESCRIPTION, marrStoreInfo.get(i).getDescription().toString());
				cv.put(KEY_FAV_BUILDING, marrStoreInfo.get(i).getBuilding().toString());
				cv.put(KEY_FAV_STREET, marrStoreInfo.get(i).getStreet().toString());
				cv.put(KEY_FAV_MALL_ID, marrStoreInfo.get(i).getMall_id().toString());
				InorbitLog.d("!!!!!!" + marrStoreInfo.get(i).getMall_id() + " " + user_id);
				cv.put(KEY_FAV_LANDMARK, marrStoreInfo.get(i).getLandmark().toString());
				cv.put(KEY_FAV_AREA, marrStoreInfo.get(i).getArea().toString());
				cv.put(KEY_FAV_CITY, marrStoreInfo.get(i).getCity().toString());
				cv.put(KEY_FAV_MOB_NO1, marrStoreInfo.get(i).getMob_no1().toString());
				cv.put(KEY_FAV_MOB_NO2, marrStoreInfo.get(i).getMob_no2().toString());
				cv.put(KEY_FAV_MOB_NO3, marrStoreInfo.get(i).getMob_no3().toString());
				cv.put(KEY_FAV_LAND_NO1, marrStoreInfo.get(i).getTel_no1().toString());
				cv.put(KEY_FAV_LAND_NO2, marrStoreInfo.get(i).getTel_no2().toString());
				cv.put(KEY_FAV_LAND_NO3, marrStoreInfo.get(i).getTel_no3().toString());
				cv.put(KEY_FAV_TOLL_FREE_NO1, marrStoreInfo.get(i).getToll_free_no1());
				cv.put(KEY_FAV_TOLL_FREE_NO2, marrStoreInfo.get(i).getToll_free_no2());
				cv.put(KEY_FAV_PHOTO, marrStoreInfo.get(i).getImage_url().toString());

				cv.put(KEY_FAV_FAVOURITED, "1");
				cv.put(KEY_FAV_SEARCH_HAS_OFFER, marrStoreInfo.get(i).getHas_offer());
				cv.put(KEY_FAV_SEARCH_OFFER_TITLE, marrStoreInfo.get(i).getTitle());
				cv.put(KEY_FAV_TOTAL_LIKE, marrStoreInfo.get(i).getTotal_like());
				cv.put(KEY_FAV_TOTAL_RATING, marrStoreInfo.get(i).getTotal_rating());
				cv.put(KEY_FAV_CATEGORY, marrStoreInfo.get(i).getCategory());

				mlRowsAffected = sqlDataBase.insert(TABLE_GEOTEXT_FAV_SEARCH, null, cv);
				InorbitLog.d("$$$$$$$$ " + mlRowsAffected);
			}
			
			close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			close();
		}

		return mlRowsAffected;

	}


	/**
	 * Get the users favourited stores with respect to the active mall id
	 * 
	 * @param msActiveAreaId Active mall id
	 * @return  ArrayList<StoreInfo>
	 */
	public ArrayList<StoreInfo> getUsersFavStores(String msActiveAreaId, String user_id){
		open();
		ArrayList<StoreInfo> msarrStoreInfo = new ArrayList<StoreInfo>();
		try{
			String msQuery = "SELECT * FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_MALL_ID+" = '"+msActiveAreaId+"' AND "+ KEY_FAV_USER_ID +"='"+user_id+"'";
			Cursor  cursor = sqlDataBase.rawQuery(msQuery,null);
			InorbitLog.d(msQuery);
			InorbitLog.d(cursor.getCount()+" ");
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				StoreInfo mStoreInfo = new StoreInfo();

				mStoreInfo.setArea(cursor.getString(cursor.getColumnIndex(KEY_FAV_AREA)));
				mStoreInfo.setBuilding(cursor.getString(cursor.getColumnIndex(KEY_FAV_BUILDING)));
				mStoreInfo.setCity(cursor.getString(cursor.getColumnIndex(KEY_FAV_CITY)));
				mStoreInfo.setDescription(cursor.getString(cursor.getColumnIndex(KEY_FAV_DESCRIPTION)));
				mStoreInfo.setHas_offer(cursor.getString(cursor.getColumnIndex(KEY_FAV_SEARCH_HAS_OFFER)));
				mStoreInfo.setId(cursor.getString(cursor.getColumnIndex(KEY_FAV_SEARCH_ID)));
				mStoreInfo.setImage_url(cursor.getString(cursor.getColumnIndex(KEY_FAV_PHOTO)));
				mStoreInfo.setLandmark(cursor.getString(cursor.getColumnIndex(KEY_FAV_LANDMARK)));
				mStoreInfo.setMall_id(cursor.getString(cursor.getColumnIndex(KEY_FAV_MALL_ID)));
				mStoreInfo.setMob_no1(cursor.getString(cursor.getColumnIndex(KEY_FAV_MOB_NO1)));
				mStoreInfo.setMob_no2(cursor.getString(cursor.getColumnIndex(KEY_FAV_MOB_NO2)));
				mStoreInfo.setMob_no3(cursor.getString(cursor.getColumnIndex(KEY_FAV_MOB_NO3)));
				mStoreInfo.setTel_no1(cursor.getString(cursor.getColumnIndex(KEY_FAV_LAND_NO1)));
				mStoreInfo.setTel_no2(cursor.getString(cursor.getColumnIndex(KEY_FAV_LAND_NO2)));
				mStoreInfo.setTel_no3(cursor.getString(cursor.getColumnIndex(KEY_FAV_LAND_NO3)));
				mStoreInfo.setToll_free_no1(cursor.getString(cursor.getColumnIndex(KEY_FAV_TOLL_FREE_NO1)));
				mStoreInfo.setToll_free_no2(cursor.getString(cursor.getColumnIndex(KEY_FAV_TOLL_FREE_NO2)));
				mStoreInfo.setName(cursor.getString(cursor.getColumnIndex(KEY_FAV_STORE_NAME)));
				mStoreInfo.setPlace_parent(cursor.getString(cursor.getColumnIndex(KEY_FAV_PLACE_PARENT)));
				mStoreInfo.setStreet(cursor.getString(cursor.getColumnIndex(KEY_FAV_STREET)));
				mStoreInfo.setTitle(cursor.getString(cursor.getColumnIndex(KEY_FAV_SEARCH_OFFER_TITLE)));
				mStoreInfo.setTotal_like(cursor.getString(cursor.getColumnIndex(KEY_FAV_TOTAL_LIKE)));
				mStoreInfo.setTotal_rating(cursor.getString(cursor.getColumnIndex(KEY_FAV_TOTAL_RATING)));
				mStoreInfo.setCategory(cursor.getString(cursor.getColumnIndex(KEY_FAV_CATEGORY)));

				msarrStoreInfo.add(mStoreInfo);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			close();
		}
		return  msarrStoreInfo;
	}

	public long getTotalRecordsOfUsersFavStores(String msActiveAreaId, String user_id){
		String rawQuery="SELECT "+KEY_FAV_SEARCH_ID+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+ " WHERE "+KEY_FAV_MALL_ID+" = '"+msActiveAreaId+"' AND "+KEY_FAV_USER_ID+" = '"+user_id+"'";
		long rowcount=0;
		try {
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			rowcount=c.getCount();
			InorbitLog.d("complete fav table "+sqlDataBase.rawQuery("SELECT * FROM " + TABLE_GEOTEXT_FAV_SEARCH, null));
			InorbitLog.d(c.getCount()+" &&&&");
			close();
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			close();
		}
		return rowcount;

	}

	/*
	 * Getting total records from Geo-text search
	 */

	public Integer getTotalRecords(int place_parent){
		int total_records = 0;
		try{
			open();
			String query="SELECT "+KEY_SEARCH_TOTAL_RECORD+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent;
			Cursor  cursor = sqlDataBase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				total_records=cursor.getInt(cursor.getColumnIndex(KEY_SEARCH_TOTAL_RECORD));
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			close();
		}
		return total_records;

	}

	/*
	 * Getting total no of pages from Geo-text search
	 */

	public Integer getTotalPages(int place_parent){
		int total_pages = 0;
		try{
			open();
			String query="SELECT "+KEY_SEARCH_TOTAL_PAGES+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent;
			Cursor  cursor = sqlDataBase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())	{
				total_pages=cursor.getInt(cursor.getColumnIndex(KEY_SEARCH_TOTAL_PAGES));
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			close();
		}
		return total_pages;

	}



	public long getRowCountOfCategoryTable(String category_id,String place_id){
		String rawQuery="SELECT COUNT("+KEY_CAT_TEXT_STORE_TOTAL_RECORD+") AS ROW_COUNT FROM "+CATEGORY_TABLE +" WHERE "+KEY_CAT_TEXT_CATEGORY_ID+" = '"+category_id+"' AND "+KEY_CAT_TEXT_STORE_PLACE_PARENT+" = '"+place_id+"'";
		long rowcount=0;
		try{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			close();
		}
		return rowcount;

	}

	public long getRowCountOfPlarentTable(){
		String rawQuery="SELECT COUNT("+KEY_PARENT_STORE_ID+") AS ROW_COUNT FROM "+TABLE_PARENT_STORE;
		long rowcount=0;
		try{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			close();
		}
		return rowcount;

	}


	public long getRowCount(int place_parent){
		String rawQuery="SELECT COUNT("+KEY_SEARCH_PLACE_PARENT+") AS ROW_COUNT FROM "+TABLE_GEOTEXT_SEARCH +" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent;
		long rowcount=0;
		try{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			close();
		}
		return rowcount;

	}

	public long getRowCount(String area){
		String rawQuery="SELECT COUNT("+KEY_SEARCH_ID+") AS ROW_COUNT FROM "+TABLE_GEOTEXT_SEARCH +" WHERE "+KEY_SEARCH_AREA+" like '"+area+"'";
		Log.i("ROW COUNT","ROW COUNT QUERY "+ rawQuery);
		long rowcount=0;
		try{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			close();
		}
		Log.i("ROW COUNT","ROW COUNT "+ rowcount);
		return rowcount;

	}

	//Fav row count
	public long getFavRowCount(){
		String rawQuery="SELECT COUNT("+KEY_FAV_SEARCH_ID+") AS ROW_COUNT FROM "+TABLE_GEOTEXT_FAV_SEARCH;
		long rowcount=0;
		try{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			close();
		}
		return rowcount;

	}

	//Fav row count
	public long getFavRowCount(int place_parent){
		String rawQuery="SELECT COUNT("+KEY_FAV_SEARCH_ID+") AS ROW_COUNT FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_PLACE_PARENT+" = "+place_parent;
		long rowcount=0;
		try{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			close();
		}
		return rowcount;

	}


	public ArrayList<StoreInfo> getCategoryStoreInfo(String categoryId,String place_id){

		ArrayList<StoreInfo> arr_storeInfo = new ArrayList<StoreInfo>();

		String query = "SELECT * FROM "+CATEGORY_TABLE+" WHERE "+KEY_CAT_TEXT_CATEGORY_ID+"='"+categoryId+"' AND "+KEY_CAT_TEXT_STORE_PLACE_PARENT+"='"+place_id+"'";

		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			StoreInfo storeInfo = new StoreInfo();

			storeInfo.setArea(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_AREA)));
			storeInfo.setBuilding(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_BUILDING)));
			storeInfo.setCity(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_CITY)));
			storeInfo.setDescription(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_DESCRIPTION)));

			storeInfo.setHas_offer(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_HAS_OFFER)));
			storeInfo.setId(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_ID)));
			storeInfo.setImage_url(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_PHOTO)));
			storeInfo.setLandmark(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_LANDMARK)));

			storeInfo.setMob_no1(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_MOB_NO1)));
			storeInfo.setName(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_STORE_NAME)));
			storeInfo.setPlace_parent(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_PLACE_PARENT)));
			storeInfo.setStreet(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_STREET)));
			storeInfo.setTitle(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_OFFER_TITLE)));
			storeInfo.setTotal_like(cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_TOTAL_LIKE)));

			arr_storeInfo.add(storeInfo);
		}



		return arr_storeInfo;
	}



	public String getTotalRecordsOfParticularCategory(String category_id,String place_id){
		String records = "";
		String query  = "SELECT "+KEY_CAT_TEXT_STORE_TOTAL_RECORD+" FROM "+CATEGORY_TABLE+" WHERE "+KEY_CAT_TEXT_CATEGORY_ID+"='"+category_id+"' AND "+KEY_CAT_TEXT_STORE_PLACE_PARENT+"='"+place_id+"'";
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			records = cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_TOTAL_RECORD));
		}

		return records;
	}

	public String getTotalPagesOfParticularCategory(String category_id,String place_id){
		String pages = "";
		String query  = "SELECT "+KEY_CAT_TEXT_STORE_TOTAL_PAGES+" FROM "+CATEGORY_TABLE+" WHERE "+KEY_CAT_TEXT_CATEGORY_ID+"='"+category_id+"' AND "+KEY_CAT_TEXT_STORE_PLACE_PARENT+"='"+place_id+"'";
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			pages = cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_TOTAL_PAGES));
		}

		return pages;
	}

	/*
	 * Get Fav by id from Geo-Text table
	 */

	public String getGeoFavorite(int store_id){
		String fav = "";
		try{
			open();
			String query="SELECT "+KEY_SEARCH_FAVOURITED+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_ID+"=" + store_id;
			Cursor  cursor = sqlDataBase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				fav=cursor.getString(cursor.getColumnIndex(KEY_SEARCH_FAVOURITED));
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			close();
		}
		return fav;

	}



	public String getGeoDistance(int store_id)
	{
		String distance = "";
		try
		{

			open();



			String query="SELECT "+KEY_SEARCH_DISTANCE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				distance=cursor.getString(cursor.getColumnIndex(KEY_SEARCH_DISTANCE));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return distance;

	}

	/*
	 * Get Distance by id from Geo-Text table
	 */

	public boolean isGeoIdExists(int store_id)
	{
		String id = "";
		boolean exists=false;

		try
		{

			open();

			String query="SELECT "+KEY_SEARCH_ID+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_ID+"=" + store_id;

			Cursor  cursor  = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{

				id=cursor.getInt(cursor.getColumnIndex(KEY_SEARCH_ID))+"";

				if(id.equalsIgnoreCase(store_id+"")){
					exists=true;
					break;
				}

			}
			cursor.close();
			close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}
		Log.i("ID EXISTIS","IS ID EXISTS " +exists);
		return exists;

	}

	public boolean isStoreExists(String storeId,String catid){
		String id = "";
		boolean exists=false;
		try{
			open();
			String query="SELECT "+KEY_CAT_TEXT_STORE_ID+" FROM "+CATEGORY_TABLE+" WHERE "+KEY_CAT_TEXT_STORE_ID+"=" + storeId+" AND "+KEY_CAT_TEXT_CATEGORY_ID+"='"+catid+"'";
			Cursor  cursor  = sqlDataBase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){

				id=cursor.getString(cursor.getColumnIndex(KEY_CAT_TEXT_STORE_ID));
				if(id.equalsIgnoreCase(storeId)){
					exists=true;
					break;
				}

			}
			cursor.close();
			close();

		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			close();
		}
		InorbitLog.d(storeId + " -- " +exists);
		return exists;
	}
	public boolean isGeoFavIdExists(int store_id)
	{
		String id = "";
		boolean exists=false;

		try
		{

			open();



			String query="SELECT "+KEY_FAV_SEARCH_ID+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_SEARCH_ID+"=" + store_id;

			Cursor  cursor  = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				id=cursor.getInt(cursor.getColumnIndex(KEY_FAV_SEARCH_ID))+"";

				if(id.equalsIgnoreCase(store_id+""))
				{
					exists=true;
					break;
				}

			}
			cursor.close();
			close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}
		Log.i("ID EXISTIS","IS ID EXISTS " +exists);
		return exists;

	}


	void deleteGeoText(int place_parent)
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_GEOTEXT_SEARCH, KEY_SEARCH_PLACE_PARENT+" = "+place_parent, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}

	void deleteCategories()
	{
		try
		{

			open();
			sqlDataBase.delete(CATEGORY_TABLE, null, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}


	public int deleteStoresOfCategory(String catgory_id, String place_parent){
		int affected = 0;
		try{

			open();
			affected = sqlDataBase.delete(CATEGORY_TABLE, KEY_CAT_TEXT_STORE_PLACE_PARENT+" ='"+place_parent+"' AND "+KEY_CAT_TEXT_CATEGORY_ID+"='"+catgory_id+"'", null);
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			close();
		}

		return affected;
	}

	void deleteGeoText(String area)
	{
		try
		{

			open();
			int res=sqlDataBase.delete(TABLE_GEOTEXT_SEARCH, KEY_SEARCH_AREA+" like '"+area+"'", null);
			Log.i("DELETED ", "DELETED  : "+res);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}

	//Delete GeoFav
	void deleteAllGeoFavText(int place_parent)
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_GEOTEXT_FAV_SEARCH, KEY_FAV_PLACE_PARENT+" = "+place_parent, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}

	//Delete GeoFav
	void deleteAllGeoFavText(String area)
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_GEOTEXT_FAV_SEARCH, KEY_SEARCH_AREA+" like '"+area+"'", null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}

	//Delete GeoFav
	void deleteAllGeoFavText()
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_GEOTEXT_FAV_SEARCH,null,null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}


	public long getGeoRowCount()
	{
		/*long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME;
		Cursor  cursor = sqlDataBase.rawQuery(query,null);*/

		/*cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();*/
		long rowCount=0;
		try
		{
			open();
			rowCount=DatabaseUtils.queryNumEntries(sqlDataBase, TABLE_GEOTEXT_SEARCH);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}
		return rowCount ;
	}


	public void updateGeoFavById(int id,String isFav)
	{
		try
		{
			ContentValues cv=new ContentValues();
			cv.put(KEY_SEARCH_FAVOURITED, isFav);
			int l=sqlDataBase.update(TABLE_GEOTEXT_SEARCH, cv, KEY_SEARCH_ID + "=" + id, null);
			Log.i("Updated","Updated "+l+" STORE ID "+id+" isFAV "+isFav);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	//******************************** GEO-TEXT-FAV *****************************************//


	/*
	 * Getting all Store name from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoStoreName()
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_STORE_NAME+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_STORE_NAME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	/*
	 * Getting all Store name from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoStoreName(int place_parent)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_STORE_NAME+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_STORE_NAME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	public ArrayList<String> getAllFavGeoStoreDESC()
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_DESCRIPTION+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_DESCRIPTION)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	public ArrayList<String> getAllFavGeoStoreDESC(int place_parent)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_DESCRIPTION+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_DESCRIPTION)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	/*
	 * Get all Fav Id's Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoStoreId()
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_SEARCH_ID+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_SEARCH_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}
	/*
	 * Get all Fav Id's Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoStoreId(int place_parent)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_SEARCH_ID+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_SEARCH_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}

	/*
	 * Getting all Mobile No's name from Geo-Text Fav table
	 */

	public ArrayList<String> getGeoFavAllMobileNos()
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_MOB_NO1+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_MOB_NO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}
	public ArrayList<String> getGeoFavAllMobileNos(int place_parent)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_MOB_NO1+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_MOB_NO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}

	/*
	 * Get Fav by id from Geo-Text table
	 */

	public String getGeoFav_Favorite(int store_id)
	{
		String fav = "";
		try
		{

			open();



			String query="SELECT "+KEY_FAV_FAVOURITED+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				fav=cursor.getString(cursor.getColumnIndex(KEY_FAV_FAVOURITED));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return fav;

	}


	/*
	 * Get all City from Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoCity()
	{
		ArrayList<String> city = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_CITY+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				city.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_CITY)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return city;

	}

	/*
	 * Get all City from Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoCity(int place_parent)
	{
		ArrayList<String> city = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_CITY+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				city.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_CITY)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return city;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoPhoto()
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_PHOTO+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_PHOTO)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoPhoto(int place_parent)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_PHOTO+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_PHOTO)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavDistances()
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_DISTANCE+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_DISTANCE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavDistances(int place_parent)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_DISTANCE+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_DISTANCE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}


	/*
	 * Get all Area from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoAreas()
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_AREA+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_AREA)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}
	/*
	 * Get all Area from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoAreas(int place_parent)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_FAV_AREA+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getString(cursor.getColumnIndex(KEY_FAV_AREA)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}

	/*
	 * Delete Fav by id
	 */
	void deleteGeoFavText(int store_id)
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_GEOTEXT_FAV_SEARCH, KEY_FAV_SEARCH_ID+"="+store_id, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}

	public void updateGeoFav_FavById(int id,String isFav)
	{
		try
		{
			ContentValues cv=new ContentValues();
			cv.put(KEY_FAV_FAVOURITED, isFav);
			int l=sqlDataBase.update(TABLE_GEOTEXT_FAV_SEARCH, cv, KEY_FAV_SEARCH_ID + "=" + id, null);
			Log.i("Updated","Updated "+l+" STORE ID "+id+" isFAV "+isFav);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}




	public void addMerchantDetails(ArrayList<MerchantDetails> merchantArr){
		for(int i=0;i<merchantArr.size();i++){

			MerchantDetails merchant = merchantArr.get(i);
			ContentValues values = new ContentValues();
			Log.d("==StrorName==", "== "+i+" "+merchant.getStore_name()+" -- "+merchant.getFav());
			values.put(id, merchant.getId());
			values.put(businessId, merchant.getBusinessId());
			values.put(store_name, merchant.getStore_name());
			values.put(building, merchant.getBuilding());
			values.put(street, merchant.getStreet());
			values.put(landmark, merchant.getLandmark());
			values.put(area, merchant.getArea());
			values.put(pincode, merchant.getPincode());
			values.put(city, merchant.getCity());
			values.put(state, merchant.getState());
			values.put(country, merchant.getCountry());
			values.put(latitude, merchant.getLatitude());
			values.put(longitude, merchant.getLongitude());
			values.put(tel_no1, merchant.getTel_no1());
			values.put(tel_no2, merchant.getTel_no2());
			values.put(mob_no1, merchant.getMob_no1());
			values.put(mob_no2, merchant.getMob_no2());
			values.put(fax_no1, merchant.getFax_no1());
			values.put(fax_no2, merchant.getFax_no2());
			values.put(toll_free_no1, merchant.getToll_free_no1());
			values.put(toll_free_no1, merchant.getToll_free_no2());
			values.put(photo, merchant.getPhoto());

			/*Log.i("Image Url:", "imagePath "+"http://stage.phonethics.in/hyperlocal/"+merchant.getPhoto());*/

			values.put(fav, merchant.getFav());


			try {
				sqlDataBase.insert(MERCHANT_TABLE, null, values);
			}
			catch (Exception e)
			{
				Log.e("==Inert DB Error==", e.toString());
				e.printStackTrace();
			}

		}
	}

	public ArrayList<MerchantDetails> getStoreNames(){

		ArrayList<MerchantDetails> storeArray = new ArrayList<MerchantDetails>();
		Cursor cursor;

		try{

			cursor = sqlDataBase.query(
					MERCHANT_TABLE, new String[]{store_name,building,street,landmark,area,pincode,city,state,country,businessId,photo,fav},null,null,null,null,null,null
					);


			cursor.moveToFirst();
			// if there is data available after the cursor's pointer, add
			// it to the ArrayList that will be returned by the method.
			if (!cursor.isAfterLast()){
				do{

					/*storeArray.add(cursor.getString(0));*/

					MerchantDetails merchant = new MerchantDetails();
					merchant.setStore_name(cursor.getString(0));
					merchant.setBuilding(cursor.getString(1));
					merchant.setStreet(cursor.getString(2));
					merchant.setLandmark(cursor.getString(3));
					merchant.setArea(cursor.getString(4));
					merchant.setPincode(cursor.getString(5));
					merchant.setCity(cursor.getString(6));
					merchant.setState(cursor.getString(7));
					merchant.setCountry(cursor.getString(8));
					merchant.setBusinessId(cursor.getString(9));
					merchant.setPhoto(cursor.getString(10));
					merchant.setFav(cursor.getString(11));
					storeArray.add(merchant);

				}while(cursor.moveToNext());
			}


			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return storeArray;
	}


	public ArrayList<Object> getStoreIds(){

		ArrayList<Object> iDArray = new ArrayList<Object>();
		Cursor cursor;

		try{

			cursor = sqlDataBase.query(
					MERCHANT_TABLE, new String[]{businessId},null,null,null,null,null,null
					);


			cursor.moveToFirst();
			// if there is data available after the cursor's pointer, add
			// it to the ArrayList that will be returned by the method.
			if (!cursor.isAfterLast()){
				do{

					iDArray.add(cursor.getString(0));

				}while(cursor.moveToNext());
			}


			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return iDArray;
	}

	public void addFav(String id){
		open();
		ContentValues values = new ContentValues();
		values.put(fav, "1");
		try {
			sqlDataBase.update(MERCHANT_TABLE, values, businessId + "=" + id, null);
			close();
		}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}
	}

	public void removeFav(String id){
		open();
		ContentValues values = new ContentValues();
		values.put(fav, "0");
		try {
			sqlDataBase.update(MERCHANT_TABLE, values, businessId + "=" + id, null);
			close();
		}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}
	}

	public ArrayList<MerchantDetails> getFavStores(){

		ArrayList<MerchantDetails> arr = new ArrayList<MerchantDetails>();
		Cursor cursor;
		String query = "SELECT * FROM " + MERCHANT_TABLE + " WHERE " + fav+"=1";
		try{
			cursor = sqlDataBase.rawQuery(query, null);
			/*cursor.moveToFirst();*/
			int iStore_name = cursor.getColumnIndex(store_name);
			int iBuilding	= cursor.getColumnIndex(building);
			int iStreet = cursor.getColumnIndex(street);
			int iLandmark = cursor.getColumnIndex(landmark);
			int iArea		= cursor.getColumnIndex(area);
			int iPincode = cursor.getColumnIndex(pincode);
			int iCity	= cursor.getColumnIndex(city);
			int iState = cursor.getColumnIndex(state);
			int iCountry = cursor.getColumnIndex(country);
			int iBusinessId = cursor.getColumnIndex(businessId);
			int iPhoto=cursor.getColumnIndex(photo);
			int iFav = cursor.getColumnIndex(fav);

			// if there is data available after the cursor's pointer, add
			// it to the ArrayList that will be returned by the method.
			/*	if (!cursor.isAfterLast()){
			do{

				//storeArray.add(cursor.getString(0));


				MerchantDetails merchant = new MerchantDetails();
				merchant.setStore_name(cursor.getString(0));
				merchant.setBuilding(cursor.getString(1));
				merchant.setStreet(cursor.getString(2));
				merchant.setLandmark(cursor.getString(3));
				merchant.setArea(cursor.getString(4));
				merchant.setPincode(cursor.getString(5));
				merchant.setCity(cursor.getString(6));
				merchant.setState(cursor.getString(7));
				merchant.setCountry(cursor.getString(8));

				arr.add(merchant);

			}while(cursor.moveToNext());
		}*/


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				MerchantDetails merchant = new MerchantDetails();
				merchant.setStore_name(cursor.getString(iStore_name));
				merchant.setBuilding(cursor.getString(iBuilding));
				merchant.setStreet(cursor.getString(iStreet));
				merchant.setLandmark(cursor.getString(iLandmark));
				merchant.setArea(cursor.getString(iArea));
				merchant.setPincode(cursor.getString(iPincode));
				merchant.setCity(cursor.getString(iCity));
				merchant.setState(cursor.getString(iState));
				merchant.setCountry(cursor.getString(iCountry));
				merchant.setBusinessId(cursor.getString(iBusinessId));
				merchant.setPhoto(cursor.getString(iPhoto));
				merchant.setFav(cursor.getString(iFav));
				arr.add(merchant);
			}

			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}

		return arr;
	}


	public ArrayList<String> getStoresId1(){
		ArrayList<String> idArr = new ArrayList<String>();
		Cursor cursor;
		String query = "SELECT BUSINESS_ID FROM " + MERCHANT_TABLE;
		String queryFav = "SELECT BUSINESS_ID FROM " + MERCHANT_TABLE + " WHERE " + fav+"=1";

		try{

			cursor = sqlDataBase.rawQuery(query, null);
			int iBus_ids = cursor.getColumnIndex(businessId);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				idArr.add(cursor.getString(iBus_ids));
			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}

		return idArr;
	}

	public ArrayList<String> getStoresFav(){
		ArrayList<String> idArr = new ArrayList<String>();
		Cursor cursor;

		String query = "SELECT BUSINESS_ID FROM " + MERCHANT_TABLE + " WHERE " + fav+"=1";

		try{

			cursor = sqlDataBase.rawQuery(query, null);
			int iBus_ids = cursor.getColumnIndex(businessId);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				idArr.add(cursor.getString(iBus_ids));
			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}

		return idArr;
	}


	public long getCount(){ 
		open();
		long rowCount = DatabaseUtils.queryNumEntries(sqlDataBase, MERCHANT_TABLE);
		Log.d("======== 1", "cursor count "+ rowCount);
		close();
		return rowCount ;

	}

	public String getId(int start,int offset){
		open();
		String id="";
		String query = "SELECT "+businessId+" FROM "+MERCHANT_TABLE+" LIMIT "+ start+","+offset;
		Cursor cursor;
		try{
			cursor = sqlDataBase.rawQuery(query, null);
			if(cursor!=null){
				cursor.moveToFirst();
			}
			int iBus_ids = cursor.getColumnIndex(businessId);
			id = cursor.getString(iBus_ids);
			Log.d("===", "id === "+id);
			close();
		}catch(Exception ex){
			Log.e("DB ERROR", ex.toString());
			ex.printStackTrace();
		}
		return id;
	}

	//===================================================== New Functions ==========================================//


	/**
	 * 
	 * Add the store information of the stores with their category [Directory/shopping/dine/entertainment] id
	 * 
	 * @param mArrStoreDetails Store information array list
	 * @param mSCategory category id
	 * @return
	 */
	public long createStoreTable(ArrayList<StoreInfo> mArrStoreDetails,String mSCategory){
		long entiresAffected=0;
		try{


			open();
			sqlDataBase.beginTransaction();

			for(int i=0;i<mArrStoreDetails.size();i++){

				ContentValues cv = new ContentValues();

				cv.put(Tables.STORE_ID, mArrStoreDetails.get(i).getId());
				cv.put(Tables.STORE_NAME, mArrStoreDetails.get(i).getName());
				cv.put(Tables.STORE_MALL_ID, mArrStoreDetails.get(i).getMall_id());
				cv.put(Tables.STORE_PLACEPARENT, mArrStoreDetails.get(i).getPlace_parent());
				cv.put(Tables.STORE_LOGO , mArrStoreDetails.get(i).getImage_url());
				cv.put(Tables.STORE_DESC, mArrStoreDetails.get(i).getDescription());
				cv.put(Tables.STORE_CITY, mArrStoreDetails.get(i).getCity());
				cv.put(Tables.STORE_BUILDING, mArrStoreDetails.get(i).getBuilding());
				cv.put(Tables.STORE_STREET, mArrStoreDetails.get(i).getStreet());
				cv.put(Tables.STORE_LANDMARK, mArrStoreDetails.get(i).getLandmark());
				cv.put(Tables.STORE_AREA, mArrStoreDetails.get(i).getArea());
				cv.put(Tables.STORE_MOB_NO_1, mArrStoreDetails.get(i).getMob_no1());
				cv.put(Tables.STORE_MOB_NO_2, mArrStoreDetails.get(i).getMob_no2());
				cv.put(Tables.STORE_MOB_NO_3, mArrStoreDetails.get(i).getMob_no3());
				cv.put(Tables.STORE_LAND_NO_1, mArrStoreDetails.get(i).getTel_no1());
				cv.put(Tables.STORE_LAND_NO_2, mArrStoreDetails.get(i).getTel_no2());
				cv.put(Tables.STORE_LAND_NO_3, mArrStoreDetails.get(i).getTel_no3());
				cv.put(Tables.STORE_TOLL_FREE_NO_1, mArrStoreDetails.get(i).getToll_free_no1());
				cv.put(Tables.STORE_TOLL_FREE_NO_2, mArrStoreDetails.get(i).getToll_free_no2());
				cv.put(Tables.STORE_WEBSITE, mArrStoreDetails.get(i).getWebsite());
				cv.put(Tables.STORE_EMAIL, mArrStoreDetails.get(i).getEmail());
				cv.put(Tables.STORE_TOTAL_LIKE, mArrStoreDetails.get(i).getTotal_like());
				cv.put(Tables.STORE_TOTAL_RATING, mArrStoreDetails.get(i).getTotal_rating());
				cv.put(Tables.STORE_HAS_OFFER, mArrStoreDetails.get(i).getHas_offer());
				cv.put(Tables.STORE_OFFER_TITLE, mArrStoreDetails.get(i).getTitle());
				cv.put(Tables.STORE_CATEGORY, mSCategory);
				cv.put(Tables.STORE_SUB_CATEGORY, mArrStoreDetails.get(i).getCategory());

				entiresAffected = sqlDataBase.insert(Tables.TABLE_STORES, null, cv);


			}
			//setTransactionSuccessful();
			sqlDataBase.setTransactionSuccessful();


		}catch(Exception ex){

		}finally{
			sqlDataBase.endTransaction();
			close();
		}
		InorbitLog.d("Entries affected "+entiresAffected);
		return entiresAffected;


	}

	void deleteStrores(String msStoreCategory, String msPlaceParent){
		try{

			if(sqlDataBase!=null && !sqlDataBase.isOpen()){
				open();	
			}
			String msWhere = Tables.STORE_CATEGORY+"='"+msStoreCategory+"' AND "+Tables.STORE_PLACEPARENT+"='"+msPlaceParent+"'";	
			int entriesAffected = sqlDataBase.delete(Tables.TABLE_STORES,msWhere,null);
			InorbitLog.d("Deleted Entries "+entriesAffected);
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			if (sqlDataBase != null && sqlDataBase.isOpen()) {
				sqlDataBase.close();
			}
		}

	}



	/**
	 * 
	 * Get the total stores count for the present category [Directory/Dine/...]  
	 * 
	 * @param msCategory  category id like [Directory/Dine/...]  
	 * @param msPlaceParent Place parent of current active mall [for Malad = 1, Vashi = 2...]
	 * @return Integer count of total stores
	 */
	public Integer getTotalStores(String msCategory,String msPlaceParent){
		long total_records = 0;
		try{

			if(sqlDataBase!=null && !sqlDataBase.isOpen()){
				open();	
			}

			String query="SELECT COUNT("+Tables.STORE_ID+") AS ROW_COUNT FROM "+Tables.TABLE_STORES + 
					" WHERE "+Tables.STORE_CATEGORY +"='"+msCategory+"' AND "+Tables.STORE_PLACEPARENT+"='"+msPlaceParent+"'";
			InorbitLog.d("Query "+query);
			Cursor  cursor = sqlDataBase.rawQuery(query, null);
			cursor.moveToFirst();
			total_records=cursor.getLong(cursor.getColumnIndex("ROW_COUNT"));
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			if (sqlDataBase != null && sqlDataBase.isOpen()) {
				sqlDataBase.close();
			}

		}
		return Integer.parseInt(total_records+"");

	}

	/**
	 * 
	 * Get the store infomration based on the category id and mall id
	 * 
	 * @param msStoreCategory Category id [Directory, Shopping,Entertainmet...] 
	 * @param msPlaceParent Place parent id of select/active mall
	 * @return ArrayList of StoreInfo
	 */
	public ArrayList<StoreInfo> getStores(String msStoreCategory,String msPlaceParent){
		ArrayList<StoreInfo> mArrStores = new ArrayList<StoreInfo>();
		try{
			if(sqlDataBase!=null && !sqlDataBase.isOpen()){
				open();
			}
			String query = "SELECT * FROM "+Tables.TABLE_STORES+
					" WHERE "+Tables.STORE_CATEGORY+"='"+msStoreCategory+"' AND "+Tables.STORE_PLACEPARENT+"='"+msPlaceParent+"'";
			InorbitLog.d("Query "+query);
			Cursor  cursor = sqlDataBase.rawQuery(query, null);
			InorbitLog.d("Cursor size "+cursor.getCount());
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				StoreInfo store = new StoreInfo();

				store.setArea(cursor.getString(cursor.getColumnIndex(Tables.STORE_AREA)));
				store.setCity(cursor.getString(cursor.getColumnIndex(Tables.STORE_CITY)));
				store.setDescription(cursor.getString(cursor.getColumnIndex(Tables.STORE_DESC)));
				store.setEmail(cursor.getString(cursor.getColumnIndex(Tables.STORE_EMAIL)));
				store.setHas_offer(cursor.getString(cursor.getColumnIndex(Tables.STORE_HAS_OFFER)));
				store.setId(cursor.getString(cursor.getColumnIndex(Tables.STORE_ID)));
				store.setImage_url(cursor.getString(cursor.getColumnIndex(Tables.STORE_LOGO)));
				store.setLandmark(cursor.getString(cursor.getColumnIndex(Tables.STORE_LANDMARK)));
				store.setMall_id(cursor.getString(cursor.getColumnIndex(Tables.STORE_MALL_ID)));
				store.setMob_no1(cursor.getString(cursor.getColumnIndex(Tables.STORE_MOB_NO_1)));
				store.setMob_no2(cursor.getString(cursor.getColumnIndex(Tables.STORE_MOB_NO_2)));
				store.setMob_no3(cursor.getString(cursor.getColumnIndex(Tables.STORE_MOB_NO_3)));
				store.setTel_no1(cursor.getString(cursor.getColumnIndex(Tables.STORE_LAND_NO_1)));
				store.setTel_no2(cursor.getString(cursor.getColumnIndex(Tables.STORE_LAND_NO_2)));
				store.setTel_no3(cursor.getString(cursor.getColumnIndex(Tables.STORE_LAND_NO_3)));
				store.setToll_free_no1(cursor.getString(cursor.getColumnIndex(Tables.STORE_TOLL_FREE_NO_1)));
				store.setToll_free_no2(cursor.getString(cursor.getColumnIndex(Tables.STORE_TOLL_FREE_NO_2)));
				store.setName(cursor.getString(cursor.getColumnIndex(Tables.STORE_NAME)));
				store.setPlace_parent(cursor.getString(cursor.getColumnIndex(Tables.STORE_PLACEPARENT)));
				store.setStreet(cursor.getString(cursor.getColumnIndex(Tables.STORE_STREET)));
				store.setTitle(cursor.getString(cursor.getColumnIndex(Tables.STORE_OFFER_TITLE)));
				store.setTotal_like(cursor.getString(cursor.getColumnIndex(Tables.STORE_TOTAL_LIKE)));
				store.setTotal_rating(cursor.getString(cursor.getColumnIndex(Tables.STORE_TOTAL_RATING)));
				store.setWebsite(cursor.getString(cursor.getColumnIndex(Tables.STORE_WEBSITE)));
				store.setCategory(cursor.getString(cursor.getColumnIndex(Tables.STORE_SUB_CATEGORY)));

				mArrStores.add(store);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if (sqlDataBase != null && sqlDataBase.isOpen()) {
				sqlDataBase.close();
			}
		}
		return mArrStores;
	}

	//==============================================                    ==========================================//



	public void deleteAreasTable()
	{

		try
		{
			open();
			sqlDataBase.delete(TABLE_AREAS, null, null);
			close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	/*
	 *  Queries for Area Table ======================================
	 */

	public long createArea(String id, String name, String latitude, String longitude, String pincode, String city, String country, String countryCode, String isoCode, String placeId,String locality,String state,String fbUrl){

		Log.d("GETINAREA","GETINAREA" + name);

		ContentValues cv=new ContentValues();
		cv.put(mall_id, id);
		cv.put(area_name, name);
		cv.put(area_latitude, latitude);
		cv.put(area_longitude, longitude);
		cv.put(area_pincode, pincode);
		cv.put(area_city, city);
		cv.put(area_country, country);
		cv.put(area_country_code, countryCode);
		cv.put(area_iso_code, isoCode);
		cv.put(place_parent, placeId);
		cv.put(area_locality, locality);
		cv.put(area_state, state);
		cv.put(fb_url, fbUrl);

		Log.d("", "Inorbit Fb >> "+fbUrl);

		return sqlDataBase.insert(TABLE_AREAS, null, cv);
	}


	/**
	 * Get the mall name from its id
	 * 
	 * @param areaId mall id 
	 * @return Mall Name
	 */
	public String getAreaNameByInorbitId(String areaId)
	{
		String addr  = "";

		String query="SELECT "+area_name+" FROM "+TABLE_AREAS +" WHERE "+ mall_id+" = '"+ areaId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(area_name));

		}

		cursor.close();
		close();
		return addr;
	}


	/*	public String getShopNameByPlaceID(String placeId)
	{
		String addr  = "";

		String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_AREAS +" WHERE "+ KEY_SEARCH_PLACE_PARENT+" = '"+ placeId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(KEY_SEARCH_STORE_NAME));

		}

		cursor.close();
		close();
		return addr;
	}*/



	public String getParentPlaceIdByAreaName(String areaName)
	{
		String addr  = "";

		String query="SELECT "+place_parent+" FROM "+TABLE_AREAS +" WHERE "+ area_name+" = '"+ areaName+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(place_parent));

		}

		cursor.close();
		close();
		return addr;
	}

	/**
	 * Get all malls title
	 * 
	 * 
	 * @return String array list of malls location
	 */
	public ArrayList<String> getAllAreas()
	{
		ArrayList<String>  allAreas =new ArrayList<String>();

		String query="SELECT "+area_name+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreas.add(cursor.getString(cursor.getColumnIndex(area_name)));

		}

		cursor.close();
		close();
		return allAreas;
	}


	public ArrayList<String> getAllMallCitys()
	{
		ArrayList<String>  allAreas =new ArrayList<String>();

		String query="SELECT "+area_city+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreas.add(cursor.getString(cursor.getColumnIndex(area_city)));

		}

		cursor.close();
		close();
		return allAreas;
	}

	/**
	 * Get tha mall ids
	 * 
	 * @return arrays list of mall ids
	 */
	public ArrayList<String> getAllMallIds()
	{
		ArrayList<String>  allAreasIds =new ArrayList<String>();

		String query="SELECT "+mall_id+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIds.add(cursor.getString(cursor.getColumnIndex(mall_id)));

		}

		cursor.close();
		close();
		return allAreasIds;
	}


	public ArrayList<String> getAllPlaceParentIds()
	{
		ArrayList<String>  allAreasIds =new ArrayList<String>();

		String query="SELECT "+place_parent+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIds.add(cursor.getString(cursor.getColumnIndex(place_parent)));

		}

		cursor.close();
		close();
		return allAreasIds;
	}

	/**
	 * 
	 * It will return the id of the mall which user is currently browsing or kept as active mall
	 * 
	 * @return id of the currently active mall location
	 */
	public String getActiveMallId(){
		open();
		String id ="";
		String query = "SELECT "+mall_id+" FROM "+TABLE_AREAS+" WHERE "+active_area+"='1'";
		Cursor cursor;
		try{
			cursor = sqlDataBase.rawQuery(query, null);
			if(cursor!=null){
				if (cursor.moveToFirst()) {
					int iArea_PlaceId = cursor.getColumnIndex(mall_id);
					id = cursor.getString(iArea_PlaceId);
				}
			}
			close();
		}catch(Exception ex){
			Log.e("DB ERROR", ex.toString());
			ex.printStackTrace();
		}
		Log.d("", "Inorbit active area id "+id);
		return id;
	}


	/**
	 * Set the active mall with this mall id
	 * 
	 * @param mallID
	 */
	public void setActiveMall(String mallID){
		open();

		ContentValues values_active = new ContentValues();
		values_active.put(active_area, "1");
		ContentValues values_inactive = new ContentValues();
		values_inactive.put(active_area, "0");
		try {
			sqlDataBase.update(TABLE_AREAS, values_active, mall_id + "=" + mallID, null);
			sqlDataBase.update(TABLE_AREAS, values_inactive, mall_id + "!=" + mallID, null);
			close();
		}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}

		Log.d("", "Inorbit Set Active Area " + mallID);

	}

	public ArrayList<String> getAllInorbitPlaceParentIds()
	{
		ArrayList<String>  allAreasIds =new ArrayList<String>();

		String query="SELECT "+place_parent+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIds.add(cursor.getString(cursor.getColumnIndex(place_parent)));

		}

		cursor.close();
		close();
		return allAreasIds;
	}

	public long getAreaRowCount()
	{
		/*long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME;
		Cursor  cursor = sqlDataBase.rawQuery(query,null);*/

		/*cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();*/
		long rowCount=0;
		try
		{
			open();
			rowCount=DatabaseUtils.queryNumEntries(sqlDataBase, TABLE_AREAS);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}
		return rowCount ;
	}


	public String getParentPlaceIdByPos(String areaId) {
		String addr  = "";

		String query="SELECT "+place_parent+" FROM "+TABLE_AREAS +" WHERE "+ mall_id+" = '"+ areaId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(place_parent));

		}

		cursor.close();
		close();
		return addr;
	}


	public String getMallIdByPos(String areaId){
		String addr  = "";

		String query="SELECT "+mall_id+" FROM "+TABLE_AREAS +" WHERE "+ mall_id+" = '"+ areaId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(mall_id));
		}

		cursor.close();
		close();
		return addr;
	}

	/**
	 * Get the mall id by its place id
	 * 
	 * 
	 * @param mallPlaceParentId : Place parent id of the mall 
	 * @return mall id : mall id of that particular mall
	 */
	public String getMallIdByPlaceParent(String mallPlaceParentId) {
		String addr  = "";

		String query="SELECT "+mall_id+" FROM "+TABLE_AREAS +" WHERE "+ place_parent+" = '"+ mallPlaceParentId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(mall_id));

		}

		cursor.close();
		close();
		return addr;
	}

	public String getPlaceIdByPlaceParent(String place_parent1){
		String addr  = "";

		String query="SELECT "+KEY_SHOP_ID+" FROM "+TABLE_PLACE_CHOOSER +" WHERE "+ KEY_SHOP_PARENT+" = '"+ place_parent1+"'";
		
		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(KEY_SHOP_ID));

		}

		cursor.close();
		close();
		return addr;
	}

	/**
	 * Get the mall name 
	 * 
	 * @param mallid
	 * @return Mall's Name
	 */
	public String getMallNameByMallID(String mallId){
		String addr  = "";

		String query="SELECT "+area_name+" FROM "+TABLE_AREAS +" WHERE "+ mall_id+" = '"+ mallId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(area_name));

		}

		cursor.close();
		close();
		return addr;
	}


	/**
	 * Get the place parent of the mall location
	 * 
	 * @param mallid
	 * @return Mall's place parent
	 */
	public String getMallPlaceParentByMallID(String mallid){
		String addr  = "";

		String query="SELECT "+place_parent+" FROM "+TABLE_AREAS +" WHERE "+ mall_id+" = '"+ mallid+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(place_parent));

		}

		cursor.close();
		close();
		return addr;
	}


	/**
	 * Get the mall location [Malad|Vashi|Cyberabad...] 
	 * 
	 * 
	 * @param mallID Selected mall position or mall id.
	 * @return Strign Mall location
	 */
	public String getMallCity(String mallID){
		String addr  = "";

		String query="SELECT "+area_city+" FROM "+TABLE_AREAS +" WHERE "+ mall_id+" = '"+ mallID+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(area_city));

		}

		cursor.close();
		close();
		return addr;
	}

	public String getMallPinCode(String areaPlaceId){
		String addr  = "";

		String query="SELECT "+area_pincode+" FROM "+TABLE_AREAS +" WHERE "+ mall_id+" = '"+ areaPlaceId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(area_pincode));

		}

		cursor.close();
		close();
		return addr;
	}




	public String getFbUrlByPos(String areaId){
		String addr  = "";

		String query="SELECT "+fb_url+" FROM "+TABLE_AREAS +" WHERE "+ mall_id+" = '"+ areaId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(fb_url));

		}

		cursor.close();
		close();
		return addr;
	}

	/*public String getTwitterUrlByPos(String areaId){
		String addr  = "";

		String query="SELECT "++" FROM "+TABLE_AREAS +" WHERE "+ mall_id+" = '"+ areaId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(fb_url));

		}

		cursor.close();
		close();
		return addr;
	}*/

	public String getFbUrlFromParentStore(String areaId){
		String addr  = "";

		String query="SELECT "+KEY_PARENT_STORE_FACEBOOK_URL+" FROM "+TABLE_PARENT_STORE +" WHERE "+ mall_id+" = '"+ areaId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_FACEBOOK_URL));

		}

		cursor.close();
		close();
		return addr;
	}

	public String getTwitterFromParentStore(String areaId){
		String addr  = "";

		String query="SELECT "+KEY_PARENT_STORE_TWITTER_URL+" FROM "+TABLE_PARENT_STORE +" WHERE "+ mall_id+" = '"+ areaId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_TWITTER_URL));

		}

		cursor.close();
		close();
		return addr;
	}

	/**
	 * 
	 * Get the total number of the stores of logged in merchant/manager
	 * 
	 * @return long: Number of stores
	 */
	public long getStoresRowCount(){

		long rowCount=0;
		try{
			open();
			rowCount=DatabaseUtils.queryNumEntries(sqlDataBase, TABLE_PLACE_CHOOSER);
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{

			close();
		}
		return rowCount ;
	}

	//	public ArrayList<ParticularStoreInfo> getSubCategoryStoresCount() {
	//
	//		ArrayList<ParticularStoreInfo> mArrStores = new ArrayList<ParticularStoreInfo>();
	//		ParticularStoreInfo storeInfo;
	//		try {
	//
	//			open();
	//			String query = "SELECT  * FROM " + TABLE_PLACE_CHOOSER + " WHERE " + KEY_SHOP_SUB_CATEGORY + " ='Dine'";
	//			Cursor  cursor = sqlDataBase.rawQuery(query,null);
	//			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
	//			{
	//				storeInfo = new ParticularStoreInfo();
	//				storeInfo.setId(cursor.getString(cursor.getColumnIndex(KEY_SHOP_ID)));
	//				storeInfo.setName(cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME)));
	//				storeInfo.setImage_url(cursor.getString(cursor.getColumnIndex(KEY_SHOP_image_url)));
	//				storeInfo.setDescription(cursor.getString(cursor.getColumnIndex(KEY_SHOP_DESCRIPTION)));
	//				storeInfo.setMall_id(cursor.getString(cursor.getColumnIndex(KEY_SHOP_MALL_ID)));
	//				storeInfo.setPlace_status(cursor.getString(cursor.getColumnIndex(KEY_SHOP_place_status)));
	//				storeInfo.setPlace_parent(cursor.getString(cursor.getColumnIndex(KEY_SHOP_PARENT)));
	//				mArrStores.add(storeInfo);
	//			}
	//			cursor.close();
	//		}catch(SQLException sqx){
	//			sqx.printStackTrace();
	//		}catch(Exception ex){
	//			ex.printStackTrace();
	//		}
	//		finally
	//		{
	//			close();
	//		}
	//		return mArrStores;
	//
	//	}
	
	public long getSubCategoryStoresCount(String category) {
		String query = "SELECT COUNT("+KEY_SHOP_SUB_CATEGORY+") AS ROW_COUNT FROM "+TABLE_PLACE_CHOOSER +" WHERE "+ KEY_SHOP_SUB_CATEGORY+" LIKE '%"+category+"%'";
		long rowcount=0;
		try {
			open();
			Cursor c=sqlDataBase.rawQuery(query, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			close();
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			close();
		}
		return rowcount;
	}
	
	public ArrayList<ParticularStoreInfo> getSubCategoryStores(String category) {
		String query = "SELECT * FROM " + TABLE_PLACE_CHOOSER + "  WHERE "+ KEY_SHOP_SUB_CATEGORY+" LIKE '%"+category+"%'";
		ArrayList<ParticularStoreInfo> all_subCat_stores = new ArrayList<ParticularStoreInfo>();
		try {
			open();
			Cursor cursor = sqlDataBase.rawQuery(query, null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()) {
				ParticularStoreInfo store = new ParticularStoreInfo();
				store.setId(cursor.getString(cursor.getColumnIndex(KEY_SHOP_ID)));
				store.setArea(cursor.getString(cursor.getColumnIndex(KEY_SHOP_AREA)));
				store.setName(cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME)));
				store.setDescription(cursor.getString(cursor.getColumnIndex(KEY_SHOP_DESCRIPTION)));
				store.setPlace_parent(cursor.getString(cursor.getColumnIndex(KEY_SHOP_PARENT)));
				store.setImage_url(cursor.getString(cursor.getColumnIndex(KEY_SHOP_image_url)));
				all_subCat_stores.add(store);
			}
			cursor.close();
		} catch(SQLException sqx) {
			sqx.printStackTrace();
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			close();
		}
		return all_subCat_stores;
	}

	public ArrayList<String> getAllStoresNames(){
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_SHOP_NAME+" FROM "+TABLE_PLACE_CHOOSER+" ORDER BY "+KEY_SHOP_place_status;;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}


	//	Place Desc
	public ArrayList<String> getAllStoreDesc()
	{
		ArrayList<String> desc = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_SHOP_DESCRIPTION+" FROM "+TABLE_PLACE_CHOOSER+" ORDER BY "+KEY_SHOP_place_status;;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				desc.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_DESCRIPTION)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return desc;

	}

	//	Place ID
	public ArrayList<String> getAllStoreIDs()
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();
			String query="SELECT "+KEY_SHOP_ID+" FROM "+TABLE_PLACE_CHOOSER+" ORDER BY "+KEY_SHOP_place_status;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{

				ids.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}

	/**
	 * Get the store details when only one record is there in store table
	 * 
	 * 
	 * @return ParticularStoreInfo 
	 */
	public ParticularStoreInfo getStoreInfo(){
		ParticularStoreInfo storeInfo = new ParticularStoreInfo();
		try{
			open();
			String query = "SELECT * FROM "+TABLE_PLACE_CHOOSER;
			Cursor  cursor = sqlDataBase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				String id = cursor.getString(cursor.getColumnIndex(KEY_SHOP_ID));
				String name = cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME));
				String image = cursor.getString(cursor.getColumnIndex(KEY_SHOP_image_url));
				String desc = cursor.getString(cursor.getColumnIndex(KEY_SHOP_DESCRIPTION));
				String mallId = cursor.getString(cursor.getColumnIndex(KEY_SHOP_MALL_ID));
				String placeStatus = cursor.getString(cursor.getColumnIndex(KEY_SHOP_place_status));
				String placeParent = cursor.getString(cursor.getColumnIndex(KEY_SHOP_PARENT));

				storeInfo.setId(id);
				storeInfo.setName(name);
				storeInfo.setImage_url(image);
				storeInfo.setDescription(desc);
				storeInfo.setMall_id(mallId);
				storeInfo.setPlace_status(placeStatus);
				storeInfo.setPlace_parent(placeParent);
			}
			cursor.close();

		}catch(SQLException sqx){
			sqx.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			close();
		}
		return storeInfo;
	}

	public ArrayList<ParticularStoreInfo> getStoreList() {
		ArrayList<ParticularStoreInfo> storeInfos = new ArrayList<ParticularStoreInfo>();
		ParticularStoreInfo storeInfo;
		try{
			open();
			String query = "SELECT * FROM "+TABLE_PLACE_CHOOSER;
			Cursor  cursor = sqlDataBase.rawQuery(query,null);
			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

				storeInfo = new ParticularStoreInfo();
				storeInfo.setId(cursor.getString(cursor.getColumnIndex(KEY_SHOP_ID)));
				storeInfo.setName(cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME)));
				storeInfo.setImage_url(cursor.getString(cursor.getColumnIndex(KEY_SHOP_image_url)));
				storeInfo.setDescription(cursor.getString(cursor.getColumnIndex(KEY_SHOP_DESCRIPTION)));
				storeInfo.setMall_id(cursor.getString(cursor.getColumnIndex(KEY_SHOP_MALL_ID)));
				storeInfo.setPlace_status(cursor.getString(cursor.getColumnIndex(KEY_SHOP_place_status)));
				storeInfo.setPlace_parent(cursor.getString(cursor.getColumnIndex(KEY_SHOP_PARENT)));

				storeInfos.add(storeInfo);
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			close();
		}
		return storeInfos;
	}

	public ArrayList<String> getAllShopsMallIds()
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{
			open();
			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_SHOP_MALL_ID+" FROM "+TABLE_PLACE_CHOOSER+" ORDER BY "+KEY_SHOP_place_status;;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_MALL_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}

	public ArrayList<String> getAllShopsPlaceParent()
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_SHOP_PARENT+" FROM "+TABLE_PLACE_CHOOSER+" ORDER BY "+KEY_SHOP_place_status;;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_PARENT)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}


	public ArrayList<String> getAllStoresPlaceStatus()
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_SHOP_place_status+" FROM "+TABLE_PLACE_CHOOSER+" ORDER BY "+KEY_SHOP_place_status;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_place_status)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}

	//	Place Photourl
	public ArrayList<String> getAllStorePhotoUrl()
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_SHOP_image_url+" FROM "+TABLE_PLACE_CHOOSER+" ORDER BY "+KEY_SHOP_place_status;;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_image_url)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}



	public void deletePlacesTable() {
		try
		{
			open();
			sqlDataBase.delete(TABLE_PLACE_CHOOSER, null, null);
			close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public long create_place_chooser(String ID, String SOTRE_PARENT,
			String SOTRE_NAME, String STORE_DESC, String BUILDING, String STREET,
			String LANDMARK, String AREA, String PINCODE, String CITY,
			String STATE, String COUNTRY, String LATITUDE, String LONGITUDE,
			String TELNO1, String TELNO2, String MOBILE_NO, String MOBNO2,
			String PHOTO, String EMAIL, String WEBSITE,String MALL_ID,String FACEBOOK,String TWITTER,String PLACE_STATUS, String SUB_CATEGORY) {
		// TODO Auto-generated method stub
		ContentValues cv=new ContentValues();


		try{

			Log.d("", "Inorbit create place >> "+ ID + "--"+
					SOTRE_PARENT + "--"+
					SOTRE_NAME + "--"+
					STORE_DESC + "--"+
					BUILDING + "--"+
					STREET + "--"+
					LANDMARK + "--"+
					AREA + "--"+
					PINCODE + "--"+
					CITY + "--"+
					STATE + "--"+
					COUNTRY + "--"+
					LATITUDE + "--"+
					LONGITUDE + "--"+
					TELNO1 + "--"+
					TELNO2 + "--"+
					MOBILE_NO + "--"+
					MOBNO2 + "--"+
					PHOTO + "--"+
					EMAIL + "--"+
					WEBSITE + "--"+
					MALL_ID + "--"+
					FACEBOOK + "--"+
					TWITTER + "--"+
					PLACE_STATUS + "--"+
					SUB_CATEGORY + "--"
					);


			cv.put(KEY_SHOP_ID, ID);
			cv.put(KEY_SHOP_PARENT, SOTRE_PARENT);
			cv.put(KEY_SHOP_NAME, SOTRE_NAME);
			cv.put(KEY_SHOP_DESCRIPTION, STORE_DESC);
			cv.put(KEY_SHOP_BUILDING, BUILDING);
			cv.put(KEY_SHOP_STREET, STREET);
			cv.put(KEY_SHOP_LANDMARK, LANDMARK);
			cv.put(KEY_SHOP_AREA, AREA);
			cv.put(KEY_SHOP_LATITUDE, LATITUDE);
			cv.put(KEY_SHOP_LONGITUDE, LONGITUDE);
			cv.put(KEY_SHOP_TELLNO1, TELNO1);
			cv.put(KEY_SHOP_TELLNO2, TELNO2);
			cv.put(KEY_SHOP_MOBNO1, MOBILE_NO);
			cv.put(KEY_SHOP_MOBNO2, MOBNO2);
			cv.put(KEY_SHOP_image_url, PHOTO);
			cv.put(KEY_SHOP_email, EMAIL);
			cv.put(KEY_SHOP_website, WEBSITE);
			cv.put(KEY_SHOP_MALL_ID, MALL_ID);
			cv.put(KEY_SHOP_facebook, FACEBOOK);
			cv.put(KEY_SHOP_twitter, TWITTER);
			cv.put(KEY_SHOP_place_status, PLACE_STATUS);
			cv.put(KEY_SHOP_SUB_CATEGORY, SUB_CATEGORY);

		}catch(Exception ex){
			ex.printStackTrace();
		}

		return sqlDataBase.insert(TABLE_PLACE_CHOOSER, null, cv);



	}

	public void beginTransaction(){
		sqlDataBase.beginTransaction();
	}

	public void setTransactionSuccessful(){
		sqlDataBase.setTransactionSuccessful();
	}

	public void endTransaction(){
		sqlDataBase.endTransaction();
	}



	/**
	 * Check whether mall manager is logged in or not.
	 * This check happend based on the condition if saved stores contains place parent = 0;
	 * 
	 * @return
	 */
	public boolean isManager(){
		boolean managerLoggedIn = false;
		String id="";
		ArrayList<String> arrPlaceParent = new ArrayList<String>();
		String query="SELECT "+KEY_SHOP_PARENT+" FROM "+TABLE_PLACE_CHOOSER +" WHERE "+ KEY_SHOP_PARENT + "='0'";
		
		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		//Toast.makeText(ourContext, "isManagerStoreCount "+cursor.getCount(), 0).show();
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			id=cursor.getString(cursor.getColumnIndex(KEY_SHOP_PARENT));
			arrPlaceParent.add(id);
		}
		
		cursor.close();
		close();
		if (arrPlaceParent.size()>0)
			managerLoggedIn = true;
		else
			managerLoggedIn = false;
		/*for(int i=0;i<arrPlaceParent.size();i++){
			if(arrPlaceParent.get(i).equalsIgnoreCase("0")) {
				managerLoggedIn = true;
				break;
			}else{
				managerLoggedIn = false;
			}
		}*/
		return managerLoggedIn;

	}
	
	public int mallStoresCount() {
		String query="SELECT "+KEY_SHOP_PARENT+" FROM "+TABLE_PLACE_CHOOSER;
		open();
		Cursor cursor = sqlDataBase.rawQuery(query, null);
		int count = 0;
		if (cursor.getCount()>0) 
			count = cursor.getCount();
		else
			count = 0;
		return count;
	}
	

	/**
	 * Check which mall manger is loggedin like Malad, Vashi, Cyberabad....
	 * 
	 * @return
	 */
	public String getMallManagersArea(){
		String msAreaName  = "";
		String query="SELECT "+KEY_SHOP_NAME+" FROM "+TABLE_PLACE_CHOOSER+" WHERE "+KEY_SHOP_PARENT+" ='0'";
		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		if(cursor.getCount()>1){
			msAreaName = "Inorbit";
		}else{

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				msAreaName=cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME));
				InorbitLog.d("Mall Manager Loggedin "+msAreaName);
				//arrPlaceParent.add(id);
			}
		}
		cursor.close();
		close();
		InorbitLog.d("Mall Manager Loggedin "+msAreaName);
		return msAreaName;
	}





	/**
	 * 
	 * 
	 * @return
	 */
	public boolean isMultipuleMallsLoggedIn(){
		boolean isMultiple = false;
		String query="SELECT "+KEY_SHOP_NAME+" FROM "+TABLE_PLACE_CHOOSER+" WHERE "+KEY_SHOP_PARENT+" ='0'";
		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		//Toast.makeText(ourContext, "isMultiMallsLoggin "+ cursor.getCount(), 0).show();
		if(cursor.getCount()>1) {
			isMultiple = true;
		}
		cursor.close();
		close();
		InorbitLog.d("Mall Manager Loggedin isMultiple "+isMultiple);
		return isMultiple;
	}


	/**
	 * Get the place id of the malls
	 * 
	 * @return ArrayList of mall ids
	 */
	public ArrayList<String> getPlaceParentIdsArr(){
		String id="";
		ArrayList<String> arrPlaceParent = new ArrayList<String>();
		String query="SELECT "+place_parent+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			id=cursor.getString(cursor.getColumnIndex(place_parent));
			arrPlaceParent.add(id);
		}
		cursor.close();
		close();
		return arrPlaceParent;
	}


	public String getLatitude(String parent_id){
		String lat = "";
		String query="SELECT "+area_latitude+" FROM "+TABLE_AREAS + " WHERE "+place_parent+"= '"+parent_id+"' ";
		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			lat=cursor.getString(cursor.getColumnIndex(area_latitude));

		}
		cursor.close();
		close();
		return lat;

	}

	public String getLongitude(String parent_id){
		String longitude = "";
		String query="SELECT "+area_longitude+" FROM "+TABLE_AREAS + " WHERE "+place_parent+"= '"+parent_id+"' ";
		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			longitude=cursor.getString(cursor.getColumnIndex(area_longitude));

		}
		cursor.close();
		close();
		return longitude;

	}


	// phone number by mall id ( for mall info screen)


	/**
	 * Get the phone numbers of selected mall
	 * 
	 * @param mallid Mall id
	 * @return String ArrayList consist of mall's phone number
	 */
	public ArrayList<String> getPhoneNumberByMallId(String mallid){

		ArrayList<String> phoneNumber = new ArrayList<String>();

		String query="SELECT "+KEY_PARENT_STORE_TEL_NO_1+","+KEY_PARENT_STORE_TEL_NO_2+","+KEY_PARENT_STORE_MOB_NO_1+","+KEY_PARENT_STORE_MOB_NO_2+" FROM "+TABLE_PARENT_STORE +" WHERE "+ KEY_PARENT_STORE_MALL_ID +" = '"+ mallid+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			String num1 = cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_TEL_NO_1));
			String num2 = cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_TEL_NO_2));
			//String num3 = "+91"+cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_MOB_NO_1));
			//String num4 = "+91"+cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_MOB_NO_2));
			String num3 = "+"+cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_MOB_NO_1));
			String num4 = "+"+cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_MOB_NO_2));

			if(!num1.equalsIgnoreCase("") && !phoneNumber.contains(num1)){
				phoneNumber.add(num1);
			}
			if(!num2.equalsIgnoreCase("") && !phoneNumber.contains(num2)){
				phoneNumber.add(num2);
			}
			if(num3.length()>1 && !phoneNumber.contains(num3)){
				phoneNumber.add(num3);
			}
			if(num4.length()>1 && !phoneNumber.contains(num4)){
				phoneNumber.add(num4);
			}

			/*phoneNumber.add(cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_TEL_NO_1)));
			phoneNumber.add(cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_TEL_NO_2)));
			phoneNumber.add(cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_MOB_NO_1)));
			phoneNumber.add(cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_MOB_NO_2)));*/

			/*Log.d("Mall_Phone","Mall_Phone 1 " +  cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_TEL_NO_1)));
			Log.d("Mall_Phone","Mall_Phone 2 " +  cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_TEL_NO_2)));
			Log.d("Mall_Phone","Mall_Phone 3 "  +  cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_MOB_NO_1)));
			Log.d("Mall_Phone","Mall_Phone 4 " +  cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_MOB_NO_2)));*/
		}

		for(int i=0;i<phoneNumber.size();i++){
			//Log.d("Mall_Phone","Mall_Phone "+i+" -- " +  phoneNumber.get(i));
		}

		cursor.close();
		close();
		return phoneNumber;
	}


	// latitude by mall_id

	/**
	 * Get latitude of selected mall based on mall id
	 * 
	 * @param mallid of selected mall
	 * @return latitude of that mall
	 */ 

	public String getLatitudeByMallId(String mallid){

		String latitude  = "";

		String query="SELECT "+KEY_PARENT_STORE_LAT+" FROM "+TABLE_PARENT_STORE +" WHERE "+ KEY_PARENT_STORE_MALL_ID +" = '"+ mallid+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			latitude = cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_LAT));
			Log.d("Mall_Lat","Mall_Lat " +  latitude);
		}

		cursor.close();
		close();
		return latitude;
	}

	// longitude by mall_id

	/**
	 * 
	 * Get longitude of selected mall based on mall id
	 * 
	 * @param mallid of selected mall
	 * @return Longitude of mall
	 */
	public String getLongitudeByMallId(String mallid){

		String longitude  = "";

		String query="SELECT "+KEY_PARENT_STORE_LONG+" FROM "+TABLE_PARENT_STORE +" WHERE "+ KEY_PARENT_STORE_MALL_ID +" = '"+ mallid+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			longitude = cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_LONG));
			Log.d("Mall_Long","Mall_Long " +  longitude);
		}

		cursor.close();
		close();
		return longitude;
	}

	// emailid by mall id

	/**
	 * Get email id of  selected mall
	 * 
	 * 
	 * @param mallid Mall id of selected mall
	 * @return Email id of that mall
	 */
	public String getEmailIdByMallId(String mallid){

		String emailId  = "";

		String query="SELECT "+KEY_PARENT_STORE_EMAIL+" FROM "+TABLE_PARENT_STORE +" WHERE "+ KEY_PARENT_STORE_MALL_ID +" = '"+ mallid+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			emailId = cursor.getString(cursor.getColumnIndex(KEY_PARENT_STORE_EMAIL));
			Log.d("Mall_Email","Mall_Email " +  emailId);
		}

		cursor.close();
		close();
		return emailId;
	}


	public String getPlaceId(String place_parent){
		String place_id = "";
		String query="SELECT "+KEY_SHOP_ID+" FROM "+TABLE_PLACE_CHOOSER +" WHERE "+ KEY_SHOP_PARENT+" = '"+ place_parent+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			place_id=cursor.getString(cursor.getColumnIndex(KEY_SHOP_ID));

		}
		cursor.close();
		close();
		return place_id;
	}

	public ArrayList<String> getPlaceIds(String place_parent){
		String place_id = "";
		String query="SELECT "+KEY_SHOP_ID+" FROM "+TABLE_PLACE_CHOOSER +" WHERE "+ KEY_SHOP_PARENT+" = '"+ place_parent+"'";
		ArrayList<String> arrPlaceIds = new ArrayList<String>();
		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			place_id=cursor.getString(cursor.getColumnIndex(KEY_SHOP_ID));
			arrPlaceIds.add(place_id);

		}
		cursor.close();
		close();
		return arrPlaceIds;
	}


	public String getMallNameById(String MallId)
	{
		String mallName = "";

		String query="SELECT "+area_name+" FROM "+TABLE_AREAS+" WHERE "+mall_id+" ='"+MallId+"'";

		open();
		Cursor cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			mallName = cursor.getString(cursor.getColumnIndex(area_name));

		}

		cursor.close();
		close();
		return mallName;
	}

	public String getMallNameByPlaceParentId(String placeParent)
	{
		String mallName = "";

		String query="SELECT "+area_name+" FROM "+TABLE_AREAS+" WHERE "+place_parent+" ='"+placeParent+"'";

		open();
		Cursor cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			mallName = cursor.getString(cursor.getColumnIndex(area_name));

		}

		cursor.close();
		close();
		return mallName;
	}



	public String getShopNameByPlaceID(String placeId)
	{
		String addr = "";

		String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH +" WHERE "+ KEY_SEARCH_ID+" = '"+ placeId+"'";

		open();
		Cursor cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(KEY_SEARCH_STORE_NAME));

		}

		//;Log.d("", "iNORBIT SHOP NAME "+addr +" Id "+placeId);

		cursor.close();
		close();
		return addr;
	}


	public String getMallIdByPlaceID(String placeId)
	{
		String addr = "";

		String query="SELECT "+KEY_SEARCH_PLACE_PARENT+" FROM "+TABLE_GEOTEXT_SEARCH +" WHERE "+ KEY_SEARCH_ID+" = '"+ placeId+"'";

		open();
		Cursor cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(KEY_SEARCH_PLACE_PARENT));

		}

		//String mallId = 


		//Log.d("", "iNORBIT SHOP NAME "+addr +" Id "+placeId);

		cursor.close();
		close();
		return addr;
	}

	public String getStoreNameFromPlaceId(String placeId)
	{
		String title = "";
		try
		{

			open();



			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_SHOP_NAME+" FROM "+TABLE_PLACE_CHOOSER+" WHERE "+ KEY_SHOP_ID+"= '"+placeId+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				//title.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME)));

				title = cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	public String getStoreLogoFromPlaceId(String placeId)
	{
		String img_url = "";
		try
		{

			open();



			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_SHOP_image_url+" FROM "+TABLE_PLACE_CHOOSER+" WHERE "+ KEY_SHOP_ID+"= '"+placeId+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				//title.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME)));
				img_url = cursor.getString(cursor.getColumnIndex(KEY_SHOP_image_url));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return img_url;

	}

	public String getMallNameFromPlaceId(String placeId)
	{
		String title = "";
		try
		{

			open();



			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_SHOP_MALL_ID+" FROM "+TABLE_PLACE_CHOOSER+" WHERE "+ KEY_SHOP_ID+"= '"+placeId+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				//title.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME)));

				title = cursor.getString(cursor.getColumnIndex(KEY_SHOP_MALL_ID));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}



	public long initializeValueForRefresh(String mallId){
		long entiresAffected = 0;
		try{
			InorbitLog.d("Refresh Defult values for Mall id "+mallId);
			open();
			ContentValues cv = new ContentValues();
			cv.put(Tables.KEY_Cat_All_oldValue, "0");
			cv.put(Tables.KEY_Cat_All_newValue, "0");
			cv.put(Tables.KEY_Cat_Dine_oldValue, "0");
			cv.put(Tables.KEY_Cat_Dine_newValue, "0");
			cv.put(Tables.KEY_Cat_Entertainment_oldValue, "0");
			cv.put(Tables.KEY_Cat_Entertainment_newValue, "0");
			cv.put(Tables.KEY_Cat_Pamper_oldValue, "0");
			cv.put(Tables.KEY_Cat_Pamper_newValue, "0");
			cv.put(Tables.KEY_Cat_Shop_oldValue, "0");
			cv.put(Tables.KEY_Cat_Shop_newValue, "0");

			cv.put(Tables.KEY_Cat_All_OfferCount_oldValue, "0");
			cv.put(Tables.KEY_Cat_All_OfferCount_newValue, "0");
			cv.put(Tables.KEY_Cat_Dine_OfferCount_oldValue, "0");
			cv.put(Tables.KEY_Cat_Dine_OfferCount_newValue, "0");
			cv.put(Tables.KEY_Cat_Entertainment_OfferCount_oldValue, "0");
			cv.put(Tables.KEY_Cat_Entertainment_OfferCount_newValue, "0");
			cv.put(Tables.KEY_Cat_Pamper_OfferCount_oldValue, "0");
			cv.put(Tables.KEY_Cat_Pamper_OfferCount_newValue, "0");
			cv.put(Tables.KEY_Cat_Shop_OfferCount_oldValue, "0");
			cv.put(Tables.KEY_Cat_Shop_OfferCount_newValue, "0");

			cv.put(Tables.KEY_Mall_Id, mallId);
			entiresAffected = sqlDataBase.insert(Tables.TABLE_Refersh, null, cv);
		}catch(Exception exception) {
			exception.printStackTrace();
		} finally {
			close();
		}

		return entiresAffected;

	}


	public void updateNewValuesForRefresh(String msMallId, String msNewValue, String msOfferNewValue) {
		try {
			InorbitLog.d("Refresh new values for mall id "+msMallId+" Store Count > "+msNewValue+" Offer Count > "+msOfferNewValue);
			open();
			ContentValues newValues = new ContentValues();
			newValues.put(Tables.KEY_Cat_All_newValue, msNewValue);
			newValues.put(Tables.KEY_Cat_Dine_newValue, msNewValue);
			newValues.put(Tables.KEY_Cat_Entertainment_newValue, msNewValue);
			newValues.put(Tables.KEY_Cat_Pamper_newValue, msNewValue);
			newValues.put(Tables.KEY_Cat_Shop_newValue, msNewValue);

			newValues.put(Tables.KEY_Cat_All_OfferCount_newValue, msOfferNewValue);
			newValues.put(Tables.KEY_Cat_Dine_OfferCount_newValue, msOfferNewValue);
			newValues.put(Tables.KEY_Cat_Entertainment_OfferCount_newValue, msOfferNewValue);
			newValues.put(Tables.KEY_Cat_Pamper_OfferCount_newValue, msOfferNewValue);
			newValues.put(Tables.KEY_Cat_Shop_OfferCount_newValue, msOfferNewValue);

			sqlDataBase.update(Tables.TABLE_Refersh, newValues, Tables.KEY_Mall_Id + "=" + msMallId, null);
			close();
		} catch(Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Check whether to auto refresh the data 
	 * 
	 * 
	 * @param msMallId Current active mall id
	 * @return boolean true | false
	 */
	public boolean compareOldAndNewValuesOfStores(String msMallId) {
		boolean mbAreValuesSame = false;
		try {
			InorbitLog.d("Refresh Comparing STORE values for mall "+mall_id); 
			open();
			String query = "";
			if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {
				query	= "SELECT "+
						Tables.KEY_Cat_All_oldValue +"," + Tables.KEY_Cat_All_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Dine_oldValue +"," + Tables.KEY_Cat_Dine_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Entertainment_oldValue +"," + Tables.KEY_Cat_Entertainment_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Shop_oldValue +"," + Tables.KEY_Cat_Shop_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Pamper_oldValue +"," + Tables.KEY_Cat_Pamper_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			}
			InorbitLog.d("Refresh Compare Query "+query);
			Cursor  cursor = sqlDataBase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){

				String msOldValue="";
				String msNewValue="";

				if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {

					msOldValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_All_oldValue));
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_All_newValue));

				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {

					msOldValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Dine_oldValue));
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Dine_newValue));

				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {

					msOldValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Entertainment_oldValue));
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Entertainment_newValue));

				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {

					msOldValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Shop_oldValue));
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Shop_newValue));

				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {

					msOldValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Pamper_oldValue));
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Pamper_newValue));

				}

				if(msOldValue.equalsIgnoreCase(msNewValue)) {

					InorbitLog.d("Refresh Require False");
					mbAreValuesSame = false;

				} else {

					InorbitLog.d("Refresh Require True");
					mbAreValuesSame = true;
				}
			}
			cursor.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return mbAreValuesSame;
	}

	public boolean compareOldAndNewValuesOfOffers(String msMallId) {
		boolean mbAreValuesSame = false;
		try {
			InorbitLog.d("Refresh Comparing OFFERS values for mall "+mall_id); 
			open();
			String query = "";
			if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {
				query	= "SELECT "+
						Tables.KEY_Cat_All_OfferCount_oldValue +"," + Tables.KEY_Cat_All_OfferCount_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Dine_OfferCount_oldValue +"," + Tables.KEY_Cat_Dine_OfferCount_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Entertainment_OfferCount_oldValue +"," + Tables.KEY_Cat_Entertainment_OfferCount_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Shop_OfferCount_oldValue +"," + Tables.KEY_Cat_Shop_OfferCount_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Pamper_OfferCount_oldValue +"," + Tables.KEY_Cat_Pamper_OfferCount_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			}
			InorbitLog.d("Refresh Compare Query "+query);
			Cursor  cursor = sqlDataBase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){

				String msOldValue="";
				String msNewValue="";

				if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {

					msOldValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_All_OfferCount_oldValue));
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_All_OfferCount_newValue));

				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {

					msOldValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Dine_OfferCount_oldValue));
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Dine_OfferCount_newValue));

				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {

					msOldValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Entertainment_OfferCount_oldValue));
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Entertainment_OfferCount_newValue));

				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {

					msOldValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Shop_OfferCount_oldValue));
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Shop_OfferCount_newValue));

				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {

					msOldValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Pamper_OfferCount_oldValue));
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Pamper_OfferCount_newValue));

				}
				InorbitLog.d("Refresh Offers old value == "+msOldValue + " new value == "+msNewValue);

				if(msOldValue.equalsIgnoreCase(msNewValue)) {

					InorbitLog.d("Refresh Require False");
					mbAreValuesSame = false;

				} else {

					InorbitLog.d("Refresh Require True");
					mbAreValuesSame = true;
				}
			}
			cursor.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return mbAreValuesSame;
	}

	/**
	 * Update the old value of the present category with respect to current mall id
	 * 
	 * @param msMallId Current active mall id
	 */
	public void updateStoresOldValue(String msMallId) {
		try {
			InorbitLog.d("Refresh Updating the old value OF STORE"); 
			open();
			String query = "";
			String msNewValue="";

			if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {
				query	= "SELECT "+
						Tables.KEY_Cat_All_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Dine_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Entertainment_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Shop_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Pamper_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			}

			Cursor  cursor = sqlDataBase.rawQuery(query,null);

			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_All_newValue));
				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Dine_newValue));
				} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Entertainment_newValue));
				} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Shop_newValue));
				} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Pamper_newValue));
				}

			}

			ContentValues newValues = new ContentValues();
			if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {
				newValues.put(Tables.KEY_Cat_All_oldValue, msNewValue);
			} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {
				newValues.put(Tables.KEY_Cat_Dine_oldValue, msNewValue);
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {
				newValues.put(Tables.KEY_Cat_Entertainment_oldValue, msNewValue);
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {
				newValues.put(Tables.KEY_Cat_Shop_oldValue, msNewValue);
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {
				newValues.put(Tables.KEY_Cat_Pamper_oldValue, msNewValue);
			}

			sqlDataBase.update(Tables.TABLE_Refersh, newValues, Tables.KEY_Mall_Id + "=" + msMallId, null);
			close();


		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void updateOffersOldValue(String msMallId) {
		try {
			InorbitLog.d("Refresh Updating the old value OF OFFERS"); 
			open();
			String query = "";
			String msNewValue="";
			if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {
				query	= "SELECT "+
						Tables.KEY_Cat_All_OfferCount_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Dine_OfferCount_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Entertainment_OfferCount_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Shop_OfferCount_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {
				query	= "SELECT "+
						Tables.KEY_Cat_Pamper_OfferCount_newValue +
						" FROM " +
						Tables.TABLE_Refersh +
						" WHERE " + 
						Tables.KEY_Mall_Id+"= '"+msMallId+"'";
			}

			Cursor  cursor = sqlDataBase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_All_OfferCount_newValue));
				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Dine_OfferCount_newValue));
				} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Entertainment_OfferCount_newValue));
				} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Shop_OfferCount_newValue));
				} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {
					msNewValue = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Pamper_OfferCount_newValue));
				}

			}

			ContentValues newValues = new ContentValues();
			if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {
				newValues.put(Tables.KEY_Cat_All_OfferCount_oldValue, msNewValue);
			} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {
				newValues.put(Tables.KEY_Cat_Dine_OfferCount_oldValue, msNewValue);
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {
				newValues.put(Tables.KEY_Cat_Entertainment_OfferCount_oldValue, msNewValue);
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {
				newValues.put(Tables.KEY_Cat_Shop_OfferCount_oldValue, msNewValue);
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {
				newValues.put(Tables.KEY_Cat_Pamper_OfferCount_oldValue, msNewValue);
			}

			sqlDataBase.update(Tables.TABLE_Refersh, newValues, Tables.KEY_Mall_Id + "=" + msMallId, null);
			close();


		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	/**
	 * Check the records of version control table
	 * 
	 * @return int Number of records in table
	 */
	public Integer getCountOfRefreshRable(){
		InorbitLog.d("Refresh count check");
		long total_records = 0;
		try{

			/*if(sqlDataBase!=null && !sqlDataBase.isOpen()){
				open();	
			}*/
			open();	
			String query="SELECT COUNT("+Tables.KEY_Mall_Id+") AS ROW_COUNT FROM "+Tables.TABLE_Refersh;
			InorbitLog.d("Query "+query);
			Cursor  cursor = sqlDataBase.rawQuery(query, null);
			cursor.moveToFirst();
			total_records=cursor.getLong(cursor.getColumnIndex("ROW_COUNT"));
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			if (sqlDataBase != null && sqlDataBase.isOpen()) {
				sqlDataBase.close();
			}
			close();

		}
		return Integer.parseInt(total_records+"");

	}

	/*public boolean isRefershRequire(String mallId) {
		boolean mbIsRefershNeeds = false;
		try {
			open();

			String query = "";
			if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {
				query="SELECT "+Tables.KEY_Cat_All+" FROM "+Tables.TABLE_Refersh+" WHERE "+ Tables.KEY_Mall_Id+"= '"+mallId+"'";
			} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {
				query="SELECT "+Tables.KEY_Cat_Dine+" FROM "+Tables.TABLE_Refersh+" WHERE "+ Tables.KEY_Mall_Id+"= '"+mallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {
				query="SELECT "+Tables.KEY_Cat_Entertainment+" FROM "+Tables.TABLE_Refersh+" WHERE "+ Tables.KEY_Mall_Id+"= '"+mallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {
				query="SELECT "+Tables.KEY_Cat_Shop+" FROM "+Tables.TABLE_Refersh+" WHERE "+ Tables.KEY_Mall_Id+"= '"+mallId+"'";
			} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {
				query="SELECT "+Tables.KEY_Cat_Pamper+" FROM "+Tables.TABLE_Refersh+" WHERE "+ Tables.KEY_Mall_Id+"= '"+mallId+"'";
			}	


			Cursor  cursor = sqlDataBase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				String msRefreshStatus="";
				if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_All)) {
					 msRefreshStatus = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_All));
				} else if (Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_DINE)) {
					 msRefreshStatus = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Dine));
				} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_ENTERTAINMENT)) {
					 msRefreshStatus = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Entertainment));
				} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_SHOP)) {
					 msRefreshStatus = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Shop));
				} else if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_PAMPERZONE)) {
					 msRefreshStatus = cursor.getString(cursor.getColumnIndex(Tables.KEY_Cat_Pamper));
				}

				if(msRefreshStatus.equalsIgnoreCase("0")) {
					InorbitLog.d("Refresh Require False");
					mbIsRefershNeeds = false;
				} else {
					InorbitLog.d("Refresh Require True");
					mbIsRefershNeeds = true;
				}
			}
			cursor.close();
		}catch(SQLException sqx) {
			sqx.printStackTrace();
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			close();
		}

		return mbIsRefershNeeds;
	}*/

	public String getPlaceParentFromPlaceId(String placeId)
	{
		String title = "";
		try
		{

			open();



			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_SHOP_PARENT+" FROM "+TABLE_PLACE_CHOOSER+" WHERE "+ KEY_SHOP_ID+"= '"+placeId+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				//title.add(cursor.getString(cursor.getColumnIndex(KEY_SHOP_NAME)));

				title = cursor.getString(cursor.getColumnIndex(KEY_SHOP_PARENT));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}
	
	public void insertMenu(String place_id, ArrayList<String> urls) {
		open();
		sqlDataBase.beginTransaction();

		try{	
			for(int i=0;i<urls.size();i++){
				ContentValues cv=new ContentValues();

				InorbitLog.d("Database "+urls.get(i));
				cv.put(MENU_PLACE_ID, place_id);
				cv.put(MENU_IMAGE_URL_KEYS, urls.get(i));

				sqlDataBase.insert(MENU_TABLE, null, cv);
			}
			sqlDataBase.setTransactionSuccessful();

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			sqlDataBase.endTransaction();
		}
		close();
	}
	
	public ArrayList<String> getMenu(String place_id) {
		ArrayList<String> urls = new ArrayList<String>();
		try {
			open();
			/*String query="SELECT "+KEY_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+MENU_IMAGE_URL_KEYS+" FROM "+MENU_TABLE+" WHERE "+ MENU_PLACE_ID+"= '"+place_id+"'";
			Cursor  cursor = sqlDataBase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()) {
				urls.add(cursor.getString(cursor.getColumnIndex(MENU_IMAGE_URL_KEYS)));
			}
			cursor.close();
		} catch(SQLException sqx) {
			sqx.printStackTrace();
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			close();
		}
		return urls;
	}
	
	public Cursor getCursorActivityWithStatus(String[] arr, String status) {
		Cursor cursor = null;
		try {
			open();
			cursor = sqlDataBase.rawQuery("SELECT * FROM " + TABLE_ANALYTICS + " WHERE " + ANALYTICS_SYNC_STATUS + " = " + status, null);
		} catch (Exception e) {}
		return cursor;
	}
}

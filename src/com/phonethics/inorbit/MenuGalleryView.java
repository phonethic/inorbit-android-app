package com.phonethics.inorbit;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class MenuGalleryView extends SherlockFragmentActivity {
	
	static Context mContext;
	ViewPager mViewPager;
	ArrayList<String> mImageUrls;
	int mPosition;
	ActionBar mActionBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_gallery_view);
		
		mContext = this;
		mViewPager = (ViewPager) findViewById(R.id.viewPagerMenu);
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle("Menu Gallery");
		//mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));
	
	
		Bundle bundle=getIntent().getExtras();
	
	
		if(bundle!=null) {
			mPosition = bundle.getInt("position");
			mImageUrls = bundle.getStringArrayList("photo_source");
		}
	
		mViewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(),mContext, mImageUrls));
		mViewPager.setCurrentItem(mPosition);
	}



	class ViewPagerAdapter extends FragmentPagerAdapter{
	
		Context context;
		ArrayList<String> images = null;
	
		public ViewPagerAdapter(FragmentManager fm, Context context, ArrayList<String> images) {
			super(fm);
			// TODO Auto-generated constructor stub
	
			this.context = context;
			this.images = images;
		}
		
		@Override
		public Fragment getItem(int arg0) {
			// TODO Auto-generated method stub
	
	
			return SplashPagerFragment.newInstance(images.get(arg0));
	
		}
	
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return images.size();
		}
	
	
	}

	public static class SplashPagerFragment extends Fragment {
	
		Context context;
		String images;
	
	
		public SplashPagerFragment()
		{
	
		}
	
		public static SplashPagerFragment newInstance(String images) {
			SplashPagerFragment fragmentFirst = new SplashPagerFragment();
			Bundle args = new Bundle();
			args.putString("images", images);
			fragmentFirst.setArguments(args);
			return fragmentFirst;
		}
	
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
	
			images = getArguments().getString("images");
			images = images.replaceAll(" ", "%20");
		}
	
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
	
			View view = inflater.inflate(R.layout.menugalleryimgholder, container, false);
			//ImageView imgView = (ImageView) view.findViewById(R.id.mStoreGalleryImg);
			final TouchImageView imgView = (TouchImageView) view.findViewById(R.id.mStoreGalleryImg);
			final ProgressBar mGalleryProgressBar = (ProgressBar) view.findViewById(R.id.mGalleryProgressBar);
			
			if (images!=null && images.length()>0 && !images.trim().equalsIgnoreCase("")) {
				/**
				 * getResources().getString(R.string.photo_url)+images
				 */
				
				Picasso.with(mContext)
				.load(getResources().getString(R.string.photo_url)+images)
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(imgView,new Callback() {
					
					@Override
					public void onSuccess() {
						// TODO Auto-generated method stub
						//Toast.makeText(mContext, "Loaded", 0).show();
						mGalleryProgressBar.setVisibility(View.GONE);
					}
					
					@Override
					public void onError() {
						// TODO Auto-generated method stub
						//mGalleryProgressBar.setVisibility(View.GONE);
						/*Toast.makeText(mContext, "Error in loading, please try again after sometime. "+getResources().getString(R.string.photo_url)+
								images, 0).show();*/
					}
				});
			} else {
				imgView.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
			}
			//imgView.setImageResource(images);
			
			imgView.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {}
			});
			
			return view;
		}
		
		@Override
		public void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
		}
	
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finish();
		return true;
	}
	
}
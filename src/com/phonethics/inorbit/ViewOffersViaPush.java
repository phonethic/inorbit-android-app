package com.phonethics.inorbit;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phoenthics.adapter.SharedListViewAdapter;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;

public class ViewOffersViaPush extends SherlockActivity implements OnClickListener{
	
	public static ImageLoader imageLoader;
	public static DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;
	
	private ActionBar mActionBar;
	private UiLifecycleHelper uiHelper;
	private SessionManager mSession;
	private TextView mtvDateRange;
	private ProgressBar mpBar;
	private LinearLayout mllMerchantView;
	private LinearLayout mllLikeShareLayout;
	private TextView mtvBroadCastTitle;
	private TextView mtvBroadCastBody;
	//private TableRow mtrimageRow;
	private ImageView mivStoreImage;
	private ImageView mivPostFav;
	private ImageView mivSharePost;
	private ImageButton mibShareButton;
	private ImageButton mibLikeButton;
	private TextView mtvPostFav;
	private TextView mtvPostShare;
	private TextView mtvPostView;
	private TextView mtvShareTextButton;
	//private ImageView mivVertiSeprator;
	private String msPostId;
	private String msStoreName;
	private String msMallName;
	private OfferDetailReceiver mbrOfferDetail;
	private PostDetail postDetail;
	private LikeThisOfferReceiver mbrLikeThisOffer;
	private boolean mbLikeThisOffer;
	private boolean mbUnLikeThisOffer;
	private UnLikeThisOfferReceiver mbrUnLikeThisOffer;
	private ShareOfferReceiver mbrShareOffer;
	private Dialog dialogShare;
	private ListView listViewShare;
	private ArrayList<String> packageNames = new ArrayList<String>();
	private ArrayList<String> appName = new ArrayList<String>();
	private ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	private boolean isFacebookPresent = false;
	private LayoutInflater inflate;
	private String msVia = "";
	private Activity mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offers_broad_cast_details);

		mContext = this;
		/**
		 * Initialize the layout views
		 */
		initViews();
		
		uiHelper	= new UiLifecycleHelper(this, null);
		uiHelper.onCreate(savedInstanceState);

		/**
		 * initialize the class variable
		 */
		mSession	= new SessionManager(this);
		dialogShare = new Dialog(this);
		inflate = this.getLayoutInflater();
		
		/**
		 * Get the bundle values
		 */
		getBundleValues();
		
		/**
		 * Set the layout with received variables
		 */
		setLayout();
		
		imageLoader=ImageLoader.getInstance();
		
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
			cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitLocalCache");
		} else {
			cacheDir = this.getCacheDir();
		}

		if (!cacheDir.exists()) {
			cacheDir.mkdirs();
		}
		
		config = new ImageLoaderConfiguration.Builder(this)
		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)
		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		
		imageLoader.init(config);
		
		options = new DisplayImageOptions.Builder().cacheOnDisc()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();
	}
	
	/**
	 * Initialize the layout views
	 */
	private void initViews() {
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Inorbit");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();

		mtvDateRange = (TextView) findViewById(R.id.text_date_range);
		mpBar = (ProgressBar) findViewById(R.id.pBar);
		mpBar.setVisibility(View.GONE);
		
		mllMerchantView	= (LinearLayout)findViewById(R.id.merchantView);
		mllLikeShareLayout	= (LinearLayout)findViewById(R.id.likeShareLayout);
		mtvBroadCastTitle	= (TextView)findViewById(R.id.txtBroadCastTitle);
		mtvBroadCastBody	= (TextView)findViewById(R.id.txtBroadCastBody);
		mivStoreImage = (ImageView)findViewById(R.id.imgStoreImage);
		mivPostFav	= (ImageView)findViewById(R.id.postFav);
		mivSharePost = (ImageView)findViewById(R.id.sharePost);
		mibShareButton	= (ImageButton)findViewById(R.id.shareButton);
		mibLikeButton = (ImageButton)findViewById(R.id.likeButton);
		mtvPostFav = (TextView)findViewById(R.id.txtPostFav);
		mtvPostShare = (TextView)findViewById(R.id.txtPostShare);
		mtvPostView	= (TextView)findViewById(R.id.txtPostView);
		mtvShareTextButton = (TextView)findViewById(R.id.shareTextButton);
		
		mtvDateRange.setTypeface(InorbitApp.getTypeFace());
		mtvPostFav.setTypeface(InorbitApp.getTypeFace());
		mtvPostShare.setTypeface(InorbitApp.getTypeFace());
		mtvPostView.setTypeface(InorbitApp.getTypeFace());
		mtvShareTextButton.setTypeface(InorbitApp.getTypeFace());
		mtvBroadCastTitle.setTypeface(InorbitApp.getTypeFace());
		mtvBroadCastBody.setTypeface(InorbitApp.getTypeFace());

		mtvPostView.setTextColor(Color.BLACK);
		mtvPostFav.setTextColor(Color.BLACK);
		mtvPostShare.setTextColor(Color.BLACK);
		mivSharePost.setVisibility(View.VISIBLE);
		mibLikeButton.setOnClickListener(this);
		mibShareButton.setOnClickListener(this);
	}
	
	/**
	 * Get the bundle values
	 */
	private void getBundleValues(){
		try{
			Bundle mBundle =getIntent().getExtras();
			if(mBundle!=null){
				
				/*fromNotification = mBundle.getBoolean("fromNotification");
				appInForeground = mBundle.getBoolean("");
				mtvBroadCastTitle.setText(mBundle.getString("title"));
				mtvBroadCastBody.setText(mBundle.getString("body"));
				msPhotoSource = mBundle.getString("photo_source");
				msUrl_source=mBundle.getString("url_source");
				msPhoto_link=mBundle.getString("photo_link");
				mbFromCustomer=mBundle.getBoolean("fromCustomer");*/
				
				/*msPlaceID=mBundle.getString("PLACE_ID");
				msUserLike=mBundle.getString("USER_LIKE");
				msStoreName=mBundle.getString("storeName");
				msMallName=mBundle.getString("mallName");
				mbFromManager = mBundle.getBoolean("fromManager");*/
				msPostId=mBundle.getString("post_id")+"";
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * Set the layout, used to check whether this file has been called from customer or merchant and accordingly it will set the views.
	 * 
	 */
	private void setLayout() {
		mivPostFav.setVisibility(View.VISIBLE);
		mibLikeButton.setVisibility(View.VISIBLE);
		mibShareButton.setVisibility(View.VISIBLE);
		mtvShareTextButton.setVisibility(View.GONE);
		mllMerchantView.setVisibility(View.VISIBLE);
		mtvDateRange.setVisibility(View.VISIBLE);
		mtvShareTextButton.setText("");
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (isTaskRoot()) {
			Intent intent=new Intent(this, HomeGrid.class);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			finish();
		} else {
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			finish();
		}
		return true;
	}
	
	private void showToast(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}
	
	/*public boolean isSingleActivityRunning() {
		try {
			ActivityManager am = (ActivityManager) this
                    .getSystemService(Context.ACTIVITY_SERVICE);
			
			List<ActivityManager.RunningTaskInfo> alltasks = am
                    .getRunningTasks(10);
			ActivityInfo[] list = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_ACTIVITIES).activities;
			showToast("infoListLength "+list.length +" "+alltasks.get(0).topActivity.getClassName());
			int countOfRunning = 0;
			for (RunningTaskInfo inf : alltasks) {
				showToast(inf.topActivity.getClassName());
				if (inf.topActivity.getClassName().startsWith(getPackageName())) {
					countOfRunning ++;
				}
			}
			showToast(countOfRunning+"");
			showToast(alltasks.get(0).numActivities+" "+alltasks.size()+" "+alltasks.get(0).numRunning);
	        return countOfRunning==1;
        } catch (Throwable t) {
            t.printStackTrace();
        }
	    return true;
    }*/
	
	/**
	 * Get the customer id
	 * @return String : Customer id
	 */
	private String getCustomerUserId(){
		HashMap<String,String> user_details = mSession.getUserDetailsCustomer();
		return user_details.get("user_id_CUSTOMER").toString();
	}

	/**
	 * Get the merchant id
	 * @return String : Merchant id
	 */
	private String getMerchantId(){
		HashMap<String,String> user_details = mSession.getUserDetails();
		return user_details.get("user_id").toString();
	}

	
	/**
	 * Get the customer auth code
	 * @return String : Auth code
	 */
	private String getCustomerUserAuthId(){
		HashMap<String,String>user_details=mSession.getUserDetailsCustomer();
		return user_details.get("auth_id_CUSTOMER").toString();
	}

	
	/**
	 * Get the merchant auth code
	 * @return String : Merchant auth code
	 */
	private String getmerchantAuthId(){
		HashMap<String,String>user_details=mSession.getUserDetails();
		return user_details.get("auth_id").toString();
	}

	
	/**
	 * Server request to get the information of selected offer
	 * 
	 */
	private void loadBroadCast() {
		try {
			mpBar.setVisibility(View.VISIBLE);
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header),getResources().getString(R.string.api_value));

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("post_id", msPostId));
			if(mSession.isLoggedInCustomer()){
				nameValuePairs.add(new BasicNameValuePair("user_id", getCustomerUserId()));
			}
			nameValuePairs.add(new BasicNameValuePair("page", "1"));
			nameValuePairs.add(new BasicNameValuePair("count", "-1"));

			MyClass myClass = new MyClass(this);
			myClass.getStoreRequest(RequestTags.Tag_GetOfferDetail, nameValuePairs, headers);
		} catch(Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Server request to Like the offer when customer is logged in
	 * 
	 */
	private void likeThisOffer(){

		try{
			mpBar.setVisibility(View.VISIBLE);
			JSONObject json = new JSONObject();
		
			json.put("user_id", getCustomerUserId());
			json.put("post_id", postDetail.getId());
			json.put("auth_id", getCustomerUserAuthId());

			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header),getResources().getString(R.string.api_value));

			MyClass myClass = new MyClass(mContext);
			myClass.postRequest(RequestTags.Tag_LikeThisOffer, headers, json);


		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	
	/**
	 * Server request to unlike the offer when customer is logged in
	 * 
	 */
	private void unlikeThisOffer(){

		mpBar.setVisibility(View.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header),getResources().getString(R.string.api_value));
		headers.put("user_id",getCustomerUserId());
		headers.put("auth_id",getCustomerUserAuthId());

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("post_id", postDetail.getId()));

		MyClass myClass = new MyClass(mContext);
		myClass.delete(RequestTags.Tag_UnLikeThisOffer, nameValuePairs, headers);

	}

	/**
	 * Server request to share the offer
	 * 
	 */
	private void callShareApi() {
		// TODO Auto-generated method stub

		/*Intent intent = new Intent(context, ShareService.class);
		intent.putExtra("shareService", shareServiceResult);
		intent.putExtra("URL",BROADCAST_URL + SHARE_URL);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);
		intent.putExtra("post_id", POST_ID);
		intent.putExtra("via", VIA);
		context.startService(intent);*/

		//FacebookOperations.shareThisInfo(context, fbObject, getResources().getString(R.string.fb_offer_object));
		mpBar.setVisibility(View.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header),getResources().getString(R.string.api_value));
		JSONObject json = new JSONObject();
		try{

			json.put("post_id", postDetail.getId());
			json.put("via", msVia);


		}catch(Exception ex){
			ex.printStackTrace();
		}

		MyClass myClass = new MyClass(mContext);
		myClass.postRequest(RequestTags.Tag_ShareThisOffer, headers, json);

	}

	
	/**
	 * Server request to delete the offer when merchant is logged in
	 * 
	 */
	void deletePost(){
		HashMap<String, String> header = new HashMap<String, String>();
		header.put(getResources().getString(R.string.api_header),getResources().getString(R.string.api_value));
		header.put("user_id", getMerchantId());
		header.put("auth_id", getmerchantAuthId());

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("post_id", postDetail.getId()));

		mpBar.setVisibility(View.VISIBLE);
		MyClass myClass = new MyClass(mContext);
		myClass.delete(RequestTags.Tag_DeletePost, nameValuePairs, header);

		//Toast.makeText(context, "Delete post",0).show();
	}

	/**
	 * Broadcast receiver of loadBroadCast()
	 * 
	 */
	private class OfferDetailReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				mpBar.setVisibility(View.GONE);
				String success = intent.getStringExtra("SUCCESS");
				if (success.equalsIgnoreCase("true")) {
					postDetail = intent.getParcelableExtra("Post_Detail");
					setData(postDetail);
				} else
					Toast.makeText(ViewOffersViaPush.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

	private class LikeThisOfferReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				mpBar.setVisibility(View.GONE);
				String success = intent.getStringExtra("SUCCESS");
				String message = intent.getStringExtra("MESSAGE");
				if(success.equalsIgnoreCase("true")){
					postDetail.setUser_like("1");
					if (mbLikeThisOffer) {
						mibLikeButton.setImageResource(R.drawable.ic_heart_filled);
					}
					
					try {
						mtvPostFav.setText((Integer.parseInt(postDetail.getTotal_like())+1)+"");
						mtvPostShare.setText(postDetail.getTotal_share());
						mtvPostView.setText(postDetail.getTotal_view());
					} catch (Exception e) {
						
					}
				} else {
					String code  = intent.getStringExtra("CODE");
					if (code.equalsIgnoreCase("-221")) {
						//Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
						Toast.makeText(mContext, "It seems you were logged in from other device too, Please login again.", Toast.LENGTH_LONG).show();
						Intent intent1=new Intent(context,LoginSigbUpCustomerNew.class);
						((Activity) mContext).startActivityForResult(intent1,2);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					} else {
						Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
					}
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}

	}

	private class UnLikeThisOfferReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {

			try{

				mpBar.setVisibility(View.GONE);
				String success = intent.getStringExtra("SUCCESS");
				String message = intent.getStringExtra("MESSAGE");
				if(success.equalsIgnoreCase("true")){  
					postDetail.setUser_like("0");
					if(mbUnLikeThisOffer){
						Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
						mibLikeButton.setImageResource(R.drawable.ic_heart_unfilled);
					}else{
						String code  = intent.getStringExtra("CODE");
						if(code.equalsIgnoreCase("-221")){
							Toast.makeText(mContext, "It seems you were logged in from other device too, Please login again.", Toast.LENGTH_LONG).show();
							//Toast.makeText(mContext, "Please login again", Toast.LENGTH_SHORT).show();
							Intent intent1=new Intent(context,LoginSigbUpCustomerNew.class);
							((Activity) mContext).startActivityForResult(intent1,2);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}else{
							Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
						}
					}
					try {
						mtvPostFav.setText((Integer.parseInt(postDetail.getTotal_like())-1)+"");
						mtvPostShare.setText(postDetail.getTotal_share());
						mtvPostView.setText(postDetail.getTotal_view());
					} catch (Exception e) {
						
					}
				}

			}catch(Exception ex){

			}

		}

	}

	/**
	 * 
	 * Broadcast receiver of deletePost()
	 * 
	 *
	 */
	class DeleteBroadCast extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				mpBar.setVisibility(View.GONE);
				String success = intent.getStringExtra("SUCCESS");
				String message = intent.getStringExtra("MESSAGE");
				String code = intent.getStringExtra("CODE");
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("Source", "Details");
				EventTracker.logEvent(getResources().getString(R.string.mViewPost_PostDeleted), map);
				//MyToast.showToast((Activity) context, success);
				if(success.equalsIgnoreCase("true")){
					Intent backIntent=new Intent();
					((Activity) context).setResult(2,backIntent);
					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
				}else{
					boolean isVolleyError = intent.getBooleanExtra("volleyError",false);
					if(isVolleyError){
						int code1 = intent.getIntExtra("CODE",0);
						String message1=intent.getStringExtra("MESSAGE");
						String errorMessage = intent.getStringExtra("errorMessage");
						String apiUrl = intent.getStringExtra("apiUrl");	
						if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
							EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "OfferBroadcast-DeletePost");
						}

						Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	
	/**
	 *
	 * Broadcast receiver of callShareApi()
	 *
	 */
	class ShareOfferReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				mpBar.setVisibility(View.GONE);
				String success = intent.getStringExtra("SUCCESS");
				String message = intent.getStringExtra("MESSAGE");
				String code = intent.getStringExtra("CODE");
				//MyToast.showToast((Activity) context, success);
				if(success.equalsIgnoreCase("true")){
					//Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
					try {
						mtvPostFav.setText(Integer.parseInt(postDetail.getTotal_like()));
						mtvPostShare.setText(Integer.parseInt(postDetail.getTotal_share())+1+"");
						mtvPostView.setText(postDetail.getTotal_view());
					} catch (Exception e) {
						
					}
				}else{
					boolean isVolleyError = intent.getBooleanExtra("volleyError",false);
					if(isVolleyError){
						int code1 = intent.getIntExtra("CODE",0);
						String message1=intent.getStringExtra("MESSAGE");
						String errorMessage = intent.getStringExtra("errorMessage");
						String apiUrl = intent.getStringExtra("apiUrl");	
						if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
							EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "OfferBroadcast-DeletePost");
						}

						//Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	
	void registerEvent(String eventName){
		try{
			Map<String, String> params = new HashMap<String, String>();
			boolean param = false;
			InorbitLog.d("StoreName -- "+ msStoreName +" - "+msMallName);
			if(msMallName!=null && !msMallName.equalsIgnoreCase("")){
				params.put("MallName", msMallName);	
				param = true;

			}else{
				param = false;
			}
			if(msStoreName!=null && !msStoreName.equalsIgnoreCase("")){
				if(param){
					params.put("StoreName", msStoreName +" - "+msMallName);
					param = true;
				}else{
					param = false;
				}
			}else{
				param = false;
			}
			if(param)
				EventTracker.logEvent(eventName, params);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(getApplicationContext());
		//imgStoreLoader.clearCache();
		super.onStop();
		try{
			if (mbrOfferDetail != null) 
				this.unregisterReceiver(mbrOfferDetail);
			if(mbrLikeThisOffer!=null)
				this.unregisterReceiver(mbrLikeThisOffer);	
			if(mbrUnLikeThisOffer!=null)
				this.unregisterReceiver(mbrUnLikeThisOffer);	
			if(mbrShareOffer!=null)
				this.unregisterReceiver(mbrShareOffer);	
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			try {
				EventTracker.startLocalyticsSession(getApplicationContext());
				EventTracker.logEvent("Push Offer Open", true);
			} catch (Exception e) {}
			
			createInhouseAnalyticsEvent(msPostId, "9", new DBUtil(this).getActiveMallId());
			
			IntentFilter offerDetalFilter = new IntentFilter(RequestTags.Tag_GetOfferDetail);
			IntentFilter likeThisOfferFilter = new IntentFilter(RequestTags.Tag_LikeThisOffer);
			IntentFilter unLikeThisOfferFilter = new IntentFilter(RequestTags.Tag_UnLikeThisOffer);
			/*IntentFilter filter_delete = new IntentFilter(RequestTags.Tag_DeletePost);*/
			IntentFilter shareThisOfferFilter = new IntentFilter(RequestTags.Tag_ShareThisOffer);

			this.registerReceiver(mbrOfferDetail = new OfferDetailReceiver(), offerDetalFilter);
			this.registerReceiver(mbrLikeThisOffer = new LikeThisOfferReceiver(), likeThisOfferFilter);
			this.registerReceiver(mbrUnLikeThisOffer = new UnLikeThisOfferReceiver(), unLikeThisOfferFilter);
			/*this.registerReceiver(mbrDeleteThisOffer = new DeleteBroadCast(), filter_delete);*/
			this.registerReceiver(mbrShareOffer = new ShareOfferReceiver(), shareThisOfferFilter);
			
			if(NetworkCheck.isNetConnected(this)){
				loadBroadCast();	
			} else {
				Toast.makeText(this, "No Internet Connection. Please try again after some time", Toast.LENGTH_LONG).show();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		uiHelper.onResume();
	}

	@Override
	protected void onPause() {
		try {
			EventTracker.endLocalyticsSession(getApplicationContext());
		} catch (Exception e) {}
		
		uiHelper.onPause();
		super.onPause();
	}

	/* on start */
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	private void setData(final PostDetail postDetail) {
		try {
			mtvBroadCastTitle.setText(postDetail.getTitle());
			mtvBroadCastBody.setText(Html.fromHtml(checkUrlInString(postDetail.getDescription())));
			mtvBroadCastBody.setMovementMethod(LinkMovementMethod.getInstance());
			
			String msStartDate = postDetail.getOffer_start_date_time();
			String msEndDate = postDetail.getOffer_end_date_time();
			
			DateFormat strtdt = new SimpleDateFormat("yyyy-MM-dd");
			Date strt_date_con = (Date) strtdt.parse(postDetail.getOffer_start_date_time());
			
			DateFormat enddt = new SimpleDateFormat("yyyy-MM-dd");
			Date end_date_con = (Date) enddt.parse(postDetail.getOffer_end_date_time());
			
			DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy");
			
			DateFormat  timeFomat=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Date startTime = (Date) timeFomat.parse(postDetail.getOffer_start_date_time());
			Date endTime = (Date) timeFomat.parse(postDetail.getOffer_end_date_time());
			
			String startdate = strtdt.format(strt_date_con);
			String enddate = strtdt.format(end_date_con);
			
			if(msStartDate.equalsIgnoreCase("0000-00-00T00:00:00GMT+05:30") &&
					(msEndDate.equalsIgnoreCase("0000-00-00T00:00:00GMT+05:30"))) {
				
				mtvDateRange.setVisibility(View.GONE);
			} else if(msStartDate.equalsIgnoreCase("0000-00-00T00:00:00GMT+05:30")) {
				mtvDateRange.setVisibility(View.VISIBLE);
				mtvDateRange.setText("Offer valid till "+targetFormat.format(end_date_con));
			} else {
				String msStartTime  = changeTimeFormat(" "+timeFomat.format(startTime),false);
				String msEndTime  = changeTimeFormat(" "+timeFomat.format(endTime),false);

				mtvDateRange.setVisibility(View.VISIBLE);
				mtvDateRange.setText("Offer valid from "+targetFormat.format(strt_date_con)+" "+msStartTime+" till "+targetFormat.format(end_date_con)+" "+msEndTime);
			}

			if (mSession.isLoggedInCustomer()) {
				String msUserLiked = postDetail.getUser_like();
				if (msUserLiked.equalsIgnoreCase("1")) {
					mibLikeButton.setImageResource(R.drawable.ic_heart_filled);
				} else {
					mibLikeButton.setImageResource(R.drawable.ic_heart_unfilled);
				}
			} else {
				mibLikeButton.setImageResource(R.drawable.ic_heart_unfilled);
			}

			String msImgUrl = postDetail.getImage_url1();
			if (msImgUrl!=null && msImgUrl.length()!=0 && !msImgUrl.equalsIgnoreCase("")) {
				msImgUrl = msImgUrl.replaceAll(" ", "%20");
				mivStoreImage.setScaleType(ScaleType.FIT_CENTER);
				mivStoreImage.setVisibility(View.VISIBLE);
				
				try {
					imageLoader.displayImage(getResources().getString(R.string.photo_url)+msImgUrl, mivStoreImage, options, new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String imageUri, View view) {
							mpBar.setVisibility(View.VISIBLE);
						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {
							//prog.setVisibility(View.INVISIBLE);
						}

						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							mpBar.setVisibility(View.GONE);
						}

						@Override
						public void onLoadingCancelled(String imageUri, View view) {
							//prog.setVisibility(View.INVISIBLE);
						}
					});
				} catch (IllegalArgumentException illegalArg) {
					illegalArg.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}


				mivStoreImage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						ArrayList<String>GALLERY_PHOTO_SOURCE=new ArrayList<String>();
						GALLERY_PHOTO_SOURCE.add(postDetail.getImage_url1());
						Intent intent = new Intent(ViewOffersViaPush.this, StorePagerGallery.class);
						intent.putExtra("photo_source", GALLERY_PHOTO_SOURCE);
						intent.putExtra("position", 1);
						startActivity(intent);	
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				});
			} else {
				mivStoreImage.setVisibility(View.GONE);
			}
			
			mtvPostFav.setText(postDetail.getTotal_like());
			mtvPostShare.setText(postDetail.getTotal_share());
			mtvPostView.setText(postDetail.getTotal_view());
		} catch(Exception ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {

		if(v.getId()==mibLikeButton.getId()){
			if(mSession.isLoggedInCustomer()){
				String msUserLiked = postDetail.getUser_like();
				if(msUserLiked.equalsIgnoreCase("1")){
					if(NetworkCheck.isNetConnected(mContext)){
						mbUnLikeThisOffer = true;
						unlikeThisOffer();	
					} else {
						mbUnLikeThisOffer = false;
						Toast.makeText(mContext, "No Internet Connection. Please try again after sometime.", Toast.LENGTH_SHORT).show();
					}
				} else {
					if(NetworkCheck.isNetConnected(mContext)){
						mbLikeThisOffer = true;
						likeThisOffer();	
					}else{
						mbLikeThisOffer = false;
						Toast.makeText(mContext, "No Internet Connection. Please try again after sometime.", Toast.LENGTH_SHORT).show();
					}

				}
			}else{
				//Toast.makeText(mContext, "Please Login", Toast.LENGTH_SHORT).show();
				loginBackDialog();


			}
		}

		if(v.getId()==mibShareButton.getId()){
			
			
			//callShareApi();
			registerEvent(getResources().getString(R.string.offerDetail_Share));
			String body = "";
			getShareIntents();
			
			if(postDetail.getImage_url1().equalsIgnoreCase("")){
				body = "\n"+ mtvBroadCastTitle.getText().toString() + "\n" + mtvBroadCastBody.getText().toString();
			}else{
				body = "\n"+ mtvBroadCastTitle.getText().toString() + "\n" + mtvBroadCastBody.getText().toString();
			}
			
			if(msMallName==null || msMallName.equalsIgnoreCase("") ){
				if(msStoreName==null || msStoreName.equalsIgnoreCase("")){
					showDialog("Inorbit " + " | "+"Offers\n" , body);
				}else{
					showDialog("Inorbit " + " | "+msStoreName + " - " + "Offers\n" , body);
				}
			}else{
				if(msStoreName==null || msStoreName.equalsIgnoreCase("")){
					showDialog("Inorbit " + " | "+"Offers\n" , body);
				}else{
					showDialog("Inorbit "+msMallName + " | "+msStoreName + " - " + "Offers\n" , body);
				}
			}

			//showDialog(body);
		}

	}

	public void showDialog(final String shareTitle,final String shareText){
		dialogShare.show();
		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

					FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder((Activity) mContext)
					.setLink("http://inorbitapp.in/download")
					.setDescription(shareText)
					.setName(shareTitle)
					.build();
					uiHelper.trackPendingDialogCall(shareDialog.present());
					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(mContext, "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}

				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					((Activity) mContext).startActivityForResult(i,3);
				}

				dismissDialog();

			}
		});
	}

	void dismissDialog(){
		dialogShare.dismiss();
	}

	private void getShareIntents(){

		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");
		final PackageManager pm = mContext.getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);
		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);

		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(mContext.getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();
				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
					//inviteFriendsLayout.setVisibility(View.VISIBLE);
				}else{
					//inviteFriendsLayout.setVisibility(View.GONE);
				}


				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}

			}

			if(!isFacebookPresent){
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
		}

		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(mContext, 0, inflate, appName, packageNames, appIcon));

	}

	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try{

			if(requestCode == 3){
				//callShareApi();
				if(resultCode == RESULT_OK){
					Log.d("TEST","TEST IS DONE SUCCESSFULLY");
					callShareApi();
				}else if (resultCode == RESULT_CANCELED) {
					Log.d("TEST","TEST IS CACELLED");
				}
			}

			if(requestCode == 2){



			}

			try{
				uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
					@Override
					public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
						Log.e("Activity", String.format("Error: %s", error.toString()));
					}

					@Override
					public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
						Log.i("Activity", "Success!");
						msVia="Android-Facebook";
						callShareApi();
					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}



		}catch(NullPointerException ex)
		{
			ex.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	
	/**
	 * Check whether offer description has any URL attached with this 
	 * 
	 * 
	 * @param msUrl 
	 * @return
	 */
	private String checkUrlInString(String msUrl){
		String s = msUrl;
		String newString = "";
		// separate input by spaces ( URLs don't have spaces )
		String [] parts = s.split("\\s+");
		//newString = msUrl;
		
		// Attempt to convert each item into an URL.   
		for( String item : parts ) {
			try {
				InorbitLog.d("Info item "+item);
				
				// If possible then replace with anchor...
				if(item.startsWith("www.")){
					item = item.replace("www.", "http://");
				}
				
				URL url = new URL(item);
				newString = newString + " <a href=\"" + url + "\">"+ url + "</a> " ;
				
			} catch (MalformedURLException e) {
				// If there was an URL that was not it!...
				System.out.print( item + " " );
				InorbitLog.d("Info "+item);
				newString = newString + " "+item;
			}
			
		}
		InorbitLog.d("Info " + newString);
		return newString;
	}

	void loginBackDialog(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
		alertDialogBuilder.setTitle("Inorbit");
		alertDialogBuilder
		.setMessage("Please login to mark this offer as favourite")
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				Intent intent=new Intent(mContext,LoginSigbUpCustomerNew.class);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		});

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}


	private String changeTimeFormat(String msTimeToConvert,boolean mbSubmitPattern){

		String msNewTime;
		String msNewFormat;
		String msOldFormat;

		if(mbSubmitPattern){
			msOldFormat=" hh:mm a";
			msNewFormat = " HH:mm";
			msNewTime = "";
		}else{
			msNewFormat=" hh:mm a";
			//msOldFormat = " HH:mm";
			msOldFormat = "yyyy-MM-dd'T'HH:mm:ss";

			msNewTime = "";
		}


		try{
			SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
			Date d = sdf.parse(msTimeToConvert);
			sdf.applyPattern(msNewFormat);
			msNewTime = sdf.format(d);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return msNewTime;
	}
	
	@Override
	public void onBackPressed() {
		if (isTaskRoot()) {
			Intent intent=new Intent(this, HomeGrid.class);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			finish();
		} else {
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			finish();
		}
		super.onBackPressed();
	}
	
	private void createInhouseAnalyticsEvent(String flag, String activityId, String mallId) {
		Uri uri = new Uri.Builder().scheme("content").authority(mContext.getResources().getString(R.string.authority)).
				appendPath("/insert").build();
        ContentValues cv = new ContentValues(6);
		cv.put("UDM_ID", mSession.getUdmIDForCustomer());
		cv.put("DATE", new Date().toString());
		cv.put("FLAG", flag);
		cv.put("ACTIVITY_ID", activityId);
		cv.put("MALL_ID", mallId);
		cv.put("SYNC_STATUS", -1);
		mContext.getContentResolver().insert(uri, cv);
	}
}
package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.mobeta.android.dslv.DragSortController;
import com.mobeta.android.dslv.DragSortListView;
import com.phonethics.model.RequestTags;
import com.phonethics.model.RestaurantMenuModel;
import com.squareup.picasso.Picasso;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class ReorderActivity extends SherlockFragmentActivity{
    DragSortListView listView;
    ReorderAdapter adapter;
    ReorderResponseReceiver reorderResponseReceiver;
    ProgressBar progress;

    private DragSortListView.DropListener onDrop = new DragSortListView.DropListener()
    {
        @Override
        public void drop(int from, int to)
        {
            if (from != to) {
                Model item = (Model) adapter.list.get(from);
                adapter.list.remove(from);
                adapter.list.add(to, item);
                adapter.notifyDataSetChanged();
            }
        }
    };

    private DragSortListView.RemoveListener onRemove = new DragSortListView.RemoveListener()
    {
        @Override
        public void remove(int which) {
            adapter.list.remove(which);
            adapter.notifyDataSetChanged();
        }
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	setTheme(R.style.Theme_City_custom);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reorder_layout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        listView = (DragSortListView) findViewById(R.id.listview);
        progress = (ProgressBar) findViewById(R.id.progress);
        
        ArrayList<RestaurantMenuModel> mMenuModel = getIntent().getParcelableArrayListExtra("all_stores");
        ArrayList<Model> list = new ArrayList<ReorderActivity.Model>();
        if (mMenuModel == null) {
        	return;
        }
        
        for (int i=0; i<mMenuModel.size(); i++) {
        	list.add(new Model(mMenuModel.get(i).getId(), mMenuModel.get(i).getImage_order(), getResources().getString(R.string.photo_url) +
        			mMenuModel.get(i).getImage_url(), mMenuModel.get(i).getThumb_url(), i));
        }
        
        adapter = new ReorderAdapter(this, R.layout.reorder_item_layout, list);
        listView.setAdapter(adapter);
        listView.setDropListener(onDrop);
        listView.setRemoveListener(onRemove);
        
        DragSortController controller = new DragSortController(listView);
        controller.setDragHandleId(R.id.menu);
                //controller.setClickRemoveId(R.id.);
        controller.setRemoveEnabled(false);
        controller.setSortEnabled(true);
        controller.setDragInitMode(1);
                //controller.setRemoveMode(removeMode);

        listView.setFloatViewManager(controller);
        listView.setOnTouchListener(controller);
        listView.setDragEnabled(true);
        
        ((TextView) findViewById(R.id.reorderBtn)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				setOrderForMenu();
			}
		});
        
        ((TextView) findViewById(R.id.cancel)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
				finish();
			}
        });
    }
    
    private void setOrderForMenu() {
    	progress.setVisibility(View.VISIBLE);
    	ArrayList<Model> list = adapter.list;
    	JSONArray jsonArr = new JSONArray();
    	for (int i=0; i<list.size(); i++) {
    		JSONObject obj = new JSONObject();
    		try {
				obj.put("image_id", list.get(i).id);
				obj.put("order", list.size()-i);
			} catch (JSONException e) {
				e.printStackTrace();
			}
    		jsonArr.put(obj);
    	}
    	JSONObject obj = new JSONObject();
    	try {
			obj.put("orders", jsonArr);
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	
    	HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		
    	MyClass myClass = new  MyClass(this);
    	myClass.postRequest(RequestTags.TAG_CHANGE_MENU_ORDER, headers, obj);
    }
    @Override
    protected void onResume() {
    	super.onResume();
    	IntentFilter intentFilter = new IntentFilter(RequestTags.TAG_CHANGE_MENU_ORDER);
		this.registerReceiver(reorderResponseReceiver = new ReorderResponseReceiver(), intentFilter);
    }
    
    @Override
    protected void onStop() {
    	if (reorderResponseReceiver != null) {
    		this.unregisterReceiver(reorderResponseReceiver);
    	}
    	super.onStop();
    }
    private class ReorderResponseReceiver extends BroadcastReceiver {
    	
    	@Override
    	public void onReceive(Context context, Intent intent) {
    		progress.setVisibility(View.GONE);
    		if (intent != null) {
    			showToast(intent.getStringExtra("MESSAGE"));
    			if (intent.getStringExtra("SUCCESS").equalsIgnoreCase("true")) {
    				
    			} else {
    				
    			}
    			finish();
    		} else {
    			showToast("Some Error was generated");
    		}
    	}
    }
    
    private void showToast(String msg) {
    	Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    
    private class Model {
    	private String id, order, url, thumb_url;
    	private int position;
    	
    	public Model (String id, String order, String url, String thumb_url, int position) {
    		this.id = id;
    		this.order = order;
    		this.url = url;
    		this.thumb_url = thumb_url;
    		this.position = position;
    	}
    	
    	@Override
    	public boolean equals(Object o) {
    		if (((Model)o).position == this.position) 
    			return true;
    		return false;
    	}
    }
    
    public DragSortListView getListView() {
    	
    	return (DragSortListView) this.findViewById(R.id.listview);
    }
    
    private class ReorderAdapter extends BaseAdapter {
    	private ArrayList<Model> list;
    	Context context;
    	
		public ReorderAdapter(Context context, int resource, ArrayList<Model> objects) {
			this.list = objects;
			this.context = context;
		}
		
		@Override
    	public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.reorder_item_layout, parent, false);
            }  
            
    		((TextView)v.findViewById(R.id.textView)).setText("Image "+(position+1));
    		String photo_source=getResources().getString(R.string.photo_url) + list.get(position).thumb_url.toString().replaceAll(" ", "%20");
			try {
				Picasso.with(context).load(photo_source)
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(((ImageView)v.findViewById(R.id.menu)));
			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			} catch(Exception e){
				e.printStackTrace();
			}
    		return v;
    	}

		@Override
		public int getCount() {
			return list.size();
		}
		
		@Override
		public Object getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	finish();
    	return true;
    }
}

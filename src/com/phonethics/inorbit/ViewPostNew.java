package com.phonethics.inorbit;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.eventtracker.EventTracker;

import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;
import com.phonethics.notification.LocalNotification;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix.ScaleToFit;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView.ScaleType;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class ViewPostNew extends SherlockFragmentActivity {

	
	
	public static ImageLoader imageLoader;
	public static DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;
	

	
	private PagerSlidingTabStrip pagerTabs;
	private JazzyViewPager mPager;
	private ArrayList<String> pages;
	
	Activity context;
	ArrayList<PostDetail> tempPOSTARR;
	ArrayList<PostDetail> POSTARR;
	
	ListView listUnapprovePost;
	static ProgressBar		pBar;
	private NetworkCheck isnetConnected;
	static String API_HEADER;
	static String API_VALUE;
	static String USER_ID="";
	static String AUTH_ID="";
	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";
	SessionManager session;
	private String STORE_ID;
	private String storeName;
	String actionTitle = "View Offers";
	private PostReceiver postReceiver;
	ActionBar actionBar;
	private Dialog operationDialog;
	private DeleteBroadCast deleteReceiver;

	static String	sStrPage1 = "Approved";
	static String	sStrPage2 = "Pending";
	static String	sStrPage3 = "Rejected";
	boolean isManagerLoggedIn =false;
	
	





	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_post_new);
		
		/**
		 * 
		 * Initialize the layout, class varibles and action bar
		 * 
		 */
		context = this;
		isnetConnected = new NetworkCheck(context);
		pBar = (ProgressBar) findViewById(R.id.pBar_view_post);
		pagerTabs = (PagerSlidingTabStrip) findViewById(R.id.pagerTabs_viewtabs);

		actionBar=getSupportActionBar();
		actionBar.setTitle(actionTitle);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		InorbitLog.d("onCreate()");

		/**
		 * Initialize the view pager
		 * 
		 */
		mPager=(JazzyViewPager)findViewById(R.id.viewPostPager);
		pages = new ArrayList<String>();
		pages.add(sStrPage1);
		pages.add(sStrPage2);
		pages.add(sStrPage3);

		
		/**
		 * Get merchant values from session manager
		 * 
		 */
		session = new SessionManager(context);
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		HashMap<String,String>user_details=session.getUserDetails();
		USER_ID=user_details.get(KEY_USER_ID).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID).toString();

	

		try{

			/**
			 * Get the bundle value to set the action bar title
			 * 
			 */
			Bundle b=getIntent().getExtras();
			if(b!=null){
				STORE_ID=b.getString("STORE_ID");
				storeName=b.getString("storeName");
				if(storeName==null||storeName.equalsIgnoreCase("")){
					actionTitle =   "Offers";
				}else{
					actionTitle = storeName + " Offers";
				}

				actionBar.setTitle(actionTitle);
				if(STORE_ID!=""){
					
					/**
					 * netwrok request
					 * 
					 */
					loadBroadCast();
				}
			}
			
			/**
			 * Check whether Mall Manager is logged in or not
			 * 
			 */
			DBUtil mdbUtil = new DBUtil(context);
			isManagerLoggedIn = mdbUtil.isManager();
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		

		imageLoader=ImageLoader.getInstance();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitLocalCache");
		}
		else
		{
			cacheDir=context.getCacheDir();
		}

		if(!cacheDir.exists())
		{
			cacheDir.mkdirs();
		}/*else if(network.isNetworkAvailable())
		{

			DeleteRecursive(cacheDir);
		}
		 */
		config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();
		
		
		
		
		
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		context.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}


	//Creating Pages with PageAdapter.
	public class PageAdapter extends FragmentStatePagerAdapter{
		Activity context;
		ArrayList<String> pages;
		public PageAdapter(FragmentManager fm,Activity context,ArrayList<String> pages) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.pages=pages;
		}

		@Override
		public String getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return new PageFragment(pages.get(position), context);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return pages.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Object obj = super.instantiateItem(container, position);
			mPager.setObjectForPosition(obj, position);
			return obj;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if(object != null){
				return ((Fragment)object).getView() == view;
			}else{
				return false;
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method 


		}
	}

	@SuppressLint("ValidFragment")
	public class PageFragment extends Fragment{

		String title;
		Context context;
		View view;


		public PageFragment(){

		}


		@SuppressLint("ValidFragment")
		public PageFragment(String title,Activity context){
			this.title=title;
			this.context=context;

		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setRetainInstance(true);
		}


		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			super.onCreateView(inflater, container, savedInstanceState);
			// TODO Auto-generated method stub
			view = inflater.inflate(R.layout.list_view_post, null);
			
			listUnapprovePost = (ListView) view.findViewById(R.id.listView_view_post);
			
			
			if(title.equalsIgnoreCase(sStrPage1)){
				//tempArr = new ArrayList<PostDetail>();
				
				final ArrayList<PostDetail> tempArr = getApprovedArray(1);
				
				InorbitLog.d("Post Approved "+tempArr.size());
				
				for(int i=0;i<tempArr.size();i++){
					InorbitLog.d("Post Approved "+tempArr.get(i).getTitle());
				}
				//POSTARR  = tempArr;
				
				listUnapprovePost.setAdapter(new SearchListAdapter((Activity) context, tempArr));
				listUnapprovePost.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						try{
							//Intent intent=new Intent(context, OffersBroadCastDetails.class);
							Intent intent=new Intent(context, OfferViewNew.class);
							Conifg.fromPedingOffers = false;
							Conifg.fromViewOfferList = true;
							intent.putExtra("storeName", storeName);
							intent.putExtra("POST_ID", tempArr.get(position).getId());
							intent.putExtra("title", tempArr.get(position).getTitle());
							intent.putExtra("body", tempArr.get(position).getDescription());
							intent.putExtra("photo_source", tempArr.get(position).getImage_url1());
							intent.putExtra("fromManager", true);
							getActivity().startActivityForResult(intent,2);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}catch(Exception ex){
							ex.printStackTrace();
						}

					}
				});
				listUnapprovePost.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int pos, long arg3) {
						// TODO Auto-generated method stub
						
						if(isManagerLoggedIn){
							showApproveDialog(pos,tempArr);
						}
						
						return true;
					}
				});
			}else if(title.equalsIgnoreCase(sStrPage2)){
				
				
				final ArrayList<PostDetail> tempArr  = getApprovedArray(0);
				
				InorbitLog.d("Post Unapproved "+tempArr.size());
				for(int i=0;i<tempArr.size();i++){
					InorbitLog.d("Post Unapproved "+tempArr.get(i).getTitle());
				}
				POSTARR  = tempArr;
				listUnapprovePost.setAdapter(new SearchListAdapter((Activity) context, tempArr));
				listUnapprovePost.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						try{
							//Intent intent=new Intent(context, OffersBroadCastDetails.class);
							Intent intent=new Intent(context, OfferViewNew.class);
							Conifg.fromPedingOffers = false;
							intent.putExtra("storeName", storeName);
							intent.putExtra("POST_ID", tempArr.get(position).getId());
							intent.putExtra("title", tempArr.get(position).getTitle());
							intent.putExtra("body", tempArr.get(position).getDescription());
							intent.putExtra("photo_source", tempArr.get(position).getImage_url1());
							intent.putExtra("fromManager", true);
							getActivity().startActivityForResult(intent,2);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}catch(Exception ex){
							ex.printStackTrace();
						}

					}
				});
				listUnapprovePost.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int pos, long arg3) {
						// TODO Auto-generated method stub
						
						if(isManagerLoggedIn){
							showApproveDialog(pos,tempArr);
						}
						return true;
					}
				});
			}else if(title.equalsIgnoreCase(sStrPage3)){
				//tempArr = new ArrayList<PostDetail>();
				final ArrayList<PostDetail> tempArr  = getApprovedArray(2);
				InorbitLog.d("Post Rejected "+tempArr.size());
				for(int i=0;i<tempArr.size();i++){
					InorbitLog.d("Post Rejected "+tempArr.get(i).getTitle());
				}
				POSTARR  = tempArr;
				listUnapprovePost.setAdapter(new SearchListAdapter((Activity) context, tempArr));
				listUnapprovePost.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						try{
							//Intent intent=new Intent(context, OffersBroadCastDetails.class);
							Intent intent=new Intent(context, OfferViewNew.class);
							Conifg.fromPedingOffers = false;
							intent.putExtra("storeName", storeName);
							intent.putExtra("POST_ID", tempArr.get(position).getId());
							intent.putExtra("title", tempArr.get(position).getTitle());
							intent.putExtra("body", tempArr.get(position).getDescription());
							intent.putExtra("photo_source", tempArr.get(position).getImage_url1());
							intent.putExtra("fromManager", true);
							getActivity().startActivityForResult(intent,2);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

						}catch(Exception ex){
							ex.printStackTrace();
						}

					}
				});
				listUnapprovePost.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int pos, long arg3) {
						// TODO Auto-generated method stub
						if(isManagerLoggedIn){
							showApproveDialog(pos,tempArr);
						}
						return true;
					}
				});
			}

			return view;
		}
	}


	
	/**
	 * Filter the Offers based on the status
	 * @param status: if  status = 1 it will return the apporved offers, if status = 0 it will return pending offers, if status = 2 it will return rejected offers
	 * @return ArrayList of PostDetail;
	 */
	public ArrayList<PostDetail> getApprovedArray(int status){
		
		ArrayList<PostDetail> postDetail = new ArrayList<PostDetail>();
		for(int i=0;i<tempPOSTARR.size();i++){
			InorbitLog.d("Post Method "+tempPOSTARR.get(i).getTitle());
			if(status==1){
				if(tempPOSTARR.get(i).getStatus().equalsIgnoreCase("1")){
					postDetail.add(tempPOSTARR.get(i));
				}
			}else if(status==0){
				if(tempPOSTARR.get(i).getStatus().equalsIgnoreCase("0")){
					postDetail.add(tempPOSTARR.get(i));
				}
			}else{
				if(tempPOSTARR.get(i).getStatus().equalsIgnoreCase("2")){
					postDetail.add(tempPOSTARR.get(i));
				}
			}


		}
		return postDetail;
	}
	
	/**
	 * 
	 * Network request to check the offers of particular store
	 * 
	 */
	void loadBroadCast()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			MyClass myClass = new MyClass(context);

			//showToast("volley request");
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("place_id", STORE_ID));

			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(API_HEADER, API_VALUE);
			headers.put("user_id", USER_ID);
			headers.put("auth_id", AUTH_ID);
			InorbitLog.d("merchant user_id "+USER_ID);
			InorbitLog.d("merchant auth_id "+AUTH_ID);


			InorbitLog.d("Using Volley");
			myClass.getStoreRequest(RequestTags.TagViewBroadcast, nameValuePairs, headers);

		}else{
			Toast.makeText(context, "No Internet Connection. Please try again after some time.", Toast.LENGTH_SHORT).show();
		}
	}


	
	static class SearchListAdapter extends ArrayAdapter<PostDetail>{
		ArrayList<PostDetail> postArr;
		Activity context;
		LayoutInflater inflate;

		private String PHOTO_PARENT_URL;
		boolean isApproved;

		public SearchListAdapter(Activity context, 
				ArrayList<PostDetail> postArr) {
			super(context, R.layout.offers_broadcast_layout,postArr);
			// TODO Auto-generated constructor stub
			this.postArr=postArr;
			this.isApproved = isApproved;

			inflate=context.getLayoutInflater();
			PHOTO_PARENT_URL=context.getResources().getString(R.string.photo_url);
			InorbitLog.d("Inorbit Inside adapter"+postArr.toString());

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return postArr.size();
		}
		@Override
		public PostDetail getItem(int position) {
			// TODO Auto-generated method stub
			return postArr.get(position);
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null)
			{
				ViewHolder holder = new ViewHolder();
				convertView=inflate.inflate(R.layout.offers_broadcast_layout,null);

				holder.txtTitle=(TextView)convertView.findViewById(R.id.broadCastOffersText);
				holder.imgBroadcastLogo=(ImageView)convertView.findViewById(R.id.imgBroadcastLogo);
				holder.txtDate=(TextView)convertView.findViewById(R.id.broadCastOffersDate);
				holder.txtMonth=(TextView)convertView.findViewById(R.id.broadCastOffersMonth);
				holder.viewOverLay=(View)convertView.findViewById(R.id.viewOverLay);
				holder.txtBroadCastStoreTotalLike=(TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalLike);
				holder.txtBroadCastStoreTotalViews = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalViews);
				holder.txtBroadCastStoreTotalShare = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalShare);
				holder.txtDate.setVisibility(View.GONE);
				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			/*hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);*/

			hold.txtTitle.setText(postArr.get(position).getTitle());
			try
			{
				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");
				Date date_con = (Date) dt.parse(postArr.get(position).getDate().toString());
				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);

				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+postArr.get(position).getDate().toString());

				hold.txtDate.setText(day+"");	
				hold.txtMonth.setText(dt2.format(date_con)+"");
				hold.txtBroadCastStoreTotalLike.setText(postArr.get(position).getTotal_like());
				hold.txtBroadCastStoreTotalViews.setText(postArr.get(position).getTotal_view());
				hold.txtBroadCastStoreTotalShare.setText(postArr.get(position).getTotal_share());

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			try
			{
				if(postArr.get(position).getImage_url1().toString().equalsIgnoreCase("")){
		
					hold.txtDate.setTextColor(Color.argb(255, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.INVISIBLE);
					InorbitLog.d("Inorbit Photo url "+"");
					hold.imgBroadcastLogo.setImageResource(R.drawable.ic_launcher);
					hold.imgBroadcastLogo.setScaleType(ScaleType.FIT_XY);
				}else{
					String photo_source=postArr.get(position).getThumb_url1().toString().replaceAll(" ", "%20");
					hold.txtDate.setTextColor(Color.argb(200, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.VISIBLE);
					try {
						/*imageLoader.displayImage(PHOTO_PARENT_URL+photo_source, hold.imgBroadcastLogo, options, new ImageLoadingListener() {

							@Override
							public void onLoadingStarted(String imageUri, View view) {
								// TODO Auto-generated method stub
								//pBar.setVisibility(View.VISIBLE);
							}
							
							@Override
							public void onLoadingFailed(String imageUri, View view,
									FailReason failReason) {
								// TODO Auto-generated method stub
								//prog.setVisibility(View.INVISIBLE);
							}

							@Override
							public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
								// TODO Auto-generated method stub
								//pBar.setVisibility(View.GONE);
							}

							@Override
							public void onLoadingCancelled(String imageUri, View view) {
								// TODO Auto-generated method stub
								//prog.setVisibility(View.INVISIBLE);
								
							}
						});*/
						Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
						.placeholder(R.drawable.ic_launcher)
						.error(R.drawable.ic_launcher)
						.into(hold.imgBroadcastLogo);;
						InorbitLog.d("Inorbit Photo url "+photo_source);


					} catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}


			return convertView;
		}

		static class ViewHolder
		{

			TextView txtTitle,txtDate,txtMonth;
			ImageView imgBroadcastLogo;
			View viewOverLay;
			TextView txtBroadCastStoreTotalLike;
			TextView txtBroadCastStoreTotalViews;
			TextView txtBroadCastStoreTotalShare;


		}

	}

	
	/**
	 * Broadcast receiver of loadBroadCast()
	 * 
	 * 
	 * 
	 * @author Nitin
	 *
	 */
	class PostReceiver extends BroadcastReceiver{

		private String TOTAL_SEARCH_PAGES;
		private String TOTAL_SEARCH_RECORDS;

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				pBar.setVisibility(View.GONE);
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String success = bundle.getString("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						String total_pages = bundle.getString("TOTAL_PAGE");
						String total_record = bundle.getString("TOTAL_RECORD");
						TOTAL_SEARCH_PAGES = total_pages;
						TOTAL_SEARCH_RECORDS = total_record;
						bundle = intent.getBundleExtra("BundleData");
						if(bundle!=null){
							ArrayList<PostDetail> postArr = bundle.getParcelableArrayList("POST_DETAILS");
							tempPOSTARR = postArr;
							for(int i=0;i<tempPOSTARR.size();i++){
								InorbitLog.d("Post "+tempPOSTARR.get(i).getTitle());
							}
							
							PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, pages);
							mPager.setAdapter(adapter);
							mPager.setOffscreenPageLimit(pages.size());
							pagerTabs.setViewPager(mPager);
							pagerTabs.setTextColor(Color.BLACK);
							pagerTabs.setBackgroundColor(Color.TRANSPARENT);
						}
					}else{
						String message = bundle.getString("MESSAGE");
						showToast(message);
						mPager.setVisibility(View.GONE);
						pages.clear();

						String code = bundle.getString("CODE");
						boolean isManagerLoggedIn = new DBUtil(context).isManager();
						if(code.equalsIgnoreCase("-172")){
							if(isManagerLoggedIn){

							}else{

								LocalNotification localNotification = new LocalNotification(context);
								localNotification.checkPref();
								localNotification.setIsPosted(false);
								localNotification.alarm();
							}
						}



						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){

							int code1 = bundle.getInt("CODE",0);
							String message1=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "View Post");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();

						}

					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	public void showApproveDialog(final int listPosition,final ArrayList<PostDetail> tempArr){
		operationDialog  = new Dialog(context);
		operationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		operationDialog.setContentView(R.layout.approve_item_dialog);
		operationDialog.setTitle(null);
		ListView listCateg = (ListView)operationDialog.findViewById(R.id.categDilogList);
		ArrayList<String> 	operationArr = new ArrayList<String>();
		operationArr.add("Edit");
		operationArr.add("Delete");
		listCateg.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, operationArr));
		listCateg.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				try{
					operationDialog.dismiss();
					if(pos==0){
						
						EventTracker.logEvent(getResources().getString(R.string.mViewPost_PostReused), false);
						ArrayList<String>SELECTED_STORE_ID=new ArrayList<String>();
						SELECTED_STORE_ID.add(STORE_ID);
					
						
						Intent intent=new Intent(context, CreatBroadCastNew.class);
						
						intent.putStringArrayListExtra("SELECTED_STORE_ID",SELECTED_STORE_ID);
						intent.putExtra("POST_DETAIL", tempArr.get(listPosition));
						intent.putExtra("POST_EDIT", true);
						context.startActivityForResult(intent,2);
						//finish();
						
						
					}else{
						
						Map<String, String> params = new HashMap<String, String>();
						params.put("Source", "List");
						EventTracker.logEvent(getResources().getString(R.string.mViewPost_PostReused), params);
						operationDialog.dismiss();
						
						deletePost(tempArr.get(listPosition).getId());
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		});
		operationDialog.show();

	}

	
	/**
	 * Network request to delete the particular offer from the server
	 * 
	 * @param postId
	 */
	void deletePost(String postId){
		HashMap<String, String> header = new HashMap<String, String>();
		header.put(API_HEADER, API_VALUE);
		header.put("user_id", USER_ID);
		header.put("auth_id", AUTH_ID);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("post_id", postId));

		pBar.setVisibility(View.VISIBLE);
		MyClass myClass = new MyClass(context);
		myClass.delete(RequestTags.Tag_DeletePost, nameValuePairs, header);

		//Toast.makeText(context, "Delete post",0).show();
	}



	/**
	 * 
	 * Broadcast revceiver of deletePost();
	 * 
	 * @author Nitin
	 *
	 */
	class DeleteBroadCast extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				pBar.setVisibility(View.GONE);
				String success = intent.getStringExtra("SUCCESS");
				String message = intent.getStringExtra("MESSAGE");
				String code = intent.getStringExtra("CODE");
				//MyToast.showToast((Activity) context, success);
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("Source", "List");
				EventTracker.logEvent(getResources().getString(R.string.mViewPost_PostDeleted), map);
				if(success.equalsIgnoreCase("true")){
	
					loadBroadCast();

				}else{
					boolean isVolleyError = intent.getBooleanExtra("volleyError",false);
					if(isVolleyError){


						int code1 = intent.getIntExtra("CODE",0);
						String message1=intent.getStringExtra("MESSAGE");
						String errorMessage = intent.getStringExtra("errorMessage");
						String apiUrl = intent.getStringExtra("apiUrl");	
						if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
							EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "OfferBroadcast-DeletePost");
						}

						Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		try{
			if(requestCode==2){
				loadBroadCast();
			} 
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void finishTo(){
		//Intent intent=new Intent(context,MerchantsHomeGrid.class);
		//Intent intent=new Intent(context,MerchantHome.class);
	//	startActivityForResult(intent, 1);
		context.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}


	void showToast(String text){
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		IntentFilter filter = new IntentFilter(RequestTags.TagViewBroadcast);
		IntentFilter approveFilter = new IntentFilter(RequestTags.Tag_ApproveBroadcast);
		IntentFilter deleteFilter = new IntentFilter(	RequestTags.Tag_DeletePost);

		context.registerReceiver(postReceiver = new PostReceiver(), filter);
		context.registerReceiver(deleteReceiver = new DeleteBroadCast(), deleteFilter);
		//context.registerReceiver(postApproveReceiver = new ApprovePostReceiver(), approveFilter);
	}

	


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		try{
			context.unregisterReceiver(postReceiver);
			context.unregisterReceiver(deleteReceiver);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onStop();
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}




}

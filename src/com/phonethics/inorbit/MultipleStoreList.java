package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.model.RequestTags;
import com.squareup.picasso.Picasso;

public class MultipleStoreList extends SherlockActivity {

	Context mContext;
	Activity mActivity;
	ListView mStoresList;
	private ActionBar mActionBar;
	TotalCount mTotalCountReciver;
	DBUtil 	dbUtil;
	RelativeLayout mStorePBar;
	ArrayList<String> mCount = new ArrayList<String>();
	ArrayList<String> mFrom = new ArrayList<String>();
	ArrayList<String> mStoreNames = new ArrayList<String>();
	ArrayList<String> storesurl = new ArrayList<String>();
	StoreListAdapter mAdapter;
	int REQUEST_CODE = 30;
	SessionManager mSessionManager;
	Handler handler;
	Runnable runnable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multiple_store_list);
		
		mContext = this;
		mActivity = this;
		mStoresList = (ListView)findViewById(R.id.mStoreList);
		mSessionManager = new SessionManager(getApplicationContext());
		
		mActionBar	=	getSupportActionBar();
		mActionBar.setTitle("Inbox");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();
		dbUtil = new DBUtil(mContext);
		mStorePBar=(RelativeLayout)findViewById(R.id.mProgressBar);

		getTotalCount();
		
		mStoresList.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				mCount.set(position, "0");
				String mall_id = dbUtil.getPlaceParentFromPlaceId(mFrom.get(position));

				Intent intent = new Intent(mContext, InboxGetMessageStoreManager.class);
				intent.putExtra("READER", mFrom.get(position));
				intent.putExtra("SENDER", mall_id);
				intent.putExtra("FROM_IMG_URL", getResources().getString(R.string.photo_url)+storesurl.get(position).trim());
				intent.putExtra("TO_IMG_URL", "");
				startActivityForResult(intent, REQUEST_CODE);
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		setResult(RESULT_OK);
		finish();
		return true;
	}

	private void getTotalCount() {
		final ArrayList<String> STORE_ID=dbUtil.getAllStoreIDs();
		
		JSONObject json = new JSONObject();
		try {
			JSONArray reader_id = new JSONArray();
			for(int i=0;i<STORE_ID.size();i++){
				reader_id.put(STORE_ID.get(i));	
			}
			json.put("reader_id", reader_id);
			json.put("auth_id", getAuthId());
			json.put("user_id", getUserId());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		callTotalCountApi(json);
	}

	void callTotalCountApi(JSONObject json){

		//mStorePBar.setVisibility(View.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		
		MyClass myclass = new MyClass(mContext);
		myclass.postRequest(RequestTags.TAG_TOTAL_COUNT, headers, json);
	}

	/**
	 * Get the user id of the merchant
	 * 
	 * @return String : user id
	 */
	String getUserId(){
		HashMap<String,String>user_details = mSessionManager.getUserDetails();
		return user_details.get("user_id").toString();

	}

	/**
	 * Get the auth id of the merchant
	 * 
	 * @return String : Auth id
	 */
	String getAuthId(){
		HashMap<String,String>user_details = mSessionManager.getUserDetails();
		return user_details.get("auth_id").toString();
	}
	
	class TotalCount extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {

			try {
				mStorePBar.setVisibility(View.GONE);
				if(intent!=null) {
					
					mCount = intent.getStringArrayListExtra("COUNT");
					mFrom = intent.getStringArrayListExtra("FROM");
					
					//to get names of all stores
					for(int i=0;i<mFrom.size();i++) {
						mStoreNames.add(dbUtil.getStoreNameFromPlaceId(mFrom.get(i)));
						storesurl.add(dbUtil.getStoreLogoFromPlaceId(mFrom.get(i)));
						//InorbitLog.d("Name " + dbUtil.getStoreNameFromPlaceId(mFrom.get(i)));
					}
					mAdapter = new StoreListAdapter(mActivity, R.drawable.ic_launcher,mCount, mStoreNames);
					mStoresList.setAdapter(mAdapter);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		IntentFilter filter_total_count = new IntentFilter(RequestTags.TAG_TOTAL_COUNT);
		mContext.registerReceiver(mTotalCountReciver = new TotalCount(), filter_total_count);
		super.onResume();
		
		getTotalMessageCount();
	}

	@Override
	protected void onStop() {
		try{
			mContext.unregisterReceiver(mTotalCountReciver);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		handler.removeCallbacks(runnable);
		super.onStop();
	}

	//to set adapter for list
	class StoreListAdapter extends ArrayAdapter<String> {

		Activity mContext;
		ArrayList<String> mCount;
		ArrayList<String> mStoreNames;
		LayoutInflater mInflater;
		
		public StoreListAdapter(Activity mContext, int resource,ArrayList<String> mCount, ArrayList<String> mStoreNames) {
			super(mContext, resource,  mCount);
			// TODO Auto-generated constructor stub

			this.mContext=mContext;
			this.mCount=mCount;
			this.mStoreNames=mStoreNames;
			mInflater = mContext.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mCount.size();
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null) {
				final ViewHolder holder=new ViewHolder();
				convertView=mInflater.inflate(R.layout.all_messages_layout,null);
				holder.store_logo = (ImageView) convertView.findViewById(R.id.imgsearchLogo);
				holder.txtStoreName=(TextView)convertView.findViewById(R.id.mStoreName);
				holder.txtCount=(TextView)convertView.findViewById(R.id.mMsgCount);
				holder.msg_background_img = (RelativeLayout) convertView.findViewById(R.id.msg_background_img);
				convertView.setTag(holder);
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.txtStoreName.setText(mStoreNames.get(position));
			hold.txtCount.setText(mCount.get(position));
			
			if (storesurl.get(position)!= null && storesurl.get(position).trim().length() > 0) {
				Picasso.with((Activity)mContext).load(getResources().getString(R.string.photo_url)+storesurl.get(position).trim())
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(hold.store_logo);
			} else {
				hold.store_logo.setImageResource(R.drawable.ic_launcher);
			}
			//hold.store_logo.setImageResource(R.drawable.ic_launcher);
			
			if (Integer.parseInt(mCount.get(position)) > 0) {
				hold.msg_background_img.setBackgroundResource(R.drawable.msg_bubble_blue);
			} else {
				hold.msg_background_img.setBackgroundResource(R.drawable.msg_bubble_grey);
			}
			
			return convertView;
		}

	}

	static class ViewHolder{
		ImageView store_logo;
		TextView txtStoreName;
		TextView txtCount;
		RelativeLayout msg_background_img;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==REQUEST_CODE){
			if(resultCode == RESULT_OK){
				//getTotalCount();
				//showToast("Call notify adapter");
				//				for(int i=0;i<mCount.size();i++){
				//					InorbitLog.d("COunt Updated " + mCount.get(i));
				//				}
				mAdapter.notifyDataSetChanged();
			}
		}
	}

	@Override
	public void onBackPressed() {
		setResult(RESULT_OK);
		finish();
		super.onBackPressed();
	}

	void showToast(String msg){
		Toast.makeText(mContext, ""+msg, 0).show();
	}
	
	public void getTotalMessageCount() {
		handler = new Handler();
		runnable = new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				try {
					getTotalCount();
					//InorbitLog.d("Called Count Api");
					handler.postDelayed(runnable, 60000);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		};
		handler.postDelayed(runnable,60000);
	}
}

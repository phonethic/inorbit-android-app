package com.phonethics.inorbit;

import android.os.Bundle;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Window;

public class ShowRestaurantMenu extends SherlockActivity {
	private ShowRestaurantMenu mContext = this;
	private ActionBar mActionbar;
	private DBUtil mDbutil;
	private SessionManager mSession;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_listing);
		setSupportProgressBarIndeterminateVisibility(false); 

		/**
		 * initialize layout 
		 * 
		 */
		initViews();

		/**
		 * initialize actionbar 
		 * 
		 */

		initActionBar();


		/**
		 * initialize class objects 
		 * 
		 */
		initObjects();

		/**
		 * create list of action bar 
		 * 
		 */
		creatActionBarList();
		
	}
	
	/**
	 * initialize layout 
	 * 
	 */
	void initViews(){
		
	}
	

	/**
	 * initialize actionbar 
	 * 
	 */
	
	void initActionBar() {
		mActionbar	= getSupportActionBar();
		mActionbar.setTitle(getString(R.string.actionBarTitle));
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.show();
	}
	
	/**
	 * initialize class objects 
	 * 
	 */
	
	void initObjects(){
		mDbutil 	= new DBUtil(mContext);
		mSession	= new SessionManager(getApplicationContext());
	}
	
	/**
	 * create action bar list
	 * 
	 */
	void creatActionBarList(){
		
	}

}

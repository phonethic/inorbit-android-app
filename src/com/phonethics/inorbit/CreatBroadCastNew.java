package com.phonethics.inorbit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.ImageRequest;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.EditStore.Back;
import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;
import com.squareup.picasso.Picasso;

public class CreatBroadCastNew extends SherlockActivity implements OnClickListener,OnCheckedChangeListener{

	public static ImageLoader imageLoader;
	public static DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;
	
	private EditText metOfferTitle;
	private EditText metOfferDesc;
	private Button mbtnChooseImage;
	private Button mbtnCapture;
	private TextView mtvStartDateTime;
	private TextView mtvEndDateTime;
	private EditText metTags;
	private  final int REQUEST_CODE_CUSTOMCAMERA = 8;
	private  final int REQUEST_CODE_POSTGALLERY = 9;
	private  final int REQUEST_CODE_POSTCROPGALLERY = 10;
	private  final int REQUEST_CODE_TAKE_PICTURE = 7;
	private byte[] mByteArr = null ;
	private String FILE_PATH="";
	private String FILE_NAME="";
	private String FILE_TYPE="";
	private Context mContext;
	private ImageView mivThumbPreview;
	private ImageView mivDeleteImg;
	private boolean mbIsImagePost = false;
	private Calendar dateTime;
	private Calendar dateTimeCurrent;
	private long mlMiliseconds;
	private String msStartDate,msEndDate,msStartTime,msEndTime;
	private long miliseconds;
	private TextView mtvStartPromt;
	private TextView mtvStartDate;
	private TextView mtvStartTime;
	private TextView mtvEndDatePromt;
	private TextView mtvEndDate;
	private TextView mtvEndTime;
	private TextView mtvIsOfferPrompt;
	private RadioGroup mrg_offer;
	private RadioButton rdbtn_offer;
	private RadioButton mrbOffer;
	private RadioButton mrbPost;
	private RelativeLayout mrlDate;
	private Button mBtnEdit;
	private Button mBtnReview;
	private Button mBtnPost;
	private LinearLayout mllOffer;
	private LinearLayout mllImageOption;
	private ImageView mivmgSeprate1;
	private ImageView mivmgSeprate2;
	private ImageView mivmgSeprate3;
	private ImageView mivmgSeprate4;
	private ImageView mivmgSeprate5;
	private ImageView mivImgCal;
	private ArrayList<String> msarrSelectedStore;
	private SessionManager mSessionManager;
	private String msEncodedpath="/sdcard/temp_photo.jpg";
	private BroadCastOfferUpdate mbroadcastOfferUpdate;
	private BroadCastOffer mbroadcastOffer;
	private ProgressBar mPbar;
	private ActionBar mActionbar;
	private DBUtil mDbUtil;
	private boolean mbIsManagerLoggedIn = false;
	private TextView mvtvSetPriority;
	private ImageView mivmgSeprate6;
	private TextView mtvSetPriority;
	private ArrayList<String> msarrPriority;
	private ArrayList<Integer> miarrPriorityIds;
	private PostDetail mPostDetail;
	private boolean mbIsPostEdit = false,mbIsSameImage=true;
	private String msPriority = "-1";
	private boolean mbStartDateSelected = false;
	private boolean mbEndDateSelected = false;
	private boolean isReview = false;
	private boolean rotateImage; 
	private int angularRotation;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_creat_broad_cast_new);

		mContext =this;
		initViews();
		getBundleValues();
		
		mDbUtil = new DBUtil(mContext);
		
		/**
		 * Check mall manager is logged in or not
		 * 
		 */
		mbIsManagerLoggedIn = mDbUtil.isManager();
		setLayout();
		

		/**
		 * 
		 * Offer priorites and there values
		 * 
		 */
		msarrPriority = new ArrayList<String>();
		msarrPriority.add("Lowest");
		msarrPriority.add("Low");
		msarrPriority.add("Medium");
		msarrPriority.add("High");
		msarrPriority.add("Highest");

		miarrPriorityIds = new ArrayList<Integer>();
		miarrPriorityIds.add(0);
		miarrPriorityIds.add(1);
		miarrPriorityIds.add(2);
		miarrPriorityIds.add(3);
		miarrPriorityIds.add(99);
		
		
		
		
		imageLoader=ImageLoader.getInstance();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitLocalCache");
		}
		else
		{
			cacheDir=mContext.getCacheDir();
		}

		if(!cacheDir.exists())
		{
			cacheDir.mkdirs();
		}/*else if(network.isNetworkAvailable())
		{

			DeleteRecursive(cacheDir);
		}
		 */
		config= new ImageLoaderConfiguration.Builder(mContext)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();
		
		
		
		
		
		/**
		 * 
		 * Set the action bar title as "Edit Offer" if Mall manager is logged in and if offers needs to be edited
		 * 
		 */
		if(mbIsManagerLoggedIn && mbIsPostEdit){
			mActionbar.setTitle("Edit Offer");
			
			/**
			 * Set the offer details like its Title, Description and other information
			 * 
			 */
			setPostDetails();
		}
		
		
		
		
		
	}

	
	
	/**
	 * 
	 * Initialize the layout and class varibles
	 * 
	 * 
	 */
	void initViews(){

		metOfferTitle = (EditText) findViewById(R.id.edtOfferTitle);
		metOfferDesc = (EditText) findViewById(R.id.edtOfferDesc);
		metTags = (EditText) findViewById(R.id.edtTags);
		mbtnChooseImage = (Button) findViewById(R.id.btnChooseImage);
		mbtnCapture = (Button) findViewById(R.id.btnCapture);
		mBtnEdit = (Button) findViewById(R.id.btn_edit);
		mBtnReview = (Button) findViewById(R.id.btn_Review);
		mBtnPost = (Button) findViewById(R.id.btn_post);
		mtvStartPromt = (TextView) findViewById(R.id.text_start_from);
		mtvStartDate = (TextView) findViewById(R.id.text_start_date);
		mtvStartTime = (TextView) findViewById(R.id.text_start_time);
		mtvSetPriority = (TextView) findViewById(R.id.text_priority);
		mtvEndDatePromt = (TextView) findViewById(R.id.text_ends_on);
		mtvEndDate = (TextView) findViewById(R.id.text_end_date);
		mtvEndTime = (TextView) findViewById(R.id.text_end_time);
		mtvIsOfferPrompt = (TextView) findViewById(R.id.text_isOffer);
		mrg_offer = (RadioGroup)findViewById(R.id.radigrp_offer);
		mrbOffer = (RadioButton) findViewById(R.id.rdbtn_offer);
		mrbPost = (RadioButton) findViewById(R.id.rdbtn_post);
		mllOffer = (LinearLayout) findViewById(R.id.linear_is_offer);
		mllImageOption = (LinearLayout) findViewById(R.id.linear_image_option);

		mtvStartPromt.setText("<u>Select offer start date</u>");
		mtvEndDatePromt.setText("<u>Select offer end date</u>");
		mtvStartPromt.setText(Html.fromHtml("<u>Select offer start date</u>"));
		mtvEndDatePromt.setText(Html.fromHtml("<u>Select offer end date</u>"));
		mtvStartPromt.setVisibility(View.VISIBLE);
		mtvEndDatePromt.setVisibility(View.VISIBLE);
		mtvStartDate.setVisibility(View.GONE);
		mtvStartTime.setVisibility(View.GONE);
		mtvEndDate.setVisibility(View.GONE);
		mtvEndTime.setVisibility(View.GONE);


		mivThumbPreview = (ImageView) findViewById(R.id.imgThumbPreview);
		mivDeleteImg = (ImageView) findViewById(R.id.deleteImage);
		mivmgSeprate1 = (ImageView) findViewById(R.id.img_seprate_1);
		mivmgSeprate2 = (ImageView) findViewById(R.id.img_seprate_2);
		mivmgSeprate3 = (ImageView) findViewById(R.id.img_seprate_3);
		mivmgSeprate4 = (ImageView) findViewById(R.id.img_seprate_4);
		mivmgSeprate5 = (ImageView) findViewById(R.id.img_seprate_5);
		mivmgSeprate6 = (ImageView) findViewById(R.id.img_seprate_6);
		mivImgCal = (ImageView) findViewById(R.id.img_ic_date);
		mrlDate = (RelativeLayout) findViewById(R.id.rel_date);
		mPbar	= (ProgressBar) findViewById(R.id.pBar);
		mPbar.setVisibility(View.GONE);

		metOfferTitle.setTypeface(InorbitApp.getTypeFace());
		metOfferDesc.setTypeface(InorbitApp.getTypeFace());
		metTags.setTypeface(InorbitApp.getTypeFace());
		mbtnChooseImage.setTypeface(InorbitApp.getTypeFace());
		mbtnCapture.setTypeface(InorbitApp.getTypeFace());
		mtvStartPromt.setTypeface(InorbitApp.getTypeFace());
		mtvEndDatePromt.setTypeface(InorbitApp.getTypeFace());
		mtvIsOfferPrompt.setTypeface(InorbitApp.getTypeFace());
		mrbOffer.setTypeface(InorbitApp.getTypeFace());
		mrbPost.setTypeface(InorbitApp.getTypeFace());
		mtvSetPriority.setTypeface(InorbitApp.getTypeFace());

		mbtnChooseImage.setOnClickListener(this);
		mBtnEdit.setOnClickListener(this);
		mBtnReview.setOnClickListener(this);
		mBtnPost.setOnClickListener(this);
		mbtnCapture.setOnClickListener(this);
		mivDeleteImg.setOnClickListener(this);
		mtvStartPromt.setOnClickListener(this);
		mtvEndDatePromt.setOnClickListener(this);
		mtvStartDate.setOnClickListener(this);
		mtvStartTime.setOnClickListener(this);
		mtvEndDate.setOnClickListener(this);
		mtvEndTime.setOnClickListener(this);
		mtvSetPriority.setOnClickListener(this);
		mivImgCal.setOnClickListener(this);
		mrg_offer.setOnCheckedChangeListener(this);


		dateTime=Calendar.getInstance();
		dateTimeCurrent=Calendar.getInstance();

		mSessionManager =new SessionManager(getApplicationContext());

		mActionbar=getSupportActionBar();
		mActionbar.setTitle(getResources().getString(R.string.createBroadCastHeader));
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.show();
	}


	
	/**
	 * 
	 * Get the bundle values 
	 * 
	 */
	void getBundleValues(){
		try{
			Bundle mBundle  = getIntent().getExtras();
			if(mBundle!=null){
				
				/**
				 * 
				 * ArrayList of place id for which offer has to be made
				 * 
				 */
				msarrSelectedStore = mBundle.getStringArrayList("SELECTED_STORE_ID");
				
				/**
				 * 
				 * if mbIsPostEdit = true than this file has been called to edit the offer
				 * if mbIsPostEdit = false than this file has been called to make new offer
				 * 
				 */
				mbIsPostEdit  = mBundle.getBoolean("POST_EDIT");
				
				/**
				 * Offer details when mbIsPostEdit = true
				 * 
				 */
				mPostDetail = mBundle.getParcelable("POST_DETAIL");
				InorbitLog.d("Selected store "+msarrSelectedStore.toString());
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * Check if mall manager is logged in and make layout for them accordingly
	 * If Mall Manager is logged than he has a right to change offer priority 
	 * where as thats not available for store manager
	 * 
	 */
	void setLayout(){

		if(mbIsManagerLoggedIn){
			mivmgSeprate6.setVisibility(View.VISIBLE);
			mtvSetPriority.setVisibility(View.VISIBLE);
		}else{
			mivmgSeprate6.setVisibility(View.GONE);
			mtvSetPriority.setVisibility(View.GONE);
		}
	}

	
	/**
	 * Set the offer details like its Title, Description and other information
	 * 
	 */
	void setPostDetails(){
		try{
			metOfferTitle.setText(mPostDetail.getTitle());
			metOfferDesc.setText(mPostDetail.getDescription());

			String msPriority = mPostDetail.getPriority();
			int miP = Integer.parseInt(msPriority);
			if(miarrPriorityIds.contains(miP)){
				int i = miarrPriorityIds.indexOf(miP);
				mtvSetPriority.setText("Priority: "+msarrPriority.get(i));
				this.msPriority = mPostDetail.getPriority();
			}
			
			
			/**
			 * 
			 * If it a offer than show is Start date and End date time
			 * else set theit visibilty as gone
			 * 
			 */
			if(mPostDetail.getIs_offered().equalsIgnoreCase("1")){

				mrbOffer.setChecked(true);
				mrbPost.setChecked(false);
				
				

				String msStartDate = mPostDetail.getOffer_start_date_time();
				String msEndDate = mPostDetail.getOffer_end_date_time();

				DateFormat strtdt = new SimpleDateFormat("yyyy-MM-dd");
				Date strt_date_con = (Date) strtdt.parse(mPostDetail.getOffer_start_date_time());

				DateFormat enddt = new SimpleDateFormat("yyyy-MM-dd");
				Date end_date_con = (Date) enddt.parse(mPostDetail.getOffer_end_date_time());

				DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy");

				DateFormat  timeFomat=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date startTime = (Date) timeFomat.parse(mPostDetail.getOffer_start_date_time());
				Date endTime = (Date) timeFomat.parse(mPostDetail.getOffer_end_date_time());

				String startdate = strtdt.format(strt_date_con);
				String enddate = strtdt.format(end_date_con);
				

				if(msStartDate.equalsIgnoreCase("0000-00-00T00:00:00GMT+05:30")){
					mbStartDateSelected = false;
					
					mtvStartPromt.setText("<u>Select offer start date</u>");
					
					mtvStartPromt.setText(Html.fromHtml("<u>Select offer start date</u>"));
					
					mtvStartPromt.setVisibility(View.VISIBLE);
					
					mtvStartDate.setVisibility(View.GONE);
					mtvStartTime.setVisibility(View.GONE);
					mtvEndDate.setVisibility(View.GONE);
					mtvEndTime.setVisibility(View.GONE);
					
				}else{
					mbStartDateSelected = true;
					mtvStartDate.setText(targetFormat.format(strt_date_con));	
					
					
					mtvStartPromt.setVisibility(View.VISIBLE);
					mtvStartPromt.setText("From :");
					mtvStartDate.setVisibility(View.VISIBLE);
					mtvStartTime.setVisibility(View.VISIBLE);
				}
				
				mtvEndDate.setText(targetFormat.format(end_date_con));
				mtvStartTime.setText(changeTimeFormat(" "+timeFomat.format(startTime),false));
				mtvEndTime.setText(changeTimeFormat(" "+timeFomat.format(endTime),false));

				//mtvStartDate.setVisibility(View.VISIBLE);
				//mtvStartTime.setVisibility(View.VISIBLE);
				
				mtvEndDate.setVisibility(View.VISIBLE);
				mtvEndTime.setVisibility(View.VISIBLE);
				mtvEndDatePromt.setText("Till :");
				
				//mbStartDateSelected = true;
				mbEndDateSelected = true;
				
			}else{
				mrbOffer.setChecked(false);
				mrbPost.setChecked(true);  
			}

			
			/**
			 * If Image url is blank than set the visibilty of image as gone
			 * else set visibilty as visible
			 */
			if(mPostDetail.getImage_url1().equalsIgnoreCase("")){

				mivThumbPreview.setVisibility(View.GONE);
				mivDeleteImg.setVisibility(View.GONE);
				
				mbtnCapture.setVisibility(View.VISIBLE);
				mbtnChooseImage.setVisibility(View.VISIBLE);

			}else{
				
				mivThumbPreview.setVisibility(View.VISIBLE);
				
				imageLoader.displayImage(getResources().getString(R.string.photo_url)+mPostDetail.getImage_url1(), mivThumbPreview, options, new ImageLoadingListener() {

					@Override
					public void onLoadingStarted(String imageUri, View view) {
						// TODO Auto-generated method stub
						mPbar.setVisibility(View.VISIBLE);
					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						// TODO Auto-generated method stub
						//prog.setVisibility(View.INVISIBLE);
					}

					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						// TODO Auto-generated method stub
						mPbar.setVisibility(View.GONE);
					}

					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						// TODO Auto-generated method stub
						//prog.setVisibility(View.INVISIBLE);
						
					}
				});
				
				/*Picasso.with(mContext).load(getResources().getString(R.string.photo_url)+mPostDetail.getImage_url1())
				
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(mivThumbPreview);*/
				
				if(mbIsPostEdit){
					mivDeleteImg.setVisibility(View.GONE);
					mbtnCapture.setVisibility(View.GONE);
					mbtnChooseImage.setVisibility(View.GONE);
				}else{
					
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	public void onClick(View v) {

		if(v.getId()==mbtnChooseImage.getId()){
			openGallery();
		}
		if (v.getId()==mbtnCapture.getId()) {
			takePictureFromCustomCamera();
			//takePicture();
		}
		if(v.getId()==mivDeleteImg.getId()){
			deleteThisImage();
		}
		if(v.getId()==mtvStartPromt.getId()){
			//selectStartDate();
			chooseStartDate();
		}
		if(v.getId()==mtvStartDate.getId()){
			//selectStartDate();
			chooseStartDate();
		}
		if(v.getId()==mtvStartTime.getId()){
			//selectStartDate();
			chooseStartTime();
		}
		if(v.getId()==mtvEndDatePromt.getId()){
			//selectEndDate();
			chooseEndDate();
		}
		if(v.getId()==mtvEndDate.getId()){
			//selectEndDate();
			chooseEndDate();
		}
		if(v.getId()==mtvEndTime.getId()){
			//selectEndDate();
			chooseEndTime();
		}
		if(v.getId()==mBtnEdit.getId()){
			createOfferView(); 
		}
		if(v.getId()==mBtnPost.getId()){

			if(NetworkCheck.isNetConnected(mContext)){
				if (mPbar.getVisibility() == View.VISIBLE) {
					return;
				}
				
				postThisOffer();	
			}else{
				showToast("No Internet Connection");
			}  

		}
		if(v.getId()==mBtnReview.getId()){
			if(metOfferTitle.getText().toString().length()==0){
				showToast("Please enter the offer title.");
			} else if(metOfferDesc.getText().toString().length()==0){
				showToast("Please enter offer description.");
			} else if(mbIsManagerLoggedIn && msPriority.equalsIgnoreCase("-1")) {
				showToast("Please select offer priority");
			} else if(mrg_offer.getCheckedRadioButtonId()==mrbOffer.getId()){
				if(!mbStartDateSelected || !mbEndDateSelected){
					showToast("Please provide offer start date and end date.");
				}else{
					reviewOfferView();
				}
			} else{
				reviewOfferView();
			}

		}

		if(v.getId()==mivImgCal.getId()){
			chooseStartDate();
		}

		if(v.getId()==mtvSetPriority.getId()){
			if(isReview){
				
			}else{
				showPriorityDialog();	
			}
			
		}

	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		try{
			MenuItem extra=menu.add("Log Out");
			extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return true;
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getTitle().toString().equalsIgnoreCase("Help")){
			startHelperActivity();
		}else{
			finishto();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}

		return true;
	}

	
	/**
	 * 
	 * Dialog to select the offer priority
	 * only availble for mall manager
	 */
	public void showPriorityDialog(){
		

		final Dialog mdPriorityDialog = new Dialog(mContext);
		mdPriorityDialog.setContentView(R.layout.dailog_interestedareas);
		mdPriorityDialog.setTitle("Offer Priority");
		mdPriorityDialog.setCancelable(true);
		mdPriorityDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		ListView mLvPriorityList = (ListView) mdPriorityDialog.findViewById(R.id.interestedAreaDialogList);
		Button mBdoneBttn			= (Button) mdPriorityDialog.findViewById(R.id.updateInterestedAreaBtn);
		mBdoneBttn.setVisibility(View.GONE);
		mLvPriorityList.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, msarrPriority));
		mLvPriorityList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				mtvSetPriority.setText("Priority : "+msarrPriority.get(pos));
				msPriority = miarrPriorityIds.get(pos)+"";
				mdPriorityDialog.dismiss();
			}
		});
		mdPriorityDialog.show();

	} 

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {

		try{
			MenuItem extra=menu.add("Help");
			extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return super.onCreateOptionsMenu(menu);
	}

	
	/**
	 * Show the helper on how to create the offer
	 * 
	 */
	private void startHelperActivity(){
		ArrayList<Integer> miarrImages = new ArrayList<Integer>(); 
		miarrImages.add(R.drawable.offer_title);
		miarrImages.add(R.drawable.offer_describe);
		miarrImages.add(R.drawable.offer_picture);
		miarrImages.add(R.drawable.offer_tag);
		miarrImages.add(R.drawable.offer_check);
		miarrImages.add(R.drawable.offer_start_end_time);

		ArrayList<String> msarrHelperText = new ArrayList<String>(); 
		msarrHelperText.add("Write the attractive offer title");
		msarrHelperText.add("Describe your offer in few lines");
		msarrHelperText.add("Attach the picture for this offer");
		msarrHelperText.add("Tag this offer, this will help customers to search.");
		msarrHelperText.add("Make sure whether its an offer or normal post");
		msarrHelperText.add("Select the offer start date and end date.");


		Intent intent = new Intent(mContext, AppHelperPager.class);
		intent.putIntegerArrayListExtra("Helper_Images_Arr", miarrImages);
		intent.putStringArrayListExtra("Helper_Text", msarrHelperText);
		intent.putExtra("Parent_Back", R.drawable.offer_main);
		intent.putExtra("Title", mActionbar.getTitle().toString());
		startActivity(intent);
		overridePendingTransition(0, 0);
	}

	
	/**
	 * Set the layout parameters to create the offer
	 * 
	 */
	void createOfferView(){
		if (mPbar.getVisibility() == View.VISIBLE) {
			return;
		}
		isReview = false;
		mBtnReview.setVisibility(View.VISIBLE);
		mBtnEdit.setVisibility(View.GONE);
		mBtnPost.setVisibility(View.GONE);

		if(mrg_offer.getCheckedRadioButtonId() == mrbOffer.getId()){
			mrlDate.setVisibility(View.VISIBLE);
			mivImgCal.setVisibility(View.VISIBLE);
		}else if(mrg_offer.getCheckedRadioButtonId() == mrbPost.getId()){
			mrlDate.setVisibility(View.GONE);
		}
		mllOffer.setVisibility(View.VISIBLE);
		mllImageOption.setVisibility(View.VISIBLE);
		mivmgSeprate4.setVisibility(View.VISIBLE);

		if(mbIsImagePost){
			mivThumbPreview.setVisibility(View.VISIBLE);
			mivDeleteImg.setVisibility(View.VISIBLE);

		}else if(mbIsPostEdit && mbIsSameImage){
			mivThumbPreview.setVisibility(View.VISIBLE);
			mivDeleteImg.setVisibility(View.GONE);
		}else{
			mivThumbPreview.setVisibility(View.GONE);
			mivDeleteImg.setVisibility(View.GONE);  
		}
		
		if(mbIsPostEdit){
			//mivDeleteImg.setVisibility(View.GONE);  
		}
				
		
		metTags.setVisibility(View.VISIBLE);
		metOfferTitle.setFocusableInTouchMode(true);
		//metOfferTitle.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);
		metOfferDesc.setFocusableInTouchMode(true);
		//metOfferDesc.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);

		mtvEndDate.setClickable(true);
		mtvEndTime.setClickable(true);
		mtvEndDatePromt.setClickable(true);
		mtvStartDate.setClickable(true);
		mtvStartTime.setClickable(true);
		mtvStartPromt.setClickable(true);
		metTags.setFocusable(true);
		metTags.setClickable(true);
		mivImgCal.setVisibility(View.VISIBLE);

		mtvEndDate.setBackgroundColor(getResources().getColor(R.color.inorbit_list_background));
		mtvEndTime.setBackgroundColor(getResources().getColor(R.color.inorbit_list_background));
		mtvStartDate.setBackgroundColor(getResources().getColor(R.color.inorbit_list_background));
		mtvStartTime.setBackgroundColor(getResources().getColor(R.color.inorbit_list_background));
		
		mtvSetPriority.setClickable(true);

	}

	
	/**
	 * Set the layout parameters to view the offer
	 */
	void reviewOfferView(){
		isReview = true;
		mBtnReview.setVisibility(View.GONE);
		mBtnEdit.setVisibility(View.VISIBLE);
		mBtnPost.setVisibility(View.VISIBLE);
		mllOffer.setVisibility(View.GONE);
		mllImageOption.setVisibility(View.GONE);
		mivDeleteImg.setVisibility(View.GONE);
		mivmgSeprate4.setVisibility(View.GONE);
		if(mbIsImagePost){
			mivThumbPreview.setVisibility(View.VISIBLE);
			mivmgSeprate3.setVisibility(View.VISIBLE);
		}else{
			mivThumbPreview.setVisibility(View.GONE);
			mivmgSeprate3.setVisibility(View.GONE);
		}
		
		if(mbIsPostEdit && mbIsSameImage){
			mivThumbPreview.setVisibility(View.VISIBLE);
			
			imageLoader.displayImage(getResources().getString(R.string.photo_url)+mPostDetail.getImage_url1(), mivThumbPreview, options, new ImageLoadingListener() {

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					// TODO Auto-generated method stub
					mPbar.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view,
						FailReason failReason) {
					// TODO Auto-generated method stub
					//prog.setVisibility(View.INVISIBLE);
					mPbar.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					// TODO Auto-generated method stub
					mPbar.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					// TODO Auto-generated method stub
					//prog.setVisibility(View.INVISIBLE);
					mPbar.setVisibility(View.GONE);
				}
			});
			
			/*Picasso.with(mContext).load(getResources().getString(R.string.photo_url)+mPostDetail.getImage_url1())
			.placeholder(R.drawable.ic_launcher)
			.error(R.drawable.ic_launcher)
			.into(mivThumbPreview);*/
			//mivDeleteImg.setVisibility(View.VISIBLE);
		} else if(mbIsImagePost){
			mivThumbPreview.setVisibility(View.VISIBLE);
		}else{
			mivThumbPreview.setVisibility(View.GONE);
		}
		
		if(metTags.getText().toString().equalsIgnoreCase("")){
			metTags.setVisibility(View.GONE);
		}else{
			metTags.setVisibility(View.VISIBLE);
			metTags.setFocusable(false);
			metTags.setClickable(false);
		}
		

		//metOfferTitle.setInputType(InputType.TYPE_NULL);
		metOfferTitle.setFocusable(false);
		//metOfferDesc.setInputType(InputType.TYPE_NULL);
		metOfferDesc.setFocusable(false);

		mivImgCal.setVisibility(View.GONE);
		mtvEndDate.setClickable(false);
		mtvEndTime.setClickable(false);
		mtvEndDatePromt.setClickable(false);
		mtvStartDate.setClickable(false);
		mtvStartTime.setClickable(false);
		mtvStartPromt.setClickable(false);
		mtvSetPriority.setClickable(false);

		mtvEndDate.setBackgroundColor(Color.WHITE);
		mtvEndTime.setBackgroundColor(Color.WHITE);
		mtvStartDate.setBackgroundColor(Color.WHITE);
		mtvStartTime.setBackgroundColor(Color.WHITE);

	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		if(checkedId == mrbOffer.getId()){
			mrlDate.setVisibility(View.VISIBLE);
		}else if(checkedId == mrbPost.getId()){
			mrlDate.setVisibility(View.GONE);
		}
	}

	
	/**
	 * Open the gallery of the device to select the picture for the offer
	 * 
	 */
	private void openGallery() {
		
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_POSTGALLERY);
	}

	
	/**
	 * OPen the custom camera to click the picture for the offer
	 * 
	 */
	private void takePictureFromCustomCamera() {
		try {
			/*Intent intent = new Intent(mContext,CameraView.class);
			intent.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);
			startActivityForResult(intent, REQUEST_CODE_CUSTOMCAMERA);*/
			
			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			if (intent.resolveActivity(getPackageManager()) != null) {
				startActivityForResult(intent,REQUEST_CODE_CUSTOMCAMERA);
			} else {
				showToast("No Supporting camera app found");
			}
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
			InorbitLog.d("cannot take picture");
		}
	}

	/*private void takePicture() {
		try {  
			Intent i = new Intent(mContext, PhotoActivity.class);
			i.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);
			startActivityForResult(i, REQUEST_CODE_TAKE_PICTURE);
		} catch (ActivityNotFoundException e) {

			Log.d("", "cannot take picture", e);
		}
	}*/

	
	/**
	 * Delete/ change the selected image 
	 * 
	 */
	private void deleteThisImage(){
		mivThumbPreview.setVisibility(View.GONE);
		//mReviewImage.setVisibility(View.GONE);
		mByteArr=null;
		FILE_PATH="";
		FILE_NAME="";
		msEncodedpath = "";
		mbIsImagePost =false;
		mbIsSameImage = false;
		mivDeleteImg.setVisibility(View.GONE);
		if(mbIsPostEdit){
			mivmgSeprate3.setVisibility(View.GONE);
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		InorbitLog.d("Request Code "+requestCode);
		try{
			switch (requestCode) {
			
			
			
			/**
			 * 
			 * Activity return here from custom camera.
			 * Request code gets called when user select to take picture for the offer
			 * 
			 */
			case REQUEST_CODE_CUSTOMCAMERA:
				
				if(resultCode == Activity.RESULT_OK){/*
					try{
						Bitmap bitmap = null;
						CameraImageSave cameraImageSave = new CameraImageSave();
						bitmap = cameraImageSave.getBitmapFromFile(480, 480);
						if(bitmap != null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							mByteArr = stream.toByteArray();
							cameraImageSave.deleteFromFile();
							cameraImageSave = null;
							bitmap = null;
						}
						setPhoto(BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length));
					}
					catch(Exception e){
						e.printStackTrace();
					}*/	
					
					Uri dataUri = data.getData();
					if (dataUri == null) {
						Cursor cursor = getContentResolver().query(Media.EXTERNAL_CONTENT_URI,
								new String[]{Media.DATA, Media.DATE_ADDED, MediaStore.Images.ImageColumns.ORIENTATION},
								Media.DATE_ADDED, null, Media.DATE_ADDED+" ASC");
						if(cursor != null && cursor.moveToFirst()) {
						    do {
						        dataUri = Uri.parse(cursor.getString(cursor.getColumnIndex(Media.DATA)));
						    } while(cursor.moveToNext());
						    cursor.close();
						}
					}
					
					angularRotation = getOrientation(dataUri);
					if (angularRotation != 0)
						rotateImage = true;
					else 
						rotateImage = false;
					Back t = new Back();
					t.execute(dataUri);
				}
				break;

				/**
				 * 
				 * Activity return here from gallery.
				 * Request code gets called when user click on "Select from Gallery" option for the offer image
				 * 
				 */	
			case REQUEST_CODE_POSTGALLERY:
				if(resultCode == Activity.RESULT_OK) {
					try {
						Uri imageUri = data.getData();

						angularRotation = getOrientation(data.getData());
						if (angularRotation != 0)
							rotateImage = true;
						else 
							rotateImage = false;
						Back t = new Back();
						t.execute(imageUri);
					}catch(Exception e){
						InorbitLog.e("Exception at Create broacast class");
						e.printStackTrace();
					}
					
					/*Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c = getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();
					
					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);
					c.close();
					
					
					 * Crop that image to 1:1
					 * 
					 *
					cropCapturedImage(imageUri);*/
				}

				break;



				/**
				 * 
				 * Activity return here when merchant complete the cropping of the image selected frm the gallery.
				 * 
				 */	
			case REQUEST_CODE_POSTCROPGALLERY:
				if(resultCode == Activity.RESULT_OK){
					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
					FILE_PATH="/sdcard/temp_photo.jpg";
					
					/*try {
>>>>>>> master
						Uri imageUri = data.getData();
						Back t = new Back();
						t.execute(imageUri);
					} catch(Exception e){
						InorbitLog.e("Exception at Create broacast class");
						e.printStackTrace();
<<<<<<< HEAD
					}
=======
					}*/
					
					/*//Create an instance of bundle and get the returned data
					Bundle extras = data.getExtras();
					//get the cropped bitmap from extras
					Bitmap thePic = extras.getParcelable("data");

					CameraImageSave cameraSaveImage = new CameraImageSave();

					try{
						cameraSaveImage.cretaeFile();
						cameraSaveImage.saveBitmapToFile(thePic);
					}catch(Exception ex){
						ex.printStackTrace();
					}

					try{
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480, 480);
						if(bitmap!=null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
							cameraSaveImage.deleteFromFile();
							mByteArr = stream.toByteArray();
							bitmap = null;
							cameraSaveImage = null;
						}
						
						setPhoto(BitmapFactory.decodeByteArray(mByteArr , 0,mByteArr.length));
					}
					catch(Exception e){
						InorbitLog.e("Exception at Create broacast class");
						e.printStackTrace();
					}*/
				}


				break;

			case REQUEST_CODE_TAKE_PICTURE:
				if(resultCode == Activity.RESULT_OK) {
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};
					
					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();
					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);
					c.close();
					cropCapturedImage(imageUri);
				}
			}
			super.onActivityResult(requestCode, resultCode, data);
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}
	

	public int getOrientation(Uri selectedImage) {
	    int orientation = 0;
	    final String[] projection = new String[]{MediaStore.Images.Media.ORIENTATION};      
	    final Cursor cursor = mContext.getContentResolver().query(selectedImage, projection, null, null, null);
	    if(cursor != null) {
	        final int orientationColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION);
	        if(cursor.moveToFirst()) {
	            orientation = cursor.isNull(orientationColumnIndex) ? 0 : cursor.getInt(orientationColumnIndex);
	        }
	        cursor.close();
	    }
	    return orientation;
	}
	
	class Back extends AsyncTask<Uri, String, Bitmap> {
		//byte[] mByteArr = null;
		String picturePath = "";
		Uri uriForImage = null;
		byte[] localImgArr = null;
		
		@Override
		protected void onPreExecute() {
			mPbar.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}
		
		@Override
		protected Bitmap doInBackground(Uri... params) {/*
			try {
				String encodedpath="";
				
				SessionManager session = new SessionManager(mContext);
				uriForImage = params[0].getData(); 
				InputStream iStream =   getContentResolver().openInputStream(uriForImage);
				mByteArr = getBytes(iStream);
				if (mByteArr.length < 2000000) {
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c = getContentResolver().query(uriForImage, filePathColumn, null, null, null);
					c.moveToFirst();
					
					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					picturePath=c.getString(columnIndex);
					c.close();
					
					BitmapFactory.Options opt =  new BitmapFactory.Options();*/

			try {
				String encodedpath="";
				
				SessionManager session = new SessionManager(mContext);
				uriForImage = params[0];
				String[] results = getRealPathFromURI(uriForImage);
				InputStream iStream = new FileInputStream(results[0]);//getContentResolver().openInputStream(uriForImage);
				mByteArr = getBytes(iStream);
				if (mByteArr.length < 2000000) {
					/*String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c = getContentResolver().query(uriForImage, filePathColumn, null, null, null);
					c.moveToFirst();
					
					int columnIndex=c.getColumnIndex(filePathColumn[0]);*/
					picturePath= results[0];
					FILE_TYPE = results[1];
					//c.getString(columnIndex);
					//c.close();
					
					BitmapFactory.Options opt =  new BitmapFactory.Options();
					
				    opt.inJustDecodeBounds = true;
				    
				    BitmapFactory.decodeFile(picturePath, opt);
			        /*if (mByteArr.length > getResources().getInteger(R.integer.thumbnail_size_limit))
			        	opt.inSampleSize = 8;*/
				    
				    opt.inSampleSize = calculateInSampleSize(opt, 480, 480);
				    opt.inJustDecodeBounds = false;
				    
				    Bitmap thePic = BitmapFactory.decodeFile(picturePath, opt);
				    return thePic;
				    /*CameraImageSave cameraSaveImage = new CameraImageSave();
			    	
					try{
						cameraSaveImage.cretaeFile();
						cameraSaveImage.saveBitmapToFile(thePic);
					}catch(Exception ex){
						ex.printStackTrace();
					}

					try{
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480, 480);
						cameraSaveImage.deleteFromFile();
						return bitmap;
					}
					catch(Exception e){
						InorbitLog.e("Exception at Create broacast class");
						e.printStackTrace();
					}*/
				} else {
					publishProgress("Image is too big");
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			return null;
		}
		
		public int calculateInSampleSize (BitmapFactory.Options options, int reqWidth, int reqHeight) {
		    // Raw height and width of image
		    final int height = options.outHeight;
		    final int width = options.outWidth;
		    int inSampleSize = 1;
		    
		    if (height > reqHeight || width > reqWidth) {
		    	
		        final int halfHeight = height / 2;
		        final int halfWidth = width / 2;
		        
		        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
		        // height and width larger than the requested height and width.
		        while ((halfHeight / inSampleSize) > reqHeight
		                && (halfWidth / inSampleSize) > reqWidth) {
		            inSampleSize *= 2;
		        }
		    }
	
		    return inSampleSize;
		}
		
		private String[] getRealPathFromURI(Uri contentURI) {
		    String[] result = new String[2];
		    Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
		    if (cursor == null) { // Source is Dropbox or other similar local file path
		        result[0] = contentURI.getPath();
		        result[1] = "image/jpeg";
		    } else { 
		        cursor.moveToFirst(); 
		        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
		        result[0] = cursor.getString(idx);
		        result[1] = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.MIME_TYPE));
		        cursor.close();
		    }
		    return result;
		}
		
		public byte[] getBytes(InputStream inputStream) throws IOException {
		      ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		      int bufferSize = 1024;
		      byte[] buffer = new byte[bufferSize];

		      int len = 0;
		      while ((len = inputStream.read(buffer)) != -1) {
		        byteBuffer.write(buffer, 0, len);
		      }
		      return byteBuffer.toByteArray();
		}
		@Override
		protected void onProgressUpdate(String... values) {
			showToast(""+values[0]);
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			mPbar.setVisibility(View.GONE);
			if (result != null) {
				setPhoto(result, picturePath);
			}
		}
	}
	

	/**
	 * Crop the selected picture to 1:1
	 * 
	 * @param picUri
	 */
	public void cropCapturedImage(Uri picUri){
		//call the standard crop action intent 
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		//indicate image type and Uri of image
		cropIntent.setDataAndType(picUri, "image/*");
		//set crop properties
		cropIntent.putExtra("crop", "true");
		//indicate aspect of desired crop
		cropIntent.putExtra("aspectX", 1);
		cropIntent.putExtra("aspectY", 1);
		//indicate output X and Y
		cropIntent.putExtra("outputX", 256);
		cropIntent.putExtra("outputY", 256);
		//retrieve data on return
		cropIntent.putExtra("return-data", true);
		//start the activity - we handle returning in onActivityResult
		startActivityForResult(cropIntent, REQUEST_CODE_POSTCROPGALLERY);
	}

	void setPhoto(Bitmap bitmap, String picturePath){
		try{
			if (FILE_TYPE == null || FILE_TYPE.equalsIgnoreCase("")) 
				FILE_TYPE="image/jpeg";
			
			FILE_NAME="temp_photo.jpg";
			FILE_PATH="/sdcard/temp_photo.jpg";
			
			if (rotateImage)
				bitmap = rotateImage(bitmap, picturePath);
			
			mivThumbPreview.setImageBitmap(bitmap);
			mivThumbPreview.setScaleType(ScaleType.FIT_CENTER);
			//mReviewImage.setImageBitmap(bitmap);
			//mReviewImage.setScaleType(ScaleType.FIT_XY);
			mivDeleteImg.setVisibility(View.VISIBLE);
			mivThumbPreview.setVisibility(View.VISIBLE);
			//mReviewImage.setVisibility(View.VISIBLE);
			
			//mEditButton.setText(getString(R.string.edit));
			//mEditButton.setTextColor(Color.WHITE);
			//mEditButton.setGravity(Gravity.CENTER);
			
			mbIsImagePost=true;
			mbIsSameImage=false;
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/**
	 * Date picker for the offer start date 
	 * 
	 */
	public void chooseStartDate(){

		dateTime.add(Calendar.DAY_OF_MONTH, 1);
		new DatePickerDialog(mContext, startDate, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
		DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
				dateTime.set(Calendar.YEAR,year);
				dateTime.set(Calendar.MONTH, monthOfYear);
				dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				updateStartDate();
			}
		};

	}

	/**
	 * Time picker for the offer end time
	 * 
	 */
	public void chooseEndTime(){
		new TimePickerDialog(mContext, endTime, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), false).show();
	}

	
	/**
	 * Date picker for offe end date
	 * 
	 */
	public void chooseEndDate(){

		dateTime.add(Calendar.DAY_OF_MONTH, 1);
		new DatePickerDialog(mContext, endDate, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
		DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
				dateTime.set(Calendar.YEAR,year);
				dateTime.set(Calendar.MONTH, monthOfYear);
				dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				updateEndDate();
			}
		};

	}

	/**
	 * Time picker for offer start time
	 * 
	 */
	public void chooseStartTime(){
		new TimePickerDialog(mContext, startTime, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), false).show();
	}

	DatePickerDialog.OnDateSetListener startDate=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			dateTime.set(Calendar.YEAR,year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateStartDate();

		}
	};

	TimePickerDialog.OnTimeSetListener startTime=new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTime.set(Calendar.MINUTE,minute);
			updateStartTime();
		}
	};

	DatePickerDialog.OnDateSetListener endDate=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			dateTime.set(Calendar.YEAR,year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateEndDate();

		}
	};

	TimePickerDialog.OnTimeSetListener endTime=new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTime.set(Calendar.MINUTE,minute);
			updateEndTime();
		}
	};

	
	
	private void updateStartTime() {

		DateFormat  dt2=new SimpleDateFormat(" hh:mm a");
		miliseconds=dateTime.getTimeInMillis();
		//offerTime_start.setText(dt2.format(dateTime.getTime()));
		mtvStartTime.setText(dt2.format(dateTime.getTime()));
	}

	private void updateStartDate() {

		Log.i("Current Date", "Current Date "+dateTimeCurrent.get(Calendar.YEAR)+" "+dateTimeCurrent.get(Calendar.MONTH)+" "+dateTimeCurrent.get(Calendar.DAY_OF_MONTH));
		Log.i("Current Date", "Current Date changed "+dateTime.get(Calendar.YEAR)+" "+dateTime.get(Calendar.MONTH)+" "+dateTime.get(Calendar.DAY_OF_MONTH));
		
		Date d1=dateTimeCurrent.getTime();
		Date d2=dateTime.getTime();

		if(dateDiff(d2, d1)<0){
			showToast("Start date cannot be less than current date");

		}else{			
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dtReview=new SimpleDateFormat("dd-MMM-yyyy");
			//offerDate_start.setText(dt.format(dateTime.getTime()));
			mtvStartDate.setText(dtReview.format(dateTime.getTime()));
			mtvStartDate.setVisibility(View.VISIBLE);
			mtvStartTime.setVisibility(View.VISIBLE);
			mtvStartPromt.setText("From :");
			mbStartDateSelected = true;

		}
	}

	private void updateEndTime() {

		DateFormat  dt2=new SimpleDateFormat(" hh:mm a");
		miliseconds=dateTime.getTimeInMillis();
		//offerTime_end.setText(dt2.format(dateTime.getTime()));
		mtvEndTime.setText(dt2.format(dateTime.getTime()));


	}

	private void updateEndDate() {

		Log.i("Current Date", "Current Date "+dateTimeCurrent.get(Calendar.YEAR)+" "+dateTimeCurrent.get(Calendar.MONTH)+" "+dateTimeCurrent.get(Calendar.DAY_OF_MONTH));
		Log.i("Current Date", "Current Date changed "+dateTime.get(Calendar.YEAR)+" "+dateTime.get(Calendar.MONTH)+" "+dateTime.get(Calendar.DAY_OF_MONTH));
		Date d1=dateTimeCurrent.getTime();
		Date d2=dateTime.getTime();

		if(dateDiff(d2, d1)<0){
			showToast("End date cannot be less than current date/start date");

		}else{			
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dtReview=new SimpleDateFormat("dd-MMM-yyyy");
			mtvEndDate.setText(dtReview.format(dateTime.getTime()));
			mtvEndDate.setVisibility(View.VISIBLE);
			mtvEndTime.setVisibility(View.VISIBLE);
			mtvEndDatePromt.setText("Till :");
			mbEndDateSelected = true;

		}
	}

	/**
	 * Check the difference between two dates
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public long dateDiff(Date d1,Date d2){
		long datediff=0;
		datediff=(d1.getTime()-d2.getTime())/(24 * 60 * 60 * 1000);
		Log.i("daysBetween", "daysBetween "+datediff);
		return datediff;
	}

	void showToast(String mStr){
		Toast.makeText(mContext, mStr, 0).show();
	}

	
	/**
	 * 
	 * Network request to send the offer details on server
	 * 
	 */
	void postThisOffer(){
		if (mPbar.getVisibility() == View.VISIBLE) {
			return;
		}
		mPbar.setVisibility(View.VISIBLE);
		
		InorbitLog.d("Posting this offer");
		JSONObject json = new JSONObject();
		
		JSONArray jsonArray=new JSONArray(new ArrayList<String>(new LinkedHashSet<String>(msarrSelectedStore)));
		try{
			json.put("place_id", jsonArray);
			json.put("title", metOfferTitle.getText().toString());//title
			json.put("description", metOfferDesc.getText().toString());//body
			json.put("url", "");//url
			json.put("tags", metTags.getText().toString());//tags
			json.put("type", "photo");//type
			json.put("state", "published");//state
			json.put("format", "html");//format
			
			if(mrg_offer.getCheckedRadioButtonId()==mrbPost.getId()){
				
				json.put("is_offered", "0");
				json.put("offer_date_time", "");
				json.put("offer_start_date_time", "");
				
			}else{
				
				String startDateAndTime = changeDateFormat(mtvStartDate.getText().toString())+changeTimeFormat(mtvStartTime.getText().toString(),true);
				String endDateAndTime = changeDateFormat(mtvEndDate.getText().toString())+changeTimeFormat(mtvEndTime.getText().toString(),true);
				json.put("is_offered","1");
				json.put("offer_date_time", endDateAndTime);//offer_date_time + offer_time
				json.put("offer_start_date_time", startDateAndTime);//offer_date_time + offer_time
			}
			
			json.put("user_id", getUserId()); // user_id
			json.put("auth_id", getAuthId()	); // auth_id
			
			
			/**
			 * If mall manager is logged in than send offer priority of the offer on server
			 * 
			 */
			if(mbIsManagerLoggedIn){
				json.put("priority", msPriority);
			}
			
			
			if(mbIsManagerLoggedIn && mbIsPostEdit){
				json.put("id", mPostDetail.getId());
				if(mbIsSameImage){
					json.put("image_url", mPostDetail.getImage_url1());
				}else{
					//json.put("image_url", "");
				}

			}
			
			/**
			 * If offer has image attached with it than set the parameters for that
			 * 
			 */
			if(mbIsImagePost){
				if(mbIsManagerLoggedIn && mbIsPostEdit){
					json.put("image_url", "");
				}	
				JSONArray fileArray=new JSONArray();
				JSONObject fileobject = new JSONObject();
				if(mbIsImagePost==true){
					if(mByteArr!=null){
						String user_file=Base64.encodeToString(mByteArr, Base64.DEFAULT);
						fileobject.put("filename", "temp_photo.jpg");
						fileobject.put("filetype", "image/jpeg");
						fileobject.put("userfile", user_file);
						fileobject.put("title", "");
						fileobject.put("angular_rotation", angularRotation+"");
						fileArray.put(fileobject);
						json.put("file",fileArray);
					}
				}
				//fileArray.put(fileobject);
			}else if(mbIsSameImage==false){
				if(mbIsManagerLoggedIn && mbIsPostEdit){
					json.put("image_url", "");
				}
			}

		}catch(JSONException jex){
			jex.printStackTrace();
		}catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		Log.i("JSON", json.toString());
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		MyClass myClass = new MyClass(mContext);
		
		/**
		 * Different api's for offer update and new offer
		 * 
		 */
		
		//writeToFile(json.toString());
		if(mbIsManagerLoggedIn && mbIsPostEdit) {
			headers.put("X-HTTP-Method-Override", "PUT");
			myClass.postRequest(RequestTags.Tag_UpdateOffer, headers, json);
		} else {
			myClass.postRequest(RequestTags.TagCreateBroadcast, headers, json);	
		}
	}
	
	void getBitMapFromUrl(){
		
		ImageRequest imgReuqest = new ImageRequest(
				getResources().getString(R.string.photo_url)+mPostDetail.getImage_url1(),
				new Listener<Bitmap>() {

					@Override
					public void onResponse(Bitmap response) {
						
					}
				},
				480, 
				480,
				Bitmap.Config.RGB_565, 
				new ErrorListener() {
					
					@Override
					public void onErrorResponse(VolleyError error) {
						
					}
				});
	}

	private String getUserId(){
		HashMap<String,String>user_details=mSessionManager.getUserDetails();
		return user_details.get("user_id").toString();

	}

	private String getAuthId(){
		HashMap<String,String>user_details=mSessionManager.getUserDetails();
		return user_details.get("auth_id").toString();
	}

	
	/**
	 * Change the date format 
	 * 
	 * @param msDateToConvert
	 * @return
	 */
	private String changeDateFormat(String msDateToConvert){

		String msOldFormat="dd-MMMM-yyyy";
		String msNewFormat = "yyyy-MM-dd";
		String msNewDate = "";

		try{
			SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
			Date d = sdf.parse(msDateToConvert);
			sdf.applyPattern(msNewFormat);
			msNewDate = sdf.format(d);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return msNewDate;
	}

	
	/**
	 * Chane the time format
	 * 
	 * @param msTimeToConvert
	 * @param mbSubmitPattern
	 * @return
	 */
	private String changeTimeFormat(String msTimeToConvert,boolean mbSubmitPattern){

		String msNewTime;
		String msNewFormat;
		String msOldFormat;

		if(mbSubmitPattern){
			msOldFormat=" hh:mm a";
			msNewFormat = " HH:mm";
			msNewTime = "";
		}else{
			msNewFormat=" hh:mm a";
			//msOldFormat = " HH:mm";
			msOldFormat = "yyyy-MM-dd'T'HH:mm:ss";

			msNewTime = "";
		}


		try{
			SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
			Date d = sdf.parse(msTimeToConvert);
			sdf.applyPattern(msNewFormat);
			msNewTime = sdf.format(d);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return msNewTime;
	}

	byte[] imageTobyteArray(String path){	

		byte[] b = null ;
		if(!path.equals("") || path.length()!=0){
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			options.inDither=true;//optional
			options.inPreferredConfig=Bitmap.Config.RGB_565;//optional
			Bitmap bm = BitmapFactory.decodeFile(path,options);
			options.inSampleSize = calculateInSampleSize(options, 320, 320);
			options.inJustDecodeBounds = false;
			bm=BitmapFactory.decodeFile(path,options);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();  
			bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object   
			b= baos.toByteArray();
		}
		return b;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		if (height > reqHeight || width > reqWidth) {
			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	
	/**
	 * Broadcast receiver of postThisOffer() when new offer is being created
	 * 
	 * 
	 * @author Nitin
	 *
	 */
	class BroadCastOffer extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String success = bundle.getString("SUCCESS");
					String code = "";
					String msg = bundle.getString("MESSAGE");
					mPbar.setVisibility(View.GONE);
					if(success.equalsIgnoreCase("true")){
						EventTracker.logEvent(getResources().getString(R.string.newPost_Posted), false);
						Toast.makeText(context, msg, 0).show();
						if(mbIsManagerLoggedIn && mbIsPostEdit){
							finish();
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						} else {
							
							Intent intent1=new Intent(mContext, ViewPostNew.class);
							//intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							//Intent intent1=new Intent();
							intent1.putExtra("STORE_ID",msarrSelectedStore.get(0));
							//startActivityForResult(intent1,2);
							startActivity(intent1);
							finish();
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}
						
					}else if(success.equalsIgnoreCase("false")){
						String message=bundle.getString("MESSAGE");
						Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
						EventTracker.logEvent(getResources().getString(R.string.newPost_PostFailed), false);
						code = bundle.getInt("CODE")+"";
						if(code.equalsIgnoreCase("-116")){
							//logOutUser();
							Toast.makeText(mContext, "Please log out and login again", 1).show();
						}
						
						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){
							int code1 = bundle.getInt("CODE");
							String message1=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CategorySearch");
							}
							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}
						//Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
					}

				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}
	
	/**
	 * Broadcast receiver of postThisOffer() when any offer is being updated
	 * 
	 * 
	 * @author Nitin
	 *
	 */
	class BroadCastOfferUpdate extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String success = bundle.getString("SUCCESS");
					String code = "";
					String msg = bundle.getString("MESSAGE");
					mPbar.setVisibility(View.GONE);
					if(success.equalsIgnoreCase("true")){
						EventTracker.logEvent(getResources().getString(R.string.newPost_Posted), false);
						Toast.makeText(context, msg, 0).show();
						if(mbIsManagerLoggedIn && mbIsPostEdit){
							finish();
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						} else {
							
							Intent intent1=new Intent(mContext, ViewPostNew.class);
							//intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							//Intent intent1=new Intent();
							intent1.putExtra("STORE_ID",msarrSelectedStore.get(0));
							//startActivityForResult(intent1,2);
							startActivity(intent1);
							finish();
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}
						
					}else if(success.equalsIgnoreCase("false")){
						String message=bundle.getString("MESSAGE");
						Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
						EventTracker.logEvent(getResources().getString(R.string.newPost_PostFailed), false);
						//code = bundle.getString("CODE");
						//if(code.equalsIgnoreCase("-116")){
							//logOutUser();
					//	}

						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){
							int code1 = bundle.getInt("CODE");
							String message1=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "CategorySearch");
							}
							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}
						//Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
					}

				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	private void logOutUser(){
		//String code = bundle.getString("CODE");
		//if(code.equalsIgnoreCase("-116")){
			Intent intent1=new Intent();
			startActivityForResult(intent1,2);
			finish();
		//}
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
		try{
			if(mbroadcastOfferUpdate!=null){
				mContext.unregisterReceiver(mbroadcastOfferUpdate);
			}
			if(mbroadcastOffer!=null){
				mContext.unregisterReceiver(mbroadcastOffer);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter filter = new IntentFilter(RequestTags.TagCreateBroadcast);
			IntentFilter updateFilter = new IntentFilter(RequestTags.Tag_UpdateOffer);
			mContext.registerReceiver(mbroadcastOffer = new BroadCastOffer(), filter);
			mContext.registerReceiver(mbroadcastOfferUpdate = new BroadCastOfferUpdate(), updateFilter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}
	
	void finishto(){
		if(metOfferTitle.getText().toString().length()!=0 || metOfferDesc.getText().toString().length()==0){
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle("Inorbit");
			alertDialogBuilder3
			.setMessage("Do you want to discard this offer?")
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

				}
			})
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
				}
			})
			;

			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();

		}else if(!mPbar.isShown()){

			//Intent intent=new Intent(mContext,MerchantHome.class);
			//startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}else{
			//MyToast.showToast(mContext, "Please wait while posting",3);
		}
	}
	
	private Bitmap rotateImage(Bitmap bmp, String path) {
        // TODO Auto-generated method stub

        Matrix matrix=new Matrix();

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String orientstring = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientstring != null ? Integer.parseInt(orientstring) : ExifInterface.ORIENTATION_NORMAL;
        int rotateangle = 0;
        if(orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            rotateangle = 90;
        } if(orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            rotateangle = 180;
        }if(orientation == ExifInterface.ORIENTATION_ROTATE_270) {
        	//mByteArr = rotateByteArray(mByteArr);
            rotateangle = 270;
        }
        
        //imageView.setScaleType(ScaleType.CENTER_CROP);   //required
        matrix.setRotate(rotateangle);
        //imageView.setImageMatrix(matrix);



        Bitmap newBit = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        
        //imageView.setImageBitmap(newBit);
        return newBit;

    }
	
	private byte [][]  rotateByteArray(byte [][] input) {
		int n =input.length;
		int m = input[0].length;
		byte [][] output = new byte [m][n];

		for (int i=0; i<n; i++)
			for (int j=0;j<m; j++)
				output [j][n-1-i] = input[i][j];
		return output;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishto();
	}
	
	private void writeToFile(String data) {
		
		try {
			File myFile = new File("/sdcard/jsonPost.txt");
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
			myOutWriter.append(data);
			myOutWriter.close();
			fOut.close();
		} 
		catch (Exception e) 
		{
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}


}

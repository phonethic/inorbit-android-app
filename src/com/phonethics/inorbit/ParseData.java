
package com.phonethics.inorbit;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.toolbox.JsonArrayRequest;
import com.phonethics.inorbit.InboxStoreList.AllMallMessages;
import com.phonethics.model.AllMessagesModel;
import com.phonethics.model.CustomerDetails;
import com.phonethics.model.DataCount;
import com.phonethics.model.MallInfoModel;
import com.phonethics.model.ParticularStoreInfo;
import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;
import com.phonethics.model.RestaurantMenuModel;
import com.phonethics.model.RestaurantsForMall;
import com.phonethics.model.SpecialPost;
import com.phonethics.model.SplashModel;
import com.phonethics.model.StoreGallery;
import com.phonethics.model.StoreInfo;
import com.phonethics.model.StoreInfoDetails;
import com.phonethics.model.TimeInfo;
import com.phonethics.model.TipsManagerResponseModel;
import com.phonethics.model.TipsModel;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class ParseData {

	Context	context;
	DBUtil dbUtil;



	public ParseData(Context context){
		this.context = context;

	}


	public void parseGalleryResponse(JSONArray jArr,String Tag){

		final ArrayList<SplashModel> splashModels = new ArrayList<SplashModel>();

		try{

			for(int i=0;i<jArr.length();i++){

				JSONObject tempObject 	= jArr.getJSONObject(i);
				SplashModel splashModel = new SplashModel();

				splashModel.setImage_url(tempObject.getString("image_url"));
				splashModel.setTitle(tempObject.getString("title"));
				splashModel.setId(tempObject.getString("id"));
				splashModel.setPlace_id(tempObject.getString("place_id"));
				splashModel.setImage_data(tempObject.getString("image_date"));

				splashModels.add(splashModel);

			}


			Intent intent  = new Intent();
			//Bundle b=new Bundle();
			/*b.putString("SUCCESS", "true");
			b.putParcelableArrayList("IMAGE_DATE", splashModels);
			intent.putExtra("BundleGall", b);*/
			intent.putExtra("SUCCESS", "true");
			intent.putParcelableArrayListExtra("IMAGE_DATE", splashModels);
			intent.setAction(Tag);
			context.sendBroadcast(intent);



		}catch(Exception ex){
			ex.printStackTrace();
		}



	}

	public void parseGalleryResponse(JSONObject response,String Tag){

		final ArrayList<SplashModel> splashModels = new ArrayList<SplashModel>();

		try{
			//String success = response.getString("success");
			String success = "false";
			if(success.equalsIgnoreCase("true")){
				InorbitLog.d(Tag + " Response "+response.toString());
				JSONArray jArr = response.getJSONArray("data");

				for(int i=0;i<jArr.length();i++){

					JSONObject tempObject 	= jArr.getJSONObject(i);
					SplashModel splashModel = new SplashModel();

					splashModel.setImage_url(tempObject.getString("image_url"));
					splashModel.setTitle(tempObject.getString("title"));
					splashModel.setId(tempObject.getString("id"));
					splashModel.setPlace_id(tempObject.getString("place_id"));
					splashModel.setImage_data(tempObject.getString("image_date"));

					splashModels.add(splashModel);

				}


				Intent intent  = new Intent();
				//intent.putExtra("SUCCESS", "true");
				intent.putExtra("SUCCESS", "false");
				intent.putParcelableArrayListExtra("IMAGE_DATE", splashModels);
				intent.setAction(Tag);
				context.sendBroadcast(intent);

			}else{
				String message = response.getString("message");
				Intent intent  = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}



	}


	public void parseStoreGalleryResponse(JSONObject response,String Tag){

		final ArrayList<StoreGallery> splashModels = new ArrayList<StoreGallery>();

		try{
			String success = response.getString("success");

			if(success.equalsIgnoreCase("true")){
				JSONArray jArr = response.getJSONArray("data");
				for(int i=0;i<jArr.length();i++){

					JSONObject tempObject 	= jArr.getJSONObject(i);
					StoreGallery splashModel = new StoreGallery();

					splashModel.setImage_url(tempObject.getString("image_url"));
					splashModel.setTitle(tempObject.getString("title"));
					splashModel.setId(tempObject.getString("id"));
					splashModel.setPlace_id(tempObject.getString("place_id"));
					splashModel.setImage_data(tempObject.getString("image_date"));
					splashModel.setThumb_url(tempObject.getString("thumb_url"));

					splashModels.add(splashModel);

				}
				Intent intent  = new Intent();
				Bundle b=new Bundle();
				intent.putExtra("STATUS", "true");
				b.putParcelableArrayList("IMAGE_DATE", splashModels);
				intent.putExtra("BundleGall", b);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}else{
				Intent intent  = new Intent();
				Bundle b=new Bundle();
				intent.putExtra("STATUS", "false");
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.putExtra("BundleGall", b);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}





		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent  = new Intent();
			Bundle b=new Bundle();

			intent.putExtra("BundleGall", b);
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}



	}


	/**
	 * Parse Mall information- this api gets called on splash screen.
	 * Result gets store on database
	 * 
	 * @param jArr
	 * @param Tag
	 */
	public void parseMallInfo(JSONArray jArr,String Tag){
		dbUtil = new DBUtil(context);
		final ArrayList<MallInfoModel> mallInfoModels = new ArrayList<MallInfoModel>();


		try{

			for(int i=0;i<jArr.length();i++){

				JSONObject tempObject 	= jArr.getJSONObject(i);
				MallInfoModel mallInfoModel = new MallInfoModel();

				mallInfoModel.setCity(tempObject.getString("city"));
				mallInfoModel.setCountry(tempObject.getString("country"));
				mallInfoModel.setCountry_code(tempObject.getString("country_code"));
				mallInfoModel.setFacebook_url(tempObject.getString("facebook_url"));
				mallInfoModel.setId(tempObject.getString("id"));
				mallInfoModel.setInorbit_place_id(tempObject.getString("inorbit_place_id"));
				mallInfoModel.setIso_code(tempObject.getString("iso_code"));
				mallInfoModel.setLatitude(tempObject.getString("latitude"));
				mallInfoModel.setLocality(tempObject.getString("locality"));
				mallInfoModel.setLongitude(tempObject.getString("longitude"));
				mallInfoModel.setPincode(tempObject.getString("pincode"));
				mallInfoModel.setState(tempObject.getString("state"));
				mallInfoModel.setSublocality(tempObject.getString("sublocality"));

				mallInfoModels.add(mallInfoModel);

			}


			dbUtil.deleteAreasTable();

			dbUtil.open();
			dbUtil.beginTransaction();

			try{
				for(int i=0;i<mallInfoModels.size();i++){
					Log.d("ALLIDS","Inorbit mall " + mallInfoModels.get(i).getSublocality());
					Log.d("ALLIDS","Inorbit mall size " + mallInfoModels.size());
					long chk = dbUtil.createArea(mallInfoModels.get(i).getId(),

							mallInfoModels.get(i).getSublocality(),
							mallInfoModels.get(i).getLatitude(),
							mallInfoModels.get(i).getLongitude(),

							mallInfoModels.get(i).getPincode(),
							mallInfoModels.get(i).getCity(),
							mallInfoModels.get(i).getCountry(),

							mallInfoModels.get(i).getCountry_code(),
							mallInfoModels.get(i).getIso_code(),
							mallInfoModels.get(i).getInorbit_place_id(),

							mallInfoModels.get(i).getLocality(),
							mallInfoModels.get(i).getState(),
							mallInfoModels.get(i).getFacebook_url());
				}
				dbUtil.setTransactionSuccessful();
			}catch(Exception ex){
				ex.printStackTrace();
			}finally{
				dbUtil.endTransaction();
			}

			dbUtil.close();

			Intent intent  = new Intent();
			intent.putExtra("SUCCESS", "true");
			intent.setAction(Tag);
			context.sendBroadcast(intent);

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}



	void parseStoreResponse(JSONObject response,String Tag){
		try{
			String success = response.getString("success");
			Log.v("Store Response", response.toString());
			if(success.equalsIgnoreCase("true")){


				JSONObject tempjObj = response.getJSONObject("data");
				JSONArray jArr = tempjObj.getJSONArray("record");
				ArrayList<StoreInfo> storeInfos = new ArrayList<StoreInfo>();
				for(int i=0;i<jArr.length();i++){
					StoreInfo storeInfo = new StoreInfo();

					JSONObject jObj = jArr.getJSONObject(i);

					storeInfo.setId(jObj.getString("id"));
					storeInfo.setPlace_parent(jObj.getString("place_parent"));
					storeInfo.setName(jObj.getString("name"));

					storeInfo.setDescription(jObj.getString("description"));
					storeInfo.setBuilding(jObj.getString("building"));
					storeInfo.setStreet(jObj.getString("street"));

					storeInfo.setLandmark(jObj.getString("landmark"));
					storeInfo.setMall_id(jObj.getString("mall_id"));
					storeInfo.setArea(jObj.getString("area"));

					storeInfo.setCity(jObj.getString("city"));
					storeInfo.setMob_no1(jObj.getString("mob_no1"));
					storeInfo.setImage_url(jObj.getString("image_url"));

					storeInfo.setEmail(jObj.getString("email"));
					storeInfo.setWebsite(jObj.getString("website"));
					storeInfo.setTotal_like(jObj.getString("total_like"));
					storeInfo.setTotal_rating("");

					storeInfo.setHas_offer(jObj.getString("has_offer"));
					storeInfo.setTitle(jObj.getString("title"));
					storeInfo.setCategory(jObj.getString("category"));
					
					try {
						storeInfo.setMob_no2(jObj.getString("mob_no2"));
						storeInfo.setMob_no3(jObj.getString("mob_no3"));
						storeInfo.setTel_no1(jObj.getString("tel_no1"));
						storeInfo.setTel_no2(jObj.getString("tel_no2"));
						storeInfo.setTel_no3(jObj.getString("tel_no3"));
						storeInfo.setToll_free_no1(jObj.getString("toll_free_no1"));
						storeInfo.setToll_free_no2(jObj.getString("toll_free_no2"));
					} catch(Exception e) {
						e.printStackTrace();
					}
					storeInfos.add(storeInfo);
				}



				/*Intent intent  = new Intent();
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "true");
				b.putString("TOTAL_SEARCH_PAGES", tempjObj.getInt("total_page")+"");
				b.putString("TOTAL_SEARCH_RECORDS", tempjObj.getInt("total_record")+"");
				b.putParcelableArrayList("StoreInfo", storeInfos);
				intent.putExtra("BundleData", b);
				intent.setAction(Tag);
				context.sendBroadcast(intent);*/

				Intent intent  = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("TOTAL_SEARCH_PAGES", tempjObj.getInt("total_page")+"");
				intent.putExtra("TOTAL_SEARCH_RECORDS", tempjObj.getInt("total_record")+"");
				intent.putParcelableArrayListExtra("StoreInfo", storeInfos);
				intent.setAction(Tag);
				context.sendBroadcast(intent);



			}else{

				String SEARCH_MESSAGE=response.getString("message");
				String code=response.getString("code");
				/*Intent intent  = new Intent();
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "false");
				b.putString("SEARCH_MESSAGE", SEARCH_MESSAGE);
				intent.putExtra("BundleData", b);
				intent.setAction(Tag);
				context.sendBroadcast(intent);*/

				Intent intent  = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", SEARCH_MESSAGE);
				intent.putExtra("CODE", code);
				intent.setAction(Tag);
				context.sendBroadcast(intent);

			}
		}catch(Exception ex){
			ex.printStackTrace();
			/*Intent intent  = new Intent();
			Bundle b=new Bundle();
			b.putString("SEARCH_STATUS", "false");
			b.putString("SEARCH_MESSAGE", context.getResources().getString(R.string.exceptionMessage));
			intent.putExtra("BundleData", b);
			intent.setAction(Tag);
			context.sendBroadcast(intent);*/

			Intent intent  = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", context.getResources().getString(R.string.exceptionMessage));
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}


	}


	void parseParticularStoreDetails(JSONObject response,String Tag){

		try{


			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				ArrayList<ParticularStoreInfo> particularStoreInfoArr = new ArrayList<ParticularStoreInfo>();
				ArrayList<TimeInfo> timeInfoArr = new ArrayList<TimeInfo>();
				ArrayList<String> categories = new ArrayList<String>();
				JSONArray jArr = response.getJSONArray("data");
				for(int i=0;i<jArr.length();i++){
					ParticularStoreInfo particularStoreInfo = new ParticularStoreInfo();
					JSONObject jObj = jArr.getJSONObject(i);

					particularStoreInfo.setArea(jObj.getString("area"));
					particularStoreInfo.setBuilding( jObj.getString("building"));
					particularStoreInfo.setCity(jObj.getString("city"));

					particularStoreInfo.setCountry(jObj.getString("country"));
					particularStoreInfo.setDescription(jObj.getString("description"));
					particularStoreInfo.setEmail(jObj.getString("email"));

					particularStoreInfo.setFacebook_url(jObj.getString("facebook_url"));
					particularStoreInfo.setFax_no1(jObj.getString("fax_no1"));
					particularStoreInfo.setFax_no2(jObj.getString("fax_no2"));

					particularStoreInfo.setId(jObj.getString("id"));
					particularStoreInfo.setImage_url(jObj.getString("image_url"));
					particularStoreInfo.setLandmark(jObj.getString("landmark"));

					particularStoreInfo.setLatitude(jObj.getString("latitude"));
					particularStoreInfo.setLongitude(jObj.getString("longitude"));
					particularStoreInfo.setMall_id(jObj.getString("mall_id"));

					particularStoreInfo.setMob_no1(jObj.getString("mob_no1"));
					particularStoreInfo.setMob_no2(jObj.getString("mob_no2"));
					particularStoreInfo.setMob_no3(jObj.getString("mob_no3"));
					particularStoreInfo.setName(jObj.getString("name"));

					particularStoreInfo.setPincode(jObj.getString("pincode"));
					particularStoreInfo.setPlace_parent(jObj.getString("place_parent"));
					particularStoreInfo.setState(jObj.getString("state"));

					particularStoreInfo.setStreet(jObj.getString("street"));
					particularStoreInfo.setTel_no1(jObj.getString("tel_no1"));
					particularStoreInfo.setTel_no2(jObj.getString("tel_no2"));
					particularStoreInfo.setTel_no3(jObj.getString("tel_no3"));

					particularStoreInfo.setToll_free_no1(jObj.getString("toll_free_no1"));
					particularStoreInfo.setToll_free_no2(jObj.getString("toll_free_no2"));
					particularStoreInfo.setTotal_like(jObj.getString("total_like"));

					particularStoreInfo.setTotal_share(jObj.getString("total_share"));
					particularStoreInfo.setTwitter_url(jObj.getString("twitter_url"));
					particularStoreInfo.setWebsite(jObj.getString("website"));

					if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_m)){
						particularStoreInfo.setTotal_view(jObj.getString("total_view"));
						particularStoreInfo.setPlace_status(jObj.getString("place_status"));
						particularStoreInfo.setOpen_time(jObj.getString("open_time"));
						particularStoreInfo.setClose_time(jObj.getString("close_time"));

					}

					if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_c)){


						String user_like = "-1";
						try{
							//user_like = jObj.getString("user_like");
							if(jObj.has("user_like")){
								user_like = jObj.getString("user_like");
							}
						}catch(JSONException ex){
							ex.printStackTrace();
							user_like = "-1";
						}
						if(!user_like.equalsIgnoreCase("-1")){
							particularStoreInfo.setUser_like(user_like);
						}

					}

					particularStoreInfoArr.add(particularStoreInfo);

					//JSONArray tempArray = jObj.getJSONArray("time");

					if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_m)){
						/*for(int p=0;p<tempArray.length();p++){
							TimeInfo timeInfo = new TimeInfo();
							JSONObject tempObject = tempArray.getJSONObject(p);
							timeInfo.setClose_time(tempObject.getString("close_time"));
							timeInfo.setDay(tempObject.getString("day"));
							timeInfo.setIs_closed(tempObject.getString("is_closed"));
							timeInfo.setOpen_time(tempObject.getString("open_time"));

							timeInfoArr.add(timeInfo);
						}*/

						JSONArray tempArr = jObj.getJSONArray("categories");
						for(int x=0; x < tempArr.length();x++){
							categories.add(tempArr.getString(x));
						}


					}

				}




				Intent intent = new Intent();

				intent.putExtra("SUCCESS", "true");
				intent.putStringArrayListExtra("Categories", categories);
				//intent.putParcelableArrayListExtra("TimeInforArr", timeInfoArr);
				intent.putParcelableArrayListExtra("ParticularStoreInfoDetails", particularStoreInfoArr);

				intent.setAction(Tag);
				context.sendBroadcast(intent);






			}else{

				String SEARCH_MESSAGE=response.getString("message");
				String code = response.getString("code");
				Intent intent  = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", SEARCH_MESSAGE);
				intent.putExtra("CODE", code);
				intent.setAction(Tag);

				context.sendBroadcast(intent);

			}

		}catch(Exception ex){
			ex.printStackTrace();
			/*Intent intent  = new Intent();
			Bundle b=new Bundle();
			b.putString("STORE_STATUS", "false");
			b.putString("STORE_MESSAGE", context.getResources().getString(R.string.exceptionMessage));
			intent.putExtra("BundleData", b);
			intent.setAction(Tag);
			context.sendBroadcast(intent);*/
		}
	}



	public void parseSpecialPostResponse(JSONObject response, String Tag){
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				JSONArray jArr = response.getJSONArray("data");
				ArrayList<SpecialPost> specialPostArr = new ArrayList<SpecialPost>();

				//arraylist for dates only (created_at)

				//ArrayList<String> createdAt = new ArrayList<String>();

				for(int i=0; i<jArr.length();i++){
					SpecialPost sp = new SpecialPost();

					JSONObject jObj = jArr.getJSONObject(i);

					//Log.d("SPECIALDATE","SPECIAL " + jObj.getString("offer_date_time"));

					//adding items in created_at list

					//createdAt.add(jObj.getString("created_at"));

					// chk for new values
					//sp.setTitle_new("New Title");


					sp.setCreated_at(jObj.getString("created_at"));
					sp.setDescription(jObj.getString("description"));
					sp.setImage_title1(jObj.getString("image_title1"));
					sp.setImage_title2(jObj.getString("image_title2"));
					sp.setImage_title3(jObj.getString("image_title3"));
					sp.setImage_url1(jObj.getString("image_url1"));
					sp.setImage_url2(jObj.getString("image_url2"));
					sp.setOffer_date_time(jObj.getString("offer_date_time"));
					sp.setPlace_id(jObj.getString("place_id"));
					sp.setPost_type(jObj.getString("post_type"));
					sp.setThumb_url1(jObj.getString("thumb_url1"));
					sp.setThumb_url2(jObj.getString("thumb_url2"));
					sp.setThumb_url3(jObj.getString("thumb_url3"));
					sp.setTitle(jObj.getString("title"));
					sp.setMall_id(jObj.getString("mall_id"));
					sp.setId(jObj.getString("id"));
					/*InorbitLog.d("Get Created Date "+sp.getCreated_at());
					InorbitLog.d("Get Created Date "+sp.getOffer_date_time());*/

					specialPostArr.add(sp);

				}

				for(int i=0;i<specialPostArr.size();i++){
					InorbitLog.d("Get Created Date Loop "+specialPostArr.get(i).getCreated_at());
					InorbitLog.d("Get Offer Date Loop "+specialPostArr.get(i).getOffer_date_time());
				}


				//				for(int i=0;i<createdAt.size();i++){
				//					
				//					Log.d("SETNEWTITLE","SETNEWTITLE PARSE " + specialPostArr.get(i).getCreated_at());
				//				}


				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				//Bundle bundle = new Bundle();
				//bundle.putString("STATUS", "true");
				intent.putParcelableArrayListExtra("SpecialPostDetails", specialPostArr);
				//				bundle.putStringArrayList("createdAtDates", createdAt);
				//				intent.putParcelableArrayListExtra("SpecialPostDetails", specialPostArr);
				//intent.putExtra("BundleData", bundle);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}else{
				String message = response.getString("message");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}

		}catch(Exception ex){
			ex.printStackTrace();

			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong!");
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}

	}



	public void parseLogin(String Tag,JSONObject response){
		try{
			String success = response.getString("success");
			String message = response.getString("message");
			String isAdmin = "";
			if(success.equalsIgnoreCase("true")){
				JSONObject tempObj = response.getJSONObject("data");
				String id = tempObj.getString("id");
				String auth_id = tempObj.getString("auth_code");
				if(Tag.equalsIgnoreCase(RequestTags.TagLoginMerchant)){
					isAdmin = tempObj.getString("is_admin");
				}


				Intent intent = new Intent();
				intent.putExtra("SUCCESS", success);
				intent.putExtra("MESSAGE", message);
				intent.putExtra("ID", id);
				intent.putExtra("AUTH_ID", auth_id);
				if(Tag.equalsIgnoreCase(RequestTags.TagLoginMerchant)){
					intent.putExtra("IS_ADMIN", isAdmin);	
				}

				intent.setAction(Tag);
				context.sendBroadcast(intent);

			}else{

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", success);
				intent.putExtra("MESSAGE", message);
				intent.setAction(Tag);
				context.sendBroadcast(intent);	
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
			intent.setAction(Tag);
			context.sendBroadcast(intent);	
		}
	}


	public void parsePlaceChooserResponse(String Tag,JSONObject response){
		dbUtil = new DBUtil(context);
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				ArrayList<ParticularStoreInfo> particularStoreInfoArr = new ArrayList<ParticularStoreInfo>();
				JSONArray jArr = response.getJSONArray("data");
				for(int i=0;i<jArr.length();i++){
					ParticularStoreInfo particularStoreInfo = new ParticularStoreInfo();
					JSONObject jObj = jArr.getJSONObject(i);

					particularStoreInfo.setArea(jObj.getString("area"));
					particularStoreInfo.setBuilding(jObj.getString("building"));
					particularStoreInfo.setCity(jObj.getString("city"));

					particularStoreInfo.setCountry(jObj.getString("country"));
					particularStoreInfo.setDescription(jObj.getString("description"));
					particularStoreInfo.setEmail(jObj.getString("email"));

					particularStoreInfo.setFacebook_url(jObj.getString("facebook_url"));
					particularStoreInfo.setFax_no1(jObj.getString("fax_no1"));
					particularStoreInfo.setFax_no2(jObj.getString("fax_no2"));

					particularStoreInfo.setId(jObj.getString("id"));
					particularStoreInfo.setImage_url(jObj.getString("image_url"));
					particularStoreInfo.setLandmark(jObj.getString("landmark"));

					particularStoreInfo.setLatitude(jObj.getString("latitude"));
					particularStoreInfo.setLongitude(jObj.getString("longitude"));
					particularStoreInfo.setMall_id(jObj.getString("mall_id"));

					particularStoreInfo.setMob_no1(jObj.getString("mob_no1"));
					particularStoreInfo.setMob_no2(jObj.getString("mob_no2"));
					particularStoreInfo.setName(jObj.getString("name"));
					
					particularStoreInfo.setPincode(jObj.getString("pincode"));
					particularStoreInfo.setPlace_parent(jObj.getString("place_parent"));
					particularStoreInfo.setState(jObj.getString("state"));
					
					particularStoreInfo.setStreet(jObj.getString("street"));
					particularStoreInfo.setTel_no1(jObj.getString("tel_no1"));
					particularStoreInfo.setTel_no2(jObj.getString("tel_no2"));

					particularStoreInfo.setToll_free_no1(jObj.getString("toll_free_no1"));
					particularStoreInfo.setToll_free_no2(jObj.getString("toll_free_no2"));
					if(jObj.has("total_like")){
						particularStoreInfo.setTotal_like(jObj.getString("total_like"));
					}

					if(jObj.has("total_share")){
						particularStoreInfo.setTotal_share(jObj.getString("total_share"));
					}
					particularStoreInfo.setTwitter_url(jObj.getString("twitter_url"));
					particularStoreInfo.setWebsite(jObj.getString("website"));


					particularStoreInfo.setWebsite(jObj.getString("place_status"));
					particularStoreInfo.setWebsite(jObj.getString("published"));


					String user_like = "-1";
					try{
						//user_like = jObj.getString("user_like");
						if(jObj.has("user_like")){
							user_like = jObj.getString("user_like");
						}
					}catch(JSONException ex){
						ex.printStackTrace();
						user_like = "-1";
					}
					if(!user_like.equalsIgnoreCase("-1")){
						particularStoreInfo.setUser_like(user_like);
					}
					
					particularStoreInfo.setSub_category(jObj.getString("category"));

					particularStoreInfoArr.add(particularStoreInfo);


				}


				try
				{
					dbUtil.deletePlacesTable();
					dbUtil.open();
					dbUtil.beginTransaction();
					for(int i=0;i<particularStoreInfoArr.size();i++) {
						//Log.i("", "PLACE CHOOSER "+ID.size()+" ID "+ID.get(i)+" STORE NAME "+SOTRE_NAME.get(i));


						dbUtil.create_place_chooser(particularStoreInfoArr.get(i).getId(), 
								particularStoreInfoArr.get(i).getPlace_parent(), 
								particularStoreInfoArr.get(i).getName(), 
								particularStoreInfoArr.get(i).getDescription(), 
								particularStoreInfoArr.get(i).getBuilding(), 
								particularStoreInfoArr.get(i).getStreet(), 
								particularStoreInfoArr.get(i).getLandmark(), 
								particularStoreInfoArr.get(i).getArea(), 
								particularStoreInfoArr.get(i).getPincode(), 
								particularStoreInfoArr.get(i).getCity(), 
								particularStoreInfoArr.get(i).getState(), 
								particularStoreInfoArr.get(i).getCountry(), 
								particularStoreInfoArr.get(i).getLatitude(), 
								particularStoreInfoArr.get(i).getLongitude(), 
								particularStoreInfoArr.get(i).getTel_no1(), 
								particularStoreInfoArr.get(i).getTel_no2(), 
								particularStoreInfoArr.get(i).getMob_no1(), 
								particularStoreInfoArr.get(i).getMob_no2(), 
								particularStoreInfoArr.get(i).getImage_url(), 
								particularStoreInfoArr.get(i).getEmail(), 
								particularStoreInfoArr.get(i).getWebsite(), 
								particularStoreInfoArr.get(i).getMall_id(), 
								particularStoreInfoArr.get(i).getFacebook_url(), 
								particularStoreInfoArr.get(i).getTwitter_url(), 
								particularStoreInfoArr.get(i).getPlace_status(),
								particularStoreInfoArr.get(i).getSub_category());
					}
					dbUtil.setTransactionSuccessful();
					
					//dbUtil.close();

					SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", context.MODE_PRIVATE);
					Editor editor=prefs.edit();
					editor.putBoolean("isPlaceRefreshRequired", false);
					editor.commit();

				}
				catch(NumberFormatException nb)
				{
					nb.printStackTrace();
					SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", context.MODE_PRIVATE);
					Editor editor=prefs.edit();
					editor.putBoolean("isPlaceRefreshRequired", true);
					editor.commit();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();

					SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", context.MODE_PRIVATE);
					Editor editor=prefs.edit();
					editor.putBoolean("isPlaceRefreshRequired", true);
					editor.commit();
				}finally{
					dbUtil.endTransaction();
					dbUtil.close();
					//MyToast.showToast((Activity) context, "Sending broadcast "+particularStoreInfoArr.size());
					Intent intent = new Intent();
					intent.putExtra("SUCCESS", "true");
					//Bundle bundle = new Bundle();
					//bundle.putParcelableArrayList("ParticularStoreInfoDetails", particularStoreInfoArr);
					intent.putParcelableArrayListExtra("ParticularStoreInfoDetails", particularStoreInfoArr);
					//intent.putExtra("BundleData", bundle);
					intent.setAction(Tag);
					context.sendBroadcast(intent);
				}
			}else{
				String message = response.getString("message");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}

	}


	public void parseNewBroadcastResponse(String Tag,JSONObject respose){
		try{
			
			InorbitLog.d("%%%%%%%%%%%"+respose.toString());
			String success = respose.getString("success");
			String message = respose.getString("message");
			
			if(success.equalsIgnoreCase("true")){
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", message);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}else{
				String code = respose.getString("code");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}
	
	public void parseBroadcastResponse(String Tag,JSONObject respose){
		try{
			String success = respose.getString("success");
			if(success.equalsIgnoreCase("true")){
				ArrayList<PostDetail> postDetailsArr = new ArrayList<PostDetail>();
				JSONObject obj = respose.getJSONObject("data");
				JSONArray jArr = obj.getJSONArray("record");
				for(int i=0;i<jArr.length();i++){
					PostDetail post = new PostDetail();
					JSONObject tempObj = jArr.getJSONObject(i);
					post.setDate(tempObj.getString("date"));
					post.setDescription(tempObj.getString("description"));
					post.setId(tempObj.getString("id"));

					post.setImage_title1(tempObj.getString("image_title1"));
					post.setImage_title2(tempObj.getString("image_title2"));
					post.setImage_title3(tempObj.getString("image_title3"));

					post.setImage_url1(tempObj.getString("image_url1"));
					post.setImage_url2(tempObj.getString("image_url2"));
					post.setImage_url3(tempObj.getString("image_url3"));

					post.setOffer_end_date_time(tempObj.getString("offer_date_time"));
					post.setPlace_id(tempObj.getString("place_id"));
					post.setTags(tempObj.getString("tags"));

					post.setThumb_url1(tempObj.getString("thumb_url1"));
					post.setThumb_url2(tempObj.getString("thumb_url2"));
					post.setThumb_url3(tempObj.getString("thumb_url3"));

					post.setTitle(tempObj.getString("title"));
					post.setTotal_like(tempObj.getString("total_like"));
					post.setTotal_share(tempObj.getString("total_share"));

					post.setTotal_view(tempObj.getString("total_view"));
					post.setType(tempObj.getString("type"));
					post.setUrl(tempObj.getString("url"));

					post.setIs_offered(tempObj.getString("is_offered"));
					post.setStatus(tempObj.getString("status"));
					post.setPriority(tempObj.getString("priority"));
					post.setOffer_start_date_time(tempObj.getString("offer_start_date_time"));


					postDetailsArr.add(post);

				}


				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("TOTAL_PAGE", obj.getInt("total_page")+"");
				intent.putExtra("TOTAL_RECORD", obj.getInt("total_record")+"");
				Bundle bundle = new Bundle();
				bundle.putParcelableArrayList("POST_DETAILS", postDetailsArr);
				intent.putExtra("BundleData", bundle);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", respose.getString("message"));
				intent.setAction(Tag);  
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong!");
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}

	}


	public void parseInorbitGalleryResponse(JSONObject response, String tag) {
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				ArrayList<String> galleryArr = new ArrayList<String>();
				JSONObject obj = response.getJSONObject("data");
				JSONArray jArr = obj.getJSONArray("Gallery");
				for(int i=0;i<jArr.length();i++){
					galleryArr.add(jArr.getString(i));
				}
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putStringArrayListExtra("Gallery", galleryArr);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}

		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}
	}


	public void parseAddStoreResponse(String tag, JSONObject response) {
		try{
			String success = response.getString("success");
			String mssg = response.getString("message");
			String code = "";
			if(success.equalsIgnoreCase("true")){
				JSONObject jObj = response.getJSONObject("data");
				String place_id = jObj.getString("place_id");

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", mssg);
				intent.putExtra("PLACE_ID", place_id);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}else{
				code = response.getString("code");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", mssg);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}

	}


	public void parsecategoreyResponse(String Tag, JSONObject response){
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				JSONArray jArr = response.getJSONArray("data");
				ArrayList<String> cat_id = new ArrayList<String>();
				ArrayList<String> cat_label = new ArrayList<String>();
				for(int i=0;i<jArr.length();i++){
					JSONObject tempObj = jArr.getJSONObject(i);
					cat_id.add(tempObj.getString("id"));
					cat_label.add(tempObj.getString("label"));
				}

				Intent intent = new Intent();
				intent.putExtra("STATUS", "true");
				intent.putStringArrayListExtra("CATEGORY_ID", cat_id);
				intent.putStringArrayListExtra("CATEGORY_LABEL", cat_label);
				intent.setAction(Tag);
				context.sendBroadcast(intent);

			}else{
				Intent intent = new Intent();
				intent.putExtra("STATUS", "false");
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("STATUS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}


	public void parseSpecialDateResponse(String tag, JSONObject response) {
		try{

			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				JSONObject jObj = response.getJSONObject("data");
				JSONArray  jArr =  jObj.getJSONArray("date_categories");
				ArrayList<String> arr_id 		= new ArrayList<String>();
				ArrayList<String> arr_category 	= new ArrayList<String>();
				for(int i=0;i<jArr.length();i++){
					JSONObject tempObj = jArr.getJSONObject(i);
					arr_id.add(tempObj.getString("id"));
					arr_category.add(tempObj.getString("name"));
				}

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putStringArrayListExtra("ID", arr_id);
				intent.putStringArrayListExtra("CATEGORY", arr_category);
				intent.setAction(tag);
				context.sendBroadcast(intent);

			}else{
				String message = response.getString("message");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.setAction(tag);
				context.sendBroadcast(intent);

			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong.");
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}

	}


	public void parseCustomerProfile(String tag, JSONObject response) {
		// TODO Auto-generated method stub
		try{
			String success = response.getString("success");
			String message = response.getString("message");
			ArrayList<String> arr_date_category_id = new ArrayList<String>();
			ArrayList<String> arr_date				= new ArrayList<String>();
			if(success.equalsIgnoreCase("true")){
				JSONObject jObj = response.getJSONObject("data");
				CustomerDetails customerDetails = new CustomerDetails();

				customerDetails.setCity(jObj.getString("city"));
				customerDetails.setEmail(jObj.getString("email"));
				customerDetails.setFacebook_access_token(jObj.getString("facebook_access_token"));
				customerDetails.setFacebook_user_id(jObj.getString("facebook_user_id"));
				customerDetails.setGender(jObj.getString("gender"));
				customerDetails.setId(jObj.getString("id"));
				customerDetails.setImage_url(jObj.getString("image_url"));
				customerDetails.setMobile(jObj.getString("mobile"));
				customerDetails.setName(jObj.getString("name"));
				customerDetails.setState(jObj.getString("state"));


				JSONArray datesArr = jObj.getJSONArray("dates");
				for(int i=0;i<datesArr.length();i++){
					JSONObject tempObj = datesArr.getJSONObject(i);
					arr_date_category_id.add(tempObj.getString("date_category"));
					arr_date.add(tempObj.getString("date"));
				}


				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CUSTOMER_DETAILS", customerDetails);
				intent.putStringArrayListExtra("DATE_CATEGORY_ID", arr_date_category_id);
				intent.putStringArrayListExtra("DATE", arr_date);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}else{
				String code = response.getString("code");

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong.");
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}
	}


	public void parseUpdateCustomerProfileResponse(String tag, JSONObject response) {
		try{
			String success = response.getString("success");
			String mssg = response.getString("message");
			if(success.equalsIgnoreCase("true")){
				JSONObject jObj = response.getJSONObject("data");
				String user_id = jObj.getString("user_id");

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", mssg);
				intent.putExtra("USER_ID", user_id);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", mssg);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}

	}


	public void parseIntresrtedMallsResponse(String Tag, JSONObject response){
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				ArrayList<String> arr_mallId = new ArrayList<String>();
				JSONObject jObj = response.getJSONObject("data");
				JSONArray jArr = jObj.getJSONArray("areas");
				for(int i=0; i<jArr.length();i++){
					JSONObject tmpObj = jArr.getJSONObject(i);
					arr_mallId.add(tmpObj.getString("mall_id"));
				}

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putStringArrayListExtra("MALL_ID_ARR", arr_mallId);
				intent.setAction(Tag);
				context.sendBroadcast(intent);

			}else{
				String message = response.getString("message");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong.");
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}

	public void parseMallsAddResponse(String Tag, JSONObject response){
		try{
			String success = response.getString("success");
			String mssg = response.getString("message");
			if(success.equalsIgnoreCase("true")){

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", mssg);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", mssg);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}

	}


	public void parseAndStoreiInDb(String Tag,JSONObject response){

		try{

			dbUtil= new DBUtil(context);
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				ArrayList<ParticularStoreInfo> particularStoreInfoArr = new ArrayList<ParticularStoreInfo>();
				ArrayList<TimeInfo> timeInfoArr = new ArrayList<TimeInfo>();
				ArrayList<String> categories = new ArrayList<String>();
				JSONArray jArr = response.getJSONArray("data");
				for(int i=0;i<jArr.length();i++){
					ParticularStoreInfo particularStoreInfo = new ParticularStoreInfo();
					JSONObject jObj = jArr.getJSONObject(i);

					particularStoreInfo.setArea(jObj.getString("area"));
					particularStoreInfo.setBuilding(jObj.getString("building"));
					particularStoreInfo.setCity(jObj.getString("city"));

					particularStoreInfo.setCountry(jObj.getString("country"));
					particularStoreInfo.setDescription(jObj.getString("description"));
					particularStoreInfo.setEmail(jObj.getString("email"));

					particularStoreInfo.setFacebook_url(jObj.getString("facebook_url"));
					particularStoreInfo.setFax_no1(jObj.getString("fax_no1"));
					particularStoreInfo.setFax_no2(jObj.getString("fax_no2"));

					particularStoreInfo.setId(jObj.getString("id"));
					particularStoreInfo.setImage_url(jObj.getString("image_url"));
					particularStoreInfo.setLandmark(jObj.getString("landmark"));

					particularStoreInfo.setLatitude(jObj.getString("latitude"));
					particularStoreInfo.setLongitude(jObj.getString("longitude"));
					particularStoreInfo.setMall_id(jObj.getString("mall_id"));

					particularStoreInfo.setMob_no1(jObj.getString("mob_no1"));
					particularStoreInfo.setMob_no2(jObj.getString("mob_no2"));
					particularStoreInfo.setName(jObj.getString("name"));

					particularStoreInfo.setPincode(jObj.getString("pincode"));
					particularStoreInfo.setPlace_parent(jObj.getString("place_parent"));
					particularStoreInfo.setState(jObj.getString("state"));

					particularStoreInfo.setStreet(jObj.getString("street"));
					particularStoreInfo.setTel_no1(jObj.getString("tel_no1"));
					particularStoreInfo.setTel_no2(jObj.getString("tel_no2"));

					particularStoreInfo.setToll_free_no1(jObj.getString("toll_free_no1"));
					particularStoreInfo.setToll_free_no2(jObj.getString("toll_free_no2"));
					particularStoreInfo.setTotal_like(jObj.getString("total_like"));

					particularStoreInfo.setTotal_share(jObj.getString("total_share"));
					particularStoreInfo.setTwitter_url(jObj.getString("twitter_url"));
					particularStoreInfo.setWebsite(jObj.getString("website"));

					if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_m)){
						particularStoreInfo.setTotal_view(jObj.getString("total_view"));
						particularStoreInfo.setPlace_status(jObj.getString("place_status"));

					}

					if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_c)){


						String user_like = "-1";
						try{
							//user_like = jObj.getString("user_like");
							if(jObj.has("user_like")){
								user_like = jObj.getString("user_like");
							}
						}catch(JSONException ex){
							ex.printStackTrace();
							user_like = "-1";
						}
						if(!user_like.equalsIgnoreCase("-1")){
							particularStoreInfo.setUser_like(user_like);
						}

					}

					particularStoreInfoArr.add(particularStoreInfo);

					//JSONArray tempArray = jObj.getJSONArray("time");

					if(Tag.equalsIgnoreCase(RequestTags.TagGetParticularStoreDetails_m)){/*
						for(int p=0;p<tempArray.length();p++){
							TimeInfo timeInfo = new TimeInfo();
							JSONObject tempObject = tempArray.getJSONObject(p);
							timeInfo.setClose_time(tempObject.getString("close_time"));
							timeInfo.setDay(tempObject.getString("day"));
							timeInfo.setIs_closed(tempObject.getString("is_closed"));
							timeInfo.setOpen_time(tempObject.getString("open_time"));

							timeInfoArr.add(timeInfo);
						}

						JSONArray tempArr = jObj.getJSONArray("categories");
						for(int x=0; x < tempArr.length();x++){
							categories.add(tempArr.getString(x));
						}

					 */}

				}

				dbUtil.deleteParentStore();
				dbUtil.open();
				for(int i=0;i<particularStoreInfoArr.size();i++){
					dbUtil.create_parent_store_Table(particularStoreInfoArr.get(i));
				}

				dbUtil.close();



			}else{


			}

		}catch(Exception ex){
			ex.printStackTrace();

		}
	}


	public void parsePostDeleteResponse(String tag, JSONObject response) {
		// TODO Auto-generated method stub
		try{
			String success = response.getString("success");
			String message = "";
			String code = "";

			if(success.equalsIgnoreCase("true")){
				message = response.getString("message");
				code = response.getString("code");

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);

			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}



		}catch(Exception ex){

			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong.");

			intent.setAction(tag);
			context.sendBroadcast(intent);
		}

	}


	public void parseMallEventsResponse(String tag, JSONObject response) {
		// TODO Auto-generated method stub
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				ArrayList<PostDetail> postDetailsArr = new ArrayList<PostDetail>();
				JSONObject jObj = response.getJSONObject("data");
				JSONArray jArr = jObj.getJSONArray("record");
				for(int i=0;i<jArr.length();i++){
					PostDetail post = new PostDetail();
					JSONObject tempObj = jArr.getJSONObject(i);
					post.setDate(tempObj.getString("date"));
					post.setDescription(tempObj.getString("description"));
					post.setId(tempObj.getString("id"));

					post.setImage_title1(tempObj.getString("image_title1"));
					post.setImage_title2(tempObj.getString("image_title2"));
					post.setImage_title3(tempObj.getString("image_title3"));

					post.setImage_url1(tempObj.getString("image_url1"));
					post.setImage_url2(tempObj.getString("image_url2"));
					post.setImage_url3(tempObj.getString("image_url3"));

					post.setOffer_end_date_time(tempObj.getString("offer_date_time"));
					post.setPlace_id(tempObj.getString("place_id"));
					post.setTags(tempObj.getString("tags"));

					post.setThumb_url1(tempObj.getString("thumb_url1"));
					post.setThumb_url2(tempObj.getString("thumb_url2"));
					post.setThumb_url3(tempObj.getString("thumb_url3"));

					post.setTitle(tempObj.getString("title"));
					post.setTotal_like(tempObj.getString("total_like"));
					post.setTotal_share(tempObj.getString("total_share"));

					post.setTotal_view(tempObj.getString("total_view"));
					post.setType(tempObj.getString("type"));
					post.setUrl(tempObj.getString("url"));

					post.setIs_offered(tempObj.getString("is_offered"));

					postDetailsArr.add(post);



				}

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("TOTAL_PAGE", jObj.getInt("total_page")+"");
				intent.putExtra("TOTAL_RECORD", jObj.getInt("total_record")+"");

				intent.putParcelableArrayListExtra("POST_DETAILS", postDetailsArr);

				intent.setAction(tag);
				context.sendBroadcast(intent);


			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}

	}


	public void parseValidateUser(String tag, JSONObject response) {
		try{
			/*{"success":"true","message":"Login successful.","code":"-113"}*/
			String success = response.getString("success");
			String message = response.getString("message");
			String code    = response.getString("code");

			if(success.equalsIgnoreCase("true")){

			}else{

			}
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", success);
			intent.putExtra("MESSAGE", message);
			intent.putExtra("CODE", code);
			intent.setAction(tag);
			context.sendBroadcast(intent);

		}catch(Exception ex){
			ex.printStackTrace();
		}


	}

	public void pasrseUnApprovedResposne(String tag, JSONObject response){
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				ArrayList<PostDetail> postDetailsArr = new ArrayList<PostDetail>();
				JSONObject obj = response.getJSONObject("data");
				JSONArray jArr = obj.getJSONArray("record");
				for(int i=0;i<jArr.length();i++){
					PostDetail post = new PostDetail();
					JSONObject tempObj = jArr.getJSONObject(i);
					post.setDate(tempObj.getString("date"));
					post.setDescription(tempObj.getString("description"));
					post.setId(tempObj.getString("id"));

					post.setImage_title1(tempObj.getString("image_title1"));
					post.setImage_title2(tempObj.getString("image_title2"));
					post.setImage_title3(tempObj.getString("image_title3"));

					post.setImage_url1(tempObj.getString("image_url1"));
					post.setImage_url2(tempObj.getString("image_url2"));
					post.setImage_url3(tempObj.getString("image_url3"));

					post.setOffer_end_date_time(tempObj.getString("offer_date_time"));
					post.setPlace_id(tempObj.getString("place_id"));
					post.setTags(tempObj.getString("tags"));

					post.setThumb_url1(tempObj.getString("thumb_url1"));
					post.setThumb_url2(tempObj.getString("thumb_url2"));
					post.setThumb_url3(tempObj.getString("thumb_url3"));

					post.setTitle(tempObj.getString("title"));
					if(tag.equalsIgnoreCase(RequestTags.Tag_UnapprovedBroadcast)){

					}else{
						post.setTotal_like(tempObj.getString("total_like"));
						post.setTotal_share(tempObj.getString("total_share"));
						post.setTotal_view(tempObj.getString("total_view"));
					}

					post.setType(tempObj.getString("type"));
					post.setUrl(tempObj.getString("url"));

					post.setIs_offered(tempObj.getString("is_offered"));

					postDetailsArr.add(post);

				}


				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				/*intent.putExtra("TOTAL_PAGE", obj.getInt("total_page")+"");
				intent.putExtra("TOTAL_RECORD", obj.getInt("total_record")+"");*/
				Bundle bundle = new Bundle();
				bundle.putParcelableArrayList("POST_DETAILS", postDetailsArr);
				intent.putExtra("BundleData", bundle);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");

				intent.putExtra("MESSAGE", response.getString("message"));
				intent.putExtra("CODE", response.getString("code"));
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}
	}


	public void parseApprovePost(String tag, JSONObject response) {
		// TODO Auto-generated method stub
		try{
			String success = response.getString("success");
			String message = response.getString("message");
			String code = response.getString("code");
			if(success.equalsIgnoreCase("true")){
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", message);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	public void parseBroadcastResponseForUser(String tag, JSONObject response) {
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				ArrayList<PostDetail> postDetailsArr = new ArrayList<PostDetail>();
				JSONObject obj = response.getJSONObject("data");
				JSONArray jArr = obj.getJSONArray("record");
				for(int i=0;i<jArr.length();i++){
					PostDetail post = new PostDetail();
					JSONObject tempObj = jArr.getJSONObject(i);
					post.setDate(tempObj.getString("date"));
					post.setDescription(tempObj.getString("description"));
					post.setId(tempObj.getString("id"));

					post.setImage_title1(tempObj.getString("image_title1"));
					post.setImage_title2(tempObj.getString("image_title2"));
					post.setImage_title3(tempObj.getString("image_title3"));

					post.setImage_url1(tempObj.getString("image_url1"));
					post.setImage_url2(tempObj.getString("image_url2"));
					post.setImage_url3(tempObj.getString("image_url3"));

					post.setOffer_end_date_time(tempObj.getString("offer_date_time"));
					post.setPlace_id(tempObj.getString("place_id"));
					post.setTags(tempObj.getString("tags"));

					post.setThumb_url1(tempObj.getString("thumb_url1"));
					post.setThumb_url2(tempObj.getString("thumb_url2"));
					post.setThumb_url3(tempObj.getString("thumb_url3"));

					post.setTitle(tempObj.getString("title"));
					post.setTotal_like(tempObj.getString("total_like"));
					post.setTotal_share(tempObj.getString("total_share"));

					post.setTotal_view(tempObj.getString("total_view"));
					post.setType(tempObj.getString("type"));
					post.setUrl(tempObj.getString("url"));

					post.setIs_offered(tempObj.getString("is_offered"));
					//post.setStatus(tempObj.getString("status"));

					postDetailsArr.add(post);

				}


				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("TOTAL_PAGE", obj.getInt("total_page")+"");
				intent.putExtra("TOTAL_RECORD", obj.getInt("total_record")+"");
				Bundle bundle = new Bundle();
				bundle.putParcelableArrayList("POST_DETAILS", postDetailsArr);
				intent.putExtra("BundleData", bundle);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	public void parseBroadcastResponseOfMall(String tag, JSONObject response) {
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				ArrayList<PostDetail> postDetailsArr = new ArrayList<PostDetail>();
				JSONObject obj = response.getJSONObject("data");
				JSONArray jArr = obj.getJSONArray("record");
				for(int i=0;i<jArr.length();i++){
					PostDetail post = new PostDetail();
					JSONObject tempObj = jArr.getJSONObject(i);
					post.setDate(tempObj.getString("date"));
					post.setDescription(tempObj.getString("description"));
					post.setId(tempObj.getString("id"));

					/*post.setImage_title1(tempObj.getString("image_title1"));
					post.setImage_title2(tempObj.getString("image_title2"));
					post.setImage_title3(tempObj.getString("image_title3"));*/

					post.setImage_url1(tempObj.getString("image_url1"));
					post.setImage_url2(tempObj.getString("image_url2"));
					post.setImage_url3(tempObj.getString("image_url3"));

					post.setOffer_end_date_time(tempObj.getString("offer_date_time"));
					/*post.setPlace_id(tempObj.getString("place_id"));*/
					/*post.setTags(tempObj.getString("tags"));*/

					post.setThumb_url1(tempObj.getString("thumb_url1"));
					post.setThumb_url2(tempObj.getString("thumb_url2"));
					post.setThumb_url3(tempObj.getString("thumb_url3"));

					post.setTitle(tempObj.getString("title"));
					post.setTotal_like(tempObj.getString("total_like"));
					post.setTotal_share(tempObj.getString("total_share"));

					post.setTotal_view(tempObj.getString("total_view"));
					post.setType(tempObj.getString("type"));
					//post.setUrl(tempObj.getString("url"));

					post.setIs_offered(tempObj.getString("is_offered"));
					post.setName(tempObj.getString("name"));
					//post.setStatus(tempObj.getString("status"));

					postDetailsArr.add(post);

				}


				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("TOTAL_PAGE", obj.getInt("total_page")+"");
				intent.putExtra("TOTAL_RECORD", obj.getInt("total_record")+"");
				Bundle bundle = new Bundle();
				bundle.putParcelableArrayList("POST_DETAILS", postDetailsArr);
				intent.putExtra("BundleData", bundle);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong!");
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}

	}


	public void parseLikePlace(String tag, JSONObject response) {
		try{
			if(response!=null){
				String success = response.getString("success");
				String message = response.getString("message");
				if(success.equalsIgnoreCase("true")){
					Intent intent = new Intent();
					intent.putExtra("SUCCESS", "true");
					intent.putExtra("MESSAGE", message);
					intent.setAction(tag);
					context.sendBroadcast(intent);

				}else{
					String code = response.getString("code");

					Intent intent = new Intent();
					intent.putExtra("SUCCESS", "false");
					intent.putExtra("MESSAGE", message);
					intent.putExtra("CODE", code);
					intent.setAction(tag);
					context.sendBroadcast(intent);
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	public void parseRegisterUser(String tag, JSONObject response) {
		// TODO Auto-generated method stub
		try{
			String SUCCESS = response.getString("success");
			String MESSAGE = response.getString("message");
			String CODE = response.getString("code");

			Intent intent = new Intent();
			intent.putExtra("SUCCESS", SUCCESS);
			intent.putExtra("MESSAGE", MESSAGE);
			intent.putExtra("CODE", CODE);
			intent.putExtra("TAG", tag);
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	public void parseDataVersionResponse(String tag, JSONObject response) {
		try{
			String SUCCESS = response.getString("success");
			if(SUCCESS.equalsIgnoreCase("true")) {
				/*JSONObject  jObj = response.getJSONObject("data");
				JSONArray jArr = jObj.getJSONArray("data_count");
				ArrayList<DataCount> dataArr = new ArrayList<DataCount>();
				for(int i=0; i<jArr.length(); i++) {
					JSONObject tempObj = jArr.getJSONObject(i);
					DataCount dataCount = new DataCount();
					dataCount.setMallID(tempObj.getString("mall_id"));
					dataCount.setPostCount(tempObj.getString("post_count"));
					dataCount.setStoreCount(tempObj.getString("store_count"));
					dataArr.add(dataCount);
				}

				StoreVersionCount storeCount = new StoreVersionCount(context, response);*/

				JSONObject  jObj = response.getJSONObject("data");
				JSONArray jArr = jObj.getJSONArray("data_count");
				dbUtil = new DBUtil(context);
				int miCount = dbUtil.getCountOfRefreshRable();
				InorbitLog.d("Refresh Count "+miCount);
				if(miCount==0) {
					InorbitLog.d("Refresh Initialising with defualt Value ");
					for(int i=0; i<jArr.length(); i++) {
						InorbitLog.d("Refresh InsertingValues ");
						JSONObject tempObj = jArr.getJSONObject(i);
						dbUtil.initializeValueForRefresh(tempObj.getString("mall_id"));
					}
				} else {
					InorbitLog.d("Refresh Updating new values ");
					for(int i=0; i<jArr.length(); i++) {
						JSONObject tempObj = jArr.getJSONObject(i);
						dbUtil.updateNewValuesForRefresh(tempObj.getString("mall_id"), 
								tempObj.getString("store_count"),
								tempObj.getString("post_count"));
					}
				}

			}


			Intent intent = new Intent();
			intent.putExtra("SUCCESS", SUCCESS);
			intent.putExtra("TAG", tag);
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	public void parseFavCountResponse(String tag, JSONObject response) {
		try {
			String SUCCESS = response.getString("success");
			String msMallId,msFavourites,msActive;
			if(SUCCESS.equalsIgnoreCase("true")) {
				JSONObject  jObj = response.getJSONObject("data");
				msMallId = jObj.getString("id");
				//msMallId = jObj.getString("mall_id");
				msFavourites = jObj.getString("favourites");
				msActive = jObj.getString("active");

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", SUCCESS);
				intent.putExtra("ID", msMallId);
				intent.putExtra("FAVOURITES", msFavourites);
				intent.putExtra("ACTIVE", msActive);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			} else {
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "fasle");
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}


		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}


	public void parseMonthlySpecialCountResponse(String tag, JSONObject response) {
		try {
			String SUCCESS = response.getString("success");
			if(SUCCESS.equalsIgnoreCase("true")) {
				JSONObject  jObj = response.getJSONObject("data");
				String msCountUser = jObj.getString("count_user");
				String msCurrentMonth = jObj.getString("current_month");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", SUCCESS);
				intent.putExtra("USER_COUNT", msCountUser);
				intent.putExtra("CURRENT_MONTH", msCurrentMonth);
				intent.setAction(tag);
				context.sendBroadcast(intent);  

			} else {
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "fasle");
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}


	void parseUserFavouriteStores(JSONObject response,String Tag){
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				JSONObject tempjObj = response.getJSONObject("data");
				JSONArray jArr = tempjObj.getJSONArray("record");
				ArrayList<StoreInfo> storeInfos = new ArrayList<StoreInfo>();
				for(int i=0;i<jArr.length();i++){
					StoreInfo storeInfo = new StoreInfo();
					JSONObject jObj = jArr.getJSONObject(i);

					storeInfo.setId(jObj.getString("id"));
					storeInfo.setPlace_parent(jObj.getString("place_parent"));
					storeInfo.setName(jObj.getString("name"));

					storeInfo.setDescription(jObj.getString("description"));
					storeInfo.setBuilding(jObj.getString("building"));
					storeInfo.setStreet(jObj.getString("street"));

					storeInfo.setLandmark(jObj.getString("landmark"));
					storeInfo.setMall_id(jObj.getString("mall_id"));
					storeInfo.setArea(jObj.getString("area"));

					storeInfo.setCity(jObj.getString("city"));
					storeInfo.setMob_no1(jObj.getString("mob_no1"));
					storeInfo.setImage_url(jObj.getString("image_url"));

					storeInfo.setEmail(jObj.getString("email"));
					storeInfo.setWebsite(jObj.getString("website"));
					storeInfo.setTotal_like(jObj.getString("total_like"));

					storeInfo.setHas_offer(jObj.getString("has_offer"));
					storeInfo.setTitle(jObj.getString("title"));



					storeInfos.add(storeInfo);

				}



				/*Intent intent  = new Intent();
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "true");
				b.putString("TOTAL_SEARCH_PAGES", tempjObj.getInt("total_page")+"");
				b.putString("TOTAL_SEARCH_RECORDS", tempjObj.getInt("total_record")+"");
				b.putParcelableArrayList("StoreInfo", storeInfos);
				intent.putExtra("BundleData", b);
				intent.setAction(Tag);
				context.sendBroadcast(intent);*/

				Intent intent  = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("TOTAL_SEARCH_PAGES", tempjObj.getInt("total_page")+"");
				intent.putExtra("TOTAL_SEARCH_RECORDS", tempjObj.getInt("total_record")+"");
				intent.putParcelableArrayListExtra("StoreInfo", storeInfos);
				intent.setAction(Tag);
				context.sendBroadcast(intent);



			}else{

				String SEARCH_MESSAGE=response.getString("message");
				String code=response.getString("code");
				/*Intent intent  = new Intent();
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "false");
				b.putString("SEARCH_MESSAGE", SEARCH_MESSAGE);
				intent.putExtra("BundleData", b);
				intent.setAction(Tag);
				context.sendBroadcast(intent);*/

				Intent intent  = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", SEARCH_MESSAGE);
				intent.putExtra("CODE", code);
				intent.setAction(Tag);
				context.sendBroadcast(intent);

			}
		}catch(JSONException jEx){

			Intent intent  = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", context.getResources().getString(R.string.exceptionMessage));
			intent.setAction(Tag);
			context.sendBroadcast(intent);

		}catch(Exception ex){

			ex.printStackTrace();
			Intent intent  = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", context.getResources().getString(R.string.exceptionMessage));
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}


	}


	public void parseOfferDetailResponse(JSONObject response, String tag) {
		try{
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				JSONObject tempjObj = response.getJSONObject("data");

				PostDetail postDetail = new PostDetail();
				postDetail.setDate(tempjObj.getString("date"));
				postDetail.setDescription(tempjObj.getString("description"));
				postDetail.setId(tempjObj.getString("id"));
				postDetail.setImage_title1(tempjObj.getString("image_title1"));
				postDetail.setImage_title2(tempjObj.getString("image_title2"));
				postDetail.setImage_title3(tempjObj.getString("image_title3"));
				postDetail.setImage_url1(tempjObj.getString("image_url1"));
				postDetail.setImage_url2(tempjObj.getString("image_url2"));
				postDetail.setImage_url3(tempjObj.getString("image_url3"));
				postDetail.setIs_offered(tempjObj.getString("is_offered"));
				//postDetail.setName(tempjObj.getString(""));
				postDetail.setOffer_end_date_time(tempjObj.getString("offer_date_time"));
				postDetail.setPlace_id(tempjObj.getString("place_id"));
				//postDetail.setStatus(tempjObj.getString(""));
				postDetail.setTags(tempjObj.getString("tags"));
				postDetail.setThumb_url1(tempjObj.getString("thumb_url1"));
				postDetail.setThumb_url2(tempjObj.getString("thumb_url2"));
				postDetail.setThumb_url3(tempjObj.getString("thumb_url3"));
				postDetail.setTitle(tempjObj.getString("title"));
				postDetail.setTotal_like(tempjObj.getString("total_like"));
				postDetail.setTotal_share(tempjObj.getString("total_share"));
				postDetail.setTotal_view(tempjObj.getString("total_view"));
				postDetail.setType(tempjObj.getString("type"));
				postDetail.setUrl(tempjObj.getString("url"));
				postDetail.setOffer_start_date_time(tempjObj.getString("offer_start_date_time"));
				postDetail.setPriority(tempjObj.getString("priority"));
				SessionManager mSession = new SessionManager(context);
				if(mSession.isLoggedInCustomer()){
					postDetail.setUser_like(tempjObj.getString("user_like"));
				}
				
				Intent intent  = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("Post_Detail", postDetail);
				intent.setAction(tag);
				context.sendBroadcast(intent);

			} else {
				Intent intent  = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();

			Intent intent  = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", context.getResources().getString(R.string.exceptionMessage));
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}

	}


	public void parseOfferLikeResponse(String tag, JSONObject response) {
		try{
			String success = response.getString("success");

			String message = response.getString("message");

			if(success.equalsIgnoreCase("true")){

				Intent intent  = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", message);
				intent.setAction(tag);
				context.sendBroadcast(intent);

			}else{
				String code = response.getString("code");

				Intent intent  = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}



		}catch(Exception ex){
			ex.printStackTrace();

			Intent intent  = new Intent();
			intent.putExtra("SUCCESS", "true");
			intent.putExtra("MESSAGE", "Something went wrong, Please try again after some time");
			intent.setAction(tag);
			context.sendBroadcast(intent);
		}

	}


	public void parseFeedbackResponse(String Tag,JSONObject respose){
		try{
			String success = respose.getString("success");
			String message = respose.getString("message");
			if(success.equalsIgnoreCase("true")){
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", message);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}else{
				String code = respose.getString("code");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}

	public void parseCreateMsg(String Tag,JSONObject respose){

		try {
			String success = respose.getString("success");
			String message = respose.getString("message");
			if(success.equalsIgnoreCase("true")){
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", message);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}else{
				String code = respose.getString("code");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}

	void parseAllMessagesForMall(JSONObject response, String tag){

		try {

			ArrayList<AllMessagesModel> mArr = new ArrayList<AllMessagesModel>();

			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){

				JSONArray data = response.getJSONArray("data");
				for(int i=0;i<data.length();i++){

					AllMessagesModel mModel = new AllMessagesModel();
					JSONObject object = data.getJSONObject(i);

					mModel.setCount(object.getString("count"));
					mModel.setIs_read(object.getString("is_read"));
					mModel.setFrom(object.getString("from"));
					mModel.setTo(object.getString("to"));
					mModel.setFrom_store_name(object.getString("from_store_name"));
					mModel.setFrom_image_url(object.getString("from_image_url"));
					mModel.setTo_store_name(object.getString("to_store_name"));
					mModel.setTo_image_url(object.getString("to_image_url"));
					mArr.add(mModel);
				}

				Intent intent = new Intent();
				intent.putParcelableArrayListExtra("ALL_MESSAGES", mArr);
				intent.putExtra("SUCCESS", "true");
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
			else{
				String message = response.getString("message");
				String code = response.getString("code");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);		
		}
	}

	void getAllConversationMessages(JSONObject response, String tag){

		try {

			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){

				ArrayList<String> to = new ArrayList<String>();
				ArrayList<String> message = new ArrayList<String>();
				ArrayList<String> id = new ArrayList<String>();
				ArrayList<String> time = new ArrayList<String>();
				ArrayList<String> from = new ArrayList<String>();

				JSONArray data = response.getJSONArray("data");
				InorbitLog.d(data.toString());
				for(int i=0;i<data.length();i++){

					JSONObject values = data.getJSONObject(i);
					to.add(values.getString("to"));
					message.add(values.getString("message"));
					id.add(values.getString("id"));
					time.add(values.getString("time"));
					from.add(values.getString("from"));
				}

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putStringArrayListExtra("CHAT_TO", to); 
				intent.putStringArrayListExtra("CHAT_MSG", message);
				intent.putStringArrayListExtra("CHAT_ID", id);
				intent.putStringArrayListExtra("CHAT_TIME",time);
				intent.putStringArrayListExtra("CHAT_FROM", from);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
			else{

				String message = response.getString("message");
				String code = response.getString("code");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);	
		}
	}

	void parseUpdateStatusResponse(String tag,JSONObject response){

		try {
			String success = response.getString("success");
			String message = response.getString("message");
			Intent intent = new Intent();
			intent.putExtra("MESSAGE", message);
			if(success.equalsIgnoreCase("true")){
				intent.putExtra("SUCCESS", "true");
			}
			else{
				intent.putExtra("SUCCESS", "false");
			}
			intent.setAction(tag);
			context.sendBroadcast(intent);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);	
		}
	}

	void parseTotalCount(String tag, JSONObject response){

		try {
			String success = response.getString("success");
			if (success.equalsIgnoreCase("true")) {
				ArrayList<String> count = new ArrayList<String>();
				ArrayList<String> from = new ArrayList<String>();
				JSONObject data = response.getJSONObject("data");
				JSONArray messages = data.getJSONArray("message"); 
				for(int i=0;i<messages.length();i++){
					JSONObject values = messages.getJSONObject(i);
					count.add(values.getString("count"));
					from.add(values.getString("from"));
				}
				String mTotalCount = data.getString("total_store_count");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putStringArrayListExtra("COUNT", count); 
				intent.putStringArrayListExtra("FROM", from);
				intent.putExtra("TOTAL_COUNT", mTotalCount);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			} else {
				String message = response.getString("message");
				String code = response.getString("code");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);	
		}
	}

	void parseRatingResponse(String tag, JSONObject response){

		try {

			String success = response.getString("success");
			String message = response.getString("message");
			if(success.equalsIgnoreCase("true")){


				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", message);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
			else{

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);	
		}
	}

	void parseUdmId(String tag, JSONObject response){

		try {

			String success = response.getString("success");

			if(success.equalsIgnoreCase("true")){
				String UDM_ID="";
				JSONArray data = response.getJSONArray("data");
				for(int i=0;i<data.length();i++){
					JSONObject values = data.getJSONObject(i);
					UDM_ID = values.getString("udm_id");
				}

				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("UDMID",UDM_ID);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
			else{
				String message = response.getString("message");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);			}
	}

	void parseAvgRating(String tag, JSONObject response){

		try {
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")) {

				ArrayList<String> mAvgRating = new ArrayList<String>();
				ArrayList<String> mPlaceId = new ArrayList<String>();
				ArrayList<String> mTotalRate = new ArrayList<String>();

				JSONArray data = response.getJSONArray("data");
				for(int i=0;i<data.length();i++){
					JSONObject values = data.getJSONObject(i);
					mAvgRating.add(values.getString("avg_rate"));
					mPlaceId.add(values.getString("place_id"));
					mTotalRate.add(values.getString("total_rate"));
				}
				
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putStringArrayListExtra("AVG_RATING", mAvgRating); 
				intent.putStringArrayListExtra("PLACE_ID", mPlaceId);
				intent.putStringArrayListExtra("TOTAL_RATE", mTotalRate);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
			else{
				String message = response.getString("message");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);			
		}
	}
	
	void parseUploadMenuResponse (String tag, JSONObject response) {
		try {
			String success = response.getString("success");
			InorbitLog.d(response.toString());
			if(success.equalsIgnoreCase("true")) {
				Intent intent = new Intent();
				intent.putExtra("success", "true");
				intent.putExtra("message", response.getString("message"));
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
			else{
				Intent intent = new Intent();
				intent.putExtra("success", "true");
				intent.putExtra("message", response.getString("message"));
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);			
		}
	}
	
	void getUserSpecificRating(JSONObject response, String tag){

		try {

			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){

				JSONObject data = response.getJSONObject("data");
				String rate =  data.getString("rate");
				
				InorbitLog.d("RATE PARSE" + rate);
				
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("RATE", rate);
				intent.setAction(tag);
				context.sendBroadcast(intent);
				
			}
			else{

				String message = response.getString("message");
				String code = response.getString("code");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);	
		}
	}
	
	void getMenuForStore(JSONObject response, String tag) {
		
		try {
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")) {
				//JSONObject data = response.getJSONObject("data");
				JSONArray dataArr = response.getJSONArray("data");
				ArrayList<RestaurantMenuModel> menus = new ArrayList<RestaurantMenuModel>();
				for (int i=0; i<dataArr.length(); i++) {
					JSONObject obj = dataArr.getJSONObject(i);
					RestaurantMenuModel model = new RestaurantMenuModel();
					model.setId(obj.getString("id"));
					model.setPlace_id(obj.getString("place_id"));
					model.setMall_id(obj.getString("mall_id"));
					model.setImage_url(obj.getString("image_url"));
					model.setThumb_url(obj.getString("thumb_url"));
					model.setDate(obj.getString("date"));
					model.setImage_order(obj.getString("image_order"));
					model.setStatus(obj.getString("status"));
					
					menus.add(model);
				}
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putParcelableArrayListExtra("MENUS", menus);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			} else {
				String message = response.getString("message");
				String code = response.getString("code");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);	
		}
	}
	
	void getMenuForMall(JSONObject response, String tag) {
		try {
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")) {
				//JSONObject data = response.getJSONObject("data");
				JSONArray dataArr = response.getJSONArray("data");
				ArrayList<RestaurantsForMall> allStores = new ArrayList<RestaurantsForMall>();
				for (int i=0; i<dataArr.length(); i++) {
					JSONObject obj = dataArr.getJSONObject(i);
					RestaurantsForMall model = new RestaurantsForMall();
					model.setStoreName(obj.getString("store_name"));
					model.setStoreLogo(obj.getString("store_image"));
					model.setPlaceId(obj.getString("place_id"));
					model.setMallId(obj.getString("mall_id"));
					model.setFirstMenuImage(obj.getString("image_url"));
					model.setFirstMenuThumbnail(obj.getString("thumb_url"));
					model.setMenuUploadDate(obj.getString("date"));
					model.setStatus(obj.getString("status"));
					
					allStores.add(model);
				}
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putParcelableArrayListExtra("MENUS", allStores);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			} else {
				String message = response.getString("message");
				String code = response.getString("code");
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.putExtra("CODE", code);
				intent.setAction(tag);
				context.sendBroadcast(intent);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(tag);
			context.sendBroadcast(intent);	
		}
	}
	
	void deleteMenuImages(String Tag, JSONObject response){
		
		try {
			
			String success = response.getString("success");
			String message = response.getString("message");
			if(success.equalsIgnoreCase("true")){
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("MESSAGE", message);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
			else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", message);
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(Tag);
			context.sendBroadcast(intent);	
		}
	}
	
	void getMenuUpdateStatusResult(JSONObject response, String Tag) {
		try {
			if (response != null) {
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", response.getString("success"));
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			} else {
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", "Something Went Wrong");
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		} catch (Exception e) {
			
		}
	}
	
	void updateMenuOrder(JSONObject response, String Tag) {
		try {
			if (response != null) {
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", response.getString("success"));
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			} else {
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", "Something Went Wrong");
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		} catch (Exception e) {
			
		}
	}
	
	void getUnapprovedMenuCount(JSONObject response, String Tag) {
		try {
			
			String success = response.getString("success");
			if(success.equalsIgnoreCase("true")){
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("TOTAL_COUNT", response.getJSONObject("data").getInt("total_count"));
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
			else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(Tag);
			context.sendBroadcast(intent);	
		}
	}
	
	void postStoreMgrTips(String Tag, JSONObject response) {
		Intent intent = new Intent();
		try {
			String success = response.getString("success");
			intent.putExtra("SUCCESS", success);
			if (success.equalsIgnoreCase("true")) {
				ArrayList<TipsManagerResponseModel> tipsModel = new ArrayList<TipsManagerResponseModel>();
				/*JSONArray arr = response.getJSONArray("data");
				for (int i=0; i<arr.length(); i++) {
					JSONObject obj = (JSONObject) arr.get(i);
					TipsManagerResponseModel tip = new TipsManagerResponseModel();
					tip.setCount(obj.getInt("count"));
					tip.setStoreId(obj.getString("store_id"));
					tipsModel.add(tip);
				}*/
				JSONObject json = response.getJSONObject("data");
				intent.putExtra("TOTAL_COUNT", json.getString("total_count"));
				JSONArray arr = json.getJSONArray("store_list");
				for (int i=0; i<arr.length(); i++) {
					JSONObject obj = (JSONObject) arr.get(i);
					TipsManagerResponseModel tip = new TipsManagerResponseModel();
					tip.setCount(obj.getInt("count"));
					tip.setStoreId(obj.getString("store_id"));
					tipsModel.add(tip);
				}
				
				intent.putParcelableArrayListExtra("data", tipsModel);
			} else {
				intent.putExtra("MESSAGE", response.getString("message"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
		} finally {
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}
	
	void getMallMgrTip(String Tag, JSONObject response) {
		Intent intent = new Intent();
		try {
			String success = response.getString("success");
			intent.putExtra("SUCCESS", success);
			if (success.equalsIgnoreCase("true")) {
				ArrayList<TipsManagerResponseModel> tipsModel = new ArrayList<TipsManagerResponseModel>();
				JSONObject jsonObj = response.getJSONObject("data");
				intent.putExtra("TOTAL_COUNT", jsonObj.getString("total_count"));
				JSONArray arr = jsonObj.getJSONArray("store_list");
				for (int i=0; i<arr.length(); i++) {
					JSONObject obj = (JSONObject) arr.get(i);
					TipsManagerResponseModel tip = new TipsManagerResponseModel();
					tip.setCount(obj.getInt("count"));
					tip.setStoreId(obj.getString("place_id"));
					tip.setStoreName(obj.getString("store_name"));
					tip.setStoreLogo(obj.getString("image_url"));
					
					tipsModel.add(tip);
				}
				intent.putParcelableArrayListExtra("data", tipsModel);
			} else {
				intent.putExtra("MESSAGE", response.getString("message"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
		} finally {
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}
	
	void getAllTips(String Tag, JSONObject response) {
		Intent intent = new Intent();
		try {
			String success = response.getString("success");
			intent.putExtra("SUCCESS", success);
			ArrayList<TipsModel> tips = new ArrayList<TipsModel>();
			if (success.equalsIgnoreCase("true")) {
				JSONArray arr = response.getJSONArray("data");
				InorbitLog.d(arr.toString());
				for (int i=0; i<arr.length(); i++) {
					JSONObject obj = arr.getJSONObject(i);
					if (obj.getString("status").equalsIgnoreCase("2")) {
						continue;
					}
					TipsModel tip = new TipsModel();
					tip.setTipId(obj.getString("tip_id"));
					tip.setTip(obj.getString("tip"));
					tip.setUdmId(obj.getString("UDM_id"));
					tip.setTime(obj.getString("time"));
					tip.setStatus(obj.getString("status"));
					tip.setName(obj.getString("name"));
					tip.setStoreName(obj.getString("store_name"));
					tip.setMallName(obj.getString("mall_name"));
					//tip.setName(obj.getString("email"));
					
					tips.add(tip);
				}
				intent.putParcelableArrayListExtra("data", tips);
			} else {
				intent.putExtra("MESSAGE", response.getString("message"));
			}
		} catch(Exception e){
			e.printStackTrace();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
		} finally {
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}
	
	void parseTipStatusChangeResponse(String Tag, JSONObject response) {
		Intent intent = new Intent();
		try {
			intent.putExtra("SUCCESS", response.getBoolean("success"));
			intent.putExtra("MESSAGE", response.getString("message"));
		} catch(Exception e) {
			e.printStackTrace();
			intent.putExtra("SUCCESS", false);
			intent.putExtra("MESSAGE", "Something went wrong");
		} finally {
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}
	
	void parseAddTip(String Tag, JSONObject response) {
		Intent intent = new Intent();
		try {
			intent.putExtra("SUCCESS", response.getBoolean("success"));
			intent.putExtra("MESSAGE", response.getString("message"));
		} catch (Exception e) {
			e.printStackTrace();
			intent.putExtra("SUCCESS", false);
			intent.putExtra("Message", "Something went wrong");
		} finally {
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}
	
	void uploadGalleryStore(String Tag, JSONObject response) {
		try {
			String success = response.getString("success");
			if (success.equalsIgnoreCase("true")) {
				Intent intent = new Intent();
				intent.putExtra("broadcast_image_status", true);
				intent.putExtra("broadcast_message", response.getString("message"));
				intent.putExtra("data", response.getJSONObject("data").getString("place_id"));
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}else{
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Intent intent = new Intent();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something Went Wrong");
			intent.setAction(Tag);
			context.sendBroadcast(intent);	
		}
	}
	
	void gcmNotificationReceiver(String Tag, JSONObject response) {
		try {
			String success = response.getString("message");
			/*if (success.equalsIgnoreCase("true")) {
				Intent intent = new Intent();
				intent.putExtra("MESSAGE", response.getString("message"));
				intent.putExtra("SUCCESS", "true");
				intent.setAction(Tag);
				context.send
			} else {
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "false");
			}*/
			//showToast(success);
		} catch (Exception e) {
			e.printStackTrace();
			//showToast("Exception");
		}
	}
	
	void apiVersionCheck(String Tag, JSONObject response) {
		try {
			String success = response.getString("success");
			if (success.equalsIgnoreCase("true")) {
				JSONObject data = response.getJSONArray("data").getJSONObject(0);
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("VERSION", data.getInt("version"));
				intent.putExtra("FLAG", data.getString("visibility_flag"));
				intent.putExtra("MESSAGE", data.getString("message"));
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		} catch (Exception e) {
			
		}
	}
	
	void backUpPush(String Tag, JSONObject response) {
		try {
			String success = response.getString("success");
			if (success.equalsIgnoreCase("true")) {
				JSONObject data = response.getJSONArray("data").getJSONObject(0);
				Intent intent = new Intent();
				intent.putExtra("SUCCESS", "true");
				intent.putExtra("GCM_ID", data.getString("id"));
				intent.putExtra("FLAG", data.getString("push_flag"));
				intent.putExtra("MESSAGE", data.getString("message"));
				intent.setAction(Tag);
				context.sendBroadcast(intent);
			}
		} catch (Exception e) {}
	}
	
	void activityTrackingResponse(String Tag, JSONObject response) {
		Intent intent = new Intent();
		try {
			String success = response.getString("success");
			if (success.equalsIgnoreCase("true")) {
				JSONArray data = response.getJSONArray("output");
				Uri uri = new Uri.Builder().scheme("content").authority(context.getResources().getString(R.string.authority)).appendPath("/update").build();
				ContentResolver resolver = context.getContentResolver();
				String[] arr = new String[data.length()];
				for (int i = 0; i < data.length(); i++) {
					arr[i] = data.getString(i);
				}
				resolver.update(uri, new ContentValues(), "", arr);
				intent.putExtra("data", arr);
				intent.putExtra("SUCCESS", "true");
			} else {
				intent.putExtra("SUCCESS", "false");
			} 
			intent.putExtra("MESSAGE", response.getString("message"));
		} catch (Exception e) {
			e.printStackTrace();
			intent.putExtra("SUCCESS", "false");
			intent.putExtra("MESSAGE", "Something went wrong");
		} finally {
			intent.setAction(Tag);
			context.sendBroadcast(intent);
		}
	}
	
	void showToast(String msg) {
		Toast.makeText(context, msg, 0).show();
	}
}

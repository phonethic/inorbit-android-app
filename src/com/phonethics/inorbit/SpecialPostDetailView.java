package com.phonethics.inorbit;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.FailReason;

//import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

public class SpecialPostDetailView extends SherlockActivity {

	public static ImageLoader imageLoader;
	public static DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;
	
	TextView txtBroadCastBody;
	TextView txtBroadCastTitle;

	ImageView imgStoreImage;

	String THUMB_IMAGE_URL;
	String Image_Url;


	Activity context;

	String PHOTO_PARENT_URL;
	ActionBar actionBar;

	String offerValidString;
	TextView txtOfferValidTill;

	TableRow imageRow;

	String STORE_ID = "";
	String MALL_ID = "";

	String STORE_NAME = "";
	String MALL_NAME = "";

	TextView PostedBy,text_code;

	DBUtil dbUtil;

	private String CODE="";
	ProgressBar pBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTheme(R.style.Theme_City_custom);
		setContentView(R.layout.activity_special_post_detail_view);

		context=this;

		dbUtil = new DBUtil(context);

		actionBar=getSupportActionBar();
		actionBar.setTitle("Shake & Win");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		imageRow = (TableRow)findViewById(R.id.imageRow);

		//Photo url
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);


		txtBroadCastTitle=(TextView)findViewById(R.id.txtBroadCastTitle);
		txtBroadCastBody=(TextView)findViewById(R.id.txtBroadCastBody);
		imgStoreImage=(ImageView)findViewById(R.id.imgStoreImage);
		txtOfferValidTill = (TextView) findViewById(R.id.txtOfferValidTill);
		PostedBy = (TextView) findViewById(R.id.PostedBy);
		text_code = (TextView) findViewById(R.id.text_code);


		txtBroadCastTitle.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
		txtBroadCastBody.setTypeface(InorbitApp.getTypeFace());
		txtOfferValidTill.setTypeface(InorbitApp.getTypeFace());
		PostedBy.setTypeface(InorbitApp.getTypeFace());
		pBar = (ProgressBar) findViewById(R.id.pBar);

		try {
			Bundle b=getIntent().getExtras();
			if(b!=null){
				STORE_ID = b.getString("PLACE_ID");
				MALL_ID = b.getString("MALL_ID");
				CODE = b.getString("CODE");
				text_code.setText("Code : "+CODE); 
				String place_parent = dbUtil.getMallPlaceParentByMallID(MALL_ID);
				STORE_NAME = dbUtil.getShopNameByPlaceID(STORE_ID);
				MALL_NAME = dbUtil.getMallNameById(MALL_ID);
				if(STORE_NAME.trim().equalsIgnoreCase("")){
					PostedBy.append("Inorbit " + MALL_NAME);
					Log.d("NAME","NAME IF " + "Inorbit " + MALL_NAME + " " + MALL_ID);
				}else{
					PostedBy.append(" " + STORE_NAME + " - Inorbit " + MALL_NAME);
					Log.d("NAME","NAME ELSE" + "Inorbit " + MALL_NAME);
				}

				txtBroadCastTitle.setText(b.getString("title"));
				txtBroadCastBody.setText(Html.fromHtml(checkUrlInString(b.getString("description"))));
				txtBroadCastBody.setMovementMethod(LinkMovementMethod.getInstance());
				offerValidString = b.getString("offerValidTill");

				try {

					DateFormat   dt=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
					Date date_time = dt.parse(offerValidString);
					SimpleDateFormat sdf2=new SimpleDateFormat("dd MMM yyyy hh:mm a");
					String txtFillDate  = sdf2.format(date_time);
					Log.d("Date valid till", " " + txtFillDate);
					txtOfferValidTill.append(txtFillDate);
				}  catch (Exception e) {
					e.printStackTrace();
				}
				
				THUMB_IMAGE_URL = b.getString("thumbUrl");
				Image_Url = b.getString("imageUrl");
				
				if(THUMB_IMAGE_URL!=null && THUMB_IMAGE_URL.length()!=0){
					imgStoreImage.setScaleType(ScaleType.FIT_CENTER);
					try {
						/*Picasso.with(context).load(PHOTO_PARENT_URL+THUMB_IMAGE_URL)
						.placeholder(R.drawable.ic_launcher)
						.error(R.drawable.ic_launcher)
						.into(imgStoreImage);*/
						
						imageLoader=ImageLoader.getInstance();

						if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
						{
							cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitLocalCache");
						}
						else
						{
							cacheDir=context.getCacheDir();
						}

						if(!cacheDir.exists())
						{
							cacheDir.mkdirs();
						}/*else if(network.isNetworkAvailable())
						{

							DeleteRecursive(cacheDir);
						}
						 */
						config= new ImageLoaderConfiguration.Builder(context)

						.denyCacheImageMultipleSizesInMemory()
						.threadPoolSize(2)

						.discCache(new UnlimitedDiscCache(cacheDir))
						.enableLogging()
						.build();
						imageLoader.init(config);
						options = new DisplayImageOptions.Builder()
						.cacheOnDisc()

						.bitmapConfig(Bitmap.Config.RGB_565)
						.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
						.build();
						
						imageLoader.displayImage(getResources().getString(R.string.photo_url)+Image_Url, imgStoreImage, options, new ImageLoadingListener() {

							@Override
							public void onLoadingStarted(String imageUri, View view) {
								// TODO Auto-generated method stub
								pBar.setVisibility(View.VISIBLE);
							}

							@Override
							public void onLoadingFailed(String imageUri, View view,
									FailReason failReason) {
								// TODO Auto-generated method stub
								//prog.setVisibility(View.INVISIBLE);
							}

							@Override
							public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
								// TODO Auto-generated method stub
								pBar.setVisibility(View.GONE);
							}

							@Override
							public void onLoadingCancelled(String imageUri, View view) {
								// TODO Auto-generated method stub
								//prog.setVisibility(View.INVISIBLE);
								
							}
						});
					} catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){
						e.printStackTrace();
					}

					imgStoreImage.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ArrayList<String>GALLERY_PHOTO_SOURCE=new ArrayList<String>();
							//GALLERY_PHOTO_SOURCE.add("");
							GALLERY_PHOTO_SOURCE.add(Image_Url);
							Intent intent=new Intent(context,StorePagerGallery.class);
							intent.putExtra("photo_source", GALLERY_PHOTO_SOURCE);
							intent.putExtra("position", 1);
							startActivity(intent);	
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});


				}else{
					imageRow.setVisibility(View.INVISIBLE);
				}
			}



			
			
			
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}

	}
	
	
	private String checkUrlInString(String msUrl){
		String s = msUrl;
		String newString = "";
		// separate input by spaces ( URLs don't have spaces )
		String [] parts = s.split("\\s+");
		//newString = msUrl;
		
		// Attempt to convert each item into an URL.   
		for( String item : parts ) {
			try {
				InorbitLog.d("Info item "+item);
				
				// If possible then replace with anchor...
				if(item.startsWith("www.")){
					item = item.replace("www.", "http://");
				}
				URL url = new URL(item);
				newString = newString + " <a href=\"" + url + "\">"+ url + "</a> " ;
				//System.out.print("<a href=\"" + url + "\">"+ url + "</a> " );    
			} catch (MalformedURLException e) {
				// If there was an URL that was not it!...
				System.out.print( item + " " );
				InorbitLog.d("Info "+item);
				newString = newString + " "+item;
			}
			
		}
		InorbitLog.d("Info " + newString);
		return newString;
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		this.finish();
		/*	overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);*/
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}
}

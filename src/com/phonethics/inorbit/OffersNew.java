package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.view.MenuItem;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phoenthics.adapter.MallOffersAdapter;
import com.phoenthics.adapter.StoreAdapter;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.adapters.ActionAdapter;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.StoreDetails.GalleryResultBroadcast;
import com.phonethics.inorbit.StoreDetails.LikePlace;
import com.phonethics.inorbit.StoreDetails.PageAdapter;
import com.phonethics.inorbit.StoreDetails.ParticularStoreDetailBroadcast;
import com.phonethics.inorbit.StoreDetails.StoresOffer;
import com.phonethics.inorbit.StoreDetails.UnLikePlace;
import com.phonethics.inorbit.StoreListing.SearachBroadcast;
import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;
import com.phonethics.model.StoreInfo;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class OffersNew extends SherlockActivity implements OnNavigationListener {

	private Activity 			mContext;
	private ActionBar 			mActionbar;
	private DBUtil				mDbutil;
	private SessionManager 		mSession;

	private EditText 			mEtSearchText;
	private ProgressBar 		mProg;
	private TextView 			mTvSearchCounter;
	private ImageView 			mIvSearch;
	private TextView 			mTvCounterBack;
	private ImageView 			mIvConnctionErr;
	private PullToRefreshListView mLvStores;
	private ArrayList<String>	mArrMallAreas;
	private ArrayList<StoreInfo> mArrStores;
	
	private SearachBroadcast mSerachBroadCast;
	private LinearLayout	mLinearSearch;
	private InobitStoresDb  mInorbitDb;
	private boolean mbIsRefreshing  = false;
	private ArrayList<PostDetail> mArrOFFERS;
	private StoresOffer offersBroadCast;
	private Map<String, String> flurryEeventParams;
	private TextView mTvOfferCounter;
	private int preVisibleItem;
	private TextView mtvErrorMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offers_new);

		mContext 	= this;
		
		
		/**
		 * initialize layout 
		 * 
		 */
		initViews();
		
		/**
		 * initialize actionbar 
		 * 
		 */
		
		initActionBar();
		

		/**
		 * initialize class objects 
		 * 
		 */
		initObjects();
		
		/**
		 * create list of action bar 
		 * 
		 */
		creatActionBarList();
		
		mLvStores.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				try{
					Intent intent=new Intent(mContext, OffersStore.class);
					intent.putExtra("STORE_INFO", mArrStores.get(position-1));
			
					mContext.startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		
		mLvStores.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				try{
					getOffersStore();
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		});
		
		mLvStores.setOnScrollListener(new OnScrollListener() {

			

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				InorbitLog.d("Scroll State "+mLvStores.getState()+" -- "+scrollState);
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				InorbitLog.d("Pre Visible = "+preVisibleItem+" Firstvisible Visible = "+firstVisibleItem);
				if(preVisibleItem!=firstVisibleItem){
					if((preVisibleItem<firstVisibleItem)){
						//InorbitLog.d("Scrolling down");
						mTvOfferCounter.setVisibility(View.INVISIBLE);
					}else{
						//InorbitLog.d("Scrolling Up");
						mTvOfferCounter.setVisibility(View.VISIBLE);
						mTvOfferCounter.bringToFront();

					}
				}

				preVisibleItem = firstVisibleItem;
				if(preVisibleItem==0 && firstVisibleItem==0){
					mTvOfferCounter.setVisibility(View.INVISIBLE);
				}
			}
		});
		
	}

	void initViews(){
		mProg				= (ProgressBar)findViewById(R.id.searchCategListProg_offers);
		mTvOfferCounter		= (TextView)findViewById(R.id.txt_total_offers);
		mtvErrorMessage = (TextView)findViewById(R.id.text_errorMessage);
		mLvStores			= (PullToRefreshListView)findViewById(R.id.listCategSearchResult_offers);

	}

	void initActionBar(){
		mActionbar	= getSupportActionBar();
		mActionbar.setTitle(getString(R.string.actionBarTitle));
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.show();
	}

	void initObjects(){
		mDbutil 	= new DBUtil(mContext);
		mInorbitDb  = InobitStoresDb.getInstance(mContext);

		mSession	= new SessionManager(getApplicationContext());
		mArrMallAreas	= new ArrayList<String>();
		mArrStores		= new ArrayList<StoreInfo>();
		mArrOFFERS		= new ArrayList<PostDetail>();

	}

	void creatActionBarList(){
		/**
		 * Get mall to initialize the areas
		 * 
		 */
		mArrMallAreas = mDbutil.getAllAreas();
		ActionAdapter mAdAction = new ActionAdapter(mContext, 0, 0, mArrMallAreas);
		mActionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		mActionbar.setListNavigationCallbacks(mAdAction, this);
		mActionbar.setSelectedNavigationItem(mArrMallAreas.indexOf(getActiveAreaName()));
	}


	String getActiveAreaId(){
		return mDbutil.getActiveMallId();
	}

	String getActiveAreaName(){
		return mDbutil.getAreaNameByInorbitId(getActiveAreaId());
	}

	String getMallName(){
		return mArrMallAreas.get((mActionbar.getSelectedNavigationIndex())+1);
	}

	String getCurrentMallId(){
		return mDbutil.getMallIdByPos(""+((mActionbar.getSelectedNavigationIndex())+1));
	}

	String getCurrentMallPlaceParent(){
		return mDbutil.getMallPlaceParentByMallID(getCurrentMallId());
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		String value = mDbutil.getMallIdByPos(""+(itemPosition+1));
		mDbutil.getMallCity(value);
		mDbutil.setActiveMall(value);
		mArrOFFERS.clear();
		createInhouseAnalyticsEvent("0", "8", mDbutil.getActiveMallId());
		
		getOffersStore();
		return false;
	}

	private void createInhouseAnalyticsEvent(String flag, String activityId, String mallId) {
		Uri uri = new Uri.Builder().scheme("content").authority(mContext.getResources().getString(R.string.authority)).
				appendPath("/insert").build();
        ContentValues cv = new ContentValues(6);
		cv.put("UDM_ID", mSession.getUdmIDForCustomer());
		cv.put("DATE", new Date().toString());
		cv.put("FLAG", flag);
		cv.put("ACTIVITY_ID", activityId);
		cv.put("MALL_ID", mallId);
		cv.put("SYNC_STATUS", -1);
		mContext.getContentResolver().insert(uri, cv);
	}
	
	/**
	 * Get the offers from the server based on the active mall id -  not in use now
	 * 
	 */
	void getOffersOfMall(){
		if(NetworkCheck.isNetConnected(mContext)){ /** check if network is availble */
			
			mProg.setVisibility(View.VISIBLE);
			mtvErrorMessage.setVisibility(View.GONE);
			
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			if(mSession.isLoggedInCustomer()){
				headers.put("user_id", mSession.getUserDetailsCustomer().get("user_id_CUSTOMER").toString());
			}else{
				
			}
			List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("mall_id", getActiveAreaId()));
			nameValuePairs.add(new BasicNameValuePair("count", "-1"));

			MyClass myClass  = new MyClass(mContext);
			myClass.getStoreRequest(RequestTags.TagViewBroadcast_mall, nameValuePairs, headers);
		}else{
			mtvErrorMessage.setText(getResources().getString(R.string.noInternetConnection));
			mtvErrorMessage.setVisibility(View.VISIBLE);
			mProg.setVisibility(View.GONE);
			//Toast.makeText(mContext, "No Internet connection", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	/**
	 * Get the store list the server which are currently running any offers from malls place parent 
	 * 
	 */
	void getOffersStore(){
		if(NetworkCheck.isNetConnected(mContext)){
			mtvErrorMessage.setVisibility(View.GONE);
			mProg.setVisibility(View.VISIBLE);
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
			
			List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("place_id", getCurrentMallPlaceParent()));
			nameValuePairs.add(new BasicNameValuePair("search", ""));
			nameValuePairs.add(new BasicNameValuePair("offer", "1"));
			
			MyClass myClass  = new MyClass(mContext);
			myClass.getStoreRequest(RequestTags.TagSearchStores, nameValuePairs, headers);
			
			
		} else {
			mProg.setVisibility(View.GONE);
			mtvErrorMessage.setText(getResources().getString(R.string.noInternetConnection));
			mtvErrorMessage.setVisibility(View.VISIBLE);
			//mProg.setVisibility(View.GONE);
			///Toast.makeText(mContext, "No Internet connection", Toast.LENGTH_SHORT).show();
		}
		
	}
	


	/**
	 * 
	 * Broadcast receiver of the store offers network responce
	 * 
	 *
	 */
	class StoresOffer extends BroadcastReceiver{

		private boolean mbisStoresCount = true;

		@Override
		public void onReceive(final Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				mLvStores.onRefreshComplete();
				mProg.setVisibility(View.GONE);
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					
					
					if(mbisStoresCount ) {
						String success = bundle.getString("SUCCESS");
						if(success.equalsIgnoreCase("true")) {
							
							
							ArrayList<StoreInfo> _mArrStoreInfos = intent.getParcelableArrayListExtra("StoreInfo");
							mArrStores.clear();
							for(int i=0;i<_mArrStoreInfos.size();i++){
								mArrStores.add(_mArrStoreInfos.get(i));
							}
							mLvStores.setVisibility(View.VISIBLE);
							mLvStores			= (PullToRefreshListView)findViewById(R.id.listCategSearchResult_offers);
							StoreAdapter _mAdapterStore = new StoreAdapter(mContext, mArrStores, true);
							mLvStores.setAdapter(_mAdapterStore);
							if(Tables.getCurrentCategory().equalsIgnoreCase(Tables.CATEGORY_OFFER)){
								mTvOfferCounter.setText("Offers - "+mArrStores.size()+" Stores");
							}else{
								mTvOfferCounter.setText(mArrStores.size()+" Stores");
							}
							
							mTvOfferCounter.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
						} else {
							mLvStores.setVisibility(View.GONE);
							String msMessage = bundle.getString("MESSAGE");
							
							ShowMessageDialog.show(mContext, msMessage);
						}
					} else {
						String success = bundle.getString("SUCCESS");
						if(success.equalsIgnoreCase("true")){
							String total_pages = bundle.getString("TOTAL_PAGE");
							String total_record = bundle.getString("TOTAL_RECORD");
							bundle = intent.getBundleExtra("BundleData");
							if(bundle!=null){
								ArrayList<PostDetail> postArr = bundle.getParcelableArrayList("POST_DETAILS");
								mArrOFFERS = postArr;
								ArrayList<String> offerTitle = new ArrayList<String>();
								for(int i=0;i<mArrOFFERS.size();i++){
									InorbitLog.d("Post "+mArrOFFERS.get(i).getTitle());
									offerTitle.add(mArrOFFERS.get(i).getTitle());
								}
								
								//mLvStores.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, offerTitle));
								mLvStores.setAdapter(new MallOffersAdapter((Activity)mContext, mArrOFFERS));
								mTvOfferCounter.setText(mArrOFFERS.size()+" Offers");
								mTvOfferCounter.setTypeface(InorbitApp.getTypeFace());
							}
						}else{
							String message = bundle.getString("MESSAGE");
							Toast.makeText(mContext, message, 0).show();

							boolean isVolleyError = bundle.getBoolean("volleyError",false);
							if(isVolleyError){

								int code1 = bundle.getInt("CODE",0);
								String message1=bundle.getString("MESSAGE");
								String errorMessage = bundle.getString("errorMessage");
								String apiUrl = bundle.getString("apiUrl");	
								if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
									EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "View Post");
								}

								Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();

							}

						}
					}
					
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
			//IntentFilter offersFilter = new IntentFilter(RequestTags.TagViewBroadcast_mall);
			IntentFilter offersFilter = new IntentFilter(RequestTags.TagSearchStores);
			
			mContext.registerReceiver(offersBroadCast = new StoresOffer(), offersFilter);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}
	

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try{
			EventTracker.endLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}



	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		try{
			EventTracker.endFlurrySession(getApplicationContext());
			if(offersBroadCast!=null){
				mContext.unregisterReceiver(offersBroadCast);
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		finishTo();
		return true;
	}
	
	void finishTo(){
		Intent intent = new Intent();
		setResult(5, intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		InorbitLog.d("Offers Back Press");
		finishTo();
	}
	
	void uploadBroadcastEvent(String title,String StoreName){
		try{
			//InorbitLog.d("Broadcast  MallName - "+MALL_NAME + " StroeName "+REC_SOTRE_NAME );
			boolean param  = false;
			flurryEeventParams = new HashMap<String, String>();
			if(getActiveAreaName()!=null && !getActiveAreaName().equals("")){
				flurryEeventParams.put("MallName", getActiveAreaName());
				param  = true;
			}else{
				param = false;
			}
			if(StoreName!=null && !StoreName.equals("")){
				flurryEeventParams.put("StoreName", StoreName +" - "+getActiveAreaName());
				param  = true;
			}else{
				param = false;
			}
			if(title!=null && !title.equals("")){
				if(param){
					param  = true;
					flurryEeventParams.put("BroadCastTitle", title +" - "+ StoreName +" - "+getActiveAreaName());
				}else{
					param  = false;
				}
			}
			if(param){
				EventTracker.logEvent(getResources().getString(R.string.offerDetail_View), flurryEeventParams);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}
	
	

}

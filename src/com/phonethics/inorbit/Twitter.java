package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.CategorySearch.CustomSpinnerAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class Twitter extends SherlockActivity implements OnNavigationListener {


	WebView web;
	ImageView img,bckimg,share_btn,bck,forth,refresh;	
	ImageView showProgress;
	Context context;
	String	URL="";
	boolean isFB=false;
	ProgressBar	pBar;
	int dropdown_position;
	DBUtil	dbUtil;
	ActionBar actionbar;
	ArrayList<String>dropdownItems;
	Activity		actContext;
	String value;
	String userAgent;
	private String MALL_NAME;
	private boolean isTwitter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter);


		context = this;
		actContext = this;
		/*	PushService.setDefaultPushCallback(context, FbTwtrLink.class);*/
		dbUtil = new DBUtil(context);
		dropdownItems=new ArrayList<String>();
		Bundle bundle=getIntent().getExtras();

		img = (ImageView)findViewById(R.id.header);
		bckimg = (ImageView) findViewById(R.id.webback);
		bck = (ImageView) findViewById(R.id.webback);
		forth = (ImageView) findViewById(R.id.forth);
		refresh = (ImageView) findViewById(R.id.refresh);
		pBar = (ProgressBar) findViewById(R.id.pBar_web);


		//Action bar
		actionbar=getSupportActionBar();
		actionbar.setTitle(getString(R.string.actionBarTitle));
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();

		dropdownItems=new ArrayList<String>();
		dropdownItems = dbUtil.getAllAreas();

		//Create Adapter
		CustomSpinnerAdapter business_list=new CustomSpinnerAdapter(actContext, 0, 0, dropdownItems);


		//Setting Navigation Type.



		web=(WebView)findViewById(R.id.web_view);
		web.getSettings().setJavaScriptEnabled(true);
		web.getSettings().setPluginState(PluginState.ON);
		//web.getSettings().setDomStorageEnabled(true);
		//String userAgent1 = "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17";
		//userAgent = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";


		if(bundle!=null){
			isFB = bundle.getBoolean("isFacebook",false);
			isTwitter = bundle.getBoolean("isTwitter",false);
			if(isFB){
				registerEvent(MALL_NAME);
				URL = bundle.getString("url");
				actionbar.setTitle("Facebook");
				actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
				actionbar.setListNavigationCallbacks(business_list, this);
				String activeAreaId = dbUtil.getActiveMallId();
				String areaName = dbUtil.getAreaNameByInorbitId(activeAreaId);
				int areaPos = dropdownItems.indexOf(areaName);
				Log.d("", "Inorbit active id "+activeAreaId+" Area : "+areaName+" ArrayPos "+areaPos);
				actionbar.setSelectedNavigationItem(areaPos);
			}else if(isTwitter){
				actionbar.setTitle("Twitter");
				actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
				actionbar.setListNavigationCallbacks(business_list, this);
				String activeAreaId = dbUtil.getActiveMallId();
				String areaName = dbUtil.getAreaNameByInorbitId(activeAreaId);
				int areaPos = dropdownItems.indexOf(areaName);
				Log.d("", "Inorbit active id "+activeAreaId+" Area : "+areaName+" ArrayPos "+areaPos);
				actionbar.setSelectedNavigationItem(areaPos);
			}else{
				boolean isYouTube = bundle.getBoolean("isYouTube",false);
				if(isYouTube){
					URL = getResources().getString(R.string.youtube_url);
					actionbar.setTitle("YouTube");
					callWebForFbTwitter(false, URL);
				}else{
					String title = bundle.getString("ActivityTitle");
					URL = bundle.getString("url");
					actionbar.setTitle(title);
					callWebForFbTwitter(false, URL);
				}
			}


		}



	}



	class MyWebViewClient extends WebViewClient {
		@Override
		// show the web page in webview but not in web browser



		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			view.loadUrl(url);
			Log.d("", "Inorbit Web Url >> " +url);




			bck.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(web.canGoBack())
					{
						web.goBack();
					}
				}
			});

			forth.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(web.canGoForward())
					{
						web.goForward();
					}


				}
			});


			refresh.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					web.reload();

				}
			});



			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);

			/*showProgress.clearAnimation();
			showProgress.setVisibility(View.GONE);*/
			pBar.setVisibility(View.GONE);
		}


		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);

			/*	showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
			showProgress.setVisibility(View.VISIBLE);*/
			pBar.setVisibility(View.VISIBLE);
		}

		@Override
		public void onLoadResource(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onLoadResource(view, url);

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
		finishTo();

	}



	class CustomSpinnerAdapter extends ArrayAdapter<String> implements SpinnerAdapter 
	{

		ArrayList<String>name;
		Activity context;
		LayoutInflater layoutInflater;
		public CustomSpinnerAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> name) {
			super(context, resource, textViewResourceId, name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.name=name;
			layoutInflater=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}


		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return name.get(position);
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/* convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_menus, parent, false);
			}
			((TextView) convertView.findViewById(R.id.listText))
			.setText(getItem(position));
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/*convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_dropdown_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_dropdown_item, parent, false);
			}
			((TextView) convertView.findViewById(R.id.txtSpinner))
			.setText(getItem(position));
			return convertView;
		}


	}



	void callWebForFbTwitter(boolean isFb,String url){
		if(isFb){

			bckimg.setVisibility(View.VISIBLE);
			//web.getSettings().setUserAgentString(userAgent1);
			web.getSettings().setBuiltInZoomControls(true);

			web.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			web.clearHistory();
			web.getSettings().setDomStorageEnabled(false);
			web.setWebViewClient(new MyWebViewClient());
			web.loadUrl(url);
		}else{


			bckimg.setVisibility(View.VISIBLE);
			//web.getSettings().setUserAgentString(userAgent);
			web.getSettings().setBuiltInZoomControls(true);

			web.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			web.clearHistory();
			web.getSettings().setDomStorageEnabled(false);
			web.setWebViewClient(new MyWebViewClient());
			web.loadUrl(url);
		}
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		value = dbUtil.getMallIdByPos(""+(itemPosition+1));
		dbUtil.setActiveMall(value);
		MALL_NAME = dbUtil.getAreaNameByInorbitId(value);
		String parant_place = dbUtil.getParentPlaceIdByAreaName(MALL_NAME);

		if(isFB){
			//callWebForFbTwitter(true,dbUtil.getFbUrlByPos(""+(itemPosition+1)));
			callWebForFbTwitter(true,dbUtil.getFbUrlFromParentStore(""+(parant_place)));
		}else{
			callWebForFbTwitter(true,dbUtil.getTwitterFromParentStore(""+(parant_place)));
		}

		return false;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		/*if(item.getTitle().toString().equalsIgnoreCase("Facebook"))
		{

			Intent intent = new Intent();
			setResult(5, intent);
			this.finish();	
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}else{
			Intent intent = new Intent();
			setResult(5, intent);
			this.finish();	
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		 */

		finishTo();
		return true;
	}


	void finishTo(){
		Intent intent = new Intent();
		setResult(5, intent);
		this.finish();	
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}




	@Override
	protected void onResume() {
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onResume();
	}


	@Override
	protected void onPause() {

		try{
			EventTracker.endLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onPause();
	}


	/* on start */
	@Override
	protected void onStart() {
		super.onStart();

		try{
			EventTracker.startFlurrySession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
	}

	void registerEvent(String eventName){
		try{
			Map<String, String> params = new HashMap<String, String>();
			params.put("MallName", MALL_NAME);
			EventTracker.logEvent(getResources().getString(R.string.socialConnect)+"Facebook", false);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

}

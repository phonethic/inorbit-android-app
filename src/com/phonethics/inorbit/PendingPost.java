

package com.phonethics.inorbit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.R.integer;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class PendingPost extends SherlockActivity {


	ActionBar 	actionBar;
	Activity 	context;
	ListView	listUnapprovePost;
	DBUtil 					dbUtil;
	String 					USER_ID;
	String 					AUTH_ID;
	SessionManager 			session;
	public static final String KEY_USER_ID="user_id";
	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";
	NetworkCheck isnetConnected;
	private PostReceiver postReceiver;
	ProgressBar  pBar;
	private Dialog operationDialog;
	ArrayList<PostDetail> POSTARR;
	private ApprovePostReceiver postApproveReceiver;
	String POST_ID = "";
	private SearchListAdapter adapter;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pending_post);

		
		/**
		 * 
		 * Initialize the layout and class variables
		 * 
		 */
		context				= this;
		actionBar			= getSupportActionBar();
		actionBar.setTitle("Pending Offers");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		session				 = new SessionManager(context);
		isnetConnected		 = new NetworkCheck(context);
		dbUtil				 = new DBUtil(context);
		pBar				 = (ProgressBar) findViewById(R.id.progressBar_unapprve);
		POSTARR			     = new ArrayList<PostDetail>();
		listUnapprovePost    = (ListView) findViewById(R.id.list_post_unapprove);
		adapter = new SearchListAdapter((Activity) context, R.layout.offers_broadcast_layout, POSTARR);
		listUnapprovePost.setAdapter(adapter);
		

		listUnapprovePost.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				try{
					//Intent intent=new Intent(context, OffersBroadCastDetails.class);
					Intent intent=new Intent(context, OfferViewNew.class);
					//intent.putExtra("storeName", storeName);
					
					Conifg.fromPedingOffers = true;
					intent.putExtra("POST_ID",POSTARR.get(pos).getId());
					intent.putExtra("title", POSTARR.get(pos).getTitle());
					intent.putExtra("body", POSTARR.get(pos).getDescription());
					intent.putExtra("photo_source", POSTARR.get(pos).getImage_url1());
					intent.putExtra("fromManager", true);
					startActivityForResult(intent, 2);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}catch(NullPointerException npex){
					npex.printStackTrace();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});

		listUnapprovePost.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				showApproveDialog(pos);
				return true;
			}
		});

		
		/** Network request */
		getAllUnApprovedPost();

	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		SubMenu subMenu1 = menu.addSubMenu("Refresh");
		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.refresh_active);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Refresh"))
		{
			Cache.clearCache(context);
			getAllUnApprovedPost();
		}else{
			onBackPressed();
		}

		return true;
	}


	String getUserId(){
		HashMap<String,String>user_details = session.getUserDetails();
		return user_details.get(KEY_USER_ID).toString();

	}

	String getAuthId(){
		HashMap<String,String>user_details = session.getUserDetails();
		return user_details.get(KEY_AUTH_ID).toString();

	}

	String getPlaceId(){
		return dbUtil.getPlaceId("0");
	}


	/**
	 * 
	 * Network request to get UnApproved post based on mall id
	 * 
	 */
	public void getAllUnApprovedPost(){
		try {
			pBar.setVisibility(View.VISIBLE);
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put(getResources().getString(R.string.api_header),getResources().getString(R.string.api_value));
			headers.put(KEY_USER_ID,getUserId());
			headers.put(KEY_AUTH_ID,getAuthId());
			MyClass myClass = new MyClass(context);
			if(isnetConnected.isNetworkAvailable()){
				/*if(dbUtil.isMultipuleMallsLoggedIn()){
					pBar.setVisibility(View.VISIBLE);
					ArrayList<String> placeIds = dbUtil.getPlaceIds("0");
					for(int i = 0;i<placeIds.size();i++){
						List<NameValuePair>  nameValue = new ArrayList<NameValuePair>();
						nameValue.add(new BasicNameValuePair("place_id", placeIds.get(i)));
						myClass.getStoreRequest(RequestTags.Tag_UnapprovedBroadcast, nameValue, headers);
					}
				} else {
					pBar.setVisibility(View.VISIBLE);
					List<NameValuePair>  nameValue = new ArrayList<NameValuePair>();
					nameValue.add(new BasicNameValuePair("place_id", dbUtil.getPlaceId("0")));
					myClass.getStoreRequest(RequestTags.Tag_UnapprovedBroadcast, nameValue, headers);
					InorbitLog.d("Merchnat user id "+getUserId());
				}*/
				
				List<NameValuePair>  nameValue = new ArrayList<NameValuePair>();
				nameValue.add(new BasicNameValuePair("place_id", dbUtil.getPlaceId("0")));
				myClass.getStoreRequest(RequestTags.Tag_UnapprovedBroadcast, nameValue, headers);
				InorbitLog.d("Merchnat user id "+getUserId());
			}else{
				showToast("No Internet Connection");
				pBar.setVisibility(View.GONE);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			pBar.setVisibility(View.GONE);
		}

	}


	/**
	 * Broadcast receiver getAllUnApprovedPost()
	 * 
	 * @author Nitin
	 *
	 */
	class PostReceiver extends BroadcastReceiver{
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String success = bundle.getString("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						/*String total_pages = bundle.getString("TOTAL_PAGE");
						String total_record = bundle.getString("TOTAL_RECORD");
						TOTAL_SEARCH_PAGES = total_pages;
						TOTAL_SEARCH_RECORDS = total_record;*/
						bundle = intent.getBundleExtra("BundleData");
						if(bundle!=null){
							POSTARR = bundle.getParcelableArrayList("POST_DETAILS");
							//POSTARR = postArr;
							adapter.notifyDataSetChanged();
						}
					}else{
						String message = bundle.getString("MESSAGE");
						
						showToast(message);
						POSTARR.clear();
						adapter.notifyDataSetChanged();
						//POSTARR = new ArrayList<PostDetail>();
						//listUnapprovePost.setAdapter(new SearchListAdapter((Activity) context, R.layout.offers_broadcast_layout, POSTARR));
						String code = bundle.getString("CODE");
						if(code.equalsIgnoreCase("-116")) {
							Intent intent1=new Intent(context,MerchantLogin.class);
							startActivity(intent1);
							finish();
						}

						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){


							int code1 = bundle.getInt("CODE",0);
							String message1=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "PendingPost");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			} finally {
				pBar.setVisibility(View.GONE);
			}
		}

	}

	
	class ApprovePostReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				pBar.setVisibility(View.GONE);
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					String message = intent.getStringExtra("MESSAGE");
					if(success.equalsIgnoreCase("true")){
						//removePost();
						showToast(message);
						getAllUnApprovedPost();
						//POSTARR = new ArrayList<PostDetail>();
						//listUnapprovePost.setAdapter(new SearchListAdapter((Activity) context, R.layout.offers_broadcast_layout, POSTARR));
					}else{
						showToast(message);
						String code = intent.getStringExtra("CODE");
						if(code.equalsIgnoreCase("-197")){
							getAllUnApprovedPost();
						}
						boolean isVolleyError = intent.getBooleanExtra("volleyError",false);
						if(isVolleyError){
							String code1 = intent.getStringExtra("CODE");
							message=intent.getStringExtra("MESSAGE");
							String errorMessage = intent.getStringExtra("errorMessage");
							String apiUrl = intent.getStringExtra("apiUrl");	
							EventTracker.reportException(code1, apiUrl+" - "+errorMessage, "PendingPost");
							Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
						}
					}
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	class SearchListAdapter extends ArrayAdapter<PostDetail>{
		/*ArrayList<PostDetail> postArr;*/
		Activity context;
		LayoutInflater inflate;
		//	ImageLoader  imageLoaderList;
		private String PHOTO_PARENT_URL;

		public SearchListAdapter(Activity context, int resource,
				ArrayList<PostDetail> postArr) {
			super(context, resource, postArr);
			// TODO Auto-generated constructor stub
			/*this.postArr=postArr;*/
			//imageLoaderList = new ImageLoader(context);
			inflate=context.getLayoutInflater();
			PHOTO_PARENT_URL=context.getResources().getString(R.string.photo_url);

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return POSTARR.size();
		}
		@Override
		public PostDetail getItem(int position) {
			// TODO Auto-generated method stub
			return POSTARR.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.pending_post_list_layout,null);

				holder.txtTitle=(TextView)convertView.findViewById(R.id.broadCastOffersText);
				holder.imgBroadcastLogo=(ImageView)convertView.findViewById(R.id.imgBroadcastLogo);
				holder.txtDate=(TextView)convertView.findViewById(R.id.broadCastOffersDate);
				holder.txtMonth=(TextView)convertView.findViewById(R.id.broadCastOffersMonth);
				holder.viewOverLay=(View)convertView.findViewById(R.id.viewOverLay);
				holder.txtBroadCastStoreTotalLike=(TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalLike);
				holder.txtBroadCastStoreTotalViews = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalViews);
				holder.txtBroadCastStoreTotalShare = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalShare);
				holder.txtStoreTitle = (TextView)convertView.findViewById(R.id.broadStoreTitle);


				holder.txtTitle.setTypeface(InorbitApp.getTypeFace());
				holder.txtStoreTitle.setTypeface(InorbitApp.getTypeFace(),Typeface.ITALIC);

				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			/*hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);*/

			hold.txtTitle.setText(POSTARR.get(position).getTitle());
			try
			{
				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");
				Date date_con = (Date) dt.parse(POSTARR.get(position).getDate().toString());
				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);

				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+POSTARR.get(position).getDate().toString());
				hold.txtDate.setText(day+"");
				hold.txtMonth.setText(dt2.format(date_con)+"");


				hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);
				hold.txtBroadCastStoreTotalViews.setVisibility(View.GONE);

				hold.txtBroadCastStoreTotalShare.setVisibility(View.GONE);



			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			try
			{
				if(POSTARR.get(position).getImage_url1().toString().equalsIgnoreCase("notfound"))
				{
					//Log.i("LOGO", "LOGO "+logo.get(position));
					//				imageLoaderList.DisplayImage("",hold.imgBroadcastLogo);

					//Picasso.with(context).load("").into(hold.imgBroadcastLogo);
					hold.txtDate.setTextColor(Color.argb(255, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.INVISIBLE);
				}
				else
				{
					String photo_source=POSTARR.get(position).getImage_url1().toString().replaceAll(" ", "%20");
					hold.txtDate.setTextColor(Color.argb(200, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.VISIBLE);
					//				imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_source, hold.imgBroadcastLogo);

					try {

						Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
						.skipMemoryCache()
						.placeholder(R.drawable.place_holder)
						.error(R.drawable.place_holder)
						.into(hold.imgBroadcastLogo);




					}catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}

				//String title = dbUtil.getShopNameByPlaceID(getItem(position).getPlace_id());
				String title = dbUtil.getStoreNameFromPlaceId(getItem(position).getPlace_id());
				
				
				//getStoreNameFromPlaceId
				//InorbitLog.d("From : "+title);
				if(title!=null && !title.equalsIgnoreCase("")){
					hold.txtStoreTitle.setText("From : "+title);
				}


			}catch(Exception ex)
			{
				ex.printStackTrace();
			}


			return convertView;
		}
	}


	static class ViewHolder
	{

		TextView txtTitle,txtDate,txtMonth;
		ImageView imgBroadcastLogo;
		View viewOverLay;
		TextView txtBroadCastStoreTotalLike;
		TextView txtBroadCastStoreTotalViews;
		TextView txtBroadCastStoreTotalShare;
		TextView txtStoreTitle;


	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		IntentFilter filter = new IntentFilter(RequestTags.Tag_UnapprovedBroadcast);
		IntentFilter approveFilter = new IntentFilter(RequestTags.Tag_ApproveBroadcast);
		context.registerReceiver(postReceiver = new PostReceiver(), filter);
		context.registerReceiver(postApproveReceiver = new ApprovePostReceiver(), approveFilter);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(postReceiver!=null){
			context.unregisterReceiver(postReceiver);	
		}
		if (postApproveReceiver != null)
			context.unregisterReceiver(postApproveReceiver);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(context, MerchantHome.class);
		startActivity(intent); 
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}


	public void showToast(String text){
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}



	public void showApproveDialog(final int listPosition){
		
		operationDialog  = new Dialog(context);
		operationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		operationDialog.setContentView(R.layout.approve_item_dialog);
		operationDialog.setTitle(null);
		ListView listCateg = (ListView)operationDialog.findViewById(R.id.categDilogList);
		
		ArrayList<String> 	operationArr = new ArrayList<String>();
		operationArr.add("Approve");
		operationArr.add("Reject");
		
		listCateg.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, operationArr));
		listCateg.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				operationDialog.dismiss();
				String storeName = dbUtil.getStoreNameFromPlaceId(POSTARR.get(listPosition).getPlace_id());
				String mallId = dbUtil.getMallNameFromPlaceId(POSTARR.get(listPosition).getPlace_id());
				String mallName  = dbUtil.getMallNameById(mallId);
				//Toast.makeText(context, title+" / "+mallName, 0).show();
				if(pos==0){
					if((storeName!=null && !storeName.equalsIgnoreCase(""))&&(mallName!=null && !mallName.equalsIgnoreCase(""))){
						registerEvent(true, storeName, mallName);
					}

					approvePost(POSTARR.get(listPosition).getId().toString(),true);
					
				}else{
					if((storeName!=null && !storeName.equalsIgnoreCase(""))&&(mallName!=null && !mallName.equalsIgnoreCase(""))){
						registerEvent(false, storeName, mallName);
					}
					approvePost(POSTARR.get(listPosition).getId().toString(),false);
				}
			}
		});
		operationDialog.show();
	}

	
	/**
	 * 
	 * Network request to approve or reject any offer
	 * 
	 * @param postId Offer id
	 * @param isApprove if true than approve the selected offer, false to reject the offer
	 */
	void approvePost(String postId,boolean isApprove){
		try{
			if(isnetConnected.isNetworkAvailable()){

				POST_ID = postId;
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put(getResources().getString(R.string.api_header),getResources().getString(R.string.api_value));
				headers.put("X-HTTP-Method-Override","PUT");
				headers.put(KEY_USER_ID,getUserId());
				headers.put(KEY_AUTH_ID,getAuthId());
				
				JSONObject jObj = new JSONObject();
				jObj.put("post_id", postId);
				if(isApprove){
					jObj.put("status", "1");
				}else{
					jObj.put("status", "2");
				}


				MyClass myClass = new MyClass(context);
				myClass.postRequest(RequestTags.Tag_ApproveBroadcast, headers, jObj);
				pBar.setVisibility(View.VISIBLE);
				InorbitLog.d("Merchnat user id "+getUserId());
				pBar.setVisibility(View.VISIBLE);
			}else{
				showToast("No Internet Connection");
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	public void removePost(){
		POSTARR.remove(getIndexOfPost());
		adapter.notifyDataSetChanged();
	}

	void registerEvent(boolean isApproved,String storeName,String mallName){
		Map<String, String> params = new HashMap<String, String>();
		if((storeName!=null && !storeName.equalsIgnoreCase("")) && (mallName!=null && !mallName.equalsIgnoreCase(""))){
			params.put("StoreName", storeName+"-"+mallName);
			params.put("MallName", mallName);

			if(isApproved){
				EventTracker.logEvent(getResources().getString(R.string.mPostApproved), params);
			}else{
				EventTracker.logEvent(getResources().getString(R.string.mPostRejected), params);
			}
		}
	}
	public int getIndexOfPost(){
		int pos = 0;
		for(int i=0;i<POSTARR.size();i++){
			String id = POSTARR.get(i).getId();
			if(id.equalsIgnoreCase(POST_ID)){
				pos  = i;
				break;
			}
		}
		return pos;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		try{
			getAllUnApprovedPost();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}




}

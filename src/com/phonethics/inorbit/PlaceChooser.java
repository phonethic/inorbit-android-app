
package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.ParticularStoreInfo;
import com.phonethics.model.RequestTags;
import com.phonethics.model.StoreInfo;
import com.phonethics.model.StoreListModel;
import com.squareup.picasso.Picasso;
/*import com.urbanairship.push.PushManager;*/

public class PlaceChooser extends SherlockActivity {

	ActionBar 			mActionBar;
	Activity 			mContext;
	//ListView 			mlvSinglePlaceChooserList;
	ListView 			mlvMultiPlaceChooserList;
	Button 				mbtnSelectStore;
	CreateBroadCastListAdapter 			mAdapter;
	String 								msSlected_store_id;
	ArrayList<CheckableStoreListModel> allStores = new ArrayList<PlaceChooser.CheckableStoreListModel>();

	PlaceChooserBroadcastReceiver 	mbPlaceChooserBroadcast;
	SessionManager 					mSessionMnger;
	
	int miChoiceMode	= -1;
	int miViewPost		= -1;
	int miAddStore		= 0;
	int miManageGallery = 0;
	int miSure_shop		= -1;
	int miTextlength	= 0;
	RelativeLayout 		mRelProgressLayout;
	DBUtil				mdbUtil;
	EditText 			metSearchText;
	int 				miMultiple_txtlngth = 0;
	boolean misNewShakeWin = true;
	private TextView textOfferMall,textOfferStore,textNext;
	LinearLayout mImgHelp;
	private boolean mScrollAble = false;


	@Override  
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_place_chooser);

		mContext = this;
		
		initViews();
		
		getBundleValues();

		
		/**
		 * Check whether to load the stores from server or not.
		 * This check is maintained to see if any store has been added/updated
		 * 
		 */
		SharedPreferences prefs=mContext.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
		boolean isPlaceRefreshRequired=prefs.getBoolean("isPlaceRefreshRequired", false);
		Log.i("isPlaceRefreshRequired", "isPlaceRefreshRequired "+isPlaceRefreshRequired);


		
		/**
		 * 
		 * If data needs to be refresh than request a server to get the new information of the stores
		 * This network request has been done through loadAllStoreForUser().
		 * 
		 */
		if(isPlaceRefreshRequired) {
			mImgHelp.setVisibility(View.INVISIBLE);
			loadAllStoreForUser();
			
			
			/**
			 * If stores information is present locally than load all these stores from database
			 * 
			 */
		}else if(mdbUtil.getStoresRowCount()!=0){
			//mImgHelp.setVisibility(View.VISIBLE);
			
			/*
			final ArrayList<String> STOREID = mdbUtil.getAllStoreIDs();
			final ArrayList<String> STORE_NAME = mdbUtil.getAllStoresNames();
			final ArrayList<String> PHOTO      = mdbUtil.getAllStorePhotoUrl();
			final ArrayList<String> STORE_DESC = mdbUtil.getAllStoreDesc();
			final ArrayList<String> MALL_ID = mdbUtil.getAllShopsMallIds();
			final ArrayList<String> PLACE_STATUS = mdbUtil.getAllStoresPlaceStatus();
			final ArrayList<String> PLACE_PARENT = mdbUtil.getAllShopsPlaceParent();
			*/
			ArrayList<ParticularStoreInfo> storeInfos = mdbUtil.getStoreList();
			for (ParticularStoreInfo p: storeInfos) {
				allStores.add(new CheckableStoreListModel(p, false));
			}
			setArraysForBroadCast();
			
			/**
			 * If stores information is not there on DB 
			 * 
			 */
		}else if(NetworkCheck.isNetConnected(mContext)){
			mImgHelp.setVisibility(View.INVISIBLE);
			loadAllStoreForUser();
			
			/**
			 *  If stores information is not there on DB and Internet is not availble for a network request 
			 *  than prompt for "No Internet Connection"
			 * 
			 */
		}else{
			mImgHelp.setVisibility(View.INVISIBLE);
			showToast(getResources().getString(R.string.noInternetConnection));
		}
		
		/**
		 * Not in use
		 * 
		 */
		textNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(textNext.getText().toString().equalsIgnoreCase("Next")){
					textNext.setText("Yes. I got it.");
					//mImgHelp.setBackgroundResource(R.drawable.help_screen_db);
					//textOfferStore.setText("Click/browse here for the offers related to stores");
					textOfferStore.setVisibility(View.VISIBLE);
					textOfferMall.setVisibility(View.INVISIBLE);
					animateThisView(textOfferStore, AnimationUtils.loadAnimation(mContext, R.anim.fade_in_splash));
				}else{
					mScrollAble = false;
					metSearchText.setVisibility(View.VISIBLE);
					animateThisView(mImgHelp, AnimationUtils.loadAnimation(mContext, R.anim.fade_out_splash));
					mImgHelp.setVisibility(View.INVISIBLE);
				}

			}

		});
	}

	/**
	 * 
	 * Initialize the layout and class variables
	 * 
	 * 
	 */
	void initViews(){
		mActionBar	=	getSupportActionBar();
		mActionBar.setTitle("Choose Place");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();


		mSessionMnger				=	new SessionManager(getApplicationContext());
		mdbUtil 					= 	new DBUtil(mContext);

		//Views
		//mlvSinglePlaceChooserList	= (ListView)findViewById(R.id.singlePlaceChooserList);

		metSearchText 				= (EditText) findViewById(R.id.searchText);
		metSearchText.setVisibility(View.VISIBLE);

		mlvMultiPlaceChooserList	= (ListView)findViewById(R.id.MultiPlaceChooserList);
		mbtnSelectStore				= (Button)findViewById(R.id.btnSelectStore);
		mRelProgressLayout			= (RelativeLayout)findViewById(R.id.progressLayout);
		mImgHelp					= (LinearLayout)findViewById(R.id.img_helper);
		textOfferMall				= (TextView)findViewById(R.id.text_offer_mall);
		textOfferStore				= (TextView)findViewById(R.id.text_offer_store);
		textNext					= (TextView) findViewById(R.id.text_next);
		textNext.setVisibility(View.VISIBLE);

		textOfferMall.setTypeface(InorbitApp.getTypeFace());
		textOfferStore.setTypeface(InorbitApp.getTypeFace());
		textOfferMall.setTextColor(Color.parseColor("#eebf31"));
		textOfferStore.setTextColor(Color.parseColor("#eebf31"));
		textNext.setTextColor(Color.parseColor("#eebf31"));

		//textOfferMall.setText("Click here for the offers related to Inorbit Mall");
		//textOfferStore.setText("Click/browse here for the offers related to stores");
		textNext.setText("Next");


	}

	
	
	/**
	 * 
	 * Get bundle values to set the layout accordingly
	 * 
	 */
	private void getBundleValues() {
		// TODO Auto-generated method stub
		Bundle bundle=getIntent().getExtras();
		if(bundle!=null){
			
			
			/**
			 * 
			 * Get the choice mode value to check which list view to show
			 * if Choice mode = 1 than normal ListView will be visible
			 * if Choice mode = 2 than ListView with checkBox will be visible
			 * 
			 */
			miChoiceMode=bundle.getInt("choiceMode");
			
			/**
			 * 
			 * if miViewPost = 1 than place chooser has been called to view the offers of the stores
			 * 
			 */
			miViewPost=bundle.getInt("viewPost");
			
			/**
			 * 
			 * if miSure_shop = 1 than place chooser has been called from "ShakeAndWin" tab 
			 * 
			 */
			miSure_shop=bundle.getInt("sure_shop");
			
			/**
			 * 
			 * if miAddStore = 1 than place chooser has been called from "Add Store" tab 
			 * 
			 */
			miAddStore=bundle.getInt("AddStore");
			
			/**
			 * 
			 * if miManageGallery = 1 than place chooser has been called from "Manage Gallery" tab 
			 * 
			 */
			miManageGallery=bundle.getInt("manageGallery");
			
			
			if(miChoiceMode==1) {
				//mlvSinglePlaceChooserList.setVisibility(View.VISIBLE);
				//mlvMultiPlaceChooserList.setVisibility(View.GONE);
				mbtnSelectStore.setVisibility(View.GONE);
			}
			//Broadcast
			else if(miChoiceMode==2){
				//mlvSinglePlaceChooserList.setVisibility(View.GONE);
				//mlvMultiPlaceChooserList.setVisibility(View.VISIBLE);
				mbtnSelectStore.setVisibility(View.VISIBLE);
				//mImgHelp.setVisibility(View.INVISIBLE);
			}
			mlvMultiPlaceChooserList.setVisibility(View.VISIBLE);
			
			metSearchText.setVisibility(View.INVISIBLE);
			
			//if(miChoiceMode==2) {
				mImgHelp.setVisibility(View.GONE); 
				metSearchText.setVisibility(View.VISIBLE);
				
			//} else {
				
				
				/**
				 * 
				 * Set the helper text
				 * 
				 */
				if(miSure_shop==1) {
					mImgHelp.setVisibility(View.GONE); 
					metSearchText.setVisibility(View.VISIBLE);
					textOfferMall.setText("Click here to create the special offer related to Inorbit Mall");
					textOfferStore.setText("Click/browse here to create special offer related to stores");
				} else if (miManageGallery==1) {
					mImgHelp.setVisibility(View.GONE); 
					metSearchText.setVisibility(View.VISIBLE);
					textOfferMall.setText("Click here to manage gallery of Inorbit Mall");
					textOfferStore.setText("Click/browse here to manage gallery of the stores");
				} else if (miViewPost==1) {
					mImgHelp.setVisibility(View.GONE); 
					metSearchText.setVisibility(View.VISIBLE);
					textOfferMall.setText("Click here to view the offers posted by Inorbit Mall");
					textOfferStore.setText("Click/browse here to view the offers posted by the stores");
				} else {
					mImgHelp.setVisibility(View.GONE); 
					metSearchText.setVisibility(View.VISIBLE);
					textOfferMall.setText("Click here to edit the information of Inorbit Mall");
					textOfferStore.setText("Click/browse here to edit the information of the stores");
				}
			//}
		}
	}

	
	/**
	 * Get the user id of the merchant
	 * 
	 * @return String : user id
	 */
	String getUserId(){
		HashMap<String,String>user_details = mSessionMnger.getUserDetails();
		return user_details.get("user_id").toString();

	}

	/**
	 * Get the auth id of the merchant
	 * 
	 * @return String : Auth id
	 */
	String getAuthId(){
		HashMap<String,String>user_details = mSessionMnger.getUserDetails();
		return user_details.get("auth_id").toString();
	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		
		
		/**
		 * 
		 * If mall manager is logged and this activity has been called from "AddStore" / "EditStore" / "ManageGallery" / "ShakeAndWin"
		 * than show the HELP option on action bar
		 * 
		 */
		if(mdbUtil.isManager()) {
			if(miChoiceMode==1 ){
				try{
					MenuItem extra=menu.add("Help");
					extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		/**
		 * If user clicks opn Help option of action bar than show the help dialog
		 * 
		 */
		if(item.getTitle().toString().equalsIgnoreCase("Help")){
			startHelperActivity();
		}else{
			finishto();
		}

		//finishto();
		return true;
	}

	
	/**
	 * 
	 * Network request to get the stores from server assigned to this merchant
	 * 
	 */
	void loadAllStoreForUser(){

		mRelProgressLayout.setVisibility(ViewGroup.VISIBLE);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		headers.put("user_id", getUserId());
		headers.put("auth_id", getAuthId()); 
		InorbitLog.d("Merchnat user id "+getUserId());
		MyClass myClass = new MyClass(mContext);
		myClass.getStoreRequest(RequestTags.TagPlaceChooser, null, headers);
		
		new ValidateUser(mContext).validateMerchant(RequestTags.Tag_ValidateMerchant);
	}

	//Create New Post
	class CreateBroadCastListAdapter extends ArrayAdapter<CheckableStoreListModel> implements Filterable{
		
		Activity context;
		LayoutInflater inflate;
		ArrayList<CheckableStoreListModel> displayList;

		public CreateBroadCastListAdapter(Activity context, ArrayList<CheckableStoreListModel> checkableStores) {
			super(context, 0, checkableStores);
			this.context=context;
			inflate=context.getLayoutInflater();
			this.displayList = checkableStores;
		}
		
		@Override
		public int getCount() {
			return displayList.size();
		}


		@Override
		public long getItemId(int position) {
			return position;
		}

		/*StoreListModel getselectedposition(int position) {
			return ((StoreListModel) getItem(position));
		}*/

		/*ArrayList<StoreListModel> getcheckedposition() {
			ArrayList<StoreListModel> checkedposition = new ArrayList<StoreListModel>();
			for (StoreListModel p : ID) {
				if (p.isSelected())
					checkedposition.add(p);
			}
			return checkedposition;
		}*/
		
		/*OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getselectedposition((Integer) buttonView.getTag()).selected = isChecked;
			}
		};*/
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			
			if(convertView==null){
				final ViewHolder holder=new ViewHolder();
				if (miChoiceMode == 2) {
					convertView=inflate.inflate(R.layout.layout_selectstore,null);
					
					holder.txtTitle=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
					holder.txtMallName = (TextView) convertView.findViewById(R.id.txtMallName);
					holder.imgStoreLogo = (ImageView)convertView.findViewById(R.id.imgsearchLogo);
					holder.imgStoreLogo.setScaleType(ScaleType.CENTER_INSIDE);
					holder.placeCheckBox=(CheckBox)convertView.findViewById(R.id.placeCheckBox);
					holder.band=(TextView)convertView.findViewById(R.id.band);
					holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
					holder.rlPlaceChooser = (RelativeLayout)convertView.findViewById(R.id.relative_place_chooser);
					holder.placeCheckBox.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							CheckableStoreListModel element=(CheckableStoreListModel)holder.placeCheckBox.getTag();
							element.checked = !element.checked;
							for (int i=0; i<displayList.size(); i++) {
								if (displayList.get(i).storeInfo.getId().equalsIgnoreCase(element.storeInfo.getId())) {
									displayList.get(i).checked = element.checked;
									for (int j=0; j<allStores.size(); j++) {
										if (displayList.get(i).equals(allStores.get(j))) {
											InorbitLog.d("elem: "+element.storeInfo.getName()+" "+"disp: "+displayList.get(i).storeInfo.getName()+" "
													+"all: "+ allStores.get(j).storeInfo.getName());
											allStores.get(j).checked = element.checked;
										}
									}
									break;
								}
							}
						}
					});
					
					holder.txtTitle.setTextColor(Color.BLACK);
					holder.txtMallName.setTextColor(Color.BLACK);
					holder.txtSearchStoreDesc.setTextColor(Color.BLACK);
					
					holder.txtTitle.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
					holder.txtMallName.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
					holder.txtSearchStoreDesc.setTypeface(InorbitApp.getTypeFace());
					convertView.setTag(holder);
					holder.placeCheckBox.setTag(displayList.get(position));
				} else {
					convertView = inflate.inflate(R.layout.layout_place_chooser, null);
					
					holder.txtTitle=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
					holder.txtMallName = (TextView) convertView.findViewById(R.id.txtMallLocation);
					holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
					holder.imgStoreLogo.setScaleType(ScaleType.FIT_CENTER);
					holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
					holder.band=(TextView)convertView.findViewById(R.id.band);
					holder.rlPlaceChooser= (RelativeLayout) convertView.findViewById(R.id.searchFront); 
					
					holder.txtTitle.setTypeface(InorbitApp.getTypeFaceTitle());
					holder.txtSearchStoreDesc.setTypeface(InorbitApp.getTypeFace());
					holder.txtSearchStoreDesc.bringToFront();
					
					holder.imgStoreLogo.setScaleType(ScaleType.CENTER_INSIDE);
					holder.txtTitle.setTypeface(InorbitApp.getTypeFaceTitle());
					holder.txtMallName.setTypeface(InorbitApp.getTypeFaceTitle(),Typeface.BOLD);
					holder.txtSearchStoreDesc.setTypeface(InorbitApp.getTypeFaceTitle());
					holder.txtSearchStoreDesc.setTextSize(12);
					convertView.setTag(holder);
				}
			} else {
				if (miChoiceMode == 2)
					((ViewHolder)convertView.getTag()).placeCheckBox.setTag(displayList.get(position));
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();
			
			hold.band.setVisibility(View.GONE);
			
			if (miChoiceMode == 2) {
				hold.placeCheckBox.setChecked(displayList.get(position).checked);
			}
			
			String photo_source=displayList.get(position).storeInfo.getImage_url().toString().replaceAll(" ", "%20");
			try {
				Picasso.with(context).load(getResources().getString(R.string.photo_url)+photo_source)
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(hold.imgStoreLogo);

			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			} catch(Exception e){
				e.printStackTrace();
			}
			hold.txtTitle.setText(displayList.get(position).storeInfo.getName());
			hold.txtSearchStoreDesc.setText(displayList.get(position).storeInfo.getDescription());
			
			String msPlaceParent = displayList.get(position).storeInfo.getPlace_parent();
			if(msPlaceParent.equalsIgnoreCase("1")){
				hold.txtMallName.setText("Inorbit Malad");
			} else if(msPlaceParent.equalsIgnoreCase("2")){
				hold.txtMallName.setText("Inorbit Vashi");
			} else if(msPlaceParent.equalsIgnoreCase("3")){
				hold.txtMallName.setText("Inorbit Cyberabad");
			} else if(msPlaceParent.equalsIgnoreCase("4")){
				hold.txtMallName.setText("Inorbit Whitefield");
			} else if(msPlaceParent.equalsIgnoreCase("5")){
				hold.txtMallName.setText("Inorbit Pune");
			} else if(msPlaceParent.equalsIgnoreCase("6")){
				hold.txtMallName.setText("Inorbit Vadodara");
			} else {
				hold.txtMallName.setText("");
			}
			
			if(msPlaceParent.equalsIgnoreCase("0")){
				hold.rlPlaceChooser.setBackgroundColor(getResources().getColor(R.color.inorbit_orange));
			}else{
				hold.rlPlaceChooser.setBackgroundResource(R.drawable.list_item_back);
			}
			
			return convertView;
		}
		
		@Override
		public Filter getFilter() {
			// TODO Auto-generated method stub
			 return new Filter() {
		            @Override
		            protected FilterResults performFiltering(CharSequence charSequence) {
		                List<CheckableStoreListModel> filteredResult = getFilteredResults(charSequence.toString());

		                FilterResults results = new FilterResults();
		                results.values = filteredResult;
		                results.count = filteredResult.size();
		                
		                return results;
		            }
		            
		            @Override
		            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
		            	if (filterResults!= null && filterResults.count > 0) {
		            		displayList = (ArrayList<CheckableStoreListModel>) filterResults.values;
		            	} else {
		            		displayList = new ArrayList<CheckableStoreListModel>();
		            	}
		            	InorbitLog.d("null "+(filterResults==null));
		            	
		            	mAdapter=new CreateBroadCastListAdapter(mContext, displayList);
		        		mlvMultiPlaceChooserList.setAdapter(mAdapter);
		                mAdapter.notifyDataSetChanged();
		            }


		            private ArrayList<CheckableStoreListModel> getFilteredResults(String constraint){
		                if (constraint.length() == 0){
		                    return  allStores;
		                }
		                ArrayList<CheckableStoreListModel> listResult = new ArrayList<CheckableStoreListModel>();
		                for (CheckableStoreListModel obj : allStores) {
		                	InorbitLog.d(obj.storeInfo.getName()+" "+obj.checked);
		                    if (constraint.length()<=obj.storeInfo.getName().length() && obj.storeInfo.getName().toLowerCase().contains(constraint.toLowerCase())/*constraint.equalsIgnoreCase(obj.storeInfo.getName().subSequence(0, constraint.length()).toString())*/){
		                        listResult.add(obj);
		                    }
		                }
		                return listResult;
		            }
		        };
		}
	}
	
	static class ViewHolder{
		TextView txtTitle, txtBackOrange, txtMallName;
		ImageView imgStoreLogo;
		TextView txtStore;
		CheckBox placeCheckBox;
		TextView txtSearchStoreDesc;
		TextView band;
		RelativeLayout rlPlaceChooser;
	}
	
	private StoreListModel get(String placename,boolean selected) {
		return new StoreListModel(placename,selected);
	}
	private StoreListModel getID(String placename,String id,boolean selected) {
		StoreListModel sm=new StoreListModel(placename, selected);
		sm.setId(id);
		return  sm;
	}

	private StoreListModel getID(String placename,String id,String placedesc,boolean selected) {
		StoreListModel sm=new StoreListModel(placename,placedesc,selected);
		sm.setId(id);
		return  sm;
	}

	void showToast(String msg){
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}

	void finishto(){
		if(Tables.mbIsMerchantLoggedInNow) {
			Tables.mbIsMerchantLoggedInNow = false;
			Intent intent=new Intent(mContext, MerchantHome.class);
			startActivity(intent);	
		}
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishto();
	}


	
	/**
	 * 
	 * 
	 * 
	 * 
	 * @param ID
	 * @param storeName
	 * @param photoArr
	 * @param storeDescArr
	 * @param mallIdArr
	 * @param place_status
	 * @param place_parent
	 */
	public void setArraysForBroadCast(){

		if(!Tables.mbIsMerchantLoggedInNow && miChoiceMode==1) {
			animateThisView(mImgHelp, AnimationUtils.loadAnimation(mContext, R.anim.fade_in_splash));
		}
		
		/*final ArrayList<String> place_parents = new ArrayList<String>(), logos = new ArrayList<String>();*/

		/*for (int i=0;i<marrStoreInfos.size();i++) {
			Log.d("", "Inorbit store id >> "+marrStoreInfos.get(i).getId());
			list.add(get(marrStoreInfos.get(i).getName(), false));
			list_id.add(getID(marrStoreInfos.get(i).getName(), marrStoreInfos.get(i).getId(), false));
			list_id_desc.add(getID(marrStoreInfos.get(i).getName(), marrStoreInfos.get(i).getId(), marrStoreInfos.get(i).getDescription(),false));
			place_parents.add(marrStoreInfos.get(i).getPlace_parent());
			logos.add(marrStoreInfos.get(i).getImage_url());
		}*/
		
		mAdapter=new CreateBroadCastListAdapter(mContext, allStores);
		mlvMultiPlaceChooserList.setAdapter(mAdapter);
		
		if(miChoiceMode == 1){
			
			metSearchText.setOnEditorActionListener(new OnEditorActionListener() {

				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					// TODO Auto-generated method stub\

					if (actionId == EditorInfo.IME_ACTION_SEARCH) {
						
					}
					return false;
				}
			});

			metSearchText.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub

					try{
						/*final ArrayList<String> STOREID_SUB = new ArrayList<String>();
						final ArrayList<String> STORE_NAME_SUB = new ArrayList<String>();
						final ArrayList<String> PHOTO_SUB = new ArrayList<String>();
						final ArrayList<String> PLACE_STATUS_SUB = new ArrayList<String>();
						final ArrayList<String> PLACE_PARENT_SUB = new ArrayList<String>();
						final ArrayList<String> STORE_DESC_SUB = new ArrayList<String>();
						final ArrayList<String> MALL_ID_SUB = new ArrayList<String>();*/
						
						final ArrayList<CheckableStoreListModel> matchingStores = new ArrayList<CheckableStoreListModel>();
						miTextlength = metSearchText.getText().toString().length();
						
						for (int i = 0; i < allStores.size(); i++) {
							if (miTextlength <= allStores.get(i).storeInfo.getName().length()){
								if (allStores.get(i).storeInfo.getName().toLowerCase().contains(metSearchText.getText().toString().toLowerCase())) {
										/*equalsIgnoreCase((String) allStores.get(i).storeInfo.getName().subSequence(0, miTextlength)))*/
									matchingStores.add(allStores.get(i));
									InorbitLog.d(allStores.get(i).storeInfo.getName()+" "+allStores.get(i).checked);
								}
							}
						}
						
						//setArraysForBroadCast(STOREID_SUB, STORE_NAME_SUB, PHOTO_SUB, STORE_DESC_SUB, MALL_ID,PLACE_STATUS_SUB,PLACE_PARENT_SUB);
						mAdapter = new CreateBroadCastListAdapter(mContext, matchingStores);
						mlvMultiPlaceChooserList.setAdapter(mAdapter);
						
						/*mAStoreSearAdapter=new StoreSearchListAdapter(mContext, R.drawable.ic_launcher, R.drawable.ic_launcher, matchingStores);
						mlvSinglePlaceChooserList.setAdapter(mAStoreSearAdapter);
						mlvSinglePlaceChooserList*/
						mlvMultiPlaceChooserList.setOnItemClickListener(new OnItemClickListener() {
							
							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
								try {
									if (miChoiceMode == 1) {
										if(miViewPost==1)
										{
											//Intent intent=new Intent(context, ViewPost.class);
											Intent intent=new Intent(mContext, ViewPostNew.class);
											intent.putExtra("storeName", matchingStores.get(position).storeInfo.getName());
											intent.putExtra("STORE_ID", matchingStores.get(position).storeInfo.getId());
											startActivity(intent);
											overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
											//finish();
										}
										else if(miViewPost==2)
										{
											msSlected_store_id=matchingStores.get(position).storeInfo.getId();
											Intent intent=new Intent(mContext, EditStore.class);
											intent.putExtra("isSplitLogin",true);
											intent.putExtra("USER_ID", getUserId());
											intent.putExtra("AUTH_ID", getAuthId());
											intent.putExtra("STORE_NAME", matchingStores.get(position).storeInfo.getName());
											intent.putExtra("STORE_LOGO", matchingStores.get(position).storeInfo.getImage_url());
											intent.putExtra("STORE_ID", matchingStores.get(position).storeInfo.getId());
											intent.putExtra("PLACE_PARENT", matchingStores.get(position).storeInfo.getPlace_parent());
											intent.putExtra("manageGallery",miManageGallery);
											/*			intent.putExtra("business_id", BUSINESS_REC_ID);*/
											startActivity(intent);
											overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
											//finish();
										}else if(miSure_shop==1){
	
											if(misNewShakeWin) {
												Intent intent=new Intent(mContext,GateToMakeSpecialOffer.class);
												intent.putExtra("SureShop",1);
												intent.putExtra("STORE_NAME", matchingStores.get(position).storeInfo.getName());
												intent.putExtra("STORE_LOGO", matchingStores.get(position).storeInfo.getImage_url());
												intent.putExtra("STORE_ID", matchingStores.get(position).storeInfo.getId());
												intent.putExtra("MALL_ID", matchingStores.get(position).storeInfo.getMall_id());
												intent.putExtra("PLACE_PARENT", matchingStores.get(position).storeInfo.getPlace_parent());
												startActivity(intent);
												overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
												//finish();
											} else {
												Intent intent=new Intent(mContext,GateToTalk.class) ;
												intent.putExtra("SureShop",1);
												intent.putExtra("STORE_NAME", matchingStores.get(position).storeInfo.getName());
												intent.putExtra("STORE_LOGO", matchingStores.get(position).storeInfo.getImage_url());
												intent.putExtra("STORE_ID", matchingStores.get(position).storeInfo.getId());
												intent.putExtra("MALL_ID", matchingStores.get(position).storeInfo.getMall_id());
												startActivity(intent);
												overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
												//finish();
											}
										}
									}
								}catch(Exception ex){
									ex.printStackTrace();
								}

							}
						});

					}catch(Exception ex){
						ex.printStackTrace();
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
				}
			});
			
		} else {
			
			metSearchText.setVisibility(View.VISIBLE);
			metSearchText.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					
					/*ArrayList<CheckableStoreListModel> matchingStores = new ArrayList<PlaceChooser.CheckableStoreListModel>();
					miMultiple_txtlngth = metSearchText.getText().toString().trim().length();

					for (int i = 0; i < allStores.size(); i++){
						if (miMultiple_txtlngth <= allStores.get(i).storeInfo.getName().toString().length()) {
							if (metSearchText.getText().toString().equalsIgnoreCase((String) allStores.get(i).storeInfo.getName().subSequence(0, miMultiple_txtlngth))) {
								matchingStores.add(allStores.get(i));
							}
						}
					}*/
					if (mAdapter != null) {
	                    mAdapter.getFilter().filter(metSearchText.getText().toString());
	                }
/*					mAdapter=new CreateBroadCastListAdapter(mContext, matchingStores);
					mlvMultiPlaceChooserList.setAdapter(mAdapter);

					mbtnSelectStore.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							
							ArrayList<String> temp = new ArrayList<String>();
							for(int i=0;i<allStores.size();i++){
								if(allStores.get(i).checked) {
									temp.add(allStores.get(i).storeInfo.getId());
								}
							}
							
							if(temp.size()!=0){
								//Intent intent=new Intent(mContext, CreateBroadCast.class);
								Intent intent=new Intent(mContext, CreatBroadCastNew.class);
								intent.putStringArrayListExtra("SELECTED_STORE_ID",temp);
								startActivity(intent);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								finish();
							}else{
								showToast("Please select atleast one store");
							}
						}
					});*/
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
		}


		mbtnSelectStore.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				ArrayList<String> temp = new ArrayList<String>();
				for(int i=0;i<allStores.size();i++){
					if(allStores.get(i).checked) {
						temp.add(allStores.get(i).storeInfo.getId());
					}
				}
				
				if(temp.size()!=0){
					//Intent intent=new Intent(mContext, CreateBroadCast.class);
					Intent intent=new Intent(mContext, CreatBroadCastNew.class);
					intent.putStringArrayListExtra("SELECTED_STORE_ID",temp);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					finish();
				}else{
					showToast("Please select atleast one store");
				}
			}
		});


		if (miChoiceMode == 1) {
			mlvMultiPlaceChooserList.setOnItemClickListener(new OnItemClickListener() {
	
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					//Edit Store
					try{
						if(miViewPost==2){
							msSlected_store_id=allStores.get(position).storeInfo.getId();
							Intent intent=new Intent(mContext, EditStore.class);
							intent.putExtra("isSplitLogin",true);
							intent.putExtra("USER_ID", getUserId());
							intent.putExtra("AUTH_ID", getAuthId());
							intent.putExtra("STORE_NAME", allStores.get(position).storeInfo.getName());
							intent.putExtra("STORE_LOGO", allStores.get(position).storeInfo.getImage_url());
							intent.putExtra("STORE_ID", allStores.get(position).storeInfo.getId());
							intent.putExtra("PLACE_PARENT", allStores.get(position).storeInfo.getPlace_parent());
							intent.putExtra("manageGallery",miManageGallery);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							//finish();
						}
	
						//View Posts
						else if(miViewPost==1){
	
							Intent intent=new Intent(mContext, ViewPostNew.class);
							intent.putExtra("storeName", allStores.get(position).storeInfo.getName());
							intent.putExtra("STORE_ID", allStores.get(position).storeInfo.getId());
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							//finish();
						}else if(miSure_shop==1){
	
							if(misNewShakeWin) {
								Intent intent=new Intent(mContext,GateToMakeSpecialOffer.class);
								intent.putExtra("SureShop",1);
								intent.putExtra("STORE_NAME", allStores.get(position).storeInfo.getName());
								intent.putExtra("STORE_LOGO", allStores.get(position).storeInfo.getImage_url());
								intent.putExtra("STORE_ID", allStores.get(position).storeInfo.getId());
								intent.putExtra("MALL_ID", allStores.get(position).storeInfo.getMall_id());
								intent.putExtra("PLACE_PARENT", allStores.get(position).storeInfo.getPlace_parent());
								startActivity(intent);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								//finish();
	
							} else {
								Intent intent=new Intent(mContext,GateToTalk.class);
								intent.putExtra("SureShop",1);
								intent.putExtra("STORE_NAME", allStores.get(position).storeInfo.getName());
								intent.putExtra("STORE_LOGO", allStores.get(position).storeInfo.getImage_url());
								intent.putExtra("STORE_ID", allStores.get(position).storeInfo.getId());
								intent.putExtra("MALL_ID", allStores.get(position).storeInfo.getMall_id());
								startActivity(intent);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								//finish();
	
							}
						}
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			});
		}
		/*mlvMultiPlaceChooserList.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				try {
					if (miChoiceMode == 1) {
						if(miViewPost==1)
						{
							//Intent intent=new Intent(context, ViewPost.class);
							Intent intent=new Intent(mContext, ViewPostNew.class);
							intent.putExtra("storeName", matchingStores.get(position).storeInfo.getName());
							intent.putExtra("STORE_ID", matchingStores.get(position).storeInfo.getId());
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							//finish();
						}
						else if(miViewPost==2)
						{
							msSlected_store_id=matchingStores.get(position).storeInfo.getId();
							Intent intent=new Intent(mContext, EditStore.class);
							intent.putExtra("isSplitLogin",true);
							intent.putExtra("USER_ID", getUserId());
							intent.putExtra("AUTH_ID", getAuthId());
							intent.putExtra("STORE_NAME", matchingStores.get(position).storeInfo.getName());
							intent.putExtra("STORE_LOGO", matchingStores.get(position).storeInfo.getImage_url());
							intent.putExtra("STORE_ID", matchingStores.get(position).storeInfo.getId());
							intent.putExtra("PLACE_PARENT", matchingStores.get(position).storeInfo.getPlace_parent());
							intent.putExtra("manageGallery",miManageGallery);
										intent.putExtra("business_id", BUSINESS_REC_ID);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							//finish();
						}else if(miSure_shop==1){

							if(misNewShakeWin) {
								Intent intent=new Intent(mContext,GateToMakeSpecialOffer.class);
								intent.putExtra("SureShop",1);
								intent.putExtra("STORE_NAME", matchingStores.get(position).storeInfo.getName());
								intent.putExtra("STORE_LOGO", matchingStores.get(position).storeInfo.getImage_url());
								intent.putExtra("STORE_ID", matchingStores.get(position).storeInfo.getId());
								intent.putExtra("MALL_ID", matchingStores.get(position).storeInfo.getMall_id());
								intent.putExtra("PLACE_PARENT", matchingStores.get(position).storeInfo.getPlace_parent());
								startActivity(intent);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								//finish();
							}else {
								Intent intent=new Intent(mContext,GateToTalk.class) ;
								intent.putExtra("SureShop",1);
								intent.putExtra("STORE_NAME", matchingStores.get(position).storeInfo.getName());
								intent.putExtra("STORE_LOGO", matchingStores.get(position).storeInfo.getImage_url());
								intent.putExtra("STORE_ID", matchingStores.get(position).storeInfo.getId());
								intent.putExtra("MALL_ID", matchingStores.get(position).storeInfo.getMall_id());
								startActivity(intent);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								//finish();
							}
						}
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
		});*/
	}

	
	/**
	 * 
	 * Broadcast receiver for loadAllStoreForUser()
	 * 
	 * @author Nitin
	 *
	 */
	class PlaceChooserBroadcastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(final Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{

				mRelProgressLayout.setVisibility(ViewGroup.GONE);
				Bundle bundle = intent.getExtras();
				if(intent!=null){
					String SEARCH_STATUS = intent.getStringExtra("SUCCESS");

					if(SEARCH_STATUS.equalsIgnoreCase("true")){

						ArrayList<ParticularStoreInfo> particularStoreInfoArr = intent.getParcelableArrayListExtra("ParticularStoreInfoDetails");
						
						/*final ArrayList<String> STOREID		= new ArrayList<String>();
						final ArrayList<String> STORE_NAME	= new ArrayList<String>();
						final ArrayList<String>PHOTO		= new ArrayList<String>();
						final ArrayList<String> STORE_DESC	= new ArrayList<String>();
						final ArrayList<String> PLACE_PARENT = new ArrayList<String>();
						final ArrayList<String> MALL_ID		= new ArrayList<String>();
						final ArrayList<String> PLACE_STATUS= new ArrayList<String>();*/


						/*for(int i=0;i<particularStoreInfoArr.size();i++){
							
							STOREID.add(particularStoreInfoArr.get(i).getId());
							STORE_NAME.add(particularStoreInfoArr.get(i).getName());
							PHOTO.add(particularStoreInfoArr.get(i).getImage_url());
							STORE_DESC.add(particularStoreInfoArr.get(i).getDescription());
							MALL_ID.add(particularStoreInfoArr.get(i).getMall_id());
							PLACE_STATUS.add(particularStoreInfoArr.get(i).getPlace_status());
							PLACE_PARENT.add(particularStoreInfoArr.get(i).getPlace_parent());
							InorbitLog.d(particularStoreInfoArr.get(i).getName()+" Place Parent "+particularStoreInfoArr.get(i).getPlace_parent());

							mArrSTORE_IDS = STOREID;

						}*/
						
						
						
						/**
						 * 
						 * Tag the merchant and maintain the check of mall manager logged in
						 * 
						 */
						boolean isManagerLoggedIn = mdbUtil.isManager();
						if(particularStoreInfoArr.size()!=0){
							if(isManagerLoggedIn){
								ConfigFile.setManagerLoogedIn(true);
								mSessionMnger.setMallManagerLoggedin(true);
							}else{
								//tagMerchant();
								ConfigFile.setManagerLoogedIn(false);
								mSessionMnger.setMallManagerLoggedin(false);
							}
						}else{
							ConfigFile.setManagerLoogedIn(false);
							mSessionMnger.setMallManagerLoggedin(false);
						}

						//setArraysForBroadCast(STOREID, STORE_NAME, PHOTO, STORE_DESC, MALL_ID,PLACE_STATUS,PLACE_PARENT);
						
						for (ParticularStoreInfo p: particularStoreInfoArr) {
							allStores.add(new CheckableStoreListModel(p, false));
						}
					} else {

						String msg=intent.getStringExtra("MESSAGE");
						showToast(msg);
						/*if(msg.startsWith("No records found.")){
							if(getResources().getBoolean(R.bool.isAppShopLocal)){
								Handler handler = new Handler(); 
								handler.postDelayed(new Runnable() { 
									public void run() { 
										Intent intent=new Intent(context, EditStore.class);
										intent.putExtra("isNew", true);
										startActivity(intent);
										overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
										finish();
									} 
								}, 500); 

							}
						}*/
						if(msg.startsWith(getResources().getString(R.string.invalidAuth))) {
							mSessionMnger.logoutUser();
							Intent intent1=new Intent(context, MerchantLogin.class);
							startActivity(intent1);
							PlaceChooser.this.finish();
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}

						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){


							int code1 = intent.getIntExtra("CODE",0);
							String message1=intent.getStringExtra("MESSAGE");
							String errorMessage = intent.getStringExtra("errorMessage");
							String apiUrl = intent.getStringExtra("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "PlaceChooser");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}
					}
				}else{
					MyToast.showToast(PlaceChooser.this, "");
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	/*void tagMerchant(){
		InorbitLog.d("Taging Merchnat >>>>>>>>>>>>>>>>>>>>>>>>>>>");
		boolean isMisMatch = false;
		Set<String> tags = new HashSet<String>(); 
		tags=PushManager.shared().getTags();

		ArrayList<String>tempIdList=new ArrayList<String>();

		String temp="";
		String replace="";
		if(tags.size()!=0){
			//Getting All Tags
			Iterator<String> iterator=tags.iterator();
			//Traversing Tags to find out if there are any tags existing with own_id
			ArrayList<String> mArrSTORE_IDS = new ArrayList<String>();
			
			for (CheckableStoreListModel str : allStores) {
				mArrSTORE_IDS.add(str.storeInfo.getId());
			}
			
			while(iterator.hasNext()){
				temp=iterator.next();
				InorbitLog.d("TAG " + "Urban TAG PRINT "+temp);
				//If tag contains own_
				if(temp.startsWith("own_")){	
					replace=temp.replaceAll("own_", "");
					tempIdList.add(replace);
					if(mArrSTORE_IDS.contains(replace)==false){
						isMisMatch=true;
						break;
					}
				}
			}

			if(isMisMatch==false)
			{

				for(int i=0;i<mArrSTORE_IDS.size();i++)
				{
					if(tempIdList.contains(mArrSTORE_IDS.get(i))==false)
					{
						isMisMatch=true;
						break;
					}
				}
			}


			InorbitLog.d("TAG ID" + "UAIR ID "+mArrSTORE_IDS.toString());
			InorbitLog.d("TAG ID" + "UAIR ID TAG "+tempIdList.toString()+" "+isMisMatch);

			if(isMisMatch==true){


				for(int i=0;i<tempIdList.size();i++){
					tags.remove("own_"+tempIdList.get(i));
				}

				//Add all ID's of Merchant Places to Tag list.
				for(int i=0;i<mArrSTORE_IDS.size();i++){
					tags.add("own_"+mArrSTORE_IDS.get(i));
				}

				//Log.i("TAG", "Urban TAG AFTER "+tags.toString());
				InorbitLog.d("TAG ID" + "Urban TAG AFTER "+tags.toString());

				//Set Tags to Urban Airship
				PushManager.shared().setTags(tags);
			}



		}
	}*/


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			IntentFilter filter = new IntentFilter(RequestTags.TagPlaceChooser);
			mContext.registerReceiver(mbPlaceChooserBroadcast = new PlaceChooserBroadcastReceiver(), filter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		try{
			if(mbPlaceChooserBroadcast!=null){
				mContext.unregisterReceiver(mbPlaceChooserBroadcast);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	private void animateThisView(View view,Animation mAnim){
		Animation 		manimFadein;
		manimFadein 	= mAnim;
		view.startAnimation(manimFadein);
		//manimFadein.setFillAfter(true);
		manimFadein.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				try {
					//mImgHelp.setVisibility(View.VISIBLE);
				} catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
	}

	
	
	/**
	 * 
	 * Help Activity as dialog
	 * 
	 */
	private void startHelperActivity(){
		ArrayList<Integer> miarrImages = new ArrayList<Integer>(); 
		miarrImages.add(R.drawable.mall_manager_mall);
		miarrImages.add(R.drawable.mall_manager_stores);
		ArrayList<String> msarrHelperText = new ArrayList<String>(); 

		if(miChoiceMode==2) {

		} else {
			if(miSure_shop==1){

				msarrHelperText.add("Select 'Inorbit Mall' to create the special offer related to Mall");
				msarrHelperText.add("Select/browse here to create special offer related to stores");

			} else if (miManageGallery==1){

				msarrHelperText.add("Select 'Inorbit Mall' to manage gallery of Mall");
				msarrHelperText.add("Select/browse to manage gallery of the stores");
				
			} else if (miViewPost==1) {

				msarrHelperText.add("Select 'Inorbit Mall' to view the offers posted by Mall");
				msarrHelperText.add("Select/browse to view the offers posted by the stores");

			} else {

				msarrHelperText.add("Select 'Inorbit Mall' to edit the information of Mall");
				msarrHelperText.add("Select/browse to edit the information of the stores");
			}
		}


		/*	msarrHelperText.add("Select Inorbit Mall");
		msarrHelperText.add("Select any store");*/
		Intent intent = new Intent(mContext, AppHelperPager.class);
		intent.putIntegerArrayListExtra("Helper_Images_Arr", miarrImages);
		intent.putStringArrayListExtra("Helper_Text", msarrHelperText);
		intent.putExtra("Parent_Back", R.drawable.merchant_stores);
		intent.putExtra("Title", mActionBar.getTitle().toString());
		startActivity(intent);
		overridePendingTransition(0, 0);
	}
	
	private class CheckableStoreListModel {
		private ParticularStoreInfo storeInfo;
		private boolean checked;
		
		public CheckableStoreListModel(ParticularStoreInfo storeInfo, boolean checked) {
			this.storeInfo = storeInfo;
			this.checked = checked;
		}
		
		@Override
		public boolean equals(Object o) {
			if (this.storeInfo.getId().equalsIgnoreCase(((CheckableStoreListModel)o).storeInfo.getId()))
				return true;
			return false;
		}
	}
}
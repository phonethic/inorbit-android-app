package com.phonethics.inorbit;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.SplashScreen.UdmIdMerchant;
import com.phonethics.model.ParticularStoreInfo;
import com.phonethics.model.RequestTags;
/*import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;*/
/*import com.urbanairship.push.PushManager;*/

public class MerchantLogin extends SherlockActivity implements OnClickListener{

	ActionBar actionBar;
	Activity context;
	Button btnLoginMerchant,btnSingupMerchant;
	MerchantLoginBroadcastReceiver merchantReceiver;
	//Login Dialog
	Dialog dialog;
	EditText mobileno,password;
	ProgressBar progLogin;
	SessionManager session;
	//User Name
	public final String KEY_USER_NAME="mobileno";
	//Password
	public final String KEY_PASSWORD="password";
	//Login back
	ImageView imgLoginBack;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;
	ImageView img_place_holder;
	RelativeLayout merchantLogin;
	TextView forgotPassword;
	TextView signUpText;
	TextView txtMerchantPlaceHolder;
	RelativeLayout infoText;

	Typeface tf;
	DecelerateInterpolator sDecelerator = new DecelerateInterpolator();
	OvershootInterpolator sOvershooter = new OvershootInterpolator(10f);
	RelativeLayout signUpMerchantLayout;
	PlaceChooserBroadcastReceiver mbPlaceChooserBroadcast;
	public ArrayList<String> mArrSTORE_IDS;

	UdmIdMerchant mUdmIdMerchant;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_split_login_sign_up);



		/**
		 * Initialize the actionbar and class variables
		 *
		 */

		//Creating Action Bar
		actionBar=getSupportActionBar();
		actionBar.setTitle("Merchant Login");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		context=this;
		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");

		dialog=new Dialog(context);
		dialog.setContentView(R.layout.logindialoge);
		dialog.setTitle("Login");
		infoText=(RelativeLayout)findViewById(R.id.infoText);
		infoText.setVisibility(View.GONE);
		imageLoader=ImageLoader.getInstance();

		/*if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitLocalCache");
		}
		else{
			cacheDir=context.getCacheDir();
		}



		config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)
		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();*/

		session = new SessionManager(getApplicationContext()); 
		signUpMerchantLayout=(RelativeLayout)findViewById(R.id.signUpMerchantLayout);
		signUpMerchantLayout.setVisibility(View.GONE);

		merchantLogin=(RelativeLayout)findViewById(R.id.merchantLogin);
		merchantLogin.bringToFront();

		btnLoginMerchant=(Button)findViewById(R.id.btnLoginMerchant);
		btnSingupMerchant=(Button)findViewById(R.id.btnSingupMerchant);
		forgotPassword=(TextView)findViewById(R.id.forgotPassword);
		signUpText=(TextView)findViewById(R.id.signUpText);

		txtMerchantPlaceHolder=(TextView)findViewById(R.id.txtMerchantPlaceHolder);
		img_place_holder = (ImageView) findViewById(R.id.placeHolder);
		img_place_holder.setVisibility(View.INVISIBLE);

		signUpText.setTypeface(tf);
		forgotPassword.setTypeface(tf);
		txtMerchantPlaceHolder.setTypeface(tf);

		forgotPassword.setTextColor(Color.BLACK);
		signUpText.setTextColor(Color.parseColor("#c4c4c4"));

		mobileno=(EditText)findViewById(R.id.edtDialogMobileNo);
		password=(EditText)findViewById(R.id.edtDialogPassword);
		progLogin=(ProgressBar)findViewById(R.id.progLogin);
		mobileno.setTextColor(Color.BLACK);
		password.setTextColor(Color.BLACK);

		mobileno.setTypeface(tf);
		password.setTypeface(tf);

		txtMerchantPlaceHolder.setTextColor(Color.parseColor("#715b57"));

		if(!getResources().getBoolean(R.bool.isAppShopLocal))
		{
			btnSingupMerchant.setVisibility(View.VISIBLE);
			forgotPassword.setVisibility(View.VISIBLE);
		}


		animate(btnLoginMerchant).setDuration(200);
		btnLoginMerchant.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) { 
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(btnLoginMerchant).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(btnLoginMerchant).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});

		animate(btnSingupMerchant).setDuration(200);
		btnSingupMerchant.setOnTouchListener(new OnTouchListener() {


			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(btnSingupMerchant).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(btnSingupMerchant).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});

		animate(forgotPassword).setDuration(200);
		forgotPassword.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub

				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(btnSingupMerchant).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(btnSingupMerchant).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}


				return false;
			}
		});


		//Adding Click Listeners
		btnLoginMerchant.setOnClickListener(this);
		btnSingupMerchant.setOnClickListener(this);
		forgotPassword.setOnClickListener(this);

		imgLoginBack=(ImageView)findViewById(R.id.imgloginBack);


		HashMap<String, String> user_details=new HashMap<String, String>();
		user_details=session.getUserDetails();
		if(!user_details.get(KEY_USER_NAME).toString().equalsIgnoreCase("not-found")){
			if(user_details.get(KEY_USER_NAME).toString().toString().startsWith("91")){
				mobileno.setText(user_details.get(KEY_USER_NAME).toString().substring(2));
			}

		}

	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		finishTo();
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==btnLoginMerchant.getId()){

			if(mobileno.getText().toString().length()==0){
				Toast.makeText(context, "Please enter Mobile no.", Toast.LENGTH_SHORT).show();
			}else if(mobileno.getText().toString().length()<10){
				Toast.makeText(context, "Please enter correct Mobile no.", Toast.LENGTH_SHORT).show();
			}else if(password.getText().toString().length()==0){
				Toast.makeText(context, "Please enter password", Toast.LENGTH_SHORT).show();
			}else if(password.getText().toString().length()<6){
				Toast.makeText(context, "Password must be atleast 6 character's long", Toast.LENGTH_SHORT).show();
			}else{
				if(NetworkCheck.isNetConnected(context)){
					if(!progLogin.isShown()){
						hideKeyBoard();

						loginMerchant();
					}else{
						MyToast.showToast(context, "Please wait while loging in", 2);
					}
				}else{
					Toast.makeText(context, "No internet connection.", Toast.LENGTH_SHORT).show();
				}
			}

		}

		if(v.getId()==btnSingupMerchant.getId()){

			EventTracker.logEvent(getResources().getString(R.string.mSignup_SignUp), false);
			//Intent intent=new Intent(this, ShopLocalMain.class);
			Intent intent=new Intent(this, InorbitMerchantSignUp.class);
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


		}
		if(v.getId()==forgotPassword.getId()){

			EventTracker.logEvent(getResources().getString(R.string.mSignup_ForgotPassword), false);
			//Intent intent=new Intent(this, ShopLocalMain.class);
			Intent intent=new Intent(this, InorbitMerchantSignUp.class);
			intent.putExtra("ForgetPassword", 1);
			startActivity(intent);
			//finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
	}

	/**
	 * 
	 * Network request to login this merchant
	 * 
	 */

	void loginMerchant(){

		EventTracker.logEvent(getResources().getString(R.string.mSignup_LoginOption), false);

		String mobile_no = "91"+mobileno.getText().toString();
		progLogin.setVisibility(View.VISIBLE);

		JSONObject json = new JSONObject();
		try {
			json.put("mobile", mobile_no);
			json.put("password", password.getText().toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		MyClass myClass = new MyClass(context);
		myClass.postRequest(RequestTags.TagLoginMerchant, headers, json);
	}



	void hideKeyBoard(){
		//EditText myEditText = (EditText) findViewById(R.id.myEditText);  
		InputMethodManager imm = (InputMethodManager)getSystemService( Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(password.getWindowToken(), 0);
	}




	/**
	 * 
	 * Broadcast receiver to login this merchant
	 * 
	 * @author Nitin
	 *
	 */
	private class MerchantLoginBroadcastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try{
				progLogin.setVisibility(View.GONE);
				Bundle bundle = intent.getExtras();
				String success   = bundle.getString("SUCCESS");
				String message   = bundle.getString("MESSAGE");

				if(success.equalsIgnoreCase("true")){
					EventTracker.logEvent(getResources().getString(R.string.mSignup_InorbitLoginSuccess), false);

					String id = bundle.getString("ID");
					String auth = bundle.getString("AUTH_ID");
					String is_admin = bundle.getString("IS_ADMIN");

					String mUserId	=	id;
					String mAuthId	=	auth;

					progLogin.setVisibility(View.INVISIBLE);
					session.createLoginSession("91"+ mobileno.getText().toString(), password.getText().toString(), mUserId, mAuthId);

					/*try {
						Set<String> tags = new HashSet<String>();
						tags=PushManager.shared().getTags();
						tags.add("merchant");
						PushManager.shared().setTags(tags);
					}catch(Exception ex){
						ex.printStackTrace();
					}*/
					
					Tables.mbIsMerchantLoggedInNow = true;
					
					/**
					 * Load the assigned stores to this merchant
					 * 
					 */
					loadAllStoreForThisMerchant();

					//To get UDM_ID while customer login
					if(is_admin.equalsIgnoreCase("1")){
						//set is_amin = 2 for mall manager
						callToGetUdmIdForMerchant(mUserId, "2");
					}
					else if(is_admin.equalsIgnoreCase("0")){
						//set is_admin = 1 for store manager
						callToGetUdmIdForMerchant(mUserId, "1");
					}

				}else{
					EventTracker.logEvent(getResources().getString(R.string.mSignup_InorbitLoginFailed), false);
					MyToast.showToast((Activity) context, message);

					boolean isVolleyError = bundle.getBoolean("volleyError",false);
					if(isVolleyError){
						int code1 = bundle.getInt("CODE",0);
						String message1=bundle.getString("MESSAGE");
						String errorMessage = bundle.getString("errorMessage");
						String apiUrl = bundle.getString("apiUrl");	
						if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
							EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "LoginMerchant");
						}

						Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
					}


				}
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}

	} 


	/**
	 * Network request to load the stores 
	 * 
	 */
	void loadAllStoreForThisMerchant(){

		progLogin.setVisibility(View.VISIBLE);

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		headers.put("user_id", getUserId());
		headers.put("auth_id", getAuthId()); 

		InorbitLog.d("Merchnat user id "+getUserId());

		MyClass myClass = new MyClass(context);
		myClass.getStoreRequest(RequestTags.TagPlaceChooser, null, headers);

	}

	String getUserId(){
		HashMap<String,String>user_details = session.getUserDetails();
		return user_details.get("user_id").toString();

	}

	String getAuthId(){
		HashMap<String,String>user_details = session.getUserDetails();
		return user_details.get("auth_id").toString();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}

	void finishTo(){
		Intent intent=new Intent(context, HomeGrid.class);
		startActivity(intent);
		this.finish();

		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}


	/**
	 * 
	 * Broadcast receiver for loadAllStoreForUser().
	 * 
	 */
	class PlaceChooserBroadcastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(final Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{

				//mRelProgressLayout.setVisibility(ViewGroup.GONE);
				progLogin.setVisibility(ViewGroup.GONE);
				Bundle bundle = intent.getExtras();
				if(intent!=null){
					String SEARCH_STATUS = intent.getStringExtra("SUCCESS");
					
					if(SEARCH_STATUS.equalsIgnoreCase("true")){

						mArrSTORE_IDS = new ArrayList<String>();
						
						ArrayList<ParticularStoreInfo> particularStoreInfoArr = intent.getParcelableArrayListExtra("ParticularStoreInfoDetails");
						//showToast("Stores count "+particularStoreInfoArr.size()+" ");
						for(int i=0;i<particularStoreInfoArr.size();i++) {
							mArrSTORE_IDS.add(particularStoreInfoArr.get(i).getId());
						}
						
						DBUtil mdbUtil = new DBUtil(context);
						
						boolean isManagerLoggedIn = mdbUtil.isManager();
						//showToast("isManager Logged In: "+isManagerLoggedIn);
						
						if(particularStoreInfoArr.size()!=0){
							if(isManagerLoggedIn){
								ConfigFile.setManagerLoogedIn(true);
								session.setMallManagerLoggedin(true);
							} else {
								//tagMerchant();
								ConfigFile.setManagerLoogedIn(false);
								session.setMallManagerLoggedin(false);
							}
						} else {
							ConfigFile.setManagerLoogedIn(false);
							session.setMallManagerLoggedin(false);
						}
						
						Handler handler = new Handler(); 
						handler.postDelayed(new Runnable() { 
							public void run() { 
								Intent intent=new Intent(context, MerchantHome.class);
								startActivity(intent);	
								finish();
							} 
						}, 500);

					}else{

						String msg=intent.getStringExtra("MESSAGE");
						Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){
							int code1 = intent.getIntExtra("CODE",0);
							String message1=intent.getStringExtra("MESSAGE");
							String errorMessage = intent.getStringExtra("errorMessage");
							String apiUrl = intent.getStringExtra("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "PlaceChooser");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}
					}
				}else{
					
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	/**
	 * 
	 * Tag this merchant on urbanairship
	 * 
	 */
	/*void tagMerchant(){

		InorbitLog.d("Taging Merchnat >>>>>>>>>>>>>>>>>>>>>>>>>>>");
		boolean isMisMatch = false;
		Set<String> tags = new HashSet<String>(); 
		tags=PushManager.shared().getTags();

		ArrayList<String>tempIdList=new ArrayList<String>();

		String temp="";
		String replace="";
		if(tags.size()!=0){
			//Getting All Tags
			Iterator<String> iterator=tags.iterator();
			//Traversing Tags to find out if there are any tags existing with own_id
			while(iterator.hasNext()){
				temp=iterator.next();
				InorbitLog.d("TAG " + "Urban TAG PRINT "+temp);
				//If tag contains own_
				if(temp.startsWith("own_")){	
					replace=temp.replaceAll("own_", "");
					tempIdList.add(replace);
					if(mArrSTORE_IDS.contains(replace)==false){
						isMisMatch=true;
						break;
					}
				}
			}

			if(isMisMatch==false)
			{

				for(int i=0;i<mArrSTORE_IDS.size();i++)
				{
					if(tempIdList.contains(mArrSTORE_IDS.get(i))==false)
					{
						isMisMatch=true;
						break;
					}
				}
			}

			if(isMisMatch==true){


				for(int i=0;i<tempIdList.size();i++){
					tags.remove("own_"+tempIdList.get(i));
				}

				//Add all ID's of Merchant Places to Tag list.
				for(int i=0;i<mArrSTORE_IDS.size();i++){
					tags.add("own_"+mArrSTORE_IDS.get(i));
				}

				//Log.i("TAG", "Urban TAG AFTER "+tags.toString());
				InorbitLog.d("TAG ID" + "Urban TAG AFTER "+tags.toString());

				//Set Tags to Urban Airship
				PushManager.shared().setTags(tags);
			}
		}
	}*/

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{

			EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter login_filter = new IntentFilter(RequestTags.TagLoginMerchant);
			IntentFilter store_filter = new IntentFilter(RequestTags.TagPlaceChooser);
			
			context.registerReceiver(mbPlaceChooserBroadcast = new PlaceChooserBroadcastReceiver(), store_filter);
			
			merchantReceiver = new MerchantLoginBroadcastReceiver();
			context.registerReceiver(merchantReceiver, login_filter);

			//for udmId merchant
			IntentFilter mMerchant	= new IntentFilter(RequestTags.TAG_GET_UDM_ID_MERCHANT);
			context.registerReceiver(mUdmIdMerchant = new UdmIdMerchant(), mMerchant);


		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
		try{

			if(merchantReceiver!=null){
				context.unregisterReceiver(merchantReceiver);
			}
			if(mUdmIdMerchant!=null){
				context.unregisterReceiver(mUdmIdMerchant);
			}
			
			if (mbPlaceChooserBroadcast != null) {
				context.unregisterReceiver(mbPlaceChooserBroadcast);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}
	
	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}


	/* on start */
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	//method to get UDM_ID for Merchant
	private void callToGetUdmIdForMerchant(String user_id, String isAdmin) {
		// TODO Auto-generated method stub
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		JSONObject jsonObj = new JSONObject();
		try {
			String regid = session.getRegistrationId();
			jsonObj.put("device_id", regid);
			jsonObj.put("device_os", "Android");
			jsonObj.put("user_id", user_id);
			jsonObj.put("is_admin", isAdmin);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		InorbitLog.d("JSON " + jsonObj.toString());

		MyClass myclass = new MyClass(context);
		myclass.postRequest(RequestTags.TAG_GET_UDM_ID_MERCHANT, headers, jsonObj);
	}

	class UdmIdMerchant extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						String udm_id_merchant = intent.getStringExtra("UDMID");
						InorbitLog.d("ID "+ udm_id_merchant);
						session.setUdmIdForMerchant(udm_id_merchant);
					}
					else{
						String message = intent.getStringExtra("MESSAGE");
						showToast(message);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	void showToast(String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}
}

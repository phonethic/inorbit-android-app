package com.phonethics.inorbit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.wheel.ArrayWheelAdapter;
import com.phonethics.wheel.OnWheelChangedListener;
import com.phonethics.wheel.OnWheelScrollListener;
import com.phonethics.wheel.WheelView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ParkAssist extends SherlockFragmentActivity implements OnWheelChangedListener,OnWheelScrollListener {



	Button 				prk, direction	;
	double 				latitude,longitude, saved_lati, saved_longi;
	LatLng 				latLng;
	public static final String SHARED_PREFERENCES_NAME = "save_lati_longi";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	SharedPreferences 	prefs;
	Editor 				editor ;
	Context 			con;
	Marker 				marker, marker1;
	String 				address;

	String 				provider;
	Map<String, String> articleParams;

	ImageView			imageView;
	String 				 path_new;
	Bitmap 				thumbnail, galleryThumbnail,bm;
	EditText			edit_data;
	WheelView			wheelFloors;
	ActionBar 			actionbar;
	Context				context;
	LocationManager 	locationManager;
	Location 			location;
	Button				bttn;
	boolean				gpsTurnedOn =false;
	Activity			actContext;
	TextView			textAcc,textAlreadyParked;
	View				viewBack;
	AlertDialog alertDialog3;
	Dialog dialogAccuracy;
	boolean callFromAccuracyDialog = false;

	String floors[] = new String[] {"0",
			"-1","-2","-3"};

	Button cameraBtn,cancelImg;

	File mFileTemp;

	Uri mImagePagerCaptureUri;
	private LocationListener locationListener;
	private NetworkCheck netCheck;
	private int currentFloor = 0;

	RelativeLayout parkTextHintLaout;
	boolean fromFindCar = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_park_assist);



		context  = this;
		actContext	= this;
		actionbar=getSupportActionBar();
		actionbar.setTitle(getString(R.string.actionBarTitle));
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();

		path_new = Environment.getExternalStorageDirectory() + "/CarPark.jpg";

		Log.d("PATHOFIMAGE","PATHOFIMAGE " + path_new);

		mFileTemp = new File(path_new);
		mImagePagerCaptureUri = Uri.fromFile(mFileTemp);


		prk 			= (Button) findViewById(R.id.park_button);
		direction 		= (Button) findViewById(R.id.getDirection);
		//bttn			= (Button) findViewById(R.id.bttn_setreminder);
		//bttn.setVisibility(View.GONE);
		//direction.setClickable(false);
		//direction.setTextColor(Color.WHITE);

		imageView		= (ImageView) findViewById(R.id.img_capture);
		edit_data		= (EditText) findViewById(R.id.edit_text);
		wheelFloors		= (WheelView) findViewById(R.id.wheel_floor);
		textAlreadyParked = (TextView) findViewById(R.id.txt_already_parked);
		viewBack = (View) findViewById(R.id.view_back);
		cameraBtn = (Button) findViewById(R.id.cameraBtn);
		cancelImg = (Button) findViewById(R.id.cancelImage);

		wheelFloors.setViewAdapter(new FloorsAdapter(this, floors, 3));
		//wheelFloors.setCurrentItem(3);
		wheelFloors.addChangingListener(this);
		wheelFloors.addScrollingListener(this);

		parkTextHintLaout = (RelativeLayout) findViewById(R.id.parkTextHintLaout);


		netCheck = new NetworkCheck(actContext);



		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);



		location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);


		getPref();
		wheelFloors.setCurrentItem(currentFloor);

		if(saved_lati == 0 ){
			//direction.setClickable(false);
		}else{
			//direction.setClickable(true);
		}
		if(path_new.equalsIgnoreCase("")){

			Log.d("PATHOFIMAGE","PATHOFIMAGE IF" + path_new);
			showImage(path_new);


		}else{

			Log.d("PATHOFIMAGE","PATHOFIMAGE ELSE" + path_new);
			showImage(path_new);
		}

		/*		Location location = locationManager.getLastKnownLocation(provider);*/

		Log.i("Location ", "Location service started");


		locationListener = new LocationListener() {

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub

				latitude = location.getLatitude();
				longitude = location.getLongitude();

				Log.d("Latitude ","Latitude " + latitude);
			}
		};

		if (netCheck !=null && netCheck.isNetworkAvailable()) {
			if(location!=null){
				Log.i("Location ", "Location not null");
				locationListener.onLocationChanged(location);
			}else{
				Log.i("Location ", "Location null");
			}
		}else{
			Log.i("Location ", "Location netwrork null");
		}

		imageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(mFileTemp.length()!=0){

					try {


						Intent intent = new Intent(context, ParkCarImage.class);
						//mImagePagerCaptureUri = Uri.fromFile(mFileTemp);
						//intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImagePagerCaptureUri);
						intent.putExtra("URI", mImagePagerCaptureUri.toString());
						intent.putExtra("PATH", path_new);
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					} catch (Exception e) {
						// TODO: handle exception

						e.printStackTrace();
					}

				}
				else{

					Toast.makeText(context, "No image is selected", 0).show();
				}


			}

		});


		cameraBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub.
				//Toast.makeText(context, "clicked", 0).show();
				EventTracker.logEvent(getResources().getString(R.string.parkAssist_AddPhoto), false);
				Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				mImagePagerCaptureUri = Uri.fromFile(mFileTemp);
				takePictureIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImagePagerCaptureUri);
				startActivityForResult(takePictureIntent, 2);	
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}

		});


		prk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fromFindCar = false;

				if(netCheck.isNetworkAvailable()){

					if(isGpsEnable()){
						LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
						location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

						if(location!=null){
							locationListener.onLocationChanged(location);
						}
						try{
							if(location==null){
								Log.d("", "Inorbit GPS location null");
								Criteria criteria1 = new Criteria();
								//provider = locationManager.getBestProvider(criteria1, false);
								//location = locationManager.getLastKnownLocation(provider);

								//provider=LocationManager.NETWORK_PROVIDER;
								location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
								
							}
						}catch(NullPointerException npx){
							npx.printStackTrace();
							Criteria criteria1 = new Criteria();
							String provider = locationManager.getBestProvider(criteria1, false);
							location = locationManager.getLastKnownLocation(provider);
							if(isGpsEnable())
							{
								//provider=LocationManager.GPS_PROVIDER;
								location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
								//showToast("IN error gps");
								InorbitLog.d("Park GPS Enable on npx");
							}
							else
							{
								//provider=LocationManager.NETWORK_PROVIDER;
								location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
								//showToast("IN error net");
								InorbitLog.d("Park Network Provider  on npx");
							}
						}

						if(location!=null){
							latitude = location.getLatitude();
							longitude = location.getLongitude();
							savePref();
							EventTracker.logEvent(getResources().getString(R.string.parkAssist_ParkHereWithLocation), false);
							Toast.makeText(context, "Your car has been parked at this location", 0).show();
							//direction.setTextColor(Color.parseColor("#FFFFFF"));
							direction.setClickable(false);

						}else{
							direction.setClickable(false);
							latitude = 0;
							longitude = 0;
							savePref();
							
							EventTracker.logEvent(getResources().getString(R.string.parkAssist_ParkHere), false);
							Toast.makeText(context, "Your car has been parked", 0).show();

							//							Log.d("Location:","Latitude_CUR " + latitude);
							//							Log.d("Location:","Longitude_CUR " + longitude);
							//
							//							Log.d("Location:","Latitude_Saved " + saved_lati);
							//							Log.d("Location:","Longitude_Saved " + saved_longi);
						}


					}else{
						showGpsAlert();
					}
				}else{
					Toast.makeText(actContext, "No Internet Connection", Toast.LENGTH_SHORT).show();
				}

				




			}
		});

		direction.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InorbitLog.d("Latitude_CUR " + latitude);
				InorbitLog.d("Longitude_CUR " + longitude);

				InorbitLog.d("Latitude_Saved " + saved_lati);
				InorbitLog.d("Longitude_Saved " + saved_longi);


				if(saved_lati==0 && saved_longi==0){
					Toast.makeText(getApplicationContext(), "Park your car first before getting directions", Toast.LENGTH_LONG).show();

				}
				else{

					if(netCheck.isNetworkAvailable()){
						if(isGpsEnable()){
							LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

							if(location!=null){
								locationListener.onLocationChanged(location);
							}
							try{
								if(location==null){
									Log.d("", "Inorbit GPS location null");
									Criteria criteria1 = new Criteria();

									location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
								}
							}catch(NullPointerException npx){
								npx.printStackTrace();
								Criteria criteria1 = new Criteria();
								String provider = locationManager.getBestProvider(criteria1, false);
								location = locationManager.getLastKnownLocation(provider);
								if(isGpsEnable())
								{
									//provider=LocationManager.GPS_PROVIDER;
									location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
									//showToast("IN error gps");
									InorbitLog.d("Park GPS Enable on npx");
								}
								else
								{
									//provider=LocationManager.NETWORK_PROVIDER;
									location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
									//showToast("IN error net");
									InorbitLog.d("Park Network Provider  on npx");
								}
							}
							EventTracker.logEvent(getResources().getString(R.string.parkAssist_FindCar), false);
							latitude = location.getLatitude();
							longitude = location.getLongitude();

							String uri = "http://maps.google.com/maps?saddr=" + latitude +","+ longitude +"&daddr="+ saved_lati +","+ saved_longi;

							//Toast.makeText(getApplicationContext(), "URL IS" + " " + uri, Toast.LENGTH_SHORT).show();
							InorbitLog.d("Parked uri >>"+uri);
							Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
							intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
							startActivity(intent);
						}else{
							fromFindCar = true;
							showGpsAlert();
						}
					}else{
						Toast.makeText(actContext, "No Internet Connection", Toast.LENGTH_SHORT).show();
					}	
				}
				
			}
		});

		cancelImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try {

					File file = new File(path_new);
					boolean deleted = file.delete();

					if(deleted){

						imageView.setImageResource(R.drawable.parking);
						Toast.makeText(context, "Image deleted successfully", 0).show();
					}
					else{

						Toast.makeText(context, "No image to delete", 0).show();
					}

				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}


			}
		});

	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		/*		slide_menu.toggle(true);*/

		if(item.getTitle().toString().equalsIgnoreCase(getString(R.string.actionBarTitle)))
		{
			Intent intent = new Intent();
			setResult(5, intent);
			this.finish();

			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		return true;
	}



	public void showImage(String path){

		Log.d("PATHOFIMAGE","PATHOFIMAGE ShowImage" + path_new);

		if(mFileTemp.length()<1){

			imageView.setImageResource(R.drawable.parking);
		}
		else{

			try {

				//				imageView.setImageBitmap(getNewBitmap(mImagePagerCaptureUri, 640));

				Bitmap bm = (getNewBitmap(mImagePagerCaptureUri, 480));
				imageView.setImageBitmap(rotateImage(bm,path));

				//				thumbnail = getNewBitmap(mImagePagerCaptureUri, 640);
				//
				//

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			prefs = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, con.MODE_WORLD_READABLE);
			editor = prefs.edit();
			//editor.putString("ImagePath", path_new);
			editor.putInt("Floor", wheelFloors.getCurrentItem());
			editor.putString("Notes", edit_data.getText().toString());
			editor.commit();


		}
	}


	public  Bitmap getNewBitmap(Uri uri,int dimensions) throws FileNotFoundException, IOException{

		InputStream input = context.getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inDither=true;//optional
		onlyBoundsOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		input.close();
		if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
			return null;

		int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		double ratio = (originalSize > dimensions) ? (originalSize / dimensions) : 1.0;

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither=true;//optional
		bitmapOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional


		input = context.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);


		input.close();
		return bitmap;
	}



	private Bitmap rotateImage(Bitmap bmp, String path) {
		// TODO Auto-generated method stub

		Log.d("PATHOFIMAGE","PATHOFIMAGE rotateImage" + path_new);

		Matrix matrix=new Matrix();

		ExifInterface exif = null;
		try {
			exif = new ExifInterface(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String orientstring = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
		int orientation = orientstring != null ? Integer.parseInt(orientstring) : ExifInterface.ORIENTATION_NORMAL;
		int rotateangle = 0;
		if(orientation == ExifInterface.ORIENTATION_ROTATE_90) 
			rotateangle = 90;
		if(orientation == ExifInterface.ORIENTATION_ROTATE_180) 
			rotateangle = 180;
		if(orientation == ExifInterface.ORIENTATION_ROTATE_270) 
			rotateangle = 270;

		//imageView.setScaleType(ScaleType.CENTER_CROP);   //required
		matrix.setRotate(rotateangle);
		imageView.setImageMatrix(matrix);



		Bitmap newBit = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

		//imageView.setImageBitmap(newBit);

		return newBit;

	}

	private static int getPowerOfTwoForSampleRatio(double ratio){
		int k = Integer.highestOneBit((int)Math.floor(ratio));
		if(k==0) return 2;
		else return k;
	}




	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub


		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 2){

			try {


				showImage(path_new);


			} catch (Exception e) {
				// TODO: handle exception

				e.printStackTrace();
			}



		}else if(resultCode == 10){
			Toast.makeText(context, "GPS Enable", 0).show();
			provider = 	LocationManager.GPS_PROVIDER;
			location = locationManager.getLastKnownLocation(provider);

		}else{ 
			Toast.makeText(context, "GPS Enable", 0).show();
			provider = 	LocationManager.GPS_PROVIDER;
			location = locationManager.getLastKnownLocation(provider);
		}
	}


	private class FloorsAdapter extends ArrayWheelAdapter<String> {
		// Index of current item
		int currentItem;
		// Index of item to be highlighted
		int currentValue;

		/**
		 * Constructor
		 */
		public FloorsAdapter(Context context, String[] items, int current) {
			super(context, items);
			this.currentValue = current;
			setTextSize(19);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			if (currentItem == currentValue) {
				view.setTextColor(0xFF0000F0);
			}
			view.setTypeface(Typeface.SANS_SERIF);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			currentItem = index;
			return super.getItem(index, cachedView, parent);
		}
	}



	@Override
	public void onScrollingStarted(WheelView wheel) {
		// TODO Auto-generated method stub

	}



	@Override
	public void onScrollingFinished(WheelView wheel) {
		// TODO Auto-generated method stub

	}



	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub

	}

	public boolean isGpsEnable(){
		boolean isGpsOn = false;
		try{
			LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
			isGpsOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return isGpsOn;
	}


	public void showGpsAlert(){

		// Build the alert dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Location Services Not Active");
		builder.setMessage("Please enable Location Services and GPS Satellites for better Results");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogInterface, int i) {
				// Show location settings when the user acknowledges the alert dialog
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//if user doesnt turn on location services put location found as false

				try
				{
					dialog.dismiss();
					if(fromFindCar){
						fromFindCar = false;
						LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

						if(location!=null){
							locationListener.onLocationChanged(location);
						}else{

						}

						if(location!=null){
							Log.d("Latitude ","Latitude " + latitude);
							latitude = location.getLatitude();
							longitude = location.getLongitude();


							//Toast.makeText(context, "Unable to fetch your current location", Toast.LENGTH_SHORT).show();
							//direction.setTextColor(getResources().getColor(R.color.inorbit_list_background));

							//direction.setEnabled(false);

							String uri = "http://maps.google.com/maps?saddr=" + latitude +","+ longitude +"&daddr="+ saved_lati +","+ saved_longi;
							Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
							intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
							startActivity(intent);
						}else{
							String uri = "http://maps.google.com/maps?saddr=" + latitude +","+ longitude +"&daddr="+ saved_lati +","+ saved_longi;
							Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
							intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
							startActivity(intent);
						}


					}else{



						LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

						if(location!=null){
							locationListener.onLocationChanged(location);
						}else{

						}

						if(location!=null){
							Log.d("Latitude ","Latitude " + latitude);
							latitude = location.getLatitude();
							longitude = location.getLongitude();

							Toast.makeText(context, "Your car has been parked at this location", 0).show();
							//direction.setTextColor(Color.parseColor("#FFFFFF"));
							direction.setClickable(false);
						}else{
							Toast.makeText(context, "Unable to fetch your current location", Toast.LENGTH_SHORT).show();
							//direction.setTextColor(getResources().getColor(R.color.inorbit_list_background));
							direction.setEnabled(false);
						}
						savePref();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});
		Dialog alertDialog = builder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();


	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();


		try{
			EventTracker.endLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			EventTracker.startLocalyticsSession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		try{
			//locationManager.removeUpdates((LocationListener) this);
			EventTracker.endFlurrySession(getApplicationContext());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.onStop();

	}

	void getPref(){
		SharedPreferences shrd = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, con.MODE_WORLD_READABLE);
		saved_lati = (double)shrd.getFloat(LATITUDE, 0);
		saved_longi = (double)shrd.getFloat(LONGITUDE, 0);
		currentFloor  = shrd.getInt("Floor", 3);
		edit_data.setText(shrd.getString("Notes", ""));

		if(saved_lati==0 || saved_longi == 0){

			direction.setTextColor(Color.parseColor("#A4A4A4"));
			direction.setEnabled(false);
		}
		else{

			direction.setTextColor(Color.parseColor("#FFFFFF"));
			direction.setEnabled(true);
		}
	}

	void savePref(){

		saved_lati = latitude;
		saved_longi = longitude;

		Log.d("SAVED","SAVED " + saved_lati);

		prefs = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, con.MODE_WORLD_READABLE);
		editor = prefs.edit();
		//editor.putString("ImagePath", path_new);
		editor.putInt("Floor", wheelFloors.getCurrentItem());

		editor.putString("Notes", edit_data.getText().toString());
		editor.putFloat(LATITUDE, (float) latitude);
		editor.putFloat(LONGITUDE, (float) longitude);
		editor.commit();
	}


	/* on start */
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}


}

package com.phonethics.inorbit;

import java.util.List;

import android.app.ActivityManager;
import android.app.Application;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.ViewDebug.FlagToString;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class NotificationService extends IntentService {
	NotificationManager mNotificationManager;
	
    public NotificationService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        
        if (!extras.isEmpty() /*&& extras.containsKey("localGCM")*/) {
        	GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        	
        	// The getMessageType() intent parameter must be the intent you received
            // in your BroadcastReceiver.
        	String messageType = gcm.getMessageType(intent);
        	
        	// has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
        	Log.d("----", "GCM MESSAGE "+messageType);
        	Log.d("----", "GCM MESSAGE Intent extras "+ extras.toString());
            /*if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString(), "", extras);
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " +extras.toString(), "", extras);
            // If it's a regular GCM message, do some work.
            } else */
        	
        	
        	if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
            	sendNotification(extras.getString("message"), extras.getString("notification_type"), extras);
            }
        }
    }
    
    /**
	    Intent intent = new Intent(mContext, LoginGateway.class);
		ComponentName cn = intent.getComponent();
		Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
		mContext.startActivity(mainIntent);
		finish();
	*/
    
    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg, String notificationType, Bundle extras) {
    	if (msg == null || msg.trim().equals("")) {
    		return;
    	}
    	
    	int NOTIFICATION_ID = 1;
        NotificationCompat.Builder builder;
        
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        
        Intent intent;
        if (notificationType != null && notificationType.equalsIgnoreCase("offer")) {
        	intent = new Intent(this, ViewOffersViaPush.class);
        	try {
        		NOTIFICATION_ID = Integer.parseInt(extras.getString("post_id"));
        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        	intent.putExtra("fromNotification", true);
        	intent.putExtras(extras);
        } else if (notificationType != null && notificationType.equalsIgnoreCase("tip")) {
        	intent = new Intent(this, CustomerViewTipsActivityViaPush.class);
        	try {
        		NOTIFICATION_ID = Integer.parseInt(extras.getString("place_id"));
        	} catch (Exception e) {
        		
        	}
        	intent.putExtras(extras);
        } else 
        	intent = new Intent(this, HomeGrid.class);
        
        //intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT); Activity Running in Background then notification click does nothing
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        intent.putExtra("KEY", true);
        
        PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        
        NotificationCompat.Builder mBuilder =  new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.ic_launcher)
        .setAutoCancel(true)
        .setContentTitle("Inorbit")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(msg))
/*        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.abcdef))
        .setStyle(new NotificationCompat.BigPictureStyle()
                 .bigPicture(BitmapFactory.decodeResource(getResources(), R.drawable.abcdef)))*/
        .setContentText(msg);
        
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
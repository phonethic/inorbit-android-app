package com.phonethics.inorbit;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.actionbarsherlock.ActionBarSherlock;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;



import com.phonethics.inorbit.ForMe.ViewHolder;
import com.phonethics.model.SpecialPost;
import com.phonethics.networkcall.DeleteSpecialPostResultReceiver;
import com.phonethics.networkcall.DeleteSpecialPostService;
import com.phonethics.networkcall.GetSpecialDatesResultReceiver;
import com.phonethics.networkcall.GetSpecialDatesService;
import com.phonethics.networkcall.MSpecialPostResultReceiver;
import com.phonethics.networkcall.DeleteSpecialPostResultReceiver.DeleteSpecialPostReceiver;
import com.phonethics.networkcall.MSpecialPostResultReceiver.MSpecialPost;
import com.phonethics.networkcall.MSpecialPostService;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ViewSpecialPosts extends SherlockActivity implements MSpecialPost, DeleteSpecialPostReceiver{

	Activity context;
	SessionManager session;
	NetworkCheck network;
	ActionBar actionBar;
	String STORE_ID="";

	//SwipeListView viewPostsList;
	ListView viewPostsList;
	ProgressBar progressBar;

	static String USER_ID="";
	static String AUTH_ID="";

	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	static String API_HEADER;
	static String API_VALUE;

	MSpecialPostResultReceiver mSpecialPosts;
	String URL;

	String PLACE_ID_Url = "?place_id=";

	String POST_ID = "?post_id=";

	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> PLACE_ID=new ArrayList<String>();
	ArrayList<String> POST_TYPE=new ArrayList<String>();
	ArrayList<String> DATE_TYPE=new ArrayList<String>();
	ArrayList<String> TITLE=new ArrayList<String>();
	ArrayList<String> DESCRIPTION=new ArrayList<String>();
	ArrayList<String> IMAGE_TITLE1=new ArrayList<String>();
	ArrayList<String> IMAGE_TITLE2=new ArrayList<String>();
	ArrayList<String> IMAGE_TITLE3=new ArrayList<String>();
	ArrayList<String> IMAGE_URL1=new ArrayList<String>();
	ArrayList<String> IMAGE_URL2=new ArrayList<String>();
	ArrayList<String> IMAGE_URL3=new ArrayList<String>();
	ArrayList<String> THUMB_URL1=new ArrayList<String>();
	ArrayList<String> THUMB_URL2=new ArrayList<String>();
	ArrayList<String> THUMB_URL3=new ArrayList<String>();
	ArrayList<String> SPECIAL_DATE=new ArrayList<String>();
	ArrayList<String> OFFER_DATE_TIME=new ArrayList<String>();
	ArrayList<String> CREATED_AT=new ArrayList<String>();

	Activity actContext;
	

	String PHOTO_PARENT_URL ;

	DeleteSpecialPostResultReceiver deleteSpecialPost;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_special_posts);

		actContext	=	this;
		context = this;


		PHOTO_PARENT_URL = getResources().getString(R.string.photo_url);

		session = new SessionManager(getApplicationContext());
		network=new NetworkCheck(context);

		actionBar=getSupportActionBar();
		actionBar.setTitle("View Special Offer");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		//viewPostsList = (SwipeListView) findViewById(R.id.viewPostsList);
		viewPostsList = (ListView) findViewById(R.id.viewPostsList);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		HashMap<String,String>user_details=session.getUserDetails();
		USER_ID=user_details.get(KEY_USER_ID).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID).toString();

		URL = getResources().getString(R.string.server_url) + getResources().getString(R.string.broadcast_api)+getResources().getString(R.string.special);

		//get list
		mSpecialPosts = new MSpecialPostResultReceiver(new Handler());
		mSpecialPosts.setReciver(this);

		//Delete Service

		deleteSpecialPost = new DeleteSpecialPostResultReceiver(new Handler());
		deleteSpecialPost.setReceiver(this);


		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			STORE_ID=b.getString("STORE_ID");

			Log.d("SOTREID","inorbit STOREID "  + STORE_ID);

		}else{

			Log.d("SOTREID","inorbit bundle null");
		}

		if(network.isNetworkAvailable()){

			callViewSpecialPost();
		}


	}

	private void callViewSpecialPost() {
		// TODO Auto-generated method stub

		Intent intent = new Intent(context, MSpecialPostService.class);
		intent.putExtra("mSpecialPosts", mSpecialPosts);
		intent.putExtra("URL", URL+PLACE_ID_Url+STORE_ID);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);
		intent.putExtra("place_id", STORE_ID);
		progressBar.setVisibility(View.VISIBLE);
		context.startService(intent);

	}

	@Override
	public void onMSpecialPost(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		progressBar.setVisibility(View.GONE);

		ID.clear();
		PLACE_ID.clear();
		POST_TYPE.clear();
		//DATE_TYPE.add(getSpecialBroadcast.get(i).getDate_type());
		TITLE.clear();
		DESCRIPTION.clear();
		IMAGE_TITLE1.clear();
		IMAGE_TITLE2.clear();
		IMAGE_TITLE3.clear();
		IMAGE_URL1.clear();
		IMAGE_URL2.clear();
		IMAGE_URL3.clear();
		THUMB_URL1.clear();
		THUMB_URL2.clear();
		THUMB_URL3.clear();
		//SPECIAL_DATE.add(getSpecialBroadcast.get(i).getSpecial_date());
		OFFER_DATE_TIME.clear();
		CREATED_AT.clear();

		final ArrayList<SpecialPost> getSpecialBroadcast = resultData.getParcelableArrayList("specialboradcast");

		String status = resultData.getString("SEARCH_STATUS");
		String msg = resultData.getString("SEARCH_MESSAGE");


		try {

			if(status.equalsIgnoreCase("true")){

				for(int i=0;i<getSpecialBroadcast.size();i++){

					ID.add(getSpecialBroadcast.get(i).getId());
					PLACE_ID.add(getSpecialBroadcast.get(i).getPlace_id());
					POST_TYPE.add(getSpecialBroadcast.get(i).getPost_type());
					//DATE_TYPE.add(getSpecialBroadcast.get(i).getDate_type());
					TITLE.add(getSpecialBroadcast.get(i).getTitle());
					DESCRIPTION.add(getSpecialBroadcast.get(i).getDescription());
					IMAGE_TITLE1.add(getSpecialBroadcast.get(i).getImage_title1());
					IMAGE_TITLE2.add(getSpecialBroadcast.get(i).getImage_title2());
					IMAGE_TITLE3.add(getSpecialBroadcast.get(i).getImage_title3());
					IMAGE_URL1.add(getSpecialBroadcast.get(i).getImage_url1());
					IMAGE_URL2.add(getSpecialBroadcast.get(i).getImage_url2());
					IMAGE_URL3.add(getSpecialBroadcast.get(i).getImage_url3());
					THUMB_URL1.add(getSpecialBroadcast.get(i).getThumb_url1());
					THUMB_URL2.add(getSpecialBroadcast.get(i).getThumb_url2());
					THUMB_URL3.add(getSpecialBroadcast.get(i).getThumb_url3());
					//SPECIAL_DATE.add(getSpecialBroadcast.get(i).getSpecial_date());
					OFFER_DATE_TIME.add(getSpecialBroadcast.get(i).getOffer_date_time());
					CREATED_AT.add(getSpecialBroadcast.get(i).getCreated_at());
				}


				//SpecialPostAdapter adapter=new SpecialPostAdapter(actContext, R.drawable.ic_launcher, R.drawable.ic_launcher, TITLE,DESCRIPTION,IMAGE_TITLE1,IMAGE_URL1, THUMB_URL1, OFFER_DATE_TIME, CREATED_AT);
				//viewPostsList.setAdapter(adapter);
				
				SpecialPostAdapterNew adapter=new SpecialPostAdapterNew(actContext,getSpecialBroadcast);
				viewPostsList.setAdapter(adapter);

				viewPostsList.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub

						Intent intent=new Intent(context, SpecialPostDetailView.class);
						intent.putExtra("PLACE_ID", getSpecialBroadcast.get(position).getPlace_id());
						//intent.putExtra("storeName", storeName);
						//intent.putExtra("POST_ID", ID.get(position));
						intent.putExtra("title", getSpecialBroadcast.get(position).getTitle());
						intent.putExtra("description", getSpecialBroadcast.get(position).getDescription());
						intent.putExtra("thumbUrl", getSpecialBroadcast.get(position).getThumb_url1());
						intent.putExtra("imageUrl", getSpecialBroadcast.get(position).getImage_url1());
						intent.putExtra("offerValidTill", getSpecialBroadcast.get(position).getOffer_date_time());
						intent.putExtra("CODE", getSpecialBroadcast.get(position).getId()+"-"+"XX");
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						startActivity(intent);

					}
				});

				viewPostsList.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int pos, long arg3) {
						// TODO Auto-generated method stub

						final int position = pos ;

						AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
						alertDialog.setTitle("Delete Offer");
						alertDialog.setMessage("Do you want to delete this offer?");
						alertDialog.setPositiveButton("Yes",new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub

								callDeleteSpecialPostService(ID.get(position));
							}


						});
						alertDialog.setNegativeButton("No",new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.cancel();
							}
						});

						alertDialog.show();

						return true;
					}
				});
				
			}else if(msg.contains("Invalid")){
				MyToast.showToast((Activity) context, "Please Login Again");
				//session.logoutCustomer();
				Intent intent=new Intent(context,MerchantLogin.class);
				intent.putExtra("isInvalidAuth", true);
				intent.putExtra("comeBackTo", Conifg.intForMe);
				startActivity(intent);
				onBackPressed();
			}else{

				showToast(msg);
				viewPostsList.setAdapter(null);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


	}


	void showToast(String message){
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

	}


	class SpecialPostAdapterNew extends ArrayAdapter<SpecialPost>{


		Activity 		context;
		LayoutInflater 	inflate;
		ArrayList<SpecialPost> specialModelArr;

		public SpecialPostAdapterNew(Activity context,ArrayList<SpecialPost> specialModelArr){
			super(context, R.layout.special_post_layout_new);

			this.context = context;
			this.specialModelArr = specialModelArr;
			inflate = context.getLayoutInflater();
			
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return specialModelArr.size();
		}

		@Override
		public SpecialPost getItem(int position) {
			// TODO Auto-generated method stub
			return specialModelArr.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.special_post_layout_new,null);

				holder.txtTitle=(TextView)convertView.findViewById(R.id.broadCastOffersText);
				holder.imgBroadcastLogo=(ImageView)convertView.findViewById(R.id.imgBroadcastLogo);
				holder.txtDate=(TextView)convertView.findViewById(R.id.broadCastOffersDate);
				holder.txtMonth=(TextView)convertView.findViewById(R.id.broadCastOffersMonth);
				holder.viewOverLay=(View)convertView.findViewById(R.id.viewOverLay);
				holder.broadCastOffersDescription=(TextView)convertView.findViewById(R.id.broadCastOffersDescription);

				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.txtTitle.setText(getItem(position).getTitle());
			hold.broadCastOffersDescription.setText(getItem(position).getDescription());
			try
			{
				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");
				Date date_con = (Date) dt.parse(getItem(position).getCreated_at());
				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);

				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+getItem(position).getCreated_at());
				hold.txtDate.setText(day+"");
				hold.txtMonth.setText(dt2.format(date_con)+"");



			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			try
			{
				if(getItem(position).getThumb_url1().toString().equalsIgnoreCase(""))
				{
					//Log.i("LOGO", "LOGO "+logo.get(position));

					//imageLoaderList.DisplayImage("",hold.imgBroadcastLogo);
					try {

						//						Picasso.with(context).load("").placeholder(R.drawable.ic_launcher)
						//						.error(R.drawable.ic_launcher)
						//						.into(hold.imgBroadcastLogo);
						hold.txtDate.setTextColor(Color.argb(255, 255, 255, 255));
						hold.viewOverLay.setVisibility(View.INVISIBLE);
						Log.d("", "Inorbit url >"+PHOTO_PARENT_URL);

					} catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){
						e.printStackTrace();
					}


				}
				else
				{

					//String photo_source=THUMB_URL1.get(position).toString().replaceAll(" ", "%20");
					Log.d("", "Inorbit url >>"+PHOTO_PARENT_URL+getItem(position).getThumb_url1().toString().replaceAll(" ", "%20"));
					hold.txtDate.setTextColor(Color.argb(200, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.VISIBLE);
					//imageLoaderList.DisplayImage(PHOTO_PARENT_URL+getItem(position).getThumb_url1().toString().replaceAll(" ", "%20"), hold.imgBroadcastLogo);

					try {

						Picasso.with(context).load(PHOTO_PARENT_URL+getItem(position).getThumb_url1().toString().replaceAll(" ", "%20")).
						placeholder(R.drawable.ic_launcher)
						.error(R.drawable.ic_launcher).into(hold.imgBroadcastLogo);;
					} catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}


			return convertView;
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		this.finish();
		/*	overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);*/
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}


	public void callDeleteSpecialPostService(String post_id) {
		// TODO Auto-generated method stub

		Intent intent = new Intent(context,DeleteSpecialPostService.class);
		intent.putExtra("deleteSpecialPost", deleteSpecialPost);
		intent.putExtra("URL",URL + POST_ID + post_id);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);

		Log.d("WHOLEURL","WHOLEURL " + URL + POST_ID + post_id);

		context.startService(intent);
	}

	@Override
	public void onDeleteSpecialPost(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		String status = resultData.getString("status");
		String message = resultData.getString("message");
		try {
			if(status.equalsIgnoreCase("true")){
				showToast(message);
				callViewSpecialPost();
			}
			else{

				showToast(message);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}


	

}

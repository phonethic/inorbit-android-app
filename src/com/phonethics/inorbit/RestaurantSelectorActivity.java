package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.model.ParticularStoreInfo;
import com.squareup.picasso.Picasso;

public class RestaurantSelectorActivity extends SherlockActivity {
	private Activity mContext;
	private ActionBar mActionBar;
	private ArrayList<ParticularStoreInfo> allStores;
	private ListView restaurantList;
	private RestaurantListAdapter adapter;
	private EditText editText;
	private boolean toApprovedPage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.restaurant_chooser);
		mContext = this;
		
		initViews();
		getBundleValues();
		
		adapter = new RestaurantListAdapter(mContext, 0, allStores);
		restaurantList.setAdapter(adapter);
		restaurantList.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				if (toApprovedPage) {
					Intent intent=new Intent(mContext, RestaurantMenuApprovalPage.class);
					intent.putExtra("STORE_NAME", adapter.displayList.get(position).getName());
					intent.putExtra("STORE_LOGO", adapter.displayList.get(position).getImage_url());
					intent.putExtra("STORE_ID", adapter.displayList.get(position).getId());
					intent.putExtra("PLACE_PARENT", adapter.displayList.get(position).getPlace_parent());
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				} else {
					Intent intent=new Intent(mContext, AddRestaurantMenu.class);
					intent.putExtra("STORE_NAME", adapter.displayList.get(position).getName());
					intent.putExtra("STORE_LOGO", adapter.displayList.get(position).getImage_url());
					intent.putExtra("STORE_ID", adapter.displayList.get(position).getId());
					intent.putExtra("PLACE_PARENT", adapter.displayList.get(position).getPlace_parent());
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		});
		
		editText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (adapter != null) {
                    adapter.getFilter().filter(editText.getText().toString());
                }
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}
	
	void initViews() {
		mActionBar	=	getSupportActionBar();
		mActionBar.setTitle("Choose Place");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();
		
		restaurantList = (ListView) findViewById(R.id.restaurantList);
		editText = (EditText) findViewById(R.id.searchText);
	}
	
	private void getBundleValues() {
		Bundle bundle=getIntent().getExtras();
		if(bundle!=null) {
			allStores = bundle.getParcelableArrayList("ALL_RESTAURANTS");
			toApprovedPage = bundle.getBoolean("toApprovedPage");
		}
	}
	
	private class RestaurantListAdapter extends ArrayAdapter<ParticularStoreInfo> implements Filterable {
		private Activity context;
		private ArrayList<ParticularStoreInfo> displayList;
		LayoutInflater inflate;
		
		public RestaurantListAdapter(Activity context, int resource, ArrayList<ParticularStoreInfo> objects) {
			super(context, resource, objects);
			this.context = context;
			this.displayList = objects;
			inflate = context.getLayoutInflater();
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			if (convertView == null) {
				ViewHolder holder = new ViewHolder();
				convertView = inflate.inflate(R.layout.layout_place_chooser, null);
				(convertView.findViewById(R.id.band)).setVisibility(View.GONE);
				
				holder.title=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.logo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.logo.setScaleType(ScaleType.FIT_CENTER);
				holder.description=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc); 
				
				holder.title.setTypeface(InorbitApp.getTypeFaceTitle());
				holder.description.setTypeface(InorbitApp.getTypeFace());
				holder.description.bringToFront();
				
				holder.logo.setScaleType(ScaleType.CENTER_INSIDE);
				holder.title.setTypeface(InorbitApp.getTypeFaceTitle());
				holder.description.setTypeface(InorbitApp.getTypeFaceTitle());
				holder.description.setTextSize(12);
				convertView.setTag(holder);
			}
			
			ViewHolder holder = (ViewHolder) convertView.getTag();
			holder.title.setText(displayList.get(position).getName());
			holder.description.setText(displayList.get(position).getDescription());
			
			String photo_source=displayList.get(position).getImage_url().toString().replaceAll(" ", "%20");
			try {
				Picasso.with(context).load(getResources().getString(R.string.photo_url)+photo_source)
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(holder.logo);
			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			} catch(Exception e){
				e.printStackTrace();
			}
			return convertView;
		}
		
		private class ViewHolder {
			private ImageView logo;
			private TextView title;
			private TextView description;
		}
		
		@Override
		public Filter getFilter() {
			// TODO Auto-generated method stub
			 return new Filter() {
		            @Override
		            protected FilterResults performFiltering(CharSequence charSequence) {
		                List<ParticularStoreInfo> filteredResult = getFilteredResults(charSequence.toString());

		                FilterResults results = new FilterResults();
		                results.values = filteredResult;
		                results.count = filteredResult.size();
		                
		                return results;
		            }
		            
		            @Override
		            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
		            	if (filterResults!= null && filterResults.count > 0) {
		            		displayList = (ArrayList<ParticularStoreInfo>) filterResults.values;
		            	} else {
		            		displayList = new ArrayList<ParticularStoreInfo>();
		            	}
		            	adapter = new RestaurantListAdapter(mContext, 0, displayList);
		        		restaurantList.setAdapter(adapter);
		                adapter.notifyDataSetChanged();
		            }


		            private ArrayList<ParticularStoreInfo> getFilteredResults(String constraint){
		                if (constraint.length() == 0){
		                    return  allStores;
		                }
		                ArrayList<ParticularStoreInfo> listResult = new ArrayList<ParticularStoreInfo>();
		                for (ParticularStoreInfo obj : allStores) {
		                    if (constraint.length()<=obj.getName().length() && obj.getName().toLowerCase().contains(constraint.toLowerCase())/*constraint.equalsIgnoreCase(obj.storeInfo.getName().subSequence(0, constraint.length()).toString())*/){
		                        listResult.add(obj);
		                    }
		                }
		                return listResult;
		            }
		        };
		}
	}
}

package com.phonethics.inorbit;

import java.util.Calendar;

public class CustomMethods {

	
	
	public static long getPresentTime(){

		long todaysInMillSec = 0;
		Calendar cal = Calendar.getInstance();
		todaysInMillSec = cal.getTimeInMillis();
		InorbitLog.d("Time "+todaysInMillSec);
		return todaysInMillSec;


	}

	public static long getDaysDiff(long lastDate){
		long diff = getPresentTime() - lastDate;
		long days =  diff / (24 * 60 * 60 * 1000);
		InorbitLog.d("Days "+days);
		return days;
	}

}

package com.phonethics.inorbit;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.MerchantHome.ValidateMerchantBroadCast;
import com.phonethics.model.RequestTags;

public class InboxCreateMessage extends SherlockActivity {

	private Context mContext;
	private ActionBar mActionBar;
	SessionManager 	mSessionMnger;
	EditText mMessage;
	Button mSendBtn;
	LinearLayout mNewMessageLayout;
	String URL = "inbox_api/";
	ArrayList<String> allMessages;
	TextView msgTxtViews[];
	RelativeLayout mProgressBarNewMsg;
	ArrayList<String> store_ids = new ArrayList<String>();
	MessageResponseReciver reciverObj;
	DBUtil 	dbUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inbox_create_message);

		mContext = this;
		mActionBar	=	getSupportActionBar();
		mActionBar.setTitle("Create Message");
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.show();

		dbUtil = new DBUtil(mContext);
		mSessionMnger = new SessionManager(mContext);
		mMessage = (EditText)findViewById(R.id.mMessage);
		mSendBtn = (Button)findViewById(R.id.mSendBtn);
		mNewMessageLayout = (LinearLayout)findViewById(R.id.mNewMessage);
		allMessages = new ArrayList<String>();
		mProgressBarNewMsg = (RelativeLayout)findViewById(R.id.mProgressBarNewMsg);

		Bundle bundle = getIntent().getExtras();
		if(bundle!=null){
			store_ids = bundle.getStringArrayList("SELECTED_STORE_ID");
		}
		
		if (store_ids.size() == 1) {
			mActionBar.setTitle(dbUtil.getStoreNameFromPlaceId(store_ids.get(0)));
		}
		mSendBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mMessage.length()!=0){
					allMessages.add(mMessage.getText().toString());
					postThisMessage();	
					//createMessageLayout();
					mMessage.setText("");
					InputMethodManager imm = (InputMethodManager)getSystemService(
						      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(mMessage.getWindowToken(), 0);
				}
				else{
					showtoast("Please enter text first");
				}

			}
		});
		
		mMessage.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.toString().trim().length() > 0) {
					mSendBtn.setBackgroundColor(Color.parseColor("#112D76"));
					mSendBtn.setTextColor(Color.parseColor("#ffffff"));;
				} else {
					mSendBtn.setBackgroundColor(Color.parseColor("#888888"));
					mSendBtn.setTextColor(Color.parseColor("#aaaaaa"));
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	}

/*
	void createMessageLayout() {
		// TODO Auto-generated method stub

		TextView msgTxtViews = new TextView(mContext);
		RelativeLayout.LayoutParams layoutParam = new RelativeLayout.LayoutParams(200,LayoutParams.WRAP_CONTENT );

		msgTxtViews.setText(mMessage.getText().toString());
		msgTxtViews.setBackgroundColor(Color.parseColor("#FFFFFF"));
		msgTxtViews.setTextColor(Color.parseColor("#000000"));
		msgTxtViews.setGravity(Gravity.LEFT);
		msgTxtViews.setTextSize(20);
		msgTxtViews.setPadding(20, 20, 20, 20);
		layoutParam.setMargins(0, 10, 0, 20);
		msgTxtViews.setLayoutParams(layoutParam);
		mNewMessageLayout.addView(msgTxtViews);

		//to add view for grey lines
		View view = new View(mContext);
		LayoutParams viewParam = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,10,0);
		view.setBackgroundResource(R.color.white_trans);
		view.setLayoutParams(viewParam);
		mNewMessageLayout.addView(view);

	}*/


	void showtoast(String msg) {
		// TODO Auto-generated method stub
		Toast.makeText(mContext, msg, 0).show();
	}

	void postThisMessage(){
		mProgressBarNewMsg.setVisibility(View.VISIBLE);

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		String place_id = dbUtil.getPlaceIdByPlaceParent("0");
		JSONObject json = new JSONObject();
		try {
			json.put("message", mMessage.getText().toString());
			JSONArray arr = new JSONArray();
			for(int i=0;i<store_ids.size();i++)
				arr.put(store_ids.get(i));
			json.put("to", arr);
			json.put("from",place_id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		MyClass myclass = new MyClass(mContext);
		myclass.postRequest(RequestTags.TAG_CREATE_NEW_MSG, headers, json);
	}

	class MessageResponseReciver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {

				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					String message = intent.getStringExtra("MESSAGE");
					mProgressBarNewMsg.setVisibility(View.GONE);
					if(success.equalsIgnoreCase("true")){
						showAlertDialog(message);
					}else{
						showtoast(message);
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		IntentFilter filter = new IntentFilter(RequestTags.TAG_CREATE_NEW_MSG);
		mContext.registerReceiver(reciverObj = new MessageResponseReciver(), filter);
	}

	protected void showAlertDialog(String message) {
		// TODO Auto-generated method stub

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(message)
		.setCancelable(false)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity
				finish();
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	protected void onStop() {
		try{
			mContext.unregisterReceiver(reciverObj);

		}catch(Exception ex){
			ex.printStackTrace();
		}

		super.onStop();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		return true;
	}
}

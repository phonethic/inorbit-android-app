package com.phonethics.inorbit;

import java.util.ArrayList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.AppEventsConstants;
import com.facebook.AppEventsLogger;
import com.phonethics.eventtracker.EventTracker;
/*import com.phonethics.networkcall.GetAllStores_Of_Perticular_User;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver.GetAllStore;*/
import com.phonethics.networkcall.MerchantLoginReceiver;
import com.phonethics.networkcall.MerchantLoginReceiver.LoginReceiver;
import com.phonethics.networkcall.MerchantLoginService;
import com.phonethics.networkcall.MerchantPasswordReceiver;
import com.phonethics.networkcall.MerchantPasswordReceiver.SetPasswordReceiver;
import com.phonethics.networkcall.MerchantPasswordService;
import com.phonethics.networkcall.MerchantRegister;
import com.phonethics.networkcall.MerchantResultReceiver;
import com.phonethics.networkcall.MerchantResultReceiver.MerchantRegisterInterface;

public class SignupMerchant extends SherlockFragmentActivity implements SetPasswordReceiver, LoginReceiver, MerchantRegisterInterface {

	
	ActionBar actionBar;

	ArrayList<String>pages=new ArrayList<String>();
	static Activity context;


	static String API_HEADER;
	static String API_VALUE;

	String SET_PASSWORD_PATH;
	String REGISTER_PATH;


	String LOGIN_PATH;
	String LOGIN_URL;

	ProgressBar shopProgress;
	//Session Manger Class
	SessionManager session;

	String mobileno="";

	static String STORE_URL;
	static String SOTRES_PATH;

	static String USER_ID="";
	static String AUTH_ID="";

	Button buttonEdit;
	TextView txtMerchantMobNo;
	Button signUpResendButton;
	Button signUpNext;
	static EditText  shopVerificationCode;
	TextView serverMessage;

	NetworkCheck isnetConnected;

	String REGISTER_URL;

	String password;
	String mobile_no;

	//Set Merchant Password Receiver
	public MerchantPasswordReceiver mPasswordRecevier;
	//Login Recevier
	public MerchantLoginReceiver mLoginRecevier;

	//All Stores
	//GetAllStores_Of_Perticular_User_Receiver mAllStores;

	String verification_code;

	MerchantResultReceiver mRegister;

	String server_message;
	EditText editPassword,editVerifyPassword;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_merchant);
		
		context=this;

		session=new SessionManager(context);

		isnetConnected=new NetworkCheck(context);

		actionBar=getSupportActionBar();
		actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
		actionBar.show();
		actionBar.setDisplayHomeAsUpEnabled(true);

		buttonEdit=(Button)findViewById(R.id.buttonEdit);
		txtMerchantMobNo=(TextView)findViewById(R.id.txtMerchantMobNo);
		signUpResendButton=(Button)findViewById(R.id.signUpResendButton);
		signUpNext=(Button)findViewById(R.id.signUpNext);
		shopVerificationCode=(EditText)findViewById(R.id.shopVerificationCode);
		editPassword = (EditText)findViewById(R.id.editPassWord);
		editVerifyPassword = (EditText)findViewById(R.id.editPassWordVerify);
		serverMessage=(TextView)findViewById(R.id.serverMessage);

		mRegister=new MerchantResultReceiver(new Handler());
		mRegister.setReceiver(this);

		
		
		REGISTER_URL=getResources().getString(R.string.server_url) + getResources().getString(R.string.merchant_api);

		//Defining URL's
		LOGIN_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.merchant_api);
		LOGIN_PATH=getResources().getString(R.string.login);

		//Store Path
		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		SOTRES_PATH=getResources().getString(R.string.allStores);


		REGISTER_PATH=getResources().getString(R.string.merchant_register);
		SET_PASSWORD_PATH=getResources().getString(R.string.set_password);

		//Api Headers
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);


		shopProgress=(ProgressBar)findViewById(R.id.customerProgressBar);

		mPasswordRecevier=new MerchantPasswordReceiver(new Handler());
		mPasswordRecevier.setReceiver(this);

		mLoginRecevier=new MerchantLoginReceiver(new Handler());
		mLoginRecevier.setReceiver(this);
		
		Bundle b=getIntent().getExtras();

		if(b!=null)
		{
			try
			{
				mobileno=b.getString("mobileno");
				server_message=b.getString("server_message");
				txtMerchantMobNo.setText(mobileno);
				serverMessage.setText(server_message);

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		buttonEdit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				context.finish();
				context.overridePendingTransition(0,R.anim.shrink_fade_out_center);
			}
		});

		signUpNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				verification_code=shopVerificationCode.getText().toString();
				password=shopVerificationCode.getText().toString();
				
				if(verification_code.length()!=0){
					
				}
				else
				{	
					EventTracker.logEvent(context.getResources().getString(R.string.mSignup_VerifyCodeInvalid), false);
					showToast("Please enter verification code");
				}
				
				String pswrd = editPassword.getText().toString();
				String vPswrd = editVerifyPassword.getText().toString();
				if(pswrd.equalsIgnoreCase("")){
					showToast("Please enter password.");
				}else{
					if(pswrd.equalsIgnoreCase(vPswrd)){
						callSetPasswordService();	
					}else{
						showToast("Passwoed did not match.");
					}
				}
				
			}
		});
		signUpResendButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callRegistrationApi();
			}
		});

	}
	
	void callRegistrationApi()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			//Calling Register Api to Register Merchant.
			Intent intent=new Intent(context, MerchantRegister.class);
			intent.putExtra("merchantRegister", mRegister);
			intent.putExtra("URL", REGISTER_URL+REGISTER_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("mobile_no",mobileno);
			context.startService(intent);
			shopProgress.setVisibility(View.VISIBLE);
		}else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}

	void loginService(){
		if(isnetConnected.isNetworkAvailable()){
			Intent intent=new Intent(context, MerchantLoginService.class);
			intent.putExtra("merchantLogin", mLoginRecevier);
			intent.putExtra("URL", LOGIN_URL+LOGIN_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("contact_no", mobileno);
			intent.putExtra("password", editPassword.getText().toString());
			context.startService(intent);
			shopProgress.setVisibility(View.VISIBLE);

		}
		else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}
	
	void callSetPasswordService()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, MerchantPasswordService.class);
			intent.putExtra("URL", REGISTER_URL+SET_PASSWORD_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("setPassword", mPasswordRecevier);
			intent.putExtra("password", editPassword.getText().toString());
			intent.putExtra("mobile_no", mobileno);
			/*if(ForgetPassword==1)
			{*/
			intent.putExtra("verification_code", verification_code);
			intent.putExtra("isForgotPassword",true);
			/*}*/
			context.startService(intent);
			shopProgress.setVisibility(View.VISIBLE);
		}else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}
	

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onReceivePasswordResult(int resultCode, Bundle resultData) {

		shopProgress.setVisibility(View.GONE);
		String passstatus=resultData.getString("passstatus");
		String passmsg=resultData.getString("passmsg");
		if(passstatus.equalsIgnoreCase("true")){
			loginService();
		}else{
			showToast(passmsg);
		}
	}


	@Override
	public void onReceiveLoginResult(int resultCode, Bundle resultData) {
	
		try
		{
			shopProgress.setVisibility(View.GONE);
			String auth_id=resultData.getString("login_authCode");
			String loginstatus=resultData.getString("loginstatus");
			String loginid=resultData.getString("loginid");

			USER_ID=loginid;
			AUTH_ID=auth_id;

			if(loginstatus.equalsIgnoreCase("true")){
				session.createLoginSession(mobile_no, password.toString(), USER_ID, AUTH_ID);

				AppEventsLogger logger = AppEventsLogger.newLogger(this);

				Bundle parameters = new Bundle();
				parameters.putString("User Type", "Merchant");
				parameters.putString("Login Type", "Mobile");

				logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION,parameters);
				
				Intent intent = new Intent(context, PlaceChooser.class);
				Tables.mbIsMerchantLoggedInNow = true;
				startActivity(intent);
				finish();
				

			}else {
				Toast.makeText(context,auth_id, Toast.LENGTH_SHORT).show();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishTo();
		return true;
	}

	void finishTo()
	{
		Intent intent=new Intent();
		intent.putExtra("isAddStore",false);
		context.setResult(4, intent);
		context.finish();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);
		//		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}


	@Override
	public void onReceiverMerchantRegister(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		shopProgress.setVisibility(View.GONE);
		String passstatus=resultData.getString("passstatus");
		String passmsg=resultData.getString("passmsg");


		if(passstatus.equalsIgnoreCase("true")){
			serverMessage.setText(passmsg);
		}else{
			showToast(passmsg);
		}
	}

	public static  class VerificationBroadCast extends BroadcastReceiver  {


		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String verification_code= intent.getStringExtra("verification_code");
			setVerificationCode(verification_code);
			EventTracker.logEvent(context.getResources().getString(R.string.mSignup_VerifyCodeDone), false);
		}

	}

	static void setVerificationCode(String verification_code)	{
		//signup page details.
		Log.i("SignUpMerchant : ", "Verification Code merchant : "+verification_code);
		shopVerificationCode.setText(verification_code);
		

	}
	
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}
	
	@Override
	protected void onStop() {
		//EventTracker.endFlurrySession(getApplicationContext());	
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
	}
	
	@Override
	protected void onResume() {
		EventTracker.startLocalyticsSession(getApplicationContext());
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
	}
	
	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}

}

	

	


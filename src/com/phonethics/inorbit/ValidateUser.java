package com.phonethics.inorbit;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse;
import com.android.volley.Request.Method;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.model.RequestTags;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

public class ValidateUser {
	
	Context 				context;
	SessionManager 			session;
	private Object USER_ID;
	private Object AUTH_ID;
	public static final String 				KEY_USER_ID="user_id";
	//Auth Id
	public static final String 				KEY_AUTH_ID="auth_id";
	

	public ValidateUser(Context context){
		this.context 	= context;
		session			= new SessionManager(context);
		
		HashMap<String,String>user_details=session.getUserDetails();
		USER_ID			= user_details.get(KEY_USER_ID).toString();
		AUTH_ID			= user_details.get(KEY_AUTH_ID).toString();
	}

	
	public void validateMerchant(String Tag){
		JSONObject jObj = new JSONObject();
		//{"register_from":"iPad 6.1.2","user_id":18,"auth_id":8868622}
		try {
			jObj.put("register_from", "");
			jObj.put("user_id", USER_ID);
			jObj.put("auth_id", AUTH_ID);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(context.getResources().getString(R.string.api_header),context.getResources().getString(R.string.api_value));
		headers.put("X-HTTP-Method-Override","PUT");
		validateUser(Tag,jObj,headers);
		
		
	}

	
	void validateUser(final String Tag,JSONObject jSonObject,final HashMap<String, String> headers){
		
		String URL  = getUrlFromTag(Tag);
		
		InorbitLog.d(Tag+" "+jSonObject.toString()+" -- URL-- "+URL);
		
		VolleyResponse.Listener<JSONObject> listener = new Listener<JSONObject>(){

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				InorbitLog.d(Tag+" "+response.toString());
				parseValidateUser(response);
					
			}

		};


		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				InorbitLog.d("Error "+Tag+" "+error.toString());
				if(Tag.equalsIgnoreCase(RequestTags.TagSearchStores)){
					try{


					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}

		};

		JsonObjectRequest req = new JsonObjectRequest(Method.POST,URL, jSonObject, listener, errorListener){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}

		};
		req.setRetryPolicy(new DefaultRetryPolicy(
				30000, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

		InorbitApp.getInstance().addToRequestQueue(req, Tag);
		
	}
	
	
	public void parseValidateUser(JSONObject response) {
		try{
			/*{"success":"true","message":"Login successful.","code":"-113"}*/
			String success = response.getString("success");
			String message = response.getString("message");
			String code    = response.getString("code");
			
			if(success.equalsIgnoreCase("true")){
				//Toast.makeText(context, "True", Toast.LENGTH_SHORT).show();
			}else{
				//Toast.makeText(context, "False", Toast.LENGTH_SHORT).show();
				loginMerchantBack();
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
	}
	
	
	void loginMerchantBack(){
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Inorbit");
		alertDialogBuilder3
		.setMessage("It seems that you were loggedin from other device too, Please Re-login to continue.")
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				//Intent intent=new Intent(context, MerchantsHomeGrid.class);
				session.logoutUser();
				Intent intent=new Intent(context,MerchantLogin.class);
				context.startActivity(intent);
				((Activity) context).finish();
				((Activity) context).overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
				

			}
		});
		AlertDialog alertDialog3 = alertDialogBuilder3.create();
		alertDialog3.show();
	}
	
	
	private String getUrlFromTag(String Tag){
		String URL = "";
		if(Tag.equalsIgnoreCase(RequestTags.Tag_ValidateMerchant)){
			URL	=	context.getResources().getString(R.string.server_url)+
					context.getResources().getString(R.string.merchant_api)+
					context.getResources().getString(R.string.validate_login);
		}
		return URL;
	}



}

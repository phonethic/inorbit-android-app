package com.phonethics.inorbit;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import java.util.ArrayList;
import java.util.Date;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.actionbarsherlock.view.SubMenu;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.ParticularStoreInfo;
import com.phonethics.model.PostDetail;
import com.phonethics.model.RequestTags;
import com.phonethics.model.StoreGallery;
import com.phonethics.model.StoreInfo;
import com.squareup.picasso.Picasso;

import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager.OnActivityResultListener;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.util.LruCache;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class StoreDetails extends SherlockFragmentActivity implements OnNavigationListener{

	static Context				context;
	TextView			txtAddress;
	//TextView			txtDescription;
	NetworkImageView	imgStoreLogo;
	NetworkCheck 		isnetConnected;
	ActionBar 			actionbar;
	JazzyViewPager 		mPager;
	ArrayList<String> 	pages;
	/**
	 * ArrayList of stores offer
	 */
	ArrayList<PostDetail> arrOFFERS;
	private String 		USER_ID;
	private String 		AUTH_ID;
	private boolean		isLoggedInCustomer;

	static String		sStrPage1 = "About";
	static String		sStrPage2 = "Offers";
	static String		sStrPage3 = "Gallery";
	static String		sStrPage4 = "Tips";
	
	static SessionManager 		session;    
	String 				STORE_ID="";
	static 	String 		API_HEADER;
	static 	String 		API_VALUE;
	String				DESCRIPTION="";
	String				REC_SOTRE_NAME="";
	ArrayList<String>	storeNumnbers;
	private boolean 	userLiked=false;
	boolean 			fromLogin = false;
	private 			FbObject fbObj;
	private boolean already_rated;
	private String category;
	private String mall_id, place_id;


	//User Name
	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";

	DBUtil 				dbutil;
	String 				isFav="";
	private String 		MALL_NAME="";
	boolean 			getLike		=	false;
	boolean 			getPostLike	=	false;
	boolean 			likePlace	=	false;
	boolean				likeBttnPressed = false;

	int 				fav_count=0;
	private ParticularStoreDetailBroadcast broadcast;
	//private GalleryResultBroadcast galleryBroadcast;
	ProgressBar			pBar;
	private PagerSlidingTabStrip pagerTabs;
	private StoresOffer offersBroadCast;
	private LikePlace likePlaceBroadcast;
	private UnLikePlace unlikePlaceBroadcast;
	private ArrayList<StoreGallery> arrStoreGallery;
	private GalleryResultBroadcast galleryBroadcast;
	private PageAdapter adapter;
	private OvershootInterpolator sOvershooter = new OvershootInterpolator(10f);
	private DecelerateInterpolator sDecelerator = new DecelerateInterpolator();
	private NetworkCheck		netCheck;
	private Map<String, String> flurryEeventParams;
	private ImageView mImgCall,mImgFav;
	public GridView gridGallery;
	public TextView textNoImages;
	private TextView avgRating;
	private TextView noOfVotes;
	private RateStore mRateStoreReciver;
	String UDM_ID = "";
	private Dialog mListDialog;
	ImageView mRateBtn;
	RatingBar mStoreRating;
	Button btnCancel;
	Button btnSave;
	TextView store_name_rate;
	float mGlobalRating ;
	String mUdm_Id = "";
	UserSpecificRate mUserSpecificRate;
	AvgRatingReceiver avgRatingReceiver;
	TextView rating_comment;
	private static final int TIPS_VIEW_PAGER = 10;
	private static final int CUSTOMER_LOGIN = 15;
	
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_details);
		getSupportMenuInflater();

		context			=	this;


		/**
		 * 
		 * Initialize the layout widgets
		 * 
		 */
		txtAddress 		= 	(TextView) findViewById(R.id.txt_address);
		//txtDescription 	= 	(TextView) findViewById(R.id.txt_description);
		imgStoreLogo	=	(NetworkImageView) findViewById(R.id.img_store_logo);
		mImgCall		=	(ImageView) findViewById(R.id.call_button);
		mImgFav			=	(ImageView) findViewById(R.id.like_button);
		pBar			=	(ProgressBar) findViewById(R.id.pBar_store_details);
		pagerTabs 		= 	(PagerSlidingTabStrip) findViewById(R.id.pagerTabs_storeDetail);
		arrOFFERS		=	new ArrayList<PostDetail>();
		storeNumnbers	=	new ArrayList<String>();
		arrStoreGallery =	new ArrayList<StoreGallery>();
		netCheck		=	new NetworkCheck(context);

		isnetConnected 	= 	new NetworkCheck(context);
		dbutil			=	new DBUtil(context);
		session			=	new SessionManager(context);

		/**
		 * Initalize the action bar of the activity
		 * 
		 */
		actionbar		=	getSupportActionBar();
		actionbar.setTitle("Store Details");
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();


		/**
		 * Initialize the view pager
		 * 
		 */
		mPager			=	(JazzyViewPager)findViewById(R.id.pager_storeDetail);
		pages 			= 	new ArrayList<String>();
		pages.add(sStrPage1);
		pages.add(sStrPage2);
		pages.add(sStrPage3);
		pages.add(sStrPage4);
		/**
		 * 
		 * Get the information of the headers
		 * 
		 */
		API_HEADER		=	getResources().getString(R.string.api_header);
		API_VALUE		=	getResources().getString(R.string.api_value);

		UDM_ID = session.getUdmIDForCustomer();
		InorbitLog.d("UDM_ID " + UDM_ID);
		mListDialog = new Dialog(context);
		mListDialog.setCancelable(true);
		mListDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mListDialog.setContentView(R.layout.store_rating);
		mRateBtn = (ImageView)findViewById(R.id.mRateBtn);
		mStoreRating = (RatingBar) mListDialog.findViewById(R.id.mStoreRating);
		btnCancel = (Button) mListDialog.findViewById(R.id.btnCancel);
		store_name_rate = (TextView) mListDialog.findViewById(R.id.store_name);
		btnSave = (Button) mListDialog.findViewById(R.id.btnSave);
		avgRating = (TextView) findViewById(R.id.avg_rating);
		noOfVotes = (TextView) findViewById(R.id.no_of_votes);
		rating_comment = (TextView) mListDialog.findViewById(R.id.rating_comment);

		mUdm_Id = session.getUdmIDForCustomer();
		try{
			Bundle b=getIntent().getExtras();
			if(b!=null){
				/**
				 * Get the logged in customers details
				 * 
				 */
				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();

				isLoggedInCustomer=session.isLoggedInCustomer();


				/**
				 * Get the selected store id and mall name
				 * 
				 */
				STORE_ID=b.getString("STORE_ID");
				MALL_NAME = b.getString("MALL_NAME");
				REC_SOTRE_NAME = b.getString("REC_SOTRE_NAME");
				category = b.getString("category");
				mall_id = b.getString("mall_id");
				place_id = b.getString("place_id");
				StoreInfo storeInfo = (StoreInfo) b.getParcelable("STORE_INFO");

				String mallid = dbutil.getActiveMallId();
				MALL_NAME = dbutil.getMallNameById(mallid);

				
				/**
				 * Set the action bar title as store title
				 */
				if(storeInfo!=null){
					InorbitLog.d("Store Details "+storeInfo.getTitle());
					InorbitLog.d("Store Details "+storeInfo.getCity());
					actionbar.setTitle(storeInfo.getName());
					STORE_ID = storeInfo.getId();

					setText(storeInfo);

					uploadStoreViewEvent();

				}

				/**
				 * Check whether customer is logged in or not
				 * if yes than get the store like value from server
				 * 
				 */
				if(isLoggedInCustomer){
					getLike=true;
				}
				else{
					getLike=false;
				}

				/**
				 * If netwrok is available than load the data from server else propmt for No internet connection
				 * 
				 */
				if(netCheck.isNetworkAvailable()){
					getPerticularStoreDetails();
					getOffersOfStores();
					getStoreGallery();
				}else{
					showToast("No Intrernet Connection");
					pBar.setVisibility(View.GONE);
				}

				loadThisImage(storeInfo.getImage_url());

				isFav=dbutil.getGeoFavorite(Integer.parseInt(STORE_ID));
				if(isFav.equalsIgnoreCase("1")){
					fav_count=1;
				}else{
					fav_count=0;
				}


			}

			/**
			 * If user has liked the store than change the proper icon for it 
			 * 
			 */
			if(userLiked){
				mImgFav.setImageResource(R.drawable.ic_heart_filled);
			}else{
				mImgFav.setImageResource(R.drawable.ic_heart_unfilled);
			}
			animate(mImgCall).setDuration(200);
			mImgCall.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent arg1) {
					// TODO Auto-generated method stub

					if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
						animate(mImgCall).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
					} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
						animate(mImgCall).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
					}
					return false;
				}
			});

			animate(mImgFav).setDuration(200);
			mImgFav.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent arg1) {
					// TODO Auto-generated method stub

					if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
						animate(mImgFav).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
					} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
						animate(mImgFav).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
					}
					return false;
				}
			});

			animate(mRateBtn).setDuration(200);
			mRateBtn.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent arg1) {
					// TODO Auto-generated method stub

					if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
						animate(mRateBtn).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
					} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
						animate(mRateBtn).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
					}
					
					return false;
				}
			});

			/**
			 * 
			 * Call the store
			 * 
			 */
			mImgCall.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					uploadCallEvents(false);
					if(storeNumnbers.size()==0){
						Toast.makeText(context, R.string.number_not_registered, 0).show();
					}else if(storeNumnbers.size()==1){
						callThisNumber(storeNumnbers.get(0));
					}else{
						showNmberDialog();
					}

				}
			});

			/**
			 * like / unlike the store
			 * 
			 */
			mImgFav.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(session.isLoggedInCustomer()){
						fromLogin = true;
						if(userLiked){
							fromLogin = true;
							if(likeBttnPressed){
								showToast("Please Wait..");
							}else{
								likeBttnPressed = true;
								unlikeThisStore();
							}
						}else{
							if(likeBttnPressed){
								showToast("Please Wait..");
							}else{
								likeBttnPressed = true;
								likeThisStore();
							}
						}
					}else{
						//showToast("Please Login To to mark as favourite");
						fromLogin = false;
						loginBackDialog("Please login first to mark this store as favourite.");
					}
				}
			});

			//to rate the store
			mRateBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (session.isLoggedInCustomer()){
						store_name_rate.setText(REC_SOTRE_NAME);
						if(NetworkCheck.isNetConnected(StoreDetails.this)) {
							mListDialog.show();
							uploadRateEvent("RatePressed");
						} else 
							showToast(StoreDetails.this.getResources().getString(R.string.noInternetConnection));
					} else {
						fromLogin = false;
						loginBackDialog("Please login first to rate this store");
					}
				}
			});
			
			btnCancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListDialog.dismiss();
					uploadRateEvent("RateCancel");
					if (!already_rated) {
						mGlobalRating = 0;
					}
				}
			});

			btnSave.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//showToast("" + mGlobalRating);
					if(netCheck.isNetworkAvailable()) {
						if(mGlobalRating!=0) {
							callStoreRateApi(""+mGlobalRating);	
							mListDialog.dismiss();
							callAvgRating(STORE_ID);
						}
						else{
							showToast("Please rate the store first");
						}
					} else {
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}
			});
			
			pagerTabs.setOnPageChangeListener(new OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int arg0) {
					if (arg0 == 3) {
						createInhouseAnalyticsEvent(place_id, "4", mall_id);
						try {
							HashMap<String, String> map = new HashMap<String, String>();
							if(MALL_NAME!=null && !MALL_NAME.equals("")){
								map.put("MallName", MALL_NAME);
							}
							if(REC_SOTRE_NAME!=null && !REC_SOTRE_NAME.equals("")){
								map.put("StoreName", REC_SOTRE_NAME +" - "+MALL_NAME);
							}
							EventTracker.logEvent(getResources().getString(R.string.storedetail_tip), map);
						} catch(Exception e) {}
						Intent intent = new Intent(StoreDetails.this, CustomerViewTipsActivity.class);
						intent.putExtra("mall_id", mall_id);
						intent.putExtra("place_id", place_id);
						intent.putExtra("store_name", REC_SOTRE_NAME);
						intent.putExtra("status", "1");
						startActivityForResult(intent, TIPS_VIEW_PAGER);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
				
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		setUpLoggedInCustomer();
		createInhouseAnalyticsEvent(place_id, "5", dbutil.getActiveMallId());
	}
	
	private static void createInhouseAnalyticsEvent(String flag, String activityId, String mallId) {
		Uri uri = new Uri.Builder().scheme("content").authority(context.getResources().getString(R.string.authority)).
				appendPath("/insert").build();
        ContentValues cv = new ContentValues(6);
		cv.put("UDM_ID", session.getUdmIDForCustomer());
		cv.put("DATE", new Date().toString());
		cv.put("FLAG", flag);
		cv.put("ACTIVITY_ID", activityId);
		cv.put("MALL_ID", mallId);
		cv.put("SYNC_STATUS", -1);
		context.getContentResolver().insert(uri, cv);
	}
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		if (category.contains("Dine")) {
			MenuItem subMenu1Item = menu.addSubMenu("Menu").getItem();
			subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			
			subMenu1Item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
				
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					try {
						HashMap<String, String> map = new HashMap<String, String>();
						if(MALL_NAME!=null && !MALL_NAME.equals("")){
							map.put("MallName", MALL_NAME);
						}
						if(REC_SOTRE_NAME!=null && !REC_SOTRE_NAME.equals("")){
							map.put("StoreName", REC_SOTRE_NAME +" - "+MALL_NAME);
						}
						EventTracker.logEvent(getResources().getString(R.string.storedetail_menu), map);
					} catch(Exception e) {}
					Intent intent = new Intent(StoreDetails.this, RestaurantMenuGridDisplayActivity.class);
					intent.putExtra("mall_id", mall_id);
					intent.putExtra("place_id", place_id);
					intent.putExtra("status", "1");
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					return true;
				}
			});
		}
		/*MenuItem subMenu1Item = menu.addSubMenu("Tips").getItem();
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		subMenu1Item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				Intent intent = new Intent(StoreDetails.this, CustomerViewTipsActivity.class);
				intent.putExtra("mall_id", mall_id);
				intent.putExtra("place_id", place_id);
				intent.putExtra("status", "1");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				return true;
			}
		});*/
		
		return true;
	}
	
	void uploadRateEvent(String rateState) {
		HashMap<String, String> map = new HashMap<String, String>();
		if(MALL_NAME!=null && !MALL_NAME.equals("")){
			map.put("MallName", MALL_NAME);
		}else{
			return;
		}
		
		if(REC_SOTRE_NAME!=null && !REC_SOTRE_NAME.equals("")){
			map.put("StoreName", REC_SOTRE_NAME +" - "+MALL_NAME);
		}else{
			return;
		}
		EventTracker.logEvent(getResources().getString(R.string.storeDetail_Rate), map);
	}
	
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		return true;
	}

	/**
	 * 
	 * Request the server to get the information of selected store, 
	 * if user is logged in than check whether user has liked this store or not.
	 * 
	 */
	void getPerticularStoreDetails(){
		pBar.setVisibility(View.VISIBLE);
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);


		List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("place_id", STORE_ID));

		if(getLike)
		{
			headers.put("user_id", USER_ID=session.getUserDetailsCustomer().get(KEY_USER_ID_CUSTOMER).toString());
			headers.put("auth_id", "");

			InorbitLog.d("Auth id " +AUTH_ID);
			nameValuePairs.add(new BasicNameValuePair("customer_id", USER_ID));
		}



		MyClass myClass = new MyClass(context);
		myClass.getStoreRequest(RequestTags.TagGetParticularStoreDetails_c, nameValuePairs, headers);

	}


	/**
	 * Get the offers of the selected store, if user is logged in than check whether user has liked those offers or not
	 * 
	 */
	void getOffersOfStores(){
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);
		if(getLike){
			//headers.put(USRE, API_VALUE);
			headers.put("user_id", USER_ID);
		}else{

		}
		
		List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("place_id", STORE_ID));

		MyClass myClass  = new MyClass(context);
		//myClass.getStoreRequest(RequestTags.TagViewBroadcast, nameValuePairs, headers);
		myClass.getStoreRequest(RequestTags.TagViewBroadcast_c, nameValuePairs, headers);
	}


	/**
	 * Like the selected store if user is logged in
	 * 
	 */
	void likeThisStore(){



		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);

		JSONObject json = new JSONObject();
		HashMap<String, String> user_details = session.getUserDetailsCustomer();
		try{
			json.put("user_id", USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString());
			json.put("place_id", STORE_ID);
			json.put("auth_id", AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString());
		}catch(Exception ex){

		}

		pBar.setVisibility(View.VISIBLE);
		MyClass myClass  = new MyClass(context);
		myClass.postRequest(RequestTags.Tag_LikePlace, headers, json);

		//FacebookOperations.likeOnFb(context, fbObj, getResources().getString(R.string.fb_store_object));

	}


	/**
	 * Unlike the store if user is logged in
	 * 
	 */
	void unlikeThisStore(){
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);
		HashMap<String, String> user_details = session.getUserDetailsCustomer();
		headers.put("user_id", USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString());
		headers.put("auth_id", AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString());
		
		JSONObject json = new JSONObject();
		try{
			json.put("user_id", USER_ID);
			json.put("place_id", STORE_ID);
			json.put("auth_id", AUTH_ID);
		}catch(Exception ex){

		}
		List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("place_id", STORE_ID));

		pBar.setVisibility(View.VISIBLE);
		MyClass myClass  = new MyClass(context);
		//myClass.postRequest(RequestTags.Tag_UnLikePlace,nameValuePairs, headers, json);
		myClass.delete(RequestTags.Tag_UnLikePlace,nameValuePairs, headers, json);
	}

	/**
	 * Get the store images 
	 * 
	 */
	void getStoreGallery(){
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(API_HEADER, API_VALUE);


		List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("place_id", STORE_ID));

		MyClass myClass = new MyClass(context);
		myClass.makeGetRequest(RequestTags.TagGetStoreGallery, STORE_ID, headers);

	}

	/**
	 * View pager adapter
	 *
	 */
	public class PageAdapter extends FragmentStatePagerAdapter{
		Activity context;
		ArrayList<String> pages;

		public PageAdapter(FragmentManager fm,Activity context,ArrayList<String> pages) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.pages=pages;
		}

		@Override
		public String getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			//if (position<3)
				return new PageFragment(pages.get(position), context);
			/*else {
				Intent intent = new Intent(StoreDetails.this, CustomerViewTipsActivity.class);
				intent.putExtra("mall_id", mall_id);
				intent.putExtra("place_id", place_id);
				intent.putExtra("status", "1");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}*/
				
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return pages.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Object obj = super.instantiateItem(container, position);
			mPager.setObjectForPosition(obj, position);
			return obj;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if(object != null){
				return ((Fragment)object).getView() == view;
			}else{
				return false;
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method 


		}
	}

	/**
	 * 
	 * Pager fragment 
	 *
	 */


	@SuppressLint("ValidFragment")
	public class PageFragment extends Fragment{

		String title;
		Context context;
		View view;
		private ListView listUnapprovePost;


		public PageFragment(){

		}


		@SuppressLint("ValidFragment")
		public PageFragment(String title,Activity context){
			this.title=title;
			this.context=context;

		}

		@Override
		public void onCreate(Bundle savedInstanceState) {

			super.onCreate(savedInstanceState);
			setRetainInstance(true);
		}


		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			super.onCreateView(inflater, container, savedInstanceState);
			// 

			try{
				if(title.equalsIgnoreCase(sStrPage1)){

					view = inflater.inflate(R.layout.about_store, null);


					TextView txtDes = (TextView) view.findViewById(R.id.text_description);
					txtDes.setText(DESCRIPTION);
					txtDes.setTypeface(InorbitApp.getTypeFace());



					final RelativeLayout buttonCall = (RelativeLayout) view.findViewById(R.id.bttn_call);
					final RelativeLayout buttonFav = (RelativeLayout) view.findViewById(R.id.bttn_fav);
					buttonCall.setVisibility(View.GONE);
					buttonFav.setVisibility(View.GONE);

					ImageView	imgCall = (ImageView) view.findViewById(R.id.bttn_call1);
					ImageView	imgFav = (ImageView) view.findViewById(R.id.bttn_fav1);
					ImageView	mBtnRate = (ImageView) view.findViewById(R.id.mBtnRate);

					//for rating of the store by user
					//RatingBar mStoreRating = (RatingBar) view.findViewById(R.id.mStoreRating);
					mStoreRating.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
						
						@Override
						public void onRatingChanged(RatingBar ratingBar, float rating,
								boolean fromUser) {
							// TODO Auto-generated method stub
							mGlobalRating = rating;
							if ((int) rating == 1) {
								rating_comment.setText("OK");
							} else if ((int) rating == 2) {
								rating_comment.setText("Good");
							} else if ((int) rating == 3) {
								rating_comment.setText("Very Good");
							} else if ((int)rating == 4) 
								rating_comment.setText("Great");
							else if ((int)rating == 5)
								rating_comment.setText("Awesome");
							else 
								rating_comment.setText("");
							//showToast("" + rating);
							//callStoreRateApi(""+rating);
						}
					});

					if(userLiked){
						imgFav.setImageResource(R.drawable.ic_heart_filled);
					}else{
						imgFav.setImageResource(R.drawable.ic_heart_unfilled);
					}
					animate(buttonCall).setDuration(200);
					buttonCall.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent arg1) {
							// TODO Auto-generated method stub

							if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
								animate(buttonCall).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
							} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
								animate(buttonCall).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
							}
							return false;
						}
					});

					animate(buttonFav).setDuration(200);
					buttonFav.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent arg1) {
							// TODO Auto-generated method stub

							if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
								animate(buttonFav).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
							} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
								animate(buttonFav).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
							}
							return false;
						}
					});


					buttonCall.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							//NumberListAdapter adapter = new NumberListAdapter((Activity) context, storeNumnbers);
							/*numberlist.setAdapter(adapter);
							phoneNumberDialog.show();*/
							uploadCallEvents(false);
							if(storeNumnbers.size()==0){
								Toast.makeText(context, R.string.number_not_registered, 0).show();
							}else if(storeNumnbers.size()==1){
								callThisNumber(storeNumnbers.get(0));
							}else{
								showNmberDialog();
							}

						}
					});

					buttonFav.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(session.isLoggedInCustomer()){

								fromLogin = true;
								if(userLiked){
									fromLogin = true;
									if(likeBttnPressed){
										showToast("Please Wait..");
									}else{
										likeBttnPressed = true;
										unlikeThisStore();
									}


								}else{
									if(likeBttnPressed){
										showToast("Please Wait..");
									}else{
										likeBttnPressed = true;
										likeThisStore();
									}

								}
							}else{
								//showToast("Please Login To to mark as favourite");
								fromLogin = false;
								loginBackDialog("Please login first to mark this store as favourite.");
							}


						}
					});


				}else if(title.equalsIgnoreCase(sStrPage2)){
					view = inflater.inflate(R.layout.offer_list, null);


					ListView listView = (ListView) view.findViewById(R.id.list_store_offers);
					listView.setAdapter(new SearchListAdapter((Activity) context, arrOFFERS));
					
					TextView text = (TextView) view.findViewById(R.id.text_no_offers);
					text.setText("No Offers for now.");
					text.setTypeface(InorbitApp.getTypeFace());
					text.setTypeface(InorbitApp.getTypeFace());
					text.setTextColor(Color.BLACK);
					
					if(arrOFFERS.size()==0){
						text.setVisibility(View.VISIBLE);
						listView.setVisibility(View.GONE);
					}else{
						listView.setVisibility(View.VISIBLE);
						text.setVisibility(View.GONE);
					}

					listView.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							// TODO Auto-generated method stub
							uploadBroadcastEvent( arrOFFERS.get(position).getTitle());
							//Intent intent=new Intent(context, OffersBroadCastDetails.class);
							
							Intent intent=new Intent(context, OfferViewNew.class);
							Conifg.fromPedingOffers = false;

							InorbitLog.d("storeName: "+ REC_SOTRE_NAME+ " , mallName: "+ MALL_NAME+" , title: "+ arrOFFERS.get(position).getTitle()
									+" , body: "+ arrOFFERS.get(position).getDescription()+" , POST_ID: "+ arrOFFERS.get(position).getId() 
									+" , PLACE_ID: "+ STORE_ID+" , fromCustomer(hardCodedBoolean): "+ true+" , photo_source: "+ arrOFFERS.get(position).getImage_url1());
							intent.putExtra("storeName", REC_SOTRE_NAME);
							intent.putExtra("mallName", MALL_NAME);
							intent.putExtra("title", arrOFFERS.get(position).getTitle());
							intent.putExtra("body", arrOFFERS.get(position).getDescription());
							intent.putExtra("POST_ID", arrOFFERS.get(position).getId());
							intent.putExtra("PLACE_ID", STORE_ID);
							intent.putExtra("fromCustomer", true);
							intent.putExtra("photo_source",arrOFFERS.get(position).getImage_url1());
							context.startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});

				}else if(title.equalsIgnoreCase(sStrPage3)){
					view = inflater.inflate(R.layout.store_image_grid, null);
					gridGallery = (GridView) view.findViewById(R.id.grid_store_images);
					textNoImages = (TextView) view.findViewById(R.id.text_no_images);
					textNoImages.setTypeface(InorbitApp.getTypeFace());
					textNoImages.setText("No photos have been added yet!");
					//textNoImages.setVisibility(View.VISIBLE);
					gridGallery.setAdapter(new GalleryAdapter((Activity) context, arrStoreGallery));
					final ArrayList<String>GALLERY_PHOTO_SOURCE=new ArrayList<String>();
					for(int i=0;i<arrStoreGallery.size();i++){
						GALLERY_PHOTO_SOURCE.add(arrStoreGallery.get(i).getImage_url());
					}
					
					gridGallery.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int pos, long arg3) {
							// TODO Auto-generated method stub
							//ArrayList<String>GALLERY_PHOTO_SOURCE=new ArrayList<String>();
							//GALLERY_PHOTO_SOURCE.add("");
							try{

								uploadGalleryViewEvent();
								GALLERY_PHOTO_SOURCE.clear();
								for(int i=0;i<arrStoreGallery.size();i++){
									GALLERY_PHOTO_SOURCE.add(arrStoreGallery.get(i).getImage_url());
								}
								Intent intent=new Intent(context,StorePagerGallery.class);
								intent.putStringArrayListExtra("photo_source", GALLERY_PHOTO_SOURCE);
								intent.putExtra("position", pos);
								startActivity(intent);	
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

							}catch(Exception ex){
								ex.printStackTrace();
							}

						}
					});
				} else if (title.equalsIgnoreCase(sStrPage4)) {
					/*Intent intent = new Intent(StoreDetails.this, CustomerViewTipsActivity.class);
					intent.putExtra("mall_id", mall_id);
					intent.putExtra("place_id", place_id);
					intent.putExtra("status", "1");
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);*/
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}

			return view;
		}


	}


	/**
	 * 
	 * 
	 * Broadcast receiver of the getPerticularStoreDetails()
	 * 
	 *
	 */
	class ParticularStoreDetailBroadcast extends BroadcastReceiver{

		private String MOBILE_NO;

		@Override
		public void onReceive(Context context, Intent intent) {  
			// TODO Auto-generated method stub


			try{
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String SEARCH_STATUS = intent.getStringExtra("SUCCESS");

					pBar.setVisibility(View.GONE);
					if(SEARCH_STATUS.equalsIgnoreCase("true")){

						ArrayList<ParticularStoreInfo> particularStoreInfoArr = intent.getParcelableArrayListExtra("ParticularStoreInfoDetails");

						ParticularStoreInfo storeInfo = particularStoreInfoArr.get(0);

						String floor = storeInfo.getBuilding();
						String street = storeInfo.getStreet();
						String landMark = storeInfo.getLandmark();
						String area = storeInfo.getArea();
						String city = storeInfo.getCity();
						String adress = "";

						fbObj = new FbObject();
						fbObj.setobjTitle(storeInfo.getName());
						fbObj.setobjDescription(storeInfo.getDescription());
						fbObj.setobjImage(getResources().getString(R.string.photo_url)+storeInfo.getImage_url());
						fbObj.setobjUrl("http://inorbit.in/");

						if(session.isLoggedInCustomer()){
							if(storeInfo.getUser_like().equalsIgnoreCase("1")){
								userLiked = true;
							}else{
								userLiked = false;
							}
						}else{

						}
						MOBILE_NO = storeInfo.getMob_no1();

						String num1 = storeInfo.getTel_no1();
						String num2 = storeInfo.getTel_no2();
						String num3 = storeInfo.getTel_no3();
						String num4 = storeInfo.getMob_no1();
						String num5 = storeInfo.getMob_no2();
						String num6 = storeInfo.getMob_no3();

						storeNumnbers.clear();
						if(!num1.equalsIgnoreCase("") && !num2.equalsIgnoreCase("") && !num3.equalsIgnoreCase("") && !num4.equalsIgnoreCase("") && !num5.equalsIgnoreCase("") && !num6.equalsIgnoreCase("") ){
							num4 = "+"+num4;
							storeNumnbers.add(num4);
							num5 = "+"+num5;
							storeNumnbers.add(num5);
							num6 = "+"+num6;
							storeNumnbers.add(num6);
							
							storeNumnbers.add(num1);
							storeNumnbers.add(num2);
							storeNumnbers.add(num3);
						}else{

							if(num4.equalsIgnoreCase("")){

							}else{
								num4 = "+"+num4;
								storeNumnbers.add(num4);
							}
							if(num5.equalsIgnoreCase("")){

							}else{
								num5 = "+"+num5;
								storeNumnbers.add(num5);
							}
							if(num6.equalsIgnoreCase("")){

							}else{
								num6 = "+"+num6;
								storeNumnbers.add(num6);
							}
							
							if(num1.equalsIgnoreCase("")){

							}else{
								storeNumnbers.add(num1);
							}
							if(num2.equalsIgnoreCase("")){

							}else{
								storeNumnbers.add(num2);
							}
							if(num3.equalsIgnoreCase("")){

							}else{
								storeNumnbers.add(num3);
							}

						}



						if(floor.equalsIgnoreCase("")){
							adress = "";
						}else{
							adress = floor;
						}

						if(street.equalsIgnoreCase("")){
							adress = "";
						}else{
							adress = adress+", "+street;
						}

						if(landMark.equalsIgnoreCase("")){
							adress = "";
						}else{
							if(!adress.equalsIgnoreCase("")){
								adress = adress+". \n"+landMark;
							}else{
								adress = landMark;
							}

						}
						if(area.equalsIgnoreCase("")){
							adress = "";
						}else{
							adress = adress+", "+area;
						}
						if(city.equalsIgnoreCase("")){
							adress = "";
						}else{
							adress = adress+", "+city;
						}

						if(adress.equalsIgnoreCase("")){
							adress = "Inorbit Mall";
						}else{

						}






						txtAddress.setText(adress);


						DESCRIPTION = storeInfo.getDescription();

						txtAddress.setTypeface(InorbitApp.getTypeFace());


						actionbar.setTitle(storeInfo.getName());
						REC_SOTRE_NAME = storeInfo.getName();

						/*Picasso.with((Activity)context).load(getResources().getString(R.string.photo_url)+storeInfo.getImage_url())
						.placeholder(R.drawable.ic_launcher)
						.error(R.drawable.ic_launcher)
						.into(imgStoreLogo);*/

						/*
						ImageLoader imageLoader = new ImageLoader(Volley.newRequestQueue(context), new ImageLoader.ImageCache() {
							private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

							@Override
							public void putBitmap(String url, Bitmap bitmap) {
								// TODO Auto-generated method stub
								 mCache.put(url, bitmap);
							}

							@Override
							public Bitmap getBitmap(String url) {
								// TODO Auto-generated method stub
								return mCache.get(url);
							}
						});


						imgStoreLogo.setImageUrl(getResources().getString(R.string.photo_url)+storeInfo.getImage_url(), imageLoader);
						imgStoreLogo.setDefaultImageResId(R.drawable.ic_info);
						imgStoreLogo.setErrorImageResId(R.drawable.ic_launcher);
						 */

						loadThisImage(storeInfo.getImage_url());


						PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, pages);
						mPager.setAdapter(adapter);
						mPager.setOffscreenPageLimit(pages.size());
						pagerTabs.setViewPager(mPager);
						pagerTabs.setTextColor(Color.BLACK);
						pagerTabs.setBackgroundColor(Color.TRANSPARENT);
						
						if(userLiked){
							//likeThisStore();
							mImgFav.setImageResource(R.drawable.ic_heart_filled);
						}else{ 
							//unlikeThisStore();
							mImgFav.setImageResource(R.drawable.ic_heart_unfilled);
						}
						uploadStoreViewEvent();


						//startHelperActivity();

					}else{
						//MyToast.showToast((Activity) context, "SEARCH_STATUS "+SEARCH_STATUS);
						String code = intent.getStringExtra("CODE");
						String message = intent.getStringExtra("MESSAGE");

						if(code.equalsIgnoreCase("-116")){
							session.logoutUser();
							Toast.makeText(context, R.string.invalid_auth, Toast.LENGTH_LONG).show();
							Intent intent1=new Intent(context,LoginSigbUpCustomerNew.class);
							context.startActivity(intent1);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}else if(code.equalsIgnoreCase("0")){
							showToast(message);
							pBar.setVisibility(View.GONE);

						}



						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){

							int code1 = bundle.getInt("CODE",0);
							String message1=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "StoreDetails");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();
						}

					}

				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	private void setText(StoreInfo storeInfo){
		String floor = storeInfo.getBuilding();
		String street = storeInfo.getStreet();
		String landMark = storeInfo.getLandmark();
		String area = storeInfo.getArea();
		String city = storeInfo.getCity();
		String adress = "";

		if(floor.equalsIgnoreCase("")){
			adress = "";
		}else{
			adress = floor;
		}

		if(street.equalsIgnoreCase("")){
			adress = "";
		}else{
			if(adress.equalsIgnoreCase("")){
				adress = street;
			}else{
				adress = adress+", "+street;
			}

		}

		if(landMark.equalsIgnoreCase("")){
			adress = "";
		}else{
			if(!adress.equalsIgnoreCase("")){
				adress = adress+". \n"+landMark;
			}else{
				adress = landMark;
			}

		}
		if(area.equalsIgnoreCase("")){
			adress = "";
		}else{
			if(adress.equalsIgnoreCase("")){
				adress = area;
			}else{
				adress = adress+", "+area;
			}

		}
		if(city.equalsIgnoreCase("")){
			adress = "";
		}else{
			if(adress.equalsIgnoreCase("")){
				adress = city;
			}else{
				adress = adress+", "+city;
			}

		}

		if(adress.equalsIgnoreCase("")){
			adress = "Inorbit Mall";
		}else{

		}






		txtAddress.setText(adress);
		DESCRIPTION = storeInfo.getDescription();
		txtAddress.setTypeface(InorbitApp.getTypeFace());


		actionbar.setTitle(storeInfo.getName());
		REC_SOTRE_NAME = storeInfo.getName();
		Picasso.with((Activity)context).load(getResources().getString(R.string.photo_url)+storeInfo.getImage_url())
		.placeholder(R.drawable.ic_launcher)
		.error(R.drawable.ic_launcher)
		.into(imgStoreLogo);
	}

	/**
	 *Broadcast receiver of getOffersOfStores()
	 *
	 */
	class StoresOffer extends BroadcastReceiver{

		@Override
		public void onReceive(final Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				pBar.setVisibility(View.GONE);
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String success = bundle.getString("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						/*String total_pages = bundle.getString("TOTAL_PAGE");
						String total_record = bundle.getString("TOTAL_RECORD");*/
						bundle = intent.getBundleExtra("BundleData");
						if(bundle!=null){


							ArrayList<PostDetail> postArr = bundle.getParcelableArrayList("POST_DETAILS");
							arrOFFERS = postArr;
							for(int i=0;i<arrOFFERS.size();i++){
								InorbitLog.d("Post "+arrOFFERS.get(i).getTitle());
							}

							PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, pages);
							mPager.setAdapter(adapter);
							mPager.setOffscreenPageLimit(pages.size());
							pagerTabs.setViewPager(mPager);
							pagerTabs.setTextColor(Color.BLACK);
							pagerTabs.setBackgroundColor(Color.TRANSPARENT);
						}
					}else{
						String message = bundle.getString("MESSAGE");
						//showToast(message);
						
						boolean isVolleyError = bundle.getBoolean("volleyError",false);
						if(isVolleyError){

							int code1 = bundle.getInt("CODE",0);
							String message1=bundle.getString("MESSAGE");
							String errorMessage = bundle.getString("errorMessage");
							String apiUrl = bundle.getString("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "View Post");
							}

							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();

						}

					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	/**
	 * Broadcast receiver of likeThisStore()
	 *
	 */
	class LikePlace extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try{
				likeBttnPressed = false;
				pBar.setVisibility(View.GONE);
				if(intent!=null){

					String success = intent.getStringExtra("SUCCESS");
					String message = intent.getStringExtra("MESSAGE");
					if(success.equalsIgnoreCase("true")){
						userLiked = true;
						if(fromLogin){
							fromLogin = false;
							showToast(message);	
						}else{

						}
						mImgFav.setImageResource(R.drawable.ic_heart_filled);
						uploadLikeEvents();

					}else{
						String code = intent.getStringExtra("CODE");
						if(fromLogin){
							fromLogin = false;
							showToast(message);	
						}else{

						}
						if(code.equalsIgnoreCase("-221")){
							Toast.makeText(context, "It seems you were login from other device too.\nPlease login to continue.", Toast.LENGTH_LONG).show();
							Intent intent1=new Intent(context,LoginSigbUpCustomerNew.class);
							startActivity(intent1);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}else{
							showToast(message);	
						}
					}
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	/**
	 * Broadcast receiver of unlikeThisStore()
	 *
	 */
	class UnLikePlace extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				likeBttnPressed = false;
				pBar.setVisibility(View.GONE);
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					String message = intent.getStringExtra("MESSAGE");
					if(success.equalsIgnoreCase("true")){

						userLiked = false;
						//showToast(message);
						if(fromLogin){
							fromLogin = false;
							showToast(message);	
						}else{

						}

						mImgFav.setImageResource(R.drawable.ic_heart_unfilled);
						uploadUnlikeEvents();
						//unlikeThisStore();
					}else{
						String code = intent.getStringExtra("CODE");
						if(fromLogin){
							fromLogin = false;
						}else{

						}
						if(code.equalsIgnoreCase("-221")){
							Toast.makeText(context, "It seems you were login from other device too.\nPlease login to continue.", Toast.LENGTH_LONG).show();
							Intent intent1=new Intent(context,LoginSigbUpCustomerNew.class);
							startActivity(intent1);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}else{
							showToast(message);	
						}
					}
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}

		}

	}


	/**
	 * Broadcast receiver of the getStoreGallery()
	 *
	 */
	class GalleryResultBroadcast extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				Bundle bundle = intent.getExtras();
				if(bundle!=null){
					String STATUS = intent.getStringExtra("STATUS");
					if(STATUS.equalsIgnoreCase("true")){
						try{
							bundle = intent.getBundleExtra("BundleGall");
							if(bundle!=null){
								ArrayList<StoreGallery> storeGalleryArr = bundle.getParcelableArrayList("IMAGE_DATE");
								arrStoreGallery = storeGalleryArr;
								gridGallery= (GridView) mPager.findViewById(R.id.grid_store_images);
								for (StoreGallery gallery: arrStoreGallery) {
									InorbitLog.d("GalleryImageCheck"+gallery.getImage_url()+" "+gallery.getThumb_url());
								}
								gridGallery.bringToFront();
								gridGallery.setVisibility(View.VISIBLE);
								TextView text = (TextView) mPager.findViewById(R.id.text_no_images);
								text.setVisibility(View.GONE);
								gridGallery.setAdapter(new GalleryAdapter((Activity) context, arrStoreGallery));
							}
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}else{
						try{
							InorbitLog.d("No images");

							textNoImages.setText("No photos have been added yet!");
							textNoImages.setVisibility(View.VISIBLE);


						}catch(Exception ex){
							InorbitLog.d("No images exception "+ex.toString());
							ex.printStackTrace();

						}
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	/**
	 * List dialog the store numbers
	 * 
	 */
	void showNmberDialog(){

		final Dialog phoneNumberDialog = new Dialog(context);
		phoneNumberDialog.setContentView(R.layout.mallinfonumbersdialog);
		phoneNumberDialog.setTitle("Select number to call");
		phoneNumberDialog.setCancelable(true);
		phoneNumberDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		TextView title = (TextView) phoneNumberDialog.findViewById(android.R.id.title);
		Button bttn = (Button) phoneNumberDialog.findViewById(R.id.Ok);

		title.setTypeface(InorbitApp.getTypeFace());

		ListView numberlist = (ListView) phoneNumberDialog.findViewById(R.id.numberlist);
		numberlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				phoneNumberDialog.dismiss();
				callThisNumber(storeNumnbers.get(arg2));
			}
		});

		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				phoneNumberDialog.dismiss();
			}
		});

		NumberListAdapter adapter = new NumberListAdapter((Activity) context, storeNumnbers);
		numberlist.setAdapter(adapter);
		phoneNumberDialog.show();



	}


	/**
	 * 
	 * List adapter of the store number
	 *
	 */
	public class NumberListAdapter extends ArrayAdapter<String> {

		Activity context;
		LayoutInflater inflator = null;
		ArrayList<String> phoneNumberWithCode;

		public NumberListAdapter(Activity context, ArrayList<String> phoneNumberWithCode) {
			super(context,R.layout.phonenumberlayout);
			this.context = context;
			//this.data = data;
			this.phoneNumberWithCode = phoneNumberWithCode;

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			View rowView = convertView;

			final int pos = position;

			if(convertView == null){
				inflator = context.getLayoutInflater();
				rowView = inflator.inflate(R.layout.phonenumberlayout, null);
				ViewHolder1 holder = new ViewHolder1();

				holder.text = (TextView) rowView.findViewById(R.id.numberText);
				holder.text.setTypeface(InorbitApp.getTypeFace());

				rowView.setTag(holder);
			}

			ViewHolder1 hold = (ViewHolder1) rowView.getTag();
			hold.text.setText(phoneNumberWithCode.get(position).toString());

			return rowView;
		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return phoneNumberWithCode.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return phoneNumberWithCode.get(position);
		}
	}


	/**
	 * Grid adapter of the stores gallery
	 * 
	 * @author Nitin
	 *
	 */
	public class GalleryAdapter extends ArrayAdapter<StoreGallery>{
		Activity context;
		LayoutInflater inflator = null;
		ArrayList<StoreGallery> gallArr;

		public GalleryAdapter(Activity context, ArrayList<StoreGallery> gallArr){
			super(context,R.layout.store_gallery_image);
			this.context = context;
			//this.data = data;
			this.gallArr = gallArr;

			//InorbitLog.d("Gallery Adapter "+gallArr.size());

		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			//View rowView = null;
			//ViewHolder2 holder = null;

			// int pos = position;

			if(convertView == null){
				inflator = context.getLayoutInflater();
				convertView = inflator.inflate(R.layout.store_gallery_image, null);
				ViewHolder2 holder = new ViewHolder2();
				holder.imgGrid = (ImageView) convertView.findViewById(R.id.img_store_gall);
				convertView.setTag(holder);

			}

			ViewHolder2 hold = (ViewHolder2) convertView.getTag();
			try {

				Picasso.with(context).load(getResources().getString(R.string.photo_url)+getItem(position).getThumb_url())
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(hold.imgGrid);

			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			}
			catch(Exception e){
				e.printStackTrace();
			}

			return convertView;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return gallArr.size();
		}

		@Override
		public StoreGallery getItem(int position) {
			// TODO Auto-generated method stub
			return gallArr.get(position);
		}


	}


	static class ViewHolder1{

		public TextView text;


	}


	static class ViewHolder2{

		public ImageView imgGrid;


	}


	/**
	 * List adapter of the stores offers
	 *
	 *
	 */
	static class SearchListAdapter extends ArrayAdapter<PostDetail>{
		ArrayList<PostDetail> postArr;
		Activity context;
		LayoutInflater inflate;
		private String PHOTO_PARENT_URL;
		boolean isApproved;

		public SearchListAdapter(Activity context, 
				ArrayList<PostDetail> postArr) {
			super(context, R.layout.offers_broadcast_layout,postArr);
			// TODO Auto-generated constructor stub
			this.postArr=postArr;
			this.isApproved = isApproved;

			inflate=context.getLayoutInflater();
			PHOTO_PARENT_URL=context.getResources().getString(R.string.photo_url);
			InorbitLog.d("Inorbit Inside adapter"+postArr.toString());

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return postArr.size();
		}
		@Override
		public PostDetail getItem(int position) {
			// TODO Auto-generated method stub
			return postArr.get(position);
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			ViewHolder hold =null;
			if(convertView==null)
			{
				ViewHolder holder = new ViewHolder();
				hold = holder;
				convertView=inflate.inflate(R.layout.offers_broadcast_layout,null);

				holder.txtTitle=(TextView)convertView.findViewById(R.id.broadCastOffersText);
				holder.imgBroadcastLogo=(ImageView)convertView.findViewById(R.id.imgBroadcastLogo);
				hold.imgBroadcastLogo.setScaleType(ScaleType.CENTER_INSIDE);
				holder.txtDate=(TextView)convertView.findViewById(R.id.broadCastOffersDate);
				holder.txtMonth=(TextView)convertView.findViewById(R.id.broadCastOffersMonth);
				holder.txtMonth.setVisibility(View.GONE);
				holder.txtDate.setVisibility(View.GONE);
				holder.viewOverLay=(View)convertView.findViewById(R.id.viewOverLay);
				holder.txtBroadCastStoreTotalLike=(TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalLike);
				holder.txtBroadCastStoreTotalViews = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalViews);
				holder.txtBroadCastStoreTotalShare = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalShare);

				convertView.setTag(holder);

			}else{
				hold=(ViewHolder)convertView.getTag();	
			}

			/*hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);*/

			hold.txtTitle.setText(postArr.get(position).getTitle());
			try
			{
				/*DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");
				Date date_con = (Date) dt.parse(postArr.get(position).getDate().toString());
				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+postArr.get(position).getDate().toString());
				hold.txtDate.setText(day+"");	
				hold.txtMonth.setText(dt2.format(date_con)+"");*/
				hold.txtBroadCastStoreTotalLike.setText(postArr.get(position).getTotal_like());
				hold.txtBroadCastStoreTotalViews.setText(postArr.get(position).getTotal_view());
				hold.txtBroadCastStoreTotalShare.setText(postArr.get(position).getTotal_share());


				String msLike = postArr.get(position).getTotal_like();
				String msView = postArr.get(position).getTotal_view();
				String msShare = postArr.get(position).getTotal_share();

				if(msLike.equalsIgnoreCase("0")){
					hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);
				} else {
					hold.txtBroadCastStoreTotalLike.setVisibility(View.VISIBLE);
				}
				if(msView.equalsIgnoreCase("0")){
					hold.txtBroadCastStoreTotalViews.setVisibility(View.GONE);
				} else {
					hold.txtBroadCastStoreTotalViews.setVisibility(View.VISIBLE);
				}
				if(msShare.equalsIgnoreCase("0")){
					hold.txtBroadCastStoreTotalShare.setVisibility(View.GONE);
				} else {
					hold.txtBroadCastStoreTotalShare.setVisibility(View.VISIBLE);
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}

			try
			{
				if(postArr.get(position).getImage_url1().toString().equalsIgnoreCase(""))
				{
					hold.txtDate.setTextColor(Color.argb(255, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.VISIBLE);
					InorbitLog.d("Inorbit Photo url "+"");
					hold.imgBroadcastLogo.setImageResource(R.drawable.ic_launcher);

				}
				else
				{
					String photo_source=postArr.get(position).getThumb_url1().toString().replaceAll(" ", "%20");
					hold.txtDate.setTextColor(Color.argb(200, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.INVISIBLE);

					try {
						
						Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
						.placeholder(R.drawable.ic_launcher)
						.error(R.drawable.ic_launcher)
						.into(hold.imgBroadcastLogo);
						InorbitLog.d("Inorbit Photo url "+photo_source);


					} catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){
						e.printStackTrace();
					}

				}

			}catch(Exception ex){
				ex.printStackTrace();
			}


			return convertView;
		}

		static class ViewHolder{

			TextView txtTitle,txtDate,txtMonth;
			ImageView imgBroadcastLogo;
			View viewOverLay;
			TextView txtBroadCastStoreTotalLike;
			TextView txtBroadCastStoreTotalViews;
			TextView txtBroadCastStoreTotalShare;


		}

	}


	/**
	 * Call the selected store
	 * 
	 * @param number Selected store number
	 */
	void callThisNumber(String number){
		try{
			boolean hasTelephony = context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
			if (hasTelephony) { 
				uploadCallEvents(true);
				Intent call = new Intent(android.content.Intent.ACTION_DIAL);
				call.setData(Uri.parse("tel:" + number));
				startActivity(call);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}else{
				Toast.makeText(context, "Calling functionality is not available in this device.", Toast.LENGTH_LONG).show();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}
	
	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}
	
	private void setUpLoggedInCustomer() {
		/**
		 * Check whether customer is logged in or not
		 * if yes than get the store like value from server
		 * 
		 */
		if(isLoggedInCustomer = session.isLoggedInCustomer()){
			getLike=true;
		}
		else{
			getLike=false;
		}
		
		/**
		 * If netwrok is available than load the data from server else propmt for No internet connection
		 * 
		 */
		if(netCheck.isNetworkAvailable()){
			getPerticularStoreDetails();
			getOffersOfStores();
			getStoreGallery();
		}else{
			showToast("No Intrernet Connection");
			pBar.setVisibility(View.GONE);
		}

		isFav=dbutil.getGeoFavorite(Integer.parseInt(STORE_ID));
		if(isFav.equalsIgnoreCase("1")){
			fav_count=1;
		}else{
			fav_count=0;
		}


		/**
		 * If user has liked the store than change the proper icon for it 
		 * 
		 */
		if(userLiked){
			mImgFav.setImageResource(R.drawable.ic_heart_filled);
		}else{
			mImgFav.setImageResource(R.drawable.ic_heart_unfilled);
		}
	
	
		mUdm_Id = session.getUdmIDForCustomer();
		callAvgRating(STORE_ID);
		if (session.isLoggedInCustomer())
			callUserSpecificRating();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		try {
			if (requestCode == TIPS_VIEW_PAGER) {
				mPager.setCurrentItem(0);
			}
			
			if (requestCode == CUSTOMER_LOGIN) {
				setUpLoggedInCustomer();
			}
			
			if(requestCode==2) {
				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
				isLoggedInCustomer=session.isLoggedInCustomer();

				if(session.isLoggedInCustomer()){
					getLike=true;
					fromLogin = true;
					getPerticularStoreDetails();
				}else{
					//showToast("Please login to mark place as favourite");
				}
			}
			if(requestCode==5)
			{

			}

		}catch(NullPointerException ex)
		{
			ex.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if (1 == 2) {
			
		} else  {
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		return true;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try{
			EventTracker.endLocalyticsSession(getApplicationContext());

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	private void loadThisImage(String msUrl){


		ImageLoader imageLoader = new ImageLoader(Volley.newRequestQueue(context), new ImageLoader.ImageCache() {
			private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

			@Override
			public void putBitmap(String url, Bitmap bitmap) {
				// TODO Auto-generated method stub
				mCache.put(url, bitmap);
			}

			@Override
			public Bitmap getBitmap(String url) {
				// TODO Auto-generated method stub
				return mCache.get(url);
			}
		});


		imgStoreLogo.setImageUrl(getResources().getString(R.string.photo_url)+msUrl, imageLoader);
		imgStoreLogo.setDefaultImageResId(R.drawable.ic_info);
		imgStoreLogo.setErrorImageResId(R.drawable.ic_launcher);

	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try{
			//FireEvents.upload();
			
			EventTracker.startLocalyticsSession(getApplicationContext());
			IntentFilter filter = new IntentFilter(RequestTags.TagGetParticularStoreDetails_c);
			IntentFilter offersFilter = new IntentFilter(RequestTags.TagViewBroadcast_c);
			IntentFilter filter_gallery = new IntentFilter(RequestTags.TagGetStoreGallery);
			IntentFilter likeFilter = new IntentFilter(RequestTags.Tag_LikePlace);
			IntentFilter unlikeFilter = new IntentFilter(RequestTags.Tag_UnLikePlace);
			IntentFilter rateStore = new IntentFilter(RequestTags.TAG_RATE_STORE);
			IntentFilter userSpecificRate = new IntentFilter(RequestTags.TAG_USER_SPECIFIC_STORE_RATE);
			IntentFilter getAvgRating = new IntentFilter(RequestTags.TAG_AVG_STORE_RATING);
			
			context.registerReceiver(broadcast = new ParticularStoreDetailBroadcast(), filter);
			context.registerReceiver(offersBroadCast = new StoresOffer(), offersFilter);
			context.registerReceiver(likePlaceBroadcast = new LikePlace(), likeFilter);
			context.registerReceiver(unlikePlaceBroadcast = new UnLikePlace(), unlikeFilter);
			context.registerReceiver(galleryBroadcast = new GalleryResultBroadcast(), filter_gallery);
			context.registerReceiver(mRateStoreReciver = new RateStore(), rateStore);
			context.registerReceiver(mUserSpecificRate = new UserSpecificRate(), userSpecificRate);
			context.registerReceiver(avgRatingReceiver = new AvgRatingReceiver(), getAvgRating);
			

			//context.registerReceiver(galleryBroadcast = new GalleryResultBroadcast(), filter_gallery);
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}



	/* on start */


	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}



	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		try{
			EventTracker.endFlurrySession(getApplicationContext());
			//imageLoaderList.clearCache();
			if(broadcast!=null){
				context.unregisterReceiver(broadcast);
			}
			if(offersBroadCast!=null){
				context.unregisterReceiver(offersBroadCast);
			}
			if(unlikePlaceBroadcast!=null){
				context.unregisterReceiver(unlikePlaceBroadcast);
			}
			if(likePlaceBroadcast!=null){
				context.unregisterReceiver(likePlaceBroadcast);
			}
			if(galleryBroadcast!=null){
				context.unregisterReceiver(galleryBroadcast);
			}
			if(mRateStoreReciver!=null){
				context.unregisterReceiver(mRateStoreReciver);
			}
			if (avgRatingReceiver != null) {
				context.unregisterReceiver(avgRatingReceiver);
			}
			if (mUserSpecificRate != null) {
				context.unregisterReceiver(mUserSpecificRate);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void registerEvent(String eventName){
		try{
			EventTracker.logEvent(eventName, false);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Login dialog if user is not logged in and trying to like any store
	 * 
	 */
	void loginBackDialog(String message){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle(REC_SOTRE_NAME);
		alertDialogBuilder
		.setMessage(message)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				Intent intent=new Intent(context,LoginSigbUpCustomerNew.class);
				intent.putExtra("dontUpdateProfile", true);
				startActivityForResult(intent, CUSTOMER_LOGIN);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}


	/**
	 * Register the event when user likes the store
	 * 
	 */
	void uploadLikeEvents(){
		try{
			InorbitLog.d("MallName - "+MALL_NAME + " StroeName "+REC_SOTRE_NAME );
			boolean param = false;
			flurryEeventParams = new HashMap<String, String>();

			if(MALL_NAME!=null && !MALL_NAME.equals("")){
				flurryEeventParams.put("MallName", MALL_NAME);
				param = true;
			}else{
				param = false;
			}


			if(REC_SOTRE_NAME!=null && !REC_SOTRE_NAME.equals("")){
				flurryEeventParams.put("StoreName", REC_SOTRE_NAME +" - "+MALL_NAME);
				param = true;
			}else{
				param = false;
			}

			if(param){
				EventTracker.logEvent(getResources().getString(R.string.storeDetail_Favourite), flurryEeventParams);
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	/**
	 * Register the event when user calls the store
	 * 
	 * @param callDone
	 */
	void uploadCallEvents(boolean callDone){
		try{
			boolean param = false;
			InorbitLog.d("MallName - "+MALL_NAME + " StroeName "+REC_SOTRE_NAME );
			flurryEeventParams = new HashMap<String, String>();
			if(MALL_NAME!=null && !MALL_NAME.equals("")){
				flurryEeventParams.put("MallName", MALL_NAME);
				param = true;
			}else{
				param = false;
			}
			if(REC_SOTRE_NAME!=null && !REC_SOTRE_NAME.equals("")){
				flurryEeventParams.put("StoreName", REC_SOTRE_NAME +" - "+MALL_NAME);
				param = true;
			}else{
				param = false;
			}
			if(param){
				if(callDone){
					EventTracker.logEvent(getResources().getString(R.string.storeDetail_CallDone), flurryEeventParams);
				}else{
					EventTracker.logEvent(getResources().getString(R.string.storeDetail_Call), flurryEeventParams);
				}

			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	/**
	 * Register the event when user unlike the store
	 * 
	 */
	void uploadUnlikeEvents(){
		try{

			boolean param = false;
			InorbitLog.d("UnLike MallName - "+MALL_NAME + " StroeName "+REC_SOTRE_NAME );
			flurryEeventParams = new HashMap<String, String>();
			if(MALL_NAME!=null && !MALL_NAME.equals("")){
				flurryEeventParams.put("MallName", MALL_NAME);
				param = true;
			}else{
				param = false;
			}
			if(REC_SOTRE_NAME!=null && !REC_SOTRE_NAME.equals("")){
				flurryEeventParams.put("StoreName", REC_SOTRE_NAME +" - "+MALL_NAME);
			}else{
				param = false;
			}

			if(param){
				EventTracker.logEvent(getResources().getString(R.string.storeDetail_UnFavourite), flurryEeventParams);
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	/**
	 * Register the event when user click on any offers
	 * 
	 * @param title Offer title
	 */
	void uploadBroadcastEvent(String title){
		try{
			InorbitLog.d("Broadcast  MallName - "+MALL_NAME + " StroeName "+REC_SOTRE_NAME );
			boolean param  = false;
			flurryEeventParams = new HashMap<String, String>();
			if(MALL_NAME!=null && !MALL_NAME.equals("")){
				flurryEeventParams.put("MallName", MALL_NAME);
				param  = true;
			}else{
				param = false;
			}
			if(REC_SOTRE_NAME!=null && !REC_SOTRE_NAME.equals("")){
				flurryEeventParams.put("StoreName", REC_SOTRE_NAME +" - "+MALL_NAME);
				param  = true;
			}else{
				param = false;
			}
			if(title!=null && !title.equals("")){
				if(param){
					param  = true;
					flurryEeventParams.put("BroadCastTitle", title +" - "+ REC_SOTRE_NAME +" - "+MALL_NAME);
				}else{
					param  = false;
				}


			}

			if(param){
				EventTracker.logEvent(getResources().getString(R.string.offerDetail_View), flurryEeventParams);

			}


		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	/***
	 * Register the event when any stores get selected
	 * 
	 */
	void uploadStoreViewEvent(){
		try{

			boolean param = false;
			InorbitLog.d("Like MallName - "+MALL_NAME + " StroeName "+REC_SOTRE_NAME );
			flurryEeventParams = new HashMap<String, String>();
			if(MALL_NAME!=null && !MALL_NAME.equals("")){
				flurryEeventParams.put("MallName", MALL_NAME);
				param = true;
			}else{
				param  = false;
			}
			if((REC_SOTRE_NAME!=null && !REC_SOTRE_NAME.equals(""))&&(param==true)){
				InorbitLog.d("Like MallName - "+MALL_NAME + " StroeName "+REC_SOTRE_NAME );
				flurryEeventParams.put("StoreName", REC_SOTRE_NAME +" - "+MALL_NAME);
				param = true;
			}else{
				param  = false;
			}

			if(param){
				EventTracker.logEvent(getResources().getString(R.string.storeDetail_View), flurryEeventParams);
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Register the event when user clicks on any image of store gallery
	 * 
	 */
	void uploadGalleryViewEvent(){
		try{

			boolean param = false;
			InorbitLog.d("Like MallName - "+MALL_NAME + " StroeName "+REC_SOTRE_NAME );
			flurryEeventParams = new HashMap<String, String>();
			if(MALL_NAME!=null && !MALL_NAME.equals("")){
				flurryEeventParams.put("MallName", MALL_NAME);
				param = true;
			}else{
				param  = false;
			}
			if(REC_SOTRE_NAME!=null && !REC_SOTRE_NAME.equals("")){
				flurryEeventParams.put("StoreName", REC_SOTRE_NAME +" - "+MALL_NAME);
				param = true;
			}else{
				param  = false;
			}

			if(param){
				EventTracker.logEvent(getResources().getString(R.string.storeDetail_ViewGallery), flurryEeventParams);
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private void startHelperActivity(){
		ArrayList<Integer> miarrImages = new ArrayList<Integer>(); 
		miarrImages.add(R.drawable.mall_manager_mall);
		miarrImages.add(R.drawable.mall_manager_stores);
		miarrImages.add(R.drawable.mall_manager_mall);
		miarrImages.add(R.drawable.mall_manager_stores);
		Intent intent = new Intent(context, AppHelperPager.class);
		intent.putIntegerArrayListExtra("Helper_Images_Arr", miarrImages);
		//intent.putExtra("Parent_Back", R.drawable.store_blur);
		intent.putExtra("Title", actionbar.getTitle().toString());
		startActivity(intent);
		overridePendingTransition(0, 0);
	}

	void callStoreRateApi(String rate) {
		// TODO Auto-generated method stub
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		JSONObject json = new JSONObject();

		try {
			json.put("place_id", STORE_ID);
			json.put("rate", rate);
			UDM_ID = session.getUdmIDForCustomer();
			json.put("UDM_id", 	UDM_ID);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		InorbitLog.d("JSON " + json);

		MyClass myclass = new MyClass(context);
		myclass.postRequest(RequestTags.TAG_RATE_STORE, null, headers, json);
	}

	class RateStore extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					String message = intent.getStringExtra("MESSAGE");
					//mProgressBarNewMsg.setVisibility(View.GONE);
					
					if(success.equalsIgnoreCase("true")){
						if (!already_rated) {
							setRatingImg(already_rated = !already_rated);
						}
						showToast(message);
					}else{
						showToast(message);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}
	
	public void setRatingImg(boolean rated) {
		if (rated) {
			mRateBtn.setImageResource(R.drawable.star_fill);
		} else {
			mRateBtn.setImageResource(R.drawable.star_outline);
		}
	}
	
	private void callAvgRating(String place_id) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("place_id", new JSONArray().put(place_id));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		MyClass myclass = new MyClass(context);
		myclass.postRequest(RequestTags.TAG_AVG_STORE_RATING, headers, jsonObject);
	}
	
	class AvgRatingReceiver extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				if (intent!=null) {
					List<String> rating = intent.getStringArrayListExtra("AVG_RATING");
					if (rating.size()>0) {
						if (rating.get(0).equalsIgnoreCase("0") || rating.get(0).equalsIgnoreCase("0.0")) {
							avgRating.setText("No ratings yet");
						} else {
							avgRating.setText("Rating "+rating.get(0)+" ");
							noOfVotes.setText("Votes: "+intent.getStringArrayListExtra("TOTAL_RATE").get(0));
							InorbitLog.d("RATE " + rating);
						}
					}/* else {
						avgRating.setText(0);
						noOfVotes.setText("Votes: 0");
					}*/
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void callUserSpecificRating() {
		// TODO Auto-generated method stub
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("UDM_id", mUdm_Id));
		nameValuePairs.add(new BasicNameValuePair("place_id", STORE_ID));
		
		MyClass myclass = new MyClass(context);
		myclass.getStoreRequest(RequestTags.TAG_USER_SPECIFIC_STORE_RATE, nameValuePairs, headers);
	}
	
	class UserSpecificRate extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			
			try {
				
				if(intent!=null){
					uploadRateEvent("RateSuccess");
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						//showToast(success);
						String rating = intent.getStringExtra("RATE");
						
						if(!rating.trim().equalsIgnoreCase("")) {
							float fRate = Float.parseFloat(rating);
							mStoreRating.setRating(fRate);	
							InorbitLog.d("RATE " + fRate);
							if (!rating.trim().equalsIgnoreCase("0") && !rating.equalsIgnoreCase("0.0")) {
								mRateBtn.setImageResource(R.drawable.star_fill);
								already_rated = true;
							} else {
								already_rated = false;
							}
						} 
						setRatingImg(already_rated);
						InorbitLog.d("RATE " + rating);
					}
					else{
						String message = intent.getStringExtra("MESSAGE");
						showToast(message);
					}
				}
				
			} catch (Exception e) {
				uploadRateEvent("RateFailed");
				e.printStackTrace();
			}
		}
		
	}
}

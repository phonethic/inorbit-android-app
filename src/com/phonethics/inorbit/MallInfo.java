package com.phonethics.inorbit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyResponse.ErrorListener;
import com.android.volley.VolleyResponse.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.CustomerProfile.AreaAdapter;
import com.phonethics.inorbit.CustomerProfile.ViewHolder;

import crittercism.android.t;

public class MallInfo extends SherlockActivity implements OnNavigationListener{


	Button 						bttn_check;
	GridView					grid_inorbit;
	InorbitServiceInfo 			serivce_info;
	Context						context;
	String						MALL_INFOR_URL = "http://dev.inorbit.in/malad/?json=1&post_type=facility&count=-1";
	String						INORBIT_URL="http://inorbit.in/";
	String						MALL_LOCATION = "malad";
	String						JSON_URL = "/?json=1&post_type=facility&count=-1";
	Activity					actContext;
	public DBUtil 						dbutil;
	NetworkCheck 				network;
	ActionBar 					actionBar;
	ProgressBar					pBar;
	ArrayList<String>			dropdownItems,areaId;
	ArrayList<Integer> 			arr_icons;
	String 						business_id="292";
	static String				PLACEHOLDER_URL = "http://www.moneycontrol.com/news_image_files/Inorbit_logo.jpg";


	RelativeLayout bttn_call_us;
	RelativeLayout bttn_locate_us;
	RelativeLayout bttn_mail_us;

	String current_latitude;
	String current_longitude;
	String place_parent;

	Location location;
	LocationListener locationListener;

	String mall_id;
	ArrayList<String> phoneNumber;
	String mall_latitude;
	String mall_longitude;
	String emailID;

	Dialog phoneNumberDialog;
	ListView numberlist;
	Button ok;

	ArrayList<String> phoneNumberWithCode;
	File						configFile;
	private AlertDialog mdMapDialog;
	private TextView text_title;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mall_info);

		
		/**
		 * initialize the layout and calss variables
		 * 
		 * 
		 */
		context	 		= 	this;
		actContext 		= 	this;
		network			=	new NetworkCheck(context);
		dbutil 			= 	new DBUtil(context);
		bttn_check 		= 	(Button) findViewById(R.id.bttn_check);
		grid_inorbit 	= 	(GridView) findViewById(R.id.grid_inorbit_service);
		
		pBar			= 	(ProgressBar) findViewById(R.id.progress);
		text_title		=	(TextView) findViewById(R.id.text_title);
		text_title.setTypeface(InorbitApp.getTypeFace());
		text_title.setTextSize(14);
		dropdownItems	=	new ArrayList<String>();
		areaId 			=	new ArrayList<String>();
	
		arr_icons		= new ArrayList<Integer>();
		phoneNumber     = new ArrayList<String>();
		phoneNumberWithCode = new ArrayList<String>();

		bttn_call_us    = (RelativeLayout)findViewById(R.id.bttn_call_us);
		bttn_locate_us    = (RelativeLayout)findViewById(R.id.bttn_locate_us);
		bttn_mail_us    = (RelativeLayout)findViewById(R.id.bttn_mail_us);

		TextView txtMailUs = (TextView) findViewById(R.id.txt_mail_us);
		TextView txtCallUs = (TextView) findViewById(R.id.txt_call_us);
		TextView txtLocate = (TextView) findViewById(R.id.txt_locate_us);
		TextView txtTitle = (TextView) findViewById(R.id.text_title_info);

		txtMailUs.setTypeface(InorbitApp.getTypeFace());
		txtCallUs.setTypeface(InorbitApp.getTypeFace());
		txtLocate.setTypeface(InorbitApp.getTypeFace());
		txtTitle.setTypeface(InorbitApp.getTypeFace());

		phoneNumberDialog = new Dialog(context);
		phoneNumberDialog.setContentView(R.layout.mallinfonumbersdialog);
		
		phoneNumberDialog.setTitle("Select number to call");
		phoneNumberDialog.setCancelable(true);
		phoneNumberDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		TextView title = (TextView) phoneNumberDialog.findViewById(android.R.id.title);
		title.setTypeface(InorbitApp.getTypeFace());

		numberlist = (ListView) phoneNumberDialog.findViewById(R.id.numberlist);
		ok = (Button) phoneNumberDialog.findViewById(R.id.Ok);
		ok.setTypeface(InorbitApp.getTypeFace());


	
		arr_icons.add(R.drawable.call_us);
		arr_icons.add(R.drawable.locate_us);
		arr_icons.add(R.drawable.mail_us);
		arr_icons.add(R.drawable.mall_info_new);

		//grid_master.setAdapter(new AdapterMaster((Activity) context, titles));
		grid_inorbit.setVisibility(View.VISIBLE);
		

		actionBar		=	getSupportActionBar();
		
		
		
		/**
		 * check for the location services
		 * 
		 */

		final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		/*		Location location = locationManager.getLastKnownLocation(provider);*/
		Log.i("Location ", "Location service started");

		
		locationListener = new LocationListener() {

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub

				current_latitude = location.getLatitude()+"";
				current_longitude = location.getLongitude()+"";
				Log.d("Latitude ","Latitude " + current_latitude);
			}
		};


		actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
		bttn_check.setVisibility(View.GONE);
		bttn_check.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/*getMallInfo(MALL_INFOR_URL);
				Toast.makeText(context, "Please Wait", 0).show();*/
			}
		});

		try{
			
			
			/**
			 * 
			 * initalize the mall areas on the action bar
			 * 
			 */

			dropdownItems 	= dbutil.getAllAreas();
			areaId 			= dbutil.getAllMallIds(); 
			
			String activeAreaId = 	dbutil.getActiveMallId();
			MALL_LOCATION 		= 	dbutil.getAreaNameByInorbitId(activeAreaId);

			CustomSpinnerAdapter business_list=new CustomSpinnerAdapter(actContext, 0, 0, dropdownItems);


			//Setting Navigation Type.
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			actionBar.setListNavigationCallbacks(business_list, this);


			if(activeAreaId.equalsIgnoreCase("")){
				actionBar.setSelectedNavigationItem(0);
			}else{
				String areaName 	= dbutil.getAreaNameByInorbitId(activeAreaId);
				int areaPos 		= dropdownItems.indexOf(areaName);
				Log.d("", "Inorbit active id "+activeAreaId+" Area : "+areaName+" ArrayPos "+areaPos);
				actionBar.setSelectedNavigationItem(areaPos);
			}
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.show();

		}catch(Exception ex){
			ex.printStackTrace();
		}


		bttn_call_us.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try {
					HashMap<String , String> data = new HashMap<String, String>();
					data.put("MallName", dbutil.getMallNameByMallID(dbutil.getActiveMallId()));
					data.put("Action", "CallUs");
					EventTracker.logEvent(MallInfo.this.getResources().getString(R.string.mallInfo_Action), data);
					if(phoneNumberWithCode.size()>1){
						
						NumberListAdapter adapter = new NumberListAdapter(actContext, phoneNumberWithCode);
						numberlist.setAdapter(adapter);
						phoneNumberDialog.show();
					}
					else{


						Intent call = new Intent(android.content.Intent.ACTION_DIAL);
						call.setData(Uri.parse("tel:" +phoneNumberWithCode.get(0)));
						startActivity(call);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}


				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}


			}
		});



		bttn_locate_us.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try {
					HashMap<String , String> data = new HashMap<String, String>();
					data.put("MallName", dbutil.getMallNameByMallID(dbutil.getActiveMallId()));
					data.put("Action", "LocateUs");
					EventTracker.logEvent(MallInfo.this.getResources().getString(R.string.mallInfo_Action), data);
					if(network.isNetworkAvailable()){

						if(isGpsEnable()){
							/**
							 * 
							 * prompt for the google maps dialog.
							 * 
							 */
							showMapDialog();
						}else{
							
							/**
							 * 
							 * prompt for the gps enable dialog.
							 * 
							 */
							showGpsAlert();
						}				
					}else{

						MyToast.showToast(actContext, getResources().getString(R.string.noInternetConnection), 1);
					}
				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}



			}
		});

		bttn_mail_us.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					HashMap<String , String> data = new HashMap<String, String>();
					data.put("MallName", dbutil.getMallNameByMallID(dbutil.getActiveMallId()));
					data.put("Action", "MailUS");
					EventTracker.logEvent(MallInfo.this.getResources().getString(R.string.mallInfo_Action), data);
					
					Intent email = new Intent(Intent.ACTION_SEND);
					email.putExtra(Intent.EXTRA_EMAIL, new String[]{""+emailID});		  
					email.putExtra(Intent.EXTRA_SUBJECT, "Enquiry");
					email.putExtra(Intent.EXTRA_TEXT, "");
					email.setType("message/rfc822");
					startActivity(Intent.createChooser(email, "Choose an Email client :"));
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});

		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				phoneNumberDialog.dismiss();
			}
		});


		numberlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub

				try {

					boolean hasTelephony = context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
					if (hasTelephony) { 
						Intent call = new Intent(android.content.Intent.ACTION_DIAL);
						call.setData(Uri.parse("tel:" + phoneNumberWithCode.get(pos)));
						startActivity(call);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}else{
						Toast.makeText(context, "Calling functionality is not available in this device.", Toast.LENGTH_LONG).show();
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			}
		});


	}


	
	
	/**
	 * Load the mall information / mall services based on the selected mall location on action bar.
	 * 
	 * 
	 * @param url : Mall ul to load the information
	 */
	void getMallInfo(String url){
		try{
			Log.d("", "Inorbit Mall Url >> "+ url.toString());
			pBar.setVisibility(View.VISIBLE);
			//Toast.makeText(context, "Please Wait..", 0).show();
			RequestQueue queue				= Volley.newRequestQueue(context);

			Listener<JSONObject> listener = new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					// TODO Auto-generated method stub
					//Toast.makeText(context, "Succes", 0).show();
					pBar.setVisibility(View.GONE);
					Log.d("", "Inorbit response >> "+ response.toString());
					writeToFile(response.toString());
					parseResponse(response);
				}

			};

			ErrorListener errorListener = new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					//Toast.makeText(context, "Something Went Wrong", 0).show();
					pBar.setVisibility(View.GONE);
					Log.d("", "Inorbit Error >> "+ error.toString());
				}
			};

			JsonObjectRequest request = new JsonObjectRequest(Method.GET,url, null, listener, errorListener);

			request.setRetryPolicy(new DefaultRetryPolicy(15*1000, 1, 1.0f));
			request.setShouldCache(true);
			queue.add(request);


		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	/**
	 * Parse the inofmration which was stored on the shared pref as json data
	 * 
	 * @param jobj
	 */
	void parseResponse(JSONObject jobj){
		try{
			
			if(MALL_LOCATION.equalsIgnoreCase("malad")){
				text_title.setText("Address:\n"+getResources().getString(R.string.address_malad));
			} else if(MALL_LOCATION.equalsIgnoreCase("vashi")){
				text_title.setText("Address:\n"+getResources().getString(R.string.address_vashi));
			} else if(MALL_LOCATION.equalsIgnoreCase("cyberabad")){
				text_title.setText("Address:\n"+getResources().getString(R.string.address_cyberabad));
			} else if(MALL_LOCATION.equalsIgnoreCase("whitefield")){
				text_title.setText("Address:\n"+getResources().getString(R.string.address_whitefield));
			} else if(MALL_LOCATION.equalsIgnoreCase("pune")){
				text_title.setText("Address:\n"+getResources().getString(R.string.address_pune));
			} else if(MALL_LOCATION.equalsIgnoreCase("vadodara")){
				text_title.setText("Address:\n"+getResources().getString(R.string.address_vadodara));
			}
			
			
			ArrayList<InorbitServiceInfo> mallInfoArr = new ArrayList<InorbitServiceInfo>();

			JSONArray jArr = jobj.getJSONArray("posts");
			Log.d("", "Inorbit Post Size >> "+ jArr.length());
			for(int i=0;i<jArr.length();i++){
				Log.d("", "Inorbit Post >> "+ i);
				JSONObject tempJObj = jArr.getJSONObject(i);
				InorbitServiceInfo mallInfo = new InorbitServiceInfo();


				mallInfo.setTitle(tempJObj.getString("title"));
				mallInfo.setContent(tempJObj.getString("content"));

				JSONObject jObj = null;
				try{
					jObj = tempJObj.getJSONObject("thumbnail_images");
				}catch(JSONException jEx){
					mallInfo.setThumbnail("");
				}catch(Exception ex){
					ex.printStackTrace();
					mallInfo.setThumbnail("");
				}
				if(jObj == null){
					mallInfo.setThumbnail("");
				}else{
					JSONObject jObjFull = jObj.getJSONObject("full");
					mallInfo.setThumbnail(jObjFull.getString("url"));
				}



				mallInfoArr.add(mallInfo);
			}

			grid_inorbit.setAdapter(new MallInfoAdapter(actContext, mallInfoArr));
 
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	/**
	 * 
	 * Check whether gps is enable or not on the user's device
	 * 
	 * @return true if gps is on | false if gps is off
	 */
	public boolean isGpsEnable(){
		boolean isGpsOn = false;
		try{
			LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
			isGpsOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return isGpsOn;
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		business_id=itemPosition+"";
		try{
			phoneNumberWithCode.clear();

			String activeAreaId = dbutil.getMallIdByPos(""+(itemPosition+1));
			dbutil.setActiveMall(activeAreaId);
			place_parent = dbutil.getParentPlaceIdByPos(""+(itemPosition+1));

			MALL_LOCATION 		= 	dbutil.getAreaNameByInorbitId(activeAreaId);
			//Log.d("", "Inorbit mall "+MALL_LOCATION);

			// getting selected mall id
			mall_id = dbutil.getMallIdByPlaceParent(place_parent);
			//Log.d("Mall_Id","Mall_Id " +  mall_id);

			// getting phone number for current mall selected
			phoneNumber = dbutil.getPhoneNumberByMallId(mall_id);
			phoneNumberWithCode = phoneNumber;
			
			// getting lat, long for current mall
			mall_latitude = dbutil.getLatitudeByMallId(mall_id);
			//Log.d("Mall_Lat","Mall_Lat " +  mall_latitude);
			mall_longitude = dbutil.getLongitudeByMallId(mall_id);
			//Log.d("Mall_Long","Mall_Long " +  mall_longitude);
			// get mail id for current mall
			emailID = dbutil.getEmailIdByMallId(mall_id);
			//Log.d("Mall_Lat","Mall_Lat " +  emailID);

			
			
			
			/**
			 * 
			 * Check whether file is present in users sd card if yes than check its last modified date.
			 * 
			 * 
			 */
			configFile		=  new File(Environment.getExternalStorageDirectory()+"/inorbit/", mall_id+".bin");
			if(!configFile.exists() || configFile.length()==0){
				//configFile		=  new File(Environment.getExternalStorageDirectory(), MALL_LOCATION+".bin");
				getMallInfo(INORBIT_URL+MALL_LOCATION+JSON_URL);	
			}else{	
				long lastDate = configFile.lastModified();
				int  days = Integer.parseInt(getResources().getString(R.string.refresh_days));
				if(CustomMethods.getDaysDiff(lastDate)>=days){
					getMallInfo(INORBIT_URL+MALL_LOCATION+JSON_URL);
				}else{
					pBar.setVisibility(View.GONE);
					String mallInfo = readFromFile();
					JSONObject response = new JSONObject(mallInfo);
					parseResponse(response);
				}
				

			}


		}catch(Exception ex){
			ex.printStackTrace();
		}
		return false;
	}

	
	
	/**
	 * Write the mall information on users sdcard
	 * 
	 * @param data
	 */
	private void writeToFile(String data) {


		try {
			File folder = new File(Environment.getExternalStorageDirectory() + "/inorbit");
			if(folder.exists()){
				
			}else{
				folder.mkdir();
			}
			File myFile = new File(Environment.getExternalStorageDirectory()+ "/inorbit",mall_id+".bin");
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
			myOutWriter.append(data);
			myOutWriter.close();
			fOut.close();


		} catch (Exception e) {
			InorbitLog.d("File == File write failed");
		}
	}

	
	
	private String readFromFile() {

		String ret = "";



		try{
			File myFile = new File(Environment.getExternalStorageDirectory()+ "/inorbit/"+mall_id+".bin");
			FileInputStream fIn = new FileInputStream(myFile);
			BufferedReader myReader = new BufferedReader( new InputStreamReader(fIn));
			String aDataRow = "";
			String aBuffer = "";
			while ((aDataRow = myReader.readLine()) != null) {
				ret += aDataRow + "\n";
			}
			myReader.close();

		}catch(Exception ex){
			ex.printStackTrace();
		}

		return ret;
	}


	class CustomSpinnerAdapter extends ArrayAdapter<String> implements SpinnerAdapter 
	{

		ArrayList<String>name;
		Activity context;
		LayoutInflater layoutInflater;
		public CustomSpinnerAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> name) {
			super(context, resource, textViewResourceId, name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.name=name;
			layoutInflater=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}


		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return name.get(position);
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/* convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_menus, parent, false);
			}
		

			TextView txtMenu = (TextView) convertView.findViewById(R.id.listText);
			txtMenu.setTypeface(InorbitApp.getTypeFace());
			txtMenu.setText(""+getItem(position));

			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				
				convertView = layoutInflater.inflate(
						R.layout.list_dropdown_item, parent, false);
			}

			TextView txtMenu = (TextView) convertView.findViewById(R.id.txtSpinner);
			txtMenu.setTypeface(InorbitApp.getTypeFace());
			txtMenu.setText(""+getItem(position));

			return convertView;
		}


	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		/*		slide_menu.toggle(true);*/


		finishTo();

		return true;
	}



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}

	/**
	 * Prompt the user to open the in Inorbit mall on google maps
	 * 
	 */
	public void showMapDialog(){
		try{
			Builder builder = new AlertDialog.Builder(context);
			View view  = ((Activity) context).getLayoutInflater().inflate(R.layout.map_dialog, null);
			Button bttnDirection = (Button) view.findViewById(R.id.btn_open_map);
			TextView text_clickingBttn = (TextView) view.findViewById(R.id.text_clickingBttn);
			bttnDirection.setText("Find Inorbit Mall & get directions on google map");
			text_clickingBttn.setText("Clicking the above button will open google maps");
			text_clickingBttn.setTypeface(InorbitApp.getTypeFace(),Typeface.ITALIC);
			bttnDirection.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
			bttnDirection.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
					location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					
					if(location!=null){
						locationListener.onLocationChanged(location);
					}
					try{
						if(location==null){
							Log.d("", "Inorbit GPS location null");
							Criteria criteria1 = new Criteria();
							
							location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						}
					}catch(NullPointerException npx){
						npx.printStackTrace();
						Criteria criteria1 = new Criteria();
						String provider = locationManager.getBestProvider(criteria1, false);
						location = locationManager.getLastKnownLocation(provider);
						if(isGpsEnable())
						{
							//provider=LocationManager.GPS_PROVIDER;
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							//showToast("IN error gps");
							InorbitLog.d("Park GPS Enable on npx");
						}
						else
						{
							//provider=LocationManager.NETWORK_PROVIDER;
							location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
							//showToast("IN error net");
							InorbitLog.d("Park Network Provider  on npx");
						}
					}
					
					if(location==null){
						Toast.makeText(context, "Unable to fetch your lcaotion. Please try restarting the device", 0).show();
					}else{
						current_latitude = location.getLatitude()+"";
						current_longitude = location.getLongitude()+"";
					}
					

					Log.d("Latitude ","Latitude " + current_latitude);
					//String uri = "http://maps.google.com/maps?saddr=" + current_latitude +","+ current_longitude +"&daddr="+ 19.1885679899768 +","+ 72.8349024609436;
					String uri = "http://maps.google.com/maps?saddr=" + current_latitude +","+ current_longitude +"&daddr="+ mall_latitude +","+ mall_longitude;
					mdMapDialog.dismiss();
					Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
					intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
					startActivity(intent);
				}
			});
			builder.setView(view);
			mdMapDialog = builder.create();
			mdMapDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
			mdMapDialog.show();
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}

	void finishTo(){

		Intent intent = new Intent();
		setResult(5, intent);
		this.finish();	
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	/**
	 * Alert dialog to enbale GPS serivce's
	 */
	public void showGpsAlert(){

		// Build the alert dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Location Services Not Active");
		
		builder.setMessage("Please enable Location Services and GPS Satellites for better Results");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogInterface, int i) {
				// Show location settings when the user acknowledges the alert dialog
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivityForResult(intent, 10);
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//if user doesnt turn on location services put location found as false
				try
				{
					dialog.dismiss();
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});
		Dialog alertDialog = builder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();


	}

	public class AdapterMaster extends ArrayAdapter<String>{

		Activity 						context;
		ArrayList<String> 				mallInfoArr;
		private  LayoutInflater 	inflator = null;


		public AdapterMaster(Activity context,ArrayList<String> mallInfoArr){
			super(context,R.layout.grid_refrence_information);
			// TODO Auto-generated constructor stub
			this.context = context;
			this.mallInfoArr = mallInfoArr;

		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mallInfoArr.size();
		}


		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return mallInfoArr.get(position);
		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				ViewHolder holder 		= new ViewHolder();
				inflator				= context.getLayoutInflater();
				convertView 			= inflator.inflate(R.layout.mall_info_grid_refrence_information, null);

				holder.text_title		= (TextView) convertView.findViewById(R.id.text_title);
				holder.text_content		= (TextView) convertView.findViewById(R.id.text_info);
				holder.image_thumbnail	= (ImageView) convertView.findViewById(R.id.img_thumb);

				convertView.setTag(holder);
			}
			ViewHolder hold = (ViewHolder) convertView.getTag();
			try{
				hold.text_content.setVisibility(View.GONE);
				hold.image_thumbnail.setImageResource(arr_icons.get(position));
			}catch(Exception ex){
				ex.printStackTrace();
			}
			return convertView;
		}


		class ViewHolder{
			public TextView 	text_title,text_content;
			public ImageView	image_thumbnail;
		}



	}



	public class NumberListAdapter extends ArrayAdapter<String> {

		Activity context;
		LayoutInflater inflator = null;
		ArrayList<String> phoneNumberWithCode;

		public NumberListAdapter(Activity context, ArrayList<String> phoneNumberWithCode) {
			super(context,R.layout.phonenumberlayout);
			this.context = context;
			//this.data = data;
			this.phoneNumberWithCode = phoneNumberWithCode;

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			View rowView = convertView;
			final int pos = position;

			if(convertView == null){
				inflator = context.getLayoutInflater();
				rowView = inflator.inflate(R.layout.phonenumberlayout, null);
				ViewHolder holder = new ViewHolder();

				holder.text = (TextView) rowView.findViewById(R.id.numberText);
				holder.text.setTypeface(InorbitApp.getTypeFace());

				rowView.setTag(holder);
			}

			ViewHolder hold = (ViewHolder) rowView.getTag();
			hold.text.setText(phoneNumberWithCode.get(position).toString());

			return rowView;
		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return phoneNumberWithCode.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return phoneNumberWithCode.get(position);
		}
	}



	static class ViewHolder{

		public TextView text;


	}


}

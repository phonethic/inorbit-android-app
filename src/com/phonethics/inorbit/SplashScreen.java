
package com.phonethics.inorbit;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.crittercism.app.Crittercism;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.phoenthics.settings.ConfigFile;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.model.RequestTags;
import com.phonethics.model.SplashModel;
import com.squareup.picasso.Picasso;

public class SplashScreen extends SherlockFragmentActivity implements OnTouchListener {

	private File 			mFileCacheDir;
	private Activity 		mActContext;
	private Context			mContext;
	private ImageView 		mimvEenter;
	private Broadcast 		mBroadCastSplesh;
	private NetBroadcast 	mBroadCastNetwork;
	private JazzyViewPager 	mPager;
	private RelativeLayout	mRelLay_img;
	private ImageView 		mImvAppSplash,mImvImg_flip,mImvImg_flip_1;
	private SessionManager 	msession;
	private DecelerateInterpolator 	sDecelerator = new DecelerateInterpolator();
	private OvershootInterpolator 	sOvershooter = new OvershootInterpolator(10f);
	private Vibrator 		mVibrate;
	private Handler 		mHandler_anim;
	private Runnable 		mRunnable_anim;
	private ActionBar 		mActionBar;
	private String 			mActiveplace;
	private boolean 		mboolPagerTouched = false;
	private DBUtil 			mDbutil;
	private Animation 		manimFadein,manimFadeOut;
	private int 			miImgCount=0;
	private TextView		mTvExpBack;
	private ImageView 		mimvLogoImage;
	private long 			mlDbCount;
	private AreaBroadcast mBroadcastArea;
	private int miLaunchVia;
	private Map<String,String> mEventMap=new HashMap<String, String>();
	private Broadcast mBroadCastDataVersion;
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	GoogleCloudMessaging gcm;
	String regid;
	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */

	String SENDER_ID = "18165388270"; /*"1052395693082";*/
	
	private ProgressDialog mProgressDialog;
	
	SessionManager session;
	DBUtil dbUtil;
	UdmIDCustomer mUdmIdCustomer;
	UdmIdMerchant mUdmIdMerchant;
	VersionCheckReceiver versionCheckReceiver;
	BackUpMsg backUpMsg;
	long old_time = 0;
	private boolean isActive = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash_screen_new);

		Crittercism.initialize(getApplicationContext(), getResources().getString(R.string.critism));

		mActContext = this;
		mContext 	= this;
		session=new SessionManager(mContext);
		dbUtil = new DBUtil(mContext);

		/** initalise the layout*/
		initViews();

		/** Request gallery to get gallery information*/
		//callGalleryRequest();
		//checkVersion();

		mimvEenter.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub

				if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
					animate(mimvEenter).setInterpolator(sDecelerator).scaleX(0.7f).scaleY(0.7f);
				} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
					animate(mimvEenter).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				mVibrate.vibrate(50);
				return false;
			}
		});


		mimvEenter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long count = mDbutil.getAreaRowCount();
				InorbitLog.d("Count "+count);
				if (count!=0) {

					//imageLoader.clearCache();
					registerEvent();
					//					Intent intent=new Intent(mActContext, HomeGrid.class);
					//					startActivity(intent);
					//					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					//					finish();

					//added by salman 
					SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
					Editor 	editor=prefs.edit();
					if(session.isLoggedIn()){
						editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
						editor.commit();
						Intent intent=new Intent(mContext,MerchantHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					}else{
						Intent intent=new Intent(mActContext, HomeGrid.class);
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						finish();
					}

				}else{
					MyToast.showToast(mActContext,getResources().getString(R.string.noInternetConnection),1);
				}

			}
		});


		mRelLay_img.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				//logoImage.setVisibility(View.VISIBLE);
				try{
					manimFadein.setFillAfter(true);
					mRelLay_img.setVisibility(View.GONE);
					mPager.setVisibility(View.VISIBLE);
					if(mHandler_anim!=null){

						mHandler_anim.removeCallbacks(mRunnable_anim);
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				return false;
			}
		});


		mPager.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				int action = event.getAction();
				switch (action) {
				case MotionEvent.ACTION_DOWN:
					mboolPagerTouched = true;
					if(mHandler_anim!=null){
						mHandler_anim.removeCallbacks(mRunnable_anim);
					}
					break;
				case MotionEvent.ACTION_UP:
					if(mHandler_anim!=null){
						mHandler_anim.postDelayed(mRunnable_anim, 2000);
					}
					break;
				case MotionEvent.ACTION_MOVE:
					break;

				default:
					break;
				}
				return false;


			}
		});
	}//onCreate Ends here

	
	private void getBackupPushMsg() {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		MyClass myClass = new MyClass(this);
		myClass.getStoreRequest(RequestTags.Back_Up_Push, new ArrayList<NameValuePair>(), headers);
	}
	
	private class BackUpMsg extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				Bundle bundle = intent.getExtras();
				if (bundle.getString("SUCCESS").equalsIgnoreCase("true")) {
					String gcmId = bundle.getString("GCM_ID");
					String gcmFlag = bundle.getString("FLAG");
					String gcmMessage = bundle.getString("MESSAGE");
					if (!session.getGCMID().equalsIgnoreCase(gcmId)) {
						if (gcmFlag.equalsIgnoreCase("yes")) {
							Intent gcmIntent = null;
							try {
								gcmIntent = new Intent(mContext, HomeGrid.class);
								gcmIntent.putExtra("KEY", true);
							} catch (android.content.ActivityNotFoundException anfe) {
								
							}
							PendingIntent pIntent = PendingIntent.getActivity(mContext, 0, gcmIntent, 0);
							NotificationManager mNotificationManager = (NotificationManager) 
									mContext.getSystemService(Context.NOTIFICATION_SERVICE);
							
							NotificationCompat.Builder mBuilder =  new NotificationCompat.Builder(mContext)
					        .setSmallIcon(R.drawable.ic_launcher)
					        .setAutoCancel(true)
					        .setContentTitle("Inorbit")
					        .setStyle(new NotificationCompat.BigTextStyle()
					        .bigText(gcmMessage))
					        .setContentText(gcmMessage);
					        
					        mBuilder.setContentIntent(pIntent);
					        mNotificationManager.notify(2, mBuilder.build());
					        session.setLastVersionUpdateTime();
					        session.setGCMID(gcmId);
							session.setGCMFlag(gcmFlag);
							session.setGCMMessage(gcmMessage);
						}
					}
				}
			}
		}
	}
	
	private void versionCheck() {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("device_os", "android"));
		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String app_version = pInfo.versionName;
			params.add(new BasicNameValuePair("app_version", app_version));
			String customer_udmId = session.getUdmIDForCustomer();
			String merchant_udmId = session.getUdmIDForMerchant();
			
			if (customer_udmId != null && !customer_udmId.equalsIgnoreCase("")) {
				params.add(new BasicNameValuePair("UDM_id", customer_udmId));
			} else if (merchant_udmId != null && !merchant_udmId.equalsIgnoreCase("")) {
				params.add(new BasicNameValuePair("UDM_id", merchant_udmId));
			} else
				return;
			
			params.add(new BasicNameValuePair("device_version", android.os.Build.VERSION.SDK_INT+""));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		MyClass myClass = new MyClass(this);
		myClass.getStoreRequest(RequestTags.AppVersionCheck, params, headers);
	}
	
	/**
	 * initialize all the views from the layouts
	 * 
	 */

	void initViews() {
		mDbutil 	 = new DBUtil(mActContext);
		Typeface tf  = Typeface.createFromAsset(getAssets(), "fonts/RAGE_comeandlive.ttf");
		mActionBar	= getSupportActionBar();
		mActionBar.setTitle(getResources().getString(R.string.actionBarTitle));
		mActionBar.hide();
		//Session
		msession	= new SessionManager(mActContext);
		//Vibrator service
		mVibrate	= (Vibrator)mActContext.getSystemService(Context.VIBRATOR_SERVICE);
		mimvEenter	= (ImageView)findViewById(R.id.open);
		mimvLogoImage	= (ImageView)findViewById(R.id.logoImage);
		mimvLogoImage.setVisibility(View.GONE);
		mTvExpBack 	= (TextView) findViewById(R.id.text_experiance_inorbit);
		mTvExpBack.setText(mActContext.getResources().getString(R.string.base_line));
		mTvExpBack.setTextColor(getResources().getColor(R.color.inorbit_blue));
		mTvExpBack.setTypeface(tf,Typeface.BOLD);
		mTvExpBack.setTextSize(20);

		mPager	= (JazzyViewPager)findViewById(R.id.viewPagerSplash);
		mImvAppSplash	= (ImageView)findViewById(R.id.appSplash);
		mImvImg_flip	= (ImageView) findViewById(R.id.img_flip);
		mImvImg_flip_1	= (ImageView) findViewById(R.id.img_flip_1);
		mImvImg_flip.setScaleType(ScaleType.CENTER_CROP);
		mImvImg_flip_1.setScaleType(ScaleType.CENTER_CROP);

		manimFadein 	= AnimationUtils.loadAnimation(mActContext, R.anim.fade_in_splash);
		manimFadeOut 	= AnimationUtils.loadAnimation(mActContext, R.anim.fade_out_splash);
		mRelLay_img		= (RelativeLayout) findViewById(R.id.relative_img_layout);

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			mFileCacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.InorbitLocalCache");
		}
		else{
			mFileCacheDir=mActContext.getCacheDir();
		}

		if(!mFileCacheDir.exists()) {
			mFileCacheDir.mkdirs();
		}else if(NetworkCheck.isNetConnected(mContext)){

		}
		mPager.setOffscreenPageLimit(4);
		Picasso.with(mActContext).load(R.drawable.splashscreen);

		mProgressDialog=new ProgressDialog(mContext);
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Please wait...");

	}

	/** check from where app get open -  
	 * 
	 * This method has been used to check that from which local notification app has been opened
	 * 
	 * */
	void checkWhereFromAppOpen() {
		try {
			Bundle bundle=getIntent().getExtras();
			if(bundle!=null) {
				miLaunchVia=bundle.getInt("requestCode",0);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}

		try {
			EventTracker.startLocalyticsSession(mActContext);
			if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.Edit_Store_alert))) {
				mEventMap.put("Type", "Edit_Store");
			}else if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.Talk_Now_alert))) {
				mEventMap.put("Type", "Talk_Now");
			}else if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.Login_alert))) {
				mEventMap.put("Type", "Customer_SignIn");
			}else if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.Favourite_alert))) {
				mEventMap.put("Type", "Customer_Favourite");
			}else if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.Customer_Profile_alert))) {
				mEventMap.put("Type", "Customer_SpecialDate");
			}else if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.App_Launch))) {
				mEventMap.put("Type", "App_NotLaunched");
			}

			if(mEventMap.size()!=0) {
				Log.i("Request Code ", "Request Code "+mEventMap.toString());
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}




	/** get users active mall id.
	 * This is been used to load the gallery on splash screen for the mall which user has kept defualt
	 * */
	void getActiveMallIdandPlaceParent() {
		try {
			if(mDbutil.getAreaRowCount()==0){
				mimvEenter.setVisibility(View.VISIBLE);
			} else {
				mimvEenter.setVisibility(View.VISIBLE);
				String activeAreaId = mDbutil.getActiveMallId();
				String activePlace  = mDbutil.getParentPlaceIdByPos(activeAreaId);
				Log.d("", "Inorbit active id "+activePlace);
				if(activePlace.equalsIgnoreCase("")){

				}else{
					mActiveplace = activePlace;
				}
			}

		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * Gallery request call t
	 * 
	 */
	void callGalleryRequest(){

		if(NetworkCheck.isNetConnected(mContext)){
			loadGallery();
		}else{
			dismissDialog();
			showToast("No Internet Connection");
		}

	}


	/**
	 * 
	 * Load the Malls information form server and store in DB.
	 * It also loads the gallery information of each mall
	 * 
	 */
	void loadGallery()
	{
		MyClass myClass = new MyClass(mActContext);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		mlDbCount = mDbutil.getAreaRowCount();
		InorbitLog.d("Count "+mlDbCount);
		if(mlDbCount==0){

			/**
			 * Stores the present date
			 * 
			 */
			msession.setMallsApiCallDate(CustomMethods.getPresentTime());

			myClass.makeGetRequest(RequestTags.TagGetMalls, "-1", headers);
			mimvEenter.setVisibility(View.GONE);

		}else{

			long lastDate 	= msession.getMallsApiCallLastDate();
			int  days 		= Integer.parseInt(getResources().getString(R.string.refresh_days));

			if(CustomMethods.getDaysDiff(lastDate)>=days){
				HashMap<String, String> headers1 = new HashMap<String, String>();
				headers1.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
				myClass.makeGetRequest(RequestTags.TagGetMalls, "-1", headers1);
				mimvEenter.setVisibility(View.VISIBLE);
				msession.setMallsApiCallDate(CustomMethods.getPresentTime());
			}else{


				mimvEenter.setVisibility(View.VISIBLE);
				//myClass.makeGetRequest(RequestTags.TagGetGallery, mActiveplace,headers);
			}
		}

	}

	//Creating Pages with PageAdapter.
	public   class PageAdapter extends FragmentStatePagerAdapter
	{
		Activity context;
		ArrayList<SplashModel> pages ;

		public PageAdapter(FragmentManager fm,Activity context,ArrayList<SplashModel> pages) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.pages=pages;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position).getTitle();
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return new PageFragment(pages.get(position).getImage_url(), context);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return pages.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Object obj = super.instantiateItem(container, position);
			mPager.setObjectForPosition(obj, position);
			return obj;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if(object != null){
				return ((Fragment)object).getView() == view;
			}else{
				return false;
			}
		}


	}	

	@SuppressLint("ValidFragment")
	public  class PageFragment extends Fragment {
		String url="";
		Activity context;
		View view;
		ImageView splashImage;
		ProgressBar prog;
		TextView txtPlaceText;
		Animation anim;
		public PageFragment()
		{

		}
		public PageFragment(String url,Activity context)
		{
			this.url=url;
			this.context=context;
			anim=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim.setDuration(1200);
		}
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			view=inflater.inflate(R.layout.splashimagelayout, null);
			splashImage=(ImageView)view.findViewById(R.id.splImage);
			splashImage.setScaleType(ScaleType.CENTER_CROP);
			prog=(ProgressBar)view.findViewById(R.id.progressBar);
			txtPlaceText=(TextView)view.findViewById(R.id.txtPlaceText);
			txtPlaceText.setVisibility(View.GONE);
			prog.setVisibility(View.GONE);
			txtPlaceText.setText(getResources().getString(R.string.actionBarTitle));

			try
			{

				Picasso.with(mContext)
				.load(getResources().getString(R.string.photo_url)+url)
				//.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher).into(splashImage);
				InorbitLog.d("URL INSIED PAGER >> "+getResources().getString(R.string.photo_url)+url);

			}catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			}
			catch(Exception e){
				e.printStackTrace();
			}

			return view;
		}

	}

	void DeleteRecursive(File file) {


		if(file.isDirectory()){

			//directory is empty, then delete it
			if(file.list().length==0){

				file.delete();
				System.out.println("Directory is deleted : " 
						+ file.getAbsolutePath());

			}else{

				//list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					//construct the file structure
					File fileDelete = new File(file, temp);

					//recursive delete
					DeleteRecursive(fileDelete);
				}

				//check the directory again, if empty then delete it
				if(file.list().length==0){
					file.delete();
					System.out.println("Directory is deleted : " 
							+ file.getAbsolutePath());
				}
			}

		}else{
			//if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}

	}

	void showToast(String msg)
	{
		Toast.makeText(mActContext, msg, Toast.LENGTH_SHORT).show();
	}

	public class Broadcast extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			mimvLogoImage.setVisibility(View.GONE);
			String success="";
			String message="";

			try{
				if(intent!=null){
					success=intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")) {
						mimvEenter.setVisibility(View.VISIBLE);
						final ArrayList<SplashModel> splashModels = intent.getParcelableArrayListExtra("IMAGE_DATE");
						Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
						anim.setFillAfter(true);
						mImvAppSplash.startAnimation(anim);
						mImvAppSplash.setVisibility(View.GONE);

						/*PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), (Activity) context, splashModels);
						mPager.setAdapter(adapter);*/

						mHandler_anim = new Handler();
						mRunnable_anim = new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								try{

									if(!mRelLay_img.isShown()){
										mRelLay_img.setVisibility(View.VISIBLE);
										mRelLay_img.setBackgroundColor(getResources().getColor(Color.WHITE));
										mPager.setVisibility(View.GONE);
										manimFadeOut.setFillAfter(true);

									}

									try {

										Picasso.with(mContext).load(getResources().getString(R.string.photo_url)+splashModels.get(miImgCount).getImage_url())
										//.placeholder(R.drawable.ic_launcher)
										.error(R.drawable.ic_launcher)
										.into(mImvImg_flip);

									} catch(IllegalArgumentException illegalArg) {
										illegalArg.printStackTrace();
									} catch(Exception e) {
										e.printStackTrace();
									}



									mImvImg_flip.startAnimation(manimFadein);
									manimFadein.setFillAfter(true);
									manimFadein.setAnimationListener(new AnimationListener() {

										@Override
										public void onAnimationStart(Animation animation) {
											// TODO Auto-generated method stub
											Log.d("=====", "Inorbit Animation start" +miImgCount);
										}

										@Override
										public void onAnimationRepeat(Animation animation) {
											// TODO Auto-generated method stub

										}

										@Override
										public void onAnimationEnd(Animation animation) {
											// TODO Auto-generated method stub
											Log.d("=====", "Inorbit Animation end" +miImgCount);
											mboolPagerTouched = false;


											try {

												Picasso.with(mContext).load(getResources().getString(R.string.photo_url)+splashModels.get(miImgCount).getImage_url())
												//.placeholder(R.drawable.ic_launcher)
												.error(R.drawable.ic_launcher)
												.into(mImvImg_flip_1);

											} catch(IllegalArgumentException illegalArg){
												illegalArg.printStackTrace();
											}
											catch(Exception e){
												e.printStackTrace();
											}

											mPager.setCurrentItem(miImgCount);
											miImgCount++;
											if(miImgCount==splashModels.size()){
												miImgCount=0;
											}
										}
									});

									mHandler_anim.postDelayed(mRunnable_anim, 3000);
								}catch(Exception ex){
									ex.printStackTrace();
								}
							}

						};
						mHandler_anim.postDelayed(mRunnable_anim,1000);


						mPager.setOnPageChangeListener(new OnPageChangeListener() {

							@Override
							public void onPageSelected(int arg0) {
								// TODO Auto-generated method stub
								try{

									Picasso.with(mContext).load(getResources().getString(R.string.photo_url)+splashModels.get(arg0).getImage_url())
									.error(R.drawable.ic_launcher)
									.into(mImvImg_flip_1);
									miImgCount = arg0;
									Log.d("=====", "Inorbit ViewPager No : " +miImgCount);
								}catch(IllegalArgumentException illegalArg){
									illegalArg.printStackTrace();
								}
								catch(Exception e){
									e.printStackTrace();
								}

							}

							@Override
							public void onPageScrolled(int arg0, float arg1, int arg2) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onPageScrollStateChanged(int state) {
								// TODO Auto-generated method stub

							}
						});

					} else {
						message=intent.getStringExtra("MESSAGE");
						mimvEenter.setVisibility(View.VISIBLE);

					}
				}else{
					showToast(message);
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}



	/**
	 * Broadcast receiver for tha malls api.
	 * 
	 * @author Nitin
	 *
	 */
	public class AreaBroadcast extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			try{
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					String message = intent.getStringExtra("MESSAGE");
					if(success.equalsIgnoreCase("true")){

						/**
						 * make enter button visible
						 * 
						 */
						mimvEenter.setVisibility(View.VISIBLE);
						//open.performClick();
					}else{                     

						boolean volleyError = intent.getBooleanExtra("volleyError",false);

						if(volleyError){
							int code1 = intent.getIntExtra("CODE",0);
							String message1=intent.getStringExtra("MESSAGE");
							String errorMessage = intent.getStringExtra("errorMessage");
							String apiUrl = intent.getStringExtra("apiUrl");	
							if(code1==ConfigFile.ServerError || code1==ConfigFile.ParseError){
								EventTracker.reportException(code1+"", apiUrl+" - "+errorMessage, "SplashScreen");
							}
							Toast.makeText(context, message1,Toast.LENGTH_SHORT).show();

						}
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

	public class NetBroadcast extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			boolean netConnect = NetworkCheck.isNetConnected(mContext);
			if(netConnect){
				if(mDbutil.getAreaRowCount()==0){
					loadGallery();
				}
			}

		}

	}
	/*
	private void checkVersion(){

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));
		MyClass myClass = new MyClass(mActContext);
		if(NetworkCheck.isNetConnected(mContext)) {
			myClass.getStoreRequest(RequestTags.Tag_GetDataVersion, null, headers);
		}
	}
	 */
	void registerEvent(){ 
		EventTracker.logEvent(getResources().getString(R.string.splashScreenEnter), false);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		isActive = true;
		
		/** check from where app get open -  used in local notification*/
		checkWhereFromAppOpen();


		/** get users active mall id*/
		getActiveMallIdandPlaceParent();
		
		old_time = session.getLastVersionUpdateTime();
		
		if (System.currentTimeMillis()-old_time > 24*60*60*1000)
			versionCheck();
		
		if (NetworkCheck.isNetConnected(mContext)) {
			getBackupPushMsg();
		}
		
		// Check device for Play Services APK.	
		if (checkPlayServices()) {
			// If this check succeeds, proceed with normal processing.
			// Otherwise, prompt user to get valid Play Services APK.
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = msession.getRegistrationId();
			
			//if (1==1) {
				//registerInBackground();
				//Toast.makeText(mContext, "id not found- requesting for new id", Toast.LENGTH_SHORT).show();
			new RegisterMyInfo().execute("");
			/*}else{
				//Toast.makeText(mContext, "id exist", Toast.LENGTH_SHORT).show();
				callGalleryRequest();

				//call for the UDM_ID
				if(session.isLoggedInCustomer()){
					//call with the user_id
					String user_id = getCustomerUserId();
					callToGetUdmIdForCustomer(user_id,"0");
				}
				else{
					//call without user_id and is_admin=0
					callToGetUdmIdForCustomer("", "0");
				}

				if(session.isLoggedIn()){
					//showToast("Merchant Login Trying");
					String user_id_manager = getMerchantUserId();
					if(dbUtil.isManager()){
						//set is_amin = 2 for mall manager
						callToGetUdmIdForMerchant(user_id_manager, "2");
					}
					else{
						//set is_admin = 1 for store manager
						callToGetUdmIdForMerchant(user_id_manager, "1");
					}
				} else {
					//showToast("No Merchant loggedIn");
					dismissDialog();
					callGalleryRequest();
				}
			}*/
		} else {
			InorbitLog.d("No valid Google Play Services APK found.");
			//mDisplay.setText("No valid Google Play Services APK found.");
		}
		
		try{
			checkFirstTimeInstall();
			checkLastUpdate();
			checkLastUsedDate();
			checkLastUsedMonth();

			//IntentFilter filter 		= new IntentFilter(RequestTags.TagGetGallery);
			IntentFilter netWork_filter = new IntentFilter();
			netWork_filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
			//mBroadCastSplesh = new Broadcast();
			//mActContext.registerReceiver(mBroadCastSplesh, filter);
			IntentFilter dataVersion	= new IntentFilter(RequestTags.Tag_GetDataVersion);
			//mActContext.registerReceiver(mBroadCastDataVersion = new Broadcast(), dataVersion);
			if(NetworkCheck.isNetConnected(mContext)){

			}else{
				InorbitLog.d("Receiver Registered");
				mActContext.registerReceiver(mBroadCastNetwork = new NetBroadcast(), netWork_filter);
			}
			
			IntentFilter area_filter 	= new IntentFilter(RequestTags.TagGetMalls);
			mActContext.registerReceiver(mBroadcastArea = new AreaBroadcast(), area_filter);
			
			if(mEventMap.size()!=0) {
				Log.i("Request Code ", "Request Code "+mEventMap.toString());
			}
			if(mEventMap.size()!=0) {
				EventTracker.logEvent("App_LaunchLocalNotification",mEventMap );
			}
			
			//for udmid customer
			IntentFilter mCustomer	= new IntentFilter(RequestTags.TAG_GET_UDM_ID_CUSTOMER);
			mContext.registerReceiver(mUdmIdCustomer = new UdmIDCustomer(), mCustomer);

			//for udmId merchant
			IntentFilter mMerchant	= new IntentFilter(RequestTags.TAG_GET_UDM_ID_MERCHANT);
			mContext.registerReceiver(mUdmIdMerchant = new UdmIdMerchant(), mMerchant);
			
			IntentFilter versionCheck = new IntentFilter(RequestTags.AppVersionCheck);
			mContext.registerReceiver(versionCheckReceiver = new VersionCheckReceiver(), versionCheck);
			
			IntentFilter backUpPush = new IntentFilter(RequestTags.Back_Up_Push);
			mContext.registerReceiver(backUpMsg = new BackUpMsg(), backUpPush);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private class VersionCheckReceiver extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				Bundle bundle = intent.getExtras();
				if (bundle.getString("SUCCESS").equalsIgnoreCase("true")) {
					if (bundle.getString("FLAG").equalsIgnoreCase("yes")) {
						PackageInfo pInfo;
						try {
							pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
							int versionCode = pInfo.versionCode;
							int new_version = bundle.getInt("VERSION");
							if (new_version > 0) {
								if (versionCode < new_version) {
									String message = bundle.getString("MESSAGE");
									Intent marketIntent = null;
									try {
										marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" +
												"com.phonethics.inorbit"));
										EventTracker.logEvent(getResources().getString(R.string.app_push_update_app), true);
									} catch (android.content.ActivityNotFoundException anfe) {
										
									}
									PendingIntent pIntent = PendingIntent.getActivity(mContext, 0, marketIntent, 0);
									NotificationManager mNotificationManager = (NotificationManager) 
											mContext.getSystemService(Context.NOTIFICATION_SERVICE);
									 
									NotificationCompat.Builder mBuilder =  new NotificationCompat.Builder(mContext)
							        .setSmallIcon(R.drawable.ic_launcher)
							        .setAutoCancel(true)
							        .setContentTitle("Inorbit")
							        .setStyle(new NotificationCompat.BigTextStyle()
							        .bigText(message))
							        .setContentText(message);
							        
							        mBuilder.setContentIntent(pIntent);
							        mNotificationManager.notify(5, mBuilder.build());
							        session.setLastVersionUpdateTime();
								}
							}
						} catch (NameNotFoundException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
		try
		{
			if(mHandler_anim!=null){
				mHandler_anim.removeCallbacks(mRunnable_anim);
			}
			//			if(mBroadCastSplesh!=null){
			//				//mActContext.unregisterReceiver(mBroadCastSplesh);
			//			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		} finally {
			if(mBroadCastNetwork!=null){
				mActContext.unregisterReceiver(mBroadCastNetwork);
			}
			if(mBroadcastArea !=null){
				mActContext.unregisterReceiver(mBroadcastArea);
			}
			/*if(mBroadCastDataVersion != null) {
				mActContext.unregisterReceiver(mBroadCastDataVersion);
			}*/
			if(mUdmIdCustomer!=null){
				mActContext.unregisterReceiver(mUdmIdCustomer);
			}
			
			if(mUdmIdMerchant!=null){
				mActContext.unregisterReceiver(mUdmIdMerchant);
			}
			
			if (versionCheckReceiver != null) {
				mActContext.unregisterReceiver(versionCheckReceiver);
			}
			
			if (backUpMsg != null) {
				mActContext.unregisterReceiver(backUpMsg);
			}
		}
	}

	@Override
	protected void onPause() {
		isActive = false;
		try
		{
			EventTracker.endLocalyticsSession(getApplicationContext());
			if(mHandler_anim!=null){
				mHandler_anim.removeCallbacks(mRunnable_anim);
			}
			//			if(mBroadCastSplesh!=null){
			//				mActContext.unregisterReceiver(mBroadCastSplesh);
			//			}
			/*if(mBroadCastNetwork!=null){
				mActContext.unregisterReceiver(mBroadCastNetwork);
			}
			if(mBroadcastArea !=null){
				mActContext.unregisterReceiver(mBroadcastArea);
			}*/

			/*if(mBroadCastDataVersion != null) {
				mActContext.unregisterReceiver(mBroadCastDataVersion);
			}*/
		}catch(Exception ex) {
			ex.printStackTrace();
		}


		super.onPause();
	}

	/* on start */
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
		try{
			//IntentFilter filter 		= new IntentFilter(RequestTags.TagGetGallery);
			IntentFilter netWork_filter = new IntentFilter();
			netWork_filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
			//mActContext.registerReceiver(mBroadCastSplesh = new Broadcast(), filter);
			IntentFilter dataVersion	= new IntentFilter(RequestTags.Tag_GetDataVersion);
			//mActContext.registerReceiver(mBroadCastDataVersion = new Broadcast(), dataVersion);
			if(NetworkCheck.isNetConnected(mContext)){

			}else{
				InorbitLog.d("Receiver Registered");
				mActContext.registerReceiver(mBroadCastNetwork = new NetBroadcast(), netWork_filter);
			}
			
			/*IntentFilter area_filter 	= new IntentFilter(RequestTags.TagGetMalls);
			mActContext.registerReceiver(mBroadcastArea = new AreaBroadcast(), area_filter);*/

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				InorbitLog.d("This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}


	class RegisterMyInfo extends AsyncTask<String, Integer, String> {
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showProgressDialog();
		}
		
		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			String msg = "";
			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(mContext);
				}
				regid = gcm.register(SENDER_ID);
				msg = "Device registered, registration ID=" + regid;
				
				// You should send the registration ID to your server over HTTP,
				// so it can use GCM/HTTP or CCS to send messages to your app.
				// The request to your server should be authenticated if your app
				// is using accounts.
				//sendRegistrationIdToOurServer();

				// For this demo: we don't need to send it because the device
				// will send upstream messages to a server that echo back the
				// message using the 'from' address in the message.

				// Persist the regID - no need to register again.
				msession.storeRegistrationId(regid);
				
			} catch (IOException ex) {
				msg = "Error :" + ex.getMessage();
				// If there is an error, don't just keep trying to register.
				// Require the user to click a button again, or perform
				// exponential back-off.
			}
			return msg;
		}

     
		protected void onProgressUpdate(Integer... progress) {
			//setProgress(progress[0]);
		}

		@Override
		protected void onPostExecute(String msg) {
			//mDisplay.append(msg + "\n");
			//for checking the logged in user & managers at the very first time
			//call for the UDM_ID
			//showToast("First time opened");
			
			/*callGalleryRequest();

			//call for the UDM_ID
			if(session.isLoggedInCustomer()){
				//call with the user_id
				String user_id = getCustomerUserId();
				callToGetUdmIdForCustomer(user_id,"0");
			}
			else{
				//call without user_id and is_admin=0
				callToGetUdmIdForCustomer("", "0");
			}

			if(session.isLoggedIn()){
				//showToast("Merchant Login Trying");
				String user_id_manager = getMerchantUserId();
				if(dbUtil.isManager()){
					//set is_amin = 2 for mall manager
					callToGetUdmIdForMerchant(user_id_manager, "2");
				}
				else{
					//set is_admin = 1 for store manager
					callToGetUdmIdForMerchant(user_id_manager, "1");
				}
			} else {
				//showToast("No Merchant loggedIn");
				dismissDialog();
				callGalleryRequest();
			}*/
			
			
			
			
			if(session.isLoggedInCustomer()){
				//call with the user_id
				String user_id = getCustomerUserId();
				callToGetUdmIdForCustomer(user_id,"0");
			}
			else{
				//call without user_id and is_admin=0
				callToGetUdmIdForCustomer("", "0");
			}

			if(session.isLoggedIn()){
				//showToast("Merchant called after async");
				String user_id_manager = getMerchantUserId();
				if(dbUtil.isManager()){
					//set is_amin = 2 for mall manager
					callToGetUdmIdForMerchant(user_id_manager, "2");
				}
				else{
					//set is_admin = 1 for store manager
					callToGetUdmIdForMerchant(user_id_manager, "1");
				}
			} else {
				//showToast("No Merchant loggedIn");
				dismissDialog();
				callGalleryRequest();
			}
			//call for the same UDM_ID
			InorbitLog.d("Registration id "+msg);
		}
	}

	void checkFirstTimeInstall(){
		try{
			final String PREFS_NAME = "MyPrefsFile";
			SharedPreferences settings = mActContext.getSharedPreferences(PREFS_NAME, 0);
			if (settings.getBoolean("my_first_time", true)) {
				EventTracker.logEvent(getResources().getString(R.string.app_Installs), false);
				settings.edit().putBoolean("my_first_time", false).commit(); 
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	void checkLastUpdate(){
		SharedPreferences prefs =  mActContext.getSharedPreferences("verCode", 1);
		SharedPreferences.Editor editor = prefs.edit();
		int versionNew = 0;
		try{
			PackageInfo pInfoNew = getPackageManager().getPackageInfo(getPackageName(), 0);
			versionNew = pInfoNew.versionCode;
		}
		catch(Exception e){}

		int tempVer = prefs.getInt("verCode", -1);

		if(tempVer == -1){ //Check for first time install
			try{
				PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
				int version = pInfo.versionCode;
				editor.putInt("verCode", version);
				editor.commit();
			}
			catch(Exception e){}
		}

		if(versionNew != tempVer){
			EventTracker.logEvent(getResources().getString(R.string.app_Upgrades), false);
			editor.putInt("verCode", versionNew);
			editor.commit();
		}
	}

	void checkLastUsedDate(){
		try{
			String current_date="";
			String lastDate="";
			Date sysDate=new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("MMMM d yyyy");
			SharedPreferences prefs =  mActContext.getSharedPreferences("dateUsed", 1);
			SharedPreferences.Editor editor = prefs.edit();
			String tempDate = prefs.getString("dateUsed", "");
			if(tempDate.length()==0){
				current_date=formatter.format(sysDate);
				Log.i("First Time Stamp", "First Time Stamp "+current_date);
				editor.putString("dateUsed", current_date);
				editor.commit();
				EventTracker.logEvent("App_DailyEngagedUsers", false);
			} else {
				lastDate=prefs.getString("dateUsed", "");	
				try {
					Date date=formatter.parse(lastDate);
					if(date.before(formatter.parse(formatter.format(sysDate)))) {
						current_date=formatter.format(sysDate);
						editor.putString("dateUsed", current_date);
						editor.commit();
						EventTracker.logEvent(getResources().getString(R.string.app_DailyEngagedUsers), false);
						lastDate=prefs.getString("dateUsed", "");	
					}
				}catch(Exception ex) {
					ex.printStackTrace();
				}
			}

		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	void checkLastUsedMonth() {

		try {
			Calendar cal = Calendar.getInstance();
			int month = cal.get(Calendar.MONTH)+1;
			SharedPreferences prefs =  mActContext.getSharedPreferences("monthUsed", 1);
			SharedPreferences.Editor editor = prefs.edit();
			int tempMonth = prefs.getInt("monthUsed", -1);
			Log.i("MONTH", "MONTH "+month);
			if(tempMonth == -1) {
				editor.putInt("monthUsed", month);
				editor.commit();
				Log.i("MONTH", "MONTH FIRST TIME "+month);
				EventTracker.logEvent(getResources().getString(R.string.app_MonthlyEngagedUse), false);
			}
			Log.i("Date time","MONTH Temp Month "+prefs.getInt("monthUsed", -1));
			if(month != tempMonth) {
				editor.putInt("monthUsed", month);
				editor.commit();
				Log.i("MONTH", "MONTH CHANGED "+month);
				EventTracker.logEvent(getResources().getString(R.string.app_MonthlyEngagedUse), false);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		/*	if(v.getId()==mimvEenter.getId()) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				animate(mimvEenter).setInterpolator(sDecelerator).scaleX(0.7f).scaleY(0.7f);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				animate(mimvEenter).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
			}
			mVibrate.vibrate(50);
		}

		if(v.getId()==mRelLay_img.getId()) {
			try{
				manimFadein.setFillAfter(true);
				mRelLay_img.setVisibility(View.GONE);
				mPager.setVisibility(View.VISIBLE);
				if(mHandler_anim!=null){

					mHandler_anim.removeCallbacks(mRunnable_anim);
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

		if(v.getId()==mPager.getId()) {
			int action = event.getAction();
			switch (action) {
			case MotionEvent.ACTION_DOWN:
				mboolPagerTouched = true;
				if(mHandler_anim!=null){
					mHandler_anim.removeCallbacks(mRunnable_anim);
				}
				break;
			case MotionEvent.ACTION_UP:
				if(mHandler_anim!=null){
					mHandler_anim.postDelayed(mRunnable_anim, 2000);
				}
				break;
			case MotionEvent.ACTION_MOVE:
				break;

			default:
				break;
			}
		}*/
		return false;
	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null && isActive){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null && isActive) {
			if(mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
			}
		}
	}

	//for Merchant UserId
	private String getMerchantUserId(){
		HashMap<String,String>user_details=session.getUserDetails();
		return user_details.get(SessionManager.KEY_USER_ID).toString();

	}


	//for Customer UserId
	private String getCustomerUserId(){
		HashMap<String,String>user_details=session.getUserDetailsCustomer();
		return user_details.get(SessionManager.KEY_USER_ID_CUSTOMER).toString();
	}

	//method to get UDM_ID
	private void callToGetUdmIdForCustomer(String user_id, String isAdmin) {
		// TODO Auto-generated method stub
		//showToast("Customer Called");
		showProgressDialog();
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		JSONObject jsonObj = new JSONObject();
		//showToast("RegId "+regid+" userid "+user_id+" isAdmin "+isAdmin);
		try {
			jsonObj.put("device_id", regid);
			jsonObj.put("device_os", "Android");
			jsonObj.put("user_id", user_id);
			jsonObj.put("is_admin", isAdmin);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		InorbitLog.d("JSON " + jsonObj.toString());

		MyClass myclass = new MyClass(mContext);
		myclass.postRequest(RequestTags.TAG_GET_UDM_ID_CUSTOMER, headers, jsonObj);
	}

	//method to get UDM_ID for Merchant
	private void callToGetUdmIdForMerchant(String user_id, String isAdmin) {
		// TODO Auto-generated method stub
		//showToast("Merchant Called");'
		showProgressDialog();
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put(getResources().getString(R.string.api_header), getResources().getString(R.string.api_value));

		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("device_id", regid);
			jsonObj.put("device_os", "Android");
			jsonObj.put("user_id", user_id);
			jsonObj.put("is_admin", isAdmin);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		InorbitLog.d("JSON " + jsonObj.toString());

		MyClass myclass = new MyClass(mContext);
		myclass.postRequest(RequestTags.TAG_GET_UDM_ID_MERCHANT, headers, jsonObj);
	}

	class UdmIDCustomer extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						String udm_id_customer = intent.getStringExtra("UDMID");
						InorbitLog.d("ID "+ udm_id_customer);
						session.setUdmIdForCustomer(udm_id_customer);
						//showToast(udm_id_customer);
					}
					else{
						String message = intent.getStringExtra("MESSAGE");
						showToast(message);
					}
					//showToast("Customer done");
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				//showToast("Customer error");
			} finally {
				dismissDialog();
			}
		}

	}

	class UdmIdMerchant extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {
				if(intent!=null){
					String success = intent.getStringExtra("SUCCESS");
					if(success.equalsIgnoreCase("true")){
						String udm_id_merchant = intent.getStringExtra("UDMID");
						//showToast("merchant " + udm_id_merchant);
						InorbitLog.d("ID "+ udm_id_merchant);
						session.setUdmIdForMerchant(udm_id_merchant);
					}
					else{
						String message = intent.getStringExtra("MESSAGE");
						showToast(message);
					}
					//showToast("Merchant done");
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				//showToast("Merchant error");
			} finally {
				dismissDialog();
				callGalleryRequest();
			}
		}

	}
}

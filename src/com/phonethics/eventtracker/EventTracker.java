package com.phonethics.eventtracker;

import java.util.Map;

import com.facebook.AppEventsLogger;
import com.flurry.android.FlurryAgent;
import com.phonethics.inorbit.R;

import android.content.Context;



public class EventTracker { 

	public static void startLocalyticsSession(Context context){

		String keyL = context.getResources().getString(R.string.localactics_id);

		//facebook app activation event
		//this function is called here as it needs to be called from onResume() of every activity.

		AppEventsLogger.activateApp(context, context.getString(R.string.fb_appid));
		startFlurrySession(context);
	}

	public static void endLocalyticsSession(Context context){
		endFlurrySession(context);
	}

	public static void logEvent(String event, boolean isScreen){
		FlurryAgent.logEvent(event);
	}

	public static void logEvent(String event, Map<String, String> param) {
		FlurryAgent.logEvent(event, param);
	}

	public static void startFlurrySession(Context context){
		String keyF = context.getResources().getString(R.string.flurryKey);
		FlurryAgent.onStartSession(context, keyF);
	}

	public static void endFlurrySession(Context context){
		FlurryAgent.onEndSession(context);
	}

	public static void reportException(String errorId, String message, String errorClass){
		try
		{
			FlurryAgent.onError(errorId, message, errorClass);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	
}

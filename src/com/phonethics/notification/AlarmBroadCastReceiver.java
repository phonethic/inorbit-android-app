package com.phonethics.notification;

import java.util.Date;
import java.text.SimpleDateFormat;

import com.phonethics.inorbit.R;
import com.phonethics.inorbit.SessionManager;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * This class Receives fired alarm and check 
 * for Alarm code and based on that creates notification and fires it.
 * @author manoj
 *
 */
public class AlarmBroadCastReceiver extends BroadcastReceiver {


	private long mlDiffSeconds;
	private long mlDiffMinutes;
	private long mlDiffHours;
	private long mlDiffDays;

	private String NOTIFICATION_ACTION="com.phonethics.notification.NOTIFICATION_ACTION";

	private SessionManager mSession;
	
	private int miRequestCode=0;
	private LocalNotification mLocalNotification;
	public void onReceive(Context context, Intent intent2) {
		try
		{
			mSession=new SessionManager(context);
			Bundle bundle = intent2.getExtras();

			mLocalNotification=new LocalNotification(context);
			Log.i("Notification Broadcast", "Notification Broadcast fired on "+bundle.getString("dateCreated") +" "+bundle.getInt("requestCode"));

			if(bundle!=null){
				miRequestCode=bundle.getInt("requestCode");
			}

			checkDateDiff(bundle.getString("dateCreated"));
			//Merchant Logged in
			if(mSession.isLoggedIn()){

				if(miRequestCode==Integer.parseInt(context.getString(R.string.Edit_Store_alert))){
					createNotification(context.getString(R.string.myplaceMessage),context);
				}

				if(miRequestCode==Integer.parseInt(context.getString(R.string.Talk_Now_alert))){
					createNotification(context.getString(R.string.talkNowMessage),context);
				}

			}
			else{
				//customer logged in
				if(mSession.isLoggedInCustomer()){
					if(miRequestCode==Integer.parseInt(context.getString(R.string.Favourite_alert))){
						createNotification(context.getString(R.string.customerFav),context);
					}

					if(miRequestCode==Integer.parseInt(context.getString(R.string.Customer_Profile_alert))){
						createNotification(context.getString(R.string.customerProfile),context);
					}

				}else{
					if(miRequestCode==Integer.parseInt(context.getString(R.string.Login_alert))){
						createNotification(context.getString(R.string.customerNotLoggedIn),context);
					}

				}

				if(miRequestCode==Integer.parseInt(context.getString(R.string.App_Launch))){
					createNotification(context.getString(R.string.appNotLaunched),context);
				}

			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * @param message Message that needs to be displayed in notification
	 * @param context
	 */
	void createNotification(String message,Context context)
	{
		try
		{

			RemoteViews myRemote=new RemoteViews(context.getPackageName(), R.layout.notification);
			myRemote.setTextViewText(R.id.message,message);

			//Notification Action
			Intent intent = new Intent(NOTIFICATION_ACTION);
			intent.putExtra("requestCode", miRequestCode);

			//Pending Intent 
			PendingIntent pIntent = PendingIntent.getBroadcast(context, miRequestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

			//Notification Builder
			NotificationCompat.Builder mBuilder=new NotificationCompat.Builder(context);
			mBuilder.setContentTitle("Inorbit");
			mBuilder.setContentText("Place Details ");
			mBuilder.setAutoCancel(true);
			mBuilder.setSmallIcon(R.drawable.ic_launcher);
			mBuilder.setWhen(System.currentTimeMillis());
			mBuilder.setContent(myRemote);
			mBuilder.setContentIntent(pIntent);


			mBuilder.setSound(RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_NOTIFICATION));
			NotificationManager  notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.notify(miRequestCode, mBuilder.build());

			//Setting Alarm Pref
			mLocalNotification.setAlarmFired(miRequestCode, true);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}

	/**
	 * 
	 * @param alarmDate Date at which alarm is fired
	 */
	public void checkDateDiff(String alarmDate)
	{
		try
		{
			//Getting Date time stamp of last saved area
			/*DateFormat format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");*/
			SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");

			Date prevDate=format.parse(alarmDate);
			Date currentDate=format.parse(format.format(new Date()));

			//in milliseconds
			long diff = currentDate.getTime() - prevDate.getTime();

			mlDiffSeconds = diff / 1000 % 60;
			mlDiffMinutes = diff / (60 * 1000) % 60;
			mlDiffHours = diff / (60 * 60 * 1000) % 24;
			mlDiffDays = diff / (24 * 60 * 60 * 1000);

			Log.i("DATE TIME DIFF", "Notification Broadcast fired on DIFF DAYS "+mlDiffDays);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/**
	 * This method cancels alarm based on request code
	 * @param requestCode  
	 * @param context
	 */
	void cancelAlarm(int requestCode,Context context)
	{
		Intent intent = new Intent();
		PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender!=null){
			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender);
		}
	}

}

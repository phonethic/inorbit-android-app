package com.phonethics.notification;

import java.util.List;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.phonethics.inorbit.SplashScreen;

/**
 * Whenever notification is being clicked this class gets called
 *  
 * @author manoj
 *
 */
public class NotificationReceiver extends BroadcastReceiver {

	private int miRequestCode=0;
	public void onReceive(Context context, Intent intent) {
		try{


			Bundle bundle = intent.getExtras();
			if(bundle!=null){
				miRequestCode=bundle.getInt("requestCode");
				cancelAlarm(miRequestCode, context);
			}

			Intent launch = new Intent(Intent.ACTION_MAIN);
			launch.putExtra("requestCode", miRequestCode);
			launch.setClass(context, SplashScreen.class);
			launch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			boolean isShoplocalRunning = isRunning(context);

			if(!isShoplocalRunning)
				context.startActivity(launch);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * @param ctx
	 * @return true if the app is running or else false 
	 */
	public boolean isRunning(Context ctx) {
		ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

		for (RunningTaskInfo task : tasks) {
			if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
				return true;   
		}
		return false;
	}


	/**
	 * 
	 * @param requestCode Unique code of alarm that needs to be canceled.
	 * @param context
	 */
	void cancelAlarm(int requestCode,Context context){

		String ALARM_ACTION="com.phonethics.localnotification.ALARM_ACTION";
		Intent intentToFire= new Intent(ALARM_ACTION);
		intentToFire.putExtra("requestCode",0);
		intentToFire.putExtra("dateCreated","");
		PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender!=null){
			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender);
		}
	}

	/**
	 * 
	 * @param processName The name of the process that needs to be checked whether its running or not
	 * @param context
	 * @return true if process is running else false
	 */
	//	boolean isNamedProcessRunning(String processName,Context context){
	//		if (processName == null) return false;
	//
	//		ActivityManager manager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
	//		List<RunningAppProcessInfo> processes = manager.getRunningAppProcesses();
	//
	//		for (RunningAppProcessInfo process : processes)
	//		{
	//			if (processName.equals(process.processName)){
	//				return true;
	//			}
	//		}
	//		return false;
	//	}


}

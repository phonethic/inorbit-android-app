package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class DeleteSpecialPostResultReceiver extends ResultReceiver {
	
	DeleteSpecialPostReceiver rec;

	public DeleteSpecialPostResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.onDeleteSpecialPost(resultCode, resultData);
		}
	}
	
	public interface DeleteSpecialPostReceiver
	{
		public void onDeleteSpecialPost(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(DeleteSpecialPostReceiver rec)
	{
		this.rec = rec;
	}

}

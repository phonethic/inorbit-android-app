package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.phonethics.inorbit.SpecialPostModel;
import com.phonethics.model.SpecialPost;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class MSpecialPostService extends IntentService{
	
	ResultReceiver getMSpecialPost;
	
	BufferedReader bufferedReader = null;
	String status = "";
	
	String getMSpecialPostStatus;
	
	ArrayList<SpecialPost> getAllSpecialPosts=new ArrayList<SpecialPost>();
	
	String message = "";
	
	public MSpecialPostService()
	{
		super("MSpecialPostService");
	}

	public MSpecialPostService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		
		
		getMSpecialPost = intent.getParcelableExtra("mSpecialPosts");
		
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
	
		
		
		try {
			
			
			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to Get data.
			HttpGet httpGet=new HttpGet(URL);
			Log.d("", "inorbit url inside service >> "+URL);
			

			//Adding header.
			httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
			httpGet.addHeader("user_id",user_id);
			httpGet.addHeader("auth_id",auth_id);

			Log.i("URL", "URL Special: "+URL);
			

			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service MSpecial Post: "+stringBuffer.toString());
			status=stringBuffer.toString();
			
			JSONObject jsonobject=new JSONObject(status);
			
			getMSpecialPostStatus = jsonobject.getString("success");
			
			if(getMSpecialPostStatus.equalsIgnoreCase("true")){
				
				JSONArray data = jsonobject.getJSONArray("data");
				
				for(int i=0;i<data.length();i++){
					
					JSONObject item = data.getJSONObject(i);

					SpecialPost posts = new SpecialPost();
					
					posts.setId(item.getString("id"));
					posts.setPlace_id(item.getString("place_id"));
					posts.setPost_type(item.getString("post_type"));
					//posts.setDate_type(item.getString("date_type"));
					posts.setTitle(item.getString("title"));
					posts.setDescription(item.getString("description"));
					posts.setImage_title1(item.getString("image_title1"));
					posts.setImage_title2(item.getString("image_title2"));
					posts.setImage_title3(item.getString("image_title3"));
					posts.setImage_url1(item.getString("image_url1"));
					posts.setImage_url2(item.getString("image_url2"));
					posts.setImage_url3(item.getString("image_url3"));
					posts.setThumb_url1(item.getString("thumb_url1"));
					posts.setThumb_url2(item.getString("thumb_url2"));
					posts.setThumb_url2(item.getString("thumb_url3"));
					//posts.setSpecial_date(item.getString("special_date"));
					posts.setOffer_date_time(item.getString("offer_date_time"));
					posts.setCreated_at	(item.getString("created_at"));

					getAllSpecialPosts.add(posts);

					Log.d("TITLE","TITLE " + item.getString("title"));
					
				}
				
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", getMSpecialPostStatus);
				b.putParcelableArrayList("specialboradcast",getAllSpecialPosts);
				getMSpecialPost.send(0, b);
				
			}
			else{
				

				Bundle b=new Bundle();
				
				String SEARCH_MESSAGE="";
				
				SEARCH_MESSAGE = jsonobject.getString("message");
				
				b.putString("SEARCH_STATUS", getMSpecialPostStatus);
				b.putString("SEARCH_MESSAGE", SEARCH_MESSAGE);
				getMSpecialPost.send(0, b);
			}

			
		} 
		catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			try {
				/*	String SEARCH_MESSAGE=jsonobject.getString("message");*/
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "false");
				b.putString("SEARCH_MESSAGE", "Connection Timed out.");
				b.putParcelableArrayList("boradcast",getAllSpecialPosts);
				getMSpecialPost.send(0, b);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}




		}catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			try {
				/*String SEARCH_MESSAGE=jsonobject.getString("message");*/
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "false");
				b.putString("SEARCH_MESSAGE", "Connection Timed out.");
				b.putParcelableArrayList("boradcast",getAllSpecialPosts);
				getMSpecialPost.send(0, b);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
			Bundle b=new Bundle();
			b.putString("SEARCH_STATUS", "false");
			b.putString("SEARCH_MESSAGE", "Something went wrong.");
			//b.putParcelableArrayList("boradcast",getAllSpecialPosts);
			getMSpecialPost.send(0, b);
		}
	
				
	}

}

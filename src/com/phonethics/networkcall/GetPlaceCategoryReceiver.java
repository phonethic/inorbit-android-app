package com.phonethics.networkcall;

import com.phonethics.inorbit.SearchActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetPlaceCategoryReceiver extends ResultReceiver {

	SearchActivity categ;
	
	public GetPlaceCategoryReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(categ!=null)
		{
			categ.onReceiveStoreCategories(resultCode, resultData);
		}
	}
	
	
	

	@Override
	public void send(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		super.send(resultCode, resultData);
	}




	/*public interface GetCategory{
		public void onReceiveStoreCategories(int resultCode, Bundle resultData);
	}*/
	
	public void setReceiver(SearchActivity categ){
		this.categ=categ;
	}
}

package com.phonethics.networkcall;

import com.phonethics.networkcall.GetSpecialDatesResultReceiver.GetSpecialDate;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class MSpecialPostResultReceiver extends ResultReceiver{

	MSpecialPost result;

	public MSpecialPostResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		if(result!=null){

			result.onMSpecialPost(resultCode, resultData);
		}
	}


	public interface MSpecialPost{

		public void onMSpecialPost(int resultCode, Bundle resultData);
	}

	public void setReciver(MSpecialPost result)
	{
		this.result=result;
	}
}

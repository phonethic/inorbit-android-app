package com.phonethics.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.phonethics.inorbit.InorbitLog;
import com.phonethics.inorbit.R;
import com.phonethics.model.MessageModel;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GetMessageAdapter extends ArrayAdapter<MessageModel> {

	private List<MessageModel> messages;
	private LayoutInflater layoutInflater;
	private String msgReaderId;
	private Context context;
	private boolean isReaderLogo;
	
	public GetMessageAdapter(Context context, List<MessageModel> objects, String msgReaderId, boolean isReaderLogo) {
		super(context, R.layout.chat_layout, objects);
		
		this.context = context;
		messages = objects;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.msgReaderId = msgReaderId; 
		this.isReaderLogo = isReaderLogo;
		InorbitLog.d(" "+msgReaderId+" "+objects.get(0).getFromid()+" ");
	}

	@Override
	public int getCount() {
		return messages.size();
	}
	
	@Override
	public MessageModel getItem(int position) {
		return messages.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		MessageModel message = messages.get(position);
		ViewHolder viewholder = new ViewHolder();
		
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.chat_layout, null);
			viewholder.sender = (LinearLayout) convertView.findViewById(R.id.sender);
			viewholder.sender_img = (ImageView) convertView.findViewById(R.id.sender_image);
			viewholder.sender_data = (RelativeLayout) convertView.findViewById(R.id.sender_data);
			viewholder.sender_msg = (TextView) convertView.findViewById(R.id.sender_msg);
			viewholder.sender_datetime = (TextView) convertView.findViewById(R.id.sender_datetime);
			
			viewholder.receiver = (LinearLayout) convertView.findViewById(R.id.receiver);
			viewholder.receiver_img = (ImageView) convertView.findViewById(R.id.receiver_image);
			viewholder.receiver_data = (RelativeLayout) convertView.findViewById(R.id.receiver_data);
			viewholder.receiver_msg = (TextView) convertView.findViewById(R.id.receiver_msg);
			viewholder.receiver_datetime = (TextView) convertView.findViewById(R.id.receiver_datetime);
			
			convertView.setTag(viewholder);
		} else {
			viewholder = (ViewHolder) convertView.getTag();
		}
		
		
		if (msgReaderId.equalsIgnoreCase(message.getFromid())) {
			//Right Layout
			viewholder.sender.setVisibility(View.GONE);
			viewholder.receiver.setVisibility(View.VISIBLE);
			
			String img_source = message.getFromImageUrl().replaceAll(" ", "%20");
			if (img_source.trim().length() != 0 ) {
				try {
					Picasso.with(context).load(img_source)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(viewholder.receiver_img);
				} catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				} catch(Exception e){
					e.printStackTrace();
				}
			} else {
				viewholder.receiver_img.setImageResource(R.drawable.ic_launcher);
			}
			
			String msOldFormat="yyyy-MM-dd HH:mm:ss";
			String msNewFormat = "dd MMM yyyy  h:mm a";
			String msNewDate = "";

			try{
				SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
				Date d = sdf.parse(message.getDatetime());
				sdf.applyPattern(msNewFormat);
				msNewDate = sdf.format(d);
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
			/*mTimeText.setText("Time: " + dateFormat.format(date));
			Date dt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(message.getDatetime());*/
			
			
			viewholder.receiver_msg.setText(message.getMsg());
			viewholder.receiver_datetime.setText(msNewDate);
			
		} else {
			//Left Layout
			viewholder.sender.setVisibility(View.VISIBLE);
			viewholder.receiver.setVisibility(View.GONE);
			
			String img_source = message.getToImageUrl().replaceAll(" ", "%20");
			if (img_source.trim().length() != 0) {
				try {
					Picasso.with(context).load(img_source)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(viewholder.sender_img);
				} catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				} catch(Exception e){
					e.printStackTrace();
				}
			} else {
				viewholder.sender_img.setImageResource(R.drawable.ic_launcher);
			}
			
			String msOldFormat="yyyy-MM-dd H:mm:ss";
			String msNewFormat = "dd MMM yyyy  h:mm a";
			String msNewDate = "";
			
			try{
				SimpleDateFormat sdf = new SimpleDateFormat(msOldFormat);
				Date d = sdf.parse(message.getDatetime());
				sdf.applyPattern(msNewFormat);
				msNewDate = sdf.format(d);
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
			viewholder.sender_msg.setText(message.getMsg());
			viewholder.sender_datetime.setText(msNewDate);
			
		}
		return convertView;
	}
	
	
	private class ViewHolder {
		private ImageView sender_img;
		private TextView sender_msg;
		private TextView sender_datetime;
		private LinearLayout sender;
		private RelativeLayout sender_data;
		
		private ImageView receiver_img;
		private TextView receiver_msg;
		private TextView receiver_datetime;
		private LinearLayout receiver;
		private RelativeLayout receiver_data;		
	}
}

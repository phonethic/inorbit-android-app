package com.phonethics.adapters;

import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

import com.phonethics.inorbit.DBUtil;
import com.phonethics.inorbit.InorbitApp;
import com.phonethics.inorbit.R;
import com.phonethics.inorbit.SessionManager;
import com.phonethics.model.StoreInfo;
import com.squareup.picasso.Picasso;

public class StoreAdapter extends ArrayAdapter<StoreInfo> {

	ArrayList<StoreInfo> 	malStoreInfos;
	Context 				mContext;
	LayoutInflater 			mInflate;
	String 					isFav="";
	String					msPhotoParentUrl;
	DBUtil					mDbutil;

	public StoreAdapter(Context context, int resource) {
		super(context, resource);
		// TODO Auto-generated constructor stub
	}

	public StoreAdapter(Context context, ArrayList<StoreInfo> storeInfos) {
		super(context, R.layout.layout_search_api);
		// TODO Auto-generated constructor stub

		this.mContext 		= context;
		this.malStoreInfos 	= storeInfos;
		mInflate			=((Activity) context).getLayoutInflater();
		msPhotoParentUrl 	= context.getResources().getString(R.string.photo_url);
		mDbutil				= new DBUtil(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return malStoreInfos.size();
	}


	@Override
	public StoreInfo getItem(int position) {
		// TODO Auto-generated method stub
		return malStoreInfos.get(position);
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=mInflate.inflate(R.layout.layout_default_search_api,null);
			holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
			holder.imgStoreLogo.setScaleType(ScaleType.FIT_CENTER);
			holder.txtStore=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
			holder.txtDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
			holder.imgCall=(ImageView)convertView.findViewById(R.id.searchCall);
			holder.imgFav=(ImageView)convertView.findViewById(R.id.searchFav);
			holder.linearCall=(RelativeLayout)convertView.findViewById(R.id.linearCall);
			holder.txtBackOffers=(TextView)convertView.findViewById(R.id.txtBackOffers);
			holder.img_Star=(ImageView)convertView.findViewById(R.id.img_Star);
			holder.imgRemoveFav=(ImageView)convertView.findViewById(R.id.searchRemoveFav);
			holder.txtFav=(TextView)convertView.findViewById(R.id.txtFav);
			holder.searchFront=(RelativeLayout)convertView.findViewById(R.id.searchFront);
			holder.searchBack=(RelativeLayout)convertView.findViewById(R.id.searchBack);
			holder.band=(TextView)convertView.findViewById(R.id.band);
			holder.textBookticket=(TextView)convertView.findViewById(R.id.textBookticket);

			holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
			holder.txtSearchStoreTotalLike=(TextView)convertView.findViewById(R.id.txtSearchStoreTotalLike);

			holder.txtSearchStoreDesc.setTypeface(InorbitApp.getTypeFace());
			holder.txtStore.setTypeface(InorbitApp.getTypeFaceTitle());
			holder.txtBackOffers.setTypeface(InorbitApp.getTypeFace());
			holder.txtFav.setTypeface(InorbitApp.getTypeFace());
			holder.txtSearchStoreDesc.setTypeface(InorbitApp.getTypeFace());
			holder.txtSearchStoreTotalLike.setTypeface(InorbitApp.getTypeFace());
			holder.band.setTypeface(InorbitApp.getTypeFace());
			holder.band.setTextColor(Color.BLACK);

			convertView.setTag(holder);

		}
		final ViewHolder hold=(ViewHolder)convertView.getTag();

		//Added Temp
		hold.searchBack.setVisibility(View.GONE);
		hold.imgFav.setVisibility(View.GONE);

		//
		//load store logos.

		if(getItem(position).getImage_url().toString().equalsIgnoreCase("")){

			hold.imgStoreLogo.setImageResource(R.drawable.ic_launcher);
		} else {
			String photo_source=getItem(position).getImage_url().toString().replaceAll(" ", "%20");
			try {

				Picasso.with(mContext).load(msPhotoParentUrl+photo_source)
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_launcher)
				.into(hold.imgStoreLogo);

			} catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			} catch(Exception e){
				e.printStackTrace();
			}


		}

		//set Total Like
		try{
			
			if(getItem(position).getTotal_like().equalsIgnoreCase("0")){
				hold.txtSearchStoreTotalLike.setVisibility(View.GONE);
			} else {
				hold.txtSearchStoreTotalLike.setVisibility(View.VISIBLE);
			}
			hold.txtSearchStoreTotalLike.setText(getItem(position).getTotal_like());
			

		} catch(Exception ex){
			ex.printStackTrace();
		}

		//set store name.
		hold.txtStore.setText(getItem(position).getName().toString());

		hold.txtSearchStoreDesc.setText(getItem(position).getDescription());

		if(getItem(position).getName().toString().equalsIgnoreCase("Inox") || getItem(position).getName().toString().equalsIgnoreCase("Cinemax") || getItem(position).getName().toString().equalsIgnoreCase("Cinepolis")) {
			hold.textBookticket.setText("Book Ticket");
			hold.textBookticket.setVisibility(View.VISIBLE);
			hold.txtSearchStoreTotalLike.setBackgroundColor(mContext.getResources().getColor(R.color.inorbit_yellow));
			hold.textBookticket.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					String url="";
					String activeAreaId = mDbutil.getActiveMallId();
					String place_id = mDbutil.getMallPlaceParentByMallID(activeAreaId);
					String areaName = mDbutil.getAreaNameByInorbitId(activeAreaId);
					
					createInhouseAnalyticsEvent(getItem(position).getId(), "6", activeAreaId);
					
					if(areaName.equalsIgnoreCase("Malad")){
						url = mContext.getResources().getString(R.string.inox_malad);
					}else if(areaName.equalsIgnoreCase("Pune")){
						url = mContext.getResources().getString(R.string.inox_pune);
					}else if(areaName.equalsIgnoreCase("Cyberabad")){
						url = mContext.getResources().getString(R.string.inox_cyberabad);
					} else if (areaName.equalsIgnoreCase("Vadodara")) {
						url = mContext.getResources().getString(R.string.inox_vadodara);
					}

					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					mContext.startActivity(browserIntent);

					/*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getResources().getString(R.string.inox_malad)));
					context.startActivity(browserIntent);*/
				}
			});
		}else{
			hold.textBookticket.setVisibility(View.GONE);
			hold.txtSearchStoreTotalLike.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
		}


		hold.txtDistance.setVisibility(View.GONE);

		isFav=mDbutil.getGeoFavorite(Integer.parseInt(getItem(position).getId()));

		hold.band.setVisibility(View.VISIBLE);
		if(getItem(position).getHas_offer().toString().length()!=0 && getItem(position).getHas_offer().equalsIgnoreCase("1"))
		{
			hold.txtBackOffers.setText(getItem(position).getTitle());
			hold.band.setText("Special Offer : "+getItem(position).getTitle());

		}else{
			hold.txtBackOffers.setText("offers");
			hold.band.setVisibility(View.GONE);

		}



		return convertView;
	}
	
	private void createInhouseAnalyticsEvent(String flag, String activityId, String mallId) {
		Uri uri = new Uri.Builder().scheme("content").authority(mContext.getResources().getString(R.string.authority)).
				appendPath("/insert").build();
        ContentValues cv = new ContentValues(6);
		cv.put("UDM_ID", new SessionManager(mContext).getUdmIDForCustomer());
		cv.put("DATE", new Date(System.currentTimeMillis()).toString());
		cv.put("FLAG", flag);
		cv.put("ACTIVITY_ID", activityId);
		cv.put("MALL_ID", mallId);
		cv.put("SYNC_STATUS", -1);
		mContext.getContentResolver().insert(uri, cv);
	}
	
	
	class ViewHolder{
		ImageView 			imgStoreLogo,imgCall,imgFav,img_Star,imgRemoveFav;
		TextView 			txtStore,band,txtSearchStoreDesc,textBookticket;
		TextView 			txtDistance,txtBackOffers,txtFav;
		TextView 			txtSearchStoreTotalLike;
		RelativeLayout 		linearCall;
		RelativeLayout 		searchFront,searchBack;
	}



}

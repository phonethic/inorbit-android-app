package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.inorbit.InorbitApp;
import com.phonethics.inorbit.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class ActionAdapter extends ArrayAdapter<String> implements SpinnerAdapter {


	ArrayList<String>	mArArea;
	Activity 			mContext;
	LayoutInflater 		mLayoutInflater;
	public ActionAdapter(Activity context, int resource,int textViewResourceId, ArrayList<String> name) {
		super(context, resource, textViewResourceId, name);
		// TODO Auto-generated constructor stub
		this.mContext	= context;
		this.mArArea	= name;
		mLayoutInflater	= context.getLayoutInflater();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mArArea.size();
	}


	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return mArArea.get(position);
	}

	@Override
	public View getDropDownView(int position, View convertView,
			ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.list_menus, parent, false);
		}


		TextView txtMenu = (TextView) convertView.findViewById(R.id.listText);
		txtMenu.setTypeface(InorbitApp.getTypeFace());
		txtMenu.setText(""+getItem(position));

		return convertView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.list_dropdown_item, parent, false);
		}


		TextView txtMenu = (TextView) convertView.findViewById(R.id.txtSpinner);
		txtMenu.setTypeface(InorbitApp.getTypeFace());
		txtMenu.setText(""+getItem(position));
		return convertView;
	}




}

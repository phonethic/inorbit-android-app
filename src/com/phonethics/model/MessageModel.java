package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MessageModel implements Parcelable {
	
	private String fromId;
	private String toId;
	private String fromImageUrl;
	private String toImageUrl;
	private String msg;
	private String datetime;
	
	public MessageModel(String fromId, String toId, String fromImageUrl, String toImageUrl, String msg, String datetime) {
		this.fromId = fromId;
		this.toId = toId;
		this.fromImageUrl = fromImageUrl;
		this.toImageUrl = toImageUrl;
		this.msg = msg;
		this.datetime = datetime;
	}
	
	public String getFromid() {
		return fromId;
	}

	public void setFromid(String fromid) {
		this.fromId = fromid;
	}

	public String getToid() {
		return toId;
	}

	public void setToid(String toid) {
		this.toId = toid;
	}

	public String getFromImageUrl() {
		return fromImageUrl;
	}

	public void setFromImageUrl(String fromImageUrl) {
		this.fromImageUrl = fromImageUrl;
	}
	
	public String getToImageUrl() {
		return toImageUrl;
	}
	
	public void setToImageUrl(String toImageUrl){
		this.toImageUrl = toImageUrl;
	}
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		
		dest.writeString(fromId);
		dest.writeString(toId);
		dest.writeString(fromImageUrl);
		dest.writeString(toImageUrl);
		dest.writeString(msg);
		dest.writeString(datetime);
		
	}

}

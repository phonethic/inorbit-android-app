package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TipsModel implements Parcelable {
	private String tipId;
	private String tip;
	private String udmId;
	private String time;
	private String status;
	private String name;	
	private String storeName;
	private String mallName;
	
	public String getTipId() {
		return tipId;
	}

	public void setTipId(String tipId) {
		this.tipId = tipId;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public String getUdmId() {
		return udmId;
	}

	public void setUdmId(String udmId) {
		this.udmId = udmId;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public TipsModel() {
		
	}
	
	public TipsModel(Parcel source){
		tipId = source.readString();
		tip = source.readString();
		udmId = source.readString();
		time = source.readString();
		status = source.readString();
		name = source.readString();
		storeName = source.readString();
		mallName = source.readString();
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(tipId);
		dest.writeString(tip);
		dest.writeString(udmId);
		dest.writeString(time);
		dest.writeString(status);
		dest.writeString(name);
		dest.writeString(storeName);
		dest.writeString(mallName);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getMallName() {
		return mallName;
	}

	public void setMallName(String mallName) {
		this.mallName = mallName;
	}

	public static final Parcelable.Creator<TipsModel> CREATOR = new Parcelable.Creator<TipsModel>() {
		public TipsModel createFromParcel(Parcel in) {
			return new TipsModel(in);
		}
		
		public TipsModel[] newArray(int size) {
			return new TipsModel[size];
		}
	};
}
package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TimeInfo implements Parcelable{
	public String day = "";
	public String is_closed = "";
	public String open_time = "";
	public String close_time = "";
	
	
	

	public TimeInfo(Parcel source){
		day 		= source.readString();
		is_closed 	= source.readString();
		open_time 	= source.readString();
		close_time 	= source.readString();
		
	}


	public TimeInfo(){

	}
	
	
	




	public String getDay() {
		return day;
	}


	public void setDay(String day) {
		this.day = day;
	}


	public String getIs_closed() {
		return is_closed;
	}


	public void setIs_closed(String is_closed) {
		this.is_closed = is_closed;
	}


	public String getOpen_time() {
		return open_time;
	}


	public void setOpen_time(String open_time) {
		this.open_time = open_time;
	}


	public String getClose_time() {
		return close_time;
	}


	public void setClose_time(String close_time) {
		this.close_time = close_time;
	}


	public static Parcelable.Creator<TimeInfo> getCreator() {
		return CREATOR;
	}
	
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(day);
		dest.writeString(is_closed);
		dest.writeString(open_time);
		dest.writeString(close_time);
		

	}
	
	
	public static final Parcelable.Creator<TimeInfo> CREATOR = new Parcelable.Creator<TimeInfo>() {
		public TimeInfo createFromParcel(Parcel in) {
			return new TimeInfo(in);
		}

		public TimeInfo[] newArray(int size) {
			return new TimeInfo[size];
		}
	};
	
	
}

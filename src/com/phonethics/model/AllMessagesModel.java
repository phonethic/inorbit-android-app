package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AllMessagesModel implements Parcelable {

	public  String count = "";
	public  String is_read = "";
	public  String from = "";
	public  String to = "";
	public  String from_store_name = "";
	public  String from_image_url = "";
	public  String to_store_name = "";
	public  String to_image_url = "";

	public AllMessagesModel() {
	}

	public AllMessagesModel(Parcel source){
		count = source.readString();
		is_read = source.readString();
		from =source.readString() ;
		to = source.readString();
		from_store_name = source.readString();
		from_image_url =source.readString() ;
		to_store_name =source.readString() ;
		to_image_url = source.readString();
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getIs_read() {
		return is_read;
	}

	public void setIs_read(String is_read) {
		this.is_read = is_read;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom_store_name() {
		return from_store_name;
	}

	public void setFrom_store_name(String from_store_name) {
		this.from_store_name = from_store_name;
	}

	public String getFrom_image_url() {
		return from_image_url;
	}

	public void setFrom_image_url(String from_image_url) {
		this.from_image_url = from_image_url;
	}

	public String getTo_store_name() {
		return to_store_name;
	}

	public void setTo_store_name(String to_store_name) {
		this.to_store_name = to_store_name;
	}

	public String getTo_image_url() {
		return to_image_url;
	}

	public void setTo_image_url(String to_image_url) {
		this.to_image_url = to_image_url;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

		dest.writeString(count);
		dest.writeString(is_read);
		dest.writeString(from);
		dest.writeString(to);
		dest.writeString(from_store_name);
		dest.writeString(from_image_url);
		dest.writeString(to_store_name);
		dest.writeString(to_image_url);
	}

	public static final Parcelable.Creator<AllMessagesModel> CREATOR = new Parcelable.Creator<AllMessagesModel>() {
		public AllMessagesModel createFromParcel(Parcel in) {
			return new AllMessagesModel(in);
		}

		public AllMessagesModel[] newArray(int size) {
			return new AllMessagesModel[size];
		}
	};
}

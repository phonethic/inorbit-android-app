package com.phonethics.model;

public class DataCount {
	
	
	private String mallID = "";
	private String storeCount = "";
	private String postCount = "";
	
	
	public String getMallID() {
		return mallID;
	}
	public void setMallID(String mallID) {
		this.mallID = mallID;
	}
	public String getStoreCount() {
		return storeCount;
	}
	public void setStoreCount(String storeCount) {
		this.storeCount = storeCount;
	}
	public String getPostCount() {
		return postCount;
	}
	public void setPostCount(String postCount) {
		this.postCount = postCount;
	}
	
	
	
	

}

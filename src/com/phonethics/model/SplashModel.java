package com.phonethics.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class SplashModel implements Parcelable{

	public  String thumb_url = "";
	public  String id = "";
	public  String place_id = "";
	public  String title = "";
	public  String image_data = "";
	public  String image_url = "";

	public  ArrayList<String> image_url_Arr = new ArrayList<String>();


	public SplashModel(Parcel source){
		thumb_url 		= source.readString();
		id 				= source.readString();
		place_id 		= source.readString();
		title 			= source.readString();
		image_data 		= source.readString();
		image_url 		= source.readString();

	}


	public SplashModel(){

	}




	public  String getThumb_url() {
		return thumb_url;
	}
	public  void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}
	public  String getId() {
		return id;
	}
	public  void setId(String id) {
		this.id = id;
	}
	public  String getPlace_id() {
		return place_id;
	}
	public  void setPlace_id(String place_id) {
		this.place_id = place_id;
	}
	public  String getTitle() {
		return title;
	}
	public  void setTitle(String title) {
		this.title = title;
	}
	public  String getImage_data() {
		return image_data;
	}
	public  void setImage_data(String image_data) {
		this.image_data = image_data;
	}
	public  String getImage_url() {
		return image_url;
	}
	public  void setImage_url(String image_url) {
		this.image_url = image_url;
		image_url_Arr.add(image_url);
	}

	public ArrayList<String> getImageUrlArr(){
		return image_url_Arr;
	}


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(thumb_url);
		dest.writeString(id);
		dest.writeString(place_id);
		dest.writeString(title);
		dest.writeString(image_data);
		dest.writeString(image_url);

	}



	public static final Parcelable.Creator<SplashModel> CREATOR = new Parcelable.Creator<SplashModel>() {
		public SplashModel createFromParcel(Parcel in) {
			return new SplashModel(in);
		}

		public SplashModel[] newArray(int size) {
			return new SplashModel[size];
		}
	};


}

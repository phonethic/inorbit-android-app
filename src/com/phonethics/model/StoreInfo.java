package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class StoreInfo implements Parcelable {
	
	public String id = "";
	public String place_parent = "";
	public String name = "";
	
	public String description = "";
	public String building = "";
	public String street = "";
	
	public String landmark = "";
	public String mall_id = "";
	public String area = "";
	
	public String city = "";
	public String mob_no1 = "";
	public String image_url = "";
	
	public String email = "";
	public String website = "";
	public String total_like = "";
	public String total_rating = "";
	

	public String has_offer = "";
	public String title = "";
	public String category = "";
	
	public String mob_no2 = "";
	public String mob_no3 = "";
	public String tel_no1 = "";
	public String tel_no2 = "";
	public String tel_no3 = "";
	
	public String toll_free_no1 = "";
	public String toll_free_no2 = "";
	


	public String getToll_free_no1() {
		return toll_free_no1;
	}


	public void setToll_free_no1(String toll_free_no1) {
		this.toll_free_no1 = toll_free_no1;
	}


	public String getToll_free_no2() {
		return toll_free_no2;
	}


	public void setToll_free_no2(String toll_free_no2) {
		this.toll_free_no2 = toll_free_no2;
	}


	public String getMob_no2() {
		return mob_no2;
	}


	public void setMob_no2(String mob_no2) {
		this.mob_no2 = mob_no2;
	}


	public String getMob_no3() {
		return mob_no3;
	}


	public void setMob_no3(String mob_no3) {
		this.mob_no3 = mob_no3;
	}


	public String getTel_no1() {
		return tel_no1;
	}


	public void setTel_no1(String tel_no1) {
		this.tel_no1 = tel_no1;
	}


	public String getTel_no2() {
		return tel_no2;
	}


	public void setTel_no2(String tel_no2) {
		this.tel_no2 = tel_no2;
	}


	public String getTel_no3() {
		return tel_no3;
	}


	public void setTel_no3(String tel_no3) {
		this.tel_no3 = tel_no3;
	}


	public StoreInfo(Parcel source){
		id 			= source.readString();
		place_parent = source.readString();
		name = source.readString();
		
		description = source.readString();
		building 	= source.readString();	
		street 		= source.readString();
		
		landmark 	= source.readString();
		mall_id 	= source.readString();
		area 		= source.readString();
		
		city 		= source.readString();
		mob_no1 	= source.readString();
		image_url 	= source.readString();
		
		email 		= source.readString();
		website 	= source.readString();	
		total_like	= source.readString();
		total_rating = source.readString();
		
		has_offer 	= source.readString();
		title 		= source.readString();
		category 	= source.readString();
		
		mob_no2 	= source.readString();
		mob_no3 	= source.readString();
		tel_no1 	= source.readString();
		tel_no2 	= source.readString();
		tel_no3 	= source.readString();
		
		toll_free_no1 = source.readString();
		toll_free_no2 = source.readString();
	}


	public StoreInfo(){

	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getPlace_parent() {
		return place_parent;
	}


	public void setPlace_parent(String place_parent) {
		this.place_parent = place_parent;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getBuilding() {
		return building;
	}


	public void setBuilding(String building) {
		this.building = building;
	}


	public String getStreet() {
		return street;
	}


	public void setStreet(String street) {
		this.street = street;
	}


	public String getLandmark() {
		return landmark;
	}


	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}


	public String getMall_id() {
		return mall_id;
	}


	public void setMall_id(String mall_id) {
		this.mall_id = mall_id;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getMob_no1() {
		return mob_no1;
	}


	public void setMob_no1(String mob_no1) {
		this.mob_no1 = mob_no1;
	}


	public String getImage_url() {
		return image_url;
	}


	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getWebsite() {
		return website;
	}


	public void setWebsite(String website) {
		this.website = website;
	}


	public String getTotal_like() {
		return total_like;
	}


	public void setTotal_like(String total_like) {
		this.total_like = total_like;
	}

	public String getTotal_rating() {
		return total_rating;
	}


	public void setTotal_rating(String total_rating) {
		this.total_rating = total_rating;
	}

	public String getHas_offer() {
		return has_offer;
	}


	public void setHas_offer(String has_offer) {
		this.has_offer = has_offer;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	
	

	


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(id);
		dest.writeString(place_parent);
		dest.writeString(name);
		
		dest.writeString(description);
		dest.writeString(building);
		dest.writeString(street);
		
		dest.writeString(landmark);
		dest.writeString(mall_id);
		dest.writeString(area);
		
		dest.writeString(city);
		dest.writeString(mob_no1);
		dest.writeString(image_url);
		
		dest.writeString(email);
		dest.writeString(website);
		dest.writeString(total_like);
		dest.writeString(total_rating);
		
		dest.writeString(has_offer);
		dest.writeString(title);
		dest.writeString(category);
		
		dest.writeString(mob_no2);
		dest.writeString(mob_no3);
		dest.writeString(tel_no1);
		dest.writeString(tel_no2);
		dest.writeString(tel_no3);
		
		dest.writeString(toll_free_no1);
		dest.writeString(toll_free_no2);
	}



	public static final Parcelable.Creator<StoreInfo> CREATOR = new Parcelable.Creator<StoreInfo>() {
		public StoreInfo createFromParcel(Parcel in) {
			return new StoreInfo(in);
		}

		public StoreInfo[] newArray(int size) {
			return new StoreInfo[size];
		}
	};

	

}

package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PostDetail implements Parcelable {
	
	
	private String id  					= "";
	private String place_id				= "";
	private String type					= "";
	
	private String title				= "";
	private String description			= "";
	private String url					= "";
	
	private String tags					= "";
	private String date					= "";
	private String is_offered			= "";
	
	private String offer_end_date_time	= "";
	private String total_like			= "";
	private String total_view			= "";
	
	private String total_share			= "";
	private String image_title1			= "";
	private String image_title2			= "";
	
	private String image_title3			= "";
	private String image_url1			= "";
	private String image_url2			= "";
	
	private String image_url3			= "";
	private String thumb_url1			= "";
	private String thumb_url2			= "";
	
	private String thumb_url3			= "";
	private String status			    = "";
	private String name			    	= "";
	
	private String offer_start_date_time	= "";
	private String user_like	= "";
	
	private String priority	= "";
	
	
	
	
	
	
	
	public PostDetail(Parcel source){
		id 			= source.readString();
		place_id = source.readString();
		type = source.readString();
		
		title = source.readString();
		description 	= source.readString();	
		url 		= source.readString();
		
		tags 	= source.readString();
		date 	= source.readString();
		is_offered 		= source.readString();
		
		offer_end_date_time 		= source.readString();
		total_like 	= source.readString();
		total_view 	= source.readString();
		
		total_share 		= source.readString();
		image_title1 	= source.readString();	
		image_title2	= source.readString();
		
		image_title3 	= source.readString();
		image_url1 		= source.readString();
		image_url2 		= source.readString();
		
		image_url3 		= source.readString();
		thumb_url1 		= source.readString();
		thumb_url2 		= source.readString();
		
		thumb_url3 		= source.readString();
		status 		= source.readString();
		name 		= source.readString();
		
		offer_start_date_time = source.readString();
		user_like = source.readString();
		
		priority = source.readString();
		

	}


	public PostDetail(){

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPlace_id() {
		return place_id;
	}

	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getIs_offered() {
		return is_offered;
	}

	public void setIs_offered(String is_offered) {
		this.is_offered = is_offered;
	}

	public String getOffer_end_date_time() {
		return offer_end_date_time;
	}

	public void setOffer_end_date_time(String offer_date_time) {
		this.offer_end_date_time = offer_date_time;
	}

	public String getTotal_like() {
		return total_like;
	}

	public void setTotal_like(String total_like) {
		this.total_like = total_like;
	}

	public String getTotal_view() {
		return total_view;
	}

	public void setTotal_view(String total_view) {
		this.total_view = total_view;
	}

	public String getTotal_share() {
		return total_share;
	}

	public void setTotal_share(String total_share) {
		this.total_share = total_share;
	}

	public String getImage_title1() {
		return image_title1;
	}

	public void setImage_title1(String image_title1) {
		this.image_title1 = image_title1;
	}

	public String getImage_title2() {
		return image_title2;
	}

	public void setImage_title2(String image_title2) {
		this.image_title2 = image_title2;
	}

	public String getImage_title3() {
		return image_title3;
	}

	public void setImage_title3(String image_title3) {
		this.image_title3 = image_title3;
	}

	public String getImage_url1() {
		return image_url1;
	}

	public void setImage_url1(String image_url1) {
		this.image_url1 = image_url1;
	}

	public String getImage_url2() {
		return image_url2;
	}

	public void setImage_url2(String image_url2) {
		this.image_url2 = image_url2;
	}

	public String getImage_url3() {
		return image_url3;
	}

	public void setImage_url3(String image_url3) {
		this.image_url3 = image_url3;
	}

	public String getThumb_url1() {
		return thumb_url1;
	}

	public void setThumb_url1(String thumb_url1) {
		this.thumb_url1 = thumb_url1;
	}

	public String getThumb_url2() {
		return thumb_url2;
	}

	public void setThumb_url2(String thumb_url2) {
		this.thumb_url2 = thumb_url2;
	}

	public String getThumb_url3() {
		return thumb_url3;
	}

	public void setThumb_url3(String thumb_url3) {
		this.thumb_url3 = thumb_url3;
	}
	
	
	
	
	public String getOffer_start_date_time() {
		return offer_start_date_time;
	}


	public void setOffer_start_date_time(String offer_start_date_time) {
		this.offer_start_date_time = offer_start_date_time;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	
	


	public String getUser_like() {
		return user_like;
	}


	public void setUser_like(String user_like) {
		this.user_like = user_like;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	
	

	public String getPriority() {
		return priority;
	}


	public void setPriority(String priority) {
		this.priority = priority;
	}


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(id);
		dest.writeString(place_id);
		dest.writeString(type);
		
		dest.writeString(title);
		dest.writeString(description);
		dest.writeString(url);
		
		dest.writeString(tags);
		dest.writeString(date);
		dest.writeString(is_offered);
		
		dest.writeString(offer_end_date_time);
		dest.writeString(total_like);
		dest.writeString(total_view);
		
		dest.writeString(total_share);
		dest.writeString(image_title1);
		dest.writeString(image_title2);
		
		dest.writeString(image_title3);
		dest.writeString(image_url1);
		dest.writeString(image_url2);
		
		dest.writeString(image_url3);
		dest.writeString(thumb_url1);
		dest.writeString(thumb_url2);
		
		dest.writeString(thumb_url2);
		dest.writeString(status);
		dest.writeString(name);
	
		dest.writeString(offer_start_date_time);
		dest.writeString(user_like);
		dest.writeString(priority);


	}



	public static final Parcelable.Creator<PostDetail> CREATOR = new Parcelable.Creator<PostDetail>() {
		public PostDetail createFromParcel(Parcel in) {
			return new PostDetail(in);
		}

		public PostDetail[] newArray(int size) {
			return new PostDetail[size];
		}
	};
	


}

package com.phonethics.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class GetSpecialDates implements Parcelable{
	
	private ArrayList<String> type = new ArrayList<String>();
	private ArrayList<String> count = new ArrayList<String>();
	private String calenderdate;

	
	public String getCalenderdate() {
		return calenderdate;
	}

	public void setCalenderdate(String calenderdate) {
		this.calenderdate = calenderdate;
	}

	public ArrayList<String> getType() {
		return type;
	}

	public void setType(String eventtype) {
		type.add(eventtype);
	}

	public ArrayList<String> getCount() {
		return count;
	}

	public void setCount(String eventcount) {
		count.add(eventcount);
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
	}

	
}

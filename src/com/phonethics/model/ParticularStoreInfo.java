package com.phonethics.model;


import android.os.Parcel;
import android.os.Parcelable;

public class ParticularStoreInfo implements Parcelable{


	private String id = "";
	private String name = "";
	private String place_parent = "";

	private String description = "";
	private String building = "";
	private String street = "";

	private String landmark = "";
	public String mall_id = "";
	private String area = "";

	private String pincode = "";
	private String city = "";
	private String state = "";

	private String country = "";
	private String latitude = "";
	private String longitude = "";

	private String tel_no1 = "";
	private String tel_no2 = "";
	private String tel_no3 = "";
	
	private String mob_no1 = "";
	private String mob_no2 = "";
	private String mob_no3 = "";
	
	private String fax_no1 = "";
	private String fax_no2 = "";

	public String toll_free_no1 = "";
	private String toll_free_no2 = "";
	private String image_url = "";

	private String email = "";
	private String website = "";
	private String facebook_url = "";


	private String twitter_url = "";
	private String total_share = "";
	private String total_like = "";
	
	private String user_like = "-1";
	private String place_status = "-1";
	private String published = "-1";
	
	private String total_view = "-1";
	
	private String open_time = "-1";
	private String close_time = "-1";
	
	private String sub_category = "";


	

	public String getSub_category() {
		return sub_category;
	}


	public void setSub_category(String sub_category) {
		this.sub_category = sub_category;
	}


	public ParticularStoreInfo(Parcel source){
		
		id 				= source.readString();
		name 			= source.readString();
		place_parent 	= source.readString();
		
		description 	= source.readString();
		building 		= source.readString();
		street 			= source.readString();
		
		landmark 		= source.readString();
		mall_id 		= source.readString();
		area 			= source.readString();
		
		pincode 		= source.readString();
		city 			= source.readString();
		state 			= source.readString();
		
		country 		= source.readString();
		latitude 		= source.readString();
		longitude 		= source.readString();
		
		tel_no1 		= source.readString();
		tel_no2 		= source.readString();
		tel_no3 		= source.readString();
		
		mob_no1 		= source.readString();
		mob_no2 		= source.readString();
		mob_no3 		= source.readString();
		
		fax_no1 		= source.readString();
		fax_no2 		= source.readString();
		
		toll_free_no1	= source.readString();
		toll_free_no2 	= source.readString();
		image_url 		= source.readString();
		
		email 			= source.readString();
		website 		= source.readString();
		facebook_url 	= source.readString();
		
		twitter_url 	= source.readString();
		total_share 	= source.readString();
		total_like 		= source.readString();
		
		user_like 		= source.readString();
		place_status 		= source.readString();
		published 		= source.readString();
		
		total_view 		= source.readString();
		open_time 		= source.readString();
		close_time 		= source.readString();
		sub_category 		= source.readString();

	}


	public ParticularStoreInfo(){

	}
	
	
	



	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPlace_parent() {
		return place_parent;
	}


	public void setPlace_parent(String place_parent) {
		this.place_parent = place_parent;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getBuilding() {
		return building;
	}


	public void setBuilding(String building) {
		this.building = building;
	}


	public String getStreet() {
		return street;
	}


	public void setStreet(String street) {
		this.street = street;
	}


	public String getLandmark() {
		return landmark;
	}


	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}


	public String getMall_id() {
		return mall_id;
	}


	public void setMall_id(String mall_id) {
		this.mall_id = mall_id;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getPincode() {
		return pincode;
	}


	public void setPincode(String pincode) {
		this.pincode = pincode;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getLatitude() {
		return latitude;
	}


	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}


	public String getLongitude() {
		return longitude;
	}


	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}


	public String getTel_no1() {
		return tel_no1;
	}


	public void setTel_no1(String tel_no1) {
		this.tel_no1 = tel_no1;
	}


	public String getTel_no2() {
		return tel_no2;
	}


	public void setTel_no2(String tel_no2) {
		this.tel_no2 = tel_no2;
	}


	public String getMob_no1() {
		return mob_no1;
	}


	public void setMob_no1(String mob_no1) {
		this.mob_no1 = mob_no1;
	}


	public String getMob_no2() {
		return mob_no2;
	}


	public void setMob_no2(String mob_no2) {
		this.mob_no2 = mob_no2;
	}


	public String getFax_no1() {
		return fax_no1;
	}


	public void setFax_no1(String fax_no1) {
		this.fax_no1 = fax_no1;
	}


	public String getFax_no2() {
		return fax_no2;
	}


	public void setFax_no2(String fax_no2) {
		this.fax_no2 = fax_no2;
	}


	public String getToll_free_no1() {
		return toll_free_no1;
	}


	public void setToll_free_no1(String toll_free_no1) {
		this.toll_free_no1 = toll_free_no1;
	}


	public String getToll_free_no2() {
		return toll_free_no2;
	}


	public void setToll_free_no2(String toll_free_no2) {
		this.toll_free_no2 = toll_free_no2;
	}


	public String getImage_url() {
		return image_url;
	}


	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getWebsite() {
		return website;
	}


	public void setWebsite(String website) {
		this.website = website;
	}


	public String getFacebook_url() {
		return facebook_url;
	}


	public void setFacebook_url(String facebook_url) {
		this.facebook_url = facebook_url;
	}


	public String getTwitter_url() {
		return twitter_url;
	}


	public void setTwitter_url(String twitter_url) {
		this.twitter_url = twitter_url;
	}


	public String getTotal_share() {
		return total_share;
	}


	public void setTotal_share(String total_share) {
		this.total_share = total_share;
	}


	public String getTotal_like() {
		return total_like;
	}


	public void setTotal_like(String total_like) {
		this.total_like = total_like;
	}


	public String getUser_like() {
		return user_like;
	}


	public void setUser_like(String user_like) {
		this.user_like = user_like;
	}
	public String getPlace_status() {
		return place_status;
	}

	

	public String getTel_no3() {
		return tel_no3;
	}


	public void setTel_no3(String tel_no3) {
		this.tel_no3 = tel_no3;
	}


	public String getMob_no3() {
		return mob_no3;
	}


	public void setMob_no3(String mob_no3) {
		this.mob_no3 = mob_no3;
	}


	public String getOpen_time() {
		return open_time;
	}


	public void setOpen_time(String open_time) {
		this.open_time = open_time;
	}


	public String getClose_time() {
		return close_time;
	}


	public void setClose_time(String close_time) {
		this.close_time = close_time;
	}


	public void setPlace_status(String place_status) {
		this.place_status = place_status;
	}


	public String getPublished() {
		return published;
	}


	public void setPublished(String published) {
		this.published = published;
	}
	
	
	


	public String getTotal_view() {
		return total_view;
	}


	public void setTotal_view(String total_view) {
		this.total_view = total_view;
	}

	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	

	
	
	

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
		dest.writeString(id);
		dest.writeString(name);
		dest.writeString(place_parent);
		
		dest.writeString(description);
		dest.writeString(building);
		dest.writeString(street);
		
		dest.writeString(landmark);
		dest.writeString(mall_id);
		dest.writeString(area);
		
		dest.writeString(pincode);
		dest.writeString(city);
		dest.writeString(state);
		
		dest.writeString(country);
		dest.writeString(latitude);
		dest.writeString(longitude);
		
		dest.writeString(tel_no1);
		dest.writeString(tel_no2);
		dest.writeString(tel_no3);
		
		dest.writeString(mob_no1);
		dest.writeString(mob_no2);
		dest.writeString(mob_no3);
		
		dest.writeString(fax_no1);
		dest.writeString(fax_no2);
		
		dest.writeString(toll_free_no1);
		dest.writeString(toll_free_no2);
		dest.writeString(image_url);
		
		dest.writeString(email);
		dest.writeString(website);
		dest.writeString(facebook_url);
		
		dest.writeString(twitter_url);
		dest.writeString(total_share);
		dest.writeString(total_like);
		
		dest.writeString(user_like);
		dest.writeString(place_status);
		dest.writeString(published);
		
		dest.writeString(total_view);
		dest.writeString(open_time);
		dest.writeString(close_time);
		dest.writeString(sub_category);
		
	}
	
	public static final Parcelable.Creator<ParticularStoreInfo> CREATOR = new Parcelable.Creator<ParticularStoreInfo>() {
		public ParticularStoreInfo createFromParcel(Parcel in) {
			return new ParticularStoreInfo(in);
		}

		public ParticularStoreInfo[] newArray(int size) {
			return new ParticularStoreInfo[size];
		}
	};
	
	
}





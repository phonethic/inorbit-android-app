package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TipsManagerResponseModel implements Parcelable {
	private String storeId = "";
	private int count = 0;
	private String storeName = "";
	private String storeLogo = "";
	
	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreLogo() {
		return storeLogo;
	}

	public void setStoreLogo(String storeLogo) {
		this.storeLogo = storeLogo;
	}

	public TipsManagerResponseModel() {
		
	}
	
	public TipsManagerResponseModel(Parcel source){
		storeId = source.readString();
		count = source.readInt();
		storeName = source.readString();
		storeLogo = source.readString();
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(storeId);
		dest.writeInt(count);
		dest.writeString(storeName);
		dest.writeString(storeLogo);
	}
	
	public static final Parcelable.Creator<TipsManagerResponseModel> CREATOR = new Parcelable.Creator<TipsManagerResponseModel>() {
		public TipsManagerResponseModel createFromParcel(Parcel in) {
			return new TipsManagerResponseModel(in);
		}
		
		public TipsManagerResponseModel[] newArray(int size) {
			return new TipsManagerResponseModel[size];
		}
	};
}

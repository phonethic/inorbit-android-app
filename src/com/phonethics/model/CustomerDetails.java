package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CustomerDetails implements Parcelable{


	private String id				= "";
	private String name				= "";
	private String mobile			= "";

	private String email			= "";
	private String gender			= "";
	private String city				= "";

	private String state			= "";
	private String image_url		= "";
	private String facebook_user_id	= "";

	private String facebook_access_token	= "";


	public CustomerDetails(Parcel source){

		id 						= source.readString();
		name 					= source.readString();
		mobile 					= source.readString();

		email 					= source.readString();
		gender 					= source.readString();
		city 					= source.readString();

		state 					= source.readString();
		image_url 				= source.readString();
		facebook_user_id 		= source.readString();

		facebook_access_token 	= source.readString();
	}
	
	
	public CustomerDetails(){

	}


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getFacebook_user_id() {
		return facebook_user_id;
	}
	public void setFacebook_user_id(String facebook_user_id) {
		this.facebook_user_id = facebook_user_id;
	}
	public String getFacebook_access_token() {
		return facebook_access_token;
	}
	public void setFacebook_access_token(String facebook_access_token) {
		this.facebook_access_token = facebook_access_token;
	}


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
		dest.writeString(id);
		dest.writeString(name);
		dest.writeString(mobile);
		
		dest.writeString(email);
		dest.writeString(gender);
		dest.writeString(city);
		
		dest.writeString(state);
		dest.writeString(image_url);
		dest.writeString(facebook_user_id);
		
		dest.writeString(facebook_access_token);
		

	}
	
	
	public static final Parcelable.Creator<CustomerDetails> CREATOR = new Parcelable.Creator<CustomerDetails>() {
		public CustomerDetails createFromParcel(Parcel in) {
			return new CustomerDetails(in);
		}

		public CustomerDetails[] newArray(int size) {
			return new CustomerDetails[size];
		}
	};




}

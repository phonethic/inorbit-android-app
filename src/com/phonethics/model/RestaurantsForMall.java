package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RestaurantsForMall implements Parcelable {
	
	String storeName = "";
	String storeLogo = "";
	String placeId = "";
	String mallId = "";
	String firstMenuImage = "";
	String firstMenuThumbnail = "";
	String menuUploadDate = "";
	String status = "";

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreLogo() {
		return storeLogo;
	}

	public void setStoreLogo(String storeLogo) {
		this.storeLogo = storeLogo;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public String getMallId() {
		return mallId;
	}

	public void setMallId(String mallId) {
		this.mallId = mallId;
	}

	public String getFirstMenuImage() {
		return firstMenuImage;
	}

	public void setFirstMenuImage(String firstMenuImage) {
		this.firstMenuImage = firstMenuImage;
	}

	public String getFirstMenuThumbnail() {
		return firstMenuThumbnail;
	}

	public void setFirstMenuThumbnail(String firstMenuThumbnail) {
		this.firstMenuThumbnail = firstMenuThumbnail;
	}

	public String getMenuUploadDate() {
		return menuUploadDate;
	}

	public void setMenuUploadDate(String menuUploadDate) {
		this.menuUploadDate = menuUploadDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int describeContents() {
		return 0;
	}
	
	public RestaurantsForMall() {
		
	}
	
	public RestaurantsForMall(Parcel source){
		storeName = source.readString();
		storeLogo = source.readString();
		placeId  = source.readString();
		mallId = source.readString();
		firstMenuImage = source.readString();
		firstMenuThumbnail = source.readString();
		menuUploadDate = source.readString();
		status = source.readString();
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(storeName);
		dest.writeString(storeLogo);
		dest.writeString(placeId);
		dest.writeString(mallId);
		dest.writeString(firstMenuImage);
		dest.writeString(firstMenuThumbnail);
		dest.writeString(menuUploadDate);
		dest.writeString(status);
	}
	
	public static final Parcelable.Creator<RestaurantsForMall> CREATOR = new Parcelable.Creator<RestaurantsForMall>() {
		public RestaurantsForMall createFromParcel(Parcel in) {
			return new RestaurantsForMall(in);
		}

		public RestaurantsForMall[] newArray(int size) {
			return new RestaurantsForMall[size];
		}
	};
}

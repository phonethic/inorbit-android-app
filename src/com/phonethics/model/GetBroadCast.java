package com.phonethics.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class GetBroadCast implements Parcelable {

	private String total_page="";
	private String total_record="";
	private String id="";
	private String type="";
	private String tags="";
	private String title="";
	private String description="";
	private String url="";
	private String date="";
	private String start_date="";
	private String place_id="";
	private String total_like="";
	private String user_like="";
	private String user_views = "";
	private String total_share = "";
	private String photo_url = "";
	private String end_date = "";
	
	private ArrayList<String>source=new ArrayList<String>();

	
	public String getPlace_id() {
		return place_id;
	}
	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}
	
	public String getTotal_share() {
		return total_share;
	}
	public void setTotal_share(String total_share) {
		this.total_share = total_share;
	}
	
	public String getUser_views() {
		return user_views;
	}
	public void setUser_views(String user_views) {
		this.user_views = user_views;
	}
	public String getTotal_like() {
		return total_like;
	}
	public void setTotal_like(String total_like) {
		this.total_like = total_like;
	}
	public String getUser_like() {
		return user_like;
	}
	public void setUser_like(String user_like) {
		this.user_like = user_like;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public void setSource(String isource)
	{
		source.add(isource);
	}
	public ArrayList<String> getSource()
	{
		return source;
	}
	public String getTotal_page() {
		return total_page;
	}

	public void setTotal_page(String total_page) {
		this.total_page = total_page;
	}

	public String getTotal_record() {
		return total_record;
	}

	public void setTotal_record(String total_record) {
		this.total_record = total_record;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
	}
	public String getPhoto_url() {
		return photo_url;
	}
	public void setPhoto_url(String photo_url) {
		this.photo_url = photo_url;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	
	
	
	
	
	
}

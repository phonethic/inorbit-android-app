package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RestaurantMenuModel implements Parcelable {
	
	String id="";
	String place_id = "";
	String mall_id = "";
	String image_url = "";
	String thumb_url = "";
	String date = "";
	String image_order = "";
	String status = "";
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	public RestaurantMenuModel() {
		
	}
	
	public RestaurantMenuModel(Parcel source){
		id = source.readString();
		place_id = source.readString();
		mall_id  = source.readString();
		image_url = source.readString();
		thumb_url = source.readString();
		date = source.readString();
		image_order = source.readString();
		status = source.readString();
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(place_id);
		dest.writeString(mall_id);
		dest.writeString(image_url);
		dest.writeString(thumb_url);
		dest.writeString(date);
		dest.writeString(image_order);
		dest.writeString(status);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPlace_id() {
		return place_id;
	}

	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}

	public String getMall_id() {
		return mall_id;
	}

	public void setMall_id(String mall_id) {
		this.mall_id = mall_id;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getThumb_url() {
		return thumb_url;
	}

	public void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getImage_order() {
		return image_order;
	}

	public void setImage_order(String image_order) {
		this.image_order = image_order;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static final Parcelable.Creator<RestaurantMenuModel> CREATOR = new Parcelable.Creator<RestaurantMenuModel>() {
		public RestaurantMenuModel createFromParcel(Parcel in) {
			return new RestaurantMenuModel(in);
		}

		public RestaurantMenuModel[] newArray(int size) {
			return new RestaurantMenuModel[size];
		}
	};
}

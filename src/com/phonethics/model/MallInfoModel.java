package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MallInfoModel implements Parcelable {


	public  String pincode = "";
	public  String id = "";
	public  String inorbit_place_id = "";
	public  String iso_code = "";
	public  String state = "";
	public  String locality = "";
	public  String longitude = "";
	public  String latitude = "";
	public  String sublocality = "";
	public  String facebook_url = "";
	public  String country_code = "";
	public  String country = "";
	public  String city = "";


	public MallInfoModel(Parcel source){
		
		pincode 				= source.readString();
		id 						= source.readString();
		inorbit_place_id 		= source.readString();
		iso_code 				= source.readString();
		state 					= source.readString();
		locality 				= source.readString();
		longitude 				= source.readString();
		latitude 				= source.readString();
		sublocality 			= source.readString();
		facebook_url 			= source.readString();
		country_code 			= source.readString();
		country 				= source.readString();
		city 					= source.readString();

	}


	public MallInfoModel(){

	}

	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInorbit_place_id() {
		return inorbit_place_id;
	}
	public void setInorbit_place_id(String inorbit_place_id) {
		this.inorbit_place_id = inorbit_place_id;
	}
	public String getIso_code() {
		return iso_code;
	}
	public void setIso_code(String iso_code) {
		this.iso_code = iso_code;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getSublocality() {
		return sublocality;
	}
	public void setSublocality(String sublocality) {
		this.sublocality = sublocality;
	}
	public String getFacebook_url() {
		return facebook_url;
	}
	public void setFacebook_url(String facebook_url) {
		this.facebook_url = facebook_url;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int arg1) {
		// TODO Auto-generated method stub

		dest.writeString(pincode);
		dest.writeString(id);
		dest.writeString(inorbit_place_id);
		dest.writeString(iso_code);
		dest.writeString(state);
		dest.writeString(locality);
		dest.writeString(longitude);
		dest.writeString(latitude);
		dest.writeString(sublocality);
		dest.writeString(facebook_url);
		dest.writeString(country_code);
		dest.writeString(country);
		dest.writeString(city);

	}



	public static final Parcelable.Creator<MallInfoModel> CREATOR = new Parcelable.Creator<MallInfoModel>() {
		public MallInfoModel createFromParcel(Parcel in) {
			return new MallInfoModel();
		}
		
		public MallInfoModel[] newArray(int size) {
			return new MallInfoModel[size];
		}
	};







	}

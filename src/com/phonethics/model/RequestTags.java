package com.phonethics.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.phonethics.inorbit.MyClass;
import com.phonethics.inorbit.R;

public class RequestTags {
	
	public static final String TagGetGallery 					= "Tag_GetGallery";
	public static final String TagGetMalls 						= "Tag_GetMalls";
	public static final String TagSearchStores 					= "Tag_SearchStores";
	public static final String Tag_LoadCategories 				= "Tag_LoadCategories";
	public static final String TagGetParticularStoreDetails_c 	= "Tag_GetParticularStoreDetails_c";
	public static final String TagGetParticularStoreDetails_m 	= "Tag_GetParticularStoreDetails_m";
	public static final String TagGetParticularStoreDetails_d 	= "Tag_GetParticularStoreDetails_d";
	public static final String TagGetStoreGallery 				= "Tag_GetStoreGalley";
	public static final String TagGetSpecialPost 				= "Tag_GetSpecailPost";
	public static final String TagLoginMerchant 				= "Tag_LoginMerchant";
	public static final String TagLoginCustomer 				= "Tag_LoginCustomer";
	public static final String TagPlaceChooser 					= "Tag_PlaceChooser";
	public static final String TagCreateBroadcast				= "Tag_CreateBroadcast";
	public static final String TagViewBroadcast					= "Tag_ViewBroadcast";
	public static final String TagViewBroadcast_c				= "Tag_ViewBroadcast_c";
	public static final String TagViewBroadcast_mall			= "Tag_ViewBroadcast_mall";
	public static final String Tag_LoadStoreGallery				= "Tag_LoadStoreGallery";
	public static final String Tag_AddStore						= "Tag_AddStore";
	public static final String Tag_CategoryInfo					= "Tag_CategoryInfo";
	public static final String Tag_SpecialDates					= "Tag_SpecialDates";
	public static final String Tag_CustomerProfile				= "Tag_CustomerProfile";
	public static final String Tag_UpdateCustomerProfile		= "Tag_UpdateCustomerProfile";
	public static final String Tag_UpdateActiveMall				= "Tag_UpdateActiveMall";
	public static final String Tag_GetIntrestedAreas			= "Tag_GetIntrestedAreas";
	public static final String Tag_UpdateIntrestedMalls			= "Tag_UpdateIntrestedMalls";
	public static final String Tag_DeletePost					= "Tag_DeletePost";
	public static final String Tag_ValidateMerchant				= "Tag_VlidateMerchant";
	public static final String Tag_UnapprovedBroadcast			= "Tag_UnapprovedBroadcast";
	public static final String Tag_ApproveBroadcast				= "Tag_ApproveBroadcast";
	public static final String Tag_LikePlace					= "Tag_LikePlace";
	public static final String Tag_UnLikePlace					= "Tag_UnLikePlace";
	public static final String Tag_RegisterUser					= "Tag_RegisterUser";
	public static final String Tag_merchantregister				= "Tag_merchantregister";
	public static final String Tag_GetMallEvents				= "Tag_GetMallEvents";
	public static final String Tag_GetDataVersion				= "Tag_GetDataVersion";
	public static final String Tag_GetFavActiveUserCount		= "Tag_GetFavActiveUserCount";
	public static final String Tag_GetMontlyOccasionCount		= "Tag_GetMontlyOccasionCount";
	public static final String Tag_UserFavouriteStores			= "Tag_UserFavouriteStores";
	public static final String Tag_SpecialOffer					= "Tag_SpecialOffer";
	public static final String Tag_GetOfferDetail				= "Tag_GetOfferDetail";
	public static final String Tag_LikeThisOffer				= "Tag_LikeThisOffer";
	public static final String Tag_UnLikeThisOffer				= "Tag_UnLikeThisOffer";
	public static final String Tag_ShareThisOffer				= "Tag_ShareThisOffer";
	public static final String Tag_LoadCategory					= "Tag_LoadCategory";
	public static final String Tag_UpdateOffer					= "Tag_UpdateOffer";
	public static final String Tag_Feedback						= "Tag_Feedback";
	
	public static final String TAG_CREATE_NEW_MSG 				= "Tag_CreateNewMsg";
	public static final String TAG_GET_MALL_CONVERSATIONS		= "Tag_Get_Mail_Conversations";
	public static final String TAG_GET_CONVERSATION				= "Tag_Get_Conversation";
	public static final String TAG_UPDATE_STATUS				= "Tag_Update_Status";
	public static final String TAG_TOTAL_COUNT					= "Tag_Total_Count";
	public static final String TAG_RATE_STORE					= "Tag_Rate_Store";
	public static final String TAG_GET_UDM_ID_CUSTOMER			= "Tag_Get_Udm_Id_Customer";
	public static final String TAG_GET_UDM_ID_MERCHANT			= "Tag_Get_Udm_Id_Merchant";
	public static final String TAG_AVG_STORE_RATING				= "Tag_Avg_Store_Rating";	
	public static final String TAG_USER_SPECIFIC_STORE_RATE 	= "Tag_User_Specific_Store_Rate";
	
	public static final String TAG_UPLOAD_MENU					= "Tag_upload_menu_image";
	public static final String TAG_GET_MENU_FOR_STORE			= "Tag_get_menu_image";
	public static final String TAG_GET_MENU_FOR_MALL			= "Tag_get_menu_for_mall";
	public static final String TAG_DELETE_MENU_IMAGES 			= "Tag_Delete_Menu_Images";
	public static final String TAG_UPDATE_MENU_IMAGE_STATUS		= "Tag_Update_Menu_Image_status";
	public static final String TAG_CHANGE_MENU_ORDER 			= "Tag_update_menu_order";
	public static final String TAG_UNAPPROVED_MENU_COUNT 		= "Tag_unapproved_menu_count";
	

	//Tips
	public static final String TAG_TIP_STORE_MANAGER 			= "Tag_tip_store_manager";
	public static final String TAG_TIP_MALL_MANAGER 			= "Tag_tip_mall_manager";
	public static final String TAG_TIP_GET_ALL					= "Tag_tip_get_all";
	public static final String TAG_TIP_UPDATE_STATUS			= "Tag_update_tip_status";
	public static final String TAG_TIP_ADD_TIP					= "Tag_add_tip";
	
	public static final String TAG_UPLOAD_PHOTO_GALLERY 		= "Tag_upload_photo_gallery";
	
	public static final String GCMNotificationClickedEvent 		= "GCM_Notification_Clicked";
	public static final String AppVersionCheck					= "App_Version_Check";
	public static final String Back_Up_Push						= "Back_Up_Push";
	
	public static final String AnalyticsRequest					= "Inhouse_Analytics";
}

package com.phonethics.model;

import org.json.JSONArray;

public class Offer {

	private String OfferTitle = "";
	private String OfferDescription = "";
	private String Url = "";
	private String Tags = "";
	private String Type = "";
	private String State = "";
	private String Format = "";
	private String IsOffere = "";
	private String EndDateTime = "";
	private String StartDateTime = "";
	private String UserID = "";
	private String AuthID = "";
	private String FileName = "";
	private String FileType = "";
	private String UserFile = "";
	private JSONArray StoreIds;
	private boolean IsNormalOffer = true;
	
	
	public Offer(boolean isThisNormalOffer){
		IsNormalOffer = isThisNormalOffer;
	}


	public String getOfferTitle() {
		return OfferTitle;
	}


	public void setOfferTitle(String offerTitle) {
		OfferTitle = offerTitle;
	}


	public String getOfferDescription() {
		return OfferDescription;
	}


	public void setOfferDescription(String offerDescription) {
		OfferDescription = offerDescription;
	}


	public String getUrl() {
		return Url;
	}


	public void setUrl(String url) {
		Url = url;
	}


	public String getTags() {
		return Tags;
	}


	public void setTags(String tags) {
		Tags = tags;
	}


	public String getType() {
		return Type;
	}


	public void setType(String type) {
		Type = type;
	}


	public String getState() {
		return State;
	}


	public void setState(String state) {
		State = state;
	}


	public String getFormat() {
		return Format;
	}


	public void setFormat(String format) {
		Format = format;
	}


	public String getEndDateTime() {
		return EndDateTime;
	}


	public void setEndDateTime(String endDateTime) {
		EndDateTime = endDateTime;
	}


	public String getStartDateTime() {
		return StartDateTime;
	}


	public void setStartDateTime(String startDateTime) {
		StartDateTime = startDateTime;
	}


	public String getUserID() {
		return UserID;
	}


	public void setUserID(String userID) {
		UserID = userID;
	}


	public String getAuthID() {
		return AuthID;
	}


	public void setAuthID(String authID) {
		AuthID = authID;
	}


	public String getFileName() {
		return FileName;
	}


	public void setFileName(String fileName) {
		FileName = fileName;
	}


	public String getFileType() {
		return FileType;
	}


	public void setFileType(String fileType) {
		FileType = fileType;
	}


	public String getUserFile() {
		return UserFile;
	}


	public void setUserFile(String userFile) {
		UserFile = userFile;
	}


	public JSONArray getStoreIds() {
		return StoreIds;
	}


	public void setStoreIds(JSONArray storeIds) {
		StoreIds = storeIds;
	}
	
	
}

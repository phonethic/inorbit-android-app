package com.phonethics.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class StoreInfoDetails implements Parcelable{
	
	
	public int total_page 	= 0;
	public int total_record = 0;
	
	public ArrayList<StoreInfo> storeInfoArr;
	
	
	
	@SuppressWarnings("unchecked")
	public StoreInfoDetails(Parcel source){
		total_page 		= source.readInt();
		total_record 	= source.readInt();
		storeInfoArr  	= new ArrayList<StoreInfo>();
		source.readTypedList(storeInfoArr, StoreInfo.CREATOR);
		//source.readTypedList(storeInfoArr, StoreInfo.CREATOR);
		
		//source.readList(storeInfoArr, null);
		

	}


	public StoreInfoDetails(){

	}

	public static final Parcelable.Creator<StoreInfoDetails> CREATOR = new Parcelable.Creator<StoreInfoDetails>() {
		public StoreInfoDetails createFromParcel(Parcel in) {
			return new StoreInfoDetails(in);
		}

		public StoreInfoDetails[] newArray(int size) {
			return new StoreInfoDetails[size];
		}
	};

	

	public int getTotal_page() {
		return total_page;
	}

	public void setTotal_page(int total_page) {
		this.total_page = total_page;
	}

	public int getTotal_record() {
		return total_record;
	}

	public void setTotal_record(int total_record) {
		this.total_record = total_record;
	}

	public ArrayList<StoreInfo> getStoreInfoArr() {
		return storeInfoArr;
	}

	public void setStoreInfoArr(ArrayList<StoreInfo> storeInfoArr) {
		this.storeInfoArr = storeInfoArr;
	}


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(total_page);
		dest.writeInt(total_record);

		dest.writeList(storeInfoArr);
		
	}
	
	
	

}

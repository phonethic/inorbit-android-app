package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class SpecialPost implements Parcelable {


	public String id;
	public String place_id;
	public String post_type;
	public String title;

	public String description;
	public String image_title1;
	public String image_title2;
	public String image_title3;

	public String image_url1;
	public String image_url2;
	public String image_url3;


	public String thumb_url1;
	public String thumb_url2;
	public String thumb_url3;

	public String offer_date_time;

	public String created_at;

	private String mall_id;


	public SpecialPost(Parcel source){




		id          = source.readString();
		place_id 	= source.readString();
		post_type 	= source.readString();
		title 		= source.readString();

		//check for new variable
		//title_new = source.readString();
		description = source.readString();
		image_title1= source.readString();
		image_title2= source.readString();
		image_title3= source.readString();


		image_url1 	= source.readString();
		image_url2 	= source.readString();
		image_url3  = source.readString(); 

		thumb_url1 	= source.readString();
		thumb_url2 	= source.readString();
		thumb_url3 	= source.readString();

		offer_date_time 		= source.readString();
		created_at 	= source.readString();

		mall_id = source.readString();

	}


	public SpecialPost(){

	}






	public String getMall_id() {
		return mall_id;
	}


	public void setMall_id(String mall_id) {
		this.mall_id = mall_id;
	}


	public String getImage_url3() {
		return image_url3;
	}


	public void setImage_url3(String image_url3) {
		this.image_url3 = image_url3;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getPlace_id() {
		return place_id;
	}
	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}
	public String getPost_type() {
		return post_type;
	}
	public void setPost_type(String post_type) {
		this.post_type = post_type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage_title1() {
		return image_title1;
	}
	public void setImage_title1(String image_title1) {
		this.image_title1 = image_title1;
	}
	public String getImage_title2() {
		return image_title2;
	}
	public void setImage_title2(String image_title2) {
		this.image_title2 = image_title2;
	}
	public String getImage_title3() {
		return image_title3;
	}
	public void setImage_title3(String image_title3) {
		this.image_title3 = image_title3;
	}
	public String getImage_url1() {
		return image_url1;
	}
	public void setImage_url1(String image_url1) {
		this.image_url1 = image_url1;
	}
	public String getImage_url2() {
		return image_url2;
	}
	public void setImage_url2(String image_url2) {
		this.image_url2 = image_url2;
	}
	public String getThumb_url3() {
		return thumb_url3;
	}
	public void setThumb_url3(String thumb_url3) {
		this.thumb_url3 = thumb_url3;
	}
	public String getOffer_date_time() {
		return offer_date_time;
	}
	public void setOffer_date_time(String offer_date_time) {
		this.offer_date_time = offer_date_time;
	}
	public String getCreated_at() {
		//Log.d("", "Inorbit Get Created Date "+this.created_at);
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getThumb_url1() {
		return thumb_url1;
	}


	public void setThumb_url1(String thumb_url1) {
		this.thumb_url1 = thumb_url1;
	}


	public String getThumb_url2() {
		return thumb_url2;
	}


	public void setThumb_url2(String thumb_url2) {
		this.thumb_url2 = thumb_url2;
	}


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub



		dest.writeString(id);
		dest.writeString(place_id);
		dest.writeString(post_type);
		dest.writeString(title);



		dest.writeString(description);
		dest.writeString(image_title1);
		dest.writeString(image_title2);
		dest.writeString(image_title3);

		dest.writeString(image_url1);
		dest.writeString(image_url2);
		dest.writeString(image_url3);

		dest.writeString(thumb_url1);
		dest.writeString(thumb_url2);
		dest.writeString(thumb_url3);

		dest.writeString(offer_date_time);
		dest.writeString(created_at);

		dest.writeString(mall_id);

	}



	public static final Parcelable.Creator<SpecialPost> CREATOR = new Parcelable.Creator<SpecialPost>() {
		public SpecialPost createFromParcel(Parcel in) {
			return new SpecialPost(in);
		}

		public SpecialPost[] newArray(int size) {
			return new SpecialPost[size];
		}
	};





}

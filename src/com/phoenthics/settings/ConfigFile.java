package com.phoenthics.settings;

public class ConfigFile {
	
	/*
	 *	0 : for customer;
	 * 	1 : for inorbit;
	 * 	2 : for phonethics;
	 */
	
	
	
	private static int ApplicationFor 	= 0;
	
	/*
	 *	true 	: for mall manager;
	 * 	false 	: for shop owner;
	 */
	
	private static boolean managerLoogedIn			= false;
	public static int ForCustomer 					= 0;
	public static int ForMerChant 					= 2;
	
	
	public static int ConnectionError 	= 1;
	public static int TimeOutError 		= 2;
	public static int NetworkError 		= 3;
	public static int ServerError 		= 4;
	public static int AuthFailure 		= 5;
	public static int ParseError 		= 6;
	public static int NointernetConnection 		= 6;
	   
	private int errorCode = ConnectionError;
	
	public static boolean isVolleyError = false;
	
	public ConfigFile(){
		
	}
	
	/*
	 * @return 0 : for Customer, 
	 * 1 - inorbit, 
	 * 2 - phonethics
	 */
	
	public static int getApplicationFor(){
		return ApplicationFor;
	}
	public static void setApplicationFor(int appfor){
		ApplicationFor = appfor;
	}

	public static boolean isManagerLoogedIn() {
		return managerLoogedIn;
	}

	public static void setManagerLoogedIn(boolean managerLoogedIn) {
		ConfigFile.managerLoogedIn = managerLoogedIn;
	}

	public  int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	

}
     
package com.phoenthics.adapter;


import java.util.ArrayList;

import com.phonethics.inorbit.InorbitLog;
import com.phonethics.inorbit.R;

import com.phonethics.model.PostDetail;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

public class MallEventsAdapter extends ArrayAdapter<PostDetail> {
	ArrayList<PostDetail> postArr;
	Activity context;
	LayoutInflater inflate;
	private String PHOTO_PARENT_URL;
	boolean isApproved;

	public MallEventsAdapter(Activity context, ArrayList<PostDetail> postArr) {
		super(context, R.layout.mall_events_list_layout,postArr);
		// TODO Auto-generated constructor stub
		this.postArr=postArr;
		this.isApproved = isApproved;

		inflate=context.getLayoutInflater();
		PHOTO_PARENT_URL=context.getResources().getString(R.string.photo_url);
		InorbitLog.d("Inorbit Inside adapter"+postArr.toString());

	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return postArr.size();
	}
	@Override
	public PostDetail getItem(int position) {
		// TODO Auto-generated method stub
		return postArr.get(position);
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		if(convertView==null)
		{
			MallEventsViewHolder holder = new MallEventsViewHolder();
			convertView=inflate.inflate(R.layout.mall_events_list_layout,null);

			holder.txtTitle=(TextView)convertView.findViewById(R.id.broadCastOffersText);
			holder.imgBroadcastLogo=(ImageView)convertView.findViewById(R.id.imgBroadcastLogo);
			holder.viewOverLay=(View)convertView.findViewById(R.id.viewOverLay);
			holder.imgBroadcastLogo.setScaleType(ScaleType.CENTER_INSIDE);
			holder.txtBroadCastStoreTotalLike=(TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalLike);
			holder.txtBroadCastStoreTotalViews = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalViews);
			holder.txtBroadCastStoreTotalShare = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalShare);
			

			convertView.setTag(holder);

		}
		final MallEventsViewHolder hold=(MallEventsViewHolder)convertView.getTag();
		/*hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);*/

		
		try
		{
			hold.txtTitle.setText(postArr.get(position).getTitle());
			String totalLike = postArr.get(position).getTotal_like();
			String totalView = postArr.get(position).getTotal_view();
			String totalShare = postArr.get(position).getTotal_share();
			if(totalLike.equalsIgnoreCase("0")){
				hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);
			}else{
				hold.txtBroadCastStoreTotalLike.setText(postArr.get(position).getTotal_like());
				hold.txtBroadCastStoreTotalLike.setVisibility(View.VISIBLE);
			}
			if(totalView.equalsIgnoreCase("0")){
				hold.txtBroadCastStoreTotalViews.setVisibility(View.GONE);
			}else{
				hold.txtBroadCastStoreTotalViews.setText(postArr.get(position).getTotal_view());
				hold.txtBroadCastStoreTotalViews.setVisibility(View.VISIBLE);
			}
			if(totalShare.equalsIgnoreCase("0")){
				hold.txtBroadCastStoreTotalShare.setVisibility(View.GONE);
			}else{
				hold.txtBroadCastStoreTotalShare.setText(postArr.get(position).getTotal_share());
				hold.txtBroadCastStoreTotalShare.setVisibility(View.VISIBLE);
			}
			
			

		}catch(Exception ex){
			ex.printStackTrace();
		}

		try{
			if(postArr.get(position).getImage_url1().toString().equalsIgnoreCase("")){
				InorbitLog.d("Inorbit Photo url "+"");
				hold.imgBroadcastLogo.setImageResource(R.drawable.ic_launcher);
			}else{
				String photo_source=postArr.get(position).getImage_url1().toString().replaceAll(" ", "%20");
				hold.viewOverLay.setVisibility(View.VISIBLE);
				try {
					Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(hold.imgBroadcastLogo);;
					InorbitLog.d("Inorbit Photo url "+photo_source);

				}catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return convertView;
	}

	

}

class MallEventsViewHolder{

	TextView txtTitle,txtDate,txtMonth;
	ImageView imgBroadcastLogo;
	View viewOverLay;
	TextView txtBroadCastStoreTotalLike;
	TextView txtBroadCastStoreTotalViews;
	TextView txtBroadCastStoreTotalShare;


}

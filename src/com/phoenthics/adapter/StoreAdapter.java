package com.phoenthics.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.android.volley.toolbox.NetworkImageView;
import com.phonethics.eventtracker.EventTracker;
import com.phonethics.inorbit.DBUtil;
import com.phonethics.inorbit.InboxListSelector;
import com.phonethics.inorbit.InorbitApp;
import com.phonethics.inorbit.InorbitLog;
import com.phonethics.inorbit.R;
import com.phonethics.inorbit.RestaurantMenuGridDisplayActivity;
import com.phonethics.inorbit.SessionManager;
import com.phonethics.inorbit.ShowRestaurantMenu;
import com.phonethics.inorbit.Tables;
import com.phonethics.inorbit.InboxListSelector.ListModel;

import com.phonethics.model.StoreInfo;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.sax.StartElementListener;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;

public class StoreAdapter extends ArrayAdapter<StoreInfo> /*implements Filterable*/{
	ArrayList<StoreInfo> storeInfos;
	Context context;
	LayoutInflater inflate;
	String isFav="",dbHasOffer,dbOffer;
	DBUtil dbutil;
	boolean showCallBtn;

	public StoreAdapter(Context context, int resource) {
		super(context, resource);
		// TODO Auto-generated constructor stub
	}

	public StoreAdapter(Context context, ArrayList<StoreInfo> storeInfos, boolean showCallBtn) {
		super(context, 0);
		// TODO Auto-generated constructor stub

		this.context 	= context;
		this.storeInfos = storeInfos;
		inflate=((Activity) context).getLayoutInflater();
		dbutil = new DBUtil(context);
		this.showCallBtn = showCallBtn;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return storeInfos.size();
	}
	
	@Override
	public StoreInfo getItem(int position) {
		return storeInfos.get(position);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=inflate.inflate(R.layout.layout_default_search_api,null);
			holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
			holder.relStoreBack = (RelativeLayout) convertView.findViewById(R.id.rel_store_back);
			holder.relStoreBack.setBackgroundResource(R.drawable.list_item_back);
			holder.imgStoreLogo.setScaleType(ScaleType.FIT_CENTER);
			holder.txtStore=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
			holder.txtDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
			holder.imgCall=(ImageView)convertView.findViewById(R.id.searchCall);
			holder.imgFav=(ImageView)convertView.findViewById(R.id.searchFav);
			holder.linearCall=(RelativeLayout)convertView.findViewById(R.id.linearCall);
			holder.txtBackOffers=(TextView)convertView.findViewById(R.id.txtBackOffers);
			holder.img_Star=(ImageView)convertView.findViewById(R.id.img_Star);
			holder.imgRemoveFav=(ImageView)convertView.findViewById(R.id.searchRemoveFav);
			holder.txtFav=(TextView)convertView.findViewById(R.id.txtFav);
			
			holder.txtCounter=(TextView)convertView.findViewById(R.id.txtCounter);
			holder.searchFront=(RelativeLayout)convertView.findViewById(R.id.searchFront);
			holder.searchBack=(RelativeLayout)convertView.findViewById(R.id.searchBack);
			holder.band=(TextView)convertView.findViewById(R.id.band);
			holder.textBookticket=(TextView)convertView.findViewById(R.id.textBookticket);
			holder.callBtn = (TextView) convertView.findViewById(R.id.callBtn);

			holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
			holder.txtSearchStoreTotalLike=(TextView)convertView.findViewById(R.id.txtSearchStoreTotalLike);
			holder.txtSearchStoreTotalRating = (TextView) convertView.findViewById(R.id.txtSearchStoreTotalRating);

			holder.txtSearchStoreDesc.setTypeface(InorbitApp.getTypeFace());
			holder.txtStore.setTypeface(InorbitApp.getTypeFaceTitle());
			holder.txtBackOffers.setTypeface(InorbitApp.getTypeFace());
			holder.txtFav.setTypeface(InorbitApp.getTypeFace());
			holder.txtSearchStoreDesc.setTypeface(InorbitApp.getTypeFace());
			holder.txtSearchStoreTotalLike.setTypeface(InorbitApp.getTypeFace());
			holder.txtSearchStoreTotalRating.setTypeface(InorbitApp.getTypeFace());
			holder.band.setTypeface(InorbitApp.getTypeFace());
			holder.band.setTextColor(Color.BLACK);
			
			convertView.setTag(holder);

		}
		final ViewHolder hold=(ViewHolder)convertView.getTag();

		//Added Temp
		try{
			hold.searchBack.setVisibility(View.GONE);
			hold.imgFav.setVisibility(View.GONE);

			//
			//load store logos.
			InorbitLog.d("Logo Url = "+context.getResources().getString(R.string.photo_url)+getItem(position).getImage_url().toString());
			if(getItem(position).getImage_url().toString().equalsIgnoreCase("")){
				hold.imgStoreLogo.setImageResource(R.drawable.ic_launcher);
			}else{
				String photo_source=getItem(position).getImage_url().toString().replaceAll(" ", "%20");
				try {
					Picasso.with(context).load(context.getResources().getString(R.string.photo_url)+photo_source)
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(hold.imgStoreLogo);

				} catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){
					e.printStackTrace();
				}

			}

			//set Total Like
			final float scale = context.getResources().getDisplayMetrics().density;
			/*hold.txtSearchStoreTotalLike.setVisibility(View.GONE);*/
			if(getItem(position).getTotal_like().equalsIgnoreCase("0")) {
				hold.txtSearchStoreTotalLike.setVisibility(View.GONE);
				//hold.relStoreBack.getLayoutParams()
				if (getItem(position).getTotal_rating().equals("") || getItem(position).getTotal_rating().equalsIgnoreCase("0") || 
						getItem(position).getTotal_rating().trim().equalsIgnoreCase("0.0")) {
					hold.txtSearchStoreTotalRating.setVisibility(View.GONE);
				} else {
					hold.txtSearchStoreTotalRating.setVisibility(View.VISIBLE);
					hold.txtSearchStoreTotalRating.setText(getItem(position).getTotal_rating());
					RelativeLayout.LayoutParams  rlp = (RelativeLayout.LayoutParams)hold.txtSearchStoreTotalRating.getLayoutParams();
					switch (context.getResources().getDisplayMetrics().densityDpi) {
			        case DisplayMetrics.DENSITY_LOW:
			        case DisplayMetrics.DENSITY_MEDIUM:
			        case DisplayMetrics.DENSITY_HIGH:
			        	rlp.setMargins(0, 0, 10, 0);
			        	break;
			        case DisplayMetrics.DENSITY_XHIGH:
			        	rlp.setMargins(0, 0, 15, 0);
			            break;
			        case DisplayMetrics.DENSITY_XXHIGH:
			        	rlp.setMargins(0, 0, 25, 0);
			            break;
					}
					hold.txtSearchStoreTotalRating.setLayoutParams(rlp);
				}
			} else {
				/*RelativeLayout.LayoutParams  rlp = (RelativeLayout.LayoutParams)hold.txtSearchStoreTotalLike.getLayoutParams();
				rlp.setMargins(0, 0, 0, 0);
				hold.txtSearchStoreTotalLike.setLayoutParams(rlp);*/
				hold.txtSearchStoreTotalLike.setVisibility(View.VISIBLE);
				if (getItem(position).getTotal_rating().equals("") || getItem(position).getTotal_rating().equalsIgnoreCase("0") || 
						getItem(position).getTotal_rating().trim().equalsIgnoreCase("0.0")) {
					hold.txtSearchStoreTotalRating.setVisibility(View.GONE);
				} else {
					hold.txtSearchStoreTotalRating.setVisibility(View.VISIBLE);
					hold.txtSearchStoreTotalRating.setText(getItem(position).getTotal_rating());
					RelativeLayout.LayoutParams  rlp = (RelativeLayout.LayoutParams)hold.txtSearchStoreTotalLike.getLayoutParams();
					rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					rlp.setMargins(0, 0, 0, 0);
					hold.txtSearchStoreTotalLike.setLayoutParams(rlp);
					rlp = (RelativeLayout.LayoutParams)hold.txtSearchStoreTotalRating.getLayoutParams();

					switch (context.getResources().getDisplayMetrics().densityDpi) {
			        case DisplayMetrics.DENSITY_LOW:
			        case DisplayMetrics.DENSITY_MEDIUM:
			        case DisplayMetrics.DENSITY_HIGH:
			        	rlp.setMargins(0, 0, 60, 0);
			            break;
			        case DisplayMetrics.DENSITY_XHIGH:
			        	rlp.setMargins(0, 0, 95, 0);
			            break;
			        case DisplayMetrics.DENSITY_XXHIGH:
			        	rlp.setMargins(0, 0, 150, 0);
			            break;
			        }
					
					rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					hold.txtSearchStoreTotalRating.setLayoutParams(rlp);
				}
				/*rlp = (RelativeLayout.LayoutParams)hold.txtSearchStoreTotalRating.getLayoutParams();
				rlp.setMargins(0, 0, 90, 0);*/
				//hold.txtSearchStoreTotalRating.setLayoutParams(rlp);
			}
			hold.txtSearchStoreTotalLike.setText(getItem(position).getTotal_like());
			
			//hold.txtSearchStoreTotalLike.setText("1262");

			//set store name.
			hold.txtStore.setText(getItem(position).getName().toString());

			hold.txtSearchStoreDesc.setText(getItem(position).getDescription());

			if(getItem(position).getName().toString().equalsIgnoreCase("Inox") 
					|| getItem(position).getName().toString().equalsIgnoreCase("Cinemax") 
					|| getItem(position).getName().toString().equalsIgnoreCase("Cinepolis")){
				hold.textBookticket.setText("Book Ticket");
				hold.textBookticket.setVisibility(View.VISIBLE);
				RelativeLayout.LayoutParams params = (LayoutParams) hold.textBookticket.getLayoutParams();
				params.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
				int height = params.height;/*new RelativeLayout.LayoutParams(0, hold.textBookticket.getHeight());*/
				hold.textBookticket.setLayoutParams(params);
				hold.callBtn.setVisibility(View.GONE);
				hold.textBookticket.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						
						try{
							HashMap<String, String> params = new HashMap<String, String>();
							String url="";

							String activeAreaId = dbutil.getActiveMallId();
							String place_id = dbutil.getMallPlaceParentByMallID(activeAreaId);
							String areaName = dbutil.getAreaNameByInorbitId(activeAreaId);
							
							createInhouseAnalyticsEvent(getItem(position).getId(), "6", activeAreaId);
							
							if(areaName.equalsIgnoreCase("Malad")){
								params.put("Mall","Malad");
								url = context.getResources().getString(R.string.inox_malad);
								EventTracker.logEvent(context.getResources().getString(R.string.book_ticket), params);
							}else if(areaName.equalsIgnoreCase("Pune")){
								params.put("Mall","Pune");
								url = context.getResources().getString(R.string.inox_pune);
								EventTracker.logEvent(context.getResources().getString(R.string.book_ticket), params);
							}else if(areaName.equalsIgnoreCase("Cyberabad")){
								params.put("Mall","Cyberabad");
								url = context.getResources().getString(R.string.inox_cyberabad);
								EventTracker.logEvent(context.getResources().getString(R.string.book_ticket), params);
							} else if (areaName.equalsIgnoreCase("Vadodara")) {
								params.put("Mall", "Vadodara");
								url = context.getResources().getString(R.string.inox_vadodara);
								EventTracker.logEvent(context.getResources().getString(R.string.book_ticket), params);
							}

							if(!url.equalsIgnoreCase("")){
								Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
								context.startActivity(browserIntent);
							}

						}catch(Exception ex){
							ex.printStackTrace();
						}

						/*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getResources().getString(R.string.inox_malad)));
					context.startActivity(browserIntent);*/
					}
				});
			} else {
				if (getItem(position).getCategory().toString().contains("Dine")) {
					hold.textBookticket.setText(" Menu ");
					hold.textBookticket.setVisibility(View.VISIBLE);
					RelativeLayout.LayoutParams params = (LayoutParams) hold.textBookticket.getLayoutParams();
					params.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
					int height = params.height;/*new RelativeLayout.LayoutParams(0, hold.textBookticket.getHeight());*/
					hold.textBookticket.setLayoutParams(params);
					hold.textBookticket.setVisibility(View.VISIBLE);
					hold.callBtn.setVisibility(View.GONE);
					
					hold.textBookticket.setOnClickListener(new OnClickListener() {
	
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(context, RestaurantMenuGridDisplayActivity.class);
							intent.putExtra("mall_id", getItem(position).getPlace_parent());
							intent.putExtra("place_id", getItem(position).getId());
							intent.putExtra("status", "1");
							context.startActivity(intent);
							try {
								HashMap<String, String> map = new HashMap<String, String>();
								String activeAreaId = dbutil.getActiveMallId();
								dbutil.getMallPlaceParentByMallID(activeAreaId);
								String areaName = dbutil.getAreaNameByInorbitId(activeAreaId);
								String storeName = getItem(position).getName();
								if(areaName!=null && !areaName.equals("")) {
									map.put("MallName", areaName);
								}
								if (storeName != null && !storeName.equals("")) {
									map.put("StoreName", storeName);
								}
								EventTracker.logEvent(context.getResources().getString(R.string.storeList_Menu), map);
							} catch (Exception e) {}
						}
					});
					
					if (getItem(position).getCategory().toString().contains("Fine Dine") || getItem(position).getCategory().toString().contains("Pamper")) {
						if (showCallBtn)
							hold.callBtn.setVisibility(View.VISIBLE);
						else 
							hold.callBtn.setVisibility(View.GONE);
						

						hold.callBtn.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								String storeName = "";
								String areaName = "";
								try {
									String activeAreaId = dbutil.getActiveMallId();
									dbutil.getMallPlaceParentByMallID(activeAreaId);
									areaName = dbutil.getAreaNameByInorbitId(activeAreaId);
									storeName = getItem(position).getName();
								} catch (Exception e) {}
								
								try {
									ArrayList<String> nos = new ArrayList<String>();
									
									String number = getItem(position).getMob_no1();
									if (number != null && number.length() > 1) {
										nos.add("+"+number);
									} 
									
									number = getItem(position).getMob_no2();
									if (number != null && number.length() > 1) {
										nos.add("+"+number);
									}
									
									number = getItem(position).getMob_no3();
									if (number != null && number.length() > 1) {
										nos.add("+"+number);
									}
									
									number = getItem(position).getTel_no1();
									if (number != null && number.length() > 1) {
										nos.add(number);
									}
									
									number = getItem(position).getTel_no2(); 
									if (number != null && number.length() > 1) {
										nos.add(number);
									}
									
									number = getItem(position).getTel_no3(); 
									if (number != null && number.length() > 1) {
										nos.add(number);
									}
									
									number = getItem(position).getToll_free_no1();
									if (number != null && number.length() > 1) {
										nos.add(number);
									}
									
									number = getItem(position).getToll_free_no2();
									if (number != null && number.length() > 1) {
										nos.add(number);
									}
									// TODO Auto-generated method stub
									/*uploadCallEvents(false);*/
									if(nos.size()==0){
										Toast.makeText(context, R.string.number_not_registered, 0).show();
									}else if(nos.size()==1){
										callThisNumber(nos.get(0), storeName, areaName);
									}else{
										showNmberDialog(nos, storeName, areaName);
									}
								} catch(Exception e) {
									e.printStackTrace();
								}
							}
						});
					} else {
						hold.callBtn.setVisibility(View.GONE);
						hold.txtSearchStoreTotalLike.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
						hold.txtSearchStoreTotalRating.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
					}
				} else if (getItem(position).getCategory().toString().contains("Pamper")) {
					hold.textBookticket.setText("");
					RelativeLayout.LayoutParams params = (LayoutParams) hold.textBookticket.getLayoutParams();
					params.width = 0;
					int height = params.height;/*new RelativeLayout.LayoutParams(0, hold.textBookticket.getHeight());*/
					hold.textBookticket.setLayoutParams(params);
					hold.textBookticket.setVisibility(View.VISIBLE);
					if (showCallBtn)
						hold.callBtn.setVisibility(View.VISIBLE);
					else 
						hold.callBtn.setVisibility(View.GONE);
					hold.textBookticket.setOnClickListener(new OnClickListener() {
	
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(context, RestaurantMenuGridDisplayActivity.class);
							intent.putExtra("mall_id", getItem(position).getPlace_parent());
							intent.putExtra("place_id", getItem(position).getId());
							intent.putExtra("status", "1");
							context.startActivity(intent);
							try {
								HashMap<String, String> map = new HashMap<String, String>();
								String activeAreaId = dbutil.getActiveMallId();
								dbutil.getMallPlaceParentByMallID(activeAreaId);
								String areaName = dbutil.getAreaNameByInorbitId(activeAreaId);
								String storeName = getItem(position).getName();
								if(areaName!=null && !areaName.equals("")) {
									map.put("MallName", areaName);
								}
								if (storeName != null && !storeName.equals("")) {
									map.put("StoreName", storeName);
								}
								EventTracker.logEvent(context.getResources().getString(R.string.storeList_Menu), map);
							} catch (Exception e) {}
						}
					});
					
					hold.callBtn.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							try {
								ArrayList<String> nos = new ArrayList<String>();
								
								String number = getItem(position).getMob_no1();
								if (number != null && number.length() > 1) {
									nos.add(number);
								} 
								
								number = getItem(position).getMob_no2();
								if (number != null && number.length() > 1) {
									nos.add(number);
								}
								
								number = getItem(position).getMob_no3();
								if (number != null && number.length() > 1) {
									nos.add(number);
								}
								
								number = getItem(position).getTel_no1();
								if (number != null && number.length() > 1) {
									nos.add(number);
								}
								
								number = getItem(position).getTel_no2(); 
								if (number != null && number.length() > 1) {
									nos.add(number);
								}
								
								number = getItem(position).getTel_no3(); 
								if (number != null && number.length() > 1) {
									nos.add(number);
								}
								
								number = getItem(position).getToll_free_no1();
								if (number != null && number.length() > 1) {
									nos.add(number);
								}
								
								number = getItem(position).getToll_free_no2();
								if (number != null && number.length() > 1) {
									nos.add(number);
								}
								
								String activeAreaId = dbutil.getActiveMallId();
								String areaName = dbutil.getAreaNameByInorbitId(activeAreaId);
								String storeName = getItem(position).getName();
								// TODO Auto-generated method stub
								/*uploadCallEvents(false);*/
								if(nos.size()==0){
									Toast.makeText(context, R.string.number_not_registered, 0).show();
								}else if(nos.size()==1){
									callThisNumber(nos.get(0), storeName, areaName);
								}else{
									showNmberDialog(nos, storeName, areaName);
								}
	
							
							} catch(Exception e) {
								e.printStackTrace();
							}
						}
					});
				} else {
					hold.textBookticket.setVisibility(View.GONE);
					hold.callBtn.setVisibility(View.GONE);
					hold.txtSearchStoreTotalLike.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
					hold.txtSearchStoreTotalRating.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
				}
			}

			hold.txtDistance.setVisibility(View.GONE);

			isFav=dbutil.getGeoFavorite(Integer.parseInt(getItem(position).getId()));

			hold.band.setVisibility(View.VISIBLE);
			if(getItem(position).getHas_offer().toString().length()!=0 && getItem(position).getHas_offer().equalsIgnoreCase("1")){
				hold.txtBackOffers.setText(getItem(position).getTitle());
				hold.band.setText("Special Offer : "+getItem(position).getTitle());
				hold.band.setTextColor(Color.WHITE);
			}else{
				hold.txtBackOffers.setText("offers");
				hold.band.setVisibility(View.GONE);

			}
			if(Tables.getCurrentCategory() == Tables.CATEGORY_All){
				hold.txtCounter.setText("Directory - "+getCount()+" Stores");
			} else if (Tables.getCurrentCategory() == Tables.CATEGORY_DINE) {
				hold.txtCounter.setText("Dine - "+getCount()+" Stores");
			} else if (Tables.getCurrentCategory() == Tables.CATEGORY_ENTERTAINMENT) {
				hold.txtCounter.setText("Entertainment - "+getCount()+" Stores");
			} else if (Tables.getCurrentCategory() == Tables.CATEGORY_PAMPERZONE) {
				hold.txtCounter.setText("Pamper Zone - "+getCount()+" Stores");
			} else if (Tables.getCurrentCategory() == Tables.CATEGORY_SHOP) {
				hold.txtCounter.setText("Shop - "+getCount()+" Stores");
			}else if (Tables.getCurrentCategory() == Tables.CATEGORY_OFFER) {
				hold.txtCounter.setText("Offers - "+getCount()+" Stores");
			}
			hold.txtCounter.setTypeface(InorbitApp.getTypeFace(),Typeface.BOLD);
			
			
			if(position==0){
				hold.txtCounter.setVisibility(View.VISIBLE);
			}else{
				hold.txtCounter.setVisibility(View.GONE);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}



		return convertView;
	}
	
	void showNmberDialog(final ArrayList<String> nos, final String storeName, final String mallName){

		final Dialog phoneNumberDialog = new Dialog(context);
		phoneNumberDialog.setContentView(R.layout.mallinfonumbersdialog);
		phoneNumberDialog.setTitle("Please select a number to call");
		phoneNumberDialog.setCancelable(true);
		phoneNumberDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		TextView title = (TextView) phoneNumberDialog.findViewById(android.R.id.title);
		Button bttn = (Button) phoneNumberDialog.findViewById(R.id.Ok);

		title.setTypeface(InorbitApp.getTypeFace());

		ListView numberlist = (ListView) phoneNumberDialog.findViewById(R.id.numberlist);
		numberlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				phoneNumberDialog.dismiss();
				callThisNumber(nos.get(arg2), storeName, mallName);
			}
		});

		bttn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				phoneNumberDialog.dismiss();
			}
		});

		NumberListAdapter adapter = new NumberListAdapter((Activity) context, nos);
		numberlist.setAdapter(adapter);
		phoneNumberDialog.show();
	}
	
	void callThisNumber(String number, String storeName, String mallName){
		try{
			boolean hasTelephony = context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
			if (hasTelephony) { 
				/*uploadCallEvents(true);*/
				try {
					HashMap<String, String> map = new HashMap<String, String>();
					if(mallName!=null && !mallName.equals("")) {
						map.put("MallName", mallName);
					}
					if (storeName != null && !storeName.equals("")) {
						map.put("StoreName", storeName);
					}
					if (number != null && !number.equals("")) {
						map.put("number", number);
					}
					EventTracker.logEvent(context.getResources().getString(R.string.storeList_call), map);
				} catch (Exception e) {}
				
				Intent call = new Intent(android.content.Intent.ACTION_DIAL);
				call.setData(Uri.parse("tel:" + number));
				context.startActivity(call);
				/*overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);*/
			}else{
				Toast.makeText(context, "Calling functionality is not available in this device.", Toast.LENGTH_LONG).show();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	/*void uploadCallEvents(boolean callDone, String mallName, String storeName){
		try{
			boolean param = false;
			InorbitLog.d("MallName - "+mallName + " StroeName "+storeName );
			HashMap<String, String> flurryEeventParams = new HashMap<String, String>();
			if(mallName!=null && !mallName.equals("")){
				flurryEeventParams.put("MallName", mallName);
				param = true;
			}else{
				param = false;
			}
			if(storeName!=null && !storeName.equals("")){
				flurryEeventParams.put("StoreName", storeName +" - "+mallName);
				param = true;
			}else{
				param = false;
			}
			if(param){
				if(callDone){
					EventTracker.logEvent(context.getResources().getString(R.string.storeDetail_CallDone), flurryEeventParams);
				}else{
					EventTracker.logEvent(context.getResources().getString(R.string.storeDetail_Call), flurryEeventParams);
				}

			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}*/
	
/*	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		 return new Filter() {
	            @Override
	            protected FilterResults performFiltering(CharSequence charSequence) {
	                List<ListModel> filteredResult = getFilteredResults(charSequence.toString());

	                FilterResults results = new FilterResults();
	                results.values = filteredResult;
	                results.count = filteredResult.size();
	                
	                return results;
	            }
	            
	            @Override
	            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
	            	if (filterResults!= null && filterResults.count > 0) {
	            		storeInfos = (ArrayList<StoreInfo>) filterResults.values;
	            	} else {
	            		storeInfos = new ArrayList<StoreInfo>();
	            	}
	            	InorbitLog.d("null "+(filterResults==null));
	                StoreAdapter.this.notifyDataSetChanged();
	            }


	            private ArrayList<StoreInfo> getFilteredResults(String constraint){
	                if (constraint.length() == 0){
	                    return  parentList;
	                }
	                ArrayList<StoreInfo> listResult = new ArrayList<StoreInfo>();
	                for (StoreInfo obj : parentList) {
	                	InorbitLog.d(obj.getName()+" "+constraint);
	                    if (constraint.length()<=obj.getName().length() && constraint.equalsIgnoreCase(obj.getName().subSequence(0, constraint.length()).toString())) {
	                        listResult.add(obj);
	                    }
	                }
	                return listResult;
	            }
	        };
	}*/
	
	private void createInhouseAnalyticsEvent(String flag, String activityId, String mallId) {
		Uri uri = new Uri.Builder().scheme("content").authority(context.getResources().getString(R.string.authority)).
				appendPath("/insert").build();
        ContentValues cv = new ContentValues(6);
		cv.put("UDM_ID", new SessionManager(context).getUdmIDForCustomer());
		cv.put("DATE", new Date().toString());
		cv.put("FLAG", flag);
		cv.put("ACTIVITY_ID", activityId);
		cv.put("MALL_ID", mallId);
		cv.put("SYNC_STATUS", -1);
		context.getContentResolver().insert(uri, cv);
	}

	class ViewHolder
	{
		ImageView imgStoreLogo,imgCall,imgFav,img_Star,imgRemoveFav;
		TextView txtStore,band,txtSearchStoreDesc,textBookticket, callBtn;
		TextView txtDistance,txtBackOffers,txtFav,txtCounter;
		TextView txtSearchStoreTotalLike, txtSearchStoreTotalRating;
		RelativeLayout linearCall;
		RelativeLayout searchFront,searchBack,relStoreBack;
	}
	
	private void showToast(String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}
	
	public class NumberListAdapter extends ArrayAdapter<String> {

		Activity context;
		LayoutInflater inflator = null;
		ArrayList<String> phoneNumberWithCode;

		public NumberListAdapter(Activity context, ArrayList<String> phoneNumberWithCode) {
			super(context,R.layout.phonenumberlayout);
			this.context = context;
			//this.data = data;
			this.phoneNumberWithCode = phoneNumberWithCode;

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			View rowView = convertView;

			final int pos = position;

			if(convertView == null){
				inflator = context.getLayoutInflater();
				rowView = inflator.inflate(R.layout.phonenumberlayout, null);
				ViewHolder1 holder = new ViewHolder1();

				holder.text = (TextView) rowView.findViewById(R.id.numberText);
				holder.text.setTypeface(InorbitApp.getTypeFace());

				rowView.setTag(holder);
			}

			ViewHolder1 hold = (ViewHolder1) rowView.getTag();
			hold.text.setText(phoneNumberWithCode.get(position).toString());

			return rowView;
		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return phoneNumberWithCode.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return phoneNumberWithCode.get(position);
		}
		
		private class ViewHolder1 {
			private TextView text;
		}
		
	}
}

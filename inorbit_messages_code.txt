 switch ($var) 
        {
        
        //---------Merchant api code start from 100 --------

            /* -----Merchant Api include following Request----
                1.merchant_api/merchant_post 
                2.merchant_api/set_password_put
                3.merchant_api/mobile_verify_code_get
                4.merchant_api/validate_credentials_post
                5.merchant_api/logout_get
                6.merchant_api/count_get
                7.merchant_api/special_date_get
            */
            
            case '-100':
                return "Invalid Data.";
                break;

            case '-101':
                return  "Thank you registering with Inorbit. You will get verification code to your mobile number."; //"To verify your mobile number please press call or give a missed call on '01246758608'."; 
                break;

            case '-102':
                return "Your mobile number already registered. You would get a verification SMS soon.";
                break;
            
            case '-103':
                return "Failed to send verification code. Please try after some time.";
                break;

            case '-104':
                return "Great, your mobile number has been verified. You are seconds away from registering your business on Inorbit."; 
                break;

            case '-105':
                return "Please try after some time.";
                break;

            case '-106':
                return "Password is set successfully.";
                break;

            case '-107':
                return "Password has been already set."; 
                break;

            case '-108':
                return "Invalid Verification Code."; 
                break;

            case '-109':
                return "Please check the mobile number that you have entered."; 
                break;

            case '-110':
                return "Your mobile number is not verified yet. Please press call or give a missed call on '01246758608'."; 
                break;
        
            case '-111':
                return 'The mobile number field is required.'; 
                break;

            case '-112':
                return 'Mobile number and Password does not match.';
                break;

            case '-113':
                return 'Login successful.'; 
                break;

            case '-114':
                return 'Logout successful.'; 
                break;

             case '-115':
                return 'Authentication Crendentials is missing.'; 
                break;

            case '-116':
                return 'Invalid Authentication Code.'; 
                break;

            case '-117':
                return 'Sorry, you are not administrator.'; 
                break;

            case '-118':
                return 'Not a single user added special event in his account.'; 
                break;

            case '-119':
                return 'Not a single merchant found.'; 
                break;

    //------------- error message for place api code start from -121 ---------

            /* -----Place Api include following Request----
                1.place_api/place_post
                2.place_api/place_put
                3.place_api/place_time_put
                4.place_api/place_gallery_post
                5.place_api/places_get
                6.place_api/place_gallery_get
                7.place_api/place_category_get
                8.place_api/place_delete
                9.place_api/search_get
                10.place_api/place_gallery_delete
                11.place_api/like_post
                12.place_api/like_delete
                13.place_api/share_post
                14.place_api/count_get
                15.place_api/gallery_get
                16.place_api/malls_get
            */

            case '-121':
                return 'Invalid Place ID.'; 
                break;

            case '-122':
                return 'Place has been added successfully.'; 
                break;

            case '-123':
                return 'Unable to add place information.';
                break;

            case '-124':
                return 'Image format only jpeg,png,jpg.';
                break;

            case '-125':
                return  'Place has been updated successfully.';
                break;

            case '-126':
                return  'Unable to update place information.';
                break;

            case '-127':
                return  'Place time information has been updated successfully.';
                break;

            case '-128':
                return  'Unable to update place time information.';
                break;
            
            case '-129':
                return  'Place gallery image has been added successfully.';
                break;

            case '-130':
                return  'Unable to add place gallery image.';
                break;

            case '-131':
                return  'Place has been deleted successfully.';
                break;
               
            case '-132':
                return  'Place gallery image has been deleted successfully.';
                break;

            case '-133':
                return  'Invalid Gallery Image ID.';
                break;
           
            case '-134':
                return   'Thank You for making this place as a favourite.';
                break;

            case '-135':
                return   'You have already made this place as a favourite.';
                break;

            case '-136':
                return   'Your place has been removed from favourites.';
                break;
                
            case '-137':
                return   'Unable to delete the place marked as a favourites.';
                break;

            case '-138':
                return  'share successfully.';
                break;

            case '-139':
                return  'Unable to share this place.';
                break;

            case '-140':
                return  'Please hide the business before you can delete it.';
                break;

            case '-141':
                return  'The Place ID field is required.';
                break;

             case '-142':
                return  'No record found.';
                break;

              case '-143':
                return  'No record found.';
                break;

             case '-144':
                return  'Not single image added in gallery.';
                break;

            case '-145':
                return  'Not a single category exists.';
                break;

            case '-146':
                return  'Place ID does not exists.';
                break;

            case '-147':
                return  'Place ID does not exists.';
                break;

            case '-148':
                return  'The Gallery image ID field is required.';
                break;

            case '-149':
                return  'Plcae ID does not exists.';
                break;

            case '-150':
                return  'Plcae ID does not exists.';
                break;

            case '-151':
                return  'Plcae ID does not exists.';
                break;

            case '-152':
                return  'No record found.';
                break;

            case '-153':
                return  'No record found.';
                break;

            case '-154':
                return  'No record found.';
                break;

            case '-155':
                return  'Whoops ! Looks like we don’t have anything here to show you yet. Come back soon!';
                break;

            case '-156':
                return  'Gallery reordered successfully.';
                break;

            case '-157':
                return  'Failed to reorder gallery.';
                break;

        //---------error message for broadcast api code  from -161 ------------

            /* -----Broadcast Api include following Request--------
                1.broadcast_api/broadcast_post 
                2.broadcast_api/broadcasts_get
                3.broadcast_api/broadcast_delete
                4.broadcast_api/like_post
                5.broadcast_api/like_delete
                6.broadcast_api/share_post
                7.broadcast_api/special_post
                8.broadcast_api/special_get
                9.broadcast_api/special_delete
                10.broadcast_api/search_get
                11.broadcast_api/special_expired_delete
            */

            case '-161': 
                return  'Yay ! Your Post has been sent to everyone using Inorbit.';
                break;
                
            case '-162': 
                return  'post failed.';
                break;

            case '-163': 
                return 'Image format only jpeg,png,jpg.';
                break;

            case '-164': 
                return  'Invalid Post ID.';
                break;
               
            case '-165': 
                return  'Post has been deleted successfully.';
                break;

            case '-166': 
                return  'Thank You for making this post as a favourite.';
                break;
                
            case '-167': 
                return  'You have already made this post as a favourite.';
                break;
                
            case '-168': 
                return 'Your post has been removed from favourites.';
                break;
 
            case '-169': 
                return 'Unable to delete the place marked as a favourites.';
                break;

            case '-170': 
                return 'share successfully .';
                break;

            case '-171': 
                return 'Unable to share this post.';
                break;

            case '-172': 
                return 'Whoops ! No broadcasts.';
                break;

            case '-173':
                return 'The post id field is required.';
                break;

            case '-174':
                return ' No such Post ID is exist.';
                break;

            case '-175':
                return 'Post ID does not exist.';
                break;

            case '-176':
                return 'Post ID does not exist.';
                break;

            case '-177':
                return 'Post limit exceeds';
                break;

             case '-178':
                return 'Post ID does not exist.';
                break;

            case '-179': 
                return 'Invalid Date.';
                break;

            case '-180':
                return 'Not a single user subscribe to this date.';
                break;

            case '-181': 
                return 'Not a single user marked this store as favourite.';
                break;

             case '-182': 
                return 'Not a single user marked this mall as an interested.';
                break;

            case '-183': 
                return 'Invalid Special Post ID.';
                break;

            case '-184': 
                return 'Special Post has been deleted successfully.';
                break;

            case'-185': 
                 return 'posted successfully to n shoppers.';
                break;

            case '-186':
                return 'Oops! No special post.';
                break;

            case '-187':
                return 'Special post id does not exist.';
                break;

            case '-188':
                return 'Post ID does not exist.';
                break;

             case '-189':
                return 'Whoops ! Looks like we don’t have anything here to show you yet. Come back soon!';
                break;

            case '-190':
                return 'The special post id field is required.';
                break;

            case '-191':
                return 'You have exceeded daily special posting limit.';
                break;

             case '-192':
                return 'All expired special post deleted successfully.';
                break;

             case '-193':
                return 'Not a single special post is expired.';
                break;


    //---------error message for User api from  200 ---------------

            /* -----user Api include following Request--------
                1.user_api/user_post
                2.user_api/set_password_put
                3.user_api/validate_credentials_post
                4.user_api/user_get
                5.user_api/ user_put
                6.user_api/logout_get
                7.user_api/verify_email_get
                8.user_api/mobile_verify_code_get
                9.user_api/favourite_places_get
                10.user_api/date_categories_get
                11.user_api/count_get
                12.user_api/interested_malls_post
                13.user_api/interested_malls_get
                14.user_api/special_get
            */

            case '-200':
                return "Invalid Data.";
                break;

            case '-201':
                return "Thank you registering with Inorbit. You will soon get activation code on your mobile number."; //"To verify your mobile number please press call or give a missed call on '01246758608'.";
                break;

            case '-202':
                return "Hey! It seems, You are already registered on Inorbit. We are sending a verification code to your mobile number. Just enter it below, choose a new password and you are good to go.";
                break;
            
            case '-203':
                return "Failed to send verification code. Please try after some time.";
                break;

            case '-204':
                return "awesome! Your mobile number has been verified. Choose a Password below and you’re good to go."; 
                break;

            case '-205':
                return "Mobile number is already verified.";
                break;

            case '-206':
                return "Please try after some time.";
                break;

            case '-207':
                return "Password is set successfully.";
                break;

            case '-208':
                return "Password has been already set."; 
                break;

            case '-209':
                return "Invalid Verification Code."; 
                break;

            case '-210':
                return "Please check the mobile number that you have entered."; 
                break;
           
            case '-211':
                return "The Mobile number field is required.";
                break;

            case '-212':
                return "Mobile number and Password does not match."; 
                break;

            case '-213':
                return 'Login successful.'; 
                break;

            case '-214':
                return 'Logout successful.'; 
                break;

            case '-215':
                return 'Authentication credentials is missing.'; 
                break;

            case '-216':
                return "Your mobile number is not verified yet. Please press call or give a missed call on '01246758608'."; 
                break;

            case '-217':
                return "This Facebook account is already linked with another Shoplocal account.";
                break;

            case '-218':
                return "Invalid facebook credentials.";
                break;

            case '-219':
                return "Invalid Verification code.";
                break;

            case '-221':
                return "Invalid Authentication Code.";
                break;

            case '-222':
                return "Image format only jpeg,png,jpg.";
                break;
            
            case '-223':
                return "The Place ID field is required.";
                break;

            case '-224':
                return "Not a single date category exists.";
                break;
            
             case '-225':
                return "Not a single customer found.";
                break;
  
            case '-231':
                return "Profile is exist.";
                break;

            case '-232':
                return "User profile does not exists.";
                break;

            case '-233':
                return "User profile updated successfully.";
                break;

            case '-234':
                return "Unable to update user profile.";
                break;

            case '-235': 
                return "You successfully updated your favourites malls.";
                break;

            case '-236': 
                return "Unable to update your favourites malls.";
                break;

            case '-237': 
                return "Not a single mall marked as an interested.";
                break;

            case '-238': 
                return "Whoops! No special posts for you.";
                break;

            case '-239': 
                return "Not a single store you marked as a favourite.";
                break;

                    
    //------------error message for feedback api from 400 ---
        
        /* -----Feedback Api include following Request----
                1.feedback_api/customer_post
                2.feedback_api/merchant_post
                3.feedback_api/reply_post
                4.feedback_api/feedback_get
                5.feedback_api/reply_get
            */        
            case '-401':
                return "Thank you for your valuable feedback.";
                break;

            case '-402':
                return "Failed to submit your feedback. Please try again later.";
                break;

            case '-403':
                return "Thank you for your valuable reply.";
                break;

            case '-404':
                return "Failed to submit your reply. Please try again later.";
                break;

            case '-405':
                return "Oops! No feedback from user.";
                break;

            case '-406':
                return "Oops! No reply from user.";
                break;

             case '-407':
                return "Thank you for your valuable feedback.";
                break;

            case '-408':
                return "Failed to submit your feedback. Please try again later.";
                break;

//----------------------error message for server maintaince from 500------------
            case '-500':
                return "Server is down for maintaince.";
                break;

            case '-501':
                return "A newer version of Inorbit is available. Kindly update the application to proceed."; //For Rest Api key chages
                break;

            case '-502':
                return "Unknown method."; 
                break;

            case '-503':
                return "Invalid Input."; //for form validation errors.
                break;

            case '0':
                return 'No records found.';
                break;
                
            default:
                return "Oops! something went wrong.";
                break;

        }